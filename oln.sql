-- phpMyAdmin SQL Dump
-- version 4.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 02, 2014 at 08:10 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oln`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`client_id` bigint(20) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `initials` varchar(10) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`client_id`, `name`, `initials`, `date_created`, `status_id`) VALUES
(1, 'Client 001', 'CL01', '2014-07-03 00:53:05', 1),
(2, 'Client 002', 'CL02', '2014-07-03 00:53:05', 1),
(3, 'Client 003', 'CL03', '2014-07-03 00:53:05', 1),
(4, 'Client 004', 'CL03', '2014-07-03 00:53:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
`location_id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `matter`
--

CREATE TABLE IF NOT EXISTS `matter` (
`matter_id` bigint(20) NOT NULL,
  `ref_no` bigint(20) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_id` bigint(20) DEFAULT NULL,
  `introducer_id` bigint(20) DEFAULT NULL COMMENT 'handler/staff, user_id from user table',
  `subject_matter` varchar(255) DEFAULT NULL,
  `matter` text,
  `handler_id` bigint(20) DEFAULT NULL COMMENT 'user_id with type handler',
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `matter`
--

INSERT INTO `matter` (`matter_id`, `ref_no`, `date_created`, `client_id`, `introducer_id`, `subject_matter`, `matter`, `handler_id`, `status_id`) VALUES
(1, 123456, '2014-07-03 00:27:25', 1, 2, 'Subject matter 001', 'This is the detailed description of the matter', 1, 1),
(2, 654321, '2014-07-03 00:28:14', 2, 1, 'Subject matter 002', 'Testing the second matter and it''s content', 1, 1),
(3, 987654, '2014-07-03 00:29:15', 2, 2, 'Criminal Case 123', 'This is a sample criminal case and it''s content', 1, 0),
(4, 123457, '2014-07-03 03:01:23', 3, 2, 'Criminal Case 147', 'This is where the details for this criminal case are located.', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rfid_tag`
--

CREATE TABLE IF NOT EXISTS `rfid_tag` (
`rfid_tag_id` bigint(20) NOT NULL,
  `tag_id` varchar(45) NOT NULL,
  `object_type` enum('matter','user') NOT NULL DEFAULT 'matter',
  `object_id` bigint(20) DEFAULT NULL COMMENT 'matter_id or user_id (depending on the object_type)',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rfid_tag_location`
--

CREATE TABLE IF NOT EXISTS `rfid_tag_location` (
`rfid_tag_location_id` bigint(20) NOT NULL,
  `rfid_tag_id` bigint(20) NOT NULL,
  `location_id` bigint(20) NOT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`user_id` bigint(20) NOT NULL,
  `type` enum('handler','staff') NOT NULL DEFAULT 'staff',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `initials` varchar(10) DEFAULT NULL,
  `email_address` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status_id` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `type`, `first_name`, `last_name`, `initials`, `email_address`, `password`, `remember_token`, `status_id`) VALUES
(1, 'staff', 'Richard Joseph', 'Porter', 'RJP', 'rjosephporter@gmail.com', '$2y$10$bEpEE0x4Lk/PKcQwpIJdkOHFnLm6K1F3OHWau4FEuf6pv846OFmui', 'PeStIr4TfSDcAvxBNShRhKUwgV1FYg9UGKI0FlugowFMTCN09kQ48y8GILIL', 1),
(2, 'handler', 'John', 'Doe', 'JD', 'john@mailinator.com', '$2y$10$xunMT1DJ5aQGhE.d/egxSOEj9gA3El.KKUlpxoHJrJIad3wPPP6iq', NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`client_id`), ADD KEY `ndx_status_id` (`status_id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
 ADD PRIMARY KEY (`location_id`), ADD KEY `ndx_status_id` (`status_id`);

--
-- Indexes for table `matter`
--
ALTER TABLE `matter`
 ADD PRIMARY KEY (`matter_id`), ADD UNIQUE KEY `ref_no_UNIQUE` (`ref_no`), ADD KEY `ndx_client_id` (`client_id`), ADD KEY `ndx_introducer_id` (`introducer_id`), ADD KEY `ndx_handler_id` (`handler_id`), ADD KEY `ndx_status_id` (`status_id`);

--
-- Indexes for table `rfid_tag`
--
ALTER TABLE `rfid_tag`
 ADD PRIMARY KEY (`rfid_tag_id`), ADD UNIQUE KEY `tag_id_UNIQUE` (`tag_id`), ADD KEY `ndx_object_type` (`object_type`), ADD KEY `ndx_object_id` (`object_id`), ADD KEY `ndx_status_id` (`status_id`);

--
-- Indexes for table `rfid_tag_location`
--
ALTER TABLE `rfid_tag_location`
 ADD PRIMARY KEY (`rfid_tag_location_id`), ADD UNIQUE KEY `unq_tag_location` (`rfid_tag_id`,`location_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `email_address_UNIQUE` (`email_address`), ADD KEY `ndx_status_id` (`status_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
MODIFY `location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `matter`
--
ALTER TABLE `matter`
MODIFY `matter_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rfid_tag`
--
ALTER TABLE `rfid_tag`
MODIFY `rfid_tag_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rfid_tag_location`
--
ALTER TABLE `rfid_tag_location`
MODIFY `rfid_tag_location_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

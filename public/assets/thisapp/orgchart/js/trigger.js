//Triggers

	//OLN Header
	$('#v-2087').click(function(){
		console.log("You click OLN!");
	});
	$('#v-2094').click(function(){
		console.log("You click Richard Healy!");
	});
	$('#v-2105').click(function(){
		console.log("You click Gordon Oldham!");
	});
	$('#v-2116').click(function(){
		console.log("You click Stephen Peaker!");
	});

	//OLN CONTENTS

	// CONSULTANTS
	$('#v-12').click(function(){
		console.log("You click Consultants!");
	});
	$('#v-19').click(function(){
		console.log("You click PhilGoh!");
	});
	$('#v-26').click(function(){
		console.log("You click Elizabeth Lim!");
	});
	$('#v-33').click(function(){
		console.log("You click Nancy Yu!");
	});
	$('#v-40').click(function(){
		console.log("You click Michael Dalton!");
	});
	$('#v-47').click(function(){
		console.log("You click Stephen Vine!");
	});
	$('#v-54').click(function(){
		console.log("You click Gregory Crichton!");
	});

	//INTELECTUAL PROPERTY
	$('#v-61').click(function(){
		console.log("You click Intellectual Property!");
	});
	$('#v-72').click(function(){
		console.log("You click Vera Sung!");
	});
	$('#v-79').click(function(){
		console.log("You click Monita Wong!");
	});
	$('#v-86').click(function(){
		console.log("You click Shanghai Office!");
	});
	$('#v-93').click(function(){
		console.log("You click She An!");
	});
	$('#v-100').click(function(){
		console.log("You click Yuki Yang!");
	});
	$('#v-107').click(function(){
		console.log("You click Echo Tang!");
	});
	$('#v-114').click(function(){
		console.log("You click Evelyne Yeung!");
	});
	$('#v-121').click(function(){
		console.log("You click Colin Chung!");
	});
	$('#v-132').click(function(){
		console.log("You click Elodie Dellavolta!");
	});
	$('#v-139').click(function(){
		console.log("You click Vincent Fong!");
	});

	//CORPORATE & COMMERCIAL
	$('#v-146').click(function(){
		console.log("You click Corporate & Commercial!");
	});
	$('#v-153').click(function(){
		console.log("You click Chris Holey!");
	});
	$('#v-160').click(function(){
		console.log("You click Ranjit Diillion!");
	});
	$('#v-167').click(function(){
		console.log("You click Stephanie Tam!");
	});
	$('#v-178').click(function(){
		console.log("You click Gary Wong!");
	});
	
	//DISPUTE RESOLUTION
	$('#v-185').click(function(){
		console.log("You click Dispute Resolution!");
	});
	$('#v-192').click(function(){
		console.log("You click Richard Healy II!");
	});
	$('#v-199').click(function(){
		console.log("You click Lisa Lee!");
	});
	$('#v-206').click(function(){
		console.log("You Click Dispute Resolution & Matrimonial Supporting Team!");
	});
	$('#v-221').click(function(){
		console.log("You Click Perry Lam!");
	});
	$('#v-228').click(function(){
		console.log("You Click Timothy Lee!");
	});
	$('#v-235').click(function(){
		console.log("You Click Thomas Lau!");
	});
	$('#v-242').click(function(){
		console.log("You Click Benny Lai!");
	});
	$('#v-249').click(function(){
		console.log("You Click Stephen Chan!");
	});
	$('#v-256').click(function(){
		console.log("You Click Maggie Wong!");
	});
	$('#v-267').click(function(){
		console.log("You Click Selwn Chan!");
	});
	$('#v-274').click(function(){
		console.log("You Click Connie Ma!");
	});
	$('#v-281').click(function(){
		console.log("You Click Vanessa Au-Yeung!");
	});
	$('#v-288').click(function(){
		console.log("You Click Olivia Kung!");
	});
	$('#v-295').click(function(){
		console.log("You Click Dora Yuen!");
	});
	$('#v-302').click(function(){
		console.log("You Click Anson Douglas!");
	});

	//MATRIMONIAL
	$('#v-309').click(function(){
		console.log("You Click Matrimonial!");
	});
	$('#v-316').click(function(){
		console.log("You Click Stephen Peaker II!");
	});
	$('#v-323').click(function(){
		console.log("You Click Brenda Leong!");
	});
	$('#v-330').click(function(){
		console.log("You Click Paul Firmin!");
	});
	$('#v-337').click(function(){
		console.log("You Click Madelaine Gomez!");
	});
	$('#v-344').click(function(){
		console.log("You Click Pamy Kuo!");
	});
	$('#v-351').click(function(){
		console.log("You Click Ivy Wong!");
	});
	$('#v-358').click(function(){
		console.log("You Click Kenneth Yung!");
	});
	$('#v-365').click(function(){
		console.log("You Click Janet Chung!");
	});
	$('#v-372').click(function(){
		console.log("You Click Nicole Cavanagh!");
	});
	$('#v-379').click(function(){
		console.log("You Click Iris Tsang!");
	});
	$('#v-390').click(function(){
		console.log("You Click Jojo Lam!");
	});
	$('#v-397').click(function(){
		console.log("You Click Yvonne Kwong!");
	});
	$('#v-404').click(function(){
		console.log("You Click Carmen Liu!");
	});
	$('#v-411').click(function(){
		console.log("You Click Madelaine Cheng!");
	});
	
	//BUSINESS ADVISORY
	$('#v-418').click(function(){
		console.log("You Click Business Advisory!");
	});
	$('#v-425').click(function(){
		console.log("You Click Gordon Oldham II!");
	});
	$('#v-432').click(function(){
		console.log("You Click Louise Longford!");
	});
	$('#v-443').click(function(){
		console.log("You Click Lai Ming Man!");
	});
	$('#v-450').click(function(){
		console.log("You Click Anna Chan!");
	});
	$('#v-457').click(function(){
		console.log("You Click Rebecca Fong!");
	});
	$('#v-468').click(function(){
		console.log("You Click Jonathan Lam!");
	});
	$('#v-475').click(function(){
		console.log("You Click Victor Ng!");
	});
	$('#v-482').click(function(){
		console.log("You Click Lucie Igoe!");
	});

	// Private Clients
	$('#v-493').click(function(){
		console.log("You Click Private Clients!");
	});
	$('#v-500').click(function(){
		console.log("You Click Alfred Ip!");
	});
	$('#v-507').click(function(){
		console.log("You Click Nelly Miu!");
	});
	$('#v-514').click(function(){
		console.log("You Click Connie Ma II!");
	});
	$('#v-521').click(function(){
		console.log("You Click Vanessa Au-Yeung II!");
	});
	$('#v-528').click(function(){
		console.log("You Click Thomas Lau II!");
	});

	// Employment & Business Immigration
	$('#v-535').click(function(){
		console.log("You Click Employment & Business Immigration!");
	});
	$('#v-548').click(function(){
		console.log("You Click Adam Hugill!");
	});
	$('#v-555').click(function(){
		console.log("You Click Maggie Wong!");
	});

	//MERGERS & ACQUISITIONS
	$('#v-562').click(function(){
		console.log("You Click Mergers & Acquisitions!");
	});
	$('#v-569').click(function(){
		console.log("You Click Tracy Yip!");
	});
	$('#v-576').click(function(){
		console.log("You Click Christy So!");
	});
	$('#v-583').click(function(){
		console.log("You Click Tony Chik!");
	});
	$('#v-590').click(function(){
		console.log("You Click Yan Chung!");
	});
	$('#v-597').click(function(){
		console.log("You Click Jade Tang!");
	});
	$('#v-604').click(function(){
		console.log("You Click Peony Wu!");
	});

	// BUSINESS & DEVELOPMENT
	$('#v-611').click(function(){
		console.log("You Click Business & Development!");
	});
	$('#v-618').click(function(){
		console.log("You Click Scherzade Burden!");
	});
	$('#v-631').click(function(){
		console.log("You Click Dora Yuen II!");
	});

	//NEW TRAINEES
	$('#v-638').click(function(){
		console.log("You Click New Trainees!");
	});
	$('#v-645').click(function(){
		console.log("You Click Henry Fung!");
	});
	$('#v-652').click(function(){
		console.log("You Click Vievien Wong!");
	});
	$('#v-659').click(function(){
		console.log("You Click Tony Tam!");
	});
	$('#v-666').click(function(){
		console.log("You Click Amelia Lo!");
	});

	// ACCOUNTING
	$('#v-673').click(function(){
		console.log("You Click Accounting!");
	});
	$('#v-680').click(function(){
		console.log("You Click Terence Siu!");
	});
	$('#v-691').click(function(){
		console.log("You Click Chinia Lee!");
	});
	$('#v-702').click(function(){
		console.log("You Click Joey Li!");
	});

	//ADMINISTRATION
	$('#v-709').click(function(){
		console.log("You Click Administration!");
	});
	$('#v-716').click(function(){
		console.log("You Click Robert Campbell!");
	});
	$('#v-727').click(function(){
		console.log("You Click Helen Li!");
	});
	$('#v-734').click(function(){
		console.log("You Click Maggie Nee!");
	});
	$('#v-741').click(function(){
		console.log("You Click Tom Wong!");
	});
	$('#v-748').click(function(){
		console.log("You Click Nicholl Cheung!");
	});
	$('#v-755').click(function(){
		console.log("You Click May Chan!");
	});
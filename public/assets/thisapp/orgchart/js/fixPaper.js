//Organizational Chart
var graph = new joint.dia.Graph;

    var paper = new joint.dia.Paper({
        el: $('#paper'),
        width: 910,
        height: 330,
        gridSize: 1,
        model: graph,
        perpendicularLinks: true,
        interactive : false
    });

    var member = function(x, y, rank, name, image, background, border) {

        var cell = new joint.shapes.org.Member({
            position: { x: x, y: y },
            attrs: {
                '.card': { fill: background, stroke: border},
                  image: { 'xlink:href': '/assets/thisapp/images/orgchart/' + image },
                '.rank': { text: rank }, '.name': { text: name},
                a: { 'xlink:href': "#", cursor: 'pointer' }
            }
        });
        graph.addCell(cell);
        return cell;
    };

    function link(source, target, breakpoints) {

        var cell = new joint.shapes.org.Arrow({
            source: { id: source.id },
            target: { id: target.id },
            vertices: breakpoints
        });
        graph.addCell(cell);
        return cell;
    }

    var oln = member(350,5,'', 'OLN', 'member1.png', '#F1C40F', '#000');
    var richard0 = member(5,150,'Partner \n Executive Committee ', 'Richard Healy', 'rmh.png', '#F1C40F', '#000');
    var gordon0 = member(350,150,'Senior Partner \n Executive Committee', 'Gordon Oldham', 'gdo.png', '#F1C40F', '#000','gdo');
    var stephen0 = member(700,150,'Partner \n Executive Committee', 'Stephen Peaker', 'sjp-1.png', '#F1C40F', '#000');

    link(oln, gordon0, [{x: 450, y: 320}]);
    link(oln, richard0, [{x: 450, y: 110}, {x: 100, y: 110}]);
    link(richard0, oln, [{x: 100, y: 280}, {x: 450, y: 280}]);
    link(oln, stephen0, [{x: 450, y: 110}, {x: 820, y: 110}]);
    link(stephen0, oln, [{x: 820, y: 280}, {x: 450, y: 280}]);

   
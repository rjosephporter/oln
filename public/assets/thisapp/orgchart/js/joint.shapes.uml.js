/*! JointJS v0.9.2 - JavaScript diagramming library  2014-09-17 


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
if (typeof exports === 'object') {

    var joint = {
        util: require('../src/core').util,
        shapes: {},
        dia: {
            Element: require('../src/joint.dia.element').Element,
            Link: require('../src/joint.dia.link').Link
        }
    };
}

joint.shapes.org = {};

joint.shapes.org.Member = joint.dia.Element.extend({

    markup: '<a><g class="rotatable"><g class="scalable"><rect class="card"/><image/></g><text class="rank"/><text class="name"/></g></a>',
    defaults: joint.util.deepSupplement({

        type: 'org.Member',
        size: { width: 200, height: 70 },
        attrs: {

            rect: { width: 170, height: 60 },

            '.card': {
                fill: '#FFFFFF', stroke: '#000000', 'stroke-width': 2,
                'pointer-events': 'visiblePainted', rx: 10, ry: 10
            },

         image: {
            width: 48, height: 48,
                ref: '.card', 'ref-x': 8, 'ref-y': 5
            },
            
             '.rank': {
                ref: '.card', 'ref-x': 0.96, 'ref-y': 0.4,
                'font-weight': 'bold',
                'font-family': 'arial', 'font-size': 12,
                'text-anchor': 'end'
            },
           
            '.name': {
                'font-weight': 'bold',
                ref: '.card', 'ref-x': 0.96, 'ref-y': 0.2,
                'font-family': 'Arial', 'font-size': 12,
                'text-anchor': 'end'
            }
        }
    }, joint.dia.Element.prototype.defaults)
});

joint.shapes.org.Arrow = joint.dia.Link.extend({

    defaults: {
        type: 'org.Arrow',
        source: { selector: '.card' }, target: { selector: '.card' },
        attrs: { '.connection': { stroke: '#585858', 'stroke-width': 3 }},
        z: -1
    }
});


if (typeof exports === 'object') {

    module.exports = joint.shapes.org;
}
var invoice_type = ['invoice'];
var table = {};
var filter = {};
	filter.invoice = {};

/* Date Range Picker - Ping*/
$( "#from_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
  }
});
/* /Date Range Picker */

/* Date Range Form Action */
$('#daterange-form').submit(function(e) {
	e.preventDefault();
	//console.log($('#from_date').val());
	//console.log($('#to_date').val());
	getCustomFilters('invoice');
	table['invoice']._fnDraw();
	table['invoice'].fnPageChange(1);
});
/* /Date Range Form Action */

/* Unpaid Days Disability */
/*
$('#invoice_payment_status').change(function() {
	if($(this).val() == 0 || $(this).val() == 1) {
		$('#invoice_unpaid_days').removeAttr('disabled');
	} else {
		$('#invoice_unpaid_days').attr('disabled', 'disabled').val('-1');
	}
});
*/
/* /Unpaid Days Disability */

var current_date = new Date();
$('#payment_date').datepicker({
	dateFormat: "MM d, yy"
});
$('#payment_date').datepicker('setDate', current_date);

$.each(invoice_type, function(ndx, type) {
	getCustomFilters(type);
	table[type] = $('#'+type+'-wrapper').find('table').dataTable(getTableOptions(type));
});

function getTableOptions(type) {
	var options = {};
	options.sPaginationType = 'simple_numbers';
	options.bProcessing = true;
	options.lengthMenu = [[12,24,36,-1],["12","24","36","All"]];
	options.sAjaxSource = base_url + "/accounting/dt-"+type;
	options.bServerSide = true;
	options.columnDefs = getColumnDefs(type);
	options.oLanguage = { sProcessing: global_ajax_loader_bar };

	switch(type) {
		case 'invoice':
			options.fnServerParams = function(aoData) {
				aoData.push( { "name" : "client", "value" : filter[type]['client_id'] } );
				aoData.push( { "name" : "from_date", "value" : filter[type]['from_date'] } );
				aoData.push( { "name" : "to_date", "value" : filter[type]['to_date'] } );
				aoData.push( { "name" : "invoice_payment", "value" : filter[type]['invoice_payment'] } );
				aoData.push( { "name" : "invoice_payment_status", "value" : filter[type]['invoice_payment_status'] } );
				aoData.push( { "name" : "invoice_unpaid_days", "value" : filter[type]['invoice_unpaid_days'] } );
			}
			options.order = [[6, 'asc']];
			
			options.fnRowCallback = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				console.log(aData);
				$(nRow).addClass(aData[14]+'-background');
			};

			options.fnDrawCallback = function(settings) {
				$('.btn-flag-reminder').tooltip();
			};
			
			break;

		case 'invoicedetail':
			options.fnServerParams = function(aoData) {
				aoData.push( { "name" : "invoice_id", "value" : filter[type]['invoice_id'] } );
			}
			options.order = [[1, 'asc']];
			options.iDisplayLength = -1;
			options.bLengthChange = false;
			options.bPaginate = false;
			options.bInfo = false;
			break;
	}

	return options;
}

function getCustomFilters(type) {

    switch(type) {
        case 'invoice':
            filter.invoice.client_id = 'all';
            filter.invoice.from_date = $('#from_date').val();
            filter.invoice.to_date = $('#to_date').val();
            filter.invoice.invoice_payment = 1;
            filter.invoice.invoice_payment_status = $('#invoice_payment_status').val();
            filter.invoice.invoice_unpaid_days = $('#invoice_unpaid_days').val()
            break;

        case 'invoicedetail':
            filter.invoicedetail.invoice_id = 'all';
            break;
    }

}

function getColumnDefs(type) {
	var result = [];
	switch(type) {
		case 'invoice':
			result.push({
				//'targets': [0,1,6,8,9,10,12,13],
				'targets': [2,3,8,10,11,12,14,15],
				'visible': false,
				'searchable': false
			});

			result.push({
				'targets': [6],
				'render': function(data,type,row) { 
	                return moment(data).format('MMMM D, YYYY');
	            }
			});

            result.push({
	            'targets': [9,10,13],
	            'render': function(data,type,row) { 
	            	if(data == null) data = '0.0000';
	                return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
	            }
            });	

            result.push({
            	'targets': [11],
            	'className': 'text-center',
            	'render': function(data,type,row) { 
            		//console.log(row);
            		var status = {
            			0 : 'Unpaid',
            			1 : 'Paid with balance',
            			2 : 'Paid'
            		};

            		status = {
            			0 : '<i class="fa fa-lg fa-ban text-danger"></i>',
            			1 : '<i class="fa fa-lg fa-tag text-info"></i>',
            			2 : '<i class="fa fa-lg fa-check-circle text-success"></i>'            			
            		};

            		var flag_unpaid_reminder = '';
            		if(row[12] == 1) flag_unpaid_reminder = '<i class="fa fa-flag text-warning"></i>';

	                return (status[data] + ' ' + flag_unpaid_reminder); 
	            }
            });

            result.push({
            	'targets': [16],
            	'className': 'text-center',
            	'render': function(data,type,row) {
            		var text = (data == 'IN') ? 'Intermediary' : 'Final';
            		return '<span class="label label-default label-invoice-' + data + '">' + text + '</span>'
            	}
            });

            result.push({
                'targets': [18],
                'className': 'text-center',
                'render': function(data, type, row) {                   
                    var delete_disabled = '';
                    if(row[11] == 1) delete_disabled = 'disabled';

                    if(row[11] == 0 || row[11] == 1) {
                    	var action_buttons = '<button class="btn btn-success btn-xs btn-flag-pay" invoice-number="'+row[2]+'" batch-number="'+row[3]+'" balance="'+row[10]+'" data-toggle="modal" data-target="#invoice-pay-modal" title="Input Payment"><i class="fa fa-check"></i></button> ' +
                            				 '<button class="btn btn-danger btn-xs btn-flag-delete" invoice-number="'+row[2]+'" batch-number="'+row[3]+'" ' + delete_disabled + ' data-toggle="modal" data-target="#invoice-delete-modal" title="Delete Invoice"><i class="glyphicon glyphicon-trash"></i></button> ' + 
                            				 '<a href="/invoice/print/'+row[2]+'" target="_blank" class="btn btn-info btn-xs" title="View Invoice"><i class="fa fa-list"></i></a> ' + 
                            				 '<a href="/timesheet/new?matter_id='+row[17]+'" target="_blank" class="btn btn-info btn-xs" title="View Timesheet"><i class="glyphicon glyphicon-time"></i></a>';
                        /*
                      	if(row[12] == 'yellow' || row[12] == 'orange') {
                      		action_buttons = action_buttons + '<button class="btn btn-warning btn-xs btn-flag-reminder" invoice-number="'+row[0]+'" batch-number="'+row[1]+'" client-id="'+row[2]+'" balance="'+row[8]+'" data-toggle="modal" data-target="#invoice-reminder-modal"><i class="fa fa-flag"></i></button>';
                      	}
                      	*/
                        return action_buttons;
                    } else {
                        return '<i class="glyphicon glyphicon-lock"></i>';
                    }
                }
            });

			result.push({
				'targets': [19],
				'className': 'text-center',
				'render': function(data, type, row) {

					var disable_reminder = {
						first : 'faded',
						second : 'faded',
						third : 'faded'
					}

					switch(row[14]) {
						case 'yellow':
							disable_reminder.first = 'btn-active';
							break;
						case 'orange':
							disable_reminder.first += ' btn-circle';
							disable_reminder.second = 'btn-active';
							break;							
						case 'red':
							disable_reminder.first += ' btn-circle';
							disable_reminder.second += ' btn-circle';
							disable_reminder.third = 'btn-active';
							break;							
					}

					var reminder_log = (row[15] !== null) ? row[15].split(',') : [];


					reminder_log[0] = (typeof reminder_log[0] === 'undefined') ? '1st reminder not sent'  : 'Sent: ' + moment(reminder_log[0]).format('MMM DD, YYYY hh:mm A');
					reminder_log[1] = (typeof reminder_log[1] === 'undefined') ? '2nd reminder not sent'  : 'Sent: ' + moment(reminder_log[1]).format('MMM DD, YYYY hh:mm A');
					reminder_log[2] = (typeof reminder_log[2] === 'undefined') ? 'Last reminder not sent' : 'Sent: ' + moment(reminder_log[2]).format('MMM DD, YYYY hh:mm A');
					/*
					var reminder_buttons = '<button class="btn btn-warning btn-xs btn-flag-reminder '+disable_reminder.first+'"  invoice-number="'+row[0]+'" batch-number="'+row[1]+'" client-id="'+row[2]+'" balance="'+row[8]+'" title="'+reminder_log[0]+'" btn-number="1">1</button> ' +
                            			   '<button class="btn btn-danger  btn-xs btn-flag-reminder '+disable_reminder.second+'" invoice-number="'+row[0]+'" batch-number="'+row[1]+'" client-id="'+row[2]+'" balance="'+row[8]+'" title="'+reminder_log[1]+'" btn-number="2">2</button> ' + 
                            			   '<button class="btn btn-primary btn-xs btn-flag-reminder '+disable_reminder.third+'"  invoice-number="'+row[0]+'" batch-number="'+row[1]+'" client-id="'+row[2]+'" balance="'+row[8]+'" title="'+reminder_log[2]+'" btn-number="3">3</button> ';
					*/
					var reminder_buttons = '<a href="/accounting/send-reminder?invoice_number='+row[2]+'&batch_number='+row[3]+'&client_id='+row[4]+'&balance='+row[10]+'&reminder_number=1" target="_blank" class="btn btn-warning btn-xs btn-flag-reminder '+disable_reminder.first+'"  title="'+reminder_log[0]+'" btn-number="1">1</a> ' +
                            			   '<a href="/accounting/send-reminder?invoice_number='+row[2]+'&batch_number='+row[3]+'&client_id='+row[4]+'&balance='+row[10]+'&reminder_number=2" target="_blank" class="btn btn-danger  btn-xs btn-flag-reminder '+disable_reminder.second+'" title="'+reminder_log[1]+'" btn-number="2">2</a> ' + 
                            			   '<a href="/accounting/send-reminder?invoice_number='+row[2]+'&batch_number='+row[3]+'&client_id='+row[4]+'&balance='+row[10]+'&reminder_number=3" target="_blank" class="btn btn-primary btn-xs btn-flag-reminder '+disable_reminder.third+'"  title="'+reminder_log[2]+'" btn-number="3">3</a> ';                            			   

                    return reminder_buttons;
				}
			});

			break;

		case 'invoicedetail':
			result.push({
				'targets': [0],
				'visible': false,
				'searchable': false
			});
			
            result.push({
	            'targets': [3],
	            'render': function(data,type,row) { 
	                return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
	            }
            });	
            	
			break;
	}

	return result;
}

/* Action - Pay */
var current_row;
$(document).on('click', '.btn-flag-pay', function() {
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	var balance = $(this).attr('balance');

	//console.log('Row number: ' + current_row);

	$('#payment_date').datepicker('setDate', new Date());

	$('#invoice-pay-batch-number').text(invoice_number);
	$('#payment_batch_number').val(batch_number);
	$('#payment_amount').val(balance);
});

$('#invoice-pay-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();
	//console.log(post_data);
	
	$.post('/accounting/pay-invoice', post_data, function(data) {
		//console.log(data);

		var row_data = table.invoice.DataTable().row(current_row).data();

		//console.log(row_data);

		row_data[10] = parseFloat(data.result.balance);
		row_data[11] = parseInt(data.result.status_id);
		if(parseFloat(data.result.balance) == 0) row_data[12] = 0;
		row_data[13] = parseFloat(row_data[13]) + parseFloat(data.payment_amount);
		if(parseInt(data.result.status_id) == 2) {
			row_data[14] = 'white';
			$(table.invoice.DataTable().row(current_row).node())
				.removeClass('white-background green-background yellow-background orange-background red-background')
				.addClass('white-background');
		}

		console.log(row_data);

		table.invoice.DataTable().row(current_row).data(row_data);

		$('#invoice-pay-modal').modal('hide');
	});
});

/* Action - Delete */
$(document).on('click', '.btn-flag-delete', function() {
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	$('#invoice-delete-batch-number').text(invoice_number);
	$('#delete_batch_number').val(batch_number);
	$('#delete_reason').val('');
});

$('#invoice-delete-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();

	$.post('/accounting/delete-invoice', post_data, function(data) {
		if(data.result !== 0) {
			var current_page = table.invoice.DataTable().page.info().page;
			table.invoice.DataTable().row(current_row).remove().draw();
			table.invoice.fnPageChange(current_page);
			$('#invoice-delete-modal').modal('hide');
		}		
	});
});

/* Reminder */
$(document).on('click', '.btn-flag-reminder', function(e) {
	e.preventDefault();
	if($(this).hasClass('faded')) return false;
	
	$('#reminder_recipient').attr('readonly', true);

	window.location = $(this).attr('href');

});
/*
$(document).on('click', '.btn-flag-reminder', function() {
	
	if($(this).hasClass('faded')) return false;
	
	$('#invoice-reminder-form').addClass('invisible');
	$('#reminder_recipient').attr('readonly', true);
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	var client_id = $(this).attr('client-id');
	var balance = $(this).attr('balance');
	var reminder_number = $(this).attr('btn-number');
	$('#invoice-reminder-batch-number').text(invoice_number);
	$('#reminder_batch_number').val(batch_number);

	$.get('/accounting/reminder-info', { client_id : client_id, batch_number : batch_number, balance : balance, reminder_number : reminder_number }, function(data) {
		console.log(data);
		$('#reminder_recipient_email').val(data.email_address);
		if($('#reminder_recipient_email').val() !== '')
			$('#reminder_recipient').val(data.email_address);
		else 
			$('#reminder_recipient').attr('readonly', false).val('');
		$('#reminder_message').val(data.message);

		$('#invoice-reminder-form').removeClass('invisible');
	});

	$('#invoice-reminder-modal').modal('show');

});

$('#invoice-reminder-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();

	$.post('/accounting/send-reminder', post_data, function(data) {
		console.log(data);

		var add_class;
		var row_data = table.invoice.DataTable().row(current_row).data();
		if(data.reminder_count == 1) {
			add_class = 'orange-background';
			row_data[12] = 'orange';
		} else {
			add_class = 'red-background';
			row_data[12] = 'red';
		}

		if(row_data[13] !== null) {
			row_data[13] = row_data[13] + ',' + data.log_data.date_created
		} else {
			row_data[13] = data.log_data.date_created;
		}

		$(table.invoice.DataTable().row(current_row).node())
			.removeClass('white-background green-background yellow-background orange-background red-background')
			.addClass(add_class);

		table.invoice.DataTable().row(current_row).data(row_data);

		$('.btn-flag-reminder').tooltip();

		$('#invoice-reminder-modal').modal('hide');
	});
});
*/
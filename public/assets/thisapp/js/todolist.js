$(document).ready(function() {
        var func;
        var currentSelection = 0;
        var $sharees, $owners, $shareesresponse,$shareesdecline,$shareesrequesthelp, $invoker, $shareesmessage, $ownerdelegate;
//        var fileinput_opts = {
//                    image: {width: "auto", height: "100px"},
//                    html: {width: "160px", height: "90px"},
//                    text: {width: "80px", height: "80px"},
//                    video: {width: "100px", height: "100px"},
//                    audio: {width: "160px", height: "40px"},
//                    flash: {width: "100px", height: "100px"},
//                    object: {width: "100px", height: "100px"},
//                    other: {width: "80px", height: "60px"}
//                };

        $('#taskwizard').on({
            'show.bs.modal': function(){
                tinymce.init({
                    selector: '.formcreatetask textarea',
                    theme: "modern",
                    height: "300",
                    mode: "textarea",
                    plugins: ["lists link table jbimages doksoft_file legacyoutput"],
                    toolbar: "undo redo | bold italic | bullist numlist outdent indent | doksoft_file | jbimages",
                    relative_urls: false,
                    remove_script_host : false,
                    convert_urls : true,
                    setup: function(editor) {
                                editor.on('keyup', function(e) {
                                    // Revalidate the hobbies field
                                    $('.formcreatetask').bootstrapValidator('revalidateField', 'message');
                                });
                            }
                });
            },
            'hide.bs.modal': function(){
                tinymce.remove();
            }
        });

        $('#messagewizard').on({
            'show.bs.modal': function(){
                tinymce.init({
                    selector: '.formnewmessage textarea',
                    theme: "modern",
                    height: "200",
                    mode: "textarea",
                    plugins: ["lists link table jbimages doksoft_file legacyoutput"],
                    toolbar: "undo redo | bold italic | bullist numlist outdent indent | doksoft_file | jbimages",
                    relative_urls: false,
                    remove_script_host : false,
                    convert_urls : true,
                    setup: function(editor) {
                                editor.on('keyup', function(e) {
                                    // Revalidate the hobbies field
                                    $('.formnewmessage').bootstrapValidator('revalidateField', 'message');
                                });
                            }
                });
            },
            'hide.bs.modal': function(){
                tinymce.remove();
            }
        });

        $('#replywizard').on({
            'show.bs.modal': function(){
                tinymce.init({
                    selector: '.formcreatemessage textarea',
                    theme: "modern",
                    height: "200",
                    mode: "textarea",
                    plugins: ["lists link table jbimages doksoft_file legacyoutput"],
                    toolbar: "undo redo | bold italic | bullist numlist outdent indent | doksoft_file | jbimages",
                    relative_urls: false,
                    remove_script_host : false,
                    convert_urls : true,
                    setup: function(editor) {
                                editor.on('keyup', function(e) {
                                    // Revalidate the hobbies field
                                    $('.formcreatemessage').bootstrapValidator('revalidateField', 'message');
                                });
                            }
                });
            },
            'hide.bs.modal': function(){
                tinymce.remove();
            }
        });

        $('#declinewizard').on({
            'show.bs.modal': function(){
                tinymce.init({
                    selector: '.formdeclinetask textarea',
                    theme: "modern",
                    height: "200",
                    mode: "textarea",
                    plugins: ["lists link table legacyoutput"],
                    toolbar: "undo redo | bold italic | bullist numlist outdent indent ",
                    relative_urls: false,
                    remove_script_host : false,
                    convert_urls : true,
                    setup: function(editor) {
                            editor.on('keyup', function(e) {
                                // Revalidate the hobbies field
                                $('.formcreatereply').bootstrapValidator('revalidateField', 'message');
                            });
                        }
                });
            },
            'hide.bs.modal': function(){
                tinymce.remove();
            }
        });

        $('#finishwizard').on({
            'shown.bs.modal': function(){
                tinymce.init({
                    selector: '.formcreatereply textarea',
                    theme: "modern",
                    height: "200",
                    mode: "textarea",
                    plugins: ["lists link table jbimages doksoft_file legacyoutput"],
                    toolbar: "undo redo | bold italic | bullist numlist outdent indent | doksoft_file | jbimages",
                    relative_urls: false,
                    remove_script_host : false,
                    convert_urls : true,
                    setup: function(editor) {
                            editor.on('keyup', function(e) {
                                // Revalidate the hobbies field
                                $('.formcreatereply').bootstrapValidator('revalidateField', 'message');
                            });
                        }
                });
            },
            'hidden.bs.modal': function(){
                tinymce.remove();
            }
        });


        $(this).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });

        $( window ).resize(function() {
            $('.todolist').css('height', $(window).height() * 0.60);
        });

        $('.todolist .task-list').on('click','.task',function(e){
                $(this).parents('li').siblings('li').removeClass('task-list-focus');
                $(this).parents('li').addClass('task-list-focus');
                /// append task
                $('#task-detail').empty();
                var data_string = $(this).parents('li').attr('data-data');
                var data_obj = jQuery.parseJSON(data_string);
                var $task = $($('#default-task').html()).appendTo('#task-detail');
                $task.find('span.task-requested-by').text(data_obj.requestedby_name);
                $task.find('span.task-to').text(data_obj.owner_name);

                var t = data_obj.requestedon.split(/[- :]/);
                var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                $task.find('span.task-date').text(d.toDateString());

                var rt = data_obj.requestedon.split(/[- :]/);
                var rd = new Date(rt[0], t[1]-1, t[2], t[3], t[4], t[5]);
                $task.find('span.task-requiredby-date').text(rd.toDateString());

                var message = $.parseJSON(data_obj.message);
                if(message.subject){
                    $task.find('span.task-subject').text(message.subject);
                }

                if(data_obj.task_conversations){
                    $task.find('ul.task-replies').show();
                    var replies = $.parseJSON(data_obj.task_conversations);
                    var current_hash;
                    $.each(replies,function(key,value){
                        if(current_hash != value.hash){
                            var $reply = $($('#default-task-reply').html());
                            $reply.find('.reply-header').append('Reply from '+value.owner+' on '+value.datecreated+' :</b></small>');
                            $reply.find('.reply-message').append(value.conversation.message);
                            $reply.appendTo($task.find('ul.task-replies'));
                            current_hash = value.hash;
                        }
                    });
                    // $($task.find('ul.task-replies li:nth-child(even)')).addClass('timeline-inverted');
                    // $task.find('div.task-message').append('<hr class="dark-hr"><small><b>Original task from '+data_obj.requestedby_name+'</b></small>');
                } else {
                    $task.find('ul.task-replies').hide();
                }

                if(data_obj.request_final_replies){
                    if(func == "my-requests"){
                        var final_replies = $.parseJSON(data_obj.request_final_replies);
                        $.each(final_replies,function(key,value){
                                var $final_reply = $($('#default-task-reply').html());
                                $final_reply.find('.reply-header').append('Final reply from '+value.owner+' on '+value.donedate+' :</b></small>');
                                $final_reply.find('.reply-message').append(value.final_reply.message);
                                $final_reply.prependTo($task.find('ul.task-replies'));
                                $final_reply.find('.timeline-panel').addClass('timeline-final');
                        });
                        
                    } else if(func == "my-finished-requests"){
                        var final_replies = $.parseJSON(data_obj.request_final_replies);
                        $.each(final_replies,function(key,value){
                            $task.find('div.task-reply-final').append('<small><b>Final reply from '+value.owner+' on '+value.donedate+':</b></small>'+value.final_reply.message);
                            $task.find('div.task-reply-final').append('<br">');
                        });
                        $task.find('div.task-reply-final').show();
                    }
                }
                $($task.find('ul.task-replies li:nth-child(even)')).addClass('timeline-inverted');

                if(data_obj.response){
                    $task.find('div.task-reply-final').show();
                    var response = $.parseJSON(data_obj.response);
                    $task.find('div.task-reply-final').append('<small><b>Final reply from '+data_obj.owner_name+' :</b></small>'+response.message);
                } else {
                    if(data_obj.request_final_replies == null){
                        $task.find('div.task-reply-final').hide();
                    }
                }

                if(message.message){
                    $task.find('div.task-message').append('<small><b>Original task from '+data_obj.requestedby_name+' :</b></small>'+message.message);
                }

                if(func == 'my-requests' || func == 'my-finished-requests'){
                    $task.find('img.requested-by-photo').hide();
                    $task.find('img.owner-photo').show();
                    $task.find('img.owner-photo').attr('src','/assets/thisapp/images/staff/'+data_obj.owner_photo);
                } else {
                    $task.find('img.owner-photo').hide();
                    $task.find('img.requested-by-photo').show();
                    $task.find('img.requested-by-photo').attr('src','/assets/thisapp/images/staff/'+data_obj.requestedby_photo);
                }

                if(func == 'my-requests' || func == 'my-finished-requests'){
                    // $('.task-controls').find('button,a').hide();
                    // $('.task-request-controls').find('button,a').show();
                    // $('.task-request-controls').find('button,a').attr('disabled',false);
                } else {
                    // $('.task-request-controls').find('button,a').hide();
                    // $('.task-controls').find('button,a').show();
                    // $('.task-controls').find('button,a').attr('disabled',false);
                    // $('.forcefinish').attr('href','/commcenter/forcefinishtask?tkid='+data_obj.hash+'&mtd=web');
                    // $('.accept').attr('href','/commcenter/accepttask?tkid='+data_obj.hash);
                    if(data_obj.type == "6"){
                       // $('.approve').attr('disabled',false);
                       // $('.approve').attr('href','/commcenter/approverequest?tkid='+data_obj.hash);
                    } else {
                        // $('.approve').attr('disabled',true);
                    }
                }

                $task.find('span.status').addClass('glyphicon-exclamation-sign '+setTaskColor(data_obj.status, data_obj.requiredbydate));

                $('#delegatewizard #formdelegatetask input[name=taskid]').val(data_obj.id);
                $('#replywizard #formcreatemessage input[name=taskid]').val(data_obj.id);
                $('#declinewizard #formdeclinetask input[name=taskid]').val(data_obj.id);
                $('#finishwizard #formcreatereply input[name=taskid]').val(data_obj.id);
                $('#replywizard #formcreatemessage input[name=subject]').val(message.subject);
                $('#finishwizard #formcreatereply input[name=subject]').val(message.subject);

                // for requests only
                $('#messagewizard #formnewmessage input[name=requestid]').val(data_obj.id);
                $('#messagewizard #formnewmessage input[name=subject]').val(message.subject);
                currentSelection = $(this).parents('li').index();
                
                switch (func){
                    case 'my-tasks':
                        $task.find('.task-controls').show();
                        $task.find('.task-request-controls').hide();
                        $('.task-controls').find('button,a').attr('disabled',false);
                        $('.forcefinish').attr('href','/commcenter/forcefinishtask?tkid='+data_obj.hash);
                        $('.accept').attr('href','/commcenter/accepttask?tkid='+data_obj.hash);
                        if(data_obj.type == "6"){
                            $('.approve').attr('disabled',false);
                            $('.approve').attr('href','/commcenter/approverequest?tkid='+data_obj.hash);
                        } else {
                            $('.approve').attr('disabled',true);
                        }
                        break;
                    case 'my-requests':
                        $task.find('.task-controls').hide();
                        $task.find('.task-request-controls').show();
                        $('.task-request-controls').find('button,a').attr('disabled',false);
                        break;
                    case 'my-finished-tasks':
                        $task.find('.task-controls').hide();
                        $task.find('.task-request-controls').hide();
                        break;
                    case 'my-finished-requests':
                        $task.find('.task-controls').hide();
                        $task.find('.task-request-controls').hide();
                        break;
                }
            });

        $('.todolist .task-list').keypress(function(e) {
          switch(e.keyCode) {
             // User pressed "up" arrow
             case 38:
                $(this).find('li.task-list-focus').prev('li').find('.task').trigger('click');
             break;
             // User pressed "down" arrow
             case 40:
                $(this).find('li.task-list-focus').next('li').find('.task').trigger('click');
             break;
          }
        });

        $('.todolist .sidebar-nav').on('click','li',function(e){
            func = $(this).attr('id');
            $('#task-detail').empty();
            $('.todolist .task-list').find('ul').empty();
            var list = $('.todolist .task-list').find('ul');
            $(this).siblings('li').removeClass('active');
            $(this).addClass('active');
            var task_list;
            switch (func){
                case 'my-tasks':
                    task_list = userdata['tasks']['pending'];
                    $('.task-sorter').find('#sort-from-ascending').text('From (A to Z)');
                    $('.task-sorter').find('#sort-from-descending').text('From (Z to A)');
                    $('.task-controls').show();
                    $('.task-controls').find('button,a').attr('disabled',true);
                    $('.task-request-controls').hide();
                    break;
                case 'my-requests':
                    task_list = userdata['requests']['pending'];
                    $('.task-sorter').find('#sort-from-ascending').text('Requested To (A to Z)');
                    $('.task-sorter').find('#sort-from-descending').text('Requested To (Z to A)');
                    $('.task-controls').hide();
                    $('.task-request-controls').show();
                    $('.task-request-controls').find('button,a').attr('disabled',true);
                    break;
                case 'my-finished-tasks':
                    task_list = userdata['tasks']['finished'];
                    $('.task-sorter').find('#sort-from-ascending').text('From (A to Z)');
                    $('.task-sorter').find('#sort-from-descending').text('From (Z to A)');
                    $('.task-controls').hide();
                    $('.task-request-controls').hide();
                    break;
                case 'my-finished-requests':
                    task_list = userdata['requests']['finished'];
                    $('.task-sorter').find('#sort-from-ascending').text('Requested To (A to Z)');
                    $('.task-sorter').find('#sort-from-descending').text('Requested To (Z to A)');
                    $('.task-controls').hide();
                    $('.task-request-controls').hide();
                    break;
            }
            console.log(task_list);
            $.each(task_list, function() {
                var $new_list_item = $($('#default-list-item').html()).appendTo(list);
                $new_list_item.attr('data-id',this.id);
                $new_list_item.attr('data-data',JSON.stringify(this));
                
                if(func == 'my-requests' || func == 'my-finished-requests'){
                    $new_list_item.find('span.task-requested-by').text(this.owner_name);
                } else {
                    $new_list_item.find('span.task-requested-by').text(this.requestedby_name);
                }
                
                var t = this.requestedon.split(/[- :]/);
                var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
                $new_list_item.find('span.task-date').text(d.toDateString());
                $new_list_item.find('span.task-date').attr('data-timestamp',d.getTime());
//                console.log(this.message);
                var message = $.parseJSON(this.message);
                if(message.subject){
                   $new_list_item.find('span.task-subject').text(message.subject);
                }
                if(message.message){
                   // $new_list_item.find('div.task-message').text(message.subject);
                   var msg = $(message.message).text();
                   $new_list_item.find('div.task-message').text(msg);
                }
                $new_list_item.find('span.status').addClass('glyphicon-exclamation-sign '+setTaskColor(this.status, this.requiredbydate));
            });
            $('.task-list div.task-message').truncate({lines:2,lineHeight:20});
            $('.task-sorter a#sort-date-descending').trigger('click');

//            if(func == 'my-requests'){
//                 $('.task-controls').hide();
//                 $('.task-request-controls').show();
//                 if($('.todolist .task-list').has('li.task-list-focus').length == 0){
//                    $('.task-request-controls').find('button,a').attr('disabled',true);
//                 }
//            } else if(func == 'my-finished-requests'){
//                $('.task-request-controls').hide();
//            } else {
//                 $('.task-request-controls').hide();
//                 if($('.todolist .task-list').has('li.task-list-focus').length == 0){
//                    $('.task-controls').find('button,a').attr('disabled',true);
//                 }
//            }

             $('.todolist .task-list').find('input').closest('div.search').remove();

             if($('.todolist .task-list>ul').length){
                $('.todolist .task-list>ul').sieve({itemSelector: "li"});
                $('.todolist .task-list').find('input').closest('div').addClass('search');
             }
        });

        $('.task-sorter').on('click','a',function(e){
            e.preventDefault();
            var sortfunc = $(this).attr('id');
            switch (sortfunc){
                case 'sort-date-descending':
                    $('.task-list>ul>li').tsort('span.task-date',{data:'timestamp', order:'desc'});
                    $(this).parents('.task-sorter').find('a span.glyphicon.glyphicon-ok').remove();
                    $(this).prepend('<span class="glyphicon glyphicon-ok"></apan>');
                    $(this).parents('.btn-group').find('.sort-button').text('Sort by Date');
                    break;
                case 'sort-date-ascending':
                    $('.task-list>ul>li').tsort('span.task-date',{data:'timestamp',order:'asc'});
                    $(this).parents('.task-sorter').find('a span.glyphicon.glyphicon-ok').remove();
                    $(this).prepend('<span class="glyphicon glyphicon-ok"></apan>');
                    $(this).parents('.btn-group').find('.sort-button').text('Sort by Date');
                    break;
                case 'sort-from-descending':
                    $('.task-list>ul>li').tsort('span.task-requested-by',{order:'desc'});
                    $(this).parents('.task-sorter').find('a span.glyphicon.glyphicon-ok').remove();
                    $(this).prepend('<span class="glyphicon glyphicon-ok"></apan>');
                    if(func == 'my-requests' || func == 'my-finished-requests'){
                        $(this).parents('.btn-group').find('.sort-button').text('Sort by Requested To');
                    } else {
                        $(this).parents('.btn-group').find('.sort-button').text('Sort by From');
                    }
                    break;
                case 'sort-from-ascending':
                    $('.task-list>ul>li').tsort('span.task-requested-by',{order:'asc'});
                    $(this).parents('.task-sorter').find('a span.glyphicon.glyphicon-ok').remove();
                    $(this).prepend('<span class="glyphicon glyphicon-ok"></apan>');
                    if(func == 'my-requests' || func == 'my-finished-requests'){
                        $(this).parents('.btn-group').find('.sort-button').text('Sort by Requested To');
                    } else {
                        $(this).parents('.btn-group').find('.sort-button').text('Sort by From');
                    }
                    break;
            }
        });

        $(this).on("click", ".task-img", function(e){
            e.preventDefault();
            var src = $(this).attr('src');
            $('.task-img-blowup').attr('src',src);
            $('.task-img-blowup').show(300);
            scrollToElement('.task-img-blowup',null,null,'#task-detail');
        });

        $(this).on('click','.forcefinish',function(){
            $('#processing').modal('show');
        });

        $('.formcreatetask').each(function(){
            var original = this;
            $sharees = $(this).find("select[id=sharees]").selectize({
                    dataAttr: 'data-data',
                    valueField: 'uid',
                    plugins: ['remove_button'],
                    render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row" style="width:100%">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div class="col-xs-8"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        }
                    }
            });

            $owners = $(this).find("select[id=owners]").selectize({
                    dataAttr: 'data-data',
                    valueField: 'uid',
                    plugins: ['remove_button'],
                    render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div><div class="clearfix"></div>' +
                            '</div>';
                        }
                    },
                    onChange: function(value) {
                        $('.formcreatetask').bootstrapValidator('revalidateField', 'owners[]');
                    }
            });

            $(original).find("#clear-all-sharees").bind('click', function(e){
                e.preventDefault();
                $sharees[0].selectize.setValue();
            });

            $(original).find("#select-all-sharees").bind('click', function(e){
                e.preventDefault();
                $sharees[0].selectize.setValue(userids);
            });

            $(original).find("#clear-all-owners").bind('click', function(e){
                e.preventDefault();
                $owners[0].selectize.setValue();
            });

            $(original).find("#select-all-owners").bind('click', function(e){
                e.preventDefault();
                $owners[0].selectize.setValue(userids);
            });

            $(this).find('input[id=requiredbydate]').datepicker({
                dateFormat:'yy-mm-dd'
            });

//            $(this).find('#clonefileupload').click(function(e){
//                e.preventDefault;
//                var num = $(original).find('.fileinput').length,
//                newNum  = new Number(num + 1),
//                newElem = $(original).find('#fileinput' + num).clone().attr('id', 'fileinput' + newNum).fadeIn('slow');
//                $(original).find('#fileinput' + num).after(newElem);
//                $(original).find('#fileinput' + newNum +' select[id=fileupload]').val();
//                $(original).find('#removeclonefileupload').attr('disabled', false);
//            });
//
//            $(this).find('#removeclonefileupload').click(function(e){
//                e.preventDefault;
//                var num = $(original).find('.fileinput').length;
//                $(original).find('#fileinput' + num).slideUp('slow', function(){
//                    $(this).remove();
//                        if (num -1 === 1){
//                            $(original).find('#removeclonefileupload').attr('disabled', true);
//                        }
//                    });
//            });

//            $(this).find('input[name="fileupload[]"]').fileinput({
//                showUpload: false,
//                maxFileSize: 4000,
//                previewSettings: fileinput_opts
//            });

       });

        $('.formcreatetask').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'owners[]': {
                    message: 'No person assigned to',
                    validators: {
                        notEmpty: {
                            message: 'A person to assign the task to is required'
                        }
                    }
                },
                'subject': {
                    message: 'Subject is empty',
                    validators: {
                        notEmpty: {
                            message: 'Subject is required and cannot be empty'
                        }
                    }
                },
                'message': {
                    validators: {
                        callback: {
                            message: 'Message must be more than 5 characters',
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinymce.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                },
                'type': {
                    message: 'Type is not set',
                    validators: {
                        notEmpty: {
                            message: 'Type is required'
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#taskwizard').modal('hide');
              $('#processing').modal('show');
        });

        $('.formcreatetask').on('change','input[name=taskorrequest]',function() {
            var taskorrequest = $(this).val();
            if(taskorrequest == 'task'){
                var action = '/commcenter/newowntask';
                $owners[0].selectize.setValue(thisuser);
                $owners[0].selectize.lock();
                $('#select-all-owners').prop('disabled',true);
                $('#clear-all-owners').prop('disabled',true);
            } else if(taskorrequest == 'request'){
                action = '/commcenter/newtaskrequest';
                $owners[0].selectize.setValue();
                $owners[0].selectize.unlock();
                $('#select-all-owners').prop('disabled',false);
                $('#clear-all-owners').prop('disabled',false);
            }
            $('.formcreatetask').attr('action',action);
        });       

        $('.formcreatetask').on('click','.cancel',function() {
            $('.formcreatetask').clearForm();
            $sharees[0].selectize.setValue();
            $owners[0].selectize.setValue();
            $('.formcreatetask').data('bootstrapValidator').resetForm();
//            $('.formcreatetask').find('input[name="fileupload[]"]').fileinput('reset');
            tinyMCE.activeEditor.setContent('');
        });

        $('.formdelegatetask').each(function(){
            var original = this;
            $ownerdelegate = $(this).find("select[id=owner]").selectize({
                dataAttr: 'data-data',
                valueField: 'uid',
                render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div><div class="clearfix"></div>' +
                            '</div>';
                        }
                    }
            });
            $ownerdelegate[0].selectize.setValue();
       });
       $('.formdelegatetask').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'owner': {
                    message: 'No person to delegate to',
                    validators: {
                        notEmpty: {
                            message: 'A person to delegate the task to is required'
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#delegatewizard').modal('hide');
              $('#processing').modal('show');
        });

        $('.formdelegatetask').on('click','.cancel',function() {
            $('.formdelegatetask').clearForm();
            $ownerdelegate[0].selectize.setValue();
            $('.formdelegatetask').data('bootstrapValidator').resetForm();
        });

        $('.formcreatemessage').each(function(){
            var original = this;
            $shareesmessage = $(this).find("select[id=sharees]").selectize({
                    dataAttr: 'data-data',
                    valueField: 'uid',
                    plugins: ['remove_button'],
                    render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div><div class="clearfix"></div>' +
                            '</div>';
                        }
                    }
            });

            $(original).find("#clear-all-sharees").bind('click', function(e){
                e.preventDefault();
                $shareesmessage[0].selectize.setValue();
            });

//            $(this).find('input[name="fileupload[]"]').fileinput({
//                showUpload: false,
//                maxFileSize: 4000,
//                previewSettings: fileinput_opts
//            });
       });

       $('.formcreatemessage').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'subject': {
                    message: 'Subject is empty',
                    validators: {
                        notEmpty: {
                            message: 'Subject is required and cannot be empty'
                        }
                    }
                },
                'message': {
                    validators: {
                        callback: {
                            message: 'Message must be more than 5 characters',
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinymce.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#replywizard').modal('hide');
              $('#processing').modal('show');
        });

        $('.formcreatemessage').on('click','.cancel',function() {
//            $('.formcreatemessage').clearForm();
//            $shareesmessage[0].selectize.setValue();
            $('.formcreatemessage').data('bootstrapValidator').resetForm();
//            $('.formcreatemessage').find('input[name="fileupload[]"]').fileinput('reset');
            tinyMCE.activeEditor.setContent('');
        });


        $('.formnewmessage').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'subject': {
                    message: 'Subject is empty',
                    validators: {
                        notEmpty: {
                            message: 'Subject is required and cannot be empty'
                        }
                    }
                },
                'message': {
                    validators: {
                        callback: {
                            message: 'Message must be more than 5 characters',
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinymce.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#messagewizard').modal('hide');
              $('#processing').modal('show');
        });

        

        $('.formdeclinetask').each(function(){
            var original = this;
            $shareesdecline = $(this).find("select[id=sharees]").selectize({
                    dataAttr: 'data-data',
                    valueField: 'uid',
                    plugins: ['remove_button'],
                    render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div><div class="clearfix"></div>' +
                            '</div>';
                        }
                    }
            });

            $(original).find("#clear-all-sharees").bind('click', function(e){
                e.preventDefault();
                $shareesdecline[0].selectize.setValue();
            });
       });

       $('.formdeclinetask').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'message': {
                    validators: {
                        callback: {
                            message: 'Message must be more than 5 characters',
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinymce.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#declinewizard').modal('hide');
              $('#processing').modal('show');
        });

        $('.formdeclinetask').on('click','.cancel',function() {
            $('.formdeclinetask').clearForm();
            $shareesdecline[0].selectize.setValue();
            $('.formdeclinetask').data('bootstrapValidator').resetForm();
        });

        $('.formcreatereply').each(function(){
            var original = this;
            $shareesresponse = $(this).find("select[id=sharees]").selectize({
                    dataAttr: 'data-data',
                    valueField: 'uid',
                    plugins: ['remove_button'],
                    render: {
                        item: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div>' +
                            '</div>';
                        },
                        option: function(item, escape) {
                            return '<div class="row">' +
                                '<div style="float:left"><img class="img-thumbnail" style="height:45px; margin-left:10px" src="'+ escape(item.thumbnail)+'"/></div>' + '<div style="float:left; margin-left:5px"><span>' + escape(item.text) + '</span><br><span style="font-size:0.7em; color:grey">' + escape(item.designation) + '</span></div><div class="clearfix"></div>' +
                            '</div>';
                        }
                    }
            });

            $(original).find("#clear-all-sharees").bind('click', function(e){
                e.preventDefault();
                $shareesresponse[0].selectize.setValue();
            });

            $(original).find("#select-all-sharees").bind('click', function(e){
                e.preventDefault();
                $shareesresponse[0].selectize.setValue(users);
            });

//            $(this).find('#clonefileupload').click(function(e){
//                e.preventDefault;
//                var num = $(original).find('.fileinput').length,
//                newNum  = new Number(num + 1),
//                newElem = $(original).find('#fileinput' + num).clone().attr('id', 'fileinput' + newNum).fadeIn('slow');
//                $(original).find('#fileinput' + num).after(newElem);
//                $(original).find('#fileinput' + newNum +' select[id=fileupload]').val();
//                $(original).find('#removeclonefileupload').attr('disabled', false);
//            });
//
//            $(this).find('#removeclonefileupload').click(function(e){
//                e.preventDefault;
//                var num = $(original).find('.fileinput').length;
//                $(original).find('#fileinput' + num).slideUp('slow', function(){
//                    $(this).remove();
//                        if (num -1 === 1){
//                            $(original).find('#removeclonefileupload').attr('disabled', true);
//                        }
//                    });
//            });

       });

       $('.formcreatereply').bootstrapValidator({
            // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'subject': {
                    message: 'Subject is empty',
                    validators: {
                        notEmpty: {
                            message: 'Subject is required and cannot be empty'
                        }
                    }
                },
                'message': {
                    validators: {
                        callback: {
                            message: 'Message must be more than 5 characters',
                            callback: function(value, validator, $field) {
                                // Get the plain text without HTML
                                var text = tinymce.activeEditor.getContent({
                                    format: 'text'
                                });
                                return text.length >= 5;
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv',function(){
              $('#finishwizard').modal('hide');
              $('#processing').modal('show');
        });

     /////// run at document loading
     $('.todolist .sidebar-nav li[id=my-tasks]').trigger('click');
     $('.task-list div.task-message').truncate({lines:2,lineHeight:20});
     $('.task-sorter a#sort-date-descending').trigger('click');
     $('.todolist').css('height', $(window).height() * 0.70);
     $('.formcreatetask input[id=request]').prop('checked',true).trigger('change');

//     if(func == 'my-requests' || func == 'my-finished-requests'){
//         $('.task-controls').find('button,a').hide();
//         $('.task-request-controls').find('button,a').show();
//         if($('.todolist .task-list').has('li.task-list-focus').length == 0){
//            $('.task-request-controls').find('button,a').attr('disabled',true);
//         }
//     } else {
//         $('.task-controls').find('button,a').show();
//         $('.task-request-controls').find('button,a').hide();
//         if($('.todolist .task-list').has('li.task-list-focus').length == 0){
//            $('.task-controls').find('button,a').attr('disabled',true);
//         }
//     }

//     $('.todolist .task-list>ul').sieve({ itemSelector: "li" });
//     $('.todolist .task-list').find('input').closest('div').addClass('search');

     $.fn.clearForm = function() {
          return this.each(function() {
            var type = this.type, tag = this.tagName.toLowerCase();
            if (tag == 'form')
              return $(':input',this).clearForm();
            if (type == 'text' || type == 'password' || tag == 'textarea')
              this.value = '';
            else if (type == 'checkbox' || type == 'radio')
              this.checked = false;
            else if (tag == 'select')
              this.selectedIndex = -1;
          });
        };
});

jQuery.nl2br = function(varTest){
    return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
};

Date.createFromMysql = function(mysql_string)
{
   if(typeof mysql_string === 'string')
   {
      var t = mysql_string.split(/[- :]/);
      //when t[3], t[4] and t[5] are missing they defaults to zero
      return new Date(t[0], t[1] - 1, t[2], t[3] || 0, t[4] || 0, t[5] || 0);
   }

   return null;
}

function scrollToElement(selector, time, verticalOffset, container) {
    time = typeof(time) != 'undefined' ? time : 1000;
    verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
    var element = $(selector);
    var offset = element.offset();
    var offsetTop = offset.top + verticalOffset;
    $(container).animate({
        scrollTop: offsetTop
    }, time);
}

function setTaskColor(status,requiredbydate){
    var label = '';
    var currentdate = new Date();
    if(status == "0"){
        if(requiredbydate != "0000-00-00 00:00:00" || typeof requiredbydate !==  'undefined'){
            if((Date.createFromMysql(requiredbydate).getTime() > currentdate.getTime()+172800000)){
                var cssclass = '';
            } else if(Date.createFromMysql(requiredbydate).getTime() < currentdate.getTime()+172800000 && Date.createFromMysql(requiredbydate).getTime() >= currentdate.getTime()){
                cssclass = 'orange';
            } else {
                cssclass = 'red';
            }
        } else {
            cssclass = '';
        }

    } else {
        cssclass = 'green';
    }
    return cssclass;
}
var invoice_type = ['invoice'];
var type_append = { invoice : '-by-matter' };
var table = {};
var filter = {};
	filter.invoice = {};

$.each(invoice_type, function(ndx, type) {
	getCustomFilters(type);
	table[type] = $('#'+type+'-wrapper').find('table').dataTable(getTableOptions(type));
});

function getTableOptions(type) {
	var options = {};
	options.sPaginationType = 'simple_numbers';
	options.bProcessing = true;
	options.lengthMenu = [[12,24,36,-1],["12","24","36","All"]];
	options.sAjaxSource = base_url + "/accounting/dt-"+type+type_append[type];
	options.bServerSide = true;
	options.columnDefs = getColumnDefs(type);
	options.oLanguage = { sProcessing: global_ajax_loader_bar };

	switch(type) {
		case 'invoice':
			options.fnServerParams = function(aoData) {
				aoData.push( { "name" : "client", "value" : filter[type]['client_id'] } );
			}
			/*
			options.order = [[4, 'asc']];
			
			options.fnRowCallback = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				console.log(aData);
				$(nRow).addClass(aData[12]+'-background');
			};
			*/
			break;
	}

	return options;
}

function getCustomFilters(type) {

    switch(type) {
        case 'invoice':
            filter.invoice.client_id = 'all';
            break;
    }

}

function getColumnDefs(type) {
	var result = [];
	switch(type) {
		case 'invoice':
			result.push({
				'targets': [],
				'visible': false,
				'searchable': false
			});

			result.push({
				'targets': [],
				'render': function(data,type,row) { 
	                return moment(data).format('MMMM D, YYYY');
	            }
			});

            result.push({
	            'targets': [4,5],
	            'render': function(data,type,row) { 
	                return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
	            }
            });	

            result.push({
            	'targets': [],
            	'className': 'text-center',
            	'render': function(data,type,row) { 
            		//console.log(row);
            		var status = {
            			0 : 'Unpaid',
            			1 : 'Paid with balance',
            			2 : 'Paid'
            		};

            		status = {
            			0 : '<i class="fa fa-lg fa-ban text-danger"></i>',
            			1 : '<i class="fa fa-lg fa-tag text-info"></i>',
            			2 : '<i class="fa fa-lg fa-check-circle text-success"></i>'            			
            		};

            		var flag_unpaid_reminder = '';
            		if(row[10] == 1) flag_unpaid_reminder = '<i class="fa fa-flag text-warning"></i>';

	                return (status[data] + ' ' + flag_unpaid_reminder); 
	            }
            });

            result.push({
                'targets': [],
                'className': 'text-center',
                'render': function(data, type, row) {                   
                    var delete_disabled = '';
                    if(row[9] == 1) delete_disabled = 'disabled';

                    if(row[9] == 0 || row[9] == 1) {
                    	var action_buttons = '<button class="btn btn-success btn-xs btn-flag-pay" invoice-number="'+row[0]+'" batch-number="'+row[1]+'" balance="'+row[8]+'" data-toggle="modal" data-target="#invoice-pay-modal"><i class="fa fa-check"></i></button> ' +
                            				 '<button class="btn btn-danger btn-xs btn-flag-delete" invoice-number="'+row[0]+'" batch-number="'+row[1]+'" ' + delete_disabled + ' data-toggle="modal" data-target="#invoice-delete-modal"><i class="glyphicon glyphicon-trash"></i></button> ';

                      	if(row[12] == 'yellow' || row[12] == 'orange') {
                      		action_buttons = action_buttons + '<button class="btn btn-warning btn-xs btn-flag-reminder" invoice-number="'+row[0]+'" batch-number="'+row[1]+'" client-id="'+row[2]+'" balance="'+row[8]+'" data-toggle="modal" data-target="#invoice-reminder-modal"><i class="fa fa-flag"></i></button>';
                      	}
                        return action_buttons;
                    } else {
                        return '<i class="glyphicon glyphicon-lock"></i>';
                    }
                }
            });

			break;
	}

	return result;
}

/* Action - Pay */
var current_row;
$(document).on('click', '.btn-flag-pay', function() {
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	var balance = $(this).attr('balance');

	//console.log('Row number: ' + current_row);

	$('#payment_date').datepicker('setDate', new Date());

	$('#invoice-pay-batch-number').text(invoice_number);
	$('#payment_batch_number').val(batch_number);
	$('#payment_amount').val(balance);
});

$('#invoice-pay-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();
	//console.log(post_data);
	
	$.post('/accounting/pay-invoice', post_data, function(data) {
		//console.log(data);

		var row_data = table.invoice.DataTable().row(current_row).data();

		//console.log(row_data);

		row_data[8] = parseFloat(data.result.balance);
		row_data[9] = parseInt(data.result.status_id);
		if(parseFloat(data.result.balance) == 0) row_data[10] = 0;
		row_data[11] = parseFloat(row_data[11]) + parseFloat(data.payment_amount);
		if(parseInt(data.result.status_id) == 2) {
			row_data[12] = 'white';
			$(table.invoice.DataTable().row(current_row).node())
				.removeClass('white-background green-background yellow-background orange-background red-background')
				.addClass('white-background');
		}

		console.log(row_data);

		table.invoice.DataTable().row(current_row).data(row_data);

		$('#invoice-pay-modal').modal('hide');
	});
});

/* Action - Delete */
$(document).on('click', '.btn-flag-delete', function() {
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	$('#invoice-delete-batch-number').text(invoice_number);
	$('#delete_batch_number').val(batch_number);
});

$('#invoice-delete-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();

	$.post('/accounting/delete-invoice', post_data, function(data) {
		if(data.result !== 0) {
			var current_page = table.invoice.DataTable().page.info().page;
			table.invoice.DataTable().row(current_row).remove().draw();
			table.invoice.fnPageChange(current_page);
			$('#invoice-delete-modal').modal('hide');
		}		
	});
});

/* Action - Reminder */
$(document).on('click', '.btn-flag-reminder', function() {
	$('#invoice-reminder-form').addClass('invisible');
	$('#reminder_recipient').attr('readonly', true);
	current_row = table.invoice.DataTable().row($(this).closest('tr')).index();
	var invoice_number = $(this).attr('invoice-number');
	var batch_number = $(this).attr('batch-number');
	var client_id = $(this).attr('client-id');
	var balance = $(this).attr('balance');
	$('#invoice-reminder-batch-number').text(invoice_number);
	$('#reminder_batch_number').val(batch_number);

	$.get('/accounting/reminder-info', { client_id : client_id, batch_number : batch_number, balance : balance }, function(data) {
		console.log(data);
		$('#reminder_recipient_email').val(data.email_address);
		if($('#reminder_recipient_email').val() !== '')
			$('#reminder_recipient').val(data.email_address);
		else 
			$('#reminder_recipient').attr('readonly', false).val('');
		$('#reminder_message').val(data.message);

		$('#invoice-reminder-form').removeClass('invisible');
	});

});

$('#invoice-reminder-form').submit(function(e) {
	e.preventDefault();

	var post_data = $(this).serializeObject();

	$.post('/accounting/send-reminder', post_data, function(data) {
		console.log(data);

		var add_class;
		var row_data = table.invoice.DataTable().row(current_row).data();
		if(data.reminder_count == 1) {
			add_class = 'orange-background';
			row_data[12] = 'orange';
		} else {
			add_class = 'red-background';
			row_data[12] = 'red';
		}

		$(table.invoice.DataTable().row(current_row).node())
			.removeClass('white-background green-background yellow-background orange-background red-background')
			.addClass(add_class);

		table.invoice.DataTable().row(current_row).data(row_data);
		$('#invoice-reminder-modal').modal('hide');
	});
});
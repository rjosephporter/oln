/* Global variables */
global.totals = {
	timesheet : {},
	agreed_cost : $('.input-agreed-cost').val().trim() == '' ? null : parseFloat($('.input-agreed-cost').val()),
	disbursement : 0,
	less : 0,
	overall : 0
};
global.counter = {
	disbursement : -2,
	less : -2
}

global.popup = {
	closed : false
};

/* Event Handlers */

//on load
$(document).ready(function() {
	$.post(base_url + '/invoice/update-invoice-session', { invoice_matter_id : $('input[name="matter[id]"]').val() }, function(data) {
		console.log('updated invoice session data.');
	});

	$('.remove-timesheet-entry').popover({
		trigger: 'focus',
		content: function() {
			return 'Are you sure you want to delete this entry? ' +
					'<button type="button" data-unique-id="' + $(this).data('unique-id') + '" class="btn btn-sm btn-danger confirm-delete-timesheet-entry">Yes</button> <button type="button" class="btn btn-sm btn-default cancel-delete-timesheet-entry">No</button>';
		},
		html: true,
	});	

  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
  if($('input[name="form_type"]').val() == 'update') {
  	$.each(global.timesheet.list, function(ndx,item) {
  		item.units = item.charged_units;
  	});
  }
});

//Datepicker
$('input[name="date"]').datepicker({
    maxDate: '0',
    dateFormat: "MM d, yy"
});

//Add disbursement/less
$('.btn-add-item').click(function(e) {
	e.preventDefault();
	var type = $(this).data('type');
	var items_container = $(this).closest('.table-'+type).find('tbody.items');
	var item_row = items_container.find('tr:first').clone();
	var index = item_row.data('index');
	
	item_row.find('input').val('');

	$.each(item_row.find('input[name^="'+type+'"]'), function(ndx,input_box) {
		//console.log(input_box);
		var name_attr = $(input_box).attr('name');
		var replaced_name_attr = name_attr.replace(index, global.counter[type]);
		$(input_box).attr('name', replaced_name_attr);
	});
	global.counter[type]--;

	items_container.append(item_row);
});

//Event for "Complimentary" checkbox
$('.chk-complimentary').change(function() {
	var row = $(this).closest('tr');
	var unique_id = $(this).data('unique-id');
	var update_data = {};
	var result;
	var other_table_row = $('input[value="'+unique_id+'"]').closest('tr');
	var orig_units = parseFloat(other_table_row.find('.input-orig-units').val());
	if($(this).is(':checked')) { //do something when checked
		other_table_row.find('.input-unit').val(0);
		other_table_row.find('.input-unit-raw').val(0);
		row.find('.input-complimented-units').val(orig_units).prop('readonly', false).prop('required', true);

		update_data.complimentary = true;
		update_data.units = 0;
		update_data.work_hour = 0;
		update_data.amount = 0;
	} else { //do something when unchecked
		other_table_row.find('.input-unit').val(orig_units);
		other_table_row.find('.input-unit-raw').val(orig_units);
		row.find('.input-complimented-units').val('').prop('readonly', true).prop('required', false);

		var hourly_rate = parseFloat(row.find('input[name*="HourlyRate"]').val());
		update_data.complimentary = false;
		update_data.units = orig_units;
		update_data.work_hour = (orig_units * 5) / 60;
		update_data.amount = update_data.work_hour * hourly_rate;
	}
	other_table_row.find('.input-unit').trigger('change');
	row.find('.input-complimented-units').trigger('change');
	
	//other_table_row.find('.text-complimentary').toggle();
	//other_table_row.find('.input-unit').toggle();

	updateTimesheetItems(unique_id, update_data);
	calculateTimesheetTotals();
	calculateOtherTotal('disbursement');
	calculateOtherTotal('less');
	calculateOverallTotal();
	displayTotals();
});

//Even for agreed cost keyup
$(document).on('keyup', '.input-agreed-cost', function() {
	var agreed_cost = ( $(this).val().trim()  == '') ? null : parseFloat( $(this).val() );

	global.totals.agreed_cost = agreed_cost;

	calculateTimesheetTotals();
	calculateOtherTotal('disbursement');
	calculateOtherTotal('less');
	calculateOverallTotal();
	displayTotals();	
});

//Event for other item (disbursement/less) keyup
$(document).on('keyup', '.input-otheritem-amount', function() {
	var type = $(this).data('item-type');
	//console.log(type);
	calculateTimesheetTotals();
	calculateOtherTotal('disbursement');
	calculateOtherTotal('less');
	calculateOverallTotal();
	displayTotals();
});

//Remove timesheet entry
$(document).on('click', '.remove-timesheet-entry', function(e) {
	e.preventDefault();

});


//Cancel remove timesheet entry
$(document).on('click', '.cancel-delete-timesheet-entry', function(e) {
	e.preventDefault();
	console.log('Ping!');
	$(this).closest('td').find('.remove-timesheet-entry').popover('hide');
});

//Confirm remove timesheet entry
$(document).on('click', '.confirm-delete-timesheet-entry', function(e) {
	var timesheet_id = $(this).data('unique-id');
	console.log('FAKE: Timesheet deleted - ' + timesheet_id);
	$.post(base_url+'/invoice/delete', { timesheet_id : timesheet_id }, function(result) {
		if(result.status == 'success') {
			toastr.success(result.message);
			var delay = 3000; //Your delay in milliseconds
	        setTimeout(function(){ window.location.reload(true); }, delay);
		}
	});
});

//Remove other item (disbursement/less)
$(document).on('click', '.remove-item', function(e) {
	e.preventDefault();
	var type = $(this).data('item-type');
	var item_count = $('.remove-item[data-item-type="'+type+'"]').length;
	var row = $(this).closest('tr');
	//console.log(row);

	//don't remove item if only one left
	if(item_count == 1) {
		toastr.error('Must have at least 1 ' + type);
		return false;
	} else {
		row.remove();
		calculateTimesheetTotals();
		calculateOtherTotal('disbursement');
		calculateOtherTotal('less');
		calculateOverallTotal();
		displayTotals();
	}
});

//Change invoice type
$('input[name="invoice_type"]').change(function() {
	var this_value = $(this).val();
	if(this_value == 'FI')
		$('input[name="box_number"]').prop('disabled', false);
	else
		$('input[name="box_number"]').prop('disabled', true);
});

//Legally close matter
$('#legally-close').click(function(e) {
	e.preventDefault();
	$('#complete_confirmation').modal('show');
});

//Confirm legally close
$('#matter_completion_form').submit(function(e) {
	e.preventDefault();
	var data = $(this).serializeArray();
	$(".confirm_body_modal").html($("#modal_preloader_confirm").tmpl());
	$.post('/invoice/legally-close-matter', data, function(result) {
		console.log(result);
		$(".confirm_body_modal").html('Complete. Redirecting page...');
        var delay = 3000; //Your delay in milliseconds
        setTimeout(function(){ window.location = '/matters/view/7/all/all'; }, delay);		
	});
});

//Preview Draft
$('#btn-preview-draft').click(function(e) {
	e.preventDefault();
	var invoice_data = $('#invoice-form').serializeArray();
	$.post('/invoice/save-draft-to-session', invoice_data, function(result) {
		console.log(result);
		window.open("/invoice/preview-draft/"+result.id, "_blank");
	});
});

//Units changed
$(document).on('change', '.input-unit', function() {
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var right_row = $('#right-table').find('tbody tr:eq('+this_index+')');
	var this_unit = parseFloat($(this).val());
	var work_hours = (this_unit * 5) / 60;
	var hourly_rate = parseFloat(right_row.find('input[name*="HourlyRate"]').val());
	var amount = work_hours * hourly_rate;
	right_row.find('input[name*="WorkHour"]').val(work_hours).prev().val(work_hours.toFixed(2));
	right_row.find('input[name*="amount"]').val(amount).prev().val(amount.formatMoney(2));
});

//Complimented units changed
$(document).on('keyup change', '.input-complimented-units', function() {
	var this_value = parseFloat($(this).val()) || 0;
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var left_table = $('#left-table').find('tbody tr:eq('+this_index+')');
	var orig_units = parseFloat(left_table.find('.input-orig-units').val());
	var charged_units = orig_units - this_value;
	left_table.find('.input-unit').val(charged_units.toFixed());
	left_table.find('.input-unit-raw').val(charged_units);
	left_table.find('.input-unit').trigger('change');
	left_table.find('.text-input-complimented-units').val( this_value == 0 ? '' : this_value );

	var update_data = {};
	var unique_id = this_row.find('.chk-complimentary').data('unique-id');
	var hourly_rate = parseFloat(this_row.find('input[name*="HourlyRate"]').val());
	//update_data.complimentary = false;
	update_data.units = charged_units;
	update_data.work_hour = (charged_units * 5) / 60;
	update_data.amount = update_data.work_hour * hourly_rate;

	updateTimesheetItems(unique_id, update_data);
	calculateTimesheetTotals();
	calculateOtherTotal('disbursement');
	calculateOtherTotal('less');
	calculateOverallTotal();
	displayTotals();
});

$('#btn-add-timesheet').click(function(e) {
	e.preventDefault();
	popupTimesheetCreate();
});

$('select[name="status"]').change(function() {
	$('#btn-request-approval').toggle();
});

//Save invoice
/*
$('#invoice-form').submit(function(e) {
	e.preventDefault();
	var data = $(this).serializeArray();
	$.post('/invoice/save2', data, function(result) {
		console.log(result);
		toastr.success(result.data);

		$('#btn-print').show();
		$('#btn-save').hide();

        //var delay = 3000; //Your delay in milliseconds
        //setTimeout(function(){ window.location = '/invoice'; }, delay);
	});
});
*/

/* Functions */

function updateTimesheetItems(unique_id, data) {
	$.each(global.timesheet.list, function(item_ndx,item) {
		if(item.UniqueID == unique_id) {
			$.each(data, function(key,value) {
				item[key] = value;
			});
			return false;
		}
	});
}

function calculateOverallTotal()
{
	var real_cost = (global.totals.agreed_cost > 0) ? global.totals.agreed_cost : global.totals.timesheet.amount;
	global.totals.overall = real_cost + global.totals.disbursement - global.totals.less;
}

function calculateTimesheetTotals() {
	global.totals.timesheet = {
		work_hour : 0,
		amount : 0,
		units : 0,
	};

	var handlers = {};

	//timesheet
	$.each(global.timesheet.list, function(item_ndx,item) {
		//if(item.complimentary == false) {
			global.totals.timesheet.work_hour += parseFloat(item.WorkHour);
			global.totals.timesheet.amount += parseFloat(item.amount);
			global.totals.timesheet.units += parseFloat(item.units);

			//cost summary
			if(typeof handlers[item.EmployeeID] == 'undefined') {
				handlers[item.EmployeeID] = {
					work_hour : parseFloat(item.WorkHour),
					amount : parseFloat(item.amount),
					units : parseFloat(item.units)
				};
			} else {
				handlers[item.EmployeeID].work_hour += parseFloat(item.WorkHour);
				handlers[item.EmployeeID].amount += parseFloat(item.amount);
				handlers[item.EmployeeID].units += parseFloat(item.units);
			}

		//}
	});

	global.totals.timesheet.handlers = handlers;
}

function calculateOtherTotal(type) { // type: disbursement or less
	if(type == 'disbursement' || type == 'less') {
		global.totals[type] = 0;
		var items = $('.input-otheritem-amount[data-item-type="'+type+'"]');
		$.each(items, function(ndx,item) {
			global.totals[type] += parseFloat($(item).val()) || 0;
		});
	}
}

function displayTotals() {
	//Timesheet
	var totalsObj = global.totals.timesheet;
	$('.input-timesheet-total-units').val(totalsObj.units);
	$('.input-timesheet-total-units-round').val(totalsObj.units.toFixed());
	$('.input-total-costs').val(totalsObj.amount);
	$('.input-total-costs-round').val(totalsObj.amount.formatMoney(2));

	$.each($('.input-handler-cost'), function(ndx,item) {
		$(item).val(parseFloat(0).formatMoney(2));
	});
	$.each($('.input-handler-cost-round'), function(ndx,item) {
		$(item).val(parseFloat(0).formatMoney(2));
	});	

	$.each(totalsObj.handlers, function(handler, totals) {
		$('.input-handler-cost[data-handler="'+handler+'"]').val(totals.amount);
		$('.input-handler-cost-round[data-handler="'+handler+'"]').val(totals.amount.formatMoney(2));
	});

	//Other Items (disbursement and less)
	$('.input-disbursement-total').val(global.totals.disbursement);
	$('.input-disbursement-total-round').val(global.totals.disbursement.formatMoney(2));
	$('.input-less-total').val(global.totals.less);
	$('.input-less-total-round').val(global.totals.less.formatMoney(2));

	//Invoice Overall
	$('.input-overall-total').val(global.totals.overall);
	$('.input-overall-total-round').val(global.totals.overall.formatMoney(2));
	$('.text-overall-total').text(global.totals.overall.formatMoney(2));
}

function popupTimesheetCreate() {
	global.popup = window.open(base_url + '/utils/fastcreate?popup=true&matter_id=' + $('input[name="matter[id]"]').val(), "TimesheetCreate", "height="+$(window).height()+",width="+$(window).width());
}

function closeTimesheetCreate() {
    if (global.popup.closed) {
        toastr.info("Refreshing page...");
        window.location.reload();
    }
}

var pollTimer = window.setInterval(function() {
    if (global.popup.closed !== false) { // !== is required for compatibility with Opera
        window.clearInterval(pollTimer);
        closeTimesheetCreate();
    }
}, 200);

/* Dynamic top menu positioning
 *
 */
var num = 100; //number of pixels before modifying styles
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.menu').addClass('fixed');
    } else {
        $('.menu').removeClass('fixed');
    }
});

/**
 * Form Validator
 *
 */
$('#invoice-form').bootstrapValidator({
	fields : {
		'client[address]' : {
			validators : {
	            notEmpty: {
	                message: 'Address is required and cannot be empty'
	            },				
			}			
		},
		'client[attn]' : {
			validators : {
	            notEmpty: {
	                message: 'Attn is required and cannot be empty'
	            },				
			}
		},
		'client[email]' : {
	        validators: {
	            notEmpty: {
	                message: 'Email is required and cannot be empty'
	            },
	            emailAddress: {
	                message: 'Email address is not a valid'
	            }
	        }			
		},
		'client[telephone]' : {
			validators : {
	            notEmpty: {
	                message: 'Telephone is required and cannot be empty'
	            },				
			}			
		},
		'client[fax]' : {
			validators : {
	            notEmpty: {
	                message: 'Fax is required and cannot be empty'
	            },				
			}			
		},
		'matter[description]' : {
			validators : {
	            notEmpty: {
	                message: 'Re is required and cannot be empty'
	            },				
			}			
		}
	}
})
.on('success.form.bv', function(e) {
	// Prevent form submission
	e.preventDefault();
	var data = $(this).serializeArray();

	if($('input[name="form_type"]').val() == 'new') {

		$.post('/invoice/save2', data, function(result) {
			console.log(result);
			toastr.success(result.data);

			$('#btn-print').show();
			$('#btn-save').hide();
		});

	} else {
		$.post('/invoice/save2', data, function(result) {
			console.log(result);
			toastr.success(result.data);

			$('#btn-print').show();
			$('#btn-update').hide();			
		});
	}
});

$(window).bind('unload', function() {
	$.ajax({
		type: 'POST',
		async: false,
		url: base_url + '/invoice/update-invoice-session',
		success: function(data) {
			console.log('Cleared Invoice Session');
		}
	});
});

//Add new timesheet entry to invoice
$('#new_timesheet_form').submit(function(e) {
	e.preventDefault();
	var form_data = $(this).serializeArray();
	$.post('/invoice/create-new-timesheet', form_data, function(result) {
		if(result.status == 'success') {
			toastr.success(result.message);
			var delay = 2000; //Your delay in milliseconds
	        setTimeout(function(){ window.location.reload(true); }, delay);			
		} else {
			toastr.error(result.message);
		}

		$('#new_timesheet_log').modal('hide');
	});
});

$(document).on('change keyup', 'input[name="new_timesheet[units]"]', function() {
    var units = parseInt($(this).val());
    var work_hour = (units * 5) / 60;
    $('input[name="new_timesheet[workhour_round]"]').val(work_hour.toFixed(2));
    $('input[name="new_timesheet[workhour]"]').val(work_hour);
});

$('input[name="new_timesheet[date]"]').datepicker({
    maxDate : '0',
    dateFormat: "MM d, yy"
}).datepicker('setDate', new Date());

$(document).on('change', 'select[name="new_timesheet[handler]"]', function() {
    var selected_handler = $(this).val();
    $(this).parent().parent().find('select[name="new_timesheet[hourlyrate]"]').val(selected_handler);
});
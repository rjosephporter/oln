var all_matters = {};
var green_matters = {};
var orange_matters = {};
var red_matters = {};
var untouched_matters = {};
var finished_matters = {};

function procDes(obj){
	var billable = 0; var billed = 0; var unbilled = 0; 
	obj.matter = $.grep(all_matters,function(elem){
		switch(obj.lvl){
			case 1: return elem.Completed == 0 && elem.mn_invl <= 1  && elem.mn_invl != null; break;
			case 2: return elem.Completed == 0 && elem.mn_invl == 2  && elem.mn_invl != null; break;
			case 3: return elem.Completed == 0 && elem.mn_invl > 2  && elem.mn_invl != null; break;
			case 4: return elem.mn_invl == null && elem.Completed == 0; break;
			case 5: return elem.Completed == 1; break;
		}
	});
	$(obj.info_count).html(Number(obj.matter.length).toLocaleString('en'));
	obj.matter.forEach(function(elem){
		billable += elem.billable_amount > 0 ? parseFloat(elem.billable_amount) : 0;
		billed += elem.billed_amount > 0 ? parseFloat(elem.billed_amount) : 0;
	});
	unbilled = billable - billed;
	$(obj.info_billable).html(billable.formatMoney(2));
	$(obj.info_billed).html(billed.formatMoney(2));
	$(obj.info_unbilled).html(unbilled.formatMoney(2));
}

function process_headers(){
	procDes({lvl : 1, matter : green_matters, info_billed : "#billed_info", info_unbilled : "#unbilled_info", info_billable : "#billable_info", info_count : "#green_count"});
	procDes({lvl : 2, matter : orange_matters, info_billed : "#billed_orange_info", info_unbilled : "#unbilled_orange_info", info_billable : "#billable_orange_info", info_count : "#orange_count"});
	procDes({lvl : 3, matter : red_matters, info_billed : "#billed_red_info", info_unbilled : "#unbilled_red_info", info_billable : "#billable_red_info", info_count : "#red_count"});
	procDes({lvl : 4, matter : untouched_matters, info_billed : "#billed_grey_info", info_unbilled : "#unbilled_grey_info", info_billable : "#billable_grey_info", info_count : "#grey_count"});
	procDes({lvl : 5, matter : finished_matters, info_billed : "#billed_blue_info", info_unbilled : "#unbilled_blue_info", info_billable : "#billable_blue_info", info_count : "#blue_count"});
}

$(function(){
	$.get('/assets/thisapp/data/bills.dat', function(data){
		all_matters = data;
		process_headers();
		$('li[data-toggle="tooltip"]').tooltip();
	},'json');
});
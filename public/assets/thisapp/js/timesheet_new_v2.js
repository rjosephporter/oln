/* Global Variables */
var global = {};
global.selectors = {
	timesheet : {
		left_table : '#timesheet-left-table',
		right_table : '#timesheet-right-table'
	}
};
global.counters = {
	added_timesheet : 0
};

global.selectize = {
	matter : null
}

/* Event Handlers */

//on load
$(document).ready(function() {
	$.post(base_url + '/timesheet/update-timesheet-session', { timesheet_matter_id : $('input[name="matter[id]"]').val() }, function(data) {
		console.log('updated timesheet session data.');
	});

	$('.btn-add-timesheet').prop('disabled', false); 

	$(window).keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
	$('.timesheet-date[data-type="initial"]').datepicker({
		maxDate: '0',
		dateFormat: "dd-M-yy"		
	});	  
	$('.drpdwn-handler[data-type="initial"]').selectize();
	global.selectize.matter = $('.drpdwn-matter[data-type="list"]').selectize();
	calculateTimesheetTotal();
	addTimesheetItem();
});

//Add timesheet item
$('.btn-add-timesheet').click(function(e) {
	e.preventDefault();
	addTimesheetItem();
})

//Change date
$(document).on('change', '.timesheet-date', function() {
	var this_row = $(this).closest('tr');
	this_row.find('.input-changed').val('1');
});

//Change solicitor
$(document).on('change', '.drpdwn-handler', function() {
	var this_value = $(this).val();
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var right_table = $(global.selectors.timesheet.right_table);
	var right_row = right_table.find('tbody tr:eq('+this_index+')');

	right_row.find('.hourly-rate-source-list').val(this_value)
	var hourly_rate = right_row.find('.hourly-rate-source-list option[value="'+this_value+'"]').text();
	right_row.find('.input-hourly-rate-round').val(parseFloat(hourly_rate).formatMoney(2));
	right_row.find('.input-hourly-rate').val(parseFloat(hourly_rate));

	var work_hours = parseFloat(right_row.find('.input-work-hours').val()) || 0;
	var timesheet_amount = parseFloat(hourly_rate) * parseFloat(work_hours);
	right_row.find('.input-timesheet-amount-round').val(timesheet_amount.formatMoney(2));
	right_row.find('.input-timesheet-amount').val(timesheet_amount);

	this_row.find('.input-changed').val('1');

	calculateTimesheetTotal();
});

//Change comment/description
$(document).on('change keyup', '.input-comment', function() {
	var this_row = $(this).closest('tr');
	this_row.find('.input-changed').val('1');
});

//Change unit
$(document).on('change keyup', '.input-unit', function() {
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var this_value = parseFloat($(this).val()) || 0;
	var right_table = $(global.selectors.timesheet.right_table);
	var right_row = right_table.find('tbody tr:eq('+this_index+')');

	var this_work_hour = (this_value * 5) / 60;
	var this_hourly_rate = parseFloat(right_row.find('.input-hourly-rate').val()) || 0;
	var this_amount = this_work_hour * this_hourly_rate;

	right_row.find('.input-work-hours-round').val(this_work_hour.toFixed(2));
	right_row.find('.input-work-hours').val(this_work_hour);

	right_row.find('.input-timesheet-amount-round').val(this_amount.formatMoney(2));
	right_row.find('.input-timesheet-amount').val(this_amount);	

	this_row.find('.input-changed').val('1');

	calculateTimesheetTotal();
});

//Change work hours
/*
$(document).on('change keyup', '.input-work-hours', function() {
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var this_value = parseFloat($(this).val()) || 0;
	var this_hourly_rate = parseFloat(this_row.find('.input-hourly-rate').val());
	var this_unit = (this_value * 60) / 5;
	var this_amount = this_value * this_hourly_rate;
	var left_table = $(global.selectors.timesheet.left_table);
	var left_row = left_table.find('tbody tr:eq('+this_index+')');
	
	left_row.find('.input-unit-round').val(this_unit.toFixed(2));
	left_row.find('.input-unit').val(this_unit);

	this_row.find('.input-timesheet-amount-round').val(this_amount.formatMoney(2));
	this_row.find('.input-timesheet-amount').val(this_amount);

	calculateTimesheetTotal();
});
*/

//Remove timesheet item
$(document).on('click', '.btn-remove-timesheet-item', function(e) {
	e.preventDefault();

	var timesheet_id = $(this).data('timesheet-id');
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var left_row = $(global.selectors.timesheet.left_table).find('tbody tr:eq('+this_index+')');
	if(timesheet_id == '') {
		//directly remove row if newly added item
		this_row.remove();
		left_row.remove();
		calculateTimesheetTotal();
	} else {
		//delete from db if timesheet item already exists
	    if (confirm("This will delete the item from our records. Click 'OK' to confirm.") == true) {
	        //proceed with delete
	        $.post('/timesheet/delete', { timesheet_id : timesheet_id }, function(result) {
				this_row.remove();
				left_row.remove();
				calculateTimesheetTotal();
	        });
	    } else {
	        //just close the dialog box
	    }		
	}

});

//Move timesheet item to another matter
$(document).on('click', '.btn-move-timesheet-item', function(e) {
	e.preventDefault();
	var modal = $('#modal-move-timesheet');

	//Get row data
	var timesheet_id = $(this).data('timesheet-id');
	var this_row = $(this).closest('tr');
	var this_index = this_row.index();
	var left_row = $(global.selectors.timesheet.left_table).find('tbody tr:eq('+this_index+')');

	var row_data = {
		date 		: left_row.find('[name*="[WorkDate]"]').val(),
		handler 	: left_row.find('[name*="[EmployeeID]"]').val(),
		description	: left_row.find('[name*="[Comment]"]').val(),
		units 		: left_row.find('[name*="[units]"]').val(),
		id 			: timesheet_id,
		index 		: this_index,
		hourly_rate : this_row.find('[name*="[HourlyRate]"]').val(),
		work_hours 	: this_row.find('[name*="[WorkHour]"]').val(),
		amount 		: this_row.find('[name*="[amount]"]').val()
	}

	//Clear existing values
	modal.find('[name^="timesheet"]').val('');
	global.selectize.matter[0].selectize.setValue('');

	//Populate values
	$.each(row_data, function(key,value) {
		modal.find('input[name="timesheet['+key+']"]').val(value);
	});

	modal.modal('show');
});

//Confirm move timesheet
$('#form-move-timesheet').submit(function(e) {
	e.preventDefault();
	var row_index = $(this).find('input[name="timesheet[index]"]').val();
	var data = $(this).serializeArray();
	$.post(base_url+'/timesheet/move-item', data, function(result) {
		if(result.status == 'success') {
			$(global.selectors.timesheet.left_table).find('tbody tr:eq('+row_index+')').remove();
			$(global.selectors.timesheet.right_table).find('tbody tr:eq('+row_index+')').remove();
			toastr.success(result.message);
		} else {			
			toastr.error(result.message);
		}
		$('#modal-move-timesheet').modal('hide');
	});
});

//Save timesheet
/* Transfered to Form Validator
$('#timesheet-form').submit(function(e) {
	e.preventDefault();
	var data = $(this).serializeArray();
	$.post('/timesheet/submit-new3', data, function(result) {
		console.log(result);
		if(result.success == true) {
			toastr.success('New timesheet saved successfully. Reloading page...');
	        var delay = 3000; //Your delay in milliseconds
	        setTimeout(function(){ window.location.reload(); }, delay);
		} else {
			toastr.error('Database error.');
		}
	});
});
*/

/* Functions */

function addTimesheetItem() {
	var left_table  = $(global.selectors.timesheet.left_table);
	var right_table = $(global.selectors.timesheet.right_table)
	var left_source_row  = $('.added-item[data-type="left-table"]').clone();
	var right_source_row = $('.added-item[data-type="right-table"]').clone();

	$.each( left_source_row.find('[name^="timesheet[list]"]'), function(ndx, item) {
		var index_number = (global.counters.added_timesheet + 1) * -1;
		var orig_name = $(item).attr('name');
		var rename = orig_name.replace('XXX', index_number);
		$(item).attr('name', rename);
	});

	$.each( right_source_row.find('[name^="timesheet[list]"]'), function(ndx, item) {
		var index_number = (global.counters.added_timesheet + 1) * -1;
		var orig_name = $(item).attr('name');
		var rename = orig_name.replace('XXX', index_number);
		$(item).attr('name', rename);
	});

	global.counters.added_timesheet++;

	//clean up row
	left_source_row.removeAttr('class data-type');
	right_source_row.removeAttr('class data-type');

	//prepend row
	left_table.find('tbody').prepend(left_source_row);
	right_table.find('tbody').prepend(right_source_row);

	//initialize DOMs
	left_source_row.find('.timesheet-date').datepicker({
	    maxDate: '0',
	    dateFormat: "dd-M-yy"		
	});	
	var selectize_option_cnt = left_source_row.find('.drpdwn-handler option').length;
	var first_option = left_source_row.find('.drpdwn-handler option:nth-child(2)').attr('value');
	var selectize_item = left_source_row.find('.drpdwn-handler').selectize();
	if(selectize_option_cnt == 2) {
		selectize_item[0].selectize.setValue(first_option);
	}
	left_source_row.find('.drpdwn-handler').trigger('change');
}

function calculateTimesheetTotal() {
	var total = {
		unit : 0,
		amount : 0
	};
	$.each($('.input-unit[data-billed="0"]'), function(ndx,item) {
		var item_value = $(item).val();
		total.unit += parseFloat(item_value) || 0;
	});
	$.each($('.input-timesheet-amount[data-billed="0"]'), function(ndx,item) {
		var item_value = $(item).val();
		total.amount += parseFloat(item_value) || 0;
	});

	$('.input-timesheet-total-units-round').val(total.unit.toFixed(2));
	$('.input-timesheet-total-units').val(total.unit);

	$('.text-overall-total').text(total.amount.formatMoney(2));

	return total;
}

/* Dynamic top menu positioning
 *
 */
var num = 100; //number of pixels before modifying styles
$(window).bind('scroll', function () {
    if ($(window).scrollTop() > num) {
        $('.menu').addClass('fixed');
    } else {
        $('.menu').removeClass('fixed');
    }
});

/**
 * Form Validator
 *
 */
$('#timesheet-form').bootstrapValidator({
	fields : {
		'client[address]' : {
			validators : {
	            notEmpty: {
	                message: 'Address is required and cannot be empty'
	            },				
			}			
		},
		'client[attn]' : {
			validators : {
	            notEmpty: {
	                message: 'Attn is required and cannot be empty'
	            },				
			}
		},
		'client[email]' : {
	        validators: {
	            notEmpty: {
	                message: 'Email is required and cannot be empty'
	            },
	            emailAddress: {
	                message: 'Email address is not a valid'
	            }
	        }			
		},
		'client[telephone]' : {
			validators : {
	            notEmpty: {
	                message: 'Telephone is required and cannot be empty'
	            },				
			}			
		},
		'client[fax]' : {
			validators : {
	            notEmpty: {
	                message: 'Fax is required and cannot be empty'
	            },				
			}			
		},
		'matter[description]' : {
			validators : {
	            notEmpty: {
	                message: 'Re is required and cannot be empty'
	            },				
			}			
		}
	}
})
.on('success.form.bv', function(e) {
	// Prevent form submission
	e.preventDefault();
	var data = $(this).serializeArray();
	$.post('/timesheet/submit-new3', data, function(result) {
		console.log(result);
		if(result.success == true) {
			toastr.success('New timesheet saved successfully. Reloading page...');
			$(window).unbind('beforeunload');
	        var delay = 3000; //Your delay in milliseconds
	        setTimeout(function(){ window.location.reload(true); }, delay);
		} else {
			toastr.error('Database error.');
		}
	});
});

//Prompt before unload
$(window).bind('beforeunload', function(){
  return 'Make sure to save your work before leaving.';
});

$(window).bind('unload', function() {
	$.ajax({
		type: 'POST',
		async: false,
		url: base_url + '/timesheet/update-timesheet-session',
		success: function(data) {
			console.log('Cleared Timesheet Session');
		}
	});
});

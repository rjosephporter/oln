var report_types = ['matters', 'handlers', 'clients', 'wip'];

var table = {};

var filter = {};
    filter.matters = {};
    filter.wip = {};
    filter.handlers = {};
    filter.clients = {};
    filter.chart = {};

var myNewChart; 

var draw_timeline = true;

getCustomFilters('matters');

$(document).ready(function(){


}); 

$.each(report_types, function(ndx, report_type) {
    table[report_type] = $('#'+report_type+'-list-wrapper').find('table').DataTable(getTableOptions(report_type));        
});

$('#handler, #last_activity, #unbilled_amount, #handler_unbilled_amount, #client_unbilled_amount').change(function(){
    var type = $(this).attr('filter-for');
    getCustomFilters(type);
    table[type].draw();
});

showChart(getCustomFilters('chart'));
$('select[id^="chart_"').change(function(){
    myNewChart.destroy();
    showChart(getCustomFilters('chart'));
});

$('#matters-list-wrapper tbody').on( 'click', 'td', function () {
    if($(this).index() !== 0) {
        pM(parseInt($(this).closest('tr').find('td:nth-child(2)').text()));
        var modal = $('#openmatter');
        modal.modal('show');
    }
} );

$('a[href="#_matter_div_timeline"]').click(function(e) {
    if(draw_timeline) {
        $('#timeline-wrapper').hide();
        $('#timeline-loader').show();
        var matter_id = $('#active_job_assignment_id').val();
        $.get('/reports/matter-timeline', { matter_id : matter_id }, function(data) {
            $('#timeline-loader').hide();
            $('#timeline-wrapper').html(data);
            $('#timeline-wrapper').show();
        });
    } else {
        $('#timeline-wrapper').show();
        $('#timeline-loader').hide();
    }
    draw_timeline = false;
});

$('.btn-folder').click(function(e) {
    var color = $(this).attr('color');
    console.log(color);
    $('#last_activity').val(color).change();
});

function showChart(ajax_data) {
    $.get(base_url + "/reports/chart", ajax_data, function(data){

        /* Charts */
        var ctx = document.getElementById("myChart").getContext("2d");

        var chart_data = {
            labels: data.months,
            datasets: [
                {
                    label: "",
                    fillColor: "rgba(24, 188, 156, 0.2)",
                    strokeColor: "rgba(24, 188, 156, 1)",
                    pointColor: "rgba(151,187,205,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(151,187,205,1)",
                    data: data[ajax_data.show]
                }
            ]        
        };
        var chart_options = {
            scaleLabel: "<%='$'+value%>",
            tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%='$'+value%>",
            //responsive: true           
        };

        myNewChart = new Chart(ctx).Line(chart_data, chart_options);
    }, 'json');
}

function getTableOptions(type) {
    var options = {};
    options.sPaginationType = 'simple_numbers';
    options.bProcessing = true;
    options.lengthMenu = [[12,24,36,-1],["12","24","36","All"]];
    options.sAjaxSource = base_url + "/reports/dt-"+type+"-list";
    options.bServerSide = true;
    options.columnDefs = getColumnDefs(type);
    options.oLanguage = { sProcessing: global_ajax_loader_bar };

    switch(type) {
        case 'matters':
            options.fnServerParams = function ( aoData ) {
                if(filter.matters.handler != '0')
                    aoData.push( { "name": "handler_id", "value": filter.matters.handler } );
                if(filter.matters.last_activity != '0')
                    aoData.push( { "name": "folder_types", "value": filter.matters.last_activity } );
                if(filter.matters.unbilled_amount != '0')
                    aoData.push( { "name": "unbilled_amount_range", "value": filter.matters.unbilled_amount } );
            };
            /*
            options.createdRow = function ( row, data, index ) {
                $('td', row).addClass(data[5]+'-background');
            };
            */
            options.order = [[7, 'desc']];
            options.drawCallback = function(settings) {
                // console.log('Grand Total [billable] = ' + settings.json.grand_total_billable_amount);
                // console.log('Grand Total [unbilled] = ' + settings.json.grand_total_unbilled_amount);
                var grand_total_billable_amount = '$0.00';
                var grand_total_unbilled_amount = '$0.00';
                
                if(settings.json.grand_total_billable_amount !== null) {
                    grand_total_billable_amount = '$' + settings.json.grand_total_billable_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }
                if(settings.json.grand_total_unbilled_amount !== null) {
                    grand_total_unbilled_amount = '$' + settings.json.grand_total_unbilled_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                }

                $('#matters-list-wrapper table').append('<tr><th colspan="6" class="text-right">Total:</th><th>'+grand_total_billable_amount+'</th><th>'+grand_total_unbilled_amount+'</th><th colspan="2"></th></tr>');
            };
            break;
        case 'handlers':
            options.fnServerParams = function ( aoData ) {
                if(filter.handlers.unbilled_amount != '0')
                    aoData.push( { "name": "unbilled_amount_range", "value": filter.handlers.unbilled_amount } );
            };
            break;
        case 'clients':
            options.fnServerParams = function ( aoData ) {
                if(filter.clients.unbilled_amount != '0')
                    aoData.push( { "name": "unbilled_amount_range", "value": filter.clients.unbilled_amount } );
            };
            break;
        case 'wip':
            options.fnServerParams = function ( aoData ) {
//                if(filter.matters.handler != '0')
//                    aoData.push( { "name": "handler_id", "value": filter.matters.handler } );
//                if(filter.matters.last_activity != '0')
//                    aoData.push( { "name": "folder_types", "value": filter.matters.last_activity } );
                if(filter.matters.unbilled_amount != '0')
                    aoData.push( { "name": "unbilled_amount_range", "value": filter.matters.unbilled_amount } );
            };
            /*
            options.createdRow = function ( row, data, index ) {
                $('td', row).addClass(data[5]+'-background');
            };
            */
//            options.order = [[7, 'desc']];
//            options.drawCallback = function(settings) {
//                console.log('Grand Total [billable] = ' + settings.json.grand_total_billable_amount);
//                console.log('Grand Total [unbilled] = ' + settings.json.grand_total_unbilled_amount);
//                var grand_total_billable_amount = '$0.00';
//                var grand_total_unbilled_amount = '$0.00';
//                
//                if(settings.json.grand_total_billable_amount !== null) {
//                    grand_total_billable_amount = '$' + settings.json.grand_total_billable_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//                }
//                if(settings.json.grand_total_unbilled_amount !== null) {
//                    grand_total_unbilled_amount = '$' + settings.json.grand_total_unbilled_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//                }
//
//                $('#wip-wrapper table').append('<tr><th colspan="6" class="text-right">Total:</th><th>'+grand_total_billable_amount+'</th><th>'+grand_total_unbilled_amount+'</th><th colspan="2"></th></tr>');
//            };
            break;    
    }

    return options;
}  

function getCustomFilters(type) {

    switch(type) {
        case 'matters':
            filter.matters.handler = $('#handler').val();
            filter.matters.last_activity = $('#last_activity').val();
            filter.matters.unbilled_amount = $('#unbilled_amount').val();
            break;
        case 'handlers':
            filter.handlers.unbilled_amount = $('#handler_unbilled_amount').val();
            break;
        case 'clients':
            filter.clients.unbilled_amount = $('#client_unbilled_amount').val();
            break;
        case 'chart':
            filter.chart.type = $('#chart_type').val();
            filter.chart.year = $('#chart_year').val();
            filter.chart.show = $('#chart_show').val();
            return filter.chart;
            break;
        case 'wip':
            // filter.matters.unbilled_amount = $('#unbilled_amount').val();
            filter.matters.unbilled_amount = "2";
            break;
    }

}

function getColumnDefs(type) {
    var result = [];
    switch(type) {
        case 'matters':
            result.push({
                        'targets': [0],
                        'className': 'text-center',
                        'render': function(data,type,row) {
                            return '<button class="btn btn-sm btn-block btn-info btn-send-reminder"><i class="fa fa-envelope"></i></button>';
                        }
                    });

            result.push({
                        'targets': [1],
                        'render': function(data,type,row) {
                            return '<img src="/assets/thisapp/images/matters/'+row[8]+'.png" style="height: 18px" />  ' + data;
                        }
                    });

            result.push({
                        'targets': [10],
                        'render': function(data,type,row) {
                            var result;
                            if(data === null) {
                                result = 'NONE';
                            } else {
                                result = moment(data).format('MMMM D, YYYY');
                            }
                            return result;
                        }
                    });

            result.push({
                        'targets': [6,7,11],
                        'render': function(data,type,row) { 
                            return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
                        }
                    });
            result.push({
                        'targets': [8,9],
                        'visible': false,
                        'searchable': false
                    });
            break;
        case 'handlers':
            result.push({
                        'targets': [2,3,4],
                        'render': function(data,type,row) { 
                            return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
                        }
                    });
            break;
        case 'clients':
            result.push({
                        'targets': [4],
                        'render': function(data,type,row) { 
                            return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
                        }
                    });
            break;
        case 'wip':
            result.push({
                        'targets': [6],
                        'visible': false,
                        'searchable': false
                    });
            break;
    }

    return result;
}

/* Action - Reminder */
$(document).on('click', '.btn-send-reminder', function(e) {
    e.preventDefault();

    $('#invoice-reminder-form').addClass('invisible');
    $('#reminder_recipient').attr('readonly', true);
    
    current_row = $(this).closest('tr').index();
    row_data = table.matters.row(current_row).data();

    console.log(row_data);
   
    $('#invoice-reminder-batch-number').text(row_data[1]);

    var request_data = {
        reminder_type: 'matter',
        matter_id: row_data[1],
        total_unbilled_amount: row_data[7]
    };

    $.get('/accounting/reminder-info', request_data, function(data) {
        console.log(data);
        $('#reminder_recipient_email').val(data.email_address);
        if($('#reminder_recipient_email').val() !== '')
            $('#reminder_recipient').val(data.email_address);
        else 
            $('#reminder_recipient').attr('readonly', false).val('');
        $('#reminder_message').val(data.message);

        $('#invoice-reminder-form').removeClass('invisible');
    });
    
    $('#invoice-reminder-modal').modal('show');
});

$('#invoice-reminder-form').submit(function(e) {
    e.preventDefault();

    var post_data = $(this).serializeObject();

    post_data.no_log = 1;

    $.post('/accounting/send-reminder', post_data, function(data) {
        console.log(data);

        $('#invoice-reminder-modal').modal('hide');
    });
});
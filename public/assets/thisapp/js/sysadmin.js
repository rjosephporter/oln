/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    var $usertable = $('#user_table');
    
    $usertable.dataTable({
        "aaSorting": [],
        "iDisplayLength": 30,
        "aoColumnDefs": [{"bSortable": false, "aTargets": [3,4,5]}],
        stateSave: true,
        sDom: 'T<"clear">lfrtip',
        tableTools: {
            "aButtons": [
                {
                    "sExtends": "print",
                    "mColumns": [0, 1, 2 ]
                }
            ],
            "sSwfPath": "/assets/DataTables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
        }

    });

    
    $usertable.on('click','.reset-password',function(){
        var userid = $(this).parents('tr').attr('data-user-id')
        var r = confirm("This will reset the user's password to '123456'. Proceed?");
        if (r == true){
            $.post('/sysadmin/reset-password',{"user_id":userid})
                    .done(function(data){
                        if(data.status == 'success'){
                            toastr.success(data.msg);
                        } else {
                            toastr.error(data.msg);
                        }
                    });
        };
    });
    
    $usertable.on('click','.edit-employee-info',function(){
        var employeeid = $(this).parents('tr').attr('data-employee-id');
        var w = window.open('/employee/employeeupdate/'+employeeid);    
    });
    
    $usertable.on('click','.view-login-history',function(){
        var employeeid = $(this).parents('tr').attr('data-employee-id');
        console.log(employeeid);
        var w = window.open('/employee/viewloginhistory/'+employeeid);    
    });
})


$(document).ready(function(){
	

    var max_fields      = 5; //maximum input boxes allowed
	    var wrapper         = $(".input_fields_wrapdir"); //Fields wrapper
	    var add_button      = $(".add_field_buttondir"); //Add button ID

	    var wrapper2         = $(".input_fields_wrapshare"); //Fields wrapper
	    var add_button2      = $(".add_field_buttonshare"); //Add button ID

	    var wrapper3         = $(".input_fields_wrapben"); //Fields wrapper
	    var add_button3      = $(".add_field_buttonben"); //Add button ID

	    var wrapper4         = $(".input_fields_wrapperson"); //Fields wrapper
	    var add_button4      = $(".add_field_buttonperson"); //Add button ID

	    $('#item-container-source').show();
    	$('#item-container-source2').hide();
    	$('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
	    
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div><br><a href="#" class="close pull-right remove_fieldnew"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_dirnew[]"/></div>'); //add input box
	        }
	    });

	    $(wrapper).on("click",".remove_fieldnew", function(e1){ //user click on remove text
	        e1.preventDefault(); $(this).parent('div').remove(); x1--;
	    })
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	    	var popOverSettings = {
		        placement: 'left',
		        html: true,
		        selector: '.remove_field',
		        content: function () {
		            return 'Delete this item? <button class="btn btn-danger btn-sm btn-confirm-delete">Yes</button> <button class="btn btn-default btn-sm btn-cancel-delete">No</button>';
		        }
		    }
		   $('body').popover(popOverSettings);
		    $(document).on('click', '.btn-cancel-delete', function(){
		        $(wrapper).find('.remove_field').popover('hide');
		    });
	        e.preventDefault();//$(this).parent('div').remove(); x--;
	    })

	    //new
	     
		    $(document).on('click', '.btn-confirm-delete', function(){
		        var delete_log_btn = $(this).parent().parent().parent().find('.remove_field');
		        $(this).parent().parent().parent('div').remove();  x--;
		        var unique_id = delete_log_btn.attr('uniqueid');
		        var type = delete_log_btn.attr('type');
		        //toastr.info('Delete: ' + unique_id);
		        if(type=='director'){
			        $.post('/clients/deletedirector', { director_id : unique_id }, function(data) {
			            //toastr[data.status](data.message);
			            toastr.success(data.message);
			            //if(data.status == 'success') 
			        });
		        }else if(type=='sharehlder'){
			         $.post('/clients/deleteshare', { share_id : unique_id }, function(data) {
		            //toastr[data.status](data.message);
		            toastr.success(data.message);
		            //if(data.status == 'success') 
		        	});

		        }else if(type=='owner'){
			       $.post('/clients/deleteowner', { owner_id : unique_id }, function(data) {
		            //toastr[data.status](data.message);
		            toastr.success(data.message);
		            //if(data.status == 'success') 
		        	});
		        }else if(type=='rep'){
			         $.post('/clients/deleterep', { rep_id : unique_id }, function(data) {
		            toastr.success(data.message);
		            //if(data.status == 'success') 
		        	});
		        }

		        delete_log_btn.popover('hide');
		    });
	    //end

	    var x1 = 1; //initlal text box count
	    $(add_button2).click(function(e1){ //on add input button click
	        e1.preventDefault();
	        if(x1 < max_fields){ //max input box allowed
	            x1++; //text box increment
	            $(wrapper2).append('<div><br><a href="#" class="close pull-right remove_fieldnew"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_sharenew[]"/></div>'); //add input box
	        }
	    });
	    
	    $(wrapper2).on("click",".remove_fieldnew", function(e1){ //user click on remove text
	        e1.preventDefault(); $(this).parent('div').remove(); x1--;
	    })

	    $(wrapper2).on("click",".remove_field", function(e1){ //user click on remove text
	    	var popOverSettings = {
		        placement: 'left',
		        html: true,
		        selector: '.remove_field',
		        content: function () {
		            return 'Delete this item? <button class="btn btn-danger btn-sm btn-confirm-delete">Yes</button> <button class="btn btn-default btn-sm btn-cancel-delete">No</button>';
		        }
		    }
		    $('body').popover(popOverSettings);
		    $(document).on('click', '.btn-cancel-delete', function(){
		        $(wrapper2).find('.remove_field').popover('hide');
		    });
		    e1.preventDefault();
	    })

	    var x2 = 1; //initlal text box count
	    $(add_button3).click(function(e2){ //on add input button click
	        e2.preventDefault();
	        if(x2 < max_fields){ //max input box allowed
	            x2++; //text box increment
	            $(wrapper3).append('<div><br><a href="#" class="close pull-right remove_fieldnew"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_bennew[]"/></div>'); //add input box
	        }
	    });
	    
	    $(wrapper3).on("click",".remove_fieldnew", function(e2){ //user click on remove text
	        e2.preventDefault(); $(this).parent('div').remove(); x2--;
	    })

	    $(wrapper3).on("click",".remove_field", function(e2){ //user click on remove text
	    	var popOverSettings = {
		        placement: 'left',
		        html: true,
		        selector: '.remove_field',
		        content: function () {
		            return 'Delete this item? <button class="btn btn-danger btn-sm btn-confirm-delete">Yes</button> <button class="btn btn-default btn-sm btn-cancel-delete">No</button>';
		        }
		    }
		    $('body').popover(popOverSettings);
		    $(document).on('click', '.btn-cancel-delete', function(){
		        $(wrapper3).find('.remove_field').popover('hide');
		    });
	        e2.preventDefault();//$(this).parent('div').remove(); x--;
	    })

	    var x3 = 1; //initlal text box count
	    $(add_button4).click(function(e3){ //on add input button click
	        e3.preventDefault();
	        if(x3 < max_fields){ //max input box allowed
	            x3++; //text box increment
	            $(wrapper4).append('<div><br><label for="new_person">Name of Person(s) representing the Company </label><br><a href="#" class="close pull-right remove_fieldnew"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_personnew[]"/><label for="new_personhkid">HKID Card No/Passport No </label><input type="text" class="form-control" name="new_personhkidnew[]"><label for="new_personpos">Capacity Position </label><input type="text" class="form-control" name="new_personposnew[]"><label for="new_personconnum">Contact Number </label><input type="text" class="form-control" name="new_personconnumnew[]"><label for="new_personemail">Email Address </label><input type="text" class="form-control" name="new_personemailnew[]"></div>'); //add input box
	        }
	    });
	    
	    $(wrapper4).on("click",".remove_fieldnew", function(e3){ //user click on remove text
	        e3.preventDefault(); $(this).parent('div').remove(); x3--;
	    })

	    $(wrapper4).on("click",".remove_field", function(e3){ //user click on remove text
	    	var popOverSettings = {
		        placement: 'left',
		        html: true,
		        selector: '.remove_field',
		        content: function () {
		            return 'Delete this item? <button class="btn btn-danger btn-sm btn-confirm-delete">Yes</button> <button class="btn btn-default btn-sm btn-cancel-delete">No</button>';
		        }
		    }
		    $('body').popover(popOverSettings);
		    $(document).on('click', '.btn-cancel-delete', function(){
		        $(wrapper4).find('.remove_field').popover('hide');
		    });
	        e3.preventDefault();//$(this).parent('div').remove(); x--;
	    })


	    //$(document).on('change', 'select[name="new_clienttype_id"]', function() {
    
    	var ctype = $('select[name="new_clienttype_id"]').val();
    	if(ctype == 1){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#item-container-source').show();
    		$('#item-container-source2').hide();
    		$('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		$('#item-container-source2').show();
    		$('#item-container-source').hide();
    	}


    	
    //});

	 //$(document).on('change', 'select[name="new_customer_id[]"]', function() {
    
    	var selected_client = $('select[name="new_customer_id[]"]').val();

    	$.get('/clients/clientinfo', { client_id : selected_client}, function(data){
            console.log(data);
            //console.log(data.HKID);
            //console.log( $('#new_client_main_form').find('.item-container').find('input[name="new_hkid[]"]'));
            
            $('#new_client_main_form').find('.item-container').find('select[name="new_handler_id[]"]').val(data.Incharge);
            $('#new_client_main_form').find('.item-container').find('input[name="new_clientname[]"]').val(data.CompanyName_A1);
            $('#new_client_main_form').find('.item-container').find('input[name="new_hkid[]"]').val(data.HKID);
            $('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').val(data.DATEOFBIRTH);
            $('#new_client_main_form').find('.item-container').find('input[name="new_pob[]"]').val(data.PLACEOFBIRTH);
            $('#new_client_main_form').find('.item-container').find('input[name="new_home_address[]"]').val(data.HOMEADDRESS);
            $('select[name="new_customer_id[]"]').parent().parent().find('select[name="new_nationality_id[]"]').val(data.NATIONALITY);
            $('#new_client_main_form').find('.item-container').find('input[name="new_home_address[]"]').val(data.HOMEADDRESS);
            $('#new_client_main_form').find('.item-container').find('input[name="new_cor_address[]"]').val(data.CORADDRESS);
            $('#new_client_main_form').find('.item-container').find('input[name="new_occupation[]"]').val(data.OCCUPATION);
            $('#new_client_main_form').find('.item-container').find('input[name="new_employer[]"]').val(data.EMPLOYER);
            $('#new_client_main_form').find('.item-container').find('input[name="new_hometel[]"]').val(data.TELNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_homefax[]"]').val(data.FAXNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_offtel[]"]').val(data.OFFICETEL);
            $('#new_client_main_form').find('.item-container').find('input[name="new_mobile[]"]').val(data.MOBILE);
            $('#new_client_main_form').find('.item-container').find('input[name="new_email[]"]').val(data.EMAIL);
            $('#new_client_main_form').find('.item-container').find('input[name="new_contactnum[]"]').val(data.REPNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_personname[]"]').val(data.REPNAME);

            if(data.ISREP==1){
               	$("#new_acting_id").prop('checked',true);
               		$('#addinfo').show();
               }
            else{
               $("#new_acting_id").prop('checked',false);
               	$('#addinfo').hide();
           }


            
        });
    	//$(this).parent().parent().find('select[name="new_customer_id[]"]').val(selected_matter);
    //});

	//$(document).on('change', 'select[name="new_customer_id2[]"]', function() {
	    
	    	var selected_client1 = $('select[name="new_customer_id2[]"').val();

	    	$.get('/clients/clientinfo', { client_id : selected_client1}, function(data){
	            
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_clientcomname[]"]').val(data.CompanyName_A1);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_comnum[]"]').val(data.ComNum);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_poi[]"]').val(data.PLACEOFINC);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_bus_address[]"]').val(data.BUSADD);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_nature[]"]').val(data.Nature_A);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_telnum[]"]').val(data.TELNUM);
	      		$('#new_client_main_form').find('.item-container2').find('input[name="new_faxnum[]"]').val(data.FAXNUM);

	            
	        });
	        var type= $('select[name="new_clienttype_id"]').val();
	        var selected_client=$('#cid').val();
	    	if(type==2){
	        $('.input_fields_wrapdir div').remove();
	        

			$.get('/clients/dirinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_dir[]"]').val(directorylist[0].DIRECTOR_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_dirid[]"]').val(directorylist[0].DIRECTORID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist.length);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapdir').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].DIRECTORID+'" type="director"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_dir[]" value="'+directorylist[index].DIRECTOR_NAME+'"/><input type="hidden" class="form-control" name="new_dirid[]" value="'+directorylist[index].DIRECTORID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapshare div').remove();
			$.get('/clients/shareinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_share[]"]').val(directorylist[0].SHAREHOLDER_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_shareid[]"]').val(directorylist[0].SHAREID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapshare').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].SHAREID+'" type="sharehlder"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_share[]" value="'+directorylist[index].SHAREHOLDER_NAME+'"/><input type="hidden" class="form-control" name="new_shareid[]" value="'+directorylist[index].SHAREID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapben div').remove();
	        $.get('/clients/benowninfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_ben[]"]').val(directorylist[0].OWNER_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_ownid[]"]').val(directorylist[0].OWNERID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapben').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].OWNERID+'" type="owner"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_ben[]" value="'+directorylist[index].OWNER_NAME+'"/><input type="hidden" class="form-control" name="new_ownid[]" value="'+directorylist[index].OWNERID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapperson div').remove();
	        $.get('/clients/repinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_person[]"]').val(directorylist[0].REPNAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personhkid[]"]').val(directorylist[0].HKID);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personconnum[]"]').val(directorylist[0].CONNUM);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personpos[]"]').val(directorylist[0].POSITION);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personemail[]"]').val(directorylist[0].EMAIL);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_repid[]"]').val(directorylist[0].REPID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapperson').append('<div><br><label for="new_person">Name of Person(s) representing the Company </label><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].REPID+'" type="rep"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_person[]" value="'+directorylist[index].REPNAME+'"/><label for="new_personhkid">HKID Card No/Passport No </label><input type="text" class="form-control" name="new_personhkid[]" value="'+directorylist[index].HKID+'"><label for="new_personpos">Capacity Position </label><input type="text" class="form-control" name="new_personpos[]" value="'+directorylist[index].POSITION+'"><label for="new_personconnum">Contact Number </label><input type="text" class="form-control" name="new_personconnum[]" value="'+directorylist[index].CONNUM+'"><label for="new_personemail">Email Address </label><input type="text" class="form-control" name="new_personemail[]" value="'+directorylist[index].EMAIL+'"><input type="hidden" class="form-control" name="new_repid[]" value="'+directorylist[index].REPID+'"/></div>');
		
				}
	           
	        });
			}
	    	//$(this).parent().parent().find('select[name="new_customer_id[]"]').val(selected_matter);
	    //});


	

});
//start
    $('#new_customer_id').selectize({
	    create: true,
	    sortField: 'text'
	});

	$('#new_customer_id2').selectize({
	    create: true,
	    sortField: 'text'
	});


	$('#new_handler_id').selectize({
	    create: true,
	    sortField: 'text'
	});

	
	report_types = [];
	new_client_log_count = 0;

	  /* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };

	var item_container = $('#item-container-source').clone().html();

      new_client_log_count++;

      $('#new_client_main_form').find('.item-container').find('select[name="new_customer_id[]"]').selectize();
      $('#new_client_main_form').find('.item-container2').find('select[name="new_customer_id2[]"]').selectize();

      //get value from input
     
      //$('#new_client_main_form').find('.item-container2').find('select[name="new_customer_id2[]"]').val(id);
      //$('#new_client_main_form').find('.item-container2').find('select[name="new_customer_id1[]"]').val(id);
      
      $(document).on('change', 'select[name="new_customer_id[]"]', function() {
    
    	var selected_client = $(this).val();

    	$.get('/clients/clientinfo', { client_id : selected_client}, function(data){
            console.log(data);
            //console.log(data.HKID);
            //console.log( $('#new_client_main_form').find('.item-container').find('input[name="new_hkid[]"]'));
            
            $('#new_client_main_form').find('.item-container').find('select[name="new_handler_id[]"]').val(data.Incharge);
            $('#new_client_main_form').find('.item-container').find('input[name="new_clientname[]"]').val(data.CompanyName_A1);
            $('#new_client_main_form').find('.item-container').find('input[name="new_hkid[]"]').val(data.HKID);
            $('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').val(data.DATEOFBIRTH);
            $('#new_client_main_form').find('.item-container').find('input[name="new_pob[]"]').val(data.PLACEOFBIRTH);
            $('#new_client_main_form').find('.item-container').find('input[name="new_home_address[]"]').val(data.HOMEADDRESS);
            $('select[name="new_customer_id[]"]').parent().parent().find('select[name="new_nationality_id[]"]').val(data.NATIONALITY);
            $('#new_client_main_form').find('.item-container').find('input[name="new_home_address[]"]').val(data.HOMEADDRESS);
            $('#new_client_main_form').find('.item-container').find('input[name="new_cor_address[]"]').val(data.CORADDRESS);
            $('#new_client_main_form').find('.item-container').find('input[name="new_occupation[]"]').val(data.OCCUPATION);
            $('#new_client_main_form').find('.item-container').find('input[name="new_employer[]"]').val(data.EMPLOYER);
            $('#new_client_main_form').find('.item-container').find('input[name="new_hometel[]"]').val(data.TELNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_homefax[]"]').val(data.FAXNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_offtel[]"]').val(data.OFFICETEL);
            $('#new_client_main_form').find('.item-container').find('input[name="new_mobile[]"]').val(data.MOBILE);
            $('#new_client_main_form').find('.item-container').find('input[name="new_email[]"]').val(data.EMAIL);
            $('#new_client_main_form').find('.item-container').find('input[name="new_contactnum[]"]').val(data.REPNUM);
            $('#new_client_main_form').find('.item-container').find('input[name="new_personname[]"]').val(data.REPNAME);

            if(data.ISREP=='1'){
               	$("#new_acting_id").prop('checked',true);
               		$('#addinfo').show();
               }
            else{
               $("#new_acting_id").prop('checked',false);
               	$('#addinfo').hide();
           }


            
        });
    	//$(this).parent().parent().find('select[name="new_customer_id[]"]').val(selected_matter);
    });

	$(document).on('change', 'select[name="new_customer_id2[]"]', function() {
	    
	    	var selected_client = $(this).val();

	    	$.get('/clients/clientinfo', { client_id : selected_client}, function(data){
	            
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_clientcomname[]"]').val(data.CompanyName_A1);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_comnum[]"]').val(data.ComNum);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_poi[]"]').val(data.PLACEOFINC);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_bus_address[]"]').val(data.BUSADD);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_nature[]"]').val(data.Nature_A);
	            $('#new_client_main_form').find('.item-container2').find('input[name="new_telnum[]"]').val(data.TELNUM);
	      		$('#new_client_main_form').find('.item-container2').find('input[name="new_faxnum[]"]').val(data.FAXNUM);

	            
	        });

	        $('.input_fields_wrapdir div').remove();

			$.get('/clients/dirinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_dir[]"]').val(directorylist[0].DIRECTOR_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_dirid[]"]').val(directorylist[0].DIRECTORID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist.length);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapdir').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].DIRECTORID+'" type="director"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_dir[]" value="'+directorylist[index].DIRECTOR_NAME+'"/><input type="hidden" class="form-control" name="new_dirid[]" value="'+directorylist[index].DIRECTORID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapshare div').remove();
			$.get('/clients/shareinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_share[]"]').val(directorylist[0].SHAREHOLDER_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_shareid[]"]').val(directorylist[0].SHAREID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapshare').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].SHAREID+'" type="sharehlder"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_share[]" value="'+directorylist[index].SHAREHOLDER_NAME+'"/><input type="hidden" class="form-control" name="new_shareid[]" value="'+directorylist[index].SHAREID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapben div').remove();
	        $.get('/clients/benowninfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_ben[]"]').val(directorylist[0].OWNER_NAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_ownid[]"]').val(directorylist[0].OWNERID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapben').append('<div><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].OWNERID+'" type="owner"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_ben[]" value="'+directorylist[index].OWNER_NAME+'"/><input type="hidden" class="form-control" name="new_ownid[]" value="'+directorylist[index].OWNERID+'"/></div>');
		
				}
	           
	        });

			$('.input_fields_wrapperson div').remove();
	        $.get('/clients/repinfo', { client_id : selected_client}, function(data){
	            console.log(data);

	            var index;
				var directorylist=data;
				$('#new_client_main_form').find('.item-container2').find('input[name="new_person[]"]').val(directorylist[0].REPNAME);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personhkid[]"]').val(directorylist[0].HKID);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personconnum[]"]').val(directorylist[0].CONNUM);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personpos[]"]').val(directorylist[0].POSITION);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_personemail[]"]').val(directorylist[0].EMAIL);
				$('#new_client_main_form').find('.item-container2').find('input[name="new_repid[]"]').val(directorylist[0].REPID);

				for (index = 1; index < directorylist.length; ++index) {
				    console.log(directorylist[index]);

				    $('#new_client_main_form').find('.item-container2').find('.input_fields_wrapperson').append('<div><br><label for="new_person">Name of Person(s) representing the Company </label><br><a href="#" class="close pull-right remove_field" uniqueid="'+directorylist[index].REPID+'" type="rep"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_person[]" value="'+directorylist[index].REPNAME+'"/><label for="new_personhkid">HKID Card No/Passport No </label><input type="text" class="form-control" name="new_personhkid[]" value="'+directorylist[index].HKID+'"><label for="new_personpos">Capacity Position </label><input type="text" class="form-control" name="new_personpos[]" value="'+directorylist[index].POSITION+'"><label for="new_personconnum">Contact Number </label><input type="text" class="form-control" name="new_personconnum[]" value="'+directorylist[index].CONNUM+'"><label for="new_personemail">Email Address </label><input type="text" class="form-control" name="new_personemail[]" value="'+directorylist[index].EMAIL+'"><input type="hidden" class="form-control" name="new_repid[]" value="'+directorylist[index].REPID+'"/></div>');
		
				}
	           
	        });
	    	//$(this).parent().parent().find('select[name="new_customer_id[]"]').val(selected_matter);
	    });

	$(document).on('change', 'select[name="new_clienttype_id"]', function() {
    
    	var selected_client = $(this).val();
    	if(selected_client == 1){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#item-container-source').show();
    		$('#item-container-source2').hide();
    		$('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		$('#item-container-source2').show();
    		$('#item-container-source').hide();
    	}


    	
    });

   $(document).on('change', 'input[name="new_acting_id"]', function() {
    
    	var ischecked = $("#new_acting_id").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#addinfo').show();
    		
    		//$('#item-container-source2').hide();
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		
    		$('#addinfo').hide();
    		//$('#item-container-source').hide();
    	}

    	
    });

    /* Delete item */
    $(document).on('click touchstart', '.delete-item', function(e){
        e.preventDefault();
        if(new_matter_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_matter_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });
var matter_filter_data;
var client_filter_data;

$('document').ready(function(e){
    
    getList('dashboard');
    getList('matter');
    getList('client');

    $('[btn-type="pager"]').click(function(e){ 
        e.preventDefault();
        pager($(this).attr('pager-for'), $(this).attr('class')); 
    });

    $('.apply-filter').click(function(e){
        e.preventDefault();
        var type = $(this).attr('filter-for');
        $('input[name="'+type+'_page"]').val(1);
        getList(type);
    });
    
    $('#dash_board_year_office').change(function(e){
        var type = $('.apply-filter').attr('filter-for');
        $('input[name="'+type+'_page"]').val(1);
        getList(type);
    });

    $('[data-toggle="tooltip"]').tooltip();
    
});

function pager(type, action) {
    var page = $('input[name="'+type+'_page"]').val();
    
    if(action == 'next')
        $('input[name="'+type+'_page"]').val(parseInt(page) + 1);
    else 
        $('input[name="'+type+'_page"]').val(parseInt(page) - 1);

    getList(type);
}

function getList(type) {
    var type_plural = type + 's';
    var response_type = 'html';

    if(type == 'dashboard') {
        response_type = 'json';
        $('.dashboard-loader').show();
        $('#dashboard-billed-amount').text('').hide();
        $('#dashboard-unbilled-amount').text('').hide();
        $('#dashboard-total-ar').text('').hide();
        $('.dashboard-date-range').text('').hide();
    } else {
        $('#ajax-loader-'+type_plural).show();
        $('#'+type_plural+'-list-container').css('visibility','hidden');
    }

    $.get('/dashboard/'+type_plural+'-list',getListFilter(type),function(data){
        switch(type) {
            case 'dashboard':
                $('.dashboard-loader').hide();
                $('#dashboard-billed-amount').text(data.billed_total_amount).show();
                $('#dashboard-unbilled-amount').text(data.unbilled_total_amount).show();
                $('#dashboard-total-ar').text(data.total_ar).show()
                $('.dashboard-date-range').text(data.earliest_billing_date + " - " + data.latest_billing_date).show();
                break;
            default:
                $('#ajax-loader-'+type_plural).hide();
                $('#'+type_plural+'-list-container').css('visibility','visible');
                $('#'+type_plural+'-list-container').html(data);
                var total_page = $('#'+type_plural+'-list-container > div[total-page]').attr('total-page');
                $('#'+type+"_total_page").text(total_page);       
                $('.'+type+'-info > li').tooltip();
                break;
        }
    }, response_type);
}

function getListFilter(type) {
    var result = {};
    //result.user_id  = user_id;
    result.user_id  = principal_user;
    result.page     = $('input[name="'+type+'_page"]').val();
    result.sortby   = $('select[name="'+type+'_sortby"]').val();
    result.order    = $('select[name="'+type+'_order"]').val();
    
    switch(type) {
        case 'dashboard':
            result.year = $('select[name="dashboard_year"]').val();
            break;
        case 'matter':
            var folder_types_array = [];
            $('input[name="matter_folder_types"]:checked').each(function(){folder_types_array.push($(this).attr('folder-type'));});

            result.folder_types = folder_types_array.join();
            result.year = $('select[name="matter_year"]').val();

            matter_filter_data = result;
            break;
        case 'client':

            result.year = $('select[name="client_year"]').val();

            client_filter_data = result;       
            break;
    }

    return result;    
}

$('#my_desk_matters').DataTable();

var matter_id;
var table = {};
var filter = {};
    filter.timesheet = {};
var type = 'timesheet';
var timesheet = {};
    timesheet.list = [];
    timesheet.chosen = [];
    //timesheet.items = []; 
var total_invoice_amount = 0; 
var total_disbursements_amount = 0;
var total_less_amount = 0;

var initial_disbursement_amount = 0;
var initial_less_amount = 0;

var form_type = $('#form-type').val();

var selected_handler_id = '';

var first_load = true;

var costs = {

    initial_timesheet_items: [],
    timesheet_items: [],
    handlers: [],
    cost_per_handlers: [],

    set: function(timesheet_list) {
        var self = this;
        this.timesheet_items = [];

        if(this.initial_timesheet_items.length > 0) $.merge(this.timesheet_items, this.initial_timesheet_items);

        this.handlers = [];
        $.each(timesheet_list, function(ndx, val) {
            var costObj = {
                employee_id: val.EmployeeID,
                hourly_rate: val.HourlyRate,
                work_hour: val.WorkHour,
                units: (val.WorkHour * 60) / 5,
                cost: val.WorkHour * val.HourlyRate,
                billed: (typeof val.billed === 'undefined') ? false : true
            };

            if(!$.inArray(val.EmployeeID, self.handlers))
                self.handlers.push(val.EmployeeID)

            self.timesheet_items.push(costObj);
        });
    },

    get: function() {
        var self = this;
        var result = {};

        if(form_type == 'new') {
            $.each(self.timesheet_items, function(ndx, val) {
                if(val.employee_id in result) {
                    result[val.employee_id].units = result[val.employee_id].units + val.units;
                    result[val.employee_id].cost  = result[val.employee_id].cost + val.cost;
                    result[val.employee_id].hourly_rate = val.hourly_rate;
                    result[val.employee_id].total_work_hours = result[val.employee_id].total_work_hours + val.work_hour;
                } else {
                    result[val.employee_id] = {};
                    result[val.employee_id].units = val.units;
                    result[val.employee_id].cost  = val.cost;                        
                    result[val.employee_id].hourly_rate = val.hourly_rate;
                    result[val.employee_id].total_work_hours = val.work_hour;
                }

            });
        } else { //update
            $.each(self.timesheet_items, function(ndx, val) {
                if(val.employee_id in result) {
                    result[val.employee_id].units = result[val.employee_id].units + val.units;
                    result[val.employee_id].cost  = result[val.employee_id].cost + val.cost;
                    result[val.employee_id].hourly_rate = val.hourly_rate;
                    result[val.employee_id].total_work_hours = result[val.employee_id].total_work_hours + val.work_hour;
                } else {
                    result[val.employee_id] = {};
                    result[val.employee_id].units = val.units;
                    result[val.employee_id].cost  = val.cost;                        
                    result[val.employee_id].hourly_rate = val.hourly_rate;
                    result[val.employee_id].total_work_hours = val.work_hour;
                }

            });
        }

        this.cost_per_handlers = result;

        return result;
    },

    update: function(handler_id, data) {
        var self = this;
        $.each(this.cost_per_handlers, function(ndx,val) {
            if(ndx == handler_id) {
                self.cost_per_handlers[handler_id] = data;
                return false;
            }
        });
    },

    total: function() {
        var result = {};
        result.costs = 0;
        result.units = 0;
        $.each(this.cost_per_handlers, function(ndx,val) {
            result.costs = result.costs + parseFloat(val.cost);
            console.log('total costs: ' + result.costs);
            result.units = result.units + parseFloat(val.units);
            console.log('total units: ' + result.units);
        }); 

        this.totals = result;

        return result;
    },

    render: function() {

    },

}; 

$(document).ready(function() {
    if(form_type == 'new' && $('#matter_id').val() !== '') {
    
        $.get('/invoice/all-timesheet', { matter_id : $('#matter_id').val(), billable_unbillable : 1 }, function(data) {
            console.log(data);
            $.each(data, function(ndx,val) {
                timesheet.list.push(val.UniqueID);
            });

            //timesheet.items = data;

            // apply check to timesheet items
            $.each(timesheet.list, function(ndx,val) {
                $('.chkbox-include-item[unique-id="'+val+'"]').prop('checked', true);
            });

            costs.set(data);
            displaySelectedTimesheet(data);
            displayCostSummary(costs.get());
        });
    
    } else if(form_type == 'update') {

        /* Prepare data if page is for update */
       $.get('/invoice/invoice-head', { invoice_number : $('#invoice_number').val() }, function(data) {
            console.log(data);
            $('#matter_id').val(data.invoicehead.matter_id).change();

            $('#reference_number').val(data.invoicehead.reference_number);
            $('#new_invoice_number').val(data.invoicehead.new_invoice_number);

            $('#invoice_batch_number').val(data.invoicehead.BatchNumber);
            $('#invoice_date').val(moment(data.invoicehead.InvoiceDate).format('MMMM DD, YYYY'));
            $('#remarks').val(data.invoicehead.Remarks);

            //console.log(data.timesheet);

            $.each(data.timesheet, function(ndx,val) {
                timesheet.list.push(val.ReferenceID);
            });

            var generated_timesheet_item = [];
            $.each(timesheet_by_handler, function(handler, items) {
                $.each(items, function(ndx, item) {
                    generated_timesheet_item.push(item);
                });
            });

            costs.set(generated_timesheet_item);
            displayCostSummary(costs.get());    

            $.each(timesheet_by_handler, function(handler, items) {
                var total_work_hours = 0;
                var total_units = 0;
                var total_amount = 0;
                $.each(items, function(ndx, item) {
                    if($('.chkbox-include-item[unique-id="'+item.UniqueID+'"]').is(':checked')) {
                        total_work_hours += parseFloat(item.WorkHour);
                        total_units += (parseFloat(item.WorkHour) * 60) / 5;
                        total_amount += (parseFloat(item.WorkHour) * parseFloat(item.HourlyRate));
                    }
                });
                var cost_row = $('.cost-item[handler-id="'+handler+'"');
                $(cost_row).find('input[name="total_work_hours[]"]').val(total_work_hours.toFixed(4));
                $(cost_row).find('input[name="amount[]"]').val(total_amount.formatMoney(2));
                $('#total_cost_amount').text('$' + parseFloat(costs.total().costs).formatMoney(2)); 

                costs.cost_per_handlers[handler].total_work_hours = total_work_hours;
                costs.cost_per_handlers[handler].units = total_units;
                costs.cost_per_handlers[handler].cost = total_amount;

            });                

            //displayInitialTimesheet(data.timesheet);
            //displayInitialCosts(data.cost_by_handler);
            displayInitialDisbursement(data.disbursements);
            displayInitialLess(data.less);

            $('input[name="agreed_cost_amount"]').val(data.invoicehead.agreed_cost || '');

            $('#total_disbursements_amount').text('$' + parseFloat(calculateTotalDisbursements()).formatMoney(2));
            $('#total_less_amount').text('$' + parseFloat(calculateTotalLess()).formatMoney(2));
            calculateInvoiceAmount();

            //return false;       
       });  
      
    }
    $('html,body').scrollTop(0); 
});

$('[data-toggle="tooltip"]').tooltip(); 

if(form_type == 'new') {
    var $matter_selectize = $('select#matter_id').selectize({
        valueField: 'JobAssignmentID',
        labelField: 'JobAssignmentID',
        searchField: ['JobAssignmentID', 'Description_A'],
        options: [],
        create: false,
        render: {
            option: function(item, escape) {
                return '<div><strong>' + item.JobAssignmentID + '</strong> - ' + item.Description_A + '</div>';
            }
        },
        load: function(query, callback) {
            if(!query.length) return callback();
            $.ajax({
                url: '/timesheet/matter-list',
                type: 'GET',
                data: {
                    matter_id_like: query,
                    matter_desc_like: query,
                },
                error: function() {
                    callback();
                },
                success: function(data) {
                    callback(data);
                }
            });
        }
    });
} else {
    //
}

$('#matter_id').on('change', function() {
    matter_id = $(this).val();

    if(form_type == 'new' && $('#clear-matter-id').length == 0) {
        window.location.replace(base_url + '/invoice/create-new?matter_id='+matter_id);
        return false;
    }

    var loading_text = 'Retrieving...';
    $('#matter_description').val(loading_text);
    $('#client_id').val(loading_text);
    $('#client_name').val(loading_text); 

    $.get('/invoice/matter-info', { matter_id : matter_id }, function(data) {
        $('#matter_description').val(data.Description_A);
        $('#client_id').val(data.CustomerID);
        $('#client_name').val(data.CompanyName_A1);
    });

    resetForm();
    calculateInvoiceAmount();
});

$('#invoice_date').datepicker({
    maxDate: '0',
    dateFormat: "MM d, yy"
});
$('#invoice_date').datepicker('setDate', new Date());

$('form').submit(function(e) {
    e.preventDefault();
});

getCustomFilters(type);
table[type] = $('#'+type+'-wrapper').find('table').dataTable(getTableOptions(type));
$('#add-timesheet').click(function(e) {
    e.preventDefault(); 
    //showTimesheetModal('');
});

function showTimesheetModal(handler_id) {
    if($('#matter_id').val() == '') {
        toastr.error('No Matter ID selected');
        return;
    }
    //table[type].DataTable().clear();
    timesheet.chosen = [];
    selected_handler_id = (handler_id !== '') ? handler_id : '';
    getCustomFilters(type);
    table[type]._fnDraw();
    $($('#add-timesheet').attr('data-target')).modal('show');    
}

/* Add disbursement */
$('#add-disbursement').click(function(e) {
    e.preventDefault();
    if($('#matter_id').val() == '') {
        toastr.error('No Matter ID selected');
        return;
    }    

    var disbursement_row = $('#reference-table').find('.disbursement-item').clone();
    $('#no_disbursement_msg').hide();
    $(disbursement_row).appendTo('#disbursement-items');

    regenerateItemNumbers('disbursement');
    calculateInvoiceAmount();
});

/* Add less */
$('#add-less').click(function(e) {
    e.preventDefault();
    if($('#matter_id').val() == '') {
        toastr.error('No Matter ID selected');
        return;
    } 

    var less_row = $('#reference-table').find('.less-item').clone();
    $('#no_less_msg').hide();
    $(less_row).appendTo('#less-items');
    regenerateItemNumbers('less');
    calculateInvoiceAmount();
});

function getTableOptions(type) {
    var options = {};
    options.sPaginationType = 'simple_numbers';
    options.bProcessing = true;
    options.lengthMenu = [[12,24,36,-1],["12","24","36","All"]];
    options.sAjaxSource = base_url + "/timesheet/dt-"+type;
    options.bServerSide = true;
    options.columnDefs = getColumnDefs(type);
    options.oLanguage = { sProcessing: global_ajax_loader_bar };

    switch(type) {
        case 'timesheet':
            options.fnServerParams = function(aoData) {
                aoData.push( { "name" : "matter_id", "value" : filter[type]['matter_id'] } );
                aoData.push( { "name" : "unbilled_only", "value" : filter[type]['unbilled_only'] } );
                aoData.push( { "name" : "from_date", "value" : filter[type]['from_date'] } );
                aoData.push( { "name" : "to_date", "value" : filter[type]['to_date'] } );
                aoData.push( { "name" : "for_invoice", "value" : 1 } );
                if(selected_handler_id !== '')
                    aoData.push( { "name" : "handler_id", "value" : filter[type]['handler_id'] } );
            };
            options.fnRowCallback = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                console.log(aData);
                var tmp_array = arrayMergeUnique(timesheet.list,timesheet.chosen);
                if($.inArray(aData[6], tmp_array) >= 0) {
                    $(nRow).addClass('green-background');
                    $(nRow).find('input[type="checkbox"]').prop('checked', true);
                } else {
                    $(nRow).addClass('red-background');
                }
            };           
            break;

    }

    return options;
}

function getCustomFilters(type) {

    switch(type) {
        case 'timesheet':
            filter.timesheet.matter_id = $('#matter_id').val();
            filter.timesheet.unbilled_only = true;
            filter.timesheet.from_date = $('#from_date').val();
            filter.timesheet.to_date = $('#to_date').val();
            
            if(selected_handler_id !== '')
                filter.timesheet.handler_id = selected_handler_id; 
            
            break;
    }

}

function getColumnDefs(type) {
    var result = [];
    switch(type) {        
        case 'timesheet':
            result.push({
                'targets': [0,5,6,8,9],
                'visible': false,
                'searchable': false
            });

            result.push({
                'targets': [2],
                'render': function(data, type, row) {
                    return parseFloat(data.toString()).toFixed(2);
                }
            });  

            result.push({
                'targets': [7],
                'render': function(data, type, row) {
                    return moment(data).format('MMMM D, YYYY');
                }
            }); 

            /*
            result.push({
                'targets': [9],
                'className': 'text-center',
                'render': function(data, type, row) {
                    return '<input type="checkbox" class="timesheet-item" unique-id="'+row[5]+'">';
                }
            });
            */ 

            result.push({
                'targets': [10],
                'className': 'text-center td-chkbox',
                'render': function(data, type, row) {
                    return '<input type="checkbox" class="chk-add-timesheet-item">';
                }
            });


            break;
    }

    return result;
}

/* Date Range Picker */
$( "#from_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
  }
});
/* /Date Range Picker */

//$('#timesheet-wrapper tbody').on('click','tr',function() { //working
$(document).on('change', '.chk-add-timesheet-item', function() {
    var this_row = $(this).closest('tr'); 
    if(!$(this_row).find('td:first').hasClass('dataTables_empty')) {
        var ndx = $(this_row).index();
        var row_data = table[type].DataTable().row(ndx).data();
        $(this_row).toggleClass('green-background');
        $(this_row).toggleClass('red-background');
        if($(this_row).hasClass('green-background')) {                
            timesheet.chosen.push(row_data[6]);
        } else {
            timesheet.list = $.grep(timesheet.list, function(value){
                return value != row_data[6];
            });            
            timesheet.chosen = $.grep(timesheet.chosen, function(value){
                return value != row_data[6];
            });
        } 
    }
});

/* Close Modal */
$('#invoice-timesheet').on('hidden.bs.modal', function() {
    //toastr.info('Modal closed');
    timesheet.chosen = [];
    $.post('/timesheet/list', { timesheet_id : timesheet.list }, function(data) {
        console.log(data);
        costs.set(data);
        displaySelectedTimesheet(data);
        displayCostSummary(costs.get());
    });
});

/* Timesheet - on Add */
$('#add-to-invoice').click(function(e) {
    e.preventDefault();
    timesheet.list = arrayMergeUnique(timesheet.list,timesheet.chosen);
    $('#invoice-timesheet').modal('hide');
    timesheet.chosen = [];
    //console.log(timesheet.list);
});

/* Remove Timesheet Item */
$(document).on('click', '.remove-timesheet', function(e) {
    e.preventDefault();
    var uniqueId = $(this).attr('unique-id');
    $(this).closest('tr').remove();
    timesheet.list = $.grep(timesheet.list, function(value){
        return value != uniqueId;
    });
    if($('#timesheet-items .timesheet-item').length == 0) 
        $('#no_timesheet_msg').show();
    regenerateItemNumbers('timesheet');
    calculateInvoiceAmount();

    $.post('/timesheet/list', { timesheet_id : timesheet.list }, function(data) {
        costs.set(data);
        displayCostSummary(costs.get());
    });
});

/* Remove Disbursement Item */
$(document).on('click', '.remove-disbursement', function(e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    if($('#disbursement-items .disbursement-item').length == 0) 
        $('#no_disbursement_msg').show();
    regenerateItemNumbers('disbursement');
    $('#total_disbursements_amount').text('$' + parseFloat(calculateTotalDisbursements()).formatMoney(2));
    calculateInvoiceAmount();
});

/* Remove Less Item */
$(document).on('click', '.remove-less', function(e) {
    e.preventDefault();
    $(this).closest('tr').remove();
    if($('#less-items .less-item').length == 0) 
        $('#no_less_msg').show();
    regenerateItemNumbers('less');
    $('#total_less_amount').text('$' + parseFloat(calculateTotalLess()).formatMoney(2));
    calculateInvoiceAmount();
});

/* Disbusement Amount - on change */
/*
$(document).on('keyup change', '#disbursement-items .amount', function() {
    var amount = $(this).val();
    if(!$.isNumeric(amount)) {
        $(this).val(0);
    }
    calculateInvoiceAmount();
});
*/

function displaySelectedTimesheet(data) {
    if(form_type == 'new') 
        $('#timesheet-items tr.timesheet-item').remove();

    var ctr = $('#timesheet-items .timesheet-item').length;    
    $.each(data, function(ndx,val) {
        var timesheet_row = $('#reference-table').find('.timesheet-item').clone();
        $(timesheet_row).attr('number', ctr+1);
        $(timesheet_row).find('.remove-timesheet').attr('unique-id', val.UniqueID);
        $(timesheet_row).find('.item-number').text(ctr+1);
        $(timesheet_row).find('.description').text(val.Comment);
        var amount = val.WorkHour * val.HourlyRate;
        $(timesheet_row).find('.amount').text(amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        $(timesheet_row).appendTo('#timesheet-items');
        ctr++;
    });

    if(data.length > 0 || form_type == 'update') 
        $('#no_timesheet_msg').hide();
    else
        $('#no_timesheet_msg').show();

    calculateInvoiceAmount();
}

function displayCostSummary(data) {
    $('#cost-items tr.cost-item').remove();

    $.each(data, function(ndx,val) {
        var cost_row = $('#reference-table').find('.cost-item').clone();
        $(cost_row).attr('handler-id', ndx);
        $(cost_row).find('.btn-show-timesheet').attr('employee-id', ndx);
        $(cost_row).find('input[name="handler[]"]').val(ndx);
        
        $(cost_row).find('input[name="hourly_rate[]"]').val(parseFloat(val.hourly_rate).toFixed(2));
        
        $(cost_row).find('input[name="total_work_hours[]"]').val(parseFloat(val.total_work_hours).toFixed(4));
        $(cost_row).find('input[name="amount[]"]').val(parseFloat(val.cost).formatMoney(2));
        $(cost_row).appendTo('#cost-items');
    });

    $('#total_cost_amount').text('$' + parseFloat(costs.total().costs).formatMoney(2)); 
    calculateInvoiceAmount();
}

function displayInitialTimesheet(data) { // for update
    $.each(data, function(ndx,val) {
        var timesheet_row = $('#reference-table').find('.timesheet-item').clone();
        $(timesheet_row).attr('number', ndx+1);
        $(timesheet_row).find('.remove-timesheet').remove();
        $(timesheet_row).find('.item-number').text(ndx+1);
        $(timesheet_row).find('.description').text(val.Description);
        $(timesheet_row).find('.amount').text(parseFloat(val.Amount).formatMoney(2));
        $(timesheet_row).appendTo('#timesheet-items');

        costs.initial_timesheet_items.push({
            employee_id : val.EmployeeID,
            hourly_rate : parseFloat(val.HourlyRate),
            work_hour   : parseFloat(val.WorkHour),
            units       : (parseFloat(val.WorkHour) * 60) / 5,
            cost        : parseFloat(val.HourlyRate) * parseFloat(val.WorkHour)
        });

    });
    if(data.length > 0) 
        $('#no_timesheet_msg').hide();
    else
        $('#no_timesheet_msg').show();
    //calculateInvoiceAmount();
}

function displayInitialCosts(data) { // for update
    costs.cost_per_handlers = {};
    $.each(data, function(ndx,val) {
        costs.cost_per_handlers[val.handler_id] = {
            hourly_rate: parseFloat(val.hourly_rate),
            total_work_hours: parseFloat(val.total_work_hours),
            units: val.total_units,
            cost: parseFloat(val.amount)
        };

        var cost_row = $('#reference-table').find('.cost-item').clone();
        $(cost_row).find('.btn-show-timesheet').attr('employee-id', val.handler_id);
        $(cost_row).find('input[name="handler[]"]').val(val.handler_id);
        $(cost_row).find('input[name="hourly_rate[]"]').val(val.hourly_rate);
        $(cost_row).find('input[name="total_work_hours[]"]').val(parseFloat(val.total_work_hours).toFixed(4));
        $(cost_row).find('input[name="complimentary[]"]').prop('checked', val.complimentary);
        $(cost_row).find('input[name="amount[]"]').val(parseFloat(val.amount).formatMoney(2));
        $(cost_row).appendTo('#cost-items');
    });

    costs.total();
    $('#total_cost_amount').text('$' + parseFloat(costs.totals.costs).formatMoney(2));
}

function displayInitialDisbursement(data) { // for update
    $('#no_disbursement_msg').hide();
    $.each(data, function(ndx,val) {
        var disbursement_row = $('#reference-table').find('.disbursement-item').clone();
        disbursement_row.find('.remove-disbursement').remove();
        disbursement_row.find('input[name="description[]"]').val(val.description).prop('disabled', true);
        disbursement_row.find('input[name="amount[]"]').val(val.amount).prop('disabled', true);

        initial_disbursement_amount += parseFloat(val.amount);

        $(disbursement_row).appendTo('#disbursement-items');

        regenerateItemNumbers('disbursement');
        //calculateInvoiceAmount();        
    });

    //if(data.length == 0) $('#no_disbursement_msg').show();
}

function displayInitialLess(data) { // for update
    $('#no_less_msg').hide();
    $.each(data, function(ndx,val) {
        var less_row = $('#reference-table').find('.less-item').clone();
        less_row.find('.remove-less').remove();
        less_row.find('input[name="description[]"]').val(val.description).prop('disabled', true);
        less_row.find('input[name="amount[]"]').val(val.amount).prop('disabled', true);

        initial_less_amount += parseFloat(val.amount);

        $(less_row).appendTo('#less-items');

        regenerateItemNumbers('less');
        //calculateInvoiceAmount();        
    });

    //if(data.length == 0) $('#no_disbursement_msg').show();
}

function regenerateItemNumbers(item_type) {
    var item_rows = $('#'+item_type+'-items .'+item_type+'-item');
    $.each(item_rows, function(ndx,item_row) {
        $(item_row).attr('number', ndx+1);
        $(item_row).find('.item-number').text(ndx+1);
    });
}

/* Reset Form and Variables */
function resetForm() {
    timesheet.list = [];
    timesheet.chosen = [];
    $('#timesheet-items .timesheet-item').remove();
    $('#no_timesheet_msg').show();
    $('#disbursement-items .disbursement-item').remove();
    $('#no_disbursement_msg').show();
    $('#no_less_msg').show();
    $('#remarks').val('');
    $('#from_date').val('');
    $('#to_date').val('');
    calculateInvoiceAmount();
}

/* Calculate Total Invoice Amount */
function calculateInvoiceAmount() {
    total_invoice_amount = 0;
    var agreed_cost = $('input[name="agreed_cost_amount"]').val();
    /*    
    var timesheet_rows = $('#timesheet-items .timesheet-item');
    $.each(timesheet_rows, function(ndx,timesheet_row) {
        var amount = $(timesheet_row).find('.amount').text();
        total_invoice_amount = total_invoice_amount + parseFloat(amount.replace(',',''));
    });
    */

    if(agreed_cost == '')
        total_invoice_amount = total_invoice_amount + parseFloat(costs.total().costs || 0);
    else
        total_invoice_amount = total_invoice_amount + parseFloat(agreed_cost);

    var disbursement_rows = $('#disbursement-items .disbursement-item');
    $.each(disbursement_rows, function(ndx,disbursement_row) {
        var amount = $(disbursement_row).find('.amount').val();
        total_invoice_amount = total_invoice_amount + parseFloat(amount || 0);
    });

    // Subtract from less
    total_invoice_amount = total_invoice_amount - total_less_amount;

    $('#total-invoice-amount').text('$'+total_invoice_amount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
}

/* Calculate Total Costs (handlers) */
function calculateTotalCosts(obj) {
    var parent_row = $(obj).closest('.cost-item');
    var handler_id = parent_row.find('input[name="handler[]"]').val();
    var data = {};
    if($(obj).attr('name') == 'hourly_rate[]') {
        data.hourly_rate = parseFloat($(obj).val());
        data.total_work_hours = parseFloat(parent_row.find('input[name="total_work_hours[]"]').val());
    }
    if($(obj).attr('name') == 'total_work_hours[]') {
        data.total_work_hours = parseFloat($(obj).val());
        data.hourly_rate = parseFloat(parent_row.find('input[name="hourly_rate[]"]').val());
    }    

    data.units = parseFloat(data.total_work_hours / 5);
    data.cost = parseFloat(data.hourly_rate * data.total_work_hours);

    costs.update(handler_id, data);
    //parent_row.find('input[name="amount[]"]').val(parseFloat(costs.cost_per_handlers[handler_id].cost).formatMoney(2));
    console.log(costs.cost_per_handlers);
    $('#total_cost_amount').text('$' + parseFloat(costs.total().costs).formatMoney(2)); 
    calculateInvoiceAmount();
}

/* Timesheet Filter */
$('#daterange-form').submit(function(e) {
    e.preventDefault();
    var temp = $(this).serializeObject();
    console.log(temp);
    getCustomFilters(type);
    table[type]._fnDraw();
});

/* Get Disbursement Items */
function getDisbursements() {
    var disbursements = [];
    var disbursement_rows;
    if(form_type == 'new')
        disbursement_rows = $('#disbursement-items tr.disbursement-item');
    else
        disbursement_rows = $('#disbursement-items tr.disbursement-item').find('.remove-disbursement').closest('tr');
    $.each(disbursement_rows, function(ndx, item_row) {
        var row_data = {};
        row_data.description = $(item_row).find('.description').val();
        row_data.amount = $(item_row).find('.amount').val();
        disbursements.push(row_data);
    });
    return disbursements;
}

/* Get Less Items */
function getLess() {
    var less = [];
    var less_rows;
    if(form_type == 'new')
        less_rows = $('#less-items tr.less-item');
    else
        less_rows = $('#less-items tr.less-item').find('.remove-less').closest('tr');
    $.each(less_rows, function(ndx, item_row) {
        var row_data = {};
        row_data.description = $(item_row).find('.description').val();
        row_data.amount = $(item_row).find('.amount').val();
        less.push(row_data);
    });
    return less;    
}

/* Save Invoice */
function saveInvoice() {
    var invoice_data = $('#invoice-form').serializeObject();
    invoice_data['timesheet_list'] = timesheet.list;
    invoice_data['costs_by_handlers'] = costs.cost_per_handlers;
    invoice_data['disbursements'] = getDisbursements();
    invoice_data['less'] = getLess();
    invoice_data['totals'] = {
        costs: costs.totals.costs,
        disbursements: total_disbursements_amount,
        less: total_less_amount,
        invoice: total_invoice_amount
    }
    console.log(invoice_data);
    
    $.post('/invoice/save', invoice_data, function(data) {
        console.log(data);
        if(data.status == 'success') {
            toastr.success(data.data);
            var delay = 3000; //Your delay in milliseconds
            setTimeout(function(){ window.location = $('#save-invoice').attr('redirect-url'); }, delay);
        } else {
            $.each(data.error_msgs, function(ndx,val) {
                toastr.error(val);
            });
        }
    });
    
}

/* Invoice Form - on submit */
$('#invoice-form').submit(function(e) {
    e.preventDefault();
    saveInvoice();
});

/* Event when cost's hourly and/or work hours are changed */
$(document).on('keyup change', 'input[name="hourly_rate[]"], input[name="total_work_hours[]"]', function() {
   
    calculateTotalCosts(this);

});




/* Complimentary */
$(document).on('change', 'input[name="complimentary[]"]', function() {
    var parent_row = $(this).closest('.cost-item');
    var handler_id = parent_row.find('input[name="handler[]"]').val();
    var data = costs.cost_per_handlers[handler_id];

    if($(this).is(':checked')) {
        data.cost = parseFloat(0);
    } else {
        data.cost = parseFloat(data.hourly_rate * data.total_work_hours);
    }

    costs.update(handler_id, data);
    parent_row.find('input[name="amount[]"]').val(parseFloat(costs.cost_per_handlers[handler_id].cost).formatMoney(2));
    $('#total_cost_amount').text('$' + parseFloat(costs.total().costs).formatMoney(2)); 
    calculateInvoiceAmount();

});

/* Calculate Total Disbursements */
function calculateTotalDisbursements() {
    var disbursement_items = getDisbursements();
    var total = 0;

    if(form_type == 'update') total += initial_disbursement_amount;

    $.each(disbursement_items, function(ndx,val) {
        total += parseFloat(val.amount || 0);
    });

    total_disbursements_amount = total;

    return total;
}
$(document).on('keyup change', '.disbursement-item input[name="amount[]"]', function() {
    console.log('key up!');
    $('#total_disbursements_amount').text('$' + parseFloat(calculateTotalDisbursements()).formatMoney(2));
    calculateInvoiceAmount();
});

/* Calculate Total Less */
function calculateTotalLess() {
    var less_items = getLess();
    var total = 0;

    if(form_type == 'update') total += initial_less_amount;

    $.each(less_items, function(ndx,val) {
        total += parseFloat(val.amount || 0);
    });

    total_less_amount = total;

    return total;  
}
$(document).on('keyup change', '.less-item input[name="amount[]"]', function() {
    console.log('key up!');
    $('#total_less_amount').text('$' + parseFloat(calculateTotalLess()).formatMoney(2));
    calculateInvoiceAmount();
});

/* Agreed Costs */
$(document).on('keyup change', 'input[name="agreed_cost_amount"]', function() {
    calculateInvoiceAmount();
});

/* Show timesheet per handler */
$(document).on('click', '.btn-show-timesheet', function(e) {
    e.preventDefault();
    var employee_id = $(this).attr('employee-id');
    var this_row = $(this).closest('tr');
    //$('#add-timesheet').click();
    //showTimesheetModal(employee_id);
    if($(this_row).next().hasClass('handler-timesheet')) {
        $(this_row).next().toggle();
    } else {
        $('#reference-table').find('.handler-timesheet[handler-id="'+employee_id+'"]')
            .insertAfter(this_row);
    }
});

/* Event when timesheet item checkbox is checked/unchecked */
$(document).on('change', '.chkbox-include-item', function() {
    var timesheet_id = $(this).attr('unique-id');
    var this_row = $(this).closest('tr.handler-timesheet');
    var handler_id = $(this_row).attr('handler-id');
    console.log($(this).is(':checked'));
    if( $(this).is(':checked') ) {
        timesheet.list.push(timesheet_id);
    } else {
        timesheet.list = $.grep(timesheet.list, function(value) {
            return value != timesheet_id;
        });
    }

    /*
    var data = $.grep(timesheet.items, function(elem) {
        return $.inArray(elem.UniqueID, timesheet.list);
    });
    */

    $.post('/timesheet/list', { timesheet_id : timesheet.list.join() }, function(data) {
        console.log(data);
        costs.set(data);
        costs.get();
        //displaySelectedTimesheet(data);
        //displayCostSummary(costs.get());

        var total_work_hours = 0;
        var total_amount = 0;

        if(typeof costs.cost_per_handlers[handler_id] !== 'undefined') {
                total_work_hours = costs.cost_per_handlers[handler_id].total_work_hours;
                total_amount = costs.cost_per_handlers[handler_id].cost;
        }

        $(this_row).prev().find('input[name="total_work_hours[]"]').val(parseFloat(total_work_hours).toFixed(4));
        $(this_row).prev().find('input[name="amount[]"]').val(parseFloat(total_amount).formatMoney(2));
        calculateTotalCosts($(this_row).find('input[name="hourly_rate[]"]'));
    });

});

$('#print-preview').click(function(e) {
    if(form_type == 'new') {
        e.preventDefault();
        toastr.error('You must save before previewing the invoice.');
    }
});

function printpage(url) {
  child = window.open(url, "", "height=300, width=300");  //Open the child in a tiny window.
  window.focus();  //Hide the child as soon as it is opened.
  $(child).ready(function() {
      child.print();  //Print the child.
      child.close();  //Immediately close the child.
  });
}

$('#clear-matter-id').click(function(e) {
    e.preventDefault();
    window.location.replace(base_url + '/invoice/create-new');
    return false;
});
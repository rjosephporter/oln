var report_types = ['timesheet'];

var table = {};

var filter = {};
    filter.timesheet = {};

var new_timesheet_log_count = $('.item-container').length;

getCustomFilters('timesheet');

$('#matter_id').selectize({
    valueField: 'JobAssignmentID',
    labelField: 'JobAssignmentID',
    searchField: ['JobAssignmentID', 'Description_A'],
    options: [],
    create: false,
    render: {
        option: function(item, escape) {
            return '<div><strong>' + item.JobAssignmentID + '</strong> - ' + item.Description_A + '</div>';
        }
    },
    load: function(query, callback) {
        if(!query.length) return callback();
        $.ajax({
            url: '/timesheet/matter-list',
            type: 'GET',
            data: {
                matter_id_like: query,
                matter_desc_like: query,
            },
            error: function() {
                callback();
            },
            success: function(data) {
                callback(data);
            }
        });
    }
});

$('#handler_id').selectize();

//$('#timesheet_date').datepicker({maxDate: '0'});

/* Date Range Picker */
$( "#from_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
  }
});
/* /Date Range Picker */

$('#print-report').click(function(e) {
    e.preventDefault();
    window.print();
});


$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip();

    $.each(report_types, function(ndx, report_type) {
        table[report_type] = $('#'+report_type+'-list-wrapper').find('table').dataTable(getTableOptions(report_type));        
    });

    $('#matter_id, #handler_id, #from_date, #to_date').change(function(){
        console.log('trigger change');
        var type = $(this).attr('filter-for');
        getCustomFilters(type);
        table[type]._fnDraw();

        var date;
        if(moment().format('MM/DD/YYYY') == $('#from_date').val())
            date = 'Today';
        else 
            date = moment($('#from_date').val(), 'MM/DD/YYYY').format('MMMM D, YYYY');

        var matter_id_text;
        if($('#matter_id').val() == '')
            matter_id_text = 'All Matters';
        else
            matter_id_text = 'Matter ID: ' + $('#matter_id').val();

        $('#date_text').text(date);
        $('#matter_id_text').text(matter_id_text);

        setTimeout(function() {
            $('#text-handler').text('');
            $('#text-handler').text($('#handler_id').next().find('.selectize-dropdown-content').find('.selected').text());
            $('#text-from-date').text($('#from_date').val());
            $('#text-to-date').text($('#to_date').val());
        }, 100);
    });

    /* Add new */
    $('#btn_add_new').bind('click touchstart', function(){
        if($('#matter_id').val() && $('#timesheet_date').val()) {
            $('input[name="new_matter_id"').val($('#matter_id').val());
            $('input[name="new_timesheet_date"').val($('#date_text').text());
            $('#new_timesheet_log').modal('show');
        } else {
            toastr.error('Please enter a valid Matter ID and Date.');
        }
    });

    /* Add item */
    var item_container = $('.item-container').clone().wrap('<div/>').parent().html();
    $('#btn_add_item').bind('click touchstart', function(e){
        e.preventDefault();
        $('#new_timesheet_main_form').append(item_container).focus();
        new_timesheet_log_count++;
    });

    /* Delete item */
    $(document).on('click touchstart', '.delete-item', function(e){
        e.preventDefault();
        if(new_timesheet_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_timesheet_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });


    /* Action */
    var popOverSettings = {
        placement: 'left',
        html: true,
        selector: '.btn-delete-log',
        content: function () {
            return 'Delete this item? <button class="btn btn-danger btn-sm btn-confirm-delete">Yes</button> <button class="btn btn-default btn-sm btn-cancel-delete">No</button>';
        }
    }
    $('body').popover(popOverSettings);
    $(document).on('click', '.btn-cancel-delete', function(){
        $(this).parent().parent().parent().find('.btn-delete-log').popover('hide');
    });
    $(document).on('click', '.btn-confirm-delete', function(){
        var delete_log_btn = $(this).parent().parent().parent().find('.btn-delete-log');
        var unique_id = delete_log_btn.attr('unique-id');
        
        //console.log(unique_id);
        //toastr.info('Delete: ' + unique_id);

        $.post('/timesheet/delete', { timesheet_id : unique_id }, function(data) {
            toastr[data.status](data.message);
            if(data.status == 'success') table.timesheet._fnDraw();
        });


        delete_log_btn.popover('hide');
    });
    /*
    $(document).on('click', '.btn-update-log', function(){
        var unique_id = $(this).attr('unique-id');
        var workhours = $(this).closest('tr').find('td:nth-child(3)').text();
        var description = $(this).closest('tr').find('td:nth-child(5)').text();
        var modal = $('#update_timesheet_log');
        modal.find('input[name="update_uniqueid"]').val(unique_id);
        modal.find('input[name="update_workhour"]').val(workhours);
        modal.find('textarea[name="update_description"]').val(description);
    });
    */
    $(document).on('click', '.btn-update-log', function(){
        var unique_id = $(this).attr('unique-id');
        var modal = $('#update_timesheet_log');
        var ndx = $(this).closest('tr').index();
        var row_data = table.timesheet.DataTable().row(ndx).data();

        console.log(row_data);

        /*
        $.get('timesheet/has-job-assignment-billing', { timesheet_id : unique_id }, function(data) {
            modal.find('input[name="new_billable_checkbox"]').prop('checked', data.found);
            modal.find('input[name="new_billable"]').val(data.found ? 1 : 0);
        });
        */
        
        modal.find('input[name="timesheet_id"]').val(unique_id);
        selectize_matter[0].selectize.setValue(row_data[2]);
        modal.find('input[name="new_units"]').val(row_data[5]);
        modal.find('input[name="new_workhour_round"]').val(row_data[4].toFixed(2));
        modal.find('input[name="new_workhour"]').val(row_data[4]);
        //selectize_timecode[0].selectize.setValue(row_data[10]);
        modal.find('input[name="new_timesheet_date"]').val(moment(row_data[0]).format('MMMM D, YYYY'));
        selectize_handler[0].selectize.setValue(row_data[1]);
        modal.find('textarea[name="new_description"]').val(row_data[3]);
    });    

    /* Save New Timesheet */
    $('#new_timesheet_log form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

        $.post('/timesheet/submit-new', $(this).serializeArray(), function(data){
            console.log(data);
            if(data.status == 'success') {
                toastr.success(data.msg);
                clearForm('new_timesheet_log');
                //table['timesheet']._fnDraw();
                table['timesheet'].fnPageChange('last');
                $('#new_timesheet_log').modal('toggle');
            } else {
                toastr.error(data.msg);
            }
        });

    });

    $('.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate').addClass('hidden-print');

    table.timesheet.DataTable().on('draw', function() {
        var timesheet_table = table.timesheet.DataTable();
        var workhours_array = timesheet_table.column(4).data();
        var units_array = timesheet_table.column(5).data();

        var total = {
            workhours: 0,
            units: 0
        }
        $.each(workhours_array, function(ndx,val) {
            total.workhours += val;
        });
        $.each(units_array, function(ndx,val) {
            total.units += val;
        });        

        $(this).find('tfoot').remove();
        $(this).append('<tfoot>' +
            '<tr>' +
                '<td colspan="4" class="text-right"><strong>TOTAL</strong></td>' +
                '<td><strong>'+total.workhours.toFixed(2)+'</strong></td>' +
                '<td><strong>'+total.units.toFixed()+'</strong></td>' +
                '<td></td>' +
                '<td class="hidden-print"></td>' +
            '</tr>' +
        '</tfoot>');
    });

    $('#text-handler').text('');
    $('#text-handler').text($('#handler_id').next().find('.selectize-dropdown-content').find('.selected').text());
    $('#text-from-date').text($('#from_date').val());
    $('#text-to-date').text($('#to_date').val());    

}); 

function getTableOptions(type) {
    var options = {};
    options.sPaginationType = 'simple_numbers';
    options.bProcessing = true;
    options.lengthMenu = [[12,24,36,-1],["12","24","36","All"]];
    options.sAjaxSource = base_url + "/timesheet/dt-"+type;
    options.bServerSide = true;
    options.columnDefs = getColumnDefs(type);
    options.oLanguage = { sProcessing: global_ajax_loader_bar };

    switch(type) {
        case 'timesheet':
            options.fnServerParams = function (aoData) {
                aoData.push( { "name": "matter_id", "value": filter.timesheet.matter_id} );
                aoData.push( { "name": "handler_id", "value": filter.timesheet.handler_id} );
                aoData.push( { "name": "from_date", "value": filter.timesheet.from_date} );
                aoData.push( { "name": "to_date", "value": filter.timesheet.to_date} );
                aoData.push( { "name": "view_type", "value": filter.timesheet.view_type} );
            };
            options.iDisplayLength = -1;
            options.order = [[0, 'desc']];
            break;
    }

    return options;
}  

function getCustomFilters(type) {

    switch(type) {
        case 'timesheet':
            filter.timesheet.matter_id = $('#matter_id').val();
            filter.timesheet.handler_id = $('#handler_id').val();
            filter.timesheet.from_date = $('#from_date').val();
            filter.timesheet.to_date = $('#to_date').val();
            filter.timesheet.view_type = $('#view_type').val();
            break;
    }

}

function getColumnDefs(type) {
    var result = [];
    switch(type) {
        case 'timesheet':

            result.push({
                    'targets': [4],
                    'render': function(data, type, row) {
                        return parseFloat(data.toString()).toFixed(2);
                    }
                });
            result.push({
                    'targets': [5],
                    'render': function(data, type, row) {
                        return parseFloat(data.toString()).toFixed();
                    }
                });            
           
            result.push({
                    'targets': [10],
                    'className': 'text-center hidden-print',
                    'render': function(data, type, row) {
                        console.log(row);
                        if(row[9] == 1 && row[7] == 1)
                            return '<button class="btn btn-info btn-xs btn-update-log" unique-id="'+row[8]+'" data-toggle="modal" data-target="#update_timesheet_log"><i class="glyphicon glyphicon-edit"></i></button>&nbsp;' +  
                                   '<button class="btn btn-danger btn-xs btn-delete-log" unique-id="'+row[8]+'"><i class="glyphicon glyphicon-trash"></i></button>';
                        else
                            return '<i class="glyphicon glyphicon-lock"></i>';
                    }
                });

            result.push({
                    'targets': [7,8,9],
                    'visible': false,
                    'searchable': false
                });
            
            result.push({
                'targets': [0,6],
                'render': function(data,type,row) { 
                    //return moment(data).format('MMMM D, YYYY');
                    return moment(data).format('DD-MMM-YYYY');
                }
            });

            result.push({
                'targets': [0,1,2,4,5],
                'width': '10%',
            });
            result.push({
                'targets': [3],
                'width': '50%',
            });
            break;
    }

    return result;
}

function clearForm(form_type) {
    switch(form_type) {
        case 'new_timesheet_log':
            $('.item-container').slice(1).remove();
            $('.item-container input,.item-container textarea,.item-container select').val('');
            new_timesheet_log_count = 1;
            break;
    }
}

/****** Added 09.25.2014 ********/

var selectize_matter = $('.item-container').find('select[name="new_matter_id"]').selectize();
var selectize_timecode = $('.item-container').find('select[name="new_timecode"]').selectize();
var selectize_handler = $('.item-container').find('select[name="new_handler"]').selectize();

$(document).on('change', 'select[name="new_handler"]', function() {
    var selected_handler = $(this).val();
    $(this).parent().parent().find('select[name="new_hourlyrate"]').val(selected_handler);
});

$('#update_timesheet_form').submit(function(e) {
    e.preventDefault();
    console.log($(this).serializeObject());
    var form_data = $(this).serializeObject();
    //$.post('/timesheet/update', form_data, function(data) {
    $.post('/timesheet/update2', form_data, function(data) {        
        console.log(data);
        if(data.status == 'success') {
            toastr.success(data.message);
            $('#update_timesheet_log').modal('hide');
            table.timesheet._fnDraw();
        } else {
            toastr.error(data.message);
        }
    });
});

var datepicker_options = {
    maxDate : '0',
    dateFormat: "MM d, yy",
};

$('.item-container').find('input[name="new_timesheet_date"]').datepicker(datepicker_options);

$(document).on('change', 'input[name="new_billable_checkbox"]', function() {
    if($(this).is(":checked")) {
        $(this).closest('.form-group').find('input[name="new_billable"]').val('1');
    } else {
        $(this).closest('.form-group').find('input[name="new_billable"]').val('0');
    }
});

$(document).on('change keyup', 'input[name="new_units"]', function() {
    var units = parseInt($(this).val());
    var work_hour = (units * 5) / 60;
    $('input[name="new_workhour_round"]').val(work_hour.toFixed(2));
    $('input[name="new_workhour"]').val(work_hour);
});

var invoice_type = ['invoice', 'invoicedetail'];
var table = {};
var filter = {};
	filter.invoice = {};
	filter.invoicedetail = {};
var current_view = 'invoice_list';
var current_invoice_total = 0;
var current_disbursement_total = 0;

$('#client_id').selectize({
});
$('#invoice_date').datepicker();
$('#btn-back').click(function(){
	$('#invoice-wrapper, #invoicedetail-wrapper, #page-title').toggle('fade');
	//$('#breadcrumb').css('visibility','hidden');
	$('#breadcrumb').toggle('fade');
	$('#batch_text').text('');
	$('#daterange-wrapper').toggle('fade');

	current_view = 'invoice_list';
});

$('#invoice-wrapper tbody').on('mouseenter','tr',function() { $(this).addClass('active'); });
$('#invoice-wrapper tbody').on('mouseleave','tr',function() { $(this).removeClass('active'); });

//$('#invoice-wrapper tbody').on( 'click', 'tr', function () {
function displayInvoiceDetail(row_index) {
	//var row_data = table['invoice'].fnGetData($(this).context._DT_RowIndex);

	$('#disbursement-total').text('');
	$('#cost-disb-total').text('');

	var row_data = table['invoice'].fnGetData(row_index);

	console.log(row_data);

	var invoice_id = row_data[1];
	var batch_id = row_data[4];
	$('#invoice-wrapper, #invoicedetail-wrapper, #page-title').toggle('fade');
	$('#breadcrumb').toggle('fade');
    console.log(row_data);
    filter.invoicedetail.invoice_id = invoice_id;
    table['invoicedetail']._fnDraw();
    //$('#batch_text').text(invoice_id);

    $('#invoice_detail_number').text(invoice_id);
    $('#invoice_detail_client').text(row_data[6]);

    $.get('/invoice/matter-info', { matter_id : row_data[11]}, function(data) {
		$('#invoice_detail_matter').text(data.Description_A);
		$('#invoice_detail_handler').text(data.Incharge + ' - ' + data.NickName);
    });

    $('#disbursement-items').html('');
    current_disbursement_total = 0;
    $.get('/invoice/disbursements', { invoice_id : invoice_id }, function(data) {
    	$.each(data.disbursements, function(ndx,val) {
    		$('#disbursement-items').append(
    			'<tr>' +
    			'<td>'+val.item_number+'</td>' +
    			'<td>'+val.description+'</td>' +
    			'<td>$'+parseFloat(val.amount).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</td>' +
    			'</tr>'
    		);
    		current_disbursement_total = parseFloat(current_disbursement_total) + parseFloat(val.amount);
    	});
    	
    	$('#disbursement-total').text( '$' + current_disbursement_total.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );

    	$.get('/invoice/invoice-detail-total', { invoice_id : invoice_id }, function(data) {
    		current_invoice_total = data.total;
	    	var cost_disb_total = parseFloat(current_invoice_total) + parseFloat(current_disbursement_total);
	    	$('#cost-disb-total').text( '$' + cost_disb_total.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") );    		
    	});
    });

    $('#invoice_detail_date').text(moment(row_data[7]).format('MMMM D, YYYY'));

    $('#daterange-wrapper').toggle('fade');

    current_view = 'invoice_detail';
} 
//);

/* Date Range Picker */
$( "#from_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#to_date" ).datepicker( "option", "minDate", selectedDate );
  }
});
$( "#to_date" ).datepicker({
  defaultDate: "+1w",
  changeMonth: true,
  numberOfMonths: 3,
  dateFormat: "MM d, yy",
  onClose: function( selectedDate ) {
    $( "#from_date" ).datepicker( "option", "maxDate", selectedDate );
  }
});
/* /Date Range Picker */

/* Date Range Form Action */
$('#daterange-form').submit(function(e) {
	e.preventDefault();
	//console.log($('#from_date').val());
	//console.log($('#to_date').val());
	getCustomFilters('invoice');
	table['invoice']._fnDraw();
});
/* /Date Range Form Action */

$.each(invoice_type, function(ndx, type) {
	getCustomFilters(type);
	table[type] = $('#'+type+'-wrapper').find('table:first').dataTable(getTableOptions(type));
});

function getTableOptions(type) {
	var options = {};
	options.sPaginationType = 'simple_numbers';
	options.bProcessing = true;
	options.lengthMenu = [[12,24,36,-1],["12","24","36", "All"]];
	options.sAjaxSource = base_url + "/invoice/dt-"+type;
	options.bServerSide = true;
	options.columnDefs = getColumnDefs(type);
	options.oLanguage = { sProcessing: global_ajax_loader_bar };

	switch(type) {
		case 'invoice':
			options.fnServerParams = function(aoData) {

				$.each(filter.invoice, function(ndx, val) {
					aoData.push( { name : ndx, value : val } );
				});
				/*
				aoData.push( { "name" : "client", "value" : filter[type]['client_id'] } );
				aoData.push( { "name" : "from_date", "value" : filter[type]['from_date'] } );
				aoData.push( { "name" : "to_date", "value" : filter[type]['to_date'] } );
				*/
			}
			options.order = [[7, 'asc']];

			options.fnRowCallback = function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				console.log(aData);
				$(nRow).addClass(aData[12]+'-background');
			};	
			options.drawCallback = function(settings) {
      			var table_data = settings.json.aaData;
      			var matters = [];
      			$.each(table_data, function(ndx, val) {
      				matters.push(val[11]);
      			});

      			var tableObj = table.invoice.DataTable();
      			$.get('/invoice/matter-invoice-balance', { matter_id_array : matters }, function(data) {
      				//var tableObj = table.invoice.DataTable();
      				$.each(settings.aiDisplay, function(ndx,val) {
      					var row_data = tableObj.row(val).data();
      					row_data[15] = '$'+data[row_data[11]].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'&nbsp;<a href="#" class="label label-info view-related-invoice" matter-id="'+row_data[11]+'" title="View related invoices"><i class="fa fa-info"></i> </a>';
      					tableObj.row(val).data(row_data);
      				});
      			});

      			if(current_view == 'invoice_related') {
      				var arr = tableObj.column(10).data();
      				var raw_amount = sumArray(arr).toFixed(2);
      				$('#matter-total-invoice').text('$'+raw_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      			}
    		}		
			break;

		case 'invoicedetail':
			options.fnServerParams = function(aoData) {
				aoData.push( { "name" : "invoice_id", "value" : filter[type]['invoice_id'] } );
			}
			options.order = [[1, 'asc']];
			options.iDisplayLength = -1;
			options.bLengthChange = false;
			options.bPaginate = false;
			options.bInfo = false;
			options.bFilter = false;
            options.drawCallback = function(settings) {
                console.log(settings.json);
               	var total_amount = '';
               	if(settings.json.total_amount) {               		
               		//current_invoice_total = settings.json.total_amount;
               		total_amount = '$' + settings.json.total_amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
               	}
                $('#invoicedetail-wrapper table:first').append('<tr><th colspan="2" class="text-right">Total:</th><th>'+total_amount+'</th></tr>');
            };			
			break;
	}

	return options;
}

function getCustomFilters(type) {

    switch(type) {
        case 'invoice':
            filter.invoice.client_id = 'all';
            filter.invoice.from_date = $('#from_date').val();
            filter.invoice.to_date = $('#to_date').val();
            filter.invoice.status = $('#status').val();
            filter.invoice.matter_id = $('#matter_id').val();
            break;

        case 'invoicedetail':
            filter.invoicedetail.invoice_id = 'all';
            break;
    }

}

function getColumnDefs(type) {
	var result = [];
	switch(type) {
		case 'invoice':
			result.push({
				'targets': [0],
				'render': function(data,type,row) {
					return (data) ? 'OEMS' : 'MBA';
				}
			});

			result.push({
				'targets': [1,4,9,11,12],
				'visible': false,
				'searchable': false
			});

			result.push({
				'targets': [7],
				'render': function(data,type,row) { 
	                return moment(data).format('MMMM D, YYYY');
	            }
			});

            result.push({
	            'targets': [13,14],
	            'render': function(data,type,row) { 
	            	var text = (data == null) ? '0.00' : data;
	                return '$' + text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ''; 
	            }
            });

            result.push({
            	'targets': [10],
            	'render': function(data,type,row) {
            		return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '&nbsp;<a href="#" class="label label-info view-invoice-detail" title="View invoice items"><i class="fa fa-info"></i> </a>'; 
            	}
            });

            result.push({
            	'targets': [17],
            	'className': 'text-center',
            	'render': function(data,type,row) {
            		return '<a href="/invoice/send-reminder/'+row[1]+'" target="_blank" class="btn btn-warning btn-xs btn-send-reminder"><i class="fa fa-envelope"></i></a>' + 
            				' <a href="/invoice/update2/'+row[1]+'" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>' +
            				' <a href="/invoice/print/'+row[1]+'" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-print"></i></a>';
            	}
            });

			break;

		case 'invoicedetail':
			result.push({
				'targets': [0],
				'visible': false,
				'searchable': false
			});

			result.push({
				'targets': [1,3],
				'width': '20%',
			});			

			result.push({
				'targets': [2],
				'width': '60%',
			});			
			
            result.push({
	            'targets': [3],
	            'render': function(data,type,row) { 
	                return '$' + data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
	            }
            });	
            	
			break;
	}

	return result;
}

$(document).on('click', '.view-invoice-detail', function(e) {
	e.preventDefault();
	var clicked_row = $(this).closest('tr');
	console.log(clicked_row);
	var row_index = clicked_row[0]._DT_RowIndex;
	displayInvoiceDetail(row_index);
});

$(document).on('click', '.view-related-invoice', function(e) {
	e.preventDefault();
	var matter_id = $(this).attr('matter-id');
	$('#matter_id').val(matter_id);

	$('#matter_id_text').text('for Matter ' + matter_id);

	getCustomFilters('invoice');
	//table['invoice']._fnDraw();
	table.invoice.DataTable().page.len(-1).draw();

	//var total_invoice_array = table.invoice.DataTable().column(7).data();
	var total_matter_balance = $(this).closest('td').text();
	$('#matter-total-invoice').text('Loading...');
	$('#matter-total-balance').text(total_matter_balance);	

	$('#matter-info-wrapper').toggle();
	table.invoice.DataTable().column([13,15]).visible(false);
	current_view = 'invoice_related';
});

$('#btn-back-related').click(function(e) {
	e.preventDefault();
	$('#matter_id').val('');
	$('#matter_id_text').text('');

	getCustomFilters('invoice');
	//table['invoice']._fnDraw();
	table.invoice.DataTable().page.len(12).draw();

	$('#matter-info-wrapper').toggle();
	table.invoice.DataTable().column([13,15]).visible(true);
	current_view = 'invoice_list';	
});

/* Reminder */
/*
$(document).on('click', '.btn-send-reminder', function(e) {
	e.preventDefault();
	$('#invoice-reminder-batch-number').text($(this).attr('invoice-number'));
	$('#reminder_message').val('Hello '+$(this).attr('client-name')+',\n\nThis email is to remind you that you still have a pending balance of '+$(this).attr('balance')+' for Invoice Number '+$(this).attr('invoice-number')+' issued last '+$(this).attr('invoice-date')+'.\n\nThanks!');
});
*/

$('#invoice-reminder-form').submit(function(e) {
	e.preventDefault();
	var post_data = $(this).serializeObject();

	$.post('/invoice/send-reminder', post_data, function(data) {
		console.log(data);

		$('#invoice-reminder-modal').modal('hide');
	});
});

var all_matters = {};
var matter_infos = [];
var fceval = {CustomerID: '', Incharge: ''};
var timeline_tmpl = "";
var billing_tmpl = "";
var matter_id = 0;
var open_tmpl = "";
var open_tmpl_body = "";

function get_info(id_){
	var new_data = {id: id_, info: {},tl:{}, bh:{}, Incharge:''};
	$.get('/matters/matter/'+ id_, function(data){
		new_data.info = data;
		$.get('/matters/introducer/' + data[0].CustomerID , function(info){
			new_data.Incharge=info[0].Incharge;
			$.get('/matters/bh/'+ id_, function(info){
				new_data.bh = info;
				$.get('/matters/tl/'+ id_, function(info){
					new_data.tl = info;
					matter_infos.push(new_data);
					display_info(new_data);
				},'json');
			},'json');
		},'json');
	},'json');
	
}

function display_info(obj){
	fceval.CustomerID = ("00000" + obj.info[0].CustomerID).substr(-5,5);
	fceval.Incharge = obj.info[0].Incharge;
	$('#_body_billing').html($.tmpl(billing_tmpl,obj.bh));
        $('#_matter_timeline_ul').html('');
	var cl_i ={class:'',badge:' success '};
	for(var i = 0; i < obj.tl.length; i++){
		var cl = i == 0 ? cl_i : (obj.tl[i].WorkDate == obj.tl[i-1].WorkDate ? cl : (cl.class.length == 0 ? {class: ' class=timeline-inverted ', badge:' info '} : cl_i));
		var subj = obj.tl[i].Comment ? obj.tl[i].Comment.substring(0,12) : '';
		var comm = obj.tl[i].Comment ? obj.tl[i].Comment : '';
		$.tmpl(timeline_tmpl,{cls: cl.class, badge: cl.badge, Incharge: obj.tl[i].EmployeeID, datetime: obj.tl[i].WorkDate, info: comm}).appendTo('#_matter_timeline_ul');
	}
	$.tmpl(timeline_tmpl,{cls: '', badge: ' warning ', Incharge: obj.tl[0].Incharge, datetime: obj.tl[0].DesignatedDate, info: obj.tl[0].Description_A}).appendTo('#_matter_timeline_ul');
        var target_matter = $.grep(all_matters,function(elem){return elem.JobAssignmentID == matter_id;});
        $('._modal_content__').html(open_tmpl_body);
        $('._matter_timeline_ul').html($('#_matter_timeline_ul').html());
        $('._body_billing').html($('#_body_billing').html())
        $('.m_current_modal_title').html('Matter No: ' + obj.info[0].JobAssignmentID + '<br>' + obj.info[0].Description_A);
	$('._active_matter_subject').val(obj.info[0].Description_A);
	$('.active_created_job_date').val($.datepicker.formatDate('MM dd, yy', new Date(obj.info[0].DesignatedDate)));
	$('.active_job_assignment_id').val(obj.info[0].JobAssignmentID);
	$('._matters_clients_').val(("00000" + obj.info[0].CustomerID).substr(-5,5) + " - " + obj.info[0].CompanyName_A1);
	$('._matters_handlers_').val(obj.info[0].Incharge);
	$('._matters_employee_').val(obj.Incharge);
        $('#billing_table').dataTable({
            order : [ 5, 'desc' ], pagingType: "full", pageLength : 5, orderClasses : true, bLengthChange : false, bFilter : false
	});
	$('.r_billable_total').html('$' + parseFloat(target_matter[0].billable_amount).formatMoney(2));
	$('.r_billed_total').html('$' + parseFloat(target_matter[0].billed_amount).formatMoney(2));
	$('.r_unbilled_total').html('$' + parseFloat(target_matter[0].unbilled).formatMoney(2));
}

function pM(_id){
    $('#openmatter').html(open_tmpl);
    $("#openmatter").modal().show();
    setTimeout(function(){
        matter_id = _id;
        var check = $.grep(matter_infos,function(elem){
            return elem.id == _id;
        });
        if(check.length)
            display_info(check[0]);
        else
            get_info(_id);
    },1000);
}

$(function(){
    $.get("/assets/thisapp/tmpl/_dis_billing.tmpl",function(tmpl){
        billing_tmpl=tmpl;
        $.get("/assets/thisapp/tmpl/_dis_timeline.tmpl",function(tmpl){
            timeline_tmpl=tmpl;
            $.get("/matters/mattersdetailslisting", function(data){
                all_matters = data;
                $.get('/assets/thisapp/tmpl/open_matter.tmpl',function(tmpl){
                    open_tmpl = tmpl;
                    $.get('/assets/thisapp/tmpl/open_matter_body.tmpl',function(tmpl){
                        open_tmpl_body = tmpl;
                        $('div[data-toggle="tooltip"]').tooltip();
                    });
                });
            },"json");
        });
    }); 
});
var conference_room_reservations;
var selectedD, selectedDT;
/* Init new variables */
var datepicker_options = {
      //maxDate : '0',
       startDate: selectedD,
       minDate:selectedD,
      dateFormat: 'yy-mm-dd',
      timeFormat: 'hh:mm:ss',
     allowTimes:[
   '9:00','10:00','11:00','12:00', '13:00','14:00','15:00', '16:00',
  '17:00', '18:00', '19:00', '20:00', '21:00','22:00','23:00',
  ]
};
var d = new Date();
//console.log(d);
var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
var dstring2= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
//console.log("dstring "+dstring);
//console.log(getUrlVars()["seldate"]==dstring);
//$('#new_conference_room_reservation_main_form').find('input[name="new_reservation_date[]"]').datepicker(datepicker_options);
selectedD=  (getUrlVars()["seldate"] == undefined ? dstring : getUrlVars()["seldate"]);
selectedDT=  ((getUrlVars()["seldate"] == undefined || getUrlVars()["seldate"]==dstring) ? 'today' : getUrlVars()["seldate"]);
$('#datetimepicker2').val(selectedD); 
$('#datetimepicker3').val(selectedD);//big textbox for selecting date when you see the office


//datepicker when you click on a conference room
/*$('#datetimepicker').datetimepicker({
  dateFormat: 'yy-mm-dd',
  timeFormat: 'hh:mm:ss',
  inline:true,
  closeOnDateSelect:true,
  allowTimes:[
  '9:00','10:00','11:00','12:00', '13:00','14:00','15:00', '16:00',
  '17:00', '18:00', '19:00', '20:00', '21:00','22:00','23:00',
  ],
  pick12HourFormat: true,
  minDate:selectedD,
  startDate: selectedD,
  onSelectTime: function(dateText) {
            //alert(date);
             //var delay = 3000; //Your delay in milliseconds
             //window.location.href = '{{ URL::to('/conference-room') }}'; 
              //window.open('/conference-room/?seldate='+date,"_self");
             // console.log($('#new_conference_room_reservation_main_form1').find('input[name="new_conference_id1"]').val());
              var startDt = $('#datetimepicker').datetimepicker('getDate');
              var startti = $('#datetimepicker').datetimepicker('getTime');
              //console.log(dateText);
               //console.log(startti);
              
            //dateAsObject = $('#datetimepicker').formatDate('mm-dd-yy', new Date(startDt));
            var startDt=getFormattedDate(dateText);
            var startTime=getFormattedTime(dateText);
            $('#new_start_time').val(startDt+" "+startTime);
            $('#new_end_time').val(startDt+" "+startTime);
            $('#reservationtime').val(startDt+" "+startTime);
             //$("#complete_confirmation1").modal('hide');
             $("#complete_confirmation2").modal().show();
             //$('#calendar').show();
        }
}); */

/*$('#datetimepicker2').datetimepicker( {
     dateFormat: 'dd-mm-yy',
     timepicker:false,
     closeOnDateSelect:true,
   
        onClose: function(date) {
        
             console.log($('#new_start_time').val);
             console.log( $('#conferenceid').val());
             var startDt=getFormattedDate(date);
             $('#datetimepicker2').val(startDt);
             //var delay = 3000; //Your delay in milliseconds
             //window.location.href = '{{ URL::to('/conference-room') }}'; 
              window.open('/conference-room/?seldate='+ startDt,"_self");
            
        }
});

$('#datetimepicker3').datetimepicker( {
     dateFormat: 'dd-mm-yy',
     timepicker:false,
     closeOnDateSelect:true,
   
        onClose: function(date) {
        
             console.log($('#new_start_time').val);
             console.log( $('#conferenceid').val());
             var startDt=getFormattedDate(date);
             $('#datetimepicker3').val(startDt);
             //var delay = 3000; //Your delay in milliseconds
             //window.location.href = '{{ URL::to('/conference-room') }}'; 
              window.open('/office/?seldate='+ startDt,"_self");
            
        }
});

function getFormattedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear().toString();
    return year + '/' + month + '/' + day;
}

function getFormattedTime(date) {
    var time = ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);
    
    return time;
}



/*$.get('/conference-room/conference-room-reservations', { dateitem :  selectedD}, function(data) {
            //alert(selectedD);
            conference_room_reservations = data.conference_rooms;
            var textattr        = textattr = { 'font-size': 8, stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
            console.log(data);
            var purpose_length = {
                1 : 20,
                2 : 2,
                3 : 15
            }    
            for (var i = 0; i < conferencerooms.length; i++) {
                var conference_room_name;
                $.each(conference_room_reservations, function(ndx, val) {
                    if(val.id == cfrm_info[conferencerooms[i].data('id')]['conference_room_id']) {
                        conference_room_name = val.name;
                        cfrm_info[conferencerooms[i].data('id')]['name'] = conference_room_name;
                        cfrm_info[conferencerooms[i].data('id')]['reservations'] = val.reservation;
                    }
                });

                conferencerooms[i].mouseover(function(e){
                    this.node.style.opacity = 0.7;
                    document.getElementById('room-name').innerHTML = cfrm_info[this.data('id')]['name'];
                    //document.getElementById('room-name').innerHTML = cfrm_info[conferencerooms[i].data('id')]['reservations'];   
                     
                });

                conferencerooms[i].mouseout(function(e){
                    this.node.style.opacity = 1;
                    document.getElementById('room-name').innerHTML = "&nbsp;";
                });

                //conferencerooms[i].attr("href","/conference-room/calendar?roomid="+cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                conferencerooms[i].click(function() { showDate(cfrm_info[this.data('id')]); });
                    //clickReservation(cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                
                labelPath(conferencerooms[i], conference_room_name);
                displayReservations(conferencerooms[i], cfrm_info[conferencerooms[i].data('id')]['reservations'], purpose_length[cfrm_info[conferencerooms[i].data('id')]['conference_room_id']],cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                //addImage(conferencerooms[i]);
            }
        });
        
*/


/*for (var i = 0; i < partners.length; i++) {
    partners[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = partner_info[this.data('id')]['name'] + " -- " + partner_info[this.data('id')]['employee_id'];
    });

    partners[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    //console.log(partner_info[partners[i].data('id')]['employee_id']);

    labelPath(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    addImage(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    
    //var item = partners[i];
    //console.log($('#'+item.data('id')));
    //addText[$('#'+item.data('id')),partner_info[item.data('id')]['name']];
}

for (var i = 0; i < associates.length; i++) {
    associates[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = associate_info[this.data('id')]['name'] + " -- " + associate_info[this.data('id')]['employee_id'];
    });

    associates[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    labelPath(associates[i], associate_info[associates[i].data('id')]['employee_id']);
    if(associate_info[associates[i].data('id')]['employee_id'] !== "") addImage(associates[i], associate_info[associates[i].data('id')]['employee_id']);
}

for (var i = 0; i < pas.length; i++) {
    pas[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = pa_info[this.data('id')]['name'] + " -- " + pa_info[this.data('id')]['employee_id'];
    });

    pas[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    labelPath(pas[i], pa_info[pas[i].data('id')]['employee_id']);
    //addImage(pas[i]);
}*/


function addText(p,text)
{
    var t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    var b = p.getBBox();
    t.setAttribute("transform", "translate(" + (b.x + b.width/2) + " " + (b.y + b.height/2) + ")");
    t.textContent = text;
    t.setAttribute("fill", "text");
    t.setAttribute("font-size", "14");
    p.parentNode.insertBefore(t, p.nextSibling);
}

function labelPath( pathObj, text, textattr )
{
    //var textObj='';
    if ( textattr == undefined )
        textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 400 };
    var bbox = pathObj.getBBox();
    //var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + bbox.height / 2, text ).attr( textattr );
    var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + 5, text ).attr( textattr );

    return textObj;
}

function addImage(pathObj)
{
    var bbox = pathObj.getBBox();
//    console.log(pathObj.data('id'));
//    console.log(bbox);
    
    var green_icon      = pathObj.paper.image('/assets/thisapp/images/trans.png',        bbox.x + bbox.width / 16, bbox.y + (bbox.height * 1 / 5) - 10, 60, 60).attr({ cursor : 'pointer' }).click(function() { showDate(); });
    

    return true;
}

function displayReservations(pathObj, reservations, purpose_length,confid)
{

    var bbox = pathObj.getBBox();
    //var reservation_text='';
    var textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
    //pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * 1 / 5) - 5, '' || 0 );
    if(reservations.length > 0) {
        $.each(reservations, function(ndx,val) {
            //pathObj.paper.text='';
            var item_number = ndx + 1;
            if(item_number<5){
            var purpose_text = (val.purpose.length > purpose_length) ? val.purpose.substr(0, purpose_length) + '...' : val.purpose;
            var time = ((val.start_time.length+val.end_time.length) > purpose_length) ? val.start_time + ' - ' + val.end_time.substr(0, 4) + '...' : val.start_time + ' - ' + val.end_time;
            //reservation_text = val.reservation_date + ' '+ time + ' \nPurpose: ' + purpose_text;
            reservation_text = val.reservation_date + ' '+ time + ' \n - Booked';
            var new_textattr = textattr;
            //new_textattr['title'] = val.purpose;
            new_textattr['title'] =  '- Booked';
            pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * item_number / 5) , reservation_text || 0 ).attr( new_textattr ).click(function() { clickReservation(confid) });
            }
        });
    } else {

        pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * 1 / 5) - 5, 'No reservations for \n'+selectedDT || 0 ).attr( textattr ).click(function() { clickReservation(confid) });
    }
}

function getHeight (width) {
  var value = width;
  value *= 1;
  var valueHeight = Math.round((value/16)*9);
  
  return valueHeight;
}

function calculateSize() {
    var width = $('.panel-body').width();
    var height = getHeight(width);
    rsr.setSize(width,height);
}

$(window).resize(calculateSize);


/**
 * On click event for handler by folder types
 *
 * @param   handler_id      e.g. GDO
 * @param   folder_type     possible values (green, orange, red, untouched, blue)
 */
function clickFolder(handler_id, folder_type) {
    var type = 0;
    switch(folder_type){
        case 'green': type = 1; break;
        case 'orange': type = 2; break;
        case 'red' : type = 3; break;
        case 'blue': type = 4; break;
        case 'untouched': type = 5; break; 
    }
    window.open("/matters/view/" + type + "/all/" + handler_id);
}

function clickReservation(roomid) {
    //console.log('here');
    //$('#datetimepicker').daterangepicker().show();
     $('#conferenceid').val(roomid);
      $("#complete_confirmation1").modal().show();
      //$('calendar').show();
}

function showDate(roomid) {
     //console.log(roomid.conference_room_id);
     //$('#datetimepicker').daterangepicker().show();
      $('#conferenceid').val(roomid.conference_room_id);
     
      $("#complete_confirmation1").modal().show();
      //$("#calendar").show();
}


function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

/* Added 09.29.2014 */
//labelPath(path_je, '\nPartner');
//labelPath(path_jf, '\nAssociate');
//labelPath(path_jg, '\nPA');
//labelPath(path_jh, '\nConference\nRoom');
//labelPath(path_ji, '\nGeneral Area');
//labelPath(path_jj, '\nWalkway');

var w = 1200;
var h = 705;

var rsr = Raphael('floorplan');

rsr.setViewBox(0, 0, w, h, true);

calculateSize();

rsr.canvas.setAttribute('preserveAspectRatio', 'none');

var partners = [];
var associates = [];
var pas = [];
var walkways = [];
var everythingelse = [];
var conferencerooms = [];

var path_av = rsr.path("m181.15 319.45v210.09").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.99929 0 0 .98644 0 6.587").data('id', 'path_av');
var path_aw = rsr.path("m681.87 529.54v-131.31h122.55v21.59").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.99929 0 0 .96791 0 12.332").data('id', 'path_aw');

var path_ax = rsr.path("m216.17 266.92v-133.05").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#000',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ax');

var path_ay = rsr.path("m104.12 322.95h45.52v-56.03").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ay');
var path_az = rsr.path("m104.12 473.51h45.52v56.03").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_az');
var path_ba = rsr.path("m104.12 438.5h45.52v-52.52h-45.52").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ba');

var path_bb = rsr.path("m16.581 443.75v-3.502h85.79l-85.79 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bb');
var path_bc = rsr.path("m16.581 443.75l85.79-3.502 3.502 3.502h-89.29").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bc');
var path_bd = rsr.path("m105.87 443.75l-3.502-3.502v-57.775l3.502 61.28").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bd');
var path_be = rsr.path("m105.87 443.75l-3.502-61.28h3.502v61.28").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_be');
var path_bf = rsr.path("m16.581 356.21v-3.501h87.54l-87.54 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bf');
var path_bg = rsr.path("m16.581 356.21l87.54-3.501v3.501h-87.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bg');
var path_bh = rsr.path("m16.581 268.68v-3.502h171.57l-171.57 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bh');
var path_bi = rsr.path("m16.581 268.68l171.57-3.502v3.502h-171.57").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bi');
var path_bj = rsr.path("m181.15 321.2v-3.501h152.31l-152.31 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bj');
var path_bk = rsr.path("m181.15 321.2l152.31-3.501-3.501 3.501h-148.81").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bk');
var path_bl = rsr.path("m329.96 321.2l3.501-3.501v211.84l-3.501-208.34").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bl');
var path_bm = rsr.path("m329.96 321.2l3.501 208.34h-3.501v-208.34").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bm');

var path_bn = rsr.path("m105.87 529.54h-3.502v-59.52l3.502 59.52").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bn');
var path_bo = rsr.path("m105.87 529.54l-3.502-59.52h3.502v59.52").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bo');
var path_bp = rsr.path("m331.71 443.75v-3.502h129.56l-129.56 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bp');
var path_bq = rsr.path("m331.71 443.75l129.56-3.502v3.502h-129.56").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bq');
var path_br = rsr.path("m333.83 321.13v-3.501h157.57l-157.57 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_br');
var path_bs = rsr.path("m333.22 320.83l157.57-3.501v3.501h-157.57").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bs');
var path_bt = rsr.path("m489.28 268.68v-3.502h50.771l-50.771 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bt');
var path_bu = rsr.path("m489.28 268.68l50.771-3.502v3.502h-50.771").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bu');
var path_bv = rsr.path("m802.67 133.87h3.501v200.46l-3.501-200.46").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bv');
var path_bw = rsr.path("m802.67 133.87l3.501 200.46-3.501-3.502v-196.96").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bw');
var path_bx = rsr.path("m802.67 330.83l3.501 3.502h-52.52l49.02-3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bx');
var path_by = rsr.path("m802.67 330.83l-49.02 3.502v-3.502h49.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_by');
var path_bz = rsr.path("m569.82 266.92h-3.501v-133.06l3.501 133.06").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_bz');
var path_ca = rsr.path("m569.82 266.92l-3.501-133.06h3.501v133.06").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ca');
var path_cb = rsr.path("m529.55 301.94h3.501v94.54l-3.501-94.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cb');
var path_cc = rsr.path("m529.55 301.94l3.501 94.54-3.501 3.502v-98.04").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cc');
var path_cd = rsr.path("m529.55 399.98l3.501-3.502h115.55l-119.05 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cd');
var path_ce = rsr.path("m529.55 399.98l119.05-3.502-3.502 3.502h-115.55").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ce');
var path_cf = rsr.path("m645.1 399.98l3.502-3.502v87.54l-3.502-84.04").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cf');
var path_cg = rsr.path("m645.1 399.98l3.502 84.04h-3.502v-84.04").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cg');
var path_ch = rsr.path("m1079.28 809.66h-3.5v-42.02l3.5 42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ch');
var path_ci = rsr.path("m1079.28 809.66l-3.5-42.02h3.5v42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ci');
var path_cj = rsr.path("m891.96 531.29v-3.502h110.29l-110.29 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cj');
var path_ck = rsr.path("m891.96 531.29l110.29-3.502v3.502h-110.29").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ck');
var path_cl = rsr.path("m1014.51 671.35v-3.502h157.56l-157.56 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cl');
var path_cm = rsr.path("m1014.51 671.35l157.56-3.502v3.502h-157.56").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cm');

var path_cn = rsr.path("m487.53 133.87h3.501v135.39l-3.501-135.39").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '0.84',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cn');
var path_co = rsr.path("m487.53 133.87l3.501 135.11h-3.501v-135.11").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '0.84',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_co');

var path_cp = rsr.path("m645.1 265.08h3.502v67.5l-3.502-67.5").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '0.58',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cp');
var path_cq = rsr.path("m645.1 264.98l3.502 67.6h-3.502v-67.6").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '0.58',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cq');

var path_cr = rsr.path("m257.09 266.87v-3.502h205.3l-205.3 3.502").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '2.01',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cr');
var path_cs = rsr.path("m257.09 266.87l205.3-3.502v3.502h-205.3").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '2.01',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cs');

var path_ct = rsr.path("m1077.53 739.63h28.89").attr({"vector-effect": 'non-scaling-stroke',stroke: '#000',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ct');
var path_cu = rsr.path("m1077.53 599.57h28.89").attr({"vector-effect": 'non-scaling-stroke',stroke: '#000',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cu');

var path_cv = rsr.path("m1172.07 737.88v3.502h-94.54l94.54-3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cv');
var path_cw = rsr.path("m1172.07 737.88l-94.54 3.502v-3.502h94.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cw');
var path_cx = rsr.path("m1172.07 597.82v3.502h-94.54l94.54-3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cx');
var path_cy = rsr.path("m1172.07 597.82l-94.54 3.502v-3.502h94.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cy');
var path_cz = rsr.path("m891.96 618.82v-3.501h96.29l-96.29 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_cz');
var path_da = rsr.path("m891.96 618.82l96.29-3.501-3.502 3.501h-92.79").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_da');
var path_db = rsr.path("m984.74 618.82l3.502-3.501v96.29l-3.502-92.79").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_db');
var path_dc = rsr.path("m984.74 618.82l3.502 92.79h-3.502v-92.79").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dc');
var path_dd = rsr.path("m986.5 737.88v3.502h-94.54l94.54-3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dd');
var path_de = rsr.path("m986.5 737.88l-94.54 3.502v-3.502h94.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_de');
var path_df = rsr.path("m1079.28 571.55h-3.5v-42.02l3.5 42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_df');
var path_dg = rsr.path("m1079.28 571.55l-3.5-42.02h3.5v42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dg');
var path_dh = rsr.path("m1079.28 711.61h-3.5v-84.04l3.5 84.04").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dh');
var path_di = rsr.path("m1079.28 711.61l-3.5-84.04h3.5v84.04").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_di');

var path_dj = rsr.path("m939.23 582.06v-52.52").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dj');
var path_dk = rsr.path("m891.96 582.06h94.54v-52.52").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dk');

var path_dl = rsr.path("m1033.77 531.29v-3.502h138.3l-138.3 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dl');
var path_dm = rsr.path("m1033.77 531.29l138.3-3.502v3.502h-138.3").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dm');
var path_dn = rsr.path("m105.87 326.45h-3.502v-59.52l3.502 59.52").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dn');
var path_do = rsr.path("m105.87 326.45l-3.502-59.52h3.502v59.52").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_do');
var path_dp = rsr.path("m216.17 268.68v-3.502h87.54l-87.54 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dp');
var path_dq = rsr.path("m216.17 268.68l87.54-3.502v3.502h-87.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dq');
var path_dr = rsr.path("m333.47 266.92h-3.501v-133.06l3.501 133.06").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dr');
var path_ds = rsr.path("m333.47 266.92l-3.501-133.06h3.501v133.06").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ds');
var path_dt = rsr.path("m533.05 526.03v7h-316.88l316.88-7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dt');
var path_du = rsr.path("m533.05 526.03l-316.88 7v-7h316.88").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_du');
var path_dv = rsr.path("m596.08 268.68v-3.502h50.772l-50.772 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dv');
var path_dw = rsr.path("m596.08 268.68l50.772-3.502v3.502h-50.772").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dw');
var path_dx = rsr.path("m984.74 767.64h3.502v42.02l-3.502-42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dx');
var path_dy = rsr.path("m984.74 767.64l3.502 42.02h-3.502v-42.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dy');
var path_dz = rsr.path("m1021.51 198.65v3.501h-55.44l55.44-3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_dz');
var path_ea = rsr.path("m1021.51 198.65l-55.44 3.501 3.501-3.501h51.939").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ea');
var path_eb = rsr.path("m969.57 198.65l-3.501 3.501v-68.28l3.501 64.778").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_eb');
var path_ec = rsr.path("m969.57 198.65l-3.501-64.778h3.501v64.778").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ec');
var path_ed = rsr.path("m887.87 200.4h-3.502v-66.53l3.502 66.53").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ed');
var path_ee = rsr.path("m887.87 200.4l-3.502-66.53h3.502v66.53").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ee');
var path_ef = rsr.path("m1051.27 200.4h-3.5v-66.53l3.5 66.53").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ef');
var path_eg = rsr.path("m1051.27 200.4l-3.5-66.53h3.5v66.53").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_eg');
var path_eh = rsr.path("m1077.53 202.15v-3.501h94.54l-94.54 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_eh');
var path_ei = rsr.path("m1077.53 202.15l94.54-3.501v3.501h-94.54").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ei');
var path_ej = rsr.path("m832.43 202.15v-3.501h107.38l-107.38 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ej');
var path_ek = rsr.path("m832.43 202.15l107.38-3.501v3.501h-107.38").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ek');
var path_el = rsr.path("m487.53 347.35h3.501v182.18l-3.501-182.18").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '1.11',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_el');
var path_em = rsr.path("m487.53 346.87l.809 42.19 2.692 140.47h-3.501v-182.66").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '1.11',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_em');
var path_en = rsr.path("m650.86 495.83l-.239-2.077-25.864-.199-25.864-.199-.191-111.2-.191-111.2-3.21-.232-3.21-.232v111.44 111.44h-95.16-95.16v-50.22-50.22h79.68 79.68v-3.02-3.02h-79.68-79.68v-73.64-73.64h98.18 98.18v-3.21-3.21l-98.18.216-98.18.216v-30.611-30.611l80.62-.192 80.62-.192.232-3.21.232-3.21h14.268 14.268l.226 4.343.226 4.343 27.377.198 27.377.198v19.627c0 16.875-.158 19.627-1.124 19.627-.985 0-1.149 7.693-1.322 61.993-.109 34.1-.056 62.13.117 62.31.173.173 32.714.315 72.31.315h71.999v52.07c0 33.897.264 52.23.755 52.53.465.288.755 6.664.755 16.615 0 9.951-.29 16.328-.755 16.615-.429.265-.755 4.03-.755 8.72v8.253l-26.24.199-26.24.199-.239 2.077-.239 2.077h-42.571-42.571l-.239-2.077").attr({fill: '#91a552',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"fill-rule": 'evenodd',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72 m.80057 0 0 .8009 12.579 129.87").data('id', 'path_en');

var path_eo = rsr.path("m181.15 529.54h35.02").attr({"vector-effect": 'non-scaling-stroke',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.9723 0 0 .99888 4.889 0").data('id', 'path_eo');
var path_ep = rsr.path("m533.05 529.54h70.03").attr({"vector-effect": 'non-scaling-stroke',stroke: '#91a552',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ep');

var path_eq = rsr.path("m1111.26 418.85h1v1h-1v-1").attr({"vector-effect": 'non-scaling-stroke',fill: '#000',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_eq');
var path_er = rsr.path("m1111.26 309.26h1v1h-1v-1").attr({"vector-effect": 'non-scaling-stroke',fill: '#000',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_er');

var path_es = rsr.path("m1114.3 529.54h-3.5v-81.7l3.5 81.7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_es');
var path_et = rsr.path("m1114.3 529.54l-3.5-81.7h3.5v81.7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_et');
var path_eu = rsr.path("m1110.8 228.41h3.5v79.95l-3.5-79.95").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_eu');
var path_ev = rsr.path("m1110.8 228.41l3.5 79.95-3.5 3.502v-83.45").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ev');
var path_ew = rsr.path("m1110.8 311.86l3.5-3.502h57.77l-61.27 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ew');
var path_ex = rsr.path("m1110.8 311.86l61.27-3.502v3.502h-61.27").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ex');
var path_ey = rsr.path("m1110.8 338.12h3.5v79.951l-3.5-79.951").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ey');
var path_ez = rsr.path("m1110.8 338.12l3.5 79.951-3.5 3.501v-83.45").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ez');
var path_fa = rsr.path("m1110.8 421.57l3.5-3.501h57.77l-61.27 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fa');
var path_fb = rsr.path("m1110.8 421.57l61.27-3.501v3.501h-61.27").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fb');

var path_fc = rsr.path("m842.93 200.4v164.57h86.37v-164.57").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m1 0 0 .9854 0 5.328").data('id', 'path_fc');
var path_fd = rsr.path("m842.93 310.11h86.37").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fd');
var path_fe = rsr.path("m842.93 255.25h86.37").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fe');

var path_ff = rsr.path("m886.12 200.4v164.57").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m1 0 0 .98453 0 5.648").data('id', 'path_ff');
var path_fg = rsr.path("m568.07 332.58v65.65").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m1 0 0 .95909 0 13.607").data('id', 'path_fg');

var path_fh = rsr.path("m1000.5 529.54h-3.5v-107.96l3.5 107.96m-250.74-329.07h-22.673").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fh');
var path_fi = rsr.path("m1000.5 529.54l-3.5-107.96 3.5-3.501v111.46").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fi');
var path_fj = rsr.path("m1000.5 418.07l-3.5 3.501h-35.01l38.515-3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fj');
var path_fk = rsr.path("m1000.5 418.07l-38.515 3.501v-3.501h38.515").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fk');
var path_fl = rsr.path("m935.72 529.54h-3.501v-107.96l3.501 107.96").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fl');
var path_fm = rsr.path("m935.72 529.54l-3.501-107.96 3.501-3.501v111.46").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fm');
var path_fn = rsr.path("m935.72 418.07l-3.501 3.501h-35.01l38.516-3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fn');
var path_fo = rsr.path("m935.72 418.07l-38.516 3.501v-3.501h38.516").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fo');
var path_fp = rsr.path("m828.93 421.57v-3.501h42.02l-42.02 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fp');
var path_fq = rsr.path("m828.93 421.57l42.02-3.501-3.502 3.501h-38.516").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fq');
var path_fr = rsr.path("m867.44 421.57l3.502-3.501v111.46l-3.502-107.96").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fr');
var path_fs = rsr.path("m867.44 421.57l3.502 107.96h-3.502v-107.96").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fs');
var path_ft = rsr.path("m727.38 268.42h-3.501v-134.55l3.501 134.55").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '1.42',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ft');
var path_fu = rsr.path("m727.38 268.93l-3.501-135.06h3.501v135.06").attr({"vector-effect": 'non-scaling-stroke',"stroke-width": '1.43',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fu');
var path_fv = rsr.path("m750.14 202.15v-3.501h54.27l-54.27 3.501").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fv');
var path_fw = rsr.path("m750.14 202.15l54.27-3.501v3.501h-54.27").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fw');
var path_fx = rsr.path("m646.85 268.68v-3.502h78.78l-78.78 3.502").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fx');
var path_fy = rsr.path("m646.85 268.68l78.78-3.502v3.502h-78.78").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fy');

var path_fz = rsr.path("m1023.73 87.59h-32.1m-332.1 83.69l68.62.267").attr({fill: 'none',stroke: '#000',"stroke-linejoin": 'miter',"stroke-linecap": 'butt',"fill-rule": 'evenodd','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_fz');

var path_ga = rsr.path('M 989.1 397.3 C 988.2 396.2 988.1 392.7 988.1 341.5 L 988.1 286.8 L 968.3 286.9 L 948.5 287.1 L 948.3 289.4 L 948.1 291.6 L 935.8 291.6 L 923.4 291.6 L 923.4 289.2 L 923.4 286.8 L 903.3 286.9 L 883.3 287.1 L 883.1 289.1 L 882.9 291 L 870.9 291 L 858.9 291 L 858.7 289.1 L 858.5 287.1 L 837.4 286.9 C 822.4 286.8 816.1 287 815.5 287.5 C 815.1 287.8 814.7 288.8 814.7 289.6 L 814.7 291 L 804.1 291 C 793.9 291 793.5 291 793.5 289.8 C 793.5 289.2 793.3 288.6 793 288.6 C 792.7 288.6 792.3 287.9 792.2 287 C 791.9 284.6 790.6 284.5 790.4 286.9 C 790.2 288 790.1 284.2 790 278.5 L 789.9 268.1 L 788.2 268 C 781 267.6 729.5 267.8 729.5 268.2 C 729.5 268.5 729.2 268.5 728.7 268.3 C 728.3 268 714.5 267.8 697.9 267.7 C 668.9 267.6 667.9 267.7 667.9 268.7 C 667.9 269.4 668.1 269.9 668.5 269.9 C 668.8 269.9 669.1 271.4 669.1 273.2 C 669.1 275 668.8 276.5 668.5 276.5 C 668.1 276.5 667.9 278.4 667.9 281.1 C 667.9 283.7 668.1 285.6 668.5 285.6 C 668.8 285.6 669.1 287.1 669.1 288.9 C 669.1 290.7 668.8 292.2 668.5 292.2 C 668.1 292.2 667.9 294.1 667.9 296.8 C 667.9 299.4 668.1 301.3 668.5 301.3 C 668.8 301.3 669.1 302.8 669.1 304.6 C 669.1 306.4 668.8 307.9 668.5 307.9 C 668.1 307.9 667.9 309.9 667.9 312.5 C 667.9 315.1 668.1 317 668.5 317 C 668.8 317 669.1 318.5 669.1 320.3 C 669.1 322.2 668.8 323.7 668.5 323.7 C 668.1 323.7 667.9 325.9 667.9 329.1 C 667.9 332.3 668.1 334.5 668.5 334.5 C 668.8 334.5 669.1 335.6 669.1 336.9 C 669.1 338.3 668.8 339.4 668.5 339.4 C 668.1 339.4 667.9 341.3 667.9 343.9 C 667.9 346.3 668.1 348.5 668.5 348.7 C 668.8 348.9 669.1 350.4 669.1 352.1 C 669.1 353.7 668.8 355.1 668.5 355.1 C 668.1 355.1 667.9 357 667.9 359.6 C 667.9 362 668.1 364.2 668.5 364.4 C 668.8 364.6 669.1 366.2 669.1 367.8 C 669.1 369.4 668.8 370.8 668.5 370.8 C 668.1 370.8 667.9 372.8 667.9 375.6 C 667.9 378.4 668.1 380.4 668.5 380.4 C 668.8 380.4 669.1 381.8 669.1 383.5 C 669.1 385.1 668.8 386.5 668.5 386.5 C 668.1 386.5 667.9 388.3 667.9 390.7 L 667.9 394.9 L 652.2 394.9 L 636.4 394.9 L 636.4 388 L 636.4 381.1 L 634.8 381.1 L 633.1 381.1 L 633.1 367.8 L 633.1 354.5 L 634.8 354.5 L 636.5 354.5 L 636.3 310.2 L 636.1 266 L 612.4 265.8 L 588.7 265.6 L 588.7 234.2 L 588.7 202.8 L 572.4 202.8 C 561.9 202.8 556.1 202.6 556.1 202.2 C 556.1 201.9 555.7 201.6 555.2 201.6 C 554.7 201.6 554.3 201.9 554.3 202.2 C 554.3 202.6 548.3 202.8 537.7 202.8 L 521 202.8 L 521 187.1 C 521 173.7 520.9 171.3 520.1 171 C 519.4 170.7 519.2 168.4 519.2 155 L 519.2 139.4 L 523.5 139.4 C 527 139.4 527.7 139.2 527.7 138.4 C 527.7 137.4 529.8 137.4 554.7 137.8 C 571.5 138 581.9 138.4 582.3 138.8 C 582.7 139.1 588.7 139.4 598.2 139.4 L 613.5 139.4 L 613.5 170.7 L 613.5 202.1 L 623.9 202.4 C 629.6 202.6 636.7 203 639.6 203.2 C 643.3 203.4 644.9 203.3 644.9 202.9 C 644.9 202.5 646.1 202.2 647.9 202.2 C 649.6 202.2 650.9 202.5 650.9 202.8 C 650.9 203.2 652.9 203.4 655.5 203.4 C 658.1 203.4 660 203.2 660 202.8 C 660 202.5 661.5 202.2 663.3 202.2 C 665.2 202.2 666.7 202.5 666.7 202.8 C 666.7 203.2 668.6 203.4 671.2 203.4 C 673.8 203.4 675.7 203.2 675.7 202.8 C 675.7 201.9 678 202.1 678.4 203 C 678.7 203.6 678.8 203.6 679.1 203 C 679.5 202.1 681.8 201.9 681.8 202.8 C 681.8 203.6 690.6 203.6 691.1 202.8 C 691.6 202 697.5 202 697.5 202.8 C 697.5 203.2 699.4 203.4 702 203.4 C 704.6 203.4 706.5 203.2 706.5 202.8 C 706.5 202.5 708 202.2 709.8 202.2 L 713.1 202.2 L 713 186.2 L 712.9 170.2 L 708.2 171 C 705.5 171.8 703.5 171.2 703.5 171.6 C 703.5 171.9 702.2 172.2 700.5 172.2 C 698.8 172.2 697.5 171.9 697.5 171.6 C 697.5 171.2 695.6 171 692.9 171 C 690.3 171 688.4 171.2 688.4 171.6 C 688.4 171.9 687 172.2 685.4 172.2 C 683.7 172.2 682.4 171.9 682.4 171.6 C 682.4 171.3 681.7 171 680.9 171 C 679.4 171 679.3 170.8 679.3 167.4 C 679.3 165.3 679.1 163.7 678.7 163.7 C 678.4 163.7 678.1 162.4 678.1 160.7 C 678.1 159.1 678.4 157.7 678.7 157.7 C 679.1 157.7 679.3 155.8 679.3 153.2 C 679.3 150.5 679.1 148.6 678.7 148.6 C 678.4 148.6 678.1 147.3 678.1 145.6 C 678.1 144 678.4 142.6 678.7 142.6 C 679.1 142.6 679.3 142.1 679.3 141.4 C 679.3 140.2 679.8 140.2 697.3 140 L 715.3 139.9 L 715.4 105.9 L 715.6 71.9 L 725.9 71.9 C 734.6 71.9 736.1 72 736.1 72.8 C 736.1 73.4 736.8 73.7 738.3 73.7 L 740.4 73.7 L 740.3 139.7 L 740.3 205.7 L 766.9 205.9 L 793.5 206 L 793.5 138.7 L 793.5 71.3 L 805.9 71.3 C 817.7 71.3 818.2 71.3 818.5 72.5 C 818.8 73.6 819.3 73.7 823.9 73.7 L 829 73.7 L 829.3 80.2 C 829.5 83.8 829.6 120.3 829.5 161.5 L 829.4 236.2 L 832.3 236.2 C 833.9 236.2 835.2 236 835.2 235.6 C 835.2 235.3 836.6 235 838.2 235 C 839.9 235 841.3 235.3 841.3 235.6 C 841.3 236 843.3 236.2 846.1 236.2 C 848.9 236.2 850.9 236 850.9 235.6 C 850.9 235.3 852.4 235 854.3 235 C 856.1 235 857.6 235.3 857.6 235.6 C 857.6 236 859.5 236.2 862.1 236.2 C 864.7 236.2 866.6 236 866.6 235.6 C 866.6 235.3 867.9 235 869.4 235 C 870.9 235 872.1 235.3 872.1 235.6 C 872.1 236 874.3 236.2 877.5 236.2 C 880.7 236.2 883 236 883 235.6 C 883 235.3 884.3 235 886 235 C 887.6 235 889 235.3 889 235.6 C 889 236 891 236.2 893.8 236.2 C 896.7 236.2 898.7 236 898.7 235.6 C 898.7 235.3 900.2 235 902 235 C 903.8 235 905.3 235.3 905.3 235.6 C 905.3 236 907.2 236.2 909.6 236.2 C 914.4 236.2 915.5 235.5 915.6 232.5 C 915.6 231.6 915.9 230.8 916.2 230.8 C 916.5 230.8 916.8 228.8 916.8 226 C 916.8 223.1 916.5 221.1 916.2 221.1 C 915.9 221.1 915.6 219.8 915.6 218.1 C 915.6 216.5 915.9 215.1 916.2 215.1 C 916.5 215.1 916.8 213.1 916.8 210.3 C 916.8 207.4 916.5 205.4 916.2 205.4 C 915.9 205.4 915.6 204.1 915.6 202.4 C 915.6 200.8 915.9 199.3 916.2 199.1 C 916.5 198.9 916.8 196.7 916.8 194.2 C 916.8 191.6 916.5 189.7 916.2 189.7 C 915.9 189.7 915.6 188.2 915.6 186.4 C 915.6 184.6 915.9 183.1 916.2 183.1 C 916.5 183.1 916.8 181.2 916.8 178.5 C 916.8 175.9 916.5 174 916.2 174 C 915.9 174 915.6 172.5 915.6 170.7 C 915.6 168.9 915.9 167.4 916.2 167.4 C 916.5 167.4 916.8 165.3 916.8 162.5 C 916.8 159.7 916.5 157.7 916.2 157.7 C 915.9 157.7 915.6 156.3 915.6 154.7 C 915.6 153 915.9 151.7 916.2 151.7 C 916.5 151.7 916.8 149.6 916.8 146.8 C 916.8 144 916.5 142 916.2 142 C 915.9 142 915.6 140.5 915.6 138.7 C 915.6 136.8 915.9 135.3 916.2 135.3 C 916.5 135.3 916.8 133.2 916.8 130.2 C 916.8 127.2 916.5 125.1 916.2 125.1 C 915.9 125.1 915.6 123.8 915.6 122.4 C 915.6 120.9 915.9 119.6 916.2 119.6 C 916.5 119.6 916.8 117.6 916.8 114.8 C 916.8 112 916.5 110 916.2 110 C 915.9 110 915.6 108.6 915.6 106.9 C 915.6 105.3 915.9 103.9 916.2 103.9 C 916.5 103.9 916.8 101.9 916.8 99.1 C 916.8 96.3 916.5 94.3 916.2 94.3 C 915.9 94.3 915.6 92.9 915.6 91.2 C 915.6 89.6 915.9 88.2 916.2 88.2 C 916.5 88.2 916.8 86.2 916.8 83.4 C 916.8 80.6 916.5 78.5 916.2 78.5 C 915.9 78.5 915.6 77.5 915.6 76.2 L 915.6 73.8 L 921.5 73.6 L 927.4 73.4 L 927.6 71.4 L 927.7 69.5 L 939.8 69.5 L 951.8 69.5 L 951.9 71.4 C 952.1 73.4 952.2 73.4 955.3 73.6 C 958 73.7 958.5 74 958.5 75 C 958.5 75.6 958.2 76.1 957.9 76.1 C 957.5 76.1 957.3 78 957.3 80.7 C 957.3 83.3 957.5 85.2 957.9 85.2 C 958.2 85.2 958.5 86.6 958.5 88.2 C 958.5 89.9 958.2 91.2 957.9 91.2 C 957.5 91.2 957.3 93.2 957.3 96.1 C 957.3 98.9 957.5 100.9 957.9 100.9 C 958.2 100.9 958.5 102.3 958.5 103.9 C 958.5 105.6 958.2 106.9 957.9 106.9 C 957.5 106.9 957.3 109 957.3 111.8 C 957.3 114.6 957.5 116.6 957.9 116.6 C 958.2 116.6 958.5 118 958.5 119.6 C 958.5 121.4 958.2 122.7 957.8 122.7 C 957.5 122.7 957.3 123.5 957.4 124.6 C 957.6 126.5 957.7 126.6 960.7 126.8 C 962.7 126.9 963.9 126.7 963.9 126.3 C 963.9 125.9 965.3 125.7 967.2 125.7 C 969.1 125.7 970.6 125.9 970.6 126.3 C 970.6 126.6 972.5 126.9 975.1 126.9 C 977.7 126.9 979.6 126.6 979.6 126.3 C 979.6 125.9 981 125.7 982.6 125.7 C 984.3 125.7 985.7 125.9 985.7 126.3 C 985.7 126.6 987.7 126.9 990.5 126.9 C 993.3 126.9 995.3 126.6 995.3 126.3 C 995.3 125.9 996.7 125.7 998.4 125.7 C 1000.1 125.7 1001.4 125.9 1001.4 126.3 C 1001.4 126.7 1002.2 126.9 1003.3 126.8 C 1005.3 126.6 1005.3 126.5 1005.5 123.1 C 1005.6 121 1005.4 119.6 1005 119.6 C 1004.7 119.6 1004.4 118.3 1004.4 116.6 C 1004.4 114.9 1004.7 113.6 1005 113.6 C 1005.4 113.6 1005.6 111.7 1005.6 109.1 C 1005.6 106.4 1005.4 104.5 1005 104.5 C 1004.7 104.5 1004.4 103 1004.4 101.2 C 1004.4 99.4 1004.7 97.9 1005 97.9 C 1005.4 97.9 1005.6 96 1005.6 93.3 C 1005.6 90.7 1005.4 88.8 1005 88.8 C 1004.7 88.8 1004.4 87.3 1004.4 85.5 C 1004.4 83.7 1004.7 82.2 1005 82.2 C 1005.3 82.2 1005.6 80.4 1005.6 78 C 1005.6 73.9 1005.6 73.8 1007.3 73.6 C 1008.7 73.4 1009 73.1 1009.1 71.4 L 1009.3 69.5 L 1021.3 69.5 C 1032.8 69.5 1033.3 69.5 1033.6 70.7 C 1033.8 71.7 1034.4 71.9 1036.4 71.9 C 1038.4 71.9 1038.8 71.7 1038.8 70.7 C 1038.8 69.5 1039.2 69.5 1051.2 69.5 L 1063.6 69.5 L 1063.6 71.6 L 1063.6 73.7 L 1082.3 73.7 L 1101.1 73.7 L 1101.1 86.1 L 1101.1 98.5 L 1099 98.5 C 1097 98.5 1096.8 98.6 1096.8 100.2 C 1096.8 101.4 1096.5 102.1 1095.7 102.3 C 1095.1 102.4 1087.8 102.5 1079.4 102.3 L 1064.2 102.1 L 1064.1 105.6 C 1064.1 107.5 1064 124.6 1063.9 143.6 L 1063.8 178.2 L 1065.2 178.2 C 1066 178.2 1066.6 178 1066.6 177.6 C 1066.6 177.3 1068 177 1069.7 177 C 1071.3 177 1072.7 177.3 1072.7 177.6 C 1072.7 178 1074.6 178.2 1077.2 178.2 C 1079.8 178.2 1081.7 178 1081.7 177.6 C 1081.7 177.3 1083.2 177 1085.1 177 C 1086.9 177 1088.4 177.3 1088.4 177.6 C 1088.4 178 1090.2 178.2 1092.6 178.2 L 1096.8 178.2 L 1097 180.8 C 1097.1 183.2 1097.3 183.4 1099.1 183.6 L 1101.1 183.8 L 1101.1 195.8 L 1101.1 207.8 L 1099 207.8 C 1097 207.8 1096.8 208 1096.8 209.7 C 1096.8 210.9 1096.5 211.5 1095.9 211.5 C 1095.4 211.5 1095 211.7 1095 212.1 C 1095 212.4 1093.7 212.7 1092 212.7 C 1090.3 212.7 1089 212.4 1089 212.1 C 1089 211.7 1087 211.5 1084.2 211.5 C 1081.3 211.5 1079.3 211.7 1079.3 212.1 C 1079.3 212.4 1078 212.7 1076.3 212.7 C 1074.6 212.7 1073.3 212.4 1073.3 212.1 C 1073.3 211.7 1071.4 211.5 1068.7 211.5 L 1064.2 211.5 L 1064.1 215.2 C 1064.1 217.3 1064 234.5 1063.9 253.3 L 1063.8 287.6 L 1065.2 287.6 C 1066 287.6 1066.6 287.3 1066.6 287 C 1066.6 286.7 1068 286.4 1069.7 286.4 C 1071.3 286.4 1072.7 286.7 1072.7 287 C 1072.7 287.3 1074.6 287.6 1077.2 287.6 C 1079.8 287.6 1081.7 287.3 1081.7 287 C 1081.7 286.7 1083.2 286.4 1085.1 286.4 C 1086.9 286.4 1088.4 286.7 1088.4 287 C 1088.4 287.3 1090.2 287.6 1092.6 287.6 L 1096.8 287.6 L 1096.8 290.3 C 1096.8 293 1096.9 293 1099 293 L 1101.1 293 L 1101.1 305.4 L 1101.1 317.8 L 1099 317.8 C 1097 317.8 1096.8 318 1096.8 319.5 C 1096.8 321.1 1096.6 321.3 1094.6 321.6 C 1093.3 321.8 1085.9 321.8 1078.1 321.7 L 1063.9 321.5 L 1064 352.7 C 1064 369.9 1063.9 387 1063.7 390.8 L 1063.4 397.6 L 1041.8 397.6 C 1023 397.6 1020 397.7 1019.7 398.5 C 1019.4 399.2 1017.2 399.4 1004.8 399.3 C 991.2 399.3 990.1 399.2 989.1 398.1 L 989.1 398.1Z M 792.2 274.3 C 792.1 271.3 791.8 269.6 791.3 269.4 C 790.7 269.2 790.5 270.4 790.5 274.1 C 790.5 278.1 790.7 278.9 791.4 278.9 C 792.2 278.9 792.3 278.2 792.2 274.3 Z M 973.6 234.8 C 973.6 234.5 975.1 234.2 976.9 234.2 C 978.7 234.2 980.2 234.5 980.2 234.8 C 980.2 235.2 982.1 235.4 984.8 235.4 C 987.4 235.4 989.3 235.2 989.3 234.8 C 989.3 234.5 990.8 234.2 992.6 234.2 C 994.4 234.2 995.9 234.5 995.9 234.8 C 995.9 235.2 998 235.4 1000.8 235.4 L 1005.6 235.4 L 1005.6 233 C 1005.6 231.3 1005.4 230.6 1004.7 230.6 C 1004 230.6 1003.8 229.9 1003.8 227.6 C 1003.8 225.3 1004 224.6 1004.7 224.6 C 1005.4 224.6 1005.6 223.6 1005.6 219.7 C 1005.6 215.8 1005.4 214.9 1004.7 214.9 C 1004 214.9 1003.8 214.1 1003.8 211.6 C 1003.8 209 1004 208.3 1004.7 208.3 C 1005.5 208.3 1005.6 207.5 1005.5 203.6 C 1005.4 200.2 1005.1 198.9 1004.5 199 C 1004 199.1 1003.8 198.2 1003.8 195.9 C 1003.8 193.3 1004 192.5 1004.7 192.5 C 1005.4 192.5 1005.6 191.6 1005.6 187.7 C 1005.6 183.8 1005.4 182.9 1004.7 182.9 C 1004 182.9 1003.8 182.1 1003.8 179.6 C 1003.8 177 1004 176.2 1004.7 176.2 C 1005.4 176.2 1005.6 175.3 1005.6 171.7 C 1005.6 168.1 1005.4 167.2 1004.7 167.2 C 1004 167.2 1003.8 166.4 1003.8 163.8 C 1003.8 162 1004.1 160.5 1004.4 160.5 C 1004.7 160.5 1005 160.2 1005 159.9 C 1005 159.6 1003.1 159.3 1000.5 159.3 C 998 159.3 995.8 159.6 995.6 159.9 C 995.4 160.2 993.9 160.5 992.3 160.5 C 990.6 160.5 989.3 160.2 989.3 159.9 C 989.3 159.6 987.3 159.3 984.5 159.3 C 981.6 159.3 979.6 159.6 979.6 159.9 C 979.6 160.2 978.1 160.5 976.3 160.5 C 974.5 160.5 973 160.2 973 159.9 C 973 159.6 971.1 159.3 968.4 159.3 C 965.8 159.3 963.9 159.6 963.9 159.9 C 963.9 160.2 962.5 160.5 960.7 160.5 L 957.5 160.5 L 957.5 197.4 L 957.5 234.2 L 960.7 234.2 C 962.5 234.2 963.9 234.5 963.9 234.8 C 963.9 235.2 965.9 235.4 968.8 235.4 C 971.6 235.4 973.6 235.2 973.6 234.8 L 974.4 234.8Z');
path_ga.attr({'fill':'#ffff7f','stroke':'none'});
walkways.push[path_ga];

var path_gc = rsr.path('M 557.4 72.4 L 557.4 8.1 L 633.7 8.1 L 709.9 8.1 L 709.9 71.4 L 709.9 134.6 L 646.4 134.6 C 597.3 134.6 582.9 134.7 582.7 134.9 C 582.5 135 582.4 135.5 582.4 135.9 L 582.4 136.7 L 569.9 136.7 L 557.4 136.7 L 557.4 72.4Z');
path_gc.attr({'fill':'#ff7f00','stroke':'none'}).data('id', 'path_gc');;
partners.push(path_gc);

var path_gd = rsr.path("m893.84 48.76v-38.584h46.19 46.19v37.516 37.516h-32.523c-24.939 0-32.597.075-32.843.32-.176.176-.32.657-.32 1.068v.748h-13.351-13.351v-38.584").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"fill-rule": 'evenodd',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_gd');
var path_ge = rsr.path("m992.11 48.627v-38.45h48.06 48.06v37.516 37.516h-32.12c-24.628 0-32.2.075-32.443.32-.176.176-.32.597-.32.935v.614h-15.62-15.621v-38.45").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"fill-rule": 'evenodd',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ge');

var path_gf = rsr.path("m646.85 332.58h78.78v-32.83h-35.01").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gf');
var path_gg = rsr.path("m690.62 332.58v-65.66").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8 8',stroke: '#7f9fff',fill: 'none',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gg');

var path_gh = rsr.path("m181.15 526.03v7h-168.07l168.07-7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gh');
var path_gi = rsr.path("m181.15 526.03l-168.07 7 7-7h161.07").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gi');
var path_gj = rsr.path("m20.08 526.03l-7 7v-402.67l7 395.67").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gj');
var path_gk = rsr.path("m20.08 526.03l-7-395.67 7 7v388.67").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gk');
var path_gl = rsr.path("m20.08 137.37l-7-7h1162.5l-1155.5 7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gl');
var path_gm = rsr.path("m20.08 137.37l1155.5-7-7.01 7h-1148.49").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gm');
var path_gn = rsr.path("m1168.57 137.37l7.01-7v682.79l-7.01-675.79").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gn');
var path_go = rsr.path("m1168.57 137.37l7.01 675.79-7.01-7v-668.78").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_go');
var path_gp = rsr.path("m1168.57 806.15l7.01 7h-287.13l280.12-7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gp');
var path_gq = rsr.path("m1168.57 806.15l-280.12 7 7-7h273.11").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gq');
var path_gr = rsr.path("m895.46 806.15l-7 7v-203.09l7 196.08").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gr');
var path_gs = rsr.path("m895.46 806.15l-7-196.08h7v196.08").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gs');
var path_gt = rsr.path("m895.46 582.06h-7v-49.02l7 49.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gt');
var path_gu = rsr.path("m895.46 582.06l-7-49.02 7-7v56.02").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gu');
var path_gv = rsr.path("m895.46 526.03l-7 7h-285.37l292.38-7").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gv');
var path_gw = rsr.path("m895.46 526.03l-292.37 7v-7h292.38").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gw');


var path_1 = rsr.path("m843.45 255.59h41.97v53.52h-41.97z").attr({id: '1',fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_1');
pas.push(path_1);

var path_2 = rsr.path("m886.3 255.93h41.97v53.52h-41.97z").attr({id: '2',fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_2');
pas.push(path_2);

var path_gx = rsr.path("m916.47 739.63v-122.56").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_gx');
var path_gy = rsr.path("m916.47 657.92h-24.51").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.7809 8.7809',"stroke-width": '1.1',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72 m.83024 0 0 1 155.58 0").data('id', 'path_gy');
var path_gz = rsr.path("m916.47 698.77h-24.51").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.82853 8.82853',"stroke-width": '1.1',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-opacity': '1'}).transform("t-12.57-129.72 m.8213 0 0 1 163.77 0").data('id', 'path_gz');

var path_ha = rsr.path("m986.5 711.61h-24.51v-94.54").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.8822 0 0 .97213 113.32 19.832").data('id', 'path_ha');
var path_hb = rsr.path("m1077.53 568.05h-38.51v-38.51").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.92689 0 0 1.003 75.942-1.63").data('id', 'path_hb');
var path_hc = rsr.path("m1077.53 631.08h-38.51v38.52 38.51h38.51").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.93099 0 0 1 71.7 0").data('id', 'path_hc');
var path_hd = rsr.path("m1077.53 785.14h-91.03").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.9366 0 0 1 65.6 0").data('id', 'path_hd');

var path_he = rsr.path("m1077.53 529.54v-78.2h35.02").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.92576 0 0 .96607 80 15.316").data('id', 'path_he');
var path_hf = rsr.path("m1112.55 306.61h-35.02v-74.7h35.02").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.92532 0 0 1 80.47 0").data('id', 'path_hf');
var path_hg = rsr.path("m1112.55 416.32h-35.02v-74.7h35.02").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m.9261 0 0 1 79.63 0").data('id', 'path_hg');
var path_hh = rsr.path("m1018.01 200.4v54.85h-46.69v-54.85").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m1 0 0 .95373 0 11.812").data('id', 'path_hh');
var path_hi = rsr.path("m1018.01 364.97h-46.69v-74.93h46.69v74.93").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_hi');
var path_hj = rsr.path("m743.14 398.23v131.31").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72 m1 0 0 .96532 0 13.81").data('id', 'path_hj');
var path_hk = rsr.path("m681.87 463.88h122.55").attr({"vector-effect": 'non-scaling-stroke',"stroke-dasharray": '8.00093 8.00093',fill: 'none',stroke: '#7f9fff',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_hk');

var path_hl = rsr.path("m988.66 785.31h86.52v20.519h-86.52z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hl');
pas.push(path_hl);

var path_hm = rsr.path("m1039.33 671.68h35.85v36.509h-35.85z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hm');
pas.push(path_hm);

var path_hn = rsr.path("m1038.99 630.85h36.19v36.508h-36.19z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hn');
pas.push(path_hn);

var path_ho = rsr.path("m962.47 618.94h21.565v92.31h-21.565z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ho');
pas.push(path_ho);

var path_hp = rsr.path("m896.16 618.94h19.865v38.55h-19.865z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hp');
pas.push(path_hp);

var path_hq = rsr.path("m896.16 658.41h19.525v39.57h-19.525z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hq');
pas.push(path_hq);

var path_hr = rsr.path("m896.16 699.24h19.525v38.21h-19.525z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hr');
pas.push(path_hr);

var path_hs = rsr.path("m896.16 531.85h42.65v49.777h-42.65z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hs');
pas.push(path_hs);

var path_ht = rsr.path("m939.69 531.51h46.39v50.12h-46.39z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ht');
pas.push(path_ht);

var path_hu = rsr.path("m1038.65 531.85h36.53v35.828h-36.53z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hu');
pas.push(path_hu);

var path_hv = rsr.path("m1077.76 451.89h32.45v74.954h-32.45z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hv');
pas.push(path_hv);

var path_hw = rsr.path("m1077.76 342h32.45v73.59h-32.45z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hw');
pas.push(path_hw);

var path_hx = rsr.path("m1078.1 232.45h32.45v73.59h-32.45z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hx');
pas.push(path_hx);

var path_hy = rsr.path("m971.66 290.63h45.37v73.59h-45.37z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hy');
// pas.push(path_hy);

var path_hz = rsr.path("m972 202.85h45.37v51.48h-45.37z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_hz');
pas.push(path_hz);

var path_ia = rsr.path("m843.45 202.51h41.97v52.16h-41.97z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ia');
pas.push(path_ia);

var path_ib = rsr.path("m843.45 310.7h41.97v53.52h-41.97z").attr({fill: '#52a5a5',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ib');
associates.push(path_ib);

var path_ic = rsr.path("m886.3 310.36h41.97v53.52h-41.97z").attr({fill: '#ffb380',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ic');
pas.push(path_ic);

var path_id = rsr.path("m211.67 496.34c0-1.184-.298-2-.802-2.195-.544-.209-.802-1.061-.802-2.655 0-1.73.211-2.347.802-2.347.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-1.169 0-1.169-7.76 0-8.209 1.185-.455 1.185-11.563 0-11.563-.64 0-.802-.795-.802-3.951 0-3.02.189-4.02.802-4.258.642-.246.802-1.426.802-5.935 0-4.646-.14-5.628-.802-5.628-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275.662 0 .802-.98.802-5.611 0-4.631-.14-5.611-.802-5.611-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275 1.185 0 1.185-11.11 0-11.563-1.169-.449-1.169-8.209 0-8.209.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-1.169 0-1.169-7.76 0-8.209 1.185-.455 1.185-11.563 0-11.563-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275.662 0 .802-.98.802-5.611 0-4.631-.14-5.611-.802-5.611-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275 1.185 0 1.185-11.11 0-11.563-1.169-.449-1.169-8.209 0-8.209.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.662 0 .802-.98.802-5.611v-5.611h91.91 91.91v127.18 127.18l-70.937.136-70.937.136-.166 2-.166 2h-20.808-20.808v-1.887").attr({fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72 m.80057 0 0 .8009 12.579 129.87").data('id', 'path_id');
// pas.push(path_id);

var path_ie = rsr.path("m886.3 202.85h41.97v52.16h-41.97z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ie');
pas.push(path_ie);

var path_if = rsr.path("m682.25 398.82h59.994v64.41h-59.994z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_if');
pas.push(path_if);

var path_ig = rsr.path("m743.36 398.48h59.654v64.41h-59.654z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ig');
// pas.push(path_ig);

var path_ih = rsr.path("m744.04 464.82h58.29v60.664h-58.29z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ih');
pas.push(path_ih);

var path_ii = rsr.path("m682.49 464.48h59.654v60.664h-59.654z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ii');
pas.push(path_ii);

var path_ij = rsr.path("m533.39 332.9h34.02v63.2h-34.02z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ij');
pas.push(path_ij);

var path_ik = rsr.path("m568.74 332.9h32.58v63.08h-32.58z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ik');
pas.push(path_ik);

var path_il = rsr.path("m648.88 269.11h42.3v63.2h-42.3z").attr({"fill-rule": 'evenodd',fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_il');
associates.push(path_il);

var path_im = rsr.path("m692.26 301.48h32.936v30.591h-32.936z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_im');
pas.push(path_im);

var path_in = rsr.path("m626.83 269.22h17.644v63.08h-17.644z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_in');
pas.push(path_in);

var path_io = rsr.path("m106.37 386.35h42.798v51.585h-42.798z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_io');
pas.push(path_io);

var path_ip = rsr.path("m106.37 269h42.798v53.52h-42.798z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ip');
pas.push(path_ip);

var path_iq = rsr.path("m106.25 474.01h42.798v51.585h-42.798z").attr({"fill-rule": 'evenodd',fill: '#ffb380',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-dasharray": '8.00093 8.00093',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_iq');
pas.push(path_iq);

var path_ir = rsr.path("m802.67 419.82h3.501v109.71l-3.501-109.71").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_ir');
var path_is = rsr.path("m802.67 419.82l3.501 109.71h-3.501v-109.71").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_is');

var path_it = rsr.path("m645.1 512.03h3.502v17.507l-3.502-17.507").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_it');
var path_iu = rsr.path("m645.1 512.03l3.502 17.507h-3.502v-17.507").attr({"vector-effect": 'non-scaling-stroke',fill: '#003f7f',"fill-rule": 'evenodd',stroke: '#003f7f',"stroke-linejoin": 'bevel',"stroke-linecap": 'square','stroke-width': '0','stroke-opacity': '1'}).transform("t-12.57-129.72").data('id', 'path_iu');

var path_iv = rsr.path("m172.25 461.65c0-24.542-.132-32.469-.548-32.885-.414-.414-7.125-.548-27.404-.548h-26.856v-1.771c0-2.327-.797-2.924-3.641-2.727l-2.297.159v-15.622-15.622l2.855-.136 2.855-.136.135-3.083.135-3.083h27.15 27.15l.249-1.028c.137-.565.192-15.621.123-33.456l-.126-32.429-27.27-.118-27.27-.118-.139-2.166-.139-2.166-2.626-.137-2.626-.137v-15.477-15.477l1.484-.143 1.484-.143v-2.74-2.74l-1.484-.143-1.484-.143v-15.477-15.477l2.626-.137 2.626-.137.139-2.17.139-2.17h27.1c24.292 0 27.13-.075 27.379-.722.152-.397.277-15.605.277-33.795v-33.07l23.865-.118 23.865-.118.137-2.626.137-2.626h16.625 16.625l.137 2.626.137 2.626h55.27 55.27v-2.855-2.855h15.616 15.616l.142 1.713.142 1.713 2.626.137 2.626.137v30.806 30.806l-95.57.115-95.57.115-.115 130.06-.115 130.06h-18.726-18.726v-32.34").attr({fill: '#ffff7e',"fill-opacity": '.98',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72 m.8 0 0 .8 12.57 129.72").data('id', 'path_iv');
var path_iw = rsr.path("m1001.62 528.31h30.86v.862h-30.86z").attr({fill: '#00f',"fill-rule": 'evenodd',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_iw');

var path_ix = rsr.path("m333.75 137.64h153.46v124.35h-153.46z").attr({fill: '#bf7fff',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ix');
conferencerooms.push(path_ix);

var path_iy = rsr.path("m20.595 137.78h194.9v126.76h-194.9z").attr({fill: '#bf7fff',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_iy');
conferencerooms.push(path_iy);

var path_iz = rsr.path("m216.63 137.54h113v126.66h-113z").attr({fill: '#bf7fff',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_iz');
conferencerooms.push(path_iz);

var path_ja = rsr.path("m20.509 356.19h81.42v83.78h-81.42z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ja');
associates.push(path_ja)

var path_jb = rsr.path("m20.609 443.33h81.12v82.58h-81.12z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jb');
associates.push(path_jb)

var path_jc = rsr.path("m753.32 202.47h48.31v127.64h-48.31z").attr({fill: '#91a552',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jc');
var path_jd = rsr.path("m530.35 266.81h1.033v34.36h-1.033z").attr({fill: '#89a02c',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jd');

var path_je = rsr.path("m887.53 137.76h77.55v60.22h-77.55z").attr({fill: '#ff7f00',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_je');
partners.push(path_je);

var path_jf = rsr.path("m969.27 137.46h77.33v60.59h-77.33z").attr({fill: '#ff7f00',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jf');
partners.push(path_jf);

var path_jg = rsr.path("m1050.8 137.65h116.41v60.53h-116.41z").attr({fill: '#ff7f00',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jg');
partners.push(path_jg);

var path_jh = rsr.path("m20.322 268.1h81.65v84.23h-81.65z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jh');
associates.push(path_jh);

var path_ji = rsr.path("m1113.75 202.47h53.48v105.03h-53.48z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_ji');
associates.push(path_ji);

var path_jj = rsr.path("m1113.88 312.02h53.35v105.16h-53.35z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jj');
associates.push(path_jj);

var path_jk = rsr.path("m1114.01 421.44h53.22v105.29h-53.22z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jk');
associates.push(path_jk);

var path_jl = rsr.path("m1078.87 531.12h88.36v65.755h-88.36z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jl');
associates.push(path_jl);

var path_jm = rsr.path("m1078.87 600.62h88.62v66.66h-88.62z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jm');
associates.push(path_jm);

var path_jn = rsr.path("m1078.87 670.9h88.62v65.756h-88.62z").attr({fill: '#ff7f00',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jn');
partners.push(path_jn);

var path_jo = rsr.path("m1078.87 741.05h88.49v63.69h-88.49z").attr({fill: '#ff7f00',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jo');
partners.push(path_jo);

var path_jp = rsr.path("m895.04 740.79h88.49v64.08h-88.49z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jp');
associates.push(path_jp);

var path_jq = rsr.path("m806.03 421.06h60.46v104.12h-60.46z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jq');
associates.push(path_jq);

var path_jr = rsr.path("m870.36 421.06h60.976v105.93h-60.976z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_jr');
associates.push(path_jr);

var path_js = rsr.path("m935.22 421.57h60.718v105.16h-60.718z").attr({fill: '#52a5a5',stroke: 'none','stroke-width':'1','stroke-opacity':'1',"stroke-linejoin": 'bevel',"stroke-linecap": 'square'}).transform("t-12.57-129.72").data('id', 'path_js');
associates.push(path_js);

var path_ke = rsr.path("m491.07 137.36h74.67v126.86h-74.67z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ke');
partners.push(path_ke);

var path_kh = rsr.path("m726.97 137.62h74.928v60.46h-74.928z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kh');
associates.push(path_kh)

var path_kj = rsr.path("m806.2 137.5h77.46v60.66h-77.46z").attr({
    "stroke-width": '0.8',
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kj');
associates.push(path_kj)

var path_ki = rsr.path('M 980.3 654 L 975.7 653.8 L 975.6 645.2 L 975.4 636.5 L 973.3 636.4 L 971.2 636.3 L 971.2 623.9 L 971.2 611.6 L 972.4 611.5 C 973.6 611.3 973.6 611.2 973.6 609.1 L 973.6 606.8 L 938.9 606.7 L 904.1 606.6 L 904.1 603.5 C 904.1 601 904 600.3 903.4 600.3 C 902.9 600.3 902.8 599.7 902.8 597.2 C 902.8 594.9 902.9 594 903.4 593.8 C 904 593.6 904.1 592.7 904.1 589.1 C 904.1 585.4 904 584.6 903.4 584.6 C 902.9 584.6 902.8 583.9 902.8 581.2 C 902.8 578.5 902.9 577.8 903.4 577.8 C 904 577.8 904.1 576.9 904.1 572.6 C 904.1 568.3 904 567.4 903.4 567.4 C 902.9 567.4 902.8 566.9 902.8 564.8 C 902.8 562.9 902.9 562.1 903.4 561.9 C 904 561.7 904.1 560.7 904.1 557.1 C 904.1 553.4 904 552.6 903.4 552.6 C 902.9 552.6 902.8 551.9 902.8 549.2 C 902.8 546.5 902.9 545.8 903.4 545.8 C 904 545.8 904.1 545 904.1 541.1 C 904.1 537.2 904 536.4 903.4 536.4 C 902.9 536.4 902.8 535.7 902.8 533 C 902.8 530.3 902.9 529.6 903.4 529.6 C 904 529.6 904.1 528.8 904.1 525.1 C 904.1 522.4 903.9 520.6 903.7 520.6 C 903.4 520.6 903.2 519.2 903.2 517.3 C 903.2 515.3 903.4 513.9 903.7 513.9 C 903.9 513.9 904.1 512 904.1 509.2 C 904.1 506.3 903.9 504.4 903.7 504.4 C 903.4 504.4 903.2 503 903.2 501.1 C 903.2 499.1 903.4 497.7 903.7 497.7 C 903.9 497.7 904.1 496 904.1 493.4 L 904.1 489.1 L 926.6 489.1 C 948.8 489.1 949.1 489.1 949.1 490 C 949.1 490.5 948.9 490.9 948.7 490.9 C 948.4 490.9 948.2 492.7 948.2 495.4 C 948.2 498.1 948.4 499.9 948.7 499.9 C 948.9 499.9 949.1 501.2 949.1 502.9 C 949.1 504.5 948.9 505.8 948.7 505.8 C 948.4 505.8 948.2 507.5 948.2 510.1 C 948.2 512.6 948.4 514.3 948.7 514.3 C 948.9 514.3 949.1 515.7 949.1 517.5 C 949.1 519.3 948.9 520.6 948.7 520.6 C 948.4 520.6 948.2 522.4 948.2 524.9 C 948.2 527.5 948.4 529.2 948.7 529.2 C 948.9 529.2 949.1 530.5 949.1 532.3 C 949.1 534.1 948.9 535.5 948.7 535.5 C 948.4 535.5 948.2 537.2 948.2 539.8 C 948.2 542.3 948.4 544 948.7 544 C 948.9 544 949.1 545.4 949.1 547.2 C 949.1 549 948.9 550.3 948.7 550.3 C 948.4 550.3 948.2 552.1 948.2 554.6 C 948.2 557.2 948.4 558.9 948.7 558.9 C 948.9 558.9 949.1 560.2 949.1 562 C 949.1 563.8 948.9 565.2 948.7 565.2 C 948.4 565.2 948.2 566.9 948.2 569.5 C 948.2 572 948.4 573.7 948.7 573.7 C 948.9 573.7 949.1 575 949.1 576.7 C 949.1 578.5 948.9 579.6 948.6 579.6 C 948.4 579.6 948.2 580.1 948.3 580.6 C 948.4 581.6 948.7 581.6 952.4 581.8 C 954.9 581.8 956.3 581.7 956.3 581.4 C 956.3 581.1 957.5 581 959.2 581 C 960.9 581 962.2 581.1 962.2 581.4 C 962.2 581.7 964.5 581.8 968.8 581.8 L 975.4 581.6 L 975.4 533 L 975.4 484.4 L 929.2 484.3 L 883 484.2 L 882.9 481.6 L 882.7 479 L 880.8 478.9 L 878.9 478.7 L 878.9 465.5 L 878.9 452.2 L 883.2 452.2 C 885.7 452.2 887.5 452 887.5 451.8 C 887.5 451.5 888.8 451.3 890.6 451.3 C 892.4 451.3 893.8 451.5 893.8 451.8 C 893.8 452 895.6 452.2 898.3 452.2 C 901 452.2 902.8 452 902.8 451.8 C 902.8 451.5 904.1 451.3 905.9 451.3 C 907.7 451.3 909.1 451.5 909.1 451.8 C 909.1 452 910.9 452.2 913.8 452.2 C 916.6 452.2 918.5 452 918.5 451.8 C 918.5 451.5 919.9 451.3 921.7 451.3 C 923.5 451.3 924.8 451.5 924.8 451.8 C 924.8 452 926.6 452.2 929.3 452.2 C 932 452.2 933.8 452 933.8 451.8 C 933.8 451.5 935.2 451.3 937 451.3 C 938.8 451.3 940.1 451.5 940.1 451.8 C 940.1 452 942 452.2 944.8 452.2 C 947.7 452.2 949.6 452 949.6 451.8 C 949.6 451.5 950.9 451.3 952.7 451.3 C 954.5 451.3 955.9 451.5 955.9 451.8 C 955.9 452 957.7 452.2 960.4 452.2 C 963.1 452.2 964.9 452 964.9 451.8 C 964.9 451.5 966.3 451.3 968.2 451.3 C 970.2 451.3 971.6 451.5 971.6 451.8 C 971.6 452 972.1 452.2 972.7 452.2 C 973.9 452.2 973.9 452.2 973.9 448.2 C 973.9 445.8 973.7 444.1 973.4 444.1 C 973.2 444.1 973 442.8 973 441 C 973 439.2 973.2 437.8 973.4 437.8 C 973.7 437.8 973.9 436 973.9 433.3 C 973.9 430.6 973.7 428.8 973.4 428.8 C 973.2 428.8 973 427.5 973 425.7 C 973 423.9 973.2 422.5 973.4 422.5 C 973.7 422.5 973.9 420.6 973.9 417.8 C 973.9 414.9 973.7 413.1 973.4 413.1 C 973.2 413.1 973 411.7 973 409.9 C 973 408.1 973.2 406.8 973.4 406.8 C 973.7 406.8 973.9 405.7 973.9 404.3 L 973.9 401.8 L 981.6 401.7 C 989.3 401.6 989.4 401.6 989.5 400.6 L 989.7 399.6 L 1004.7 399.6 L 1019.7 399.6 L 1019.9 400.6 C 1020 401.5 1020.3 401.6 1022.6 401.7 L 1025.2 401.9 L 1025.2 420.1 L 1025.2 438.3 L 1028.1 438.3 C 1029.8 438.3 1031 438.1 1031 437.8 C 1031 437.6 1032.4 437.4 1034.2 437.4 C 1036 437.4 1037.3 437.6 1037.3 437.8 C 1037.3 438.1 1039.2 438.3 1042.1 438.3 C 1044.9 438.3 1046.8 438.1 1046.8 437.8 C 1046.8 437.6 1048.1 437.4 1049.9 437.4 C 1051.7 437.4 1053.1 437.6 1053.1 437.8 C 1053.1 438.1 1054.8 438.3 1057.3 438.3 L 1061.6 438.3 L 1061.7 440 C 1061.9 441.6 1061.9 441.6 1064 441.8 L 1066.1 441.9 L 1066.1 454.2 L 1066.1 466.6 L 1064.9 466.7 C 1063.7 466.8 1063.7 467 1063.7 469.1 C 1063.7 471.2 1063.7 471.4 1064.9 471.5 L 1066.1 471.6 L 1066.1 484 L 1066.1 496.3 L 1064 496.4 C 1061.9 496.6 1061.9 496.6 1061.7 498.2 L 1061.6 499.8 L 1055.2 500 C 1051.7 500.2 1043.5 500.2 1037.1 500.2 L 1025.4 500.1 L 1025.4 518.5 L 1025.4 536.8 L 1012.9 537.6 L 1000.4 538.4 L 1000.5 540.8 L 1000.7 543.2 L 1012.8 543.3 L 1025 543.4 L 1025.3 549.8 C 1025.4 553.3 1025.5 561.5 1025.4 567.9 L 1025.3 579.6 L 1043.5 579.6 L 1061.6 579.6 L 1061.7 581.4 L 1061.9 583.2 L 1064 583.4 L 1066.1 583.5 L 1066.1 595.8 L 1066.1 608.1 L 1064.9 608.3 C 1063.7 608.4 1063.7 608.6 1063.7 610.7 C 1063.7 612.8 1063.7 612.9 1064.9 613.1 L 1066.1 613.2 L 1066.1 625.5 L 1066.1 637.9 L 1064 638 L 1061.9 638.1 L 1061.7 646.8 L 1061.6 655.5 L 1057.2 655.5 C 1046.9 655.6 984.5 655.7 980.3 655.6 L 981.1 656.4Z');
path_ki.attr({'fill':'#ffff7f','stroke':'none','stroke-linecap':'square','stroke-linejoin':'bevel'});

/*** 3rd floor
 */

var path_ma = rsr.path('M 294 515.1 L 288.8 515.1 L 288.8 434.8 L 294 515.1')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_ma');
everythingelse.push(path_ma);

var path_mb = rsr.path('M 294 515.1 L 288.8 434.8 L 294 440.5 L 294 515.1')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mb');
everythingelse.push(path_mb);

var path_mc = rsr.path('M 294 440.5 L 288.8 434.8 L 563.1 434.8 L 294 440.5')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mc');
everythingelse.push(path_mc);

var path_md = rsr.path('M 294 440.5 L 563.1 434.8 L 557.9 440.5 L 294 440.5')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_md');
everythingelse.push(path_md);

var path_me = rsr.path('M 557.9 440.5 L 563.1 434.8 L 563.1 658.4 L 557.9 440.5')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_me');
everythingelse.push(path_me);

var path_mf = rsr.path('M 557.9 440.5 L 563.1 658.4 L 557.9 652.6 L 557.9 440.5')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mf');
everythingelse.push(path_mf);

var path_mg = rsr.path('M 557.9 652.6 L 563.1 658.4 L 288.8 658.4 L 557.9 652.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mg');
everythingelse.push(path_mg);

var path_mh = rsr.path('M 557.9 652.6 L 288.8 658.4 L 294 652.6 L 557.9 652.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mh');
everythingelse.push(path_mh);

var path_mi = rsr.path('M 294 652.6 L 288.8 658.4 L 288.8 543.8 L 294 652.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mi');
everythingelse.push(path_mi);

var path_mj = rsr.path('M 294 652.6 L 288.8 543.8 L 294 543.8 L 294 652.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mj');
everythingelse.push(path_mj);

var path_mk = rsr.path('M 341.4 508 L 341.4 510.8 L 291.4 510.8 L 341.4 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mk');
everythingelse.push(path_mk);

var path_ml = rsr.path('M 341.4 508 L 291.4 510.8 L 291.4 508 L 341.4 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_ml');
everythingelse.push(path_ml);

var path_mm = rsr.path('M 401.7 508 L 401.7 510.8 L 363.4 510.8 L 401.7 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mm');
everythingelse.push(path_mm);

var path_mn = rsr.path('M 401.7 508 L 363.4 510.8 L 366 508 L 401.7 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mn');
everythingelse.push(path_mn);

var path_mo = rsr.path('M 366 508 L 363.4 510.8 L 363.4 437.6 L 366 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mo');
everythingelse.push(path_mo);

var path_mp = rsr.path('M 366 508 L 363.4 437.6 L 366 437.6 L 366 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mp');
everythingelse.push(path_mp);

var path_mq = rsr.path('M 445.9 508 L 445.9 510.8 L 423.8 510.8 L 445.9 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mq');
everythingelse.push(path_mq);

var path_mr = rsr.path('M 445.9 508 L 423.8 510.8 L 426.4 508 L 445.9 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mr');
everythingelse.push(path_mr);

var path_ms = rsr.path('M 426.4 508 L 423.8 510.8 L 423.8 437.6 L 426.4 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_ms');
everythingelse.push(path_ms);

var path_mt = rsr.path('M 426.4 508 L 423.8 437.6 L 426.4 437.6 L 426.4 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mt');
everythingelse.push(path_mt);

var path_mu = rsr.path('M 560.5 508 L 560.5 510.8 L 484.8 510.8 L 560.5 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mu');
everythingelse.push(path_mu);

var path_mv = rsr.path('M 560.5 508 L 484.8 510.8 L 484.8 508 L 560.5 508')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mv');
everythingelse.push(path_mv);

var path_mw = rsr.path('M 483.5 437.6 L 486.1 437.6 L 486.1 483.6 L 483.5 437.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mw');
everythingelse.push(path_mw);

var path_mx = rsr.path('M 483.5 437.6 L 486.1 483.6 L 483.5 483.6 L 483.5 437.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mx');
everythingelse.push(path_mx);

var path_my = rsr.path('M 483.5 535.2 L 486.1 535.2 L 486.1 581.1 L 483.5 535.2')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_my');
everythingelse.push(path_my);

var path_mz = rsr.path('M 483.5 535.2 L 486.1 581.1 L 483.5 581.1 L 483.5 535.2')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_mz');
everythingelse.push(path_mz);

var path_na = rsr.path('M 426 582.6 L 426 579.7 L 560.5 579.7 L 426 582.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_na');
everythingelse.push(path_na);

var path_nb = rsr.path('M 426 582.6 L 560.5 579.7 L 560.5 582.6 L 426 582.6')
.attr({'fill':'#003f7f','stroke':'#003f7f','stroke-width':'0.27','stroke-linecap':'square','stroke-linejoin':'bevel','stroke-opacity':'1'})
.data('id', 'path_nb');
everythingelse.push(path_nb);

var path_nc = rsr.path('M 291 544 L 291 515.3')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nc');
everythingelse.push(path_nc);

var path_nd = rsr.path('M 341.8 509.3 L 363.4 509.3')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nd');
everythingelse.push(path_nd);

var path_ne = rsr.path('M 401.8 509.1 L 423.8 509.1')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_ne');
everythingelse.push(path_ne);

var path_nf = rsr.path('M 446.4 509.3 L 484.5 508.9')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nf');
everythingelse.push(path_nf);

var path_ng = rsr.path('M 484.5 483.7 L 484.5 535.4')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_ng');
everythingelse.push(path_ng);

var path_nh = rsr.path('M 426.2 581.9 L 294 581.9')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nh');
everythingelse.push(path_nh);

var path_ni = rsr.path('M 293.6 549.4 L 446.6 549.4 L 446.6 580.7')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_ni');
everythingelse.push(path_ni);

var path_nj = rsr.path('M 370.2 549.4 L 370.2 581.2')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nj');
everythingelse.push(path_nj);

var path_nk = rsr.path('M 408.7 549.4 L 408.7 581.9')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nk');
everythingelse.push(path_nk);

var path_nl = rsr.path('M 329 549.4 L 329 581.9')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nl');
everythingelse.push(path_nl);

var path_nm = rsr.path('M 293.6 473.3 L 363.8 473.3')
.attr({'fill':'none','stroke':'#000000','stroke-width':'0.8','stroke-linecap':'butt','stroke-linejoin':'miter','stroke-opacity':'1'})
.data('id', 'path_nm');
everythingelse.push(path_nm);

var path_nn = rsr.path('M 341.6 508.3 L 341.5 507.7 L 317.9 507.7 L 294.2 507.6 L 294.2 490.6 L 294.2 473.7 L 328.7 473.7 L 363.2 473.7 L 363.2 491.2 L 363.2 508.8 L 352.5 508.8 L 341.7 508.8 L 341.6 508.3Z')
.attr({'fill':'#52a5a5','stroke':'none'})
.data('id', 'path_nn');
associates.push(path_nn);

var path_no = rsr.path('M 294.2 456.7 L 294.2 472.7 L 328.7 472.7 L 363.2 472.7 L 363.2 456.7 L 363.2 440.7 L 328.7 440.7 L 294.2 440.7 L 294.2 456.7Z')
.attr({'fill':'#52a5a5','stroke':'none'})
.data('id', 'path_no');
associates.push(path_no);

var path_np = rsr.path('M 401.9 508.1 C 401.9 507.6 401.7 507.6 384.1 507.6 L 366.3 507.6 L 366.3 474.2 L 366.3 440.7 L 394.9 440.7 L 423.5 440.7 L 423.5 474.6 L 423.5 508.5 L 412.7 508.5 C 402 508.5 401.9 508.5 401.9 508.1Z')
.attr({'fill':'#bf7fff','stroke':'none','stroke-width':'0.5'})
.data('id', 'path_np');
conferencerooms.push(path_np);

var path_nq = rsr.path('M 446.2 508.3 L 446.1 507.7 L 436.3 507.7 L 426.5 507.6 L 426.5 474.2 L 426.5 440.7 L 454.9 440.7 L 483.2 440.7 L 483.2 462.3 C 483.2 481.3 483.3 483.9 483.6 483.9 C 483.9 483.9 483.9 485.4 483.9 496.1 L 483.9 508.3 L 476.9 508.4 C 473 508.5 464.5 508.6 458.1 508.7 L 446.3 508.8 L 446.2 508.3 L 446.2 508.3Z')
.attr({'fill':'#bf7fff','stroke':'none'})
.data('id', 'path_nq');
conferencerooms.push(path_nq);

var path_nr = rsr.path('M 447.2 564.2 L 447.2 548.9 L 370.7 548.9 L 294.2 548.9 L 294.2 546.3 L 294.2 543.6 L 292.9 543.6 L 291.6 543.6 L 291.6 529.5 L 291.6 515.4 L 292.9 515.4 L 294.2 515.4 L 294.2 513.3 L 294.2 511.2 L 317.9 511.1 L 341.5 511 L 341.6 510.4 L 341.7 509.7 L 352.5 509.7 L 363.2 509.7 L 363.2 510.4 L 363.2 511.2 L 382.6 511.2 L 401.9 511.2 L 401.9 510.3 L 401.9 509.5 L 412.7 509.5 L 423.4 509.5 L 423.5 510.3 L 423.6 511 L 434.8 511 L 446.1 511 L 446.2 510.4 L 446.3 509.8 L 465 509.6 L 483.8 509.4 L 483.9 522.1 C 483.9 533.2 483.9 534.8 483.6 534.9 C 483.3 535.1 483.2 537.8 483.2 557.3 L 483.2 579.5 L 465.2 579.5 L 447.2 579.5 L 447.2 564.2Z')
.attr({'fill':'#ffff7f','stroke':'none'})
.data('id', 'path_nr');
everythingelse.push(path_nr);

var path_ns = rsr.path('M 294.2 565.6 L 294.2 549.8 L 311.3 549.8 L 328.4 549.8 L 328.4 565.6 L 328.4 581.4 L 311.3 581.4 L 294.2 581.4 L 294.2 565.6Z')
.attr({'fill':'#ffb380','stroke':'none'})
.data('id', 'path_ns');
pas.push(path_ns);

var path_nt = rsr.path('M 329.6 565.6 L 329.6 549.8 L 349.6 549.8 L 369.6 549.8 L 369.6 565.6 L 369.6 581.4 L 349.6 581.4 L 329.6 581.4 L 329.6 565.6Z')
.attr({'fill':'#ffb380','stroke':'none'})
.data('id', 'path_nt');
everythingelse.push(path_nt);

var path_nu = rsr.path('M 370.8 565.6 L 370.8 549.8 L 389.4 549.8 L 408.1 549.8 L 408.1 565.6 L 408.1 581.4 L 389.4 581.4 L 370.8 581.4 L 370.8 565.6Z')
.attr({'fill':'#ffb380','stroke':'none'})
.data('id', 'path_nu');
everythingelse.push(path_nu);

var path_nv = rsr.path('M 409.2 565.6 L 409.2 549.8 L 427.6 549.8 L 446 549.8 L 446 564.6 L 446 579.4 L 435.9 579.5 L 425.7 579.6 L 425.6 580.5 L 425.6 581.4 L 417.4 581.4 L 409.2 581.4 L 409.2 565.6Z')
.attr({'fill':'#ffb380','stroke':'none'})
.data('id', 'path_nv');
pas.push(path_nv);

var path_nw = rsr.path('M 486.3 557.2 L 486.2 535 L 485.7 534.9 L 485.1 534.8 L 485.1 523 L 485.1 511.2 L 521.4 511.2 L 557.7 511.2 L 557.7 545.3 L 557.7 579.5 L 522 579.5 L 486.3 579.5 L 486.3 557.2 L 486.3 557.2Z')
.attr({'fill':'#ff7f00','stroke':'none'})
.data('id', 'path_nw');
partners.push(path_nw);

var path_nx = rsr.path('M 485.1 495.8 L 485.1 483.9 L 485.7 483.8 L 486.2 483.8 L 486.3 462.2 L 486.3 440.7 L 522 440.7 L 557.7 440.7 L 557.7 474.2 L 557.7 507.6 L 521.4 507.6 L 485.1 507.6 L 485.1 495.8Z')
.attr({'fill':'#91a552','stroke':'none'})
.data('id', 'path_nx');
everythingelse.push(path_nx);


var path_ny = rsr.path('M 294.1 617.4 L 294.1 582.4 L 392.7 582.8 C 446.9 582.9 506.1 583.1 524.3 583.1 L 557.4 584.1 L 557.4 618.7 L 557.4 653.3 L 425.8 653.3 L 294.1 653.3 L 294.1 618.4Z')
.attr({'fill':'#91a552','stroke':'none'})
.data('id', 'path_ny');
everythingelse.push(path_ny);
/*
 *
 */


var partner_info = {
    'path_ke':{
        name:"Richard Healy",
        employee_id:"RMH"
    },
    'path_gc':{
        name:"Gordon Oldham",
        employee_id:"GDO"
    },
    'path_je':{
        name:"Adam Hugill",
        employee_id:"ASH"
    },
    'path_jf':{
        name:"Alfred Ip",
        employee_id:"ASI"
    },
    'path_jg':{
        name:"Chris Hooley",
        employee_id:"CRH"
    },
    'path_jn':{
        name:"Paul Firmin",
        employee_id:"PEF"
    },
    'path_jo':{
        name:"Stephen Peaker",
        employee_id:"SJP"
    },
    'path_nw':{
        name:"Vera Sung",
        employee_id:"VYS"
    }
};

var associate_info = {
    'path_jb':{
        name:"Selwyn Chan",
        employee_id:"SYC"
    },
    'path_ja':{
        name:"Stephen Chan",
        employee_id:"SNC"
    },

    'path_js':{
        name:"Nicole Cavanagh",
        employee_id:"NYC"
    },

    'path_jh':{
        name:"Tony Chik",
        employee_id:"TC"
    },
    'path_kh':{
        name:"Terence Siu",
        employee_id:"TLS"
    },
    'path_kj':{
        name:"Anna Chan",
        employee_id:"AKC"
    },
    'path_jq':{
        name:"Connie Ma",
        employee_id:"CYM"
    },
    'path_jr':{
        name:"Scherzade Burden",
        employee_id:"SAW"
    },
//    'path_js':{
//        name:"Stephen Chan",
//        employee_id:"SNC"
//    },
    'path_ji':{
        name:"Gary Wong",
        employee_id:"GNW"
    },
    'path_jj':{
        name:"Kenneth Yung",
        employee_id:"KSY"
    },
    'path_jk':{
        name:"Jade Tang",
        employee_id:"JYT"
    },
    'path_jl':{
        name:"Tracy Yip",
        employee_id:"TPY"
    },
    'path_jm':{
        name:"Yvonne Kong - Jojo",
        employee_id:"YWK"
    },
    'path_jp':{
        name:"Pamy Kuo",
        employee_id:"PCK"
    },
    'path_il':{
        name:"Robert Campbell",
        employee_id:"RJC"
    },
    'path_nn':{
        name:"Evelyn Yueng",
        employee_id:"EMY"
    },
    'path_no':{
        name:"Elodie Dellavolta",
        employee_id:"EMD"
    },
    'path_ib':{
        name:"Olivia Kung",
        employee_id:"OYK"
    }
};

var pa_info = {
    'path_iq':{
        name:"reserved",
        employee_id:""
    },
    'path_io':{
        name:"Maggie Wong",
        employee_id:"MKW"
    },
    'path_ip':{
        name:"reserved",
        employee_id:""
    },
//    'path_ig':{
//        name:"reserved",
//        employee_id:""
//    },
    'path_ij':{
        name:"Lisa Lee",
        employee_id:"LTL"
    },
    'path_ik':{
        name:"Louise",
        employee_id:"LML"
    },
    'path_in':{
        name:"Lai Ming",
        employee_id:"LMM"
    },
    'path_im':{
        name:"Helen Li",
        employee_id:"HYL"
    },
    'path_ia':{
        name:"Rebecca Fong",
        employee_id:"RYF"
    },
    'path_1':{
        name:"Vanessa Au-Yeung",
        employee_id:"VAY"
    },
//    
//    'path_ie':{
//        name:"Monita Wong",
//        employee_id:"MCW"
//    },
    'path_2':{
        name:"Nelly Mui",
        employee_id:"NHM"
    },
    'path_ic':{
        name:"Yan Chung",
        employee_id:"YWC"
    },
    'path_hz':{
        name:"Ranjit Dillon",
        employee_id:"RKD"
    },
    'path_hw':{
        name:"Janet Chung - Prumjit",
        employee_id:"JWC"
    },
    'path_hx':{
        name:"Stephanie Tam",
        employee_id:"SYT"
    },
    'path_ie':{
        name:"Dora Yuen",
        employee_id:"DYY"
    },
    'path_hv':{
        name:"Peony Wu",
        employee_id:"PPW"
    },
    'path_hu':{
        name:"Christy So",
        employee_id:"CKS"
    },
    'path_hn':{
        name:"Iris Tsang",
        employee_id:"IYT"
    },
    'path_hm':{
        name:"Madelaine Gomez",
        employee_id:"MLG"
    },
    'path_hl':{
        name:"Brenda Leong",
        employee_id:"BLL"
    },
    'path_ho':{
        name:"Madelaine Cheng",
        employee_id:"MKC"
    },
    'path_hp':{
        name:"Timothy Lee",
        employee_id:"TKL"
    },
    'path_hq':{
        name:"Benny Lai",
        employee_id:"BML"
    },
    'path_hr':{
        name:"Ivy Wong",
        employee_id:"ISW"
    },
    'path_ii':{
        name:"Michael Dalton",
        employee_id:"AMD"
    },
    'path_if':{
        name:"Perry Lam",
        employee_id:"PKL"
    },
//    'path_ih':{
//        name:"Vincent Fong",
//        employee_id:"VSF"
//    },
    'path_ih':{
        name:"Victor Ng",
        employee_id:"VMN"
    },
//    'path_ns':{
//        name:"Collin Cheung",
//        employee_id:"CFW"
//    },
//    'path_nv':{
//        name:"Monita Wong",
//        employee_id:"MCW"
//    }
    'path_hs':{
        name:"Amelia Lo",
        employee_id:"AWL"
    },
    'path_ht':{
        name:"Thomas Lau",
        employee_id:"THL"
    },
};

var cfrm_info = {
    'path_iy':{
        conference_room_id: 1,
        name:"Conference Rm #1"
    },
    'path_iz':{
        conference_room_id: 2,
        name:"Conference Rm #2"
    },
    'path_ix':{
        conference_room_id: 3,
        name:"Conference Rm #3"
    },
    'path_np':{
        conference_room_id: 4,
        name:"Conference Rm #4"
    },
    'path_nq':{
        conference_room_id: 5,
        name:"Conference Rm #5"
    }
}

//for (var i = 0; i < everythingelse.length; i++) {
//    everythingelse[i].mouseover(function(e){
//        this.node.style.opacity = 0.7;
//        document.getElementById('room-name').innerHTML = this.data('id');
//    });
//
//    everythingelse[i].mouseout(function(e){
//        this.node.style.opacity = 1;
//        document.getElementById('room-name').innerHTML = "&nbsp;";
//    });
//}

/*var conference_room_reservations;
$.get('/conference-room/conference-room-reservations', null, function(data) {
    conference_room_reservations = data.conference_rooms;
    var purpose_length = {
        1 : 20,
        2 : 2,
        3 : 15
    }    
    for (var i = 0; i < conferencerooms.length; i++) {
        var conference_room_name;
        $.each(conference_room_reservations, function(ndx, val) {
            if(val.id == cfrm_info[conferencerooms[i].data('id')]['conference_room_id']) {
                conference_room_name = val.name;
                cfrm_info[conferencerooms[i].data('id')]['name'] = conference_room_name;
                cfrm_info[conferencerooms[i].data('id')]['reservations'] = val.reservation;
                //document.getElementById('room-name').innerHTML = cfrm_info[conferencerooms[i].data('id')]['name']+"\n"+cfrm_info[conferencerooms[i].data('id')]['reservations'];
            }
        });

        conferencerooms[i].mouseover(function(e){
            this.node.style.opacity = 0.7;
            document.getElementById('room-name').innerHTML = cfrm_info[this.data('id')]['name'];
        });

        conferencerooms[i].mouseout(function(e){
            this.node.style.opacity = 1;
            document.getElementById('room-name').innerHTML = "&nbsp;";
        });
        labelPath(conferencerooms[i], conference_room_name);
        displayReservations(conferencerooms[i], cfrm_info[conferencerooms[i].data('id')]['reservations'], purpose_length[cfrm_info[conferencerooms[i].data('id')]['conference_room_id']]);
    }
});*/
var d = new Date();
var ev = [];
//console.log(d);
var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
var dstring2= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
//console.log("dstring "+dstring);
//console.log(getUrlVars()["seldate"]==dstring);
//$('#new_conference_room_reservation_main_form').find('input[name="new_reservation_date[]"]').datepicker(datepicker_options);
selectedD=  (getUrlVars()["seldate"] == undefined ? dstring : getUrlVars()["seldate"]);
selectedDT=  ((getUrlVars()["seldate"] == undefined || getUrlVars()["seldate"]==dstring) ? 'today' : getUrlVars()["seldate"]);
$.get('/conference-room/conference-room-reservations', { dateitem :  selectedD}, function(data) {
            //alert(selectedD);
            conference_room_reservations = data.conference_rooms;
            var textattr        = textattr = { 'font-size': 8, stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
           // console.log(data);
            var purpose_length = {
                1 : 40,
                2 : 15,
                3 : 15,
                4 : 2,
                5 : 2
            }    
            for (var i = 0; i < conferencerooms.length; i++) {
                var conference_room_name;
                $.each(conference_room_reservations, function(ndx, val) {
                    if(val.id == cfrm_info[conferencerooms[i].data('id')]['conference_room_id']) {
                        conference_room_name = val.name;
                        if(val.id==4 || val.id==5){
                        		cfrm_info[conferencerooms[i].data('id')]['name'] = conference_room_name.substr(0,10);
                        		if(val.reservation.length>10)
                        			cfrm_info[conferencerooms[i].data('id')]['reservations'] = val.reservation.substr(0,10);
                        		else
                        			cfrm_info[conferencerooms[i].data('id')]['reservations'] = val.reservation;

                        }else{
                        	 cfrm_info[conferencerooms[i].data('id')]['name'] = conference_room_name;
                       		 cfrm_info[conferencerooms[i].data('id')]['reservations'] = val.reservation;
                    	}
                    	 
                    }
                });

                conferencerooms[i].mouseover(function(e){
                    this.node.style.opacity = 0.7;
                    document.getElementById('room-name').innerHTML = cfrm_info[this.data('id')]['name'];
                    //document.getElementById('room-name').innerHTML = cfrm_info[conferencerooms[i].data('id')]['reservations'];   
                     
                });

                conferencerooms[i].mouseout(function(e){
                    this.node.style.opacity = 1;
                    document.getElementById('room-name').innerHTML = "&nbsp;";
                });

                //conferencerooms[i].attr("href","/conference-room/calendar?roomid="+cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                conferencerooms[i].click(function() { showDate(cfrm_info[this.data('id')]); });
                    //clickReservation(cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                
                labelPath(conferencerooms[i], conference_room_name);
                displayReservations(conferencerooms[i], cfrm_info[conferencerooms[i].data('id')]['reservations'], purpose_length[cfrm_info[conferencerooms[i].data('id')]['conference_room_id']],cfrm_info[conferencerooms[i].data('id')]['conference_room_id']);
                //displayCalendar(ev);
                //addImage(conferencerooms[i]);
            }
        });

for (var i = 0; i < partners.length; i++) {
    partners[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = partner_info[this.data('id')]['name'] + " -- " + partner_info[this.data('id')]['employee_id'];
    });

    partners[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    //console.log(partner_info[partners[i].data('id')]['employee_id']);

    labelPath(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    addImage(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    
    //var item = partners[i];
    //console.log($('#'+item.data('id')));
    //addText[$('#'+item.data('id')),partner_info[item.data('id')]['name']];
}

for (var i = 0; i < associates.length; i++) {
    associates[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = associate_info[this.data('id')]['name'] + " -- " + associate_info[this.data('id')]['employee_id'];
    });

    associates[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    labelPath(associates[i], associate_info[associates[i].data('id')]['employee_id']);
    if(associate_info[associates[i].data('id')]['employee_id'] !== "") addImage(associates[i], associate_info[associates[i].data('id')]['employee_id']);
}

for (var i = 0; i < pas.length; i++) {
    pas[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = pa_info[this.data('id')]['name'] + " -- " + pa_info[this.data('id')]['employee_id'];
        //document.getElementById('room-name').innerHTML = this.data('id');
    });

    pas[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    //labelPath(pas[i], pa_info[pas[i].data('id')]['employee_id']);
    //addImage(pas[i]);
}



function addText(p,text)
{
    var t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    var b = p.getBBox();
    t.setAttribute("transform", "translate(" + (b.x + b.width/2) + " " + (b.y + b.height/2) + ")");
    t.textContent = text;
    t.setAttribute("fill", "text");
    t.setAttribute("font-size", "14");
    p.parentNode.insertBefore(t, p.nextSibling);
}

function labelPath( pathObj, text, textattr )
{
    if ( textattr == undefined )
        textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 400 };
    var bbox = pathObj.getBBox();
    //var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + bbox.height / 2, text ).attr( textattr );
    var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + 5, text ).attr( textattr );

    return textObj;
}

function addImage(pathObj, handler_id)
{
    var bbox = pathObj.getBBox();
//    console.log(pathObj.data('id'));
//    console.log(bbox);
    
    var green_icon      = pathObj.paper.image('/assets/thisapp/images/matters/green_xs.png',        bbox.x + bbox.width / 16, bbox.y + (bbox.height * 1 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'green'); });
    var orange_icon     = pathObj.paper.image('/assets/thisapp/images/matters/orange_xs.png',       bbox.x + bbox.width / 16, bbox.y + (bbox.height * 2 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'orange'); });
    var red_icon        = pathObj.paper.image('/assets/thisapp/images/matters/red_xs.png',          bbox.x + bbox.width / 16, bbox.y + (bbox.height * 3 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'red'); });
    var untouched_icon  = pathObj.paper.image('/assets/thisapp/images/matters/untouched_xs.png',    bbox.x + bbox.width / 16, bbox.y + (bbox.height * 4 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'untouched'); });
    var blue_icon       = pathObj.paper.image('/assets/thisapp/images/matters/blue_xs.png',         bbox.x + bbox.width / 16, bbox.y + (bbox.height * 5 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'blue'); });
    
    $.get('/office/matter-count', { handler_array : handler_id }, function(data) {
        //console.log(data);
        var textattr        = textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
        var green_cnt       = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 1 / 5) - 5, data[handler_id]['green'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'green') });
        var orange_cnt      = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 2 / 5) - 5, data[handler_id]['orange'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'orange') });
        var red_cnt         = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 3 / 5) - 5, data[handler_id]['red'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'red') });
        var untouched_cnt   = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 4 / 5) - 5, data[handler_id]['untouched'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'untouched') });
        var blue_cnt        = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 5 / 5) - 5, data[handler_id]['blue'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'blue') });        
    });


    return true;
}

function displayReservations(pathObj, reservations, purpose_length,confid)
{

    var bbox = pathObj.getBBox();
    //var reservation_text='';
    var textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
    //pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * 1 / 5) - 5, '' || 0 );
    if(reservations.length > 0) {
        $.each(reservations, function(ndx,val) {
            //pathObj.paper.text='';
            var item_number = ndx + 1;
            if(item_number<5){
            var purpose_text = (val.purpose.length > purpose_length) ? val.purpose.substr(0, purpose_length) + '...' : val.purpose;
            var time = ((val.start_time.length+val.end_time.length) > purpose_length) ? val.start_time + ' - ' + val.end_time.substr(0, 4) + '...' : val.start_time + ' - ' + val.end_time;
            //reservation_text = val.reservation_date + ' '+ time + ' \nPurpose: ' + purpose_text;
             reservation_text = val.reservation_date + ' '+ time + ' \n - Booked';
            var new_textattr = textattr;
            new_textattr['title'] =  '- Booked';//val.purpose;
            pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * item_number / 5) , reservation_text || 0 ).attr( new_textattr ).click(function() { clickReservation(confid) });


        	}
        	 var monthSource = new Object();
			monthSource.title = val.purpose; // this should be string
			monthSource.start = new Date(); // this should be date object
			monthSource.end = new Date();
			monthSource.resourceId=confid;

		            
    		ev.push(monthSource);
		    //console.log(ev);//only show up to 5 items
        });
    } else {

        pathObj.paper.text( bbox.x + (bbox.width / 16) + 0, bbox.y + (bbox.height * 1 / 5) - 5, 'No reservations for \n'+selectedDT || 0 ).attr( textattr ).click(function() { clickReservation(confid) });
    }

    

	if(reservations.length > 0) {
		        $.each(reservations, function(ndx,val) {
		            //pathObj.paper.text='';
		            //var item_number = ndx + 1;
		            //if(item_number<5){
		            //var purpose_text = (val.purpose.length > purpose_length) ? val.purpose.substr(0, purpose_length) + '...' : val.purpose;
		            //var time = ((val.start_time.length+val.end_time.length) > purpose_length) ? val.start_time + ' - ' + val.end_time.substr(0, 4) + '...' : val.start_time + ' - ' + val.end_time;
		            //reservation_text = val.reservation_date + ' '+ time + ' \nPurpose: ' + purpose_text;
		            //var new_textattr = textattr;
		            //new_textattr['title'] = val.purpose;
		            
		           	//console.log(start);
		        	//}//only show up to 5 items
		        });
	}
}

function getHeight (width) {
  var value = width;
  value *= 1;
  var valueHeight = Math.round((value/16)*9);
  
  return valueHeight;
}

function calculateSize() {
    var width = $('.panel-body').width();
    var height = getHeight(width);
    rsr.setSize(width,height);
}

$(window).resize(calculateSize);


/**
 * On click event for handler by folder types
 *
 * @param   handler_id      e.g. GDO
 * @param   folder_type     possible values (green, orange, red, untouched, blue)
 */ 
function clickFolder(handler_id, folder_type) {
    var type = 0;
    switch(folder_type){
        case 'green': type = 1; break;
        case 'orange': type = 2; break;
        case 'red' : type = 3; break;
        case 'blue': type = 4; break;
        case 'untouched': type = 5; break; 
    }
    window.open("/matters/view/" + type + "/all/" + handler_id);
}

/*function clickReservation() {
    window.open('/conference-room');
}*/

/* Added 09.29.2014 */
//labelPath(path_je, '\nPartner');
//labelPath(path_jf, '\nAssociate');
//labelPath(path_jg, '\nPA');
//labelPath(path_jh, '\nConference\nRoom');
//labelPath(path_ji, '\nGeneral Area');
//labelPath(path_jj, '\nWalkway');

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function displayCalendar(res){
	
			//console.log(res);
		    //var myJsonString = JSON.stringify(res);
		    //console.log(myJsonString);
			 var selectedD;
                $('#complete_confirmation1').on('shown.bs.modal', function () {
                  $("#calendar2").fullCalendar('destroy');
                   $("#calendar2").fullCalendar('render');
                   
                   $("#calendar").fullCalendar('render');

                    var calendar2=$("#calendar2").fullCalendar({
                      selectable:true,
                      select: function(start, end, allDay, jsEvent, view, resource,date) {
                                selectedD=start;
                                //console.log(start);
                                //$("#complete_confirmation2").modal().show();
                               // $("#calendar").fullCalendar('render');
                                $("#calendar").fullCalendar('gotoDate',start);
                                // $("#calendar").fullCalendar('addEventSource', myJsonString);
			 					$("#calendar").fullCalendar('rerenderEvents');
                            },

                    });
                })
               
                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();
            	//$('#calendar').fullCalendar('addEventSource', myJsonString );
               
                var calendar = $("#calendar").fullCalendar({
                    header: {
                        left: "prev,next today",
                        center: "title",
                        right: 'resourceDay'
                    },
                
                    height:900,
                    weekends:false,
                    allDaySlot:false,
                    minTime:"9:00",
                    titleFormat: "ddd, MMM dd, yyyy",
                    defaultView: "resourceDay",
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end, allDay, event, resourceId) {
                        $('#new_start_time').val(start);//val(startDt+" "+startTime);
                        $('#new_end_time').val(start);//val(startDt+" "+startTime);
                        $('#reservationtime').val(start);//val(startDt+" "+startTime);
                         //$("#complete_confirmation1").modal('hide');
                         $("#complete_confirmation").modal().show();
                        calendar.fullCalendar("unselect");
                    },
                    eventResize: function(event, dayDelta, minuteDelta) {
                        //console.log("@@ resize event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.resourceId);
                    },
                    eventDrop: function( event, dayDelta, minuteDelta, allDay) {
                        //console.log("@@ drag/drop event " + event.title + ", start " + event.start + ", end " + event.end + ", resource " + event.resourceId);
                    },
                    
                    editable: true,
              
                    resources: [
                        {
                            name: "Conference Room 1",
                            id: "1"
                        },
                        {
                            name: "Conference Room 2",
                            id: "2"
                        },
                        {
                            name: "Conference Room 3",
                            id: "3"
                        }
                    ], 
                    eventMouseover: function(event, jsEvent, view) {
                      if (view.name !== 'agendaDay') {
                          $(jsEvent.target).attr('title', event.title);
                      }
                  }
                });

				
}




var report_types = ['conference_room'];

var table = {};

var filter = {};
    filter.conference_room = {};

var new_conference_room_log_count = $('.item-container').length;


/* Add item */
    var item_container = $('.item-container').clone().wrap('<div/>').parent().html();
    $('#btn_add_item').bind('click touchstart', function(e){
        e.preventDefault();
        $('#new_conference_room_main_form').append(item_container).focus();
        new_conference_room_log_count++;
    });

var matter = {id: '', type: '', modal: '', descr: '', green: 0, orange: 0, red :0, complete: 0, untouched :0};
var green_tmpl = '';
var orange_tmpl = '';
var red_tmpl = '';
var untouched_tmpl = '';
var finished_tmpl = '';
var matter_id = 0;
var m_clients = {};
var m_employees = {};
var timeline = {cls: '', badge: '', subject: '', datetime: '', info: ''};
var billing_tmpl = "";
var timeline_tmpl = "";
var h_list = "";
var h_list_s = "";
var active = "";
var inactive = "";
var fceval = {CustomerID: '', Incharge: ''};
var all_matters = {};
var green_matters = {};
var orange_matters = {};
var red_matters = {};
var untouched_matters = {};
var finished_matters = {};
var matter_infos = [];

function _green(){
    return {obj: green_matters, next: '#active_next', prev: '#active_prev', con: '#active_div_content', v:17, tmpl: green_tmpl,target: '#openmatter'};
}

function _orange(){
    return {obj: orange_matters, next: '#active_orange_next', prev: '#active_orange_prev', con: '#active_orange_content', v: 18, tmpl: orange_tmpl,target: '#openmatter'};
}

function _red(){
    return {obj: red_matters,next: '#active_red_next', prev: '#active_red_prev', con: '#active_red_content', v: 18, tmpl: red_tmpl,target: '#openmatter'};
}

function _untouched(){
    return {obj: untouched_matters, next: '#untouched_next', prev: '#untouched_prev', con: '#untouched_matter_content', v: 18, tmpl: untouched_tmpl, target: '#openinactivematter'};
}

function _finished(){
    return {obj: finished_matters, next: '#finished_next', prev: '#finished_prev', con: '#finished_matter_content',v: 18, tmpl: finished_tmpl, target: '#openinactivematter'};
}

function process_matters(){
    green_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl >=0 && elem.mn_invl <= 1 && elem.mn_invl != null;});
    var matters_display = green_matters.slice(0,17);
    $('#active_div_content').html($.tmpl(green_tmpl,matters_display));
    orange_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl == 2 && elem.mn_invl != null;});
    matters_display = orange_matters.slice(0,18);
    $('#active_orange_content').html($.tmpl(orange_tmpl,matters_display));
    red_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl > 2 && elem.mn_invl != null;});
    matters_display = red_matters.slice(0,18);
    $('#active_red_content').html($.tmpl(red_tmpl,matters_display));
    untouched_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl == null;});
    matters_display = untouched_matters.slice(0,18);
    $('#untouched_matter_content').html($.tmpl(untouched_tmpl,matters_display));
    finished_matters = $.grep(all_matters,function(elem){return elem.Completed == 1});
    matters_display = finished_matters.slice(0,18);
    $('#finished_matter_content').html($.tmpl(finished_tmpl,matters_display));
}

function fCE(obj){
    var sl = {sel: '', intr: ''};
    $.tmpl(h_list_s,{s: '', e: 'Select a Client'}).appendTo('._matters_clients_');
    $.tmpl(h_list_s,{s: '', e: 'Select an Introducer'}).appendTo('._matters_employee_');
    $.tmpl(h_list_s,{s: '', e: 'Select a Handler'}).appendTo('._matters_handlers_');
    for(var i = 0; i < m_clients.length; i++){
        sl = m_clients[i].CustomerID.localeCompare(obj.CustomerID) == 0 ? {sel: ' selected ', intr: m_clients[i].Incharge} : { sel: '', intr: sl.intr };
        $.tmpl(h_list_s,{s: sl.sel, e: m_clients[i].CustomerID}).appendTo('._matters_clients_');
        if(i < m_employees.length){
            var s2 = m_employees[i].EmployeeID.localeCompare(obj.Incharge) == 0 ? ' selected ' : '';
            $.tmpl(h_list_s,{s: s2, e: m_employees[i].EmployeeID}).appendTo('._matters_handlers_');
        }
    }
    for(var i = 0; i < m_employees.length; i++){
        var s2 = m_employees[i].EmployeeID.localeCompare(sl.intr) == 0 ? ' selected ' : '';
        $.tmpl(h_list_s,{s: s2, e: m_employees[i].EmployeeID}).appendTo('._matters_employee_');
    }
}

function get_info(id_){
    var new_data = {id: id_, info: {},tl:{}, bh:{}, Incharge:''};
    $.get('/matters/matter/'+ id_, function(data){
        new_data.info = data;
        $.get('/matters/introducer/' + data[0].CustomerID , function(info){
            new_data.Incharge=info[0].Incharge;
            $.get('/matters/tl/' + id_, function(info){
                new_data.tl = info;
                $.get('/matters/bh/' + id_, function(info){
                    new_data.bh = info;
                    matter_infos.push(new_data);
                    display_info(new_data);
                },'json');
            },'json');
        },'json');
    },'json');
}

function display_info(obj){
    $('#openmatter').html(active);
    $('#openinactivematter').html(inactive);
    $('.m_current_modal_title').html('Matter No: ' + obj.info[0].JobAssignmentID);
    $('._active_matter_subject').val(obj.info[0].Description_A);
    $('.active_created_job_date').val(obj.info[0].DesignatedDate);
    $('.active_job_assignment_id').val(obj.info[0].JobAssignmentID);
    fceval.CustomerID = ("00000" + obj.info[0].CustomerID).substr(-5,5); 
    fceval.Incharge = obj.info[0].Incharge;
    $('._matters_clients_').val(("00000" + obj.info[0].CustomerID).substr(-5,5));
    $('._matters_handlers_').val(obj.info[0].Incharge);
    $('._matters_employee_').val(obj.Incharge);
    $('._body_billing').html($.tmpl(billing_tmpl,obj.bh));
    var cl_i ={class:'',badge:' success '};
    $.tmpl(timeline_tmpl,{cls: '', badge: ' warning ', subject: 'Matter Accepted', datetime: obj.tl[0].DesignatedDate, info: obj.tl[0].Description_A}).appendTo('._matter_timeline_ul');
    for(var i = 0; i < obj.tl.length; i++){
        var cl = i == 0 ? cl_i : (obj.tl[i].WorkDate == obj.tl[i-1].WorkDate ? cl : (cl.class.length == 0 ? {class: ' class=timeline-inverted ', badge:' info '} : cl_i));
        var subj = obj.tl[i].Comment ? obj.tl[i].Comment.substring(0,12) : '';
        var comm = obj.tl[i].Comment ? obj.tl[i].Comment : '';
        $.tmpl(timeline_tmpl,{cls: cl.class, badge: cl.badge, subject: subj, datetime: obj.tl[i].WorkDate, info: comm}).appendTo('._matter_timeline_ul');
    }
}

function pM(_id){
    matter_id = _id;
    var check = $.grep(matter_infos,function(elem){
        return elem.id == _id;
    });
    if(check.length)
        display_info(check[0])
    else
        get_info(_id);
}

function pF(){
    var matter_display = {};
    $('._holder_green').html('');
    $('._holder_orange').html('');
    $('._holder_red').html('');
    $('._holder_untouched').html('');
    $('._holder_finished').html('');
    $('._filter').html('');
    switch(parseInt($('._matter_type').prop('value'))){
        case 0:
            matter_display = $.grep(green_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(green_tmpl,matter_display).appendTo('._filter'); 
            matter_display = $.grep(orange_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(orange_tmpl,matter_display).appendTo('._filter');
            matter_display = $.grep(red_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(red_tmpl,matter_display).appendTo('._filter'); 
            matter_display = $.grep(untouched_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(untouched_tmpl,matter_display).appendTo('._filter');
            matter_display = $.grep(finished_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(finished_tmpl,matter_display).appendTo('._filter'); 
        break;
        case 1:
            matter_display = $.grep(green_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(green_tmpl,matter_display).appendTo('._filter'); 
        break;
        case 2:
            matter_display = $.grep(orange_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(orange_tmpl,matter_display).appendTo('._filter');
        break;
        case 3:
            matter_display = $.grep(red_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(red_tmpl,matter_display).appendTo('._filter'); 
        break;
        case 4:
            matter_display = $.grep(finished_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(finished_tmpl,matter_display).appendTo('._filter'); 
        break;
        case 5:
            matter_display = $.grep(untouched_matters,function(elem){return elem.Incharge == $('._handlers_').prop('value');});
            $.tmpl(untouched_tmpl,matter_display).appendTo('._filter');
        break;
    }
}

function next(obj){
    var m_obj = {obj: {}, next: '#', prev: '', con: '', v: 0, tmpl: '', target: '#openmatter'};
    m_obj.next += $(obj).prop('id');
    m_obj = m_obj.next.localeCompare('#active_next') == 0 ? _green() : (
                m_obj.next.localeCompare('#active_orange_next') == 0 ? _orange() : (
                    m_obj.next.localeCompare('#active_red_next') == 0 ? _red() : (
                        m_obj.next.localeCompare('#untouched_next') == 0 ? _untouched() : _finished()          
                    )
                )
            );
    $(m_obj.prev).prop('name',parseInt($(m_obj.prev).prop('name')) + m_obj.v);
    $(m_obj.next).prop('name',parseInt($(m_obj.next).prop('name')) + m_obj.v);
    var matters_display = m_obj.obj.slice($(m_obj.prev).prop('name'),$(m_obj.next).prop('name'));
    $(m_obj.con).html($.tmpl(m_obj.tmpl,matters_display));
}

function prev(obj){
    var m_obj = {obj: {},next: '', prev: '#', con: '', m: 0, v: 0, tmpl: '', target: '#openmatter'};
    m_obj.prev += $(obj).prop('id');
    m_obj = m_obj.prev.localeCompare('#active_prev') == 0 ? _green() : (
                m_obj.prev.localeCompare('#active_orange_prev') == 0 ? _orange() : (
                    m_obj.prev.localeCompare('#active_red_prev') == 0 ? _red() : (
                            m_obj.prev.localeCompare('#untouched_prev') == 0 ? _untouched() : _finished()
                    )
                )
            );
    var ndx = parseInt($(m_obj.prev).prop('name')) == 0 ? {prev: 0, next: m_obj.v} : {prev: parseInt($(m_obj.prev).prop('name')) - m_obj.v, next: parseInt($(m_obj.next).prop('name')) - m_obj.v };
    $(m_obj.prev).prop('name',ndx.prev);
    $(m_obj.next).prop('name',ndx.next);
    var matters_display = m_obj.obj.slice($(m_obj.prev).prop('name'),$(m_obj.next).prop('name'));
    $(m_obj.con).html($.tmpl(m_obj.tmpl,matters_display));
}

$(function(){
    $.get("/assets/thisapp/tmpl/matters_con.tmpl",function(tmpl){
    	$('._matters_main_con_').html(tmpl);
    	$.get("/assets/thisapp/tmpl/header_matter.tmpl",function(tmpl){
            $('._header_matter').html(tmpl);
            $.get("/assets/thisapp/tmpl/holder_green.tmpl",function(tmpl){
                $('._holder_green').html(tmpl);
                $.get("/assets/thisapp/tmpl/holder_orange.tmpl",function(tmpl){
                    $('._holder_orange').html(tmpl);
                    $.get("/assets/thisapp/tmpl/holder_red.tmpl",function(tmpl){
                        $('._holder_red').html(tmpl);
                        $.get("/assets/thisapp/tmpl/holder_untouched.tmpl",function(tmpl){
                            $('._holder_untouched').html(tmpl);
                            $.get("/assets/thisapp/tmpl/holder_finished.tmpl",function(tmpl){
                                $('._holder_finished').html(tmpl);
                                $.get("/assets/thisapp/data/info.dat",function(data){
                                    all_matters = data;
                                    $.get("/assets/thisapp/tmpl/_dis_billing.tmpl",function(tmpl){
                                        billing_tmpl=tmpl;
                                        $.get("/assets/thisapp/tmpl/_dis_timeline.tmpl",function(tmpl){
                                            timeline_tmpl=tmpl;
                                            $.get("/assets/thisapp/tmpl/open_matter.tmpl",function(tmpl){
                                                active=tmpl;
                                                $.get("/assets/thisapp/tmpl/inactive_matter.tmpl",function(tmpl){
                                                    inactive=tmpl;
                                                    $.get("/assets/thisapp/tmpl/newmatter.tmpl",function(tmpl){
                                                        $('#newmatter').html(tmpl);
                                                        $.get("/assets/thisapp/tmpl/_dis_m_h.tmpl",function(tmpl){
                                                            h_list=tmpl;
                                                            $.get("/assets/thisapp/tmpl/_dis_matter_green.tmpl",function(tmpl){
                                                                green_tmpl = tmpl;
                                                                $.get("/assets/thisapp/tmpl/_dis_matter_orange.tmpl",function(tmpl){
                                                                    orange_tmpl = tmpl;
                                                                    $.get("/assets/thisapp/tmpl/_dis_matter_red.tmpl",function(tmpl){
                                                                        red_tmpl = tmpl;
                                                                        $.get("/assets/thisapp/tmpl/_dis_matter_untouched.tmpl",function(tmpl){
                                                                            untouched_tmpl=tmpl;
                                                                            $.get("/assets/thisapp/tmpl/_dis_matter_finished.tmpl",function(tmpl){
                                                                                finished_tmpl = tmpl;
                                                                                $.get("/assets/thisapp/tmpl/_dis_m_hs.tmpl",function(tmpl){
                                                                                    h_list_s=tmpl;
                                                                                    process_matters();
                                                                                    $.get('/assets/thisapp/data/handl.dat', function(info){
                                                                                        //m_clients = info.c;
                                                                                        m_employees= info.e;
                                                                                        $.tmpl(h_list,m_employees).appendTo('._handlers_');
                                                                                    },'json');
                                                                                });
                                                                            });
                                                                        });
                                                                    }); 
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                },'json');
                            });
                        });
                    });
                });
            });
        });
    });
});

var client_active = "";
var c_modal="";
var client_inactive = "";
var c_history = "";
var active_obj = {};
var inactive_obj = {};
var obj = {};
var a_nav = {tmpl: '',obj: {}, next: '#active_client_next', prev: '#active_client_prev', con: '#active_client_area', v: 35};
var hist = [];
var current_client = {};
var active_table_ = {};
var all_matters = {};
var all_clients_list = {};

function pCM(){
    active_table_.clear().draw().destroy();
	var client_matters = {};
	switch(parseInt($('.client_matter_type').val())){
		case 1: client_matters = $.grep(current_client.info,function(elem){return elem.Completed == 0 && elem.matter_type == 'green'}); break;
		case 2: client_matters = $.grep(current_client.info,function(elem){return elem.Completed == 0 && elem.matter_type == 'orange' }); break;
		case 3: client_matters = $.grep(current_client.info,function(elem){return elem.Completed == 0 && elem.matter_type == 'red'}); break;
		case 4: client_matters = $.grep(current_client.info,function(elem){return elem.Completed == 1}); break;
		case 5: client_matters = $.grep(current_client.info,function(elem){return elem.Completed == 0 && elem.mn_invl == null}); break;
		default: client_matters = current_client.info; break;
	}
	$('#client_billing_history').html($.tmpl(c_history,client_matters));
	var total_unbilled = 0; var total_billable = 0 ; var total_billed = 0;
	client_matters.forEach(function(elem){
		total_billable += parseFloat(elem.billable_amount);
		total_billed += parseFloat(elem.billed_amount);
		total_unbilled += parseFloat(elem.unbilled);
	});
	client_matters.sort(function(a, b) {
		return (b.billable_amount - a.billable_amount);
	}).sort(function(a, b) {
		return (b.unbilled - a.unbilled);
	});
	$('.c_billable').html('$' + parseFloat(total_billable).formatMoney(2));
	$('.c_billed').html('$' + parseFloat(total_billed).formatMoney(2));
	$('.c_unbilled').html('$' + parseFloat(total_unbilled).formatMoney(2));
        active_table_ = $('#clients_matters_info').DataTable({
            order : [ 7, 'desc' ], pagingType: "full", pageLength : 5, orderClasses : true, bLengthChange : false, bFilter : false
	});

}

function display_info(obj){
	$('#client_billing_history').html($.tmpl(c_history,obj.info));
	$('.c_billable').html('$'+parseFloat(obj.billable).formatMoney(2));
	$('.c_billed').html('$'+parseFloat(obj.billed).formatMoney(2));
	$('.c_unbilled').html('$'+parseFloat(obj.unbilled).formatMoney(2));
	current_client=obj;
        active_table_ = $('#clients_matters_info').DataTable({
            order : [ 7, 'desc' ], pagingType: "full", pageLength : 5, orderClasses : true, bLengthChange : false, bFilter : false
	});
}

function get_info(id){
	var total_billable = 0;
	var total_billed = 0;
	var total_unbilled = 0;
	
	var client_matters = $.grep(all_matters,function(in_elem){
		return in_elem.CustomerID == id;
	});
	

	client_matters.forEach(function(elem){
		total_billable += parseFloat(elem.billable_amount);
		total_billed += parseFloat(elem.billed_amount);
		total_unbilled += parseFloat(elem.unbilled);
	});

	var new_data = {cid : id, billable : total_billable, billed : total_billed, unbilled : total_unbilled, info : client_matters};
	hist.push(new_data);
	display_info(new_data);

}

function dis_client(c_id){
    window.open('/matters/view/7/' + c_id);
    /*
	var data = {CustomerID:("00000" + c_id).substr(-5,5), CustomerName : $('.' + parseInt(c_id)).attr('title'), Introducer : $('.' + parseInt(c_id)).attr('data-introducer')};
	$('.modal_con').html($.tmpl(c_modal,data));
	var check = $.grep(hist,function(elem){
		return parseInt(elem.cid) == parseInt(c_id);
	});
	
	if(check.length)
		display_info(check[0]);
	else
		get_info(c_id);
    */
}

function process(){
	a_nav.tmpl = client_active; a_nav.obj = active_obj;

}

function client_prev(obj){
	var m_obj = $(obj).prop('id').localeCompare('active_client_prev') == 0 ? a_nav : i_nav ;
	var ndx = parseInt($(m_obj.prev).prop('name')) == 0 ? {prev: 0, next: m_obj.v} : {prev: parseInt($(m_obj.prev).prop('name')) - m_obj.v, next: parseInt($(m_obj.next).prop('name')) - m_obj.v };
	$(m_obj.prev).prop('name',ndx.prev);
	$(m_obj.next).prop('name',ndx.next);
	var inner_active = m_obj.obj.slice(ndx.prev, ndx.next);
	$(m_obj.con).html($.tmpl(m_obj.tmpl,inner_active));
}

function client_next(obj){
    console.log(obj);
	var m_obj = $(obj).prop('id').localeCompare('active_client_next') == 0 ? a_nav : i_nav ;
	$(m_obj.prev).prop('name',parseInt($(m_obj.prev).prop('name')) + m_obj.v);
	$(m_obj.next).prop('name',parseInt($(m_obj.next).prop('name')) + m_obj.v);
        console.log(m_obj);
	var inner_active = m_obj.obj.slice(parseInt($(m_obj.prev).prop('name')), parseInt($(m_obj.next).prop('name')));
	$(m_obj.con).html($.tmpl(m_obj.tmpl,inner_active));
}

function process_amounts(data){
	data.forEach(function(elem_c){
		var client_matters = $.grep(all_matters,function(elem_m){return elem_m.CustomerID == elem_c.CustomerID});
		var unbilled = 0;
		var billable = 0;
		var billed = 0;
		if(client_matters.length > 0){
			client_matters.forEach(function(h_m){
				unbilled += parseFloat(h_m.unbilled);
		        billable += parseFloat(h_m.billable_amount);
		        billed +=  parseFloat(h_m.billed_amount);
			});
		}
		elem_c.unbilled = unbilled;
		elem_c.billed = billed;
		elem_c.billable = billable;
	});
	data.sort(function(a, b) {
	    return (b.unbilled - a.unbilled);
	});
	
	return data;
}

function process_search(){
    if($("#search_key").length > 0){
         $('#active_client_area').html("");
        var copy = all_clients_list;
        copy = $.grep(copy,function(elem){
            return elem.CompanyName_A1.toLowerCase().indexOf($('#search_key').val().toLowerCase()) >= 0 ||
                    elem.Incharge.toLowerCase().indexOf($('#search_key').val().toLowerCase()) >= 0;
        });
        active_obj = copy;
        obj = copy;
        var inner_active = active_obj.slice(0,35);
        $('#active_client_area').html($.tmpl(client_active,inner_active));
        process();
    }
}

function active(stat){
    $('#active_client_area').html("");
    var copy = all_clients_list;
    copy = $.grep(copy,function(elem){
        if(stat == 1)
            return elem.LastActivityNweeks <=8;
        else
            return elem.LastActivityNweeks > 8;
    });
    active_obj = copy;
    obj = copy;
    var inner_active = active_obj.slice(0,35);
    $('#active_client_area').html($.tmpl(client_active,inner_active));
    process();
}

$(function(){
    $.get('/assets/thisapp/tmpl/clients_modal.tmpl',function(info){
        c_modal=info;
        $.get('/assets/thisapp/tmpl/_dis_client_active.tmpl',function(info){
            client_active = info;
            $.get('/assets/thisapp/tmpl/_dis_client_inactive.tmpl',function(info){
                client_inactive = info;
                $.get('/matters/mattersdetailslisting' ,function(info){
                    all_matters = info;
                    $.get('/handlers/currentuser',function(data){
                        current_user = data;
                        if(current_user.role != "admin")
                            all_matters = $.grep(all_matters,function(elem){return elem.Incharge == current_user.user});
                        $.get('/clients/clientlist/',function(data){
                            if(current_user.role != "admin")
                                data = $.grep(data,function(elem){return elem.Incharge == current_user.user});
                            all_clients_list = data;
                            active_obj = process_amounts(data);
                            var inner_active = active_obj.slice(0,35);
                            $('#active_client_area').html($.tmpl(client_active,inner_active));
                            if(current_user.role != "admin")
                                    data = $.grep(data,function(elem){return elem.Incharge == current_user.user});
                                obj = active_obj;
                                process();
                                $.get('/assets/thisapp/tmpl/_dis_client_history.tmpl',function(info){
                                    c_history = info;
                                    $('li[data-toggle="tooltip"]').tooltip();
                                });
                        },'json');
                    },'json');
                },'json');
            });
        });
    });
});

var all_matters = {};
var green_matters = {};
var orange_matters = {};
var red_matters = {};
var untouched_matters = {};
var finished_matters = {};
var current_user = "";
var all_acc = {};
var check_this = false;
var extern_flag = false;

function procDes(obj){
    var billable = 0; var billed = 0; var unbilled = 0; 
    obj.matter = $.grep(all_matters,function(elem){
        switch(obj.lvl){
            case 1: return elem.Completed == 0 && elem.matter_type == 'green' && (current_user.role == "admin" && current_user.user == 'ADMIN' ? true : elem.Incharge == current_user.user) == true; break;
            case 2: return elem.Completed == 0 && elem.matter_type == 'orange' && (current_user.role == "admin" && current_user.user == 'ADMIN'  ? true : elem.Incharge == current_user.user) == true; break;
            case 3: return elem.Completed == 0 && elem.matter_type == 'red' && (current_user.role == "admin" && current_user.user == 'ADMIN'  ? true : elem.Incharge == current_user.user) == true; break;
            case 4: return elem.Completed == 1 && (current_user.role == "admin" && current_user.user == 'ADMIN'  ? true : elem.Incharge == current_user.user) == true; break;
            case 5: return elem.matter_type == 'gray' && elem.Completed == 0 && (current_user.role == "admin" && current_user.user == 'ADMIN'  ? true : elem.Incharge == current_user.user) == true; break;
        }
    });
    obj.m_count = Number(obj.matter.length).toLocaleString('en');
    obj.matter.forEach(function(elem){
        billable += elem.billable_amount > 0 ? parseFloat(elem.billable_amount) : 0;
        billed += elem.billed_amount > 0 ? parseFloat(elem.billed_amount) : 0;
    });
    unbilled = billable - billed;
    obj.billable = parseFloat(billable).formatMoney(2);
    obj.billed = parseFloat(billed).formatMoney(2);
    obj.unbilled = parseFloat(unbilled).formatMoney(2);
    obj.Incharge = current_user.user;
    $(obj.id).html($("#_dis_matter_landing_tmpl").tmpl(obj));
}

function process_headers(){

    procDes({lvl : 1, matter : green_matters, type : "green",id:'#green__'});
    procDes({lvl : 2, matter : orange_matters, type : "orange",id:'#orange__'});
    procDes({lvl : 3, matter : red_matters,type :"red",id:'#red__'});
    procDes({lvl : 4, matter : finished_matters, type : "blue",id:'#blue__'});
    procDes({lvl : 5, matter : untouched_matters, type: "gray",id:'#gray__'});
}

$(function(){
    $.get('/matters/mattersdetailslisting', function(data){
        all_matters = data;

            $.get('/handlers/currentuser',function(data){
                current_user = data;
                if(current_user.acc){
                    current_user.user='ADMIN';
                    all_matters.forEach(function(elem){
                        if(elem.CompleteStatus == 1){
                            elem.matter_type = 'red';
                            elem.Completed = 0;
                        }
                        else
                        if(elem.CompleteStatus == 2)
                            elem.matter_type = 'blue';
                        else{
                            elem.matter_type = '';
                            elem.Completed = 0;
                        }
                   });
                }
                process_headers();
                $('li[data-toggle="tooltip"]').tooltip(); 
                if(extern_flag)
                    _matters_landing_js_intern();
            },'json');

    },'json');
});
var green_matters = {};
var orange_matters = {};
var red_matters = {};
var untouched_matters = {};
var finished_matters = {};
var active_searched_matters = {};
var green_tmpl = "";
var orange_tmpl = "";
var red_tmpl = "";
var untouched_tmpl = "";
var finished_tmpl = "";
var paging_tmpl = "";
var gen_tmpl = "";
var pages = {current : 0, prev_p : 0, prev_1 : 0, prev_2 : 0, next_1 : 0, next_2 : 0, next_p : 0, last_p : 0, items : 0, offset : 0, limit: 0, nums : 12};
var _active_ = {matter : {}, tmpl : "", type : 0};

function _next_(){
    if(pages.current + 1 <= pages.last_p){
        $('._filter').html('');
        pages.current ++;
        pages.next_p ++;
        if(pages.prev_1 + 1 < pages.last_p)
                pages.prev_1 ++;
        if(pages.prev_2 + 2 < pages.last_p)
                pages.prev_2 = pages.prev_1 + 1;
        if(pages.prev_2 + 2 > pages.next_p){
                pages.next_1 = pages.prev_2 + 1 < pages.last_p ? pages.prev_2 + 1 : pages.last_p;
                pages.next_2 = pages.next_1 + 1 < pages.last_p ? pages.next_1 + 1 : pages.last_p;
        }else{
                pages.next_1 = pages.next_p - 2;
                pages.next_2 = pages.next_p - 1;
        }

        pages.offset = (pages.current - 1) * pages.nums;
        dis_paging();
        $("#paging_list").html( $.tmpl(paging_tmpl,{'p1' : pages.prev_1, 'p2' : pages.prev_2, 'l1': pages.next_1, 'l2' : pages.next_2}));
    }
}

function _next_1_(){
	pages.current = pages.next_1 - 1;
	_next_();
}

function _next_2_(){
	pages.current = pages.next_2 + 1;
	_next_();
}

function _prev_(){
	if(pages.current - 1 >= 1){
		$('._filter').html('');
		pages.current --;
		pages.prev_p = pages.current - 1;
		pages.next_p --;
		if(pages.prev_1 - 1 > 0){
			pages.prev_1 --;
			pages.prev_2 = pages.prev_1 + 1 < pages.last_p ? pages.prev_1 + 1 : pages.last_p;
		}	
		if(pages.prev_2 + 2 > pages.next_p){
			pages.next_1 = pages.prev_2 + 1 < pages.last_p ? pages.prev_2 + 1 : pages.last_p;
			pages.next_2 = pages.next_1 + 1 < pages.last_p ? pages.next_1 + 1 : pages.last_p;
		}else{
			pages.next_1 = pages.next_p - 2;
			pages.next_2 = pages.next_p - 1;
		}
		
		pages.offset = (pages.current - 1) * pages.nums;
		dis_paging();
		$("#paging_list").html( $.tmpl(paging_tmpl,{'p1' : pages.prev_1, 'p2' : pages.prev_2, 'l1': pages.next_1, 'l2' : pages.next_2}));
	}
	
}

function _prev_1_(){
	pages.current = pages.prev_1 + 1;
	_prev_();
}

function _prev_2_(){
	pages.current = pages.prev_2 + 1;
	_prev_();
}

function _first_(){
	$('._filter').html('');
	init_first();
	dis_paging();
	$("#paging_list").html( $.tmpl(paging_tmpl,{'p1' : pages.prev_1, 'p2' : pages.prev_2, 'l1': pages.next_1, 'l2' : pages.next_2}));
}

function init_first(){
	pages.offset = 0;
	pages.nums = 12;
	pages.last_p = Math.ceil(parseInt(_active_.matter.length)/12);
	pages.current = 1;
	pages.prev_1 = pages.last_p > 1 ? 2 : pages.last_p;
	pages.prev_2 = pages.last_p > 2 ? 3 : pages.last_p;
	pages.next_1 = pages.last_p > 3 ? 4 : pages.last_p;
	pages.next_2 = pages.last_p > 4 ? 5 : pages.last_p;
	pages.next_p = pages.last_p > 2 ? 2 : pages.last_p;
	pages.prev_p = pages.current;
}

function _last_(){
	$('._filter').html('');
	pages.current = pages.last_p;
	pages.next_p = pages.current;
	pages.prev_p = pages.current - 1 > 0 ? pages.current - 1 : 1;
	pages.offset = (pages.current - 1) * pages.nums;
	pages.prev_1 = pages.last_p > 1 ? 2 : pages.last_p;
	pages.prev_2 = pages.last_p > 2 ? 3 : pages.last_p;
	pages.next_1 = pages.next_p - 2 > 0 ? pages.next_p - 2 : pages.last_p;
	pages.next_2 = pages.next_p - 1 > 0 ? pages.next_p - 1 : pages.last_p;
	dis_paging();
	$("#paging_list").html( $.tmpl(paging_tmpl,{'p1' : pages.prev_1, 'p2' : pages.prev_2, 'l1': pages.next_1, 'l2' : pages.next_2}));
}

function dis_paging(){
	var dis_1 = pages.offset + 1;
	pages.limit = pages.offset + pages.nums < _active_.matter.length ? pages.offset + pages.nums : _active_.matter.length;
	if(pages.items == 0)
		pages.items = parseInt(_active_.matter.length);
	var matt_dis = _active_.matter.slice(pages.offset,pages.limit);
	$.tmpl(_active_.tmpl,matt_dis).appendTo('._filter');
	$("#paging_caption").html('Showing '+ dis_1 + ' to '+ pages.limit +' of ' + Number(pages.items).toLocaleString('en') + ' entries')
}

function init_matters(){
	green_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl >=0 && elem.mn_invl <= 1 && elem.mn_invl != null;});
	orange_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl == 2 && elem.mn_invl != null;});
	red_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl > 2 && elem.mn_invl != null;});
	untouched_matters = $.grep(all_matters,function(elem){return elem.Completed == 0 && elem.mn_invl == null;});
	finished_matters = $.grep(all_matters,function(elem){return elem.Completed == 1});
}

function process_headers(){
	init_matters();
	_active_.type = parseInt($('#matter_type').val());

	switch(_active_.type){
		case 1: _active_.matter = green_matters; _active_.tmpl = green_tmpl; dis_paging(); break;
		case 2: _active_.matter = orange_matters; _active_.tmpl = orange_tmpl; dis_paging(); break;
		case 3: _active_.matter = red_matters; _active_.tmpl = red_tmpl; dis_paging(); break;
		case 4: _active_.matter = finished_matters; _active_.tmpl = finished_tmpl; dis_paging(); break;
		case 5: _active_.matter = untouched_matters; _active_.tmpl = untouched_tmpl; dis_paging(); break;
	}
	init_first();
	$("#paging_list").html( $.tmpl(paging_tmpl,{'p1' : pages.prev_1, 'p2' : pages.prev_2, 'l1': pages.next_1, 'l2' : pages.next_2}));
}

function _search_matters(tmpl,matters){
	var matter_desc = $('#matter_desc').val().toLowerCase();
	var matter_code = parseInt($('#matter_code').val());
	var handlers = $('._handlers_').val();
	var dis_mat = {};
	$('._filter').html('');

	dis_mat = $.grep(matters,function(elem){
		if($('#matter_desc').val().length > 0 && $('#matter_code').val().length > 0){
			if(handlers == 1)
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0 && elem.JobAssignmentID == $('#matter_code').val();
			else if(handlers == 0)
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0 && elem.JobAssignmentID == $('#matter_code').val() && elem.Incharge.length == 0;
			else
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0 && elem.JobAssignmentID == $('#matter_code').val() && elem.Incharge == handlers;
		}
		else if($('#matter_desc').val().length > 0){
			if(handlers == 1)
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0;
			else if(handlers == 0)
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0 && elem.Incharge.length == 0;
			else 
				return elem.Description_A.toLowerCase().indexOf($('#matter_desc').val().toLowerCase()) >= 0 && elem.Incharge == handlers;
		}
		else if($('#matter_code').val().length > 0){
			if(handlers == 1)
				return elem.JobAssignmentID == $('#matter_code').val();
			else if(handlers == 0)
				return elem.JobAssignmentID == $('#matter_code').val() && elem.Incharge.length == 0;
			else
				return elem.JobAssignmentID == $('#matter_code').val() && elem.Incharge == handlers;
		}
		else{
			if(handlers == 1)
				return 1;
			else if(handlers == 0)
				return elem.Incharge.length == 0;
			else
				return elem.Incharge == handlers;
		}
	});
	_active_.matter = dis_mat;
	_active_.tmpl = tmpl;
	init_first();
	pages.items = parseInt(dis_mat.length);
	dis_paging();
}

function pF(){
	var matter_display = {};
	$('._filter').html('');
	switch(parseInt($('._matter_type').prop('value'))){
		case 0: _search_matters(gen_tmpl,all_matters); break;
		case 1: _search_matters(green_tmpl,green_matters); break;
		case 2: _search_matters(orange_tmpl,orange_matters); break;
		case 3: _search_matters(red_tmpl,red_matters); break;
		case 4: _search_matters(finished_tmpl,finished_matters); break;
		case 5: _search_matters(untouched_tmpl,untouched_matters); break;
	}
}

$(function(){
	$.get("/assets/thisapp/tmpl/matters_con.tmpl",function(tmpl){
		$('._matters_main_con_').html(tmpl);
		$.get("/assets/thisapp/tmpl/header_matter.tmpl",function(tmpl){
			$('._header_matter').html(tmpl);
			$.get("/assets/thisapp/tmpl/newmatter.tmpl",function(tmpl){
				$('#newmatter').html(tmpl);
				$.get("/assets/thisapp/tmpl/_dis_m_h.tmpl",function(tmpl){
					h_list=tmpl;
					$.get("/assets/thisapp/tmpl/_dis_matter_green.tmpl",function(tmpl){
						green_tmpl = tmpl;
						$.get("/assets/thisapp/tmpl/_dis_matter_orange.tmpl",function(tmpl){
							orange_tmpl = tmpl;
							$.get("/assets/thisapp/tmpl/_dis_matter_red.tmpl",function(tmpl){
								red_tmpl = tmpl;
								$.get("/assets/thisapp/tmpl/_dis_matter_untouched.tmpl",function(tmpl){
									untouched_tmpl=tmpl;
									$.get("/assets/thisapp/tmpl/_dis_matter_finished.tmpl",function(tmpl){
										finished_tmpl = tmpl;
										$.get("/assets/thisapp/tmpl/_dis_m_hs.tmpl",function(tmpl){
											h_list_s=tmpl;
											$.get("/assets/thisapp/tmpl/_dis_paging_list.tmpl",function(tmpl){
												paging_tmpl = tmpl;
												$.get("/assets/thisapp/tmpl/_dis_matter_.tmpl",function(tmpl){
													gen_tmpl = tmpl;
													$.get('/matters/mattersdetailslisting', function(data){
                                                                                                            all_matters = data;
                                                                                                            $.get('/handlers/currentuser',function(data){
                                                                                                                current_user = data;
                                                                                                                if(current_user.user != "all")
                                                                                                                    all_matters = $.grep(all_matters,function(elem){return elem.Incharge == current_user.user});
                                                                                                                process_headers();
                                                                                                                $.get('/handlers/handlersdetailslisting', function(info){
                                                                                                                        m_employees= info.e;
                                                                                                                        $.tmpl(h_list,{EmployeeID : 1}).appendTo('._handlers_');
                                                                                                                        $.tmpl(h_list,m_employees).appendTo('._handlers_');
                                                                                                                        $('li[data-toggle="tooltip"]').tooltip();
                                                                                                                },'json');
                                                                                                            },'json');
                                                                                                        },'json');
												});	
											});
										});
									});
								});
							}); 
						});
					});
				});
			});
		});
	});
});
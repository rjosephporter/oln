var w = 1200;
var h = 705;

var rsr = Raphael('floorplan');

rsr.setViewBox(0, 0, w, h, true);

calculateSize();

rsr.canvas.setAttribute('preserveAspectRatio', 'none');

var partners = [];
var associates = [];
var pas = [];
var walkways = [];

var path_bc = rsr.path("m181.15 319.45v210.09").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.99929 0 0 .98644 0 6.587").data('id', 'path_bc');
var path_bd = rsr.path("m681.87 529.54v-131.31h122.55v21.59").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.99929 0 0 .96791 0 12.332").data('id', 'path_bd');
var path_be = rsr.path("m223.96 758.66v-16.908h34.618 34.618v16.908 16.908h-34.618-34.618v-16.908").attr({
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_be');
var path_bf = rsr.path("m216.17 266.92v-133.05").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: 'none',
    "stroke-dasharray": '8 8',
    stroke: '#000',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bf');
var path_bg = rsr.path("m104.12 322.95h45.52v-56.03").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bg');
var path_bh = rsr.path("m104.12 473.51h45.52v56.03").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bh');
var path_bi = rsr.path("m104.12 438.5h45.52v-52.52h-45.52").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bi');
var path_bj = rsr.path("m16.581 443.75v-3.502h85.79l-85.79 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bj');
var path_bk = rsr.path("m16.581 443.75l85.79-3.502 3.502 3.502h-89.29").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bk');
var path_bl = rsr.path("m105.87 443.75l-3.502-3.502v-57.775l3.502 61.28").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bl');
var path_bm = rsr.path("m105.87 443.75l-3.502-61.28h3.502v61.28").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bm');
var path_bn = rsr.path("m16.581 356.21v-3.501h87.54l-87.54 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bn');
var path_bo = rsr.path("m16.581 356.21l87.54-3.501v3.501h-87.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bo');
var path_bp = rsr.path("m16.581 268.68v-3.502h171.57l-171.57 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bp');
var path_bq = rsr.path("m16.581 268.68l171.57-3.502v3.502h-171.57").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bq');
var path_br = rsr.path("m181.15 321.2v-3.501h152.31l-152.31 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_br');
var path_bs = rsr.path("m181.15 321.2l152.31-3.501-3.501 3.501h-148.81").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bs');
var path_bt = rsr.path("m329.96 321.2l3.501-3.501v211.84l-3.501-208.34").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bt');
var path_bu = rsr.path("m329.96 321.2l3.501 208.34h-3.501v-208.34").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bu');
var path_bv = rsr.path("m105.87 529.54h-3.502v-59.52l3.502 59.52").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bv');
var path_bw = rsr.path("m105.87 529.54l-3.502-59.52h3.502v59.52").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bw');
var path_bx = rsr.path("m331.71 443.75v-3.502h129.56l-129.56 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bx');
var path_by = rsr.path("m331.71 443.75l129.56-3.502v3.502h-129.56").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_by');
var path_bz = rsr.path("m331.71 356.21v-3.501h157.57l-157.57 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_bz');
var path_ca = rsr.path("m331.71 356.21l157.57-3.501v3.501h-157.57").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ca');
var path_cb = rsr.path("m487.53 133.87h3.501v192.58l-3.501-192.58").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cb');
var path_cc = rsr.path("m487.53 133.87l3.501 192.58h-3.501v-192.58").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cc');
var path_cd = rsr.path("m645.1 133.87h3.502v198.71l-3.502-198.71").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cd');
var path_ce = rsr.path("m645.1 133.87l3.502 198.71h-3.502v-198.71").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ce');
var path_cf = rsr.path("m489.28 268.68v-3.502h50.771l-50.771 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cf');
var path_cg = rsr.path("m489.28 268.68l50.771-3.502v3.502h-50.771").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cg');
var path_ch = rsr.path("m802.67 133.87h3.501v200.46l-3.501-200.46").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ch');
var path_ci = rsr.path("m802.67 133.87l3.501 200.46-3.501-3.502v-196.96").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ci');
var path_cj = rsr.path("m802.67 330.83l3.501 3.502h-52.52l49.02-3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cj');
var path_ck = rsr.path("m802.67 330.83l-49.02 3.502v-3.502h49.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ck');
var path_cl = rsr.path("m569.82 266.92h-3.501v-133.06l3.501 133.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cl');
var path_cm = rsr.path("m569.82 266.92l-3.501-133.06h3.501v133.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cm');
var path_cn = rsr.path("m529.55 301.94h3.501v94.54l-3.501-94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cn');
var path_co = rsr.path("m529.55 301.94l3.501 94.54-3.501 3.502v-98.04").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_co');
var path_cp = rsr.path("m529.55 399.98l3.501-3.502h115.55l-119.05 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cp');
var path_cq = rsr.path("m529.55 399.98l119.05-3.502-3.502 3.502h-115.55").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cq');
var path_cr = rsr.path("m645.1 399.98l3.502-3.502v87.54l-3.502-84.04").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cr');
var path_cs = rsr.path("m645.1 399.98l3.502 84.04h-3.502v-84.04").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cs');
var path_ct = rsr.path("m1079.28 809.66h-3.5v-42.02l3.5 42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ct');
var path_cu = rsr.path("m1079.28 809.66l-3.5-42.02h3.5v42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cu');
var path_cv = rsr.path("m646.85 202.15v-3.501h54.27l-54.27 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cv');
var path_cw = rsr.path("m646.85 202.15l54.27-3.501v3.501h-54.27").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cw');
var path_cx = rsr.path("m891.96 531.29v-3.502h110.29l-110.29 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cx');
var path_cy = rsr.path("m891.96 531.29l110.29-3.502v3.502h-110.29").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cy');
var path_cz = rsr.path("m1014.51 671.35v-3.502h157.56l-157.56 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_cz');
var path_da = rsr.path("m1014.51 671.35l157.56-3.502v3.502h-157.56").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_da');
var path_db = rsr.path("m1077.53 739.63h28.89").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: 'none',
    stroke: '#000',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_db');
var path_dc = rsr.path("m1077.53 599.57h28.89").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: 'none',
    stroke: '#000',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dc');
var path_dd = rsr.path("m1172.07 737.88v3.502h-94.54l94.54-3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dd');
var path_de = rsr.path("m1172.07 737.88l-94.54 3.502v-3.502h94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_de');
var path_df = rsr.path("m1172.07 597.82v3.502h-94.54l94.54-3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_df');
var path_dg = rsr.path("m1172.07 597.82l-94.54 3.502v-3.502h94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dg');
var path_dh = rsr.path("m891.96 618.82v-3.501h96.29l-96.29 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dh');
var path_di = rsr.path("m891.96 618.82l96.29-3.501-3.502 3.501h-92.79").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_di');
var path_dj = rsr.path("m984.74 618.82l3.502-3.501v96.29l-3.502-92.79").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dj');
var path_dk = rsr.path("m984.74 618.82l3.502 92.79h-3.502v-92.79").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dk');
var path_dl = rsr.path("m986.5 737.88v3.502h-94.54l94.54-3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dl');
var path_dm = rsr.path("m986.5 737.88l-94.54 3.502v-3.502h94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dm');
var path_dn = rsr.path("m1079.28 571.55h-3.5v-42.02l3.5 42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dn');
var path_do = rsr.path("m1079.28 571.55l-3.5-42.02h3.5v42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_do');
var path_dp = rsr.path("m1079.28 711.61h-3.5v-84.04l3.5 84.04").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dp');
var path_dq = rsr.path("m1079.28 711.61l-3.5-84.04h3.5v84.04").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dq');
var path_dr = rsr.path("m939.23 582.06v-52.52").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dr');
var path_ds = rsr.path("m891.96 582.06h94.54v-52.52").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ds');
var path_dt = rsr.path("m1033.77 531.29v-3.502h138.3l-138.3 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dt');
var path_du = rsr.path("m1033.77 531.29l138.3-3.502v3.502h-138.3").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_du');
var path_dv = rsr.path("m105.87 326.45h-3.502v-59.52l3.502 59.52").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dv');
var path_dw = rsr.path("m105.87 326.45l-3.502-59.52h3.502v59.52").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dw');
var path_dx = rsr.path("m216.17 268.68v-3.502h87.54l-87.54 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dx');
var path_dy = rsr.path("m216.17 268.68l87.54-3.502v3.502h-87.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dy');
var path_dz = rsr.path("m333.47 266.92h-3.501v-133.06l3.501 133.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_dz');
var path_ea = rsr.path("m333.47 266.92l-3.501-133.06h3.501v133.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ea');
var path_eb = rsr.path("m533.05 526.03v7h-316.88l316.88-7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eb');
var path_ec = rsr.path("m533.05 526.03l-316.88 7v-7h316.88").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ec');
var path_ed = rsr.path("m596.08 268.68v-3.502h50.772l-50.772 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ed');
var path_ee = rsr.path("m596.08 268.68l50.772-3.502v3.502h-50.772").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ee');
var path_ef = rsr.path("m984.74 767.64h3.502v42.02l-3.502-42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ef');
var path_eg = rsr.path("m984.74 767.64l3.502 42.02h-3.502v-42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eg');
var path_eh = rsr.path("m1021.51 198.65v3.501h-55.44l55.44-3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eh');
var path_ei = rsr.path("m1021.51 198.65l-55.44 3.501 3.501-3.501h51.939").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ei');
var path_ej = rsr.path("m969.57 198.65l-3.501 3.501v-68.28l3.501 64.778").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ej');
var path_ek = rsr.path("m969.57 198.65l-3.501-64.778h3.501v64.778").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ek');
var path_el = rsr.path("m887.87 200.4h-3.502v-66.53l3.502 66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_el');
var path_em = rsr.path("m887.87 200.4l-3.502-66.53h3.502v66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_em');
var path_en = rsr.path("m1051.27 200.4h-3.5v-66.53l3.5 66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_en');
var path_eo = rsr.path("m1051.27 200.4l-3.5-66.53h3.5v66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eo');
var path_ep = rsr.path("m1077.53 202.15v-3.501h94.54l-94.54 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ep');
var path_eq = rsr.path("m1077.53 202.15l94.54-3.501v3.501h-94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eq');
var path_er = rsr.path("m832.43 202.15v-3.501h107.38l-107.38 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_er');
var path_es = rsr.path("m832.43 202.15l107.38-3.501v3.501h-107.38").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_es');
var path_et = rsr.path("m487.53 382.47h3.501v147.06l-3.501-147.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_et');
var path_eu = rsr.path("m487.53 382.47l3.501 147.06h-3.501v-147.06").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_eu');
var path_ev = rsr.path("m181.15 529.54h35.02").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#7f9fff',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.9723 0 0 .99888 4.889 0").data('id', 'path_ev');

var path_ew = rsr.path("m533.05 529.54h70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#91a552',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ew');

var path_ex = rsr.path("m1111.26 418.85h1v1h-1v-1").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#000',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ex');
var path_ey = rsr.path("m1111.26 309.26h1v1h-1v-1").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#000',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ey');
var path_ez = rsr.path("m1114.3 529.54h-3.5v-81.7l3.5 81.7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ez');
var path_fa = rsr.path("m1114.3 529.54l-3.5-81.7h3.5v81.7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fa');
var path_fb = rsr.path("m1110.8 228.41h3.5v79.95l-3.5-79.95").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fb');
var path_fc = rsr.path("m1110.8 228.41l3.5 79.95-3.5 3.502v-83.45").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fc');
var path_fd = rsr.path("m1110.8 311.86l3.5-3.502h57.77l-61.27 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fd');
var path_fe = rsr.path("m1110.8 311.86l61.27-3.502v3.502h-61.27").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fe');
var path_ff = rsr.path("m1110.8 338.12h3.5v79.951l-3.5-79.951").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ff');
var path_fg = rsr.path("m1110.8 338.12l3.5 79.951-3.5 3.501v-83.45").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fg');
var path_fh = rsr.path("m1110.8 421.57l3.5-3.501h57.77l-61.27 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fh');
var path_fi = rsr.path("m1110.8 421.57l61.27-3.501v3.501h-61.27").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fi');
var path_fj = rsr.path("m842.93 200.4v164.57h86.37v-164.57").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m1 0 0 .9854 0 5.328").data('id', 'path_fj');
var path_fk = rsr.path("m842.93 310.11h86.37").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fk');
var path_fl = rsr.path("m842.93 255.25h86.37").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fl');
var path_fm = rsr.path("m886.12 200.4v164.57").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m1 0 0 .98453 0 5.648").data('id', 'path_fm');
var path_fn = rsr.path("m568.07 332.58v65.65").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m1 0 0 .95909 0 13.607").data('id', 'path_fn');
var path_fo = rsr.path("m1000.5 529.54h-3.5v-107.96l3.5 107.96").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fo');
var path_fp = rsr.path("m1000.5 529.54l-3.5-107.96 3.5-3.501v111.46").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fp');
var path_fq = rsr.path("m1000.5 418.07l-3.5 3.501h-35.01l38.515-3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fq');
var path_fr = rsr.path("m1000.5 418.07l-38.515 3.501v-3.501h38.515").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fr');
var path_fs = rsr.path("m935.72 529.54h-3.501v-107.96l3.501 107.96").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fs');
var path_ft = rsr.path("m935.72 529.54l-3.501-107.96 3.501-3.501v111.46").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ft');
var path_fu = rsr.path("m935.72 418.07l-3.501 3.501h-35.01l38.516-3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fu');
var path_fv = rsr.path("m935.72 418.07l-38.516 3.501v-3.501h38.516").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fv');
var path_fw = rsr.path("m828.93 421.57v-3.501h42.02l-42.02 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fw');
var path_fx = rsr.path("m828.93 421.57l42.02-3.501-3.502 3.501h-38.516").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fx');
var path_fy = rsr.path("m867.44 421.57l3.502-3.501v111.46l-3.502-107.96").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fy');
var path_fz = rsr.path("m867.44 421.57l3.502 107.96h-3.502v-107.96").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_fz');
var path_ga = rsr.path("m727.38 200.4h-3.501v-66.53l3.501 66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ga');
var path_gb = rsr.path("m727.38 200.4l-3.501-66.53h3.501v66.53").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gb');
var path_gc = rsr.path("m750.14 202.15v-3.501h54.27l-54.27 3.501").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gc');
var path_gd = rsr.path("m750.14 202.15l54.27-3.501v3.501h-54.27").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gd');
var path_ge = rsr.path("m646.85 268.68v-3.502h78.78l-78.78 3.502").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ge');
var path_gf = rsr.path("m646.85 268.68l78.78-3.502v3.502h-78.78").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gf');
var path_gg = rsr.path("m699.37 224.91h3.502v42.02l-3.502-42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gg');
var path_gh = rsr.path("m699.37 224.91l3.502 42.02h-3.502v-42.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gh');
var path_gi = rsr.path("m646.85 332.58h78.78v-32.83h-35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gi');
var path_gj = rsr.path("m690.62 332.58v-65.66").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gj');
var path_gk = rsr.path("m181.15 526.03v7h-168.07l168.07-7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gk');
var path_gl = rsr.path("m181.15 526.03l-168.07 7 7-7h161.07").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gl');
var path_gm = rsr.path("m20.08 526.03l-7 7v-402.67l7 395.67").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gm');
var path_gn = rsr.path("m20.08 526.03l-7-395.67 7 7v388.67").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gn');
var path_go = rsr.path("m20.08 137.37l-7-7h1162.5l-1155.5 7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_go');
var path_gp = rsr.path("m20.08 137.37l1155.5-7-7.01 7h-1148.49").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gp');
var path_gq = rsr.path("m1168.57 137.37l7.01-7v682.79l-7.01-675.79").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gq');
var path_gr = rsr.path("m1168.57 137.37l7.01 675.79-7.01-7v-668.78").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gr');
var path_gs = rsr.path("m1168.57 806.15l7.01 7h-287.13l280.12-7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gs');
var path_gt = rsr.path("m1168.57 806.15l-280.12 7 7-7h273.11").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gt');
var path_gu = rsr.path("m895.46 806.15l-7 7v-203.09l7 196.08").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gu');
var path_gv = rsr.path("m895.46 806.15l-7-196.08h7v196.08").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gv');
var path_gw = rsr.path("m895.46 582.06h-7v-49.02l7 49.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gw');
var path_gx = rsr.path("m895.46 582.06l-7-49.02 7-7v56.02").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gx');
var path_gy = rsr.path("m895.46 526.03l-7 7h-285.37l292.38-7").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gy');
var path_gz = rsr.path("m895.46 526.03l-292.37 7v-7h292.38").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_gz');

var path_1 = rsr.path("m843.45 255.59h41.97v53.52h-41.97z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    id: '1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_1');
pas.push(path_1);

var path_2 = rsr.path("m886.3 255.93h41.97v53.52h-41.97z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    id: '2',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_2');
pas.push(path_2);

var path_ha = rsr.path("m916.47 739.63v-122.56").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ha');
var path_hb = rsr.path("m916.47 657.92h-24.51").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.7809 8.7809',
    "stroke-width": '1.1',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.83024 0 0 1 155.58 0").data('id', 'path_hb');
var path_hc = rsr.path("m916.47 698.77h-24.51").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.82853 8.82853',
    "stroke-width": '1.1',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.8213 0 0 1 163.77 0").data('id', 'path_hc');
var path_hd = rsr.path("m986.5 711.61h-24.51v-94.54").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.8822 0 0 .97213 113.32 19.832").data('id', 'path_hd');
var path_he = rsr.path("m1077.53 568.05h-38.51v-38.51").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.92689 0 0 1.003 75.942-1.63").data('id', 'path_he');
var path_hf = rsr.path("m1077.53 631.08h-38.51v38.52 38.51h38.51").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.93099 0 0 1 71.7 0").data('id', 'path_hf');
var path_hg = rsr.path("m1077.53 785.14h-91.03").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.9366 0 0 1 65.6 0").data('id', 'path_hg');
var path_hh = rsr.path("m1077.53 529.54v-78.2h35.02").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.92576 0 0 .96607 80 15.316").data('id', 'path_hh');
var path_hi = rsr.path("m1112.55 306.61h-35.02v-74.7h35.02").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.92532 0 0 1 80.47 0").data('id', 'path_hi');
var path_hj = rsr.path("m1112.55 416.32h-35.02v-74.7h35.02").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.9261 0 0 1 79.63 0").data('id', 'path_hj');
var path_hk = rsr.path("m1018.01 200.4v54.85h-46.69v-54.85").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m1 0 0 .95373 0 11.812").data('id', 'path_hk');
var path_hl = rsr.path("m1018.01 364.97h-46.69v-74.93h46.69v74.93").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_hl');
var path_hm = rsr.path("m743.14 398.23v131.31").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m1 0 0 .96532 0 13.81").data('id', 'path_hm');
var path_hn = rsr.path("m681.87 463.88h122.55").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_hn');

var path_ho = rsr.path("m988.66 785.31h86.52v20.519h-86.52z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ho');
pas.push(path_ho);

var path_hp = rsr.path("m1039.33 671.68h35.85v36.509h-35.85z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hp');
pas.push(path_hp);

var path_hq = rsr.path("m1038.99 630.85h36.19v36.508h-36.19z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hq');
pas.push(path_hq);

var path_hr = rsr.path("m962.47 618.94h21.565v92.31h-21.565z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hr');
pas.push(path_hr);

var path_hs = rsr.path("m896.16 618.94h19.865v38.55h-19.865z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hs');
pas.push(path_hs);

var path_ht = rsr.path("m896.16 658.41h19.525v39.57h-19.525z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ht');
pas.push(path_ht);

var path_hu = rsr.path("m896.16 699.24h19.525v38.21h-19.525z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hu');
pas.push(path_hu);

var path_hv = rsr.path("m896.16 531.85h42.65v49.777h-42.65z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hv');
associates.push(path_hv);

var path_hw = rsr.path("m939.69 531.51h46.39v50.12h-46.39z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hw');
associates.push(path_hw);

var path_hx = rsr.path("m1038.65 531.85h36.53v35.828h-36.53z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hx');
pas.push(path_hx);

var path_hy = rsr.path("m1077.76 451.89h32.45v74.954h-32.45z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hy');
pas.push(path_hy);

var path_hz = rsr.path("m1077.76 342h32.45v73.59h-32.45z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_hz');
pas.push(path_hz);

var path_ia = rsr.path("m1078.1 232.45h32.45v73.59h-32.45z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ia');
pas.push(path_ia);

var path_ib = rsr.path("m971.66 290.63h45.37v73.59h-45.37z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ib');
pas.push(path_ib);

var path_ic = rsr.path("m972 202.85h45.37v51.48h-45.37z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ic');
pas.push(path_ic);

var path_id = rsr.path("m843.45 202.51h41.97v52.16h-41.97z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_id');
pas.push(path_id);

var path_ie = rsr.path("m843.45 310.7h41.97v53.52h-41.97z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ie');
pas.push(path_ie);

var path_if = rsr.path("m886.3 310.36h41.97v53.52h-41.97z").attr({
    fill: '#ffb380',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_if');
pas.push(path_if);

var path_ig = rsr.path("m211.67 496.34c0-1.184-.298-2-.802-2.195-.544-.209-.802-1.061-.802-2.655 0-1.73.211-2.347.802-2.347.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-1.169 0-1.169-7.76 0-8.209 1.185-.455 1.185-11.563 0-11.563-.64 0-.802-.795-.802-3.951 0-3.02.189-4.02.802-4.258.642-.246.802-1.426.802-5.935 0-4.646-.14-5.628-.802-5.628-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275.662 0 .802-.98.802-5.611 0-4.631-.14-5.611-.802-5.611-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275 1.185 0 1.185-11.11 0-11.563-1.169-.449-1.169-8.209 0-8.209.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-1.169 0-1.169-7.76 0-8.209 1.185-.455 1.185-11.563 0-11.563-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275.662 0 .802-.98.802-5.611 0-4.631-.14-5.611-.802-5.611-.646 0-.802-.831-.802-4.275 0-3.444.156-4.275.802-4.275 1.185 0 1.185-11.11 0-11.563-1.169-.449-1.169-8.209 0-8.209.664 0 .802-1.01.802-5.878 0-4.869-.138-5.878-.802-5.878-.641 0-.802-.802-.802-4.01 0-3.206.16-4.01.802-4.01.662 0 .802-.98.802-5.611v-5.611h91.91 91.91v127.18 127.18l-70.937.136-70.937.136-.166 2-.166 2h-20.808-20.808v-1.887").attr({
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72 m.80057 0 0 .8009 12.579 129.87").data('id', 'path_ig');
pas.push(path_ig);

var path_ih = rsr.path("m886.3 202.85h41.97v52.16h-41.97z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ih');
pas.push(path_ih);

var path_ii = rsr.path("m682.25 398.82h59.994v64.41h-59.994z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ii');
pas.push(path_ii);

var path_ij = rsr.path("m743.36 398.48h59.654v64.41h-59.654z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ij');
pas.push(path_ij);

var path_ik = rsr.path("m744.04 464.82h58.29v60.664h-58.29z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ik');
pas.push(path_ik);

var path_il = rsr.path("m682.49 464.48h59.654v60.664h-59.654z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_il');
pas.push(path_il);

var path_im = rsr.path("m533.39 332.9h34.02v63.2h-34.02z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_im');
pas.push(path_im);

var path_in = rsr.path("m568.74 332.9h32.58v63.08h-32.58z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_in');
pas.push(path_in);

var path_io = rsr.path("m648.88 269.11h42.3v63.2h-42.3z").attr({
    "fill-rule": 'evenodd',
    fill: '#52a5a5',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_io');
associates.push(path_io);

var path_ip = rsr.path("m692.26 301.48h32.936v30.591h-32.936z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ip');
pas.push(path_ip);

var path_iq = rsr.path("m626.83 269.22h17.644v63.08h-17.644z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_iq');
pas.push(path_iq);

var path_ir = rsr.path("m106.37 386.35h42.798v51.585h-42.798z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ir');
pas.push(path_ir);

var path_is = rsr.path("m106.37 269h42.798v53.52h-42.798z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_is');
pas.push(path_is);

var path_it = rsr.path("m106.25 474.01h42.798v51.585h-42.798z").attr({
    "fill-rule": 'evenodd',
    fill: '#ffb380',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8.00093 8.00093',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_it');
pas.push(path_it);

var path_iu = rsr.path("m802.67 419.82h3.501v109.71l-3.501-109.71").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_iu');
var path_iv = rsr.path("m802.67 419.82l3.501 109.71h-3.501v-109.71").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_iv');
var path_iw = rsr.path("m645.1 512.03h3.502v17.507l-3.502-17.507").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_iw');
var path_ix = rsr.path("m645.1 512.03l3.502 17.507h-3.502v-17.507").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#003f7f',
    stroke: '#003f7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ix');

var path_iy = rsr.path("m1236.22 496.63c-.919-1.044-1.244-16.828-1.434-69.52l-.245-68.16h-24.857-24.857l-.303 2.629-.303 2.629h-15.233-15.233v-2.868-2.868h-24.857-24.857v2.39 2.39h-15.23c-15.07 0-15.233-.023-15.536-2.151l-.305-2.151-27.01-.254-27.01-.254v2.405 2.405h-12.975c-9.845 0-13.298-.323-14.313-1.338-.989-.989-1.408-4.791-1.605-14.58l-.267-13.241-77.2-.245-77.2-.245v79.83 79.83h-20.08-20.08v-8.604c0-5.099-.389-8.604-.956-8.604-.6 0-.956-6.05-.956-16.253 0-10.198.356-16.253.956-16.253.626 0 .956-19.28.956-55.928v-55.928h-29.16-29.16v-39.675-39.675h-43.02-43.02v-19.12c0-12.11-.351-19.12-.956-19.12-.607 0-.956-7.33-.956-20.08v-20.08h5.258c5.205 0 5.258-.029 5.258-2.868v-2.868h15.775c15.14 0 15.775.077 15.775 1.912 0 1.487.637 1.912 2.868 1.912 1.912 0 2.868-.478 2.868-1.434 0-1.215 2.337-1.434 15.297-1.434h15.297v2.39 2.39h19.11 19.11l.25 39.914.25 39.914 62.859.246 62.859.246v-21.518-21.518h-21.989-20.804l-.296-18.643.148-18.347 20.952-.296h21.989v-2.868-2.868h-14.341-14.341v-25.335-25.335h-2.39-2.39v-12.804c0-10.5.258-12.903 1.434-13.354.789-.303 1.434-1.639 1.434-2.97v-2.42h12.906c10.835 0 12.906.23 12.906 1.434 0 1 1.01 1.434 3.346 1.434 2.337 0 3.346-.432 3.346-1.434 0-1.206 2.125-1.434 13.384-1.434h13.384v2.868c0 2.16.44 2.868 1.78 2.868 1.495 0 1.856.955 2.256 5.975.262 3.286.402 40.28.311 82.22l-.164 76.24h33.76 33.76v-85.09-85.09h15.233 15.233l.303 2.629c.293 2.546.507 2.638 6.756 2.912l6.453.283v101.3 101.3h54.972 54.972v-101.3-101.3l6.453-.283c6.249-.274 6.463-.366 6.756-2.912l.303-2.629h15.169 15.17l.303 2.629c.252 2.191.821 2.679 3.41 2.929l3.107.3v32.923 32.923h30.12 30.12v-32.917-32.917l2.151-.306c1.54-.219 2.237-1.052 2.454-2.935l.303-2.629h15.233c12.903 0 15.233.219 15.233 1.434 0 .956.956 1.434 2.868 1.434 1.912 0 2.868-.478 2.868-1.434 0-1.215 2.33-1.434 15.233-1.434h15.233l.303 2.629.303 2.629 23.18.256 23.18.256v15.28 15.28h-2.39c-1.745 0-2.39.511-2.39 1.893 0 1.821-.792 1.903-20.794 2.151l-20.794.258v47.32 47.32l20.735.257 20.735.257.298 3.089c.248 2.569.74 3.139 2.927 3.391l2.629.303v15.233 15.233h-2.868c-2.221 0-2.868.427-2.868 1.893 0 1.821-.792 1.903-20.794 2.151l-20.794.258v47.32 47.32l20.794.258 20.794.258v3.327c0 3.06.231 3.327 2.868 3.327h2.868v15.297 15.297h-2.868c-2.416 0-2.868.374-2.868 2.371v2.371l-20.794.258-20.794.258-.248 47.563-.248 47.563h-27.24c-17.521 0-27.24.341-27.24.956 0 .606-6.748.943-18.404.92-14.62-.029-18.648-.314-19.592-1.387m20.787-250.01v-47.802h-30.12-30.12v47.802 47.802h30.12 30.12z").attr({
    fill: '#ffff7e',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-dasharray": '8 8',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72 m.80057 0 0 .8009 12.579 129.87").data('id', 'path_iy');
walkways.push[path_iy];

var path_iz = rsr.path("m753.65 200.4v132.18").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: 'none',
    "stroke-dasharray": '8 8',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_iz');
var path_ja = rsr.path("m891.96 582.06v28.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: 'none',
    "stroke-dasharray": '8 8',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ja');
var path_jb = rsr.path("m531.3 332.58h70.35v65.65").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.96191 0 0 .95632 22.987 14.633").data('id', 'path_jb');
var path_jc = rsr.path("m646.85 332.58h-20.36v-65.66").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8 8',
    fill: 'none',
    stroke: '#7f9fff',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72 m.87891 0 0 .9628 75.86 12.37").data('id', 'path_jc');

var path_jd = rsr.path("m646.85 484.02v28.01").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#91a552',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jd');

var path_je = rsr.path("m223.57 672.34v-35.01h70.03v35.01h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#ff7f00',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_je');


var path_jf = rsr.path("m223.57 724.7v-35.01h70.03v35.01h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#52a5a5',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jf');
associates.push(path_jf);

var path_jg = rsr.path("m223.57 777.07v-35.02h70.03v35.02h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    "stroke-dasharray": '8.00093 8.00093',
    stroke: '#7f9fff',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jg');

var path_jh = rsr.path("m527.22 672.34v-35.01h70.03v35.01h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#bf7fff',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jh');

var path_ji = rsr.path("m527.22 724.7v-35.01h70.03v35.01h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#91a552',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_ji');

var path_jj = rsr.path("m527.22 777.07v-35.02h70.03v35.02h-70.03").attr({
    "vector-effect": 'non-scaling-stroke',
    stroke: '#000',
    fill: 'none',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jj');

var path_jk = rsr.path("m223.57 672.34h70.03l-70.03-35.01v35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#ff7f00',
    stroke: '#ff7f00',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jk');

var path_jl = rsr.path("m293.6 672.34v-35.01h-70.03l70.03 35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#ff7f00',
    stroke: '#ff7f00',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jl');

var path_jm = rsr.path("m223.57 724.7h70.03l-70.03-35.01v35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#52a5a5',
    stroke: '#52a5a5',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jm');
var path_jn = rsr.path("m293.6 724.7v-35.01h-70.03l70.03 35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#52a5a5',
    stroke: '#52a5a5',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jn');

var path_jo = rsr.path("m527.22 672.34h70.03l-70.03-35.01v35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#bf7fff',
    stroke: '#bf7fff',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jo');

var path_jp = rsr.path("m597.25 672.34v-35.01h-70.03l70.03 35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#bf7fff',
    stroke: '#bf7fff',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jp');

var path_jq = rsr.path("m527.22 724.7h70.03l-70.03-35.01v35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#91a552',
    stroke: '#91a552',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jq');

var path_jr = rsr.path("m597.25 724.7v-35.01h-70.03l70.03 35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#91a552',
    stroke: '#91a552',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jr');

var path_js = rsr.path("m527.22 777.07h70.03l-70.03-35.01v35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#ffff7e',
    stroke: '#ffff7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_js');

var path_jt = rsr.path("m597.25 777.07v-35.01h-70.03l70.03 35.01").attr({
    "vector-effect": 'non-scaling-stroke',
    fill: '#ffff7e',
    stroke: '#ffff7f',
    "fill-rule": 'evenodd',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square',
    'stroke-width': '0',
    'stroke-opacity': '1'
}).transform("t-12.57-129.72").data('id', 'path_jt');

var path_ju = rsr.path("m172.25 461.65c0-24.542-.132-32.469-.548-32.885-.414-.414-7.125-.548-27.404-.548h-26.856v-1.771c0-2.327-.797-2.924-3.641-2.727l-2.297.159v-15.622-15.622l2.855-.136 2.855-.136.135-3.083.135-3.083h27.15 27.15l.249-1.028c.137-.565.192-15.621.123-33.456l-.126-32.429-27.27-.118-27.27-.118-.139-2.166-.139-2.166-2.626-.137-2.626-.137v-15.477-15.477l1.484-.143 1.484-.143v-2.74-2.74l-1.484-.143-1.484-.143v-15.477-15.477l2.626-.137 2.626-.137.139-2.17.139-2.17h27.1c24.292 0 27.13-.075 27.379-.722.152-.397.277-15.605.277-33.795v-33.07l23.865-.118 23.865-.118.137-2.626.137-2.626h16.625 16.625l.137 2.626.137 2.626h55.27 55.27v-2.855-2.855h15.616 15.616l.142 1.713.142 1.713 2.626.137 2.626.137v30.806 30.806l-95.57.115-95.57.115-.115 130.06-.115 130.06h-18.726-18.726v-32.34").attr({
    fill: '#ffff7e',
    "fill-opacity": '.98',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72 m.8 0 0 .8 12.57 129.72").data('id', 'path_ju');
walkways.push(path_ju);

var path_jv = rsr.path("m651.19 496.14l-.202-2.099-26.319-.169-26.319-.169-.164-89.62-.164-89.62h-2.584-2.584l-.164 89.62-.164 89.62h-95.27-95.27v-50.38-50.38l79.61-.164 79.61-.164v-2.907-2.907l-79.61-.164-79.61-.164v-51.35-51.35l97.05-.164 97.05-.164.201-2.795c.17-2.37-.001-2.848-1.13-3.143-1.298-.339-1.331-.739-1.331-15.775v-15.427h2.261 2.261v-36.17-36.17h24.22 24.22v20.02c0 17.398-.127 20.02-.972 20.02-.86 0-.953 7.138-.807 61.848l.165 61.848 71.86.164 71.86.164v52.38c0 51.835.013 52.38 1.292 52.716 1.249.327 1.292.881 1.292 16.658 0 15.777-.043 16.332-1.292 16.658-1.209.316-1.292.881-1.292 8.788v8.45l-25.999.169-25.999.169-.202 2.099-.202 2.099h-42.549-42.549l-.202-2.099").attr({
    fill: '#91a552',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72 m.8 0 0 .8 12.57 129.72").data('id', 'path_jv');

var path_jw = rsr.path("m1001.62 528.31h30.86v.862h-30.86z").attr({
    fill: '#00f',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_jw');

var path_jx = rsr.path("m333.75 137.13h153.46v215.22h-153.46z").attr({
    fill: '#bf7fff',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_jx');

var path_jy = rsr.path("m20.595 137.78h194.9v126.76h-194.9z").attr({
    fill: '#bf7fff',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_jy');

var path_jz = rsr.path("m216.63 137.54h113v126.66h-113z").attr({
    fill: '#bf7fff',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_jz');

var path_ka = rsr.path("m20.509 356.19h81.42v83.78h-81.42z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ka');
associates.push(path_ka)

var path_kb = rsr.path("m20.609 443.33h81.12v82.58h-81.12z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kb');
associates.push(path_kb)

var path_kc = rsr.path("m753.32 202.47h48.31v127.64h-48.31z").attr({
    fill: '#91a552',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kc');

var path_kd = rsr.path("m530.35 266.81h1.033v34.36h-1.033z").attr({
    fill: '#89a02c',
    "stroke-width": '0.8',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kd');

var path_ke = rsr.path("m491.07 137.36h74.67v126.86h-74.67z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ke');
partners.push(path_ke);

var path_kf = rsr.path("m569.88 137.36h74.15v127.12h-74.15z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kf');
partners.push(path_kf);

var path_kg = rsr.path("m648.16 137.36h75.44v60.975h-75.44z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kg');
associates.push(path_kg)

var path_kh = rsr.path("m726.97 137.62h74.928v60.46h-74.928z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kh');
associates.push(path_kh)

var path_ki = rsr.path("m648.16 201.96h50.38v62.784h-50.38z").attr({
    "stroke-width": '0.8',
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ki');
associates.push(path_ki)

var path_kj = rsr.path("m806.2 137.5h77.46v60.66h-77.46z").attr({
    "stroke-width": '0.8',
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kj');
associates.push(path_kj)

var path_kk = rsr.path("m887.53 137.76h77.55v60.22h-77.55z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kk');
partners.push(path_kk);

var path_kl = rsr.path("m969.27 137.46h77.33v60.59h-77.33z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kl');
partners.push(path_kl);

var path_km = rsr.path("m1050.8 137.65h116.41v60.53h-116.41z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_km');
partners.push(path_km);

var path_kn = rsr.path("m20.322 268.1h81.65v84.23h-81.65z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kn');
associates.push(path_kn)

var path_ko = rsr.path("m1113.75 202.47h53.48v105.03h-53.48z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ko');
associates.push(path_ko)

var path_kp = rsr.path("m1113.88 312.02h53.35v105.16h-53.35z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kp');
associates.push(path_kp)

var path_kq = rsr.path("m1114.01 421.44h53.22v105.29h-53.22z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kq');
associates.push(path_kq)

var path_kr = rsr.path("m1078.87 531.12h88.36v65.755h-88.36z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kr');
associates.push(path_kr)

var path_ks = rsr.path("m1078.87 600.62h88.62v66.66h-88.62z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ks');
associates.push(path_ks)

var path_kt = rsr.path("m1078.87 670.9h88.62v65.756h-88.62z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kt');
partners.push(path_kt);

var path_ku = rsr.path("m1078.87 741.05h88.49v63.69h-88.49z").attr({
    fill: '#ff7f00',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ku');
partners.push(path_ku);

var path_kv = rsr.path("m895.04 740.79h88.49v64.08h-88.49z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kv');
associates.push(path_kv)

var path_kw = rsr.path("m806.03 421.06h60.46v104.12h-60.46z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kw');
associates.push(path_kw)

var path_kx = rsr.path("m870.36 421.06h60.976v105.93h-60.976z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_kx');
associates.push(path_kx)

var path_ky = rsr.path("m935.22 421.57h60.718v105.16h-60.718z").attr({
    fill: '#52a5a5',
    "fill-rule": 'evenodd',
    stroke: 'none',
    'stroke-width':'1',
    'stroke-opacity':'1',
    "stroke-linejoin": 'bevel',
    "stroke-linecap": 'square'
}).transform("t-12.57-129.72").data('id', 'path_ky');
associates.push(path_ky)


var partner_info = {
    'path_ke':{
        name:"Richard Healy",
        employee_id:"RMH"
    },
    'path_kf':{
        name:"Gordon Oldham",
        employee_id:"GDO"
    },
    'path_kk':{
        name:"Vera Sung",
        employee_id:"VYS"
    },
    'path_kl':{
        name:"Alfred Ip",
        employee_id:"ASI"
    },
    'path_km':{
        name:"Chris Hooley",
        employee_id:"CRH"
    },
    'path_kt':{
        name:"Paul Firmin",
        employee_id:"PEF"
    },
    'path_ku':{
        name:"Stephen Peaker",
        employee_id:"SJP"
    }
};

var associate_info = {
    'path_kb':{
        name:"reserved",
        employee_id:""
    },
    'path_ka':{
        name:"reserved",
        employee_id:""
    },
    'path_kn':{
        name:"reserved",
        employee_id:""
    },
    'path_ki':{
        name:"Evelyne Yeung",
        employee_id:"EMY"
    },
    'path_kg':{
        name:"Selwyn Chan",
        employee_id:"SYC"
    },
    'path_kh':{
        name:"Terence Siu",
        employee_id:"TLS"
    },
    'path_kj':{
        name:"Anna Chan",
        employee_id:"AKC"
    },
    'path_kw':{
        name:"Connie Ma",
        employee_id:"CYM"
    },
    'path_kx':{
        name:"Yan Chu",
        employee_id:"YWC"
    },
    'path_ky':{
        name:"Stephen Chan",
        employee_id:"SNC"
    },
    'path_ko':{
        name:"Gary Wong",
        employee_id:"GNW"
    },
    'path_kp':{
        name:"Kenneth Yung",
        employee_id:"KSY"
    },
    'path_kq':{
        name:"Jade Tang",
        employee_id:"JYT"
    },
    'path_kr':{
        name:"Tracy Yip",
        employee_id:"TPY"
    },
    'path_ks':{
        name:"Yvonne Kong - Jojo",
        employee_id:"YWK"
    },
    'path_kv':{
        name:"Pamy Kuo",
        employee_id:"PCK"
    },
    'path_io':{
        name:"Robert Campbell",
        employee_id:"RJC"
    },
    'path_hv':{
        name:"Elodie Dellavolta",
        employee_id:"EMD"
    },
    'path_hw':{
        name:"Charmaine Lam",
        employee_id:"CCL"
    },
    'path_jf':{
        name:"reserved",
        employee_id:""
    }
};

var pa_info = {
    'path_it':{
        name:"reserved",
        employee_id:""
    },
    'path_ir':{
        name:"reserved",
        employee_id:""
    },
    'path_is':{
        name:"reserved",
        employee_id:""
    },
    'path_ig':{
        name:"reserved",
        employee_id:""
    },
    'path_im':{
        name:"Lisa Lee",
        employee_id:"LTL"
    },
    'path_in':{
        name:"Louise",
        employee_id:"LML"
    },
    'path_iq':{
        name:"Lai Ming",
        employee_id:"LMM"
    },
    'path_ip':{
        name:"Helen Li",
        employee_id:"HYL"
    },
    'path_id':{
        name:"Rebecca Fong",
        employee_id:"RYF"
    },
    'path_1':{
        name:"Phil McDonald",
        employee_id:"N/A"
    },
    'path_ie':{
        name:"Olivia Kung",
        employee_id:"OYK"
    },
    'path_ih':{
        name:"Monita Wong",
        employee_id:"MCW"
    },
    'path_2':{
        name:"Nelly Mui",
        employee_id:"NHM"
    },
    'path_if':{
        name:"Anson Douglas",
        employee_id:"AJD"
    },
    'path_ic':{
        name:"Ranjit Dillon",
        employee_id:"RKD"
    },
    'path_ib':{
        name:"Janet Chung - Prumjit",
        employee_id:"JWC"
    },
    'path_ia':{
        name:"Stephanie Tam",
        employee_id:"SYT"
    },
    'path_hz':{
        name:"Dora Yuen",
        employee_id:"DYY"
    },
    'path_hy':{
        name:"Peony Wu",
        employee_id:"PPW"
    },
    'path_hx':{
        name:"Christy So",
        employee_id:"CKS"
    },
    'path_hq':{
        name:"Irene Cheng",
        employee_id:"IYC"
    },
    'path_hp':{
        name:"Madelaine Gomez",
        employee_id:"MLG"
    },
    'path_ho':{
        name:"Brenda Leong",
        employee_id:"BLL"
    },
    'path_hr':{
        name:"Madelaine Cheng",
        employee_id:"MKC"
    },
    'path_hs':{
        name:"Timothy Lee",
        employee_id:"TKL"
    },
    'path_ht':{
        name:"Benny Lai",
        employee_id:"BML"
    },
    'path_hu':{
        name:"Ivy Wong",
        employee_id:"ISW"
    },
    'path_il':{
        name:"Michael Dalton",
        employee_id:"AMD"
    },
    'path_ii':{
        name:"Perry Lam",
        employee_id:"PKL"
    },
    'path_ik':{
        name:"Vincent Fong",
        employee_id:"VSF"
    },
    'path_ij':{
        name:"Victor Ng",
        employee_id:"VMN"
    },
};

for (var i = 0; i < partners.length; i++) {
    partners[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = partner_info[this.data('id')]['name'] + " -- " + partner_info[this.data('id')]['employee_id'];
    });

    partners[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    //console.log(partner_info[partners[i].data('id')]['employee_id']);

    labelPath(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    addImage(partners[i], partner_info[partners[i].data('id')]['employee_id']);
    
    //var item = partners[i];
    //console.log($('#'+item.data('id')));
    //addText[$('#'+item.data('id')),partner_info[item.data('id')]['name']];
}

for (var i = 0; i < associates.length; i++) {
    associates[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = associate_info[this.data('id')]['name'] + " -- " + associate_info[this.data('id')]['employee_id'];
    });

    associates[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    labelPath(associates[i], associate_info[associates[i].data('id')]['employee_id']);
    if(associate_info[associates[i].data('id')]['employee_id'] !== "") addImage(associates[i], associate_info[associates[i].data('id')]['employee_id']);
}

for (var i = 0; i < pas.length; i++) {
    pas[i].mouseover(function(e){
        this.node.style.opacity = 0.7;
        document.getElementById('room-name').innerHTML = pa_info[this.data('id')]['name'] + " -- " + pa_info[this.data('id')]['employee_id'];
    });

    pas[i].mouseout(function(e){
        this.node.style.opacity = 1;
        document.getElementById('room-name').innerHTML = "&nbsp;";
    });

    labelPath(pas[i], pa_info[pas[i].data('id')]['employee_id']);
    //addImage(pas[i]);
}

function addText(p,text)
{
    var t = document.createElementNS("http://www.w3.org/2000/svg", "text");
    var b = p.getBBox();
    t.setAttribute("transform", "translate(" + (b.x + b.width/2) + " " + (b.y + b.height/2) + ")");
    t.textContent = text;
    t.setAttribute("fill", "text");
    t.setAttribute("font-size", "14");
    p.parentNode.insertBefore(t, p.nextSibling);
}

function labelPath( pathObj, text, textattr )
{
    if ( textattr == undefined )
        textattr = { 'font-size': 10, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 400 };
    var bbox = pathObj.getBBox();
    //var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + bbox.height / 2, text ).attr( textattr );
    var textObj = pathObj.paper.text( bbox.x + bbox.width / 2, bbox.y + 5, text ).attr( textattr );

    return textObj;
}

function addImage(pathObj, handler_id)
{
    var bbox = pathObj.getBBox();
    console.log(pathObj.data('id'));
    console.log(bbox);
    
    var green_icon      = pathObj.paper.image('/assets/thisapp/images/matters/green_xs.png',        bbox.x + bbox.width / 16, bbox.y + (bbox.height * 1 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'green'); });
    var orange_icon     = pathObj.paper.image('/assets/thisapp/images/matters/orange_xs.png',       bbox.x + bbox.width / 16, bbox.y + (bbox.height * 2 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'orange'); });
    var red_icon        = pathObj.paper.image('/assets/thisapp/images/matters/red_xs.png',          bbox.x + bbox.width / 16, bbox.y + (bbox.height * 3 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'red'); });
    var untouched_icon  = pathObj.paper.image('/assets/thisapp/images/matters/untouched_xs.png',    bbox.x + bbox.width / 16, bbox.y + (bbox.height * 4 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'untouched'); });
    var blue_icon       = pathObj.paper.image('/assets/thisapp/images/matters/blue_xs.png',         bbox.x + bbox.width / 16, bbox.y + (bbox.height * 5 / 5) - 10, 10, 10).attr({ cursor : 'pointer' }).click(function() { clickFolder(handler_id, 'blue'); });
    
    $.get('/office/matter-count', { handler_array : handler_id }, function(data) {
        //console.log(data);
        var textattr        = textattr = { 'font-size': 8, fill: '#000', stroke: 'none', 'font-family': 'Arial,Helvetica,sans-serif', 'font-weight': 100, 'text-anchor': 'start', 'cursor' : 'pointer' };
        var green_cnt       = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 1 / 5) - 5, data[handler_id]['green'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'green') });
        var orange_cnt      = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 2 / 5) - 5, data[handler_id]['orange'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'orange') });
        var red_cnt         = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 3 / 5) - 5, data[handler_id]['red'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'red') });
        var untouched_cnt   = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 4 / 5) - 5, data[handler_id]['untouched'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'untouched') });
        var blue_cnt        = pathObj.paper.text( bbox.x + (bbox.width / 16) + 12, bbox.y + (bbox.height * 5 / 5) - 5, data[handler_id]['blue'] || 0 ).attr( textattr ).click(function() { clickFolder(handler_id, 'blue') });        
    });


    return true;
}

function getHeight (width) {
  var value = width;
  value *= 1;
  var valueHeight = Math.round((value/16)*9);
  
  return valueHeight;
}

function calculateSize() {
    var width = $('.panel-body').width();
    var height = getHeight(width);
    rsr.setSize(width,height);
}

$(window).resize(calculateSize);


/**
 * On click event for GDO by folder types
 *
 * @param   handler_id      e.g. GDO
 * @param   folder_type     possible values (green, orange, red, untouched, blue)
 */
function clickFolder(handler_id, folder_type) {
    
    window.open("/handlers/matters?type=" + (folder_type == "untouched" ? "gray" : folder_type) + "&handler=" + handler_id);
    //alert('Clicked! Handler: ' + handler_id + ' | Folder: ' + folder_type);
    /*$("#openmatter").modal().show();
    gH2(handler_id,folder_type);
    var val = folder_type == "green" ? 1 :(folder_type == "orange" ? 2: (folder_type == "red" ? 3 : (folder_type == "blue" ? 4 : 5)));
    $("#handler_matters_selection").val(val);
    search();
    */
}

/* Added 09.29.2014 */
$('#handlers').selectize();

labelPath(path_je, '\nPartner');
labelPath(path_jf, '\nAssociate');
labelPath(path_jg, '\nPA');
labelPath(path_jh, '\nConference\nRoom');
labelPath(path_ji, '\nGeneral Area');
labelPath(path_jj, '\nWalkway');

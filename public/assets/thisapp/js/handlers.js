var handler = {};
var dis_h = '';
var empl = {code: '',type : ''};
var row_tmpl = '';
var initials = '';
var g_header = '';
var tabs =  '';
var header = '';
var modal_tmpl = '';
var handlers_list = {};
var all_matters = {};
var active_table_ = {};

function gH(e){
    $('#openmatter').html(modal_tmpl);
    empl.code = e;
    $('.modal-header').html($.tmpl(header,empl));
    $('.tmpl_tabs').html($.tmpl(tabs,empl));
    $('.tmpl_group_header').html($.tmpl(g_header,empl));
    $('.tmpl_initials').html($.tmpl(initials,empl));
    $("#openmatter").modal().show();
    hs();
}

function gH2(e,t){
    $('#openmatter').html(modal_tmpl);
    empl.code = e;
    empl.type = t;
    $('.modal-header').html($.tmpl(header,empl));
    $('.tmpl_tabs').html($.tmpl(tabs,empl));
    $('.tmpl_group_header').html($.tmpl(g_header,empl));
    $('.tmpl_initials').html($.tmpl(initials,empl));
    hs();
}

function search(){
    active_table_.clear().draw().destroy();
    var handler_matters = {};
    switch(parseInt($('#handler_matters_selection').val())){
        case 1: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code && elem.Completed == 0 && elem.matter_type == 'green'}); break;
        case 2: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code && elem.Completed == 0 && elem.matter_type == 'orange' }); break;
        case 3: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code && elem.Completed == 0 && elem.matter_type == 'red'}); break;
        case 4: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code && elem.Completed == 1}); break;
        case 5: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code && elem.Completed == 0 && elem.mn_invl == null}); break;
        default: handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code}); break;
    }
    var total_unbilled = 0; var total_billable = 0 ; var total_billed = 0;
    handler_matters.forEach(function(elem){
        total_billable += parseFloat(elem.billable_amount);
        total_billed += parseFloat(elem.billed_amount);
        total_unbilled += parseFloat(elem.unbilled);
    });
    $('#handler_info').html($.tmpl(row_tmpl,handler_matters));
    $('.h_b1').html('$' + parseFloat(total_billable).formatMoney(2));
    $('.h_b2').html('$' + parseFloat(total_billed).formatMoney(2));
    $('.h_b3').html('$' + parseFloat(total_unbilled).formatMoney(2));
    active_table_ = $('#handler_information_').DataTable({pagingType: "full",pageLength : 8, bLengthChange : false, bFilter : false});
}

function hs(){
    var handler_matters = $.grep(all_matters,function(elem){return elem.Incharge == empl.code});
    var target_handler = $.grep(handlers_list,function(elem){return elem.EmployeeID == empl.code});
    var total_unbilled = 0; var total_billable = 0 ; var total_billed = 0;
    handler_matters.forEach(function(elem){
        total_billable += parseFloat(elem.billable_amount);
        total_billed += parseFloat(elem.billed_amount);
        total_unbilled += parseFloat(elem.unbilled);
    });
    $('#handler_info').html($.tmpl(row_tmpl,handler_matters));
    $('.h_billable').html('$' + parseFloat(total_billable).formatMoney(2));
    $('.h_billed').html('$' + parseFloat(total_billed).formatMoney(2));
    $('.h_unbilled').html('$' + parseFloat(total_unbilled).formatMoney(2));
    $('#handler_nick_name').val(target_handler[0].NickName);
    target_handler[0].photo ? $('#handler_image').attr('src','/assets/thisapp/images/staff/' + target_handler[0].photo) : $('#handler_image').attr('src','/assets/thisapp/images/staff/handler.png');
    active_table_ = $('#handler_information_').DataTable({pagingType: "full",pageLength : 8, bLengthChange : false, bFilter : false});
}

$(function(){
    $.get("/assets/thisapp/tmpl/handler_con.tmpl",function(tmpl){
        $('._handlers_con_').html(tmpl);
        $.get("/assets/thisapp/tmpl/handler_modal.tmpl",function(tmpl){
            modal_tmpl = tmpl;
            $.get("/assets/thisapp/tmpl/_dis_h_h.tmpl",function(tmpl){
                dis_h = tmpl;
                $.get("/assets/thisapp/tmpl/_dis_h_r.tmpl",function(tmpl){
                    row_tmpl=tmpl;
                    $.get("/assets/thisapp/tmpl/_dis_h_initials.tmpl",function(tmpl){
                        initials=tmpl;
                        $.get("/assets/thisapp/tmpl/_dis_h_g_header.tmpl",function(tmpl){
                            g_header=tmpl;
                            $.get("/assets/thisapp/tmpl/_dis_h_tabs.tmpl",function(tmpl){
                                tabs=tmpl;
                                $.get("/assets/thisapp/tmpl/_dis_h_header.tmpl",function(tmpl){
                                    header=tmpl;
                            		$.get('/matters/mattersdetailslisting' ,function(info){
                                        all_matters = info;
                                        $.get('/handlers/currentuser',function(data){
                                            current_user = data;
                                            if(current_user.role != "admin")
                                                all_matters = $.grep(all_matters,function(elem){return elem.Incharge == current_user.user});
	                                    $.get('/handlers/h',function(info){
                                            $.get('/utils/pic/',function(fnames){
                                                var _photos = fnames;
                                                        info.forEach(function(elem_h){
                                                        var handler_matters = $.grep(all_matters,function(elem_m){return elem_m.Incharge == elem_h.EmployeeID});
                                                        var unbilled = 0;
                                                        var billable = 0;
                                                        var billed = 0;
                                                        handler_matters.forEach(function(h_m){
                                                        unbilled +=  parseFloat(h_m.unbilled);
                                                        billable += parseFloat(h_m.billable_amount);
                                                        billed += parseFloat(h_m.billed_amount);
                                                        });
                                                        elem_h.unbilled = unbilled;
                                                        elem_h.billed = billed;
                                                        elem_h.billable = billable;
                                                        var targetphoto = $.grep(_photos,function(elem_photo){return elem_photo.EmployeeID == elem_h.EmployeeID});
                                                        console.log(targetphoto);
                                                        elem_h.photo = targetphoto[0].Photo;

                                                        console.log(elem_h);
                                                    });
                                                        info.sort(function(a, b) {
                                                        return (b.unbilled - a.unbilled);
                                                    });
                                                   handlers_list = info;
                                                                                                   $('._active_handlers').html("");
                                                 //$("#_new_handler_tmpl").tmpl().appendTo('._active_handlers');
                                            for(var i = 0; i < info.length; i++){
                                                   
                                                    handler.id = info[i].EmployeeID;
                                                    handler.type = 'text-success';
                                                    handler.photo = info[i].photo;
                                                    handler.billable = info[i].billable;
                                                    handler.billed = info[i].billed;
                                                    handler.unbilled = info[i].unbilled;
                                                        handler.green = $.grep(all_matters,function(elem){return elem.Incharge == handler.id && elem.matter_type == "green"}).length;
                                                        handler.orange = $.grep(all_matters,function(elem){return elem.Incharge == handler.id&& elem.matter_type == "orange"}).length;
                                                        handler.red = $.grep(all_matters,function(elem){return elem.Incharge == handler.id && elem.matter_type == "red"}).length;
                                                        handler.gray = $.grep(all_matters,function(elem){return elem.Incharge == handler.id && elem.matter_type == "gray"}).length;
                                                        handler.blue = $.grep(all_matters,function(elem){return elem.Incharge == handler.id && elem.matter_type == "blue"}).length;
                                                        handler.all = handler.green + handler.orange + handler.red + handler.gray + handler.blue;
                                                        handler.name = handler.id + " - " + info[i].NickName;
                                                        $.tmpl(dis_h,handler).appendTo('._active_handlers');
                                                 console.log(handler);
                                                /*
                                                if(info[i].DepartmentCode){
                                                    if(info[i].on_leave == 1){
                                                        handler.type = 'text-danger';
                                                        $.tmpl(dis_h,handler).appendTo('._in_active_handlers');
                                                    }else{
                                                        handler.type = 'text-success';
                                                        $.tmpl(dis_h,handler).appendTo('._active_handlers');
                                                    }
                                                }
                                                */
                                            }//end-for
                                            $('div[data-toggle="tooltip"]').tooltip();
                                            },'json');


	                                    },'json');
                                        },"json");
                                    },'json');
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Routes to authenticated users only */
Route::group(array('before' => 'auth'), function()
{
	Route::controller('clients','ClientsController');
	//Route::controller('commcenter','CommcenterController');
	Route::controller('handlers','HandlersController');
	Route::controller('matters','MattersController');
	Route::controller('office','OfficeController');
	Route::controller('reports','ReportsController');
	Route::get('/','WebsiteController@getIndex');
	Route::controller('timesheet','TimesheetController');
	Route::controller('invoice','InvoiceController');
	//Route::controller('accounting','AccountingController');
	Route::controller('dashboard', 'DashboardController');
	Route::controller('conference-room', 'ConferenceRoomController');
	Route::controller('conference-room-reservation', 'ConferenceRoomReservationController');
	Route::controller('account-settings', 'AccountSettingsController');
	Route::controller('employee', 'EmployeeController');
        Route::controller('sysadmin', 'SysadminController');
	Route::post('upload',array('as'=>'upload', 'before'=>'auth','uses'=>'UploadController@index'));
    Route::controller('commcenter','CommcenterController');
    Route::controller('orgchart','OrgchartController');
    Route::controller('global-search','GlobalSearchController');
});

Route::controller('utils','UtilsController');
Route::controller('email','EmailController');
Route::get('/invoice-print/{invoice_id}', 'InvoiceController@getPrint');
Route::get('/invoice-template', 'InvoiceController@getTemplateinvoice1');

/* Routes to admin users only */
Route::group(array('before' => 'admin_access'), function()
{
	//Route::controller('timesheet','TimesheetController');
	//Route::controller('reports','ReportsController');
	//Route::controller('invoice','InvoiceController');
	Route::controller('accounting','AccountingController');
});

//Route::group(array('before' => 'sysadmin_access'), function()
//{
//	Route::controller('sysadmin','SysadminController');
//});

/* Auth Routes */
Route::get('auth/{action}', 'AuthController@getAction');
Route::post('auth/{action}','AuthController@postAction');

/* API route */
Route::controller('api', 'ApiController');

/* Temporary routes for user creation */
Route::get('newuser/{email_address}/{password}', 'AuthController@tempCreateUser');

/* Routes for debugging (testing fragments of codes, etc.) */
Route::controller('debug/richard', 'DebugController');

Route::controller('feedback', 'FeedbackController');
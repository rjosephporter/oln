<?php

class HelperLibrary {

	public static function test()
	{
		return "Hello World! Testing for Helper class!";
	}

	/**
	 * Generate UniqueID based on current timestamp and the current item number to be stored
	 *
	 */
	public static function generateUniqueId($item_number = 1)
	{
		$cur_datetime = date('YmdHis');

		$ms = microtime(true);
		$ms = explode('.',$ms);
		$ms = $ms[1];
		$ms = substr($ms,0,2);
		$ms = str_pad($ms, 2, "0", STR_PAD_RIGHT);

		return $cur_datetime.$ms.$item_number;		
	}

	/**
	 * Log email sent data and other data to database
	 *
	 * @param 	string 	other data to be saved (e.g. json, CSVs, etc.)
	 * @param 	int 	sent_email_type_id (from "sent_email_type" table)
	 */
	public static function logSentEmail($data, $type = 1)
	{
        $email_log = new SentEmailLog;
        $email_log->sent_email_type_id = $type;
        $email_log->date_sent = date('Y-m-d H:i:s');
        $email_log->other_info = $data;
        $email_log->created_by = Auth::user()->EmployeeID;
        $email_log->save();

        return $email_log->id;		
	}

	/**
	 * Get logged in user's principal user
	 *
	 */
	public static function principalUser()
	{
		return Session::has('principal_user') ? Session::get('principal_user') : Auth::id() ;
	}

    /**
     * Convert minutes to hours and minutes
     *
     */
    public static function convertToHoursMins($time, $format = '%d:%d') {
        settype($time, 'integer');
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * Highlights keyword of a given string
     *
     */
    public static function highlightKeyword($str, $search) {
        //$highlightcolor = "#daa732";
        $occurrences = substr_count(strtolower($str), strtolower($search));
        $newstring = $str;
        $match = array();
     
        for ($i=0;$i<$occurrences;$i++) {
            $match[$i] = stripos($str, $search, $i);
            $match[$i] = substr($str, $match[$i], strlen($search));
            $newstring = str_replace($match[$i], '[#]'.$match[$i].'[@]', strip_tags($newstring));
        }
     
        //$newstring = str_replace('[#]', '<span style="color: '.$highlightcolor.';">', $newstring);
        //$newstring = str_replace('[@]', '</span>', $newstring);
        $newstring = str_replace('[#]', '<mark>', $newstring);
        $newstring = str_replace('[@]', '</mark>', $newstring);
        return $newstring;
     
    }

    /**
     * Store additional data into session table
     *
     */
    public static function storeSessionData($data = array()) {
        $current_session_id = Session::getId();
        //$count = DB::table('sessions')->where('id', $current_session_id)->update($data);
        $session_model = Sessions::find($current_session_id);
        foreach($data as $key => $value) {
            $session_model->$key = $value;
        }
        $session_model->save();
    }

    /**
     * Get Employee's hourly rate
     *
     */
    public static function getHourlyRate($employee_id) {
        $user = new User();
        $handler_user_id = $user->getUserID($employee_id);
        $role = $user->getEmployeeJob(array($handler_user_id));
        return $role[0]->HourlyRate;          
    }
        
        public static function setInvoiceAsFinal($matter_id){
            $u = new Utilities;
            $obj = array('matter' => $matter_id, 'msg' => 'No Action', 'batch_number' => '');
            $lastinvoice = $u->getTimeSheet($matter_id);
            if(count($lastinvoice) > 0):
                $u->setInvoiceAsFinal($lastinvoice[0]->BatchNumber);
                $obj['msg'] = 'Invoice set to Final';
                $obj['batch_number'] = $lastinvoice[0]->BatchNumber;
            endif;
            
            return Response::json(array('obj' => $obj));
        }
        
        public static function LegallyCloseMatter($obj){
        $j = new JobAssignment;
        $u = new Utilities;

        $j->completeMatter($obj);
        $u->setCloseInfo(array(
            'date' => date("Y-m-d"),
            'time' => date("H:i:s"),
            'jobid' => $obj['matter_id'],
            'boxno' => $obj['box_number'],
            'closedby' => $obj['current_user'],
            'role' => $obj['CloseStatus']
        ));
        if($obj['CloseStatus'] == 1):
            HelperLibrary::setInvoiceAsFinal($obj['matter_id']);
        endif; 
        $matters = json_encode($j->getMattersBill());
        $u->saveDefaults(array('date' => date("Y-m-d"), 'time' => date("H:i:s"), 'data' => bzcompress($matters), 'name' => 'matters'));
        }
        
        public static function closeMatterbyInvoice($matter_id, $box_number, $invoice_type){
            /*
             * Invoice Type Flag:
             * 1 = Intermediary Invoice
             * 2 = Final Invoice
             */
            
            $msg = "No Action";
            $obj = array();
            
            if($invoice_type == 2):
                $u = new Utilities;
                $j = new JobAssignment;
                $obj['matter_id'] = $matter_id;
                $obj['box_number'] = $box_number;
                $obj['current_user'] = User::getEmployeeID(Session::get("principal_user"));
                $obj['datetime'] = date("Y-m-d H:i:s");
                $j->completeMatter($obj);
                $u->setCloseInfo(array(
                    'date' => date("Y-m-d"),
                    'time' => date("H:i:s"),
                    'jobid' => $obj['matter_id'],
                    'boxno' => $obj['box_number'],
                    'closedby' => $obj['current_user'],
                    'role' => 1
                ));
                $msg = "Matter Legally Closed Through Invoice";
                HelperLibrary::setInvoiceAsFinal($matter_id);
                $u->resetFlag();
            endif;
                            
            return Response::json(array('msg' => $msg, 'details' => $obj));
        }
        
        public static function RefreshMatters(){
            $j = new JobAssignment;
            $u = new Utilities;
        
            $bills = json_encode($j->getMattersBill());
            $u->saveDefaults(array('date' => date("Y-m-d"), 'time' => date("H:i:s"), 'data' => bzcompress($bills), 'name' => 'matters'));
            $u->setFlag();
            
            return Response::json(array('message' => 'Matters Updated '.date("Y-m-d H:i:s")));
        }
        
        public static function RefeshHandlers(){
            $j = new JobAssignment;
            $u = new Utilities;
            
            $handlers = json_encode(array('e'=>$j->getE()));
            $u->saveDefaults(array('date' => date("Y-m-d"), 'time' => date("H:i:s"), 'data' => bzcompress($handlers), 'name' => 'handlers'));
            
            return Response::json(array('message' => 'Handlers Updated '.date("Y-m-d H:i:s")));
        }
        
         public static function MattersListing($type,$client,$handler,$update,$bill_order,$delegated='',$ip=''){
            $u = new Utilities;
            $user = new User;
           
            $current_user = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
            $user_id = User::getEmployeeID($current_user);
            $obj['type'] = $type;
            $obj['reg_user'] = User::getEmployeeID(Auth::user()->user_id);
            $obj['user'] = User::hasRoleById($current_user, 'Admin') ? 'admin' : $user_id;
            $obj['role'] = User::hasRoleById($current_user, 'Accounting') ? 'accounting' : 'others';
            $obj['openrights'] = User::hasRoleById(Auth::user()->user_id, 'Accounting') ? 1 : 0;
            $obj['handler'] = $user_id == 'ADMIN' && $handler == 'ADMIN' ? 'all' : (($handler == 'ADMIN' || $handler == 'all') && $user_id != 'ADMIN' ? $user_id : $handler);
            $obj['current'] = $user_id;
            $obj['handlers_list']= $user->getEmployeesByRoles("Handler");
            $obj['ip'] = $ip;
            $obj['special_matters'] = array(); 
            switch($type):
                case 1: $obj['type'] = "green"; break;
                case 2: $obj['type'] = "orange"; break;
                case 3: $obj['type'] = "red"; break;
                case 4: $obj['type'] = "blue"; break;
                case 5: $obj['type'] = "gray"; break;
                case 7: $obj['type'] = "all"; break;
            endswitch;
            
            $tmp    = array();
            $tmph   = array();
            $obj['delegated'] = $delegated;
            $obj['delegated_by'] = array();
            $obj['delegated_to'] = array();
            $delegated_by = $u->getMattersDelegated($user_id,'by');
            if(count($delegated_by) > 0):
                foreach ($delegated_by as $row):
                    $r = (array) $row;
                    if (!in_array($r['JobAssignmentID'],$tmp)){ 
                        array_push($tmp,$r['JobAssignmentID']);
                        array_push($tmph,$r['delegatedTo']);
                    }
                endforeach;
                $obj['delegated_by'] = $tmp;
                $obj['delegatedmatters']    = $tmp;
                $obj['deleg_handlers']    = $tmph;      
            endif;
        
            $delegated_to = (array)$u->getMattersDelegated($user_id,'to');
            if(count($delegated_to) > 0):
                foreach ($delegated_to as $row):
                     $r = (array) $row;
                    if (!in_array($r['JobAssignmentID'],$tmp)){
                        array_push($tmp,$r['JobAssignmentID']);
                        array_push($tmph,$r['delegatedBy']);
                    }
                endforeach;
                $obj['delegated_to'] = $tmp;
                $obj['delegatedmatters']    = $tmp;
                $obj['deleg_handlers']    = $tmph;
            endif;
            
            if($ip == 'ip')
                $active_matters = $u->getMattersIP();
            else{
                $check = $u->getFlag();
                if($update || $check):
                    HelperLibrary::RefreshMatters();
                endif;
                $data = $u->retrieveDefaults('matters');
                $active_matters = json_decode(bzdecompress($data[0]->data));
            }
              
            if(!$delegated){
                if($obj['role'] == 'accounting' || $handler == 'TLS'):
                    $obj['handler'] = 'TLS';
                    foreach($active_matters as $obj_matter):
                        if($obj_matter->CompleteStatus == 1 ){
                            $obj_matter->matter_type = 'red';
                            $obj_matter->Completed = 0;
                        }
                        else
                        if($obj_matter->CompleteStatus == 2)
                            $obj_matter->matter_type = 'blue';
                        else{
                            $obj_matter->matter_type = '';
                            $obj_matter->Completed = 0;
                        }
                    endforeach;
                    $active_matters = array_values(array_filter($active_matters,function($info){
                            return $info->CompleteStatus > 0;
                        })
                    );
                endif;
                if($obj['handler'] == $obj['user'] || $obj['user'] == 'admin'):
                    $obj['client'] = $client;
                    $obj['billing'] = $bill_order;
                    $obj['matters'] = array_values(array_filter($active_matters,function($info) use ($obj){
                            return ($obj['type'] == 'all' ? 1 : $info->matter_type == $obj['type']) && ($obj['handler'] == 'all' || $obj['handler'] == 'TLS' ? 1 : $info->Incharge == $obj['handler']) && ($obj['client'] == 'all' ? 1 : $obj['client'] == $info->CustomerID);
                        })
                    );
                endif;
            }else{
                $obj['handler'] = $handler;
                $obj['billing'] = $bill_order;
                $obj['client'] = $client;
                $deleg_matters = $obj['delegatedmatters'];
                $obj['temp_deleg'] = array();
                foreach ($deleg_matters as $deleg_matter):
                    array_push($obj['temp_deleg'] ,$deleg_matter);
                endforeach;
         
                $obj['matters'] = array_values(array_filter($active_matters,function($info) use ($obj){
                        return in_array($info->JobAssignmentID, $obj['temp_deleg']) && ($obj['type'] == 'all' ? 1 : $info->matter_type == $obj['type']);
                    })
                );
            }

            return $obj;
        }
        
        public static function getSpecialMatters(){
            return DB::select("SELECT JobAssignmentID, Description_A FROM jobassignment WHERE Completed = 10 ORDER BY JobAssignmentID ASC");
        }
        
        public static function createLoginHistory($data = array())
        {
            // close logins which have expired
            DB::update('
                    UPDATE
                    user_login_history
                    SET 
                    logout = ?,
                    logged_in = 0
                    WHERE
                    session_id NOT IN
                    (SELECT session_id FROM sessions)
                    ',array(date('Y-m-d H:i:s')));
            //insert new login
            if(Auth::check()){
                return DB::insert("
                    INSERT INTO 
                    user_login_history
                    (user_id, session_id, from_ip,login,logged_in)
                    SELECT ?,?,?,?,1
                    FROM dual
                    WHERE NOT EXISTS
                    (SELECT 
                    * 
                    FROM 
                    user_login_history 
                    WHERE user_id = ?
                    AND session_id = ?)
                    ",array($data['user_id'],$data['session_id'],$data['ip_address'],date('Y-m-d H:i:s'),$data['user_id'],$data['session_id']));
            }
        }   
        
        public static function logoutLoginHistory($data = array())
        {
            return DB::update('
                    UPDATE
                    user_login_history
                    SET 
                    logout = ?,
                    logged_in = 0
                    WHERE
                    session_id = ?
                    AND user_id = ?
                    ',array(date('Y-m-d H:i:s'),$data['session_id'],$data['user_id']));
        }
}
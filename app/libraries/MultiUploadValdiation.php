<?php

class MultiUploadValidation
{

    protected $array = array();
    protected $errors = array();
    protected $field;
    protected $rules;

    public static function factory(array $array, $field)
    {
        return new MultiUploadValidation($array, $field);
    }

    public function __construct(array $array, $field)
    {
        $this->array[$field] = arr::rotate($array[$field]);
        $this->field = $field;
    }

    public function set_rules($rules)
    {
        $this->rules = func_get_args();
    }

    public function validate()
    {
        $field = $this->field;
        $array = $this->array;
        $rules = $this->rules;
        $result = TRUE;
        foreach ($array[$field] as $key => $f)
        {
            $g[$field] = $f;
            $validations[$key] = Validator::make($g);
            foreach ($this->rules as $rule)
            {
                if(isset($rule[1])){
                    $validations[$key]->rule($field, $rule[0],$rule[1]);
                } else {
                    $validations[$key]->rule($field, $rule[0]);
                }
            }
            $temp = $validations[$key]->check();
            $result = $result && $temp;
            $this->errors[$key] = $validations[$key]->errors();
        }
        return $result;
    }

    public function errors()
    {
        return $this->errors;
    }
}

?>
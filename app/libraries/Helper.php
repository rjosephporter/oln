<?php

class Helper {
    public static function search_array_r($array, $key, $value)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, Helper::search_array_r($subarray, $key, $value));
            }
        }

        return $results;
    }
    public static function save_file($files,$options=array())
    {
        if(empty($options) == FALSE){
            $file_limit = $options['file_limit'];
            $upload_dir = $options['upload_dir'];
            $upload_type = $options['upload_type'];
            $thumb_dir = $options['thumb_dir'];
            $basename_only = $options['basename_only'];
        } else {
            $file_limit = '4000000';
            $upload_dir = '';
            $thumb_dir = '';
            $upload_type = array('txt','xls','xlsx','doc','docx','pdf','jpg','jpeg', 'png', 'gif');
            $basename_only = false;
        }

        foreach(Input::file('fileupload') as $file){
            if($file->isValid()){
                $rules = array(
                    'file' => 'required|mimes:'.join(',',$upload_type).'|max:'.$file_limit
                );
                $validator = Validator::make(array('file'=> $file), $rules);
                if($validator->passes()){
                    $ext = $file->guessClientExtension(); // (Based on mime type)
                    $filename = str_random(10).'.'.$ext;
                    $filename = strtolower($filename);
                    if($upload_dir == ''){
                        $img_validator = Validator::make(array('file'=> $file), array('image'=>'image'));
                        if($img_validator->passes()){
                            $directory = public_path().'/assets/thisapp/uploads/tasks/images/';
                        } else {
                            $directory = public_path().'/assets/thisapp/uploads/tasks/documents/';
                        }
                    } else {
                        $directory = public_path().$upload_dir;
                    }
                    $file->move($directory, $filename);
                    if($thumb_dir != ''){
                        Image::make($directory.$filename)
                        ->resize(125, 125)
                        ->save($thumb_dir.$filename);
                    }
                    if($basename_only){
                        $filepaths[] = $filename;
                    } else {
                        $filepaths[] = $directory.$filename;
                    }
                }else{
                    //Does not pass validation
                    $errors = $validator->errors();
                }
            }
        }
        return $filepaths;
    }

    public static function get_email_addresses($userids)
    {
        $emails = array();
        $temp = array();
        if(is_array($userids)){
            foreach($userids as $id):
                $temp[] = $id;
            endforeach;
        } else {
            $temp[] = $userids;
        }
        $commcenter = new Commcenter();
        $users = $commcenter->getsomeusers($temp);
        foreach($users as $user):
            if($user->email_address != ''){
                $emails[$user->email_address] = $user->NickName;
            }
        endforeach;
        return $emails;
    }

    /***Standard task finisher with email function***/
    public static function finish_task($data,$files = null)
    {
        $tasks = new Tasks();
        $tasks->createtaskresponse($data);
        $task = $tasks->gettask($data['taskid']);
        foreach($task as $temp):
            $task_data = $temp;
        endforeach;
        $data['email_id'] = '[TaskID#:'.$data['taskid'].']';
        $data['email_to'] = Helper::get_email_addresses($task_data->requestedby);
        $sharees = array();
        $data['email_sharees'] = Helper::get_email_addresses($sharees + explode('|',$task_data->responsesharedwith));
        $email['html'] = $data['email_message'];
        $data['email_from'] = Helper::get_email_addresses($task_data->owner);
        $data['email_from_name'] = $task_data->NickName;
        Mail::send('emails/default_container', $email, function($message) use ($data)
        {
            $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
            $message->cc($data['email_sharees']);
            $message->from($data['email_from'],$data['email_from_name']);
            $message->subject($data['email_id'].$data['email_subject']);
        });
    }

    /***Standard task replier with email function***/
    public static function reply_to_task($data,$files = null)
    {
        $tasks = new Tasks();
        $task = $tasks->gettask($data['taskid']);
        foreach($task as $temp):
            $task_data = $temp;
        endforeach;
        $data['email_id'] = '[TaskID#:'.$data['taskid'].']';
        $data['email_subject'] = 'Reply from '.$task_data->NickName;
        $data['owner'] = $task_data->owner;
        $data['requestedby'] = $task_data->requestedby;
        $data['responsesharedwith'] = $task_data->responsesharedwith;
        $tasks->createtaskmessage($data);
        $data['email_to'] = Helper::get_email_addresses($data['requestedby']);
        $sharees = array();
        $data['email_sharees'] = Helper::get_email_addresses($sharees + explode('|',$data['responsesharedwith']));
        $email['html'] = Helper::getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-task?taskid=".$data['taskid'].'&action=conversation');
        $data['email_from'] = Helper::get_email_addresses($data['owner']);
        $data['email_from_name'] = $task_data->NickName;
        Mail::send('emails/default_container', $email, function($message) use ($data)
        {
            $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
            $message->cc($data['email_sharees']);
            $message->from($data['email_from'],$data['email_from_name']);
            $message->subject($data['email_id'].$data['email_subject']);
        });
    }

    /***Standard message from task requester with email function***/
    public static function send_message_for_request($data,$files = null)
    {
        $tasks = new Tasks();
        $original_request = $tasks->getrequest($data['requestid']);
        $request_tasks = $tasks->gettasksbyrequest($data['requestid']);
        $data['responsesharedwith'] = array();
        //Set same hash for all messages
        $hash = md5(time().$data['requestid'].$data['requestid']);
        //////
        foreach($request_tasks as $request_task):
            $temp_data['taskid'] = $request_task->id;
            ////switch!!!!
            $temp_data['owner'] = $request_task->requestedby;
            $temp_data['requestedby'] = $request_task->owner;
            /////////////
            $temp_data['subject'] = $data['subject'];
            $temp_data['message'] = $data['message'];
            $temp_data['responsesharedwith'] = $request_task->responsesharedwith;
            $temp_data['hash'] = $hash;
            $tasks->createtaskmessage($temp_data);
            $data['requestedby'][] = $request_task->owner;
            $data['responsesharedwith'] = $data['responsesharedwith'] + explode('|',$request_task->responsesharedwith);
        endforeach;
        foreach($original_request as $o_request):
        endforeach;
        $data['email_id'] = '';
        $data['email_to'] = Helper::get_email_addresses($data['requestedby']);
        $data['email_subject'] = 'Reply from '.$o_request->requestedby_name;
        $sharees = array();
        $data['email_sharees'] = Helper::get_email_addresses($sharees + $data['responsesharedwith']);
        $email['html'] = Helper::getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-request?requestid=".$data['requestid'].'&action=conversation');
        $data['email_from'] = Helper::get_email_addresses($request_task->requestedby);
        $data['email_from_name'] = $o_request->requestedby_name;
        Mail::send('emails/default_container', $email, function($message) use ($data)
        {
            $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
            $message->cc($data['email_sharees']);
            $message->from($data['email_from'],$data['email_from_name']);
            $message->subject($data['email_id'].$data['email_subject']);
        });
    }

    /***Standard task creator with email function***/
    public static function create_task_by_request($data,$files = null)
    {
        $data['email_to'] = array();
        $data['email_to_requester_subject'] = 'You have sent a new task to ';
        $sharees = array();
        $task_ids = array();
        $tasks = new Tasks;
        $request_id = $tasks->createtaskrequest($data);
        $requesttasks = $tasks->gettasks(array('requestid'=>$request_id));
        foreach($requesttasks as $r_task):
            $task_ids[] = $r_task->id;
            $data['email_id'] = '[TaskID#:'.$r_task->id.']';
            $data['email_to'] = $r_task->email_address;
            $sharees += explode('|',$r_task->responsesharedwith);
            $data['email_to_requester_subject'] .= $r_task->NickName;
            $sharees = array();
            $data['email_sharees'] = Helper::get_email_addresses($sharees);
            $email['html'] = $data['email_message'];
            Mail::send('emails/default_container', $email, function($message) use ($data)
            {
                $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
                $message->cc(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_sharees']);
                $message->bcc('admin@m-s-s-asia.com');
                $message->from($data['email_from'],$data['email_from_name']);
                $message->subject($data['email_id'].$data['email_subject']);
            });

        endforeach;
        // send notice email to sender of request
        $email_r['html'] = '<b>You sent the following task to: '.$r_task->NickName.'</b><hr>'.$data['email_message'].'<br><br><b>Please do not reply to this system generated message.</b>';
        Mail::send('emails/default_container', $email_r, function($message) use ($data)
        {
            // $message->to($data['email_to_requester']);
            $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to_requester']);
            // $message->to('mssasia@mailinator.com');
            $message->from('admin@m-s-s-asia.com', 'OEMS');
            $message->subject($data['email_to_requester_subject']);
        });
        return $task_ids;
    }

    public static function get_user_info($id)
    {
        $user = null;
        $commcenter = new Commcenter();
        $users = $commcenter->getsomeusers(array($id));
        foreach($users as $data){
            $user = $data;
        }
        return $user;
    }


    /***Email helpers for invoices***/
    public static function send_new_invoice($data)
    {
        /**
	 * PA sends invoice via email to Handler
	 *
	 */
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        // $introducer_info = Helper::get_user_info($data['introducer_id']);
        $handler_info = Helper::get_user_info($data['handler_id']);
        $subject = 'New invoice '.$data['invoice_no'].' prepared by '.$user_info->NickName;
        $message_body = Helper::getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999').'/invoice-print/'.$data['invoice_id'].'?raw=2');
        //$message_body = file_get_contents(URL::to('/').'/invoice-template');
        $message = '';
        $message .= '<p>Dear '.$handler_info->NickName.',<br><br>';
        $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have generated this invoice '.$data['invoice_no'].' and will send it to the client.<br>';
        $message .= 'Attached is a copy of the invoice for your reference.<br><br>';
        $message .= 'Thank you,<br><br>';
        $message .= $user_info->NickName;
        $message .= '<br><br></p>';
        $message .= $message_body;
        $data['owners'] = array($data['handler_id']);
        $data['subject'] = 'New Invoice '.' prepared by '.$user_info->NickName;
        $data['requestedby'] = $user->user_id;
        $data['message'] = json_encode(array('subject'=>$subject,'message'=>$message));
        $data['minuteid'] = '';
        $data['meetingid'] = '';
        $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
        $data['type'] = '3'; // info only
        $data['email_subject'] = $user_info->NickName .' has sent a new invoice '.$data['invoice_no'].' for your reference';
        $data['email_message'] = $message;
        $data['email_to_requester'] = $user_info->email_address;
        $data['email_from'] = $user_info->email_address;
        $data['email_from_name'] = $user_info->NickName;
        $data['email_sharees'] = Helper::get_email_addresses(array('85')); //Terence always gets a copy
        $task_ids = Helper::create_task_by_request($data);
        return $task_ids;
    }
    
    public static function send_final_invoice($data)
    {
        /**
	 * PA sends final invoice to Handler
	 *
	 */
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $handler_info = Helper::get_user_info($data['handler_id']);
        $subject = 'Final invoice '.$data['invoice_no'].' prepared by '.$user_info->NickName;
        $message_body = Helper::getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999').'/invoice-print/'.$data['invoice_id'].'?raw=2');
        //$message_body = file_get_contents(URL::to('/').'/invoice-template');
        $message = '';
        $message .= '<p>Dear '.$handler_info->NickName.',<br><br>';
        $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have generated this final invoice '.$data['invoice_no'].' and will send it to the client.<br>';
        $message .= 'Attached is a copy of the invoice for your reference.<br><br>';
        $message .= 'Thank you,<br><br>';
        $message .= $user_info->NickName;
        $message .= '<br><br></p>';
        $message .= $message_body;
        $data['owners'] = array($data['handler_id']);
        $data['subject'] = 'Final Invoice '.' prepared by '.$user_info->NickName;
        $data['requestedby'] = $user->user_id;
        $data['message'] = json_encode(array('subject'=>$subject,'message'=>$message));
        $data['minuteid'] = '';
        $data['meetingid'] = '';
        $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
        $data['type'] = '3'; // info only
        $data['email_subject'] = $user_info->NickName .' has sent a new invoice '.$data['invoice_no'].' for your reference';
        $data['email_message'] = $message;
        $data['email_to_requester'] = $user_info->email_address;
        $data['email_from'] = $user_info->email_address;
        $data['email_from_name'] = $user_info->NickName;
        $data['email_sharees'] = Helper::get_email_addresses(array('85')); //Terence always gets a copy
        $task_ids = Helper::create_task_by_request($data);
        
        /*
         * Send task to Terence
         */
        $message_c = '';
        $message_c .= '<p>Dear Terence,<br><br>';
        $message_c .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have legally closed this matter '.$data['matter_id'].' and have forwarded it to your OEMS folder.<br>';
        $message_c .= '<b>Matter No: '.$data['matter_id'].'</b><br>';
        $message_c .= '<b>'.$data['matter_description'].'</b><br>';
        $message_c .= '<b>Client: '.$data['matter_client'].'</b><br><br>';
        $message_c .= 'Thank you,<br><br>';
        $message_c .= $user_info->NickName;
        $message_c .= '<br><br></p>';
        $data_c = array();
        $data_c['owners'] = array('85'); //Terence
        $data_c['subject'] = 'Matter '.$data['matter_id'].' has been legally closed.';
        $data_c['requestedby'] = $user->user_id;
        $data_c['message'] = json_encode(array('subject'=>$data_c['subject'],'message'=>$message_c));
        $data_c['minuteid'] = '';
        $data_c['meetingid'] = '';
        $data_c['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
        $data_c['type'] = '3'; // info only
        Helper::create_task_by_request($data_c);
    }

    /***Email helpers for timesheet***/
    /** email all timesheet entries made by personal assistants
     *
     */
    public static function send_latest_timesheet_entries()
    {
        /*
         * Get all timesheet entries for given date
         */
        
        $m = new Matter();
        $u = new User;
        $filter = array();
        $t_records_by_user = array();
        $t_records_by_handler = array();
        $filter['create_date'] = date('Y-m-d',strtotime('yesterday')); //get date yesterday
        // $filter['create_date'] = '2014-11-04';
        $user_id = '3'; // admin
        $t_records = $m->prepareQueryForGetTimesheet($user_id,$filter)->get();        
        foreach($t_records as $t_record):
                $t_records_by_user[$t_record->User_C]['records'][] = $t_record;
                $t_records_by_handler[$t_record->EmployeeID]['records'][] = $t_record;
        endforeach;
        foreach($t_records_by_user as $k => $v):
            $user_id = $u->getUserID($k);
            $user_info = Helper::get_user_info($user_id);
            if($user_info){
                $data['requestedby'] = $user_id;
                $data['owner'] = $user_id;
                $subject = 'Timesheet records made in your behalf yesterday, '.$filter['create_date'];
                $message = View::make('emails/timesheet_records',array(
                        'user_name' => $user_info->NickName,
                        'report_date' => date('m/d/Y',strtotime($filter['create_date'])),
                        'records' => isset($v['records']) ? $v['records'] : null
                    ));
                $data['minuteid'] = '';
                $data['meetingid'] = '';
                $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
                $data['type'] = '3'; // type info
                $data['message'] = json_encode(array('subject'=>$subject,'message'=>utf8_encode($message))); //utf8_encode is important. If not used the message will be null
//                print_r($data['message']);
                 $tasks = new Tasks();
                 $tasks->createowntask($data);

                $data['email_to'] = $user_info->email_address;
                $data['email_subject'] = $subject;
                $data['email_message'] = $message;
                $email['html'] = $message;
                echo $message.'<br>';
                Mail::send('emails/default_container', $email, function($message) use ($data)
                {
                    $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
                    // $message->to('mssasia@mailinator.com');
                    $message->bcc('admin@m-s-s-asia.com');
                    $message->bcc('thomas@m-s-s-asia.com');
                    $message->from('mssasia@oln-law.com','OEMS');
                    $message->subject($data['email_subject']);
                });
            }
        endforeach;
        foreach($t_records_by_handler as $k => $v):
            $user_id = $u->getUserID($k);
            $user_info = Helper::get_user_info($user_id);
            if($user_info){
                $data['requestedby'] = $user_id;
                $data['owner'] = $user_id;
                $subject = 'Timesheet records made in your behalf yesterday, '.$filter['create_date'];
                $message = View::make('emails/timesheet_records',array(
                        'user_name' => $user_info->NickName,
                        'report_date' => date('m/d/Y',strtotime($filter['create_date'])),
                        'records' => isset($v['records']) ? $v['records'] : null
                    ));
                $data['minuteid'] = '';
                $data['meetingid'] = '';
                $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
                $data['type'] = '3'; // type info
                $data['message'] = json_encode(array('subject'=>$subject,'message'=>utf8_encode($message))); //utf8_encode is important. If not used the message will be null
//                print_r($data['message']);
                 $tasks = new Tasks();
                 $tasks->createowntask($data);

                $data['email_to'] = $user_info->email_address;
                $data['email_subject'] = $subject;
                $data['email_message'] = $message;
                $email['html'] = $message;
                echo $message.'<br>';
                Mail::send('emails/default_container', $email, function($message) use ($data)
                {
                    $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
                    // $message->to('mssasia@mailinator.com');
                    $message->bcc('admin@m-s-s-asia.com');
                    $message->bcc('thomas@m-s-s-asia.com');
                    $message->from('mssasia@oln-law.com','OEMS');
                    $message->subject($data['email_subject']);
                });
            }
        endforeach;
    }
    
//    public static function send_latest_timesheet_entries()
//    {
//        /*
//         * Get all timesheet entries for given date
//         */
//        
//        $m = new Matter();
//        $u = new User;
//        $filter = array();
//        $t_records_by_pa = array();
//        // $filter['create_date'] = date('Y-m-d',strtotime('yesterday')); //get date yesterday
//        $filter['create_date'] = '2014-11-03';
//        $user_id = '3'; // admin
//        $t_records = $m->prepareQueryForGetTimesheet($user_id,$filter)->get();
//        $pas = $u->getEmployeesByRoles('Handler'); //get all personal assistants
//        foreach($pas as $pa):
//            $t_records_by_pa[$pa->EmployeeID]['user_id'] = $pa->user_id;
//            $t_records_by_pa[$pa->EmployeeID]['name'] = $pa->NickName;
//        endforeach;
//        foreach($t_records as $t_record):
//            if(isset($t_records_by_pa[$t_record->User_C])){
//                $t_records_by_pa[$t_record->User_C]['records'][$t_record->UniqueID] = $t_record;
//            }
//        endforeach;
//        foreach($t_records_by_pa as $v):
//            $data['requestedby'] = $v['user_id'];
//            $data['owner'] = $v['user_id'];
//            $subject = 'Timesheet records made by you yesterday, '.$filter['create_date'];
//            $message = '';
//            $message .= 'Hello,<br><br>';
//            $message .='&nbsp:&nbsp:&nbsp:&nbsp:&nbsp:These were the timesheet records you made yesterday:<br><br>';
//            $message .= View::make('emails/timesheet_records',array(
//                    'pa_name' => $v['name'],
//                    'report_date' => date('m/d/Y',strtotime($filter['create_date'])),
//                    'records' => isset($v['records']) ? $v['records'] : null
//                ));
//            $data['minuteid'] = '';
//            $data['meetingid'] = '';
//            $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
//            $data['type'] = '3'; // type info
//            $data['message'] = json_encode(array('subject'=>$subject,'message'=>$message));
//            // $tasks = new Tasks();
//            // $tasks->createowntask($data);
//            $data['email_subject'] = $subject;
//            $data['email_message'] = $message;
//            $email['html'] = $message;
//            echo $message.'<br>';
//            Mail::queue('emails/default_container', $email, function($message) use ($data)
//            {
//                // $message->to(App::environment('alpha') ? 'mssasia@mailinator.com' : $data['email_to']);
//                $message->to('mssasia@mailinator.com');
//                $message->bcc('mssasia@mailinator.com');
//                $message->from('mssasia@oln-law.com','OEMS');
//                $message->subject($data['email_subject']);
//            });
//        endforeach;
//    }

    public static function delegate_matter($data)
    {
        /**
	 * Handler delegates matter to other handles
	 *
	 */
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $delegated_to_info = Helper::get_user_info($data['delegated_to']);
        $subject = $user_info->NickName.' has delegated Matter No: '.$data['matter_no'].' ('.$data['matter_description'].') '.' to you.';
        $message = '';
        $message .= '<p>Dear '.$delegated_to_info->NickName.',<br><br>';
        $message .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I have delegated the following matter to you:<br><br>';
        $message .= '<b>Matter No: '.$data['matter_no'].'</b><br>';
        $message .= '<b>'.$data['matter_description'].'</b><br>';
        $message .= '<b>Client: '.$data['matter_client'].'</b><br><br>';
        // $message .= '<br>If you wish to decline this delegated task please click <a href="'.(App::environment('alpha') ? 'http://192.168.1.250:9999:' : 'http://192.168.99.8:9999:').'/matters/decline/'.$data['matter_no'].'/'.$user_info->EmployeeID.'">here</a>.<br><br>';
        $message .= 'If you wish to decline the task please do so in your delgated matters list.<br><br>';
        $message .= 'If you have questions please do not hesitate to ask.<br><br>';
        $message .= 'Thank you,<br><br>';
        $message .= $user_info->NickName;
        $message .= '<br><br></p>';
        $data['owners'] = array($data['delegated_to']);
        $data['subject'] = 'Matter No: '.$data['matter_no'].' has been delegated to you.';
        $data['requestedby'] = $user->user_id;
        $data['message'] = json_encode(array('subject'=>$subject,'message'=>$message));
        $data['minuteid'] = '';
        $data['meetingid'] = '';
        $data['requiredbydate'] = date('Y-m-d',strtotime('+30 days'));
        $data['type'] = '1'; // info only
        $data['email_subject'] = $subject;
        $data['email_message'] = $message;
        $data['email_to_requester'] = $user_info->email_address;
        $data['email_from'] = $user_info->email_address;
        $data['email_from_name'] = $user_info->NickName;
        $task_ids = Helper::create_task_by_request($data);
        $c_data = array();
        foreach($task_ids as $t_id){
            $c_data[] = array(
                    'taskid' => $t_id,
                    'original_action' => 'delegate',
                    'reply_action' => 'decline',
                    'item_connected' => 'matter',
                    'item_id' => $data['matter_no'],
                );
        }
        // Helper::create_task_connections($c_data);               
    }
    
    public static function create_task_connections($data)
    {
        /*
         * taskid = unique task id created
         * orginal_action = delegate/
         * reply_action = decline / acknowledge / approve
         * item_connected = matter / invoice /
         * item_id = unqiue matter_id / invoice_id
         */
        DB::table('tdl_task_connections')->insert($data);
    }       
    
    function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    
    public static function getFileContents($url)
    {
        $request = Request::create($url, 'GET');
        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);
        return $response->getContent();   
    }
    
    public static function geoCheckIP($ip)
    {
        //check, if the provided ip is valid
        if(!filter_var($ip, FILTER_VALIDATE_IP))
        {
                throw new InvalidArgumentException("IP is not valid");
        }

        //contact ip-server
        $response=@file_get_contents('http://www.netip.de/search?query='.$ip);
        if (empty($response))
        {
                throw new InvalidArgumentException("Error contacting Geo-IP-Server");
        }

        //Array containing all regex-patterns necessary to extract ip-geoinfo from page
        $patterns=array();
        $patterns["domain"] = '#Domain: (.*?)&nbsp;#i';
        $patterns["country"] = '#Country: (.*?)&nbsp;#i';
        $patterns["state"] = '#State/Region: (.*?)<br#i';
        $patterns["town"] = '#City: (.*?)<br#i';

        //Array where results will be stored
        $ipInfo=array();

        //check response from ipserver for above patterns
        foreach ($patterns as $key => $pattern)
        {
                //store the result in array
                $ipInfo[$key] = preg_match($pattern,$response,$value) && !empty($value[1]) ? $value[1] : 'not found';
        }

        return $ipInfo;
    }
    
    public static function reserved_ip($ip)
    {
        $reserved_ips = array( // not an exhaustive list
        '167772160'  => 184549375,  /*    10.0.0.0 -  10.255.255.255 */
        '3232235520' => 3232301055, /* 192.168.0.0 - 192.168.255.255 */
        '2130706432' => 2147483647, /*   127.0.0.0 - 127.255.255.255 */
        '2851995648' => 2852061183, /* 169.254.0.0 - 169.254.255.255 */
        '2886729728' => 2887778303, /*  172.16.0.0 -  172.31.255.255 */
        '3758096384' => 4026531839, /*   224.0.0.0 - 239.255.255.255 */
        );

        $ip_long = sprintf('%u', ip2long($ip));

        foreach ($reserved_ips as $ip_start => $ip_end)
        {
            if (($ip_long >= $ip_start) && ($ip_long <= $ip_end))
            {
                return TRUE;
            }
        }
        return FALSE;
    }
}
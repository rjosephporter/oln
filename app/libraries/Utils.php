<?php
class Utils {
    public static function pay($obj){
        $flag = FALSE;
        $u = new Utilities;
        $info=array();
        $data['id']     =   $obj['id'];
        $data['type']   =   2;
        $data['amount'] =   0;
        $data['user']   =   User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);
        $data['date']   =   date('Y-m-d');
        $data['time']   =   date('H:i:s');
        
        $i = $u->getMatterCost($data['id']);
        $balance = 0;
        
        foreach ($i as $credit):
            $balance += $credit->type == 1 ? $credit->Amount : 0;
            $balance -= $credit->type == 2 ? $credit->Amount : 0;
        endforeach;
        
        if($balance >= $obj['amount']){
            $flag = TRUE;
            $info['status_id']              = 1;
            $info['message']                = "Successfully Credited Invoice to Cost on Account";
            $info['amount_credited']        = $obj['amount'];
            $info['balance_on_account']     = $balance - $obj['amount'];
            $info['balance_payable']        = 0;
            $data['amount']                 = $info['amount_credited'];
        }else
        if($balance > 0 && $obj['amount'] > $balance){
            $flag = TRUE;
            $info['status_id']              = 2;
            $info['message']                = "Partially Credited Invoice to Cost on Account";
            $info['amount_credited']        = $balance;
            $info['balance_on_account']     = 0;
            $info['balance_payable']        = $obj['amount'] - $info['amount_credited'];
            $data['amount']                 = $info['amount_credited'];
        }else
        if($balance <= 0){
            $info['status_id']              = 0;
            $info['message']                = "Cannot Credit, Insufficient Balance";
            $info['amount_credited']        = 0;
            $info['balance_on_account']     = 0;
            $info['balance_payable']        = $obj['amount'];
            $data['amount']                 = 0;
        }
        if($flag)
            $u->insertMatterCost($data);
        
        return Response::json($info);
    }
    
    public static function balance($id){
        $u = new Utilities;
        $i = $u->getMatterCost($id);
        $balance = 0;
        
        foreach ($i as $credit):
            $balance += $credit->type == 1 ? $credit->Amount : 0;
            $balance -= $credit->type == 2 ? $credit->Amount : 0;
        endforeach;
        
        if($balance > 0)
            $data = array('creditable' => TRUE, 'balance_on_account' => $balance);
        else
            $data = array('creditable' => FALSE, 'balance_on_account' => $balance);
        
        return Response::json($data);
    }

    public static function declineMatter($data){
        $u = new Utilities;
        $check = $u->checkDelegationStatus($data);
        $check['delegated'] = isset($check[0]->status) ? $check[0]->status : 0;
        $obj = array();
        switch ($check['delegated']) {
            case 1:
                $u->declineMatter(array('id' => $check['0']->id));
                $obj['message'] = $data['delegatedTo'].' successfully declined Matter '.$data['id'];
                $obj['status'] = 1;
            break;

            case 2:
                $obj['message'] = $data['delegatedTo'].' already declined Matter '.$data['id'];
                $obj['status'] = 2;
            break;

            case 3:
                $obj['message'] = 'Decline failed, '.$data['delegatedTo'].' already completed Matter '.$data['id'];
                $obj['status'] = 3;
            break;

            case 0:
                $obj['message'] = 'Matter '.$data['id'].' is not delegated to '.$data['delegatedTo'];
                $obj['status'] = 0;
            break;
        }

        return ($obj);
    }

    public static function completeMatter($data){
        $u = new Utilities;
        $check = $u->checkDelegationStatus($data);
        $check['delegated'] = isset($check[0]->status) ? $check[0]->status : 0;
        $obj = array();
        switch ($check['delegated']) {
            case 1:
                $u->completeMatter(array('id' => $check['0']->id));
                $obj['message'] = $data['delegatedTo'].' successfully completed Matter '.$data['id'];
                $obj['status'] = 1;
            break;

            case 2:
                $obj['message'] = 'Cannot mark matter as complete, '.$data['delegatedTo'].' already declined Matter '.$data['id'];
                $obj['status'] = 2;
            break;

            case 3:
                $obj['message'] = $data['delegatedTo'].' already completed Matter '.$data['id'];
                $obj['status'] = 3;
            break;

            case 0:
                $obj['message'] = 'Matter '.$data['id'].' is not delegated to '.$data['delegatedTo'];
                $obj['status'] = 0;
            break;
        }

        return ($obj);
    }

    public static function getAllMatters(){
                return DB::select("
            SELECT 
            jobassignment.JobAssignmentID, jobassignment.CustomerID, customer.CompanyName_A1 AS client_name, jobassignment.Description_A, jobassignment.Incharge
            FROM jobassignment
            LEFT JOIN customer
                ON jobassignment.CustomerID = customer.CustomerID
            GROUP BY jobassignment.JobAssignmentID");
    }

    public static function reopenMatter($obj){
        DB::update("UPDATE jobassignment SET Completed = 0 WHERE JobAssignmentID = :id LIMIT 1", array('id' => $obj['jobid']));
    }

    public static function getDelegatedMatters(){
        return DB::select("SELECT * FROM jobassignmentdelegated WHERE status = 1");
    }
}
<?php
class SendEmail {

    public function fire($job, $data)
    {
        //
    }

    public function gdo_reminder($job, $data)
    {
        Mail::send('emails/inactive_matter_reminder', $data, function($message) use ($data)
        {
            $recipient_email = $data['handler_id'].'@mailinator.com';
            $recipient = $data['handler_name'];   
            $from_email = $data['from_email'];
            $from_name = $data['from_name'];
            $message->to($recipient_email, $recipient)
                    ->from($from_email, $from_name)
                    ->subject('Inactive Matters Reminder');
        });

        $job->delete();    	
    }

}
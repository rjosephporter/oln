<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class SyncTable extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:synctable';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync table records from source (MSSQL) to target (MYSQL) database.';

	/* Table list and status */
	private $table_status;

	/* Unique Key by table */
	private $unique_key;

	/* Monolog */
	private $monolog;		

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

    	// Set tables' status
    	$this->table_status = $this->_getSourceTableStatus();

    	// Set tables' unique key
		foreach($this->table_status as $detail)
			$this->unique_key[$detail->source_table] = $detail->unique_column;

		// Initialize file logger
    	$this->monolog = Log::getMonolog();
    	$handler = new RotatingFileHandler(storage_path().'/logs/sync.log',0,Logger::INFO);
		$this->monolog->pushHandler($handler);

		set_time_limit(120);
		DB::disableQueryLog();		
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//$result = $this->executeCommand();

		/* Test */
		$task = $this->argument('task');
		switch ($task) {
			case 'fire':
				$result = $this->executeCommand();
				break;

			case 'show-tables':
				$temp_result = 'Table 		last_insert_key 		last_update_date'.PHP_EOL;
				foreach($this->table_status as $data) {
					$temp_result .= $data->source_table.'		'.$data->last_insert_key.'		'.$data->last_update_date.PHP_EOL;
				}
				$result = $temp_result;
				break;

			case 'force-update-table-status':
				$records_updated = $this->_updateSourceTableStatusForce();
				$result = 'Total table status updated: '.$records_updated;
				break;

			case 'rebuild-report-tables':
				$result = $this->_rebuildReportTables();
				break;

			case 'rebuild-dat-files':
				$result = $this->_rebuildDatFiles();
				break;

			case 'update-invoicehead':
				$result = $this->_updateInvoiceHead();
				break;

			case 'send-timesheet-report':
				$result = $this->_sendTimesheetReport();
				break;
			
			default:
				$result = 'Invalid argument.';
				break;
		}
		/* /Test*/

		$this->info($result);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('task', InputArgument::REQUIRED, 'The task to execute.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

    private function getForceUpdateTableStatus()
    {
    	return Response::json($this->_updateSourceTableStatusForce());
    }

    private function getSyncDeletedRecords()
    {
    	$table = Input::get('table');

    	$result = $this->_deleteRecords($table);

    	return Response::json($result);
    }

	private function executeCommand()
	{
		$result = array();
		$table_status = $this->table_status;
		foreach($table_status as $data) {
			/* Get new records from source */
			$new_records = DB::connection('sqlsrv')
							->table($data->source_table)
							->orderBy($data->key_column, 'asc');
			if($data->key_column == 'Date_C')
				$new_records->whereRaw('CONVERT(varchar(23), '.$data->key_column.', 20) > \''.$data->last_insert_key.'\'');
			else				
				$new_records->where($data->key_column, '>', $data->last_insert_key);

			$new_records = $new_records->get();

			$result[$data->source_table]['new'] = $new_records;
			$last_new_record = end($new_records);
			$result[$data->source_table]['last_insert_key'] = null;
			if($last_new_record) {
				$result[$data->source_table]['last_insert_key'] = ($data->key_column !== 'Date_C') ? $last_new_record->{$data->key_column} : date('Y-m-d H:i:s', strtotime($last_new_record->{$data->key_column}));
			}
			/* /Get new records from source */

			/* Get UPDATED records from source */
			$updated_records = array();
			if(!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) { // skip these tables, will depend on TimeSheet. NOTE: TimeSheet must appear first before TimeSheetWork and InvoiceDetail in sourcetable_status in order for this to work
				$updated_records = DB::connection('sqlsrv')
									->table($data->source_table)
									->whereRaw('CONVERT(varchar(23), Date_U, 20) > \''.$data->last_update_date.'\'')
									->orderBy('Date_U', 'asc')
									->get();
			} elseif(count($result['TimeSheet']['updated']) > 0) { // get updated TimeSheet records to also update TimeSheetWork and InvoiceDetail tables
				$join_key = array(
					'TimeSheetWork' => 'UniqueID',
					'InvoiceDetail'	=> 'ReferenceID'
				);

				$updated_timesheet_ids = array();
				foreach($result['TimeSheet']['updated'] as $updated_timesheet)
					$updated_timesheet_ids[] = $updated_timesheet->UniqueID;

				$updated_records = DB::connection('sqlsrv')
									->table($data->source_table)
									->select($data->source_table.'.*')
									->leftJoin('TimeSheet', 'TimeSheet.UniqueID', '=', $data->source_table.'.'.$join_key[$data->source_table])
									->whereIn('TimeSheet.UniqueID', $updated_timesheet_ids)
									->get();
			}

			$result[$data->source_table]['updated'] = $updated_records;
			$last_updated_record = end($updated_records);
			$result[$data->source_table]['last_update_date'] = null;
			if($last_updated_record) {
				$result[$data->source_table]['last_update_date'] = (!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) 
																	? date('Y-m-d H:i:s', strtotime($last_updated_record->Date_U))
																	: $result['TimeSheet']['last_update_date'];
			}
			/* /Get UPDATED records from source */
		}

		$inserted_records = array();
		$updated_records = array();
		$final_result = array();
		foreach($result as $table_name => &$table_data) {
			/* Insert new records to target (MYSQL) */
			$inserted_records[$table_name] = array();
			if(count($table_data['new']) > 0) {
				$inserted_records[$table_name] = $this->_insertNewRecords($table_name, $table_data['new']);
			}
			$table_data['inserted_records'] = $inserted_records[$table_name];
			/* /Insert new records to target (MYSQL) */

			/* Update updated records to target (MYSQL) */
			$updated_records[$table_name] = array();
			if(count($table_data['updated']) > 0) {
				$updated_records[$table_name] = $this->_updateRecords($table_name, $table_data['updated']);
			}
			$table_data['updated_records'] = $updated_records[$table_name];
			/* /Update updated records to target (MYSQL) */

			/* Update table status reference */	
			if(!is_null($table_data['last_insert_key']))
				$this->_updateSourceTableStatus($table_name,'last_insert_key',$table_data['last_insert_key']);
			if(!is_null($table_data['last_update_date']))
				$this->_updateSourceTableStatus($table_name,'last_update_date',$table_data['last_update_date']);
			/* /Update table status reference */

			/* Get counts for final result */
			foreach($table_data as $data_key => $data_value)
				$final_result[$table_name][$data_key] = (!in_array($data_key, array('last_insert_key','last_update_date'))) ? count($table_data[$data_key]) : $table_data[$data_key];
			/* /Get counts for final result */
		}

		$this->_log('Sync Summary', $final_result);

		//return Response::make(var_dump($result));
		//return Response::json($result);
		return Response::json($final_result);
	}

	/**
	 * Get list of table names from source
	 *
	 */
	private function _getSourceTables()
	{
		$tables = array();
		$query = DB::connection('sqlsrv')
				->select(DB::raw('SELECT Distinct TABLE_NAME FROM information_schema.TABLES'));

		foreach($query as $table)
			$tables = $table->TABLE_NAME;

		return $tables;
	}

	/**
	 * Get source table status (last update date, last record inserted, etc.)
	 *
	 * NOTE: data is taken from MYSQL server, not the source
	 */
	private function _getSourceTableStatus()
	{
		$result = DB::table('sourcetable_status')
					->get();
		return $result;
	}

	/**
	 * Update source status by table
	 *
	 */
	private function _updateSourceTableStatus($table, $column, $data)
	{
		$temp_data = null;
		if($column == 'last_insert_key') {
			$temp_data = (!strtotime($data))
						? $data
						: date('Y-m-d H:i:s', strtotime($data));
		}
		if($column == 'last_update_date') {
			$temp_data = (!in_array($table, array('TimeSheetWork','InvoiceDetail'))) 
						? date('Y-m-d H:i:s', strtotime($data))
						: DB::table('sourcetable_status')->where('source_table', '=', 'TimeSheet')->pluck('last_update_date');
		}
		$final_data = array(
			$column => $temp_data
		);
		$result = DB::table('sourcetable_status')
						->where('source_table', '=', $table)
						->take(1)
						->update($final_data);
		return $result;
	}

	/**
	 * Force update source tables status
	 *
	 */
	private function _updateSourceTableStatusForce()
	{
		$result = array();
		$update_data = array();
		$table_status = $this->table_status;
		foreach($table_status as $data) {

			$update_data[$data->source_table] = array(
				'last_insert_key'	=> ($data->key_column !== 'Date_C')
										? DB::connection('sqlsrv')->table($data->source_table)->max($data->key_column)
										: date('Y-m-d H:i:s', strtotime(DB::connection('sqlsrv')->table($data->source_table)->max($data->key_column))),
				'last_update_date'	=> (!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) 
										? date('Y-m-d H:i:s', strtotime(DB::connection('sqlsrv')->table($data->source_table)->max('Date_U')))
										: $update_data['TimeSheet']['last_update_date'] 
			);
			$result[] = DB::table('sourcetable_status')
							->where('source_table', '=', $data->source_table)
							->update($update_data[$data->source_table]);
		}

		return array_sum($result);
	}

	/**
	 * Insert new records to target tables (MYSQL)
	 *
	 */
	private function _insertNewRecords($table, $records)
	{
		$final_records = array();

		$chunked_records = array_chunk($records, 500);
		foreach($chunked_records as $main_records) {
			$batch_records = array();
			foreach($main_records as $record) {
				$row = array();
				foreach($record as $column => $data) {
					$data = (is_string($data)) ? trim($data) : $data;
					$data = (strlen($data) >= 20 && strtotime($data)) ? date('Y-m-d H:i:s', strtotime($data)) : $data;
					$row[$column] = $data;
				}
				$final_records[] = $row;
				$batch_records[] = $row;
			}
			DB::table(strtolower($table))->insert($batch_records);
		}

		//DB::table(strtolower($table))->insert($final_records);

		return $final_records;
	}

	/**
	 * Update updated records from source to target tables (MYSQL)
	 *
	 */
	private function _updateRecords($table, $records)
	{
		$final_records = array();

		foreach($records as $record) {
			$row = array();
			foreach($record as $column => $data) {
				$data = (is_string($data)) ? trim($data) : $data;
				$data = (strlen($data) >= 20 && strtotime($data)) ? date('Y-m-d H:i:s', strtotime($data)) : $data;
				$row[$column] = $data;
			}
			/*
			$special_key = array(
				'JobAssignment' => 'JobAssignmentID',
				'Customer' => 'CustomerID',
				'Employee' => 'EmployeeID'
			);
			*/
			//$update_key = (!in_array($table, array_keys($special_key))) ? 'UniqueID' : $special_key[$table];
			$update_key = $this->unique_key[$table];

			$update = DB::table(strtolower($table))->where($update_key, '=', $row[$update_key])->take(1);
			
			/* Exclude primary columns from update */
			unset($row[$update_key]);
			if($table == 'InvoiceDetail') unset($row['ItemLineNo']);
			
			$update->update($row);

			$final_records[] = $row;
		}

		return $final_records;
	}

	/**
	 * Delete records from target (MYSQL) that no longer exist in source (MSSQL)
	 *
	 * NOTE: Must be used after tables are synced (insert & update)
	 */
	private function _deleteRecords($table)
	{
		// List unique values from source table
		$source_list = DB::connection('sqlsrv')
						->table($table)
						->lists($this->unique_key[$table]);
		// List unique values from target table
		$target_list = DB::table($table)
						->lists($this->unique_key[$table]);

		$delete_keys = array_diff($target_list, $source_list);

		$delete_ctr = 0;
		if(count($delete_keys) > 0) {
			$chunked_delete_keys = array_chunk($delete_keys, 500);
			foreach ($chunked_delete_keys as $batch_delete_keys) {
				$count = DB::table($table)->whereIn($this->unique_key[$table], $batch_delete_keys)->delete();
				$delete_ctr = ($count > 0) ? $delete_ctr + $count : $delete_ctr + 0;
			}
		}

		$result = array(
			'keys_to_delete' => $delete_keys,
			'actual_keys_deleted' => $delete_ctr
		);

		return $result;
	}

	/**
	 * Log result to file
	 *
	 * @param 	mixed 	array/object to log
	 */
	private function _log($message, $result)
	{
		$this->monolog->addInfo($message.' - '.json_encode($result));
	}

	/**
	 * Rebuild report_* tables in MySQL Server
	 *
	 */	
	private function _rebuildReportTables()
	{
		DB::table('report_matters')->truncate();
		DB::insert('
			insert into report_matters (matter_id, workdate_year, workdate_month, workdate_day, total_billed_amount, total_billable_amount, total_unbilled_amount, trans_date)
			(
				select 
					ja.JobAssignmentID as matter_id,
					year(ts.WorkDate) as workdate_year,
					month(ts.WorkDate) as workdate_month,
					day(ts.WorkDate) as workdate_day,

					round(COALESCE(sum(ind.Amount),0),2) AS total_billed_amount,
					round(COALESCE(sum(jab.PlanAmount),0),2) AS total_billable_amount,
					round(COALESCE(sum(jab.PlanAmount),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount,
					date(ts.WorkDate) as trans_date
				from `jobassignment` as `ja` 
					left join timesheet as ts on ts.JobAssignmentID = ja.JobAssignmentID
					left join timesheetwork as tsw on tsw.UniqueID = ts.UniqueID
					left join jobassignmentbilling as jab on jab.UniqueID = tsw.UniqueID
					left join invoicedetail as ind on ind.ReferenceID = ts.UniqueID
				group by `ja`.`JobAssignmentID`, workdate_year, workdate_month, workdate_day 
				having workdate_year is not null
			) 
		');

		DB::table('report_clients')->truncate();
		DB::insert('
			insert into report_clients (client_id, workdate_year, workdate_month, workdate_day, total_billed_amount, total_billable_amount, total_unbilled_amount, trans_date)
			(
				select 
					ctm.CustomerID as client_id,
					year(ts.WorkDate) as workdate_year,
					month(ts.WorkDate) as workdate_month,
					day(ts.WorkDate) as workdate_day,

					round(COALESCE(sum(ind.Amount),0),2) AS total_billed_amount,
					round(COALESCE(sum(jab.PlanAmount),0),2) AS total_billable_amount,
					round(COALESCE(sum(jab.PlanAmount),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount,
					date(ts.WorkDate) as trans_date
				from `customer` as `ctm` 
					left join jobassignment as ja on ja.CustomerID = ctm.CustomerID
					left join timesheet as ts on ts.JobAssignmentID = ja.JobAssignmentID
					left join timesheetwork as tsw on tsw.UniqueID = ts.UniqueID
					left join jobassignmentbilling as jab on jab.UniqueID = tsw.UniqueID
					left join invoicedetail as ind on ind.ReferenceID = ts.UniqueID		
				group by `ctm`.`CustomerID`, workdate_year, workdate_month, workdate_day  
				having workdate_year is not null
			)			
		');


		DB::table('report_handlers')->truncate();
		DB::insert('
			insert into report_handlers (handler_id, workdate_year, workdate_month, workdate_day, total_billed_amount, total_billable_amount, total_unbilled_amount, trans_date)
			(
				select 
					emp.EmployeeID as handler_id,
					year(ts.WorkDate) as workdate_year,
					month(ts.WorkDate) as workdate_month,
					day(ts.WorkDate) as workdate_day,

					round(COALESCE(sum(ind.Amount),0),2) AS total_billed_amount,
					round(COALESCE(sum(jab.PlanAmount),0),2) AS total_billable_amount,
					round(COALESCE(sum(jab.PlanAmount),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount,
					date(ts.WorkDate) as trans_date
				 from `employee` as `emp` 
					left join jobassignment as ja on ja.Incharge = emp.EmployeeID
					left join timesheet as ts on ts.JobAssignmentID = ja.JobAssignmentID
					left join timesheetwork as tsw on tsw.UniqueID = ts.UniqueID
					left join jobassignmentbilling as jab on jab.UniqueID = tsw.UniqueID
					left join invoicedetail as ind on ind.ReferenceID = ts.UniqueID	
				group by `emp`.`EmployeeID`, workdate_year, workdate_month, workdate_day 
				having workdate_year is not null
			)
		');

		$result['report_matters_count'] = DB::table('report_matters')->count();
		$result['report_clients_count'] = DB::table('report_clients')->count();
		$result['report_handlers_count'] = DB::table('report_handlers')->count();

		return json_encode($result);
	}

	/**
	 * Rebuild .dat files
	 *
	 */
	private function _rebuildDatFiles()
	{
        $j = new JobAssignment;
        $handlers = json_encode(array('e'=>$j->getE()));
        $file2 = fopen(public_path() . "/assets/thisapp/data/handl.dat","w");
        fwrite($file2,$handlers);
        fclose($file2);
        $j->setFlag();

        $bills = $j->getMattersBill();
        $file3 = fopen(public_path() . "/assets/thisapp/data/bills.dat","w");
        fwrite($file3,json_encode($bills));
        fclose($file3);
        
        return Response::json(array('bills_count' => $bills));
	}

	/**
	 * Update invoicehead table with appropriate matter_id
	 *
	 */

	private function _updateInvoiceHead()
	{
		$update_count = DB::update('
			update invoicehead as ih
				left join
				(
					select 
						inh.UniqueID,
						inh.BatchNumber,
						count(*) as item_count,
						group_concat(distinct ts.JobAssignmentID) as matter_id
					from invoicehead as inh
						left join invoicedetail as ind on ind.UniqueID = inh.UniqueID
						left join timesheet as ts on ts.UniqueID = ind.ReferenceID
					group by inh.UniqueID
				) as t1 on t1.UniqueID = ih.UniqueID
			set 
				ih.matter_id = t1.matter_id
			where ih.matter_id is null
		');

		$result['rows_updated'] = $update_count;

		return Response::json($result);
	}

	/**
	 * Send timesheet report
	 *
	 */
	private function _sendTimesheetReport()
	{
		Helper::send_latest_timesheet_entries();

		$result['status'] = 'success';

		return Response::json($result);
	}
}

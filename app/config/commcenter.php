<?php

return array(
    /*
     * Task types used in the Comm Center
     */
    'task_types' => array(
        '1' => 'TODO',
        '2' => 'DECISION',
        '3' => 'INFO',
        '4' => 'IDEA',
        '5' => 'TODO-ONGOING',
        '6' => 'TODO-REQUEST APPROVAL'
    ),
    'task_status' => array(

    )
);

?>

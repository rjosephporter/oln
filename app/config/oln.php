<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Admin IDs
	|--------------------------------------------------------------------------
	|
	| Set admin IDs based on user_id from "user" table
	| 
	*/

	//'admin' => array(3,95,96,28,97),
	'admin_id' => 3,
	'gdo_id' => array(96,28),
	'app_name' => 'Office Efficiency Management System',
	'app_name_abbr' => 'OEMS',

	'invoice' => array(
		'template_1' => array(
			'company_name' 			=> 'Oldham, Li & Nie Solicitors',
			
			'company_address_1' 	=> 'Suite 503, St. George\'s Building',
			'company_address_2'		=> '2 Ice House Street',
			'company_address_3'		=> 'Central, Hong Kong',

			'company_tel_no'		=> '(852) 2868 0696',
			'company_fax_no'		=> '(852) 2810 6796',

			'cheque_payable_to'		=> 'OLDHAM, LI & NIE, Solicitors',

			'name_of_banker'		=> array(
							'line_1'	=> 'The Hong Kong and Shanghai Banking Corporation Ltd.',
							'line_2'	=> 'Hong Kong Main Branch',
							'line_3'	=> 'HSBC Main Building',
							'line_4'	=> '1 Queen\'s Road Central, Hong Kong'
			),

			'account_name'			=> 'Oldham, Li & Nie, Solicitors',
			'account_no'			=> '652-059478-838',
			'swift_code'			=> 'HSBCHKHHHKH'
		),
		'minutes_per_unit' => 5
	),

	'matter' => array(
		'type' => array(
			'open' => 0,
			'completed' => 1,
			'ip' => 9,
			'special' => 10
		)
	),

	'timesheet' => array(
		'budget_billable_hours_per_day' => 4.28571429
	)

);
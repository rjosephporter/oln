<?php

class DashboardController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function getIndex()
    {
    	$user_id = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
    	
    	$user = new User();
    	$billing = new Billing();

        $params['type'] = 'red';
        $params['handler'] = (Session::has('principal_user')) ? User::getEmployeeID(Session::get('principal_user')) : Auth::user()->EmployeeID;

        // unset handler if principal user is admin
        if(User::isAdmin(Session::get('principal_user'))) unset($params['handler']);

        $request = Request::create('matters/msummarylist', 'GET', $params);

        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);

        $matters =  json_decode($response->getContent());        

    	$data['matters_count'] = number_format($user->getMattersCount($user_id), 0, '.', ',');
    	$data['clients_count'] = number_format($user->getClientsCount($user_id), 0, '.', ',');
        $data['my_desk_matters'] = $matters->result;

        return View::make('pages/dashboard', $data);
    }

    public function getMattersList()
    {
    	$user_id = Input::get('user_id');
    	$page = Input::get('page');
    	$folder_types = Input::get('folder_types');
        $sortby = Input::get('sortby');
        $order = Input::get('order');
        $year = Input::get('year');

    	$matter = new Matter();
		
        $filter['folder_types'] = explode(',', $folder_types);
        $filter['sortby'] = $sortby;
        $filter['order'] = $order;
        if($year != '0') $filter['workdate_year'] = $year;

        $filter['show_select_basic'] = true;
        $filter['show_select_mattertype'] = true;
        $filter['show_select_billdetails'] = true;        
		
        $matter_list = $matter->getList($user_id, $page, $filter);
		$data['matters'] = $matter_list['data'];
        $data['total_page'] = ceil($matter_list['total_count'] / 12);

    	return View::make('pages/office_matters', $data);
    }

    public function getClientsList()
    {
        $user_id = Input::get('user_id');
        $page = Input::get('page');
        $sortby = Input::get('sortby');
        $order = Input::get('order');
        $year = Input::get('year');

        $client = new Client();

        $filter['sortby'] = $sortby;
        $filter['order'] = $order;
        if($year != '0') $filter['workdate_year'] = $year;

        $client_list = $client->getList($user_id, $page, $filter);
        $data['clients'] = $client_list['data'];
        $data['total_page'] = ceil($client_list['total_count'] / 12);

        return View::make('pages/office_clients', $data);
    }

    public function getDashboardsList()
    {
        $filter = array();
        $user_id = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $year = Input::get('year');
        $end_year = $year + 1;
       
        

        if($year != '0') 
            $filter['workdate_year'] = $year;
        else {
            $end_year = date("Y") + 1;
        }

        $billing = new Billing();
        $billing_detail = $billing->getTotalUnbilledAmount($user_id, $filter);
        $total_ar = $billing->getTotalAccountsReceivable($user_id, $filter);
        
        $data['billed_total_amount'] = "$" . number_format($billing_detail->total_billed_amount, 0, '.', ',');
        $data['unbilled_total_amount'] = "$" . number_format($billing_detail->total_unbilled_amount, 0, '.', ',');
        $data['total_ar'] = "$" . number_format($total_ar->total_balance, 0, '.', ',');
        $data['earliest_billing_date'] = $billing_detail->earliest_date;
        $data['latest_billing_date'] = "June ".$end_year;//$billing_detail->latest_date;
        
        return Response::json($data);
        //return Response::json($billing_detail);
    }

    public function getMatterCountTest()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;
        $year = Input::get('year');
        if($year != '0') $filter['workdate_year'] = $year;

        $matter = new Matter();
        $folder_count = $matter->getCountByFolderType($user_id, $filter);

        return Response::json($folder_count);
    }

}

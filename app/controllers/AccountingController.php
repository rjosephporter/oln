<?php
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AccountingController extends BaseController {

    private $monolog;

	public function getIndex()
	{
		return View::make('pages/accounting');
        //return View::make('pages/accounting_v2');
	}

    public function getOld()
    {
        return View::make('pages/accounting');
    }

    public function getNew()
    {
        return View::make('pages/accounting_v2');
    }    

    public function getDebug()
    {
        $billing = new Billing();
        var_dump($billing->getInvoiceTotal('20140702-011'));
    }

	public function getDtInvoice()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;

        if(Input::get('from_date')) $filter['from_date'] = Input::get('from_date');
        if(Input::get('to_date')) $filter['to_date'] = Input::get('to_date');
        if(Input::get('invoice_payment')) $filter['invoice_payment'] = Input::get('invoice_payment');
        $filter['invoice_payment_status'] = Input::get('invoice_payment_status');
        $filter['invoice_unpaid_days'] = Input::get('invoice_unpaid_days');
        $filter['status_array'] = array('0','1');

        $billing = new Billing();
        Session::put('accounting.list', $billing->prepareQueryForGetInvoice($filter)->get());


        return Datatable::query($billing->prepareQueryForGetInvoice($filter))
        ->showColumns('reference_number', 'new_invoice_number', 'UniqueID','BatchNumber','CustomerID', 'CompanyName_A1', 'InvoiceDate', 'User_C', 'Date_C', 'total_amount', 'balance', 'status_id', 'remind_unpaid', 'paid_amount', 'row_color', 'reminder_log', 'InvoiceType', 'matter_id_for_accounting')
        ->searchColumns('inh.UniqueID','inh.BatchNumber','inh.CustomerID', 'cust.CompanyName_A1', 'inh.matter_id', 'inh.reference_number', 'inh.new_invoice_number')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('UniqueID','BatchNumber','CustomerID', 'CompanyName_A1', 'InvoiceDate', 'User_C', 'Date_C', 'total_amount', 'balance', 'status_id', 'remind_unpaid', 'paid_amount', 'row_color', 'InvoiceType', 'matter_id_for_accounting', 'reference_number', 'new_invoice_number')
        ->make();
    }

    public function getExport($type = 'excel')
    {
        if(Session::has('accounting.list')) {
            $data = Session::get('accounting.list');
            switch ($type) {
                case 'excel':
                    Excel::create('Invoice_List_'.time(), function($excel) use ($data) {
                        $excel->sheet('List', function($sheet) use ($data) {
                            $final_data = array();
                            foreach($data as $d) {
                                $final_data[] = array(
                                    'Date'                  => $d->InvoiceDate,
                                    'Reference Number'      => $d->reference_number,
                                    'Invoice Number'        => $d->new_invoice_number, 
                                    'Client ID'             => $d->CustomerID,
                                    'Client Name'           => $d->CompanyName_A1,
                                    'Cost'                  => $d->total_cost,
                                    'Disbursement'          => $d->total_disbursements,
                                    'Less'                  => $d->total_less,
                                    'Total'                 => $d->total_amount,
                                    /*
                                    'reference_number'      => $d->reference_number, 
                                    'new_invoice_number'    => $d->new_invoice_number, 
                                    'UniqueID'              => $d->UniqueID,
                                    'BatchNumber'           => $d->BatchNumber,
                                    'CustomerID'            => $d->CustomerID, 
                                    'CompanyName_A1'        => $d->CompanyName_A1, 
                                    'InvoiceDate'           => $d->InvoiceDate, 
                                    'User_C'                => $d->User_C, 
                                    'Date_C'                => $d->Date_C, 
                                    'total_amount'          => $d->total_amount, 
                                    'balance'               => $d->balance, 
                                    'status_id'             => $d->status_id, 
                                    'remind_unpaid'         => $d->remind_unpaid, 
                                    'paid_amount'           => $d->paid_amount, 
                                    'row_color'             => $d->row_color, 
                                    'reminder_log'          => $d->reminder_log, 
                                    'InvoiceType'           => $d->InvoiceType, 
                                    'matter_id_for_accounting' => $d->matter_id_for_accounting
                                    */
                                );
                            }
                            $sheet->fromArray($final_data);
                        });
                    })->download('xls');
                    break;
                
                default:
                    # code...
                    break;
            }
        }
    }    

    public function getDtInvoiceByMatter()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;

        $billing = new Billing();
        return Datatable::query($billing->prepareQueryForGetInvoiceByMatter($filter))
        ->showColumns('JobAssignmentID', 'Description_A', 'CustomerID', 'CompanyName_A1', 'total_invoice', 'total_balance')
        ->searchColumns('ja.JobAssignmentID', 'ja.Description_A', 'ja.CustomerID', 'cust.CompanyName_A1')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('JobAssignmentID', 'Description_A', 'CustomerID', 'CompanyName_A1', 'total_invoice', 'total_balance')
        ->make();
    }

    public function postPayInvoice()
    {
        $data = Input::all();

        $billing = new Billing();
        $result = $billing->payInvoice($data['payment_batch_number'], $data['payment_amount'], date('Y-m-d', strtotime($data['payment_date'])));

        $data['result'] = $result;

        return Response::json($data);
    }

    public function postDeleteInvoice()
    {
        $data = Input::all();

        $billing = new Billing();

        if(!$billing->isExistInvoicePayment($data['delete_batch_number']))
            $result = $billing->deleteInvoice($data['delete_batch_number']);
        else 
            $result = false;

        $data['result'] = $result;

        /* Log Reason */
        $this->monolog = Log::getMonolog();
        $handler = new StreamHandler(storage_path().'/logs/delete_invoice_reasons.log', Logger::INFO);
        $this->monolog->pushHandler($handler);
        $this->monolog->addInfo(json_encode($data));

        return Response::json($data);
    }

    public function getReminderInfo()
    {
        $reminder_type = Input::get('reminder_type');

        switch ($reminder_type) {
            case 'matter':
                $matter_id = Input::get('matter_id');
                $total_unbilled_amount = Input::get('total_unbilled_amount');

                $matter = new Matter();
                $info = $matter->getInfo($matter_id);
                $nickname = ($info->NickName) ? $info->NickName : '[Handler]';

                $info->message = "Hello " . $nickname . ",\r\n\r\nThis is a reminder that ".$info->CompanyName_A1." still has a total unbilled amount of $".number_format($total_unbilled_amount, 2, '.', ',')." for Matter ID ".$matter_id." (".$info->Description_A."). \r\n\r\nThanks!";
            break;
            
            default: //invoice
                $client_id = Input::get('client_id');
                $batch_number = Input::get('batch_number');
                $balance = Input::get('balance');            

                $client = new Client();
                $info = $client->getHandlerInfo($client_id);

                // For 3rd reminder, address to higher authority
                if(Input::get('reminder_number') == '3') {
                    $info->NickName = 'Robert'; 
                    $info->email_address = 'Robert@testemail.com';                   
                }

                $nickname = ($info->NickName) ? $info->NickName : '[Handler]';

                $info->message = "Hello " . $nickname . ",\r\n\r\nPlease remind " . $info->CompanyName_A1 . " to settle their account for Invoice No. " . $batch_number . " with an amount of $" . number_format($balance, 2, '.', ',') . ".\r\n\r\nThanks!";
            break;
        }

        return Response::json($info);

    }

    public function postSendReminder()
    {
        $billing = new Billing();

        $data['body'] = Input::get('reminder_message');

        Mail::send('emails.simple', $data, function($message)
        {
            $recipient = Input::get('reminder_recipient');
            $recipient_email = Input::get('reminder_recipient_email');
            
            if(empty($recipient_email))
                $recipient_email = Input::get('reminder_recipient');
                
            $batch_number = Input::get('reminder_batch_number');
            $from_email = Auth::user()->email_address;
            $from_name = Auth::user()->EmployeeID;
            $message->to($recipient_email, $recipient)
                    ->from($from_email, $from_name)
                    ->subject('Invoice Payment Reminder ['.$batch_number.']');
        });

        $no_log = Input::get('no_log');
        if(!$no_log) {
            $batch_number = Input::get('reminder_batch_number');
            $logdata = $billing->logInvoiceReminder($batch_number);
            //$data['reminder_count'] = $billing->logInvoiceReminder($batch_number);
            $data['reminder_count'] = $logdata->reminder_count;
            $data['log_data'] = $logdata;
        }

        return Response::json($data);
    }

    public function getSendReminder()
    {
        $billing = new Billing();        

        $filter['invoice_balance'] = true;
        $filter['invoice_id'] = Input::get('invoice_number');
        $invoice_details = $billing->prepareQueryForGetInvoice($filter)->first();

        $request_data = array(
            'client_id'         => Input::get('client_id'),
            'batch_number'      => Input::get('batch_number'),
            'balance'           => Input::get('balance'),
            'reminder_number'   => Input::get('reminder_number')
        );
        $request = Request::create('accounting/reminder-info', 'GET', $request_data);
        $reminder_info = Route::dispatch($request)->getContent();
        $reminder_info = json_decode($reminder_info);      

        $data['type']           = 'accounting';
        $data['invoice_number'] = Input::get('invoice_number');
        $data['batch_number']   = Input::get('batch_number');
        $data['recipient_email']   = $reminder_info->email_address;
        $data['recipient_name']    = $reminder_info->NickName;
        $message = "Hello " . $data['recipient_name'] . ",\r\n\r\nPlease remind " . $reminder_info->CompanyName_A1 . " to settle their account for Invoice No. " . $request_data['batch_number'] . " with an amount of $" . number_format($request_data['balance'], 2, '.', ',') . ".\r\n\r\nThanks!";
        $data['message']        = $message;

        return View::make('pages/invoice_reminder', $data);
    }

}
<?php

class OfficeController extends BaseController {

    public function getIndex()
    {
        $data['handler_list'] = $this->_handlerList();

        $handlers = array();
        foreach($data['handler_list'] as $handler) {
            $handlers[] = $handler->EmployeeID;
        }

        $data['handlers'] = $handlers;

        $data['last_reminder_date'] = null;
        $data['last_reminder_recipients'] = null;

        $email_log = SentEmailLog::latest()->first();
        if(count($email_log) > 0) {
            $last_reminder_recipients = json_decode($email_log->other_info);
            $last_reminder_recipients = implode(', ', $last_reminder_recipients->recipient_handlers);

            $data['last_reminder_date'] = date('F j, Y - h:i A', strtotime($email_log->date_sent));
            $data['last_reminder_recipients'] = $last_reminder_recipients;
        }


        $data['show_reminder_log'] = ( !empty($data['last_reminder_date']) && !empty($data['last_reminder_recipients']) ) ? true : false;


        // Active Conference Rooms
        $conference_room = ConferenceRoom::active();
        $user = new User();
        
        // Get reservations for each conference room
        $date = Input::get('date', date('Y-m-d'));
        $date = date('Y-m-d', strtotime($date));
        $data['conference_rooms'] = $conference_room->with(array('reservation' => function($query) use ($date) {
            $query->where('reservation_date', $date);
        }))->get();

        // Modal data
        $data['modal_data'] = array(
            'modal_id'      => 'add_reservation_modal',
            'modal_title'   => 'Add Reservation',
            'modal_form_id' => 'add_reservation_form'
        );  



        $handler = new Handlers();
        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }

         $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data['handler_list'] = $handler_list;
        //$data['empid']=$employee_id;
        $role=$user->getEmployeesByAssistant($id_active);
        //$data['roles']=$role;
        Debugbar::info($role); // will show in your log


 
        $current_user = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id; 
        if(User::find($current_user)->hasRole('Admin') || User::find($current_user)->hasRole('Handler'))
        $data['condition']=$employee_id;
        else
        $data['condition']=$role[0]->employee_id;

        return View::make('pages/office_', $data);
    }

    public function getMatterCount()
    {
       
        $handler_array = Input::get('handler_array');
        $handler_array = explode(',', $handler_array);
        $folder_counts = array('green' => 0, 'orange' => 0, 'red' => 0, 'blue' => 0, 'untouched' => 0);
        
        
        $final_result = array();
        foreach($handler_array as $handler) {
            $request = Request::create('matters/mattersdetailslisting', 'GET');
            $matters = json_decode(Route::dispatch($request)->getContent());
            
            if($handler == "TLS"){
                foreach($matters as $obj_matter):
                    if($obj_matter->CompleteStatus == 1 ){
                        $obj_matter->matter_type = 'red';
                        $obj_matter->Completed = 0;
                    }
                    else
                    if($obj_matter->CompleteStatus == 2)
                        $obj_matter->matter_type = 'blue';
                    else{
                        $obj_matter->matter_type = '';
                        $obj_matter->Completed = 0;
                    }
                endforeach;
            }else
                $matters = array_filter($matters ,function($info) use($handler){return  $info->Incharge == $handler;});
            
            
            
            foreach($matters as $matter):
                switch($matter->matter_type):
                    case "green": $folder_counts['green'] ++; break;
                    case "orange": $folder_counts['orange'] ++; break;
                    case "red": $folder_counts['red'] ++; break;
                    case "gray": $folder_counts['untouched'] ++; break;
                    case "blue": $folder_counts['blue'] ++; break;
                endswitch;
                
            endforeach;

            $final_result[$handler] = $folder_counts;
        }

        return Response::json($final_result);
    }

    private function _handlerList()
    {
        $params = array();
        $request = Request::create('handlers/hlist', 'GET', $params);
        return json_decode(Route::dispatch($request)->getContent());
    }

    private function _matterDetails($handler_id)
    {
        $params = array(
            'handler'   => $handler_id,
            'type'      => 'red'
        );
        $request = Request::create('matters/msummarylist', 'GET', $params);

        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);

        return json_decode($response->getContent());        
    }

    public function postSendReminder()
    {
        $handlers = explode(',', Input::get('handlers'));
        $user = Auth::user();
        foreach($handlers as $handler) {

            $params = array(
                'handler'   => $handler,
                'type'      => 'red'
            );
            $request = Request::create('matters/msummarylist', 'GET', $params);

            $originalInput = Request::input();
            Request::replace($request->input());
            $response = Route::dispatch($request);
            Request::replace($originalInput);

            $matters =  json_decode($response->getContent());  

            $handlersObj = new Handlers();
            $handler_info = $handlersObj->getInfo($handler);
            
            $data['from_email']    = Auth::user()->email_address;
            $data['from_name']     = Auth::user()->EmployeeID; 
            $data['handler_id']    = $handler;
            $data['handler_name']  = $handler_info->NickName;
            $data['handler_email'] = (App::environment() !== 'live') ? 'admin@m-s-s-asia.com' : $handler_info->email_address;
            $data['base_url']      = URL::to('/');
            $data['matters']       = $matters->result;            

            //Queue::push('SendEmail@gdo_reminder', $data);      

            Queue::push(function($job) use ($data) {
                
                Mail::send('emails/inactive_matter_reminder', $data, function($message) use ($data)
                {
                    $recipient_email = $data['handler_email'];
                    $recipient = $data['handler_name'];   
                    $from_email = $data['from_email'];
                    $from_name = $data['from_name'];
                    $message->to($recipient_email, $recipient)
                            //->from($from_email, $from_name)
                            ->subject('Inactive Matters Reminder');
                });

                $job->delete();

            });

            //create task in Communication Center
            $table = '';
            foreach($matters as $matter):
                $table = '<tr><td>'.$matter->JobAssignmentID.'</td><td'.$matter->client_name.'</td><td>'.$matter->Description_A.'</td><td>'.number_format(abs($matter->unbilled),2).'</td></tr>';
            endforeach;


            $message = <<<EOT
<p>Hello "$handler_info->NickName",</p>
<p>This is to remind you that the following matters below have been inactive for 8 weeks or more.</p>
<table border="1">
<thead>
<th>Matter ID</th>
<th>Client Name</th>
<th>Matter Name</th>
<th>Unbilled Amount</th>
</thead>
<tbody>
"$table"
</tbody>
</table>'
EOT;
            $task['owners'] = $handler_info->user_id;
            $task['minuteid'] = null;
            $task['type'] = '1';
            $task['meetingid'] = null;
            $task['requiredbydate'] = '0000-00-00 00:00:00';

            $task['requestedby'] = $user->user_id;
            $task['message'] = json_encode(
                    array(
                        'subject'=>'Inactive Matters Reminder‏',
                        'message'=>$message
                        )
                    );
            $post['filepaths'] = null;
            $tasks = new Tasks();
            $tasks->createtaskrequest($task);
        }

        // Log reminder
        $log_data['recipient_handlers'] = $handlers;
        $log_result = HelperLibrary::logSentEmail(json_encode($log_data));

        Session::flash('message.success.send_reminder', 'Reminders Sent!');

        return Redirect::to('office');
    }

    public function getDebugRedMatterList()
    {
        $params = array(
            'type'      => 'red'
        );
        $request = Request::create('matters/msummarylist', 'GET', $params);

        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);

        $matters =  json_decode($response->getContent());

        $data['matters'] = $matters->result;

        return View::make('template/debug', $data);
    }

}

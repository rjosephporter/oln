<?php

class ConferenceRoomController extends BaseController {

    private $conference_room;

    public function __construct()
    {
        $this->conference_room = new ConferenceRoom();
    }

	public function getIndex()
	{
		// Active Conference Rooms
		$conference_room = ConferenceRoom::active();
        $user = new User();
		
		// Get reservations for each conference room
		$date = Input::get('date', date('Y-m-d'));
		$date = date('Y-m-d', strtotime($date));
		$data['conference_rooms'] = $conference_room->with(array('reservation' => function($query) use ($date) {
			$query->where('reservation_date', $date);
		}))->get();

		// Modal data
		$data['modal_data']	= array(
			'modal_id'		=> 'add_reservation_modal',
			'modal_title'	=> 'Add Reservation',
			'modal_form_id' => 'add_reservation_form'
		);	



        $handler = new Handlers();
        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }

         $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data['handler_list'] = $handler_list;
        //$data['empid']=$employee_id;
        $role=$user->getEmployeesByAssistant($id_active);
        //$data['roles']=$role;
        Debugbar::info($role); // will show in your log


 
        $current_user = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id; 
        if(User::find($current_user)->hasRole('Admin') || User::find($current_user)->hasRole('Handler'))
        $data['condition']=$employee_id;
        else
            $data['condition']=$employee_id; //$data['condition']=$role[0]->employee_id;


        
                        

		return View::make('pages/conferenceroom__', $data);
	}

    // ajax request for conference room reservations
	public function getConferenceRoomReservations2()
    {
        $result = array();
        $date = '2014-10-31';//Input::get('dateitem',date('Y-m-d'));
        //Input::get('date', date('Y-m-d'));
        
        //if(Request::ajax()) {
            $result = $this->conference_room->active()->with(array('reservation' => function($query) use ($date) {
                $query->where('reservation_date', $date)
                        ->orderBy('start_time', 'asc');
            }))->get();     

            print_r($result); 

            while ($record = $result->fetch()) {
                $event_array[] = array(
                    'id' => $record['id'],
                    'title' => $record['purpose'],
                    'start' => $record['reservation_date'].' '.$record['start_time'],
                    'end' => $record['reservation_date'].' '.$record['end_time'],
                    'resourceId' =>$record['conference_room_id'],
                );
            }
      
        //}

        return Response::json($event_array);
    }

    public function getConferenceRoomReservations()
    {
        $result = array();
        $date = Input::get('dateitem',date('Y-m-d'));
        //Input::get('date', date('Y-m-d'));
        
        //if(Request::ajax()) {
            $result['conference_rooms'] = $this->conference_room->active()->with(array('reservation' => function($query) use ($date) {
                $query->where('reservation_date', $date)
                        ->orderBy('start_time', 'asc');
            }))->get();            
        //}

        return Response::json($result);
    }

	public function getDebug()
	{
		$result = array();

		$conference_room = ConferenceRoom::active();
		$result = $conference_room->with(array('reservation' => function($query) {
			$query->where('reservation_date', '2014-09-26');
		}))->get();

		/*
		foreach(ConferenceRoom::with('reservation')->get() as $room) {
			$result[$room->id] = $room->reservation;
		}
		*/

		//echo json_encode($result);

		return Response::json($result);
	}

	public function getReservation()
    {

        $handler = new Handlers();
        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }

        $data['handler_list'] = $handler_list;
        $data['conference_id'] = Input::get('roomid');

		// Modal data
		$data['modal_data']	= array(
			'modal_id'		=> 'add_reservation_modal',
			'modal_title'	=> 'Add Reservation',
			'modal_form_id' => 'add_reservation_form'
		);	
  
        return View::make('pages/conference_room_reservation', $data);
    }

    public function getNew()
    {

        $id_active = Auth::user()->user_id;
        //if(in_array($id_active, Config::get('oln.admin'))){
        //if(User::hasRoleById($id_active, 'Admin')){
        if(User::isAdmin($id_active)){
            //allowed
            $redirectpage='pages/conference_room_new';

        }else{
            //do something else
            $redirectpage='pages/conference_room_new';
        }

        // Modal data
        $data['modal_data'] = array(
            'modal_id'      => 'add_reservation_modal',
            'modal_title'   => 'Add Reservation',
            'modal_form_id' => 'add_reservation_form'
        );  
  
        return View::make($redirectpage, $data);
    }

    public function postDeleteRoom()
    {
        $response = array();
        if(!Input::has('conference_room_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [conference_room_id]';
            return Response::json($response);
        }

        $conference_room_id = Input::get('conference_room_id');
        $delete_result = $this->conference_room->delete($conference_room_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Conference room deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postSubmitnewroom()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        

        $item_count = count($data['new_conference_room_name']);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_conference_room_name'     => $data['new_conference_room_name'][$i],
                    'new_conference_room_description' => $data['new_conference_room_description'][$i]
                    
                ),
                array(
                    'new_conference_room_name'     => 'required',
                    'new_conference_room_description'=> 'required'
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($this->conference_room->createRoom($item_count, $data)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return Response::json($result);
    }  

    public function getCalendar()
    {
        // Active Conference Rooms
        $conference_room = ConferenceRoom::active();

        
        // Get reservations for each conference room
        $date = Input::get('date', date('Y-m-d'));
        $date = date('Y-m-d', strtotime($date));
        $data['conference_rooms'] = $conference_room->with(array('reservation' => function($query) use ($date) {
            $query->where('reservation_date', $date);
        }))->get();

        // Modal data
        $data['modal_data'] = array(
            'modal_id'      => 'add_reservation_modal',
            'modal_title'   => 'Add Reservation',
            'modal_form_id' => 'add_reservation_form'
        );  

        $data['conference_id'] = Input::get('roomid');

        $handler = new Handlers();
        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }

        $data['handler_list'] = $handler_list;

        return View::make('pages/conferenceroom_calendar', $data);
    }

}
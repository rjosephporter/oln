<?php

class EmailController extends Controller {

    public function getEmailTask()
    {
        Debugbar::disable();
        switch(Input::get('action')):
            case 'new':
                $message = array('message'=>'<message_container>');
                break;
            case 'refused':
                $tasks = new Tasks();
                $task = $tasks->gettask(Input::get('taskid'));
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original task from ".$task_data->NickName;
                $replies = $tasks->gettaskreplies(Input::get('taskid'));
                $final_message = '<final_message_container>';
                break;
            case 'approved':
                $tasks = new Tasks();
                $task = $tasks->gettask(Input::get('taskid'));
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original task from ".$task_data->NickName;
                $replies = $tasks->gettaskreplies(Input::get('taskid'));
                $final_message = '<final_message_container>';
                break;
            case 'forcefinished':
                $tasks = new Tasks();
                $task = $tasks->gettask(Input::get('taskid'));
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original task from ".$task_data->NickName;
                $replies = $tasks->gettaskreplies(Input::get('taskid'));
                $final_message = '<final_message_container>';
                break;
            case 'conversation':
                $tasks = new Tasks();
                $task = $tasks->gettask(Input::get('taskid'));
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original task from ".$task_data->NickName;
                $replies = $tasks->gettaskreplies(Input::get('taskid'));
                break;
            case 'finished':
                $tasks = new Tasks();
                $task = $tasks->gettask(Input::get('taskid'));
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original task from ".$task_data->NickName;
                $replies = $tasks->gettaskreplies(Input::get('taskid'));
                $final_message = '<final_message_container>';
                break;
        endswitch;
        $footer_message = 'You can login to the OEMS system <a href="'.URL::to('/').'/">here</a> to reply.<br><br>';
        $data = array();
        $data['title'] = 'OLN OEM Task';
        $data['message'] = $message;
        if(isset($final_message)){
            $data['final_message'] = $final_message;
        }
        if(isset($message_header)){
            $data['message_header'] = $message_header;
        }
        if(isset($replies)){
            $data['replies'] = $replies;
        }
        $data['footer_message'] = $footer_message;
        return View::make('emails/email_task',$data);
    }

    public function getEmailRequest()
    {
        $get = $_GET;
        switch($get['action']):
            case 'conversation':
                $tasks = new Tasks();
                $task = $tasks->getrequest($get['requestid']);
                foreach($task as $temp):
                    $task_data = $temp;
                endforeach;
                $message = json_decode($task_data->message,true);
                $message_header = "<hr>Original request from ".$task_data->requestedby_name;
                $replies = $tasks->getrequestreplies($get['requestid']);
                break;
        endswitch;
        $footer_message = 'You can login to the AAC system <a href="'.URL::to('/').'/">here</a> to reply.<br><br>';
        $data = array();
        $data['title'] = 'OLN OEM Task';
        $data['message'] = $message;
        if(isset($message_header)){
            $data['message_header'] = $message_header;
        }
        if(isset($replies)){
            $data['replies'] = $replies;
        }
        $data['footer_message'] = $footer_message;
        return View::make('emails/email_task',$data);
    }
    
    public function getSendnewtimesheetentries()
    {
        Helper::send_latest_timesheet_entries();
    }
    
    public function getFilegettest()
    {
        $request = Request::create('/invoice-print/20141205165704621?raw=2', 'GET');
        $originalInput = Request::input();
//
        Request::replace($request->input());
//
//        // Dispatch your request instance with the router.
        $response = Route::dispatch($request);
//
//        // Replace the input again with the original request input.
        Request::replace($originalInput);
        echo $response->getContent();   
    }
    
    function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    
    function getFileContents($url)
    {
        $request = Request::create($url, 'GET');
        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);
        return $response->getContent();   
    }
}
<?php

class ConferenceRoomReservationController extends BaseController {

    private $conference_room_reservation;

    public function __construct()
    {
        $this->conference_room_reservation = new ConferenceRoomReservation();
    }

    public function getIndex()
    {
        // Active Conference Rooms
        $conference_room_reservation = ConferenceRoomReservation::active();

        
        // Get reservations for each conference room
        $date = Input::get('date', date('Y-m-d'));
        $date = date('Y-m-d', strtotime($date));
        $data['conference_rooms'] = $conference_room->with(array('reservation' => function($query) use ($date) {
            $query->where('reservation_date', $date);
        }))->get();
        $user = new User();

        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        // Modal data
        $data['modal_data'] = array(
            'modal_id'      => 'add_reservation_modal',
            'modal_title'   => 'Add Reservation',
            'modal_form_id' => 'add_reservation_form'
        );  

        $handler = new Handlers();
        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }

        $data['handler_list'] = $handler_list;


    

        return View::make('pages/conference_room_reservation', $data);
    }

    public function postSavenewreservation()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $data['conference_id'] = Input::get('roomid');
        //var_dump($data2);
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);
        //$room_num=$data2['conference_id'];
        //$resdate=$data2['new_reservation_date'][0];

        $item_count = count($data['reservedby']);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    
                    'conference_room_id'=> $data['conference_id'] ,
                    'reserved_by'       => $data['reservedby'][$i],
                    'reservation_date'  => $data['new_reservation_date'][$i],
                    'purpose'           => $data['new_purpose'][$i],
                    'notes'             => $data['new_notes'][$i],
                    'start_time'        => $data['new_start_time'][$i],
                    'end_time'          => $data['new_end_time'][$i],
                    'status'            => '1',
                    'created_by'        => $employee_id,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                    
                ),
                array(
                    //'conference_room_id'     => 'required',
                    'reserved_by'            => 'required',
                    'reservation_date'       => 'required',
                    'purpose'                => 'required',
                    'notes'                  => 'required',
                    'start_time'             => 'required',
                    'end_time'               => 'required',
                    'status'                 => 'required',
                    'created_by'             => 'required',
                    'created_at'             => 'required',
                    'updated_at'             => 'required'

                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($this->conference_room_reservation->createReservation($item_count, $data)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return Response::json($result);
    }
	
    public function postSubmitnewreservation()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);
        $room_num=$data['conferenceid'] ;
        $time=$data['new_start_time'][0];
        $startdate=explode(" ", $time);
        $startdate=$startdate[0];
        

        $get_result = $this->conference_room_reservation->getTimes($room_num,$startdate);
        //var_dump($get_result);

        $result['val'] = $get_result;
        $result['date'] = date('Y-m-d', strtotime($startdate));

        return Response::json($result);
    } 

    public function postSavenewreservation2()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
       // $data['conference_id'] = //Input::get('roomid');
        //var_dump($data2);
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);
        //$room_num=$data2['conference_id'];
        //$resdate=$data2['new_reservation_date'][0];
        
        //edited by cha 111014
        $time=$data['new_start_time'][0];
        $timee=$data['new_end_time'][0];
        
        $time2=date_format(date_create($time), 'Y-m-d H:i:s');
        $time3=date_format(date_create($timee), 'Y-m-d H:i:s');
        $startdate=explode(" ", $time2);
        $startdatee=explode(" ", $time3);
        $startdate1=$startdate[0];

       // $startdate=moment($time).format('YYYY-MM-DD');//Mon Nov 10 2014 09:00:00 GMT+0800 (PHT)

        /*$time=$data['new_time'][0];
        $resdate=explode("-", $time);
        $resdate1=explode(" ", $resdate[0]);
        $enddate1=explode(" ", $resdate[1]);
   
        $startdate=$resdate1[0];
        $starttime=$resdate1[1]."".$resdate1[2];
        $endtime=$enddate1[1]."".$enddate1[2];*/

        $item_count = count($data['reservedby']);

        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    
                    'conference_room_id'=> $data['conferenceid'] ,
                    'id'=> $data['eventid'] ,
                    'reserved_by'       => $data['reservedby'][$i],
                    'reservation_date'  => $startdate1,//$data['new_reservation_date'][$i],
                    'purpose'           => $data['new_purpose'][$i],
                    'notes'             => (Input:: has('new_notes') ? $data['new_notes'][$i] : ''),
                    'start_time'        => $startdate[1],//$data['new_start_time'][$i],//$starttime,
                    'end_time'          => $startdatee[1],//$data['new_end_time'][$i],//$endtime,
                    'status'            => '1',
                    'created_by'        => $employee_id,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                    
                ),
                array(
                    //'conference_room_id'     => 'required',
                    'reserved_by'            => 'required',
                    //'reservation_date'       => 'required',
                    'purpose'                => 'required',
                    //'notes'                  => 'required',
                    //'start_time'             => 'required',
                    //'end_time'               => 'required',
                    'status'                 => 'required',
                    'created_by'             => 'required',
                    'created_at'             => 'required',
                    'updated_at'             => 'required'

                )
            );

            
        }

         $update_data = array(
             'conference_room_id'=> $data['conferenceid'] ,
                    'id'=> $data['eventid'] ,
                    'reserved_by'       => $data['reservedby'][0],
                    'reservation_date'  => $startdate1,//$data['new_reservation_date'][$i],
                    'purpose'           => $data['new_purpose'][0],
                    'notes'             => (Input:: has('new_notes') ? $data['new_notes'][0] : ''),
                    'start_time'        => $startdate[1],//$data['new_start_time'][$i],//$starttime,
                    'end_time'          => $startdatee[1],//$data['new_end_time'][$i],//$endtime,
                    'status'            => '1',
                    
                    
                    'updated_at'        => date('Y-m-d H:i:s')
        
        );


        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        //var_dump($data['eventid']);
        Debugbar::input($data['eventid']);
        if(Input:: has('eventid')){
                $res=$this->conference_room_reservation->updateReservation($data['eventid'], $update_data); 
        }
        else{
                $res= $this->conference_room_reservation->createReservation($item_count, $data);
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } else if($res){
                //$result['res']= $res;
             $result['status'] = 'success';
             $result['msg'] = 'Updated Successfully';
        }else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return Response::json($result);
    }   

    public function getAllreservation()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();

        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);
       // $data['conferenceid'] = '3';//Input::get('roomid');
        //$room_num=$data['conferenceid'];

        //Debugbar::info($room_num);
        $get_result = $this->conference_room_reservation->getAllTimes2();
        //var_dump($get_result);

        $result['val'] = $get_result;
        //$result['date'] = date('Y-m-d', strtotime($resdate));

        
        $directorylist=$get_result;
        //print_r($get_result);
        $formattedEventData=array();
        $arrne=array();
        for ($k = 0; $k < count($directorylist); $k += 1) {
             /*$str='{"title": "Reserved By: ".$directorylist[$k]->reserved_by." \nPurpose: ".$directorylist[$k]->purpose,
                            "start": ($directorylist[$k]->start_time !="00:00:00" ? $directorylist[$k]->reservation_date+"T"+$directorylist[$k]->start_time : $directorylist[$k]->reservation_date),
                            "end": ($directorylist[$k]->end_time !="00:00:00" ? $directorylist[$k]->reservation_date+"T"+$directorylist[$k]->end_time : $directorylist[$k]->reservation_date),
                            "resourceId": $directorylist[$k]->conference_room_id}';*/
             //$arr = json_decode($formattedEventData, true);
             array_push($arrne,array('description' => "Reserved By: ".$directorylist[$k]->reserved_by,'calid'=>$directorylist[$k]->id,'title'=>"Purpose: ".$directorylist[$k]->purpose,'purpose'=>$directorylist[$k]->purpose, 'start' => ($directorylist[$k]->start_time !="00:00:00" ? $directorylist[$k]->reservation_date." ".$directorylist[$k]->start_time : $directorylist[$k]->reservation_date), 
                'end' => ($directorylist[$k]->end_time !="00:00:00" ? $directorylist[$k]->reservation_date." ".$directorylist[$k]->end_time : $directorylist[$k]->reservation_date),"resources:"=>$directorylist[$k]->conference_room_id."","allDay"=>false));
             
             //$arrne['name'] = "dsds";
             //echo json_encode($arrne);
             //array_push( $formattedEventData, $arrne);
           
                          
        }
        //$formattedEventData=array($arrne);

        //print_r($formattedEventData);

        return Response::json($directorylist);
    } 

    public function getPage(){
        return Redirect::back()
                ->withInput(Input::get('seldate'));

    }

    public function postDeletereservation()
    {
        $data = Input::all();
        $dir_id = $data['eventidname'];
        $delete_result = $this->conference_room_reservation->deletereservation($dir_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Reservation deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

}
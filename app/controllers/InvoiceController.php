<?php

class InvoiceController extends BaseController {

    public function getIndex()
    {
        return View::make('pages/invoice');
    }

    public function getPrint($invoice_id = null)
    {
        Debugbar::disable();
        $billing = new Billing();
        $handler = new Handlers();
        
        $filter = array();
        $filter['invoice_id'] = $invoice_id;
        $filter['remember_query'] = 5;
        $filter['timesheet']['join'] = true;
        $filter['timesheet']['order_by'] = 'ts.WorkDate';
        $filter['timesheet']['sort_order'] = 'asc';
        $filter['timesheet']['group_by'] = 'ind.ReferenceID';
        
        $invoice_items = $billing->prepareQueryForGetInvoiceDetail($filter)->get();

        // Get min and max work date
        $inner_query = $billing->prepareQueryForGetInvoiceDetail($filter)->toSql();
        $bindings = $billing->prepareQueryForGetInvoiceDetail($filter)->getBindings();
        $query = DB::select('select min(WorkDate) as min_workdate, max(WorkDate) as max_workdate from ('.$inner_query.') as t1', $bindings);
        $min_workdate = $query[0]->min_workdate;
        $max_workdate = $query[0]->max_workdate;

        $filter['timesheet']['group_by'] = 'tsw.EmployeeID';

        /*if(count($billing->getCostByHandler($invoice_id)) > 0) {
            $cost_per_handler = array();
            $ctr = 0;
            foreach($billing->getCostByHandler($invoice_id) as $i) {
                $cost_per_handler[$ctr] = new stdClass;
                $cost_per_handler[$ctr]->NickName = $handler->getInfo($i->handler_id)->NickName;
                $cost_per_handler[$ctr]->EmployeeID = $i->handler_id;
                $cost_per_handler[$ctr]->HourlyRate = $i->hourly_rate;
                $cost_per_handler[$ctr]->total_amount = $i->amount;
                $ctr++;
            }

        } else {*/
            $cost_per_handler = $billing->prepareQueryForGetInvoiceDetail($filter)->get();
        /*}*/

        //Get position for each employee
        $user = new User;
        $employee_job = array();
        foreach ($cost_per_handler as $ndx => $item) {
            $ejobObj = $user->getEmployeeJob( array( User::getUserID( $item->EmployeeID ) ) );
            $employee_job[$item->EmployeeID] = isset($ejobObj[0]->PositionDesc_A) ? $ejobObj[0]->PositionDesc_A: '';
        }

        $disbursements = $billing->getDisbursements($invoice_id);

        $less = $billing->getInvoiceLessItems($invoice_id);

        // Get Totals
        $total_complimented_units = 0;
        $total_units = 0;
        $total_costs = 0;
        $total_disbursements = 0;
        $total_less = 0;
        foreach($invoice_items as $item) {
            $total_complimented_units += $item->complimented_units > 0 ? $item->complimented_units : 0;
            $total_units += $item->charged_units;
        }
        foreach($cost_per_handler as $cph) {
            $total_costs += $cph->total_amount;
        }
        foreach($disbursements as $disb) {
            $total_disbursements += $disb->amount;
        }
        foreach($less as $l) {
            $total_less += $l->amount;
        }     

        $raw = Input::get('raw', false);

        $data['parent_template'] = $raw ? 'template/print' : 'template/default';
        $data['title'] = 'Invoice No. '. $invoice_id;
        $data['invoicehead'] = $billing->getInvoiceHead($invoice_id);

        //Get client info
        $client = Client::find($data['invoicehead']->CustomerID);
        $data['client']['id']               = $client->CustomerID;
        $data['client']['name']             = $client->CompanyName_A1;
        $data['client']['address']          = $client->CORADDRESS;
        $data['client']['attn']             = $client->REPNAME;
        $data['client']['email']            = $client->EMAIL;
        $data['client']['telephone']        = $client->TELNUM;
        $data['client']['fax']              = $client->FAXNUM;

        $data['matter'] = Matter::find($data['invoicehead']->matter_id);
        $data['handler'] = $handler->getInfo($data['matter']->Incharge);
        $data['min_workdate'] = $min_workdate;
        $data['max_workdate'] = $max_workdate;
        $data['invoice_items'] = $invoice_items;
        $data['costs'] = $cost_per_handler;
        $data['disbursements'] = $disbursements;
        $data['less'] = $less;

        $data['total_complimented_units'] = $total_complimented_units;
        $data['total_units'] = $total_units;
        $data['total_costs'] = $total_costs;
        $data['agreed_cost'] = $data['invoicehead']->agreed_cost;
        $data['total_disbursements'] = $total_disbursements;
        $data['total_less'] = $total_less;
        $actual_cost = ( !empty($data['agreed_cost']) ) ? $data['agreed_cost'] : $total_costs;
        $data['total_cost_disb'] = ($actual_cost + $total_disbursements) - $total_less;

        $data['employee_job'] = $employee_job;

        $data['template'] = Config::get('oln.invoice.template_1');
        return $raw == 2 ? View::make('template/invoice1', $data) : View::make('print/invoice', $data);
    }

    public function getDtInvoice()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;

        if(Input::get('from_date')) $filter['from_date'] = Input::get('from_date');
        if(Input::get('to_date')) $filter['to_date'] = Input::get('to_date');
        if(Input::get('status')) $filter['status'] = Input::get('status');
        
        $matter_id = trim(Input::get('matter_id'));
        if(!empty($matter_id)) $filter['matter_id'] = $matter_id;

        $filter['remember_query'] = 5;
        $filter['invoice_balance'] = true;

        $billing = new Billing();
        return Datatable::query($billing->prepareQueryForGetInvoice($filter))
        ->showColumns('source','UniqueID', 'reference_number', 'new_invoice_number', 'BatchNumber','CustomerID', 'CompanyName_A1', 'InvoiceDate', 'User_C', 'Date_C', 'total_amount', 'matter_id', 'color_code', 'matter_total_billed_amount', 'invoice_balance', 'matter_total_balance', 'invoice_matter_id')
        ->searchColumns('inh.UniqueID','inh.BatchNumber','inh.CustomerID', 'cust.CompanyName_A1', 'inh.matter_id', 'inh.reference_number', 'inh.new_invoice_number')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('source','UniqueID', 'reference_number', 'new_invoice_number', 'BatchNumber','CustomerID', 'CompanyName_A1', 'InvoiceDate', 'User_C', 'Date_C', 'total_amount', 'matter_total_billed_amount', 'invoice_balance', 'matter_total_balance', 'inh.matter_id')
        ->make();
    }

    public function getDtInvoicedetail()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;

        if(Input::get('invoice_id')) $filter['invoice_id'] = Input::get('invoice_id');

        $billing = new Billing();

        $inner_query = $billing->prepareQueryForGetInvoiceDetail($filter)->toSql();
        $bindings = $billing->prepareQueryForGetInvoiceDetail($filter)->getBindings();

        $query = DB::select('select sum(rounded_amount) as total_amount from ('.$inner_query.') as t1', $bindings);
        $data['total_amount'] = $query[0]->total_amount;

        $filter['remember_query'] = 5;

        return Datatable::query($billing->prepareQueryForGetInvoiceDetail($filter))
        ->showColumns('ReferenceID','ItemLineNo', 'Description', 'rounded_amount')
        ->searchColumns('ReferenceID','Description')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('ReferenceID','ItemLineNo', 'Description', 'rounded_amount')
        ->make_with_param($data);
    }

    public function getMatterInvoiceBalance()
    {
        $matter_id_array = Input::get('matter_id_array');

        $matter = new Matter();

        $result = array();
        $raw_result = $matter->getInvoiceBalance($matter_id_array);
        foreach($raw_result as $data) {
            $result[$data->JobAssignmentID] = $data->total_invoice_balance;
        }

        return Response::json($result);
    }

    public function postSendReminder()
    {
        $data['body'] = Input::get('reminder_message');
        Mail::send('emails.simple', $data, function($message)
        {
            $recipient = Input::get('reminder_recipient');
            $recipient_email = Input::get('reminder_recipient_email');
            
            if(empty($recipient_email))
                $recipient_email = Input::get('reminder_recipient');
                
            $batch_number = Input::get('reminder_batch_number');
            $from_email = Auth::user()->email_address;
            $from_name = Auth::user()->EmployeeID;
            $message->to($recipient_email, $recipient)
                    ->from($from_email, $from_name)
                    ->subject('Invoice Payment Reminder ['.$batch_number.']');
        });

        $no_log = Input::get('no_log');
        if(!$no_log) {
            $batch_number = Input::get('reminder_batch_number');
            $logdata = $billing->logInvoiceReminder($batch_number);
            //$data['reminder_count'] = $billing->logInvoiceReminder($batch_number);
            $data['reminder_count'] = $logdata->reminder_count;
            $data['log_data'] = $logdata;
        }

        return Response::json($data);
    }

    /* New invoice as of 10.28.2014 */
    public function getCreateNew()
    {
        $data = array();
        $this->_generateInvoiceSession();

        //Check if matter_id is passed
        if( Input::has('matter_id') ) {
            $matter = new Matter;
            $matter_id = Input::get('matter_id');

            //Check if able to create invoice for matter
            if( ! Sessions::ableTo('save-invoice', $matter_id)) {
                $error_data['type'] = 'invoice';
                $error_data['matter_id'] = $matter_id;
                return View::make('errors/matter_in_use', $error_data);
            }            

            //Check if matter exists
            if( ! $matter->exists($matter_id) ) {
                App::abort(404);
            } else {
                $filter['matter_id'] = $matter_id;
                $filter['unbilled_only'] = 1;
                $filter['sort_by'] = 'ts.WorkDate';
                $filter['sort_order'] = 'asc';

                //Get matter info
                $info = $matter->getInfo($matter_id);
                $client = Client::find($info->CustomerID);

                //Get handler info who handles the matter
                $handler_user_id = User::getUserID($info->Incharge);
                $handler_email = isset(User::find($handler_user_id)->email_address) ? User::find($handler_user_id)->email_address : ''; 

                //Get min and max workdate
                $inner_query = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->toSql();
                $bindings = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->getBindings();
                $query = DB::select('select min(t1.WorkDate) as min_workdate, max(t1.WorkDate) as max_workdate from ('.$inner_query.') as t1', $bindings);
                $min_workdate = $query[0]->min_workdate;
                $max_workdate = $query[0]->max_workdate;                

                //Get all available timesheet items for matter
                $all_timesheet = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->get();
                $timesheet_by_handler = array();
                foreach($all_timesheet as &$timesheet) {
                    $timesheet->included = true;
                    $timesheet->complimentary = false;
                    $timesheet_by_handler[$timesheet->EmployeeID][] = $timesheet;
                }

                //Get cost summary
                $filter['group_by'] = 'tsw.EmployeeID';
                $cost_summary = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->get();

                //Get totals
                $filter['group_by'] = 'ts.JobAssignmentID';
                $cost_totals = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->first();

                //Get disbursements from ledger
                $disbursementsQuery = DB::table('invoice_disbursement')->where('matter_id', $matter_id);
                $disbursements_total = $disbursementsQuery;
                $disbursements = $disbursementsQuery->get();
                $disbursements_total = $disbursements_total->sum('amount');

                //Get fixed amount
                $utils = new Utilities;
                $jobassignmentdetails = $utils->getMatterDetails($matter_id);
                $agreed_cost = null;
                if(count($jobassignmentdetails) > 0) {
                    $agreed_cost = ($jobassignmentdetails[0]->fixedpay == 1) ? $jobassignmentdetails[0]->fixedamount : null;
                }

                //Get less
                $request = Request::create('matters/balance/'.$matter_id, 'GET');
                $less = Route::dispatch($request)->getContent();
                $less = json_decode($less);                
                $less_arr = array();
                $less = $less->balance_on_account;
                if($less > 0) {
                    Debugbar::info('sulod');
                    $lessObj = new stdClass;
                    $lessObj->invoice_less_id = '';
                    $lessObj->description = 'Cost on account';
                    $lessObj->amount = $less;
                    $lessObj->type = 'deposit';
                    $less_arr[] = $lessObj;

                }
                $less_total = 0;
                foreach($less_arr as $less_item)
                    $less_total += $less_item->amount;

                //Set data for view
                $data['form_type']                  = 'new';
                $data['id']                         = Session::get('invoice.generated_id');
                $data['batch_number']               = Session::get('invoice.generated_batchno');
                $data['reference_number']           = $info->Incharge.'/'.$matter_id.'/'.date('Y').'/'.$client->Incharge;
                $data['new_id']                     = Session::get('invoice.generated_new_id');
                $data['handler_email']              = $handler_email;
                $data['date']                       = date('F j, Y');
                $data['created_by']                 = Auth::user()->EmployeeID;
                $data['status']                     = -2;
                $data['matter']['id']               = $matter_id;                
                $data['matter']['description']      = $info->Description_A;
                $data['client']['id']               = $info->CustomerID;
                $data['client']['type']             = $client->CUSTTYPE;
                $data['client']['name']             = $info->CompanyName_A1;
                $data['client']['address']          = $client->CORADDRESS;
                $data['client']['attn']             = $client->REPNAME;
                $data['client']['email']            = $client->EMAIL;
                $data['client']['telephone']        = $client->TELNUM;
                $data['client']['fax']              = $client->FAXNUM;
                $data['timesheet']['min_date']      = $min_workdate;
                $data['timesheet']['max_date']      = $max_workdate;
                $data['timesheet']['list']          = $all_timesheet;
                $data['timesheet']['by_handler']    = $timesheet_by_handler;
                $data['timesheet']['cost_summary']  = $cost_summary;
                $data['timesheet']['agreed_cost']   = $agreed_cost;
                $data['disbursements']              = $disbursements;
                $data['less']                       = $less_arr;
                $data['remarks']                    = null;
                $data['total']['cost']['units']     = isset($cost_totals->sum_units) ? $cost_totals->sum_units : 0;
                $data['total']['cost']['amount']    = isset($cost_totals->sum_amount) ? $cost_totals->sum_amount : 0;
                $data['total']['disbursement']      = $disbursements_total;
                $data['total']['less']              = $less_total;

                $final_cost_amount = ($agreed_cost !== null) ? $agreed_cost : $data['total']['cost']['amount'];

                $data['total']['overall']           = $final_cost_amount + $data['total']['disbursement'] - $data['total']['less'];

                $data['template'] = Config::get('oln.invoice.template_1');
            }

        } else {
            //Do something when no matter_id
        }

        //Put current $data to session
        Session::put('invoice.data', $data);
        //Debugbar::info(Session::get('invoice.data'));

        //Generate view

        if(Input::has('raw'))
            return Response::json($data);
        else
            return View::make('pages/invoice_create_v2', $data);
        //return Response::json($data);
        //echo json_encode($data);

    }

    private function _generateInvoiceSession()
    {
        if(!Session::has('invoice.generated_id') && !Session::has('invoice.generated_batchno') && !Session::has('invoice.generated_new_id')) {
            $billing = new Billing();
            $invoicehead = $billing->saveInvoiceHead();
            Session::put('invoice.generated_id', $invoicehead->UniqueID);
            Session::put('invoice.generated_batchno', $invoicehead->BatchNumber);
            Session::put('invoice.generated_new_id', $invoicehead->new_invoice_number);
        }        
    }

    public function getCreateNewOld()
    {
        $data = array();
        $this->_generateInvoiceSession();

        if(Input::has('matter_id')) {
            $matter = new Matter;
            $matter_id = Input::get('matter_id');
            if(!$matter->exists($matter_id)) {
                App::abort(404);
            } else {
                $info = $matter->getInfo($matter_id);
                $data['matter_description'] = $info->Description_A;
                $data['client_id']          = $info->CustomerID;
                $data['client_name']        = $info->CompanyName_A1;

                // Get all available timesheet items for matter
                /*
                $filter['matter_id'] = $matter_id;
                $filter['unbilled_only'] = 1;
                $all_timesheet = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->get();
                $timesheet_by_handler = array();
                foreach($all_timesheet as $timesheet) {
                    $timesheet_by_handler[$timesheet->EmployeeID][] = $timesheet;
                }
                */

                $timesheet_by_handler = $this->_allTimesheetByHandler($matter_id, false);

                // Get min and max hourly rate per handler
                $hourly_rate = array();
                foreach($timesheet_by_handler as $handler => $timesheet) {
                    foreach($timesheet as $item) {
                        $hourly_rate[$handler][] = $item->HourlyRate;
                    }
                }
                $final_hourly_rate = array();
                foreach($hourly_rate as $handler => $rate) {
                    $final_hourly_rate[$handler]['min'] = min($rate);
                    $final_hourly_rate[$handler]['max'] = max($rate);
                }
                Debugbar::info($final_hourly_rate);
                $data['hourly_rate_by_handler'] = $final_hourly_rate;
                $data['timesheet_by_handler'] = $timesheet_by_handler;
            }
        }

        $data['form_type']           = 'new';
        $data['invoicehead_id']      = Session::get('invoice.generated_id');
        $data['invoicehead_batchno'] = Session::get('invoice.generated_batchno'); 

        return View::make('pages/invoice_create', $data);        
    }

    private function _allTimesheetByHandler($matter_id, $unbilled_only = true, $invoice_number = false)
    {
        $matter = new Matter();

        // Get all available timesheet items for matter
        $filter['matter_id'] = $matter_id;
        if($unbilled_only == true) $filter['unbilled_only'] = 1;
        if($invoice_number !== false) $filter['invoice_number'] = $invoice_number;
        $filter['billable_unbillable'] = 1;
        $all_timesheet = $matter->prepareQueryForGetTimesheet($this->principal_user, $filter)->get();
        $timesheet_by_handler = array();
        foreach($all_timesheet as $timesheet) {
            $timesheet_by_handler[$timesheet->EmployeeID][] = $timesheet;
        }

        return $timesheet_by_handler;        
    }

    public function getMatterInfo()
    {
        $matter = new Matter();
        $matter_id = Input::get('matter_id');
        $info = $matter->getInfo($matter_id);
        return Response::json($info);
    }

    /* New invoice saving as of 10.29.14 */
    public function postSave2()
    {    
        //if(Input::get('form_type') == 'update')
        //return Response::json(Input::all());

        $response = array();
        $error_ctr = 0;
        $error_msgs = array();
        
        $matter = new Matter();
        $client = new Client();
        $user = new User();
        $timesheet = new Timesheet();
        $billing = new Billing();
        
        /* Get raw data */
        $form_type                      = Input::get('form_type');
        $matter_id                      = Input::get('matter.id');
        $client_id                      = Input::get('client.id');

        $reference_number               = Input::get('reference_number');
        $new_invoice_number             = Input::get('new_id');

        $invoice_number                 = Input::get('id');
        $invoice_batch_number           = Input::get('batch_number');
        $invoice_date                   = Input::get('date');
        $created_by                     = (Input::has('created_by')) ? Input::get('created_by') : Auth::user()->EmployeeID;
        $invoice_type                   = Input::get('invoice_type');
        $approver                       = Input::get('approver');
        $status                         = Input::get('status');
        //$other_data['remarks']          = Input::get('remarks');
        $invoice_items['timesheet']     = Input::get('timesheet.list');
        $invoice_items['costs_by_handlers'] = Input::get('timesheet.cost_summary');
        
        $invoice_items['disbursements'] = Input::get('disbursements');
        $invoice_items['less']          = Input::get('less');

        $other_data['agreed_cost']      = Input::get('timesheet.agreed_cost');
        $other_data['total_cost']       = Input::get('total.cost.amount');
        $other_data['total_disbursements'] = Input::get('total.disbursement');
        $other_data['total_less']       = Input::get('total.less');
        $other_data['total_invoice']    = Input::get('total.overall');

        /* Data validation */
        if(!$matter->exists($matter_id)) {
            $error_ctr++;
            $error_msgs[] = 'Matter ID does not exist';
        }
        if(!$client->exists($client_id)) {
            $error_ctr++;
            $error_msgs[] = 'Client ID does not exist';
        }
        if(!strtotime($invoice_date)) {
            $error_ctr++;
            $error_msgs[] = 'Invalid Invoice date';            
        } else $invoice_date = date('Y-m-d', strtotime($invoice_date)); // format date
        if(!$user->getUserID($created_by)) {
            $error_ctr++;
            $error_msgs[] = 'Employee ID does not exist';
        }
        foreach($invoice_items['less'] as $l) {
            if(isset($l['type']) && $l['type'] == 'deposit') {
                $request = Request::create('matters/pay/'.$matter_id.'/'.$l['amount'], 'GET');
                $requestResult = Route::dispatch($request)->getContent();
                $requestResult = json_decode($requestResult);

                if($requestResult->status_id == 0) {
                    $error_ctr++;
                    $error_msg[] = $requestResult->message;
                }
            }
        }

        /* Save Client Data */
        $client_data = array(
            'CORADDRESS' => Input::get('client.address'),
            'REPNAME'    => Input::get('client.attn'),
            'EMAIL'      => Input::get('client.email'),
            'TELNUM'     => Input::get('client.telephone'),
            'FAXNUM'     => Input::get('client.fax')
        );
        $client->updateClient($client_id, $client_data);

        /* Close Matter if Final Invoice */
        if($invoice_type == 'FI') {
            $box_number = Input::get('box_number');
            HelperLibrary::closeMatterbyInvoice($matter_id, trim($box_number), 2);
        }

        /* Get timesheet details (for invoicedetail saving) */
        //$timesheet_detail = $timesheet->getDetail($invoice_items['timesheet']);
        $timesheet_detail = $invoice_items['timesheet'];
        $invoice_items['timesheet'] = array();
        foreach($timesheet_detail as $detail) {
            $detail_row = array(
                'description'   =>  $detail['Comment'],
                'amount'        =>  $detail['WorkHour'] * $detail['HourlyRate'],
                'timesheet_id'  =>  $detail['UniqueID'],
                'complimentary' =>  isset($detail['complimentary']) ? 1 : 0,
                'complimented_units' => $detail['complimented_units']
            );
            $invoice_items['timesheet'][] = $detail_row;
        }

        if($error_ctr > 0) {
            $response['status'] = 'error';
            $response['error_msgs'] = $error_msgs;
        } else {
            if($billing->invoiceExists($invoice_number)) {
                //Update head, insert items
                $invoicehead_update_data = array(
                    'CustomerID'    => $client_id,
                    'InvoiceDate'   => $invoice_date,
                    //'Remarks'       => $other_data['remarks'],
                    //'User_C'        => $created_by,
                    'matter_id'     => $matter_id,
                    'Status'        => $status,

                    'agreed_cost'   => empty($other_data['agreed_cost']) ? null : $other_data['agreed_cost'] ,
                    'total_cost'    => $other_data['total_cost'],
                    'total_disbursements' => $other_data['total_disbursements'],
                    'total_less'    => $other_data['total_less'],
                    'total_invoice' => $other_data['total_invoice'],

                    'reference_number' => $reference_number,
                    'new_invoice_number' => $new_invoice_number,

                    //'InvoiceType'   => $invoice_type,
                    'Approver1'     => $approver
                );

                if($form_type == 'new') {
                    $invoicehead_update_data['User_C'] = $created_by;
                    $invoicehead_update_data['InvoiceType'] = $invoice_type;
                }

                //Separate disbursements to save and to update
                $disbursements_update = $disbursements_save = array();
                foreach($invoice_items['disbursements'] as $k => $v) {
                    if($k < 0) {
                        $disbursements_save[$k] = $v;
                    } else {
                        $disbursements_update[$k] = $v;
                        $disbursements_update[$k]['invoice_id'] = $invoice_number;
                        $disbursements_update[$k]['billed'] = 1;
                    }
                }

                $proc1   = $billing->updateInvoiceHead($invoice_number, $invoicehead_update_data);
                
                if($form_type == 'new') {
                    $proc1_2 = $billing->saveInvoiceBatch($invoice_batch_number, $invoice_date, $created_by);
                } else { // update
                    $invoicebatch_update_data = array(
                        'InvoiceDate'   => $invoice_date,
                        'User_U'        => $created_by,
                        'Date_U'        => date('Y-m-d H:i:s')
                    );
                    $proc1_2 = $billing->updateInvoiceBatch($invoice_batch_number, $invoicebatch_update_data);
                }
                
                $proc2   = (count($invoice_items['timesheet']) > 0)     ? $billing->saveInvoiceDetail($invoice_number, $invoice_items['timesheet'])
                                                                        : true;
                if($form_type == 'new') {
                    $proc3   = (count($disbursements_save) > 0) ? $billing->saveInvoiceDisbursement($invoice_number, $disbursements_save)
                                                                        : true;
                    $proc3_1 = (count($disbursements_update) > 0) ? $billing->updateDisbursement($disbursements_update)
                                                                        : true;
                } else {
                    $proc3   = (count($invoice_items['disbursements']) > 0) ? $billing->saveInvoiceDisbursement($invoice_number, $invoice_items['disbursements'])
                                                                        : true;                    
                }
                $proc4   = (count($invoice_items['less']) > 0) ? $billing->saveInvoiceLess($invoice_number, $invoice_items['less'])
                                                                        : true;
                $cbh = (array) $invoice_items['costs_by_handlers'];
                $proc5   = (!empty($cbh)) ? $billing->saveInvoiceCostsByHandler2($invoice_number, $invoice_items['costs_by_handlers'])
                            :true;

                if($proc1_2 && $proc2 && $proc3 && $proc4 && $proc5) {
                    $response['status'] = 'success';
                    $response['data'] = 'Invoice saved successfully!';                    
                } else {
                    $response['status'] = 'error';
                    $response['error_msgs'] = array('Database error');                    
                }
            } else {
                //Insert new
                if($billing->saveInvoice($matter_id, $client_id, $invoice_date, $created_by, $other_data, $invoice_items)) {
                    $response['status'] = 'success';
                    $response['data'] = 'Invoice saved successfully!';
                } else {
                    $response['status'] = 'error';
                    $response['error_msgs'] = array('Database error');
                }
            }
        } 

        // Clear generated invoice id and batch number if success
        if($response['status'] == 'success') {
            Session::forget('invoice');
        }

        // Send email to a powerful being if invoice status is set to Approved
        if(!in_array(Auth::user()->EmployeeID, array('ADMIN', 'MASTER'))) {
            $info = $matter->getInfo($matter_id);
            $email_data['invoice_id'] = $invoice_number; //'20141108141315531';
            $email_data['handler_id'] = $info->user_id; //assign the handler_id
            $email_data['invoice_no'] = $new_invoice_number; //'2585/2014';
            $email_data['ref_no'] = $reference_number; //'SYC/33162/SJP';
            $email_data['matter_id'] = $matter_id;
            $email_data['matter_description'] = $info->Description_A;
            $email_data['matter_client'] = $info->Incharge;
            if($status == '0' && $invoice_type == "IN") {
                Helper::send_new_invoice($email_data);
            } else if($status == '0' && $invoice_type == "FI"){
                Helper::send_final_invoice($email_data);
            }
        }

        return Response::json($response);
    }

    public function postSave()
    {
        $response = array();
        $error_ctr = 0;
        $error_msgs = array();
        
        $matter = new Matter();
        $client = new Client();
        $user = new User();
        $timesheet = new Timesheet();
        $billing = new Billing();
        
        /* Get raw data */
        $form_type                      = Input::get('form_type');
        $matter_id                      = Input::get('matter_id');
        $client_id                      = Input::get('client_id');

        $reference_number               = Input::get('reference_number');
        $new_invoice_number             = Input::get('new_invoice_number');

        $invoice_number                 = Input::get('invoice_number');
        $invoice_batch_number           = Input::get('invoice_batch_number');
        $invoice_date                   = Input::get('invoice_date');
        $created_by                     = (Input::has('employee_id')) ? Input::get('employee_id') : Auth::user()->EmployeeID;
        $other_data['remarks']          = Input::get('remarks');
        $invoice_items['timesheet']     = Input::get('timesheet_list');
        $invoice_items['disbursements'] = Input::get('disbursements');

        $invoice_items['costs_by_handlers'] = Input::get('costs_by_handlers');
        $invoice_items['less']          = Input::get('less');
        $other_data['agreed_cost']      = Input::get('agreed_cost_amount');
        $other_data['total_cost']       = Input::get('totals.costs');
        $other_data['total_disbursements'] = Input::get('totals.disbursements');
        $other_data['total_less']       = Input::get('totals.less');
        $other_data['total_invoice']    = Input::get('totals.invoice');

        /* Data validation */
        if(!$matter->exists($matter_id)) {
            $error_ctr++;
            $error_msgs[] = 'Matter ID does not exist';
        }
        if(!$client->exists($client_id)) {
            $error_ctr++;
            $error_msgs[] = 'Client ID does not exist';
        }
        if(!strtotime($invoice_date)) {
            $error_ctr++;
            $error_msgs[] = 'Invalid Invoice date';            
        } else $invoice_date = date('Y-m-d', strtotime($invoice_date)); // format date
        if(!$user->getUserID($created_by)) {
            $error_ctr++;
            $error_msgs[] = 'Employee ID does not exist';
        }

        if($form_type == 'update') {
            if(!empty($invoice_items['timesheet']) && !$timesheet->belongsToMatter($invoice_items['timesheet'], $matter_id)) {
                $error_ctr++;
                $error_msgs[] = 'Invalid timesheet item(s)';                
            }
        } else { // new
            if(empty($invoice_items['timesheet']) || !$timesheet->belongsToMatter($invoice_items['timesheet'], $matter_id)) {
                $error_ctr++;
                $error_msgs[] = 'Invalid timesheet item(s)';                
            }
        }

        /*
        if(!is_array($invoice_items['timesheet']) || !$timesheet->belongsToMatter($invoice_items['timesheet'], $matter_id)) {
            $error_ctr++;
            $error_msgs[] = 'Invalid timesheet item(s)';
        }
        */

        /* Get timesheet details (for invoicedetail saving) */
        $timesheet_detail = $timesheet->getDetail($invoice_items['timesheet']);
        $invoice_items['timesheet'] = array();
        foreach($timesheet_detail as $detail) {
            $detail_row = array(
                'description'   =>  $detail->Comment,
                'amount'        =>  $detail->WorkHour * $detail->HourlyRate,
                'timesheet_id'  =>  $detail->UniqueID
            );
            $invoice_items['timesheet'][] = $detail_row;
        }

        if($error_ctr > 0) {
            $response['status'] = 'error';
            $response['error_msgs'] = $error_msgs;
        } else {
            if($billing->invoiceExists($invoice_number)) {
                //Update head, insert items
                $invoicehead_update_data = array(
                    'CustomerID'    => $client_id,
                    'InvoiceDate'   => $invoice_date,
                    'Remarks'       => $other_data['remarks'],
                    'User_C'        => $created_by,
                    'matter_id'     => $matter_id,
                    'Status'        => '0',

                    'agreed_cost'   => empty($other_data['agreed_cost']) ? null : $other_data['agreed_cost'] ,
                    'total_cost'    => $other_data['total_cost'],
                    'total_disbursements' => $other_data['total_disbursements'],
                    'total_less'    => $other_data['total_less'],
                    'total_invoice' => $other_data['total_invoice'],

                    'reference_number' => $reference_number,
                    'new_invoice_number' => $new_invoice_number
                );

                $proc1   = $billing->updateInvoiceHead($invoice_number, $invoicehead_update_data);
                
                if($form_type == 'new') {
                    $proc1_2 = $billing->saveInvoiceBatch($invoice_batch_number, $invoice_date, $created_by);
                } else { // update
                    $invoicebatch_update_data = array(
                        'InvoiceDate'   => $invoice_date,
                        'User_U'        => $created_by,
                        'Date_U'        => date('Y-m-d H:i:s')
                    );
                    $proc1_2 = $billing->updateInvoiceBatch($invoice_batch_number, $invoicebatch_update_data);
                }
                
                $proc2   = (count($invoice_items['timesheet']) > 0)     ? $billing->saveInvoiceDetail($invoice_number, $invoice_items['timesheet'])
                                                                        : true;
                $proc3   = (count($invoice_items['disbursements']) > 0) ? $billing->saveInvoiceDisbursement($invoice_number, $invoice_items['disbursements'])
                                                                        : true;
                $proc4   = (count($invoice_items['less']) > 0) ? $billing->saveInvoiceLess($invoice_number, $invoice_items['less'])
                                                                        : true;
                $cbh = (array) $invoice_items['costs_by_handlers'];
                $proc5   = (!empty($cbh)) ? $billing->saveInvoiceCostsByHandler($invoice_number, $invoice_items['costs_by_handlers'])
                            :true;

                if($proc1_2 && $proc2 && $proc3 && $proc4 && $proc5) {
                    $response['status'] = 'success';
                    $response['data'] = 'Invoice saved successfully! Returning to previous page...';                    
                } else {
                    $response['status'] = 'error';
                    $response['error_msgs'] = array('Database error');                    
                }
            } else {
                //Insert new
                if($billing->saveInvoice($matter_id, $client_id, $invoice_date, $created_by, $other_data, $invoice_items)) {
                    $response['status'] = 'success';
                    $response['data'] = 'Invoice saved successfully! Returning to previous page...';
                } else {
                    $response['status'] = 'error';
                    $response['error_msgs'] = array('Database error');
                }
            }
        } 

        // Clear generated invoice id and batch number if success
        if($response['status'] == 'success') {
            Session::forget('invoice');
        }


        return Response::json($response);
    }

    public function getPreviewDraft($invoice_number)
    {
        $raw = false;
        $handler = new Handlers;

        $invoice = Session::get('invoice.draft.'.$invoice_number);

        //Get position for each employee
        $user = new User;
        $employee_job = array();
        foreach ($invoice['timesheet']['cost_summary'] as $ndx => $item) {
            $ejobObj = $user->getEmployeeJob( array( User::getUserID( $item['EmployeeID'] ) ) );
            $employee_job[$item['EmployeeID']] = isset($ejobObj[0]->PositionDesc_A) ? $ejobObj[0]->PositionDesc_A: '';
        }

        //Get total work hours per handler
        $workHoursPerHandler = array();
        $total_complimented_units = 0;
        foreach($invoice['timesheet']['list'] as $ndx => $item) {
            if(!isset($workHoursPerHandler[$item['EmployeeID']]))
                $workHoursPerHandler[$item['EmployeeID']] = $item['WorkHour'];
            else
                $workHoursPerHandler[$item['EmployeeID']] += $item['WorkHour'];

            //Get total complimented units
            $total_complimented_units += $item['complimented_units'] > 0 ? $item['complimented_units'] : 0;
        }


        $data['parent_template'] = $raw ? 'template/print' : 'template/default';
        $data['title'] = 'Invoice No. '. $invoice['id'];
        //$data['invoicehead'] = $billing->getInvoiceHead($invoice_id);

        //Invoice Detail
        $data['date']               = $invoice['date'];
        $data['reference_number']   = $invoice['reference_number'];
        $data['new_id']             = $invoice['new_id'];


        //Get client info
        //$client = Client::find($invoice['client']['id']);
        $data['client']['id']               = $invoice['client']['id'];
        $data['client']['name']             = $invoice['client']['name'];
        $data['client']['address']          = $invoice['client']['address'];  
        $data['client']['attn']             = isset($invoice['client']['attn']) ? $invoice['client']['attn'] : null; 
        $data['client']['email']            = $invoice['client']['email'];    
        $data['client']['telephone']        = $invoice['client']['telephone'];
        $data['client']['fax']              = $invoice['client']['fax'];

        $data['matter'] = Matter::find($invoice['matter']['id']);
        $data['handler'] = $handler->getInfo($data['matter']->Incharge);
        $data['min_workdate'] = $invoice['timesheet']['min_date'];
        $data['max_workdate'] = $invoice['timesheet']['min_date'];
        $data['invoice_items'] = $invoice['timesheet']['list'];
        $data['costs'] = $invoice['timesheet']['cost_summary'];
        $data['disbursements'] = $invoice['disbursements'];
        $data['less'] = $invoice['less'];

        $data['total_complimented_units'] = $total_complimented_units;
        $data['total_units'] = $invoice['total']['cost']['units'];
        $data['total_costs'] = $invoice['total']['cost']['amount'];
        $data['agreed_cost'] = $invoice['timesheet']['agreed_cost'];
        $data['total_disbursements'] = $invoice['total']['disbursement'];
        $data['total_less'] = $invoice['total']['less'];
        $actual_cost = ( !empty($data['agreed_cost']) ) ? $data['agreed_cost'] : $data['total_costs'];
        $data['total_cost_disb'] = ($actual_cost + $data['total_disbursements']) - $data['total_less'];

        $data['employee_job'] = $employee_job;
        $data['work_hours_per_handler'] = $workHoursPerHandler;

        $data['template'] = Config::get('oln.invoice.template_1');
        
        return View::make('print/invoice_draft', $data);        
    }

    public function postSaveDraftToSession()
    {
        $data = Input::all();

        //Save to Session
        Session::put('invoice.draft.'.$data['id'], $data);

        //Save to DB

        return Response::json($data);
    }

    public function getUpdate2($invoice_number)
    {
        //$matter = new Matter();
        $billing = new Billing();
        $utils = new Utilities;
        $invoicehead = $billing->getInvoiceHead($invoice_number);
        $matter = Matter::find($invoicehead->matter_id);
        $client = Client::find($invoicehead->CustomerID);

        //Check if able to create invoice for matter
        if( ! Sessions::ableTo('save-invoice', $invoicehead->matter_id)) {
            $error_data['type'] = 'invoice';
            $error_data['matter_id'] = $invoicehead->matter_id;
            return View::make('errors/matter_in_use', $error_data);
        }        

        //Get handler info who handles the matter
        $handler_user_id = User::getUserID($matter->Incharge);
        $handler_email = isset(User::find($handler_user_id)->email_address) ? User::find($handler_user_id)->email_address : ''; 
        
        //Get invoice detail
        $total_units = 0;
        $filter['invoice_id'] = $invoice_number;
        $filter['timesheet']['join'] = true;
        $filter['timesheet']['order_by'] = 'ts.WorkDate';
        $filter['timesheet']['sort_order'] = 'asc';
        $filter['timesheet']['group_by'] = 'ts.UniqueID';
        $invoice_items = $billing->prepareQueryForGetInvoiceDetail($filter)->get();
        $processed_invoice_items = array();
        foreach($invoice_items as $ndx => $item) {
            $item->Comment = $item->Description;
            //$item->WorkHour = $item->work_hour;
            $item->WorkHour = ($item->charged_units * 5) / 60;
            $item->amount =  $item->total_amount;            

            //if($item->complimentary == 0) 
                $total_units += $item->charged_units;

            $processed_invoice_items[] = $item;
        }

        //Get min and max date from invoice items
        $inner_query = $billing->prepareQueryForGetInvoiceDetail($filter)->toSql();
        $bindings = $billing->prepareQueryForGetInvoiceDetail($filter)->getBindings();
        $query = DB::select('select min(t1.WorkDate) as min_workdate, max(t1.WorkDate) as max_workdate from ('.$inner_query.') as t1', $bindings);
        $min_workdate = $query[0]->min_workdate;
        $max_workdate = $query[0]->max_workdate;        
        
        //Get cost summary
        $total_cost_amount = 0;
        $filter['timesheet']['group_by'] = 'tsw.EmployeeID';
        $cost_summary = $billing->prepareQueryForGetInvoiceDetail($filter)->get();
        $processed_cost_summary = array();
        foreach($cost_summary as $ndx => $item) {
            $item->sum_amount = $item->total_amount;

            $total_cost_amount += $item->total_amount;

            $processed_cost_summary[] = $item;
        }

        //Get disbursements
        $disbursement_total = 0;
        $disbursements = $billing->getDisbursements($invoice_number);
        foreach($disbursements as $item)
            $disbursement_total += $item->amount;

        //Get less
        $less_total = 0;
        $less = $billing->getInvoiceLessItems($invoice_number);
        foreach($less as $item)
            $less_total += $item->amount;

        //Calculate overall
        $final_cost = (trim($invoicehead->agreed_cost) == '') ? $total_cost_amount : $invoicehead->agreed_cost;
        $overall_total = ($final_cost + $disbursement_total) - $less_total;

        //Get box number
        $box_number = null;
        if($invoicehead->InvoiceType == 'FI') {
            $box_number = $utils->getCloseInfo($invoicehead->matter_id);
            $box_number = $box_number[0]->box;
        }

        //Set data for view
        $data = array();
        $data['form_type']                  = 'update';
        $data['id']                         = $invoicehead->UniqueID;
        $data['batch_number']               = $invoicehead->BatchNumber;
        $data['reference_number']           = $invoicehead->reference_number;
        $data['new_id']                     = $invoicehead->new_invoice_number;
        $data['handler_email']              = $handler_email;
        $data['date']                       = date('F j, Y', strtotime($invoicehead->InvoiceDate));
        $data['created_by']                 = $invoicehead->User_C;
        $data['type']                       = $invoicehead->InvoiceType;
        $data['approver']                   = $invoicehead->Approver1;
        $data['status']                     = $invoicehead->Status;
        $data['box_number']                 = $box_number;
        $data['matter']['id']               = $invoicehead->matter_id;                
        $data['matter']['description']      = $matter->Description_A;
        $data['client']['id']               = $invoicehead->CustomerID;
        $data['client']['type']             = $client->CUSTTYPE;
        $data['client']['name']             = $client->CompanyName_A1;
        $data['client']['address']          = $client->CORADDRESS;
        $data['client']['attn']             = $client->REPNAME;
        $data['client']['email']            = $client->EMAIL;
        $data['client']['telephone']        = $client->TELNUM;
        $data['client']['fax']              = $client->FAXNUM;
        $data['timesheet']['min_date']      = $min_workdate;
        $data['timesheet']['max_date']      = $max_workdate;
        $data['timesheet']['list']          = $processed_invoice_items;
        $data['timesheet']['cost_summary']  = $processed_cost_summary;
        $data['timesheet']['agreed_cost']   = $invoicehead->agreed_cost;
        $data['disbursements']              = $disbursements;
        $data['less']                       = $less;
        $data['remarks']                    = null;
        $data['total']['cost']['units']     = $total_units;
        $data['total']['cost']['amount']    = $total_cost_amount;
        $data['total']['disbursement']      = $disbursement_total;
        $data['total']['less']              = $less_total;
        $data['total']['overall']           = $overall_total;

        $data['template'] = Config::get('oln.invoice.template_1');

        $user = new User;
        $data['handler_list'] = $user->getEmployeesByRoles('Handler');
        $data['hourly_rate_list'] = $user->getEmployeeJob();        

        return View::make('pages/invoice_create_v2', $data);
    }

    public function getUpdate($invoice_number)
    {
        $matter = new Matter();
        $billing = new Billing();
        $matter_id = $billing->getInvoiceHead($invoice_number)->matter_id;
        $data = array();
        
        $data['form_type']           = 'update';
        $data['invoicehead_id']      = $invoice_number;
        $data['invoicehead_batchno'] = '';
        $data['matters']             = $matter->listMatter(Auth::user()->user_id);
        
        $filter['timesheet']['join'] = true;
        $filter['timesheet']['order_by'] = 'ts.WorkDate';
        $filter['timesheet']['sort_order'] = 'asc';
        $filter['timesheet']['group_by'] = 'ts.UniqueID';
        $filter['invoice_id'] = $invoice_number;
        $billed_timesheet = $billing->prepareQueryForGetInvoiceDetail($filter)->get();

        $data['timesheet_by_handler'] = $this->_allTimesheetByHandler($matter_id);


        foreach($billed_timesheet as $item) {
            $item->billed = true;
            $data['timesheet_by_handler'][$item->EmployeeID][] = $item;
        }


        Debugbar::info($billed_timesheet);
        Debugbar::info($data['timesheet_by_handler']);

        return View::make('pages/invoice_create', $data);
    }

    public function getInvoiceHead()
    {
        $billing = new Billing();
        $result = array();
        
        $invoice_number = Input::get('invoice_number');
        $result['invoicehead'] = $billing->getInvoiceHead($invoice_number);

        $filter['invoice_id'] = $invoice_number;
        $filter['sort_by'] = 'ItemLineNo';
        $filter['order'] = 'asc';
        
        $timesheet_result = $billing->prepareQueryForGetInvoiceDetail($filter)->get();
        /*
        foreach($timesheet_result as $row) {
            $result['timesheet'][] = array(
                'item_number'   => $row->ItemLineNo,
                'description'   => $row->Description,
                'amount'        => round($row->Amount,2)
            );
        }
        */
        $result['timesheet'] = $timesheet_result;

        $cost_result = $billing->getCostByHandler($invoice_number);
        if(count($cost_result) == 0) {
            // pull costs from invoicedetail
            $filter = array();
            $filter['invoice_id'] = $invoice_number;
            $filter['remember_query'] = 5;
            $filter['timesheet']['join'] = true;
            $filter['timesheet']['order_by'] = 'ts.WorkDate';
            $filter['timesheet']['sort_order'] = 'asc';
            $filter['timesheet']['group_by'] = 'ind.ReferenceID';       
            $filter['timesheet']['group_by'] = 'tsw.EmployeeID';

            $cost_by_handler = array();
            $cbh = $billing->prepareQueryForGetInvoiceDetail($filter)->get();
            foreach($cbh as $cost) {
                $cost_by_handler[] = array(
                    'handler_id'    => $cost->EmployeeID,
                    'hourly_rate'   => $cost->HourlyRate,
                    'total_work_hours'  => $cost->work_hour,
                    'total_units'   => $cost->units,
                    'complimentary' => 0,
                    'amount'        => $cost->total_amount
                );
            }
            $result['cost_by_handler'] = $cost_by_handler;
        } else {
            // pull from db
            $result['cost_by_handler'] = $cost_result;
        }

        
        $disbursement_result = $billing->getDisbursements($invoice_number);
        /*
        foreach($disbursement_result as $row) {
            $result['disbursements'][] = array(
                'item_number'   => $row->item_number,
                'description'   => $row->description,
                'amount'        => round($row->amount,2)
            );
        }
        */              
        $result['disbursements'] = $disbursement_result;

        $less_result = $billing->getInvoiceLessItems($invoice_number);
        $result['less'] = $less_result;

        return Response::json($result);
    }

    public function getSendReminder($invoice_number)
    {
        $billing = new Billing();        

        $filter['invoice_balance'] = true;
        $filter['invoice_id'] = $invoice_number;
        $invoice_details = $billing->prepareQueryForGetInvoice($filter)->first();

        $data['type']           = 'invoice';
        $data['invoice_number'] = $invoice_number;
        $data['recipient_email']   = '';
        $data['recipient_name']    = $invoice_details->CompanyName_A1;
        $message = "Hello ".$invoice_details->CompanyName_A1.",\n\nThis email is to remind you that you still have a pending balance of $".number_format($invoice_details->invoice_balance, 2) ." for Invoice Number ".$invoice_number." issued last ".date('F j, Y', strtotime($invoice_details->InvoiceDate)).".\n\nThanks!";
        $data['message']        = $message;

        return View::make('pages/invoice_reminder', $data);
    }

    public function getDisbursements()
    {
        $billing = new Billing(); 
        $invoice_id = Input::get('invoice_id');
        $result['disbursements'] = $billing->getDisbursements($invoice_id);

        return Response::json($result);
    }

    // without disbursement
    public function getInvoiceDetailTotal()
    {
        $invoice_id = Input::get('invoice_id');
        $result['total'] = DB::table('invoicedetail')->where('UniqueID', $invoice_id)->sum('Amount');
        return Response::json($result);
    }

    public function getAllTimesheet()
    {
        $filter = array();
        $user_id = $this->principal_user;

        if(Input::get('matter_id') !== null && Input::get('matter_id') !== '') 
            $filter['matter_id'] = Input::get('matter_id');

        if(Input::get('unbilled_only')) $filter['unbilled_only'] = Input::get('unbilled_only');
        if(Input::get('billable_unbillable')) $filter['billable_unbillable'] = Input::get('billable_unbillable');

        $matter = new Matter();
        $result = $matter->prepareQueryForGetTimesheet($user_id, $filter)->get();
        return Response::json($result);
    }

    public function postLegallyCloseMatter()
    {
        $data = Input::all();

        $obj = array(
            'matter_id'     => Input::get('matter_id'),
            'box_number'    => Input::get('box_number'),
            'CloseStatus'   => Input::get('CloseStatus'),
            'current_user'  => User::getEmployeeID( Session::get('principal_user') ),
            'datetime'      => date('Y-m-d H:i:s')
        );

        HelperLibrary::LegallyCloseMatter($obj);

        return Response::json($data);
    }

    public function getTemplateinvoice1(){
        Config::set('laravel-debugbar::config.enabled', false);
        return View::make('template/invoice1');
    }

    public function postUpdateInvoiceSession()
    {
        $data = Input::has('invoice_matter_id') ? Input::get('invoice_matter_id') : null;

        HelperLibrary::storeSessionData(array('invoice_matter_id' => $data));

        return Response::json(array('status' => 'success'));
    } 

    public function postDelete()
    {
        $result = array();
        $timesheet_id = Input::get('timesheet_id');

        $billing = new Billing;
        $delete_result = $billing->deleteInvoiceDetail($timesheet_id);

        $result['status'] = 'success';
        $result['message'] = 'Timesheet entry deleted successfully. Reloading page...';

        return Response::json($result);        
    }

    public function postArchiveTimesheetEntry()
    {
        $result = array();
        $timesheet_id = Input::get('timesheet_id');

        //Prepare data to archive
        $ts_data = DB::table('timesheet')->where('UniqueID', $timesheet_id)->first();
        $tsw_data = DB::table('timesheetwork')->where('UniqueID', $timesheet_id)->first();

        //Save to archive
        $timesheet_archive = new TimesheetArchive;
        $timesheet_archive->UniqueID            = $ts_data->UniqueID;
        $timesheet_archive->JobAssignmentID     = $ts_data->JobAssignmentID;
        $timesheet_archive->WorkDate            = $ts_data->WorkDate;
        $timesheet_archive->User_C              = $ts_data->User_C;
        $timesheet_archive->Date_C              = $ts_data->Date_C;
        $timesheet_archive->User_U              = $ts_data->User_U;
        $timesheet_archive->Date_U              = $ts_data->Date_U;
        $timesheet_archive->EmployeeID          = $tsw_data->EmployeeID;
        $timesheet_archive->WorkHour            = $tsw_data->WorkHour;
        $timesheet_archive->ChargeHour          = $tsw_data->ChargeHour;
        $timesheet_archive->Completed           = $tsw_data->Completed;
        $timesheet_archive->HourlyRate          = $tsw_data->HourlyRate;
        $timesheet_archive->ChargeRate          = $tsw_data->ChargeRate;
        $timesheet_archive->Comment             = $tsw_data->Comment;
        $timesheet_archive->BillingStatus       = $tsw_data->BillingStatus;
        $timesheet_archive->BillingUpdatedBy    = $tsw_data->BillingUpdatedBy;
        $timesheet_archive->BillingUpdatedOn    = $tsw_data->BillingUpdatedOn;
        $timesheet_archive->Unbillable          = $tsw_data->Unbillable;
        $timesheet_archive->Invisible           = $tsw_data->Invisible;
        $timesheet_archive->archived_by         = Auth::user()->EmployeeID;
        $timesheet_archive->archived_date       = date('Y-m-d H:i:s');
        $timesheet_archive->save();

        //Remove timesheet record
        $timesheet = new Timesheet;
        $timesheet->delete($timesheet_id);

        //Remove invoicedetail record
        DB::table('invoicedetail')->where('ReferenceID', $timesheet_id)->limit(1)->delete();

        $result['status'] = 'success';
        $result['message'] = 'Timesheet entry deleted successfully. Reloading page...';

        return Response::json($result);
    } 

    public function postCreateNewTimesheet()
    {
        //Save to timesheet tables
        $timesheet = new Timesheet;
        $timesheet_data = array();
        $timesheet_data[] = array(
            'JobAssignmentID'   => Input::get('new_timesheet.matter_id'),
            'WorkDate'          => date('Y-m-d', strtotime(Input::get('new_timesheet.date'))),
            'TimeCode'          => 'LEGAL-LEG-GEN',
            'EmployeeID'        => Input::get('new_timesheet.handler'),
            'WorkHour'          => Input::get('new_timesheet.workhour'),
            'HourlyRate'        => HelperLibrary::getHourlyRate(Input::get('new_timesheet.handler')),
            'Comment'           => Input::get('new_timesheet.description')
        );
        $other_data['return_timesheet_ids'] = true;
        $timesheet_save_result = $timesheet->create3(Input::get('new_timesheet.matter_id'), $timesheet_data, $other_data);

        //Get saved timesheet entries and save to invoice tables
        $matter = new Matter;
        $filter['only_included_timesheets'] = $timesheet_save_result;
        $timesheet_entries = $matter->prepareQueryForGetTimesheet(3, $filter)->get();
        $invoicedetail_data = array();
        foreach($timesheet_entries as $te) {
            $invoicedetail_data[] = array(
                'description'   => $te->Comment,
                'amount'        => $te->amount,
                'timesheet_id'  => $te->UniqueID
            );
        }
        $billing = new Billing;
        $repopulate_table = false;
        $invoicedetail_save_result = $billing->saveInvoiceDetail(Input::get('new_timesheet.invoice_id'), $invoicedetail_data, $repopulate_table);

        $result = array();
        if($invoicedetail_save_result > 0) {
            $result['status'] = 'success';
            $result['message'] = 'Timesheet added to Invoice successfully. Reloading page...';
        } else {
            $result['status'] = 'error';
            $result['message'] = 'Database error. Please try again.';
        }

        return Response::json($result);        
    }  

}
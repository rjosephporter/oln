<?php

class CommcenterController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function getIndex($page=''){
        switch($page):
            case 'todolist':
                return $this->getTodoList();
                break;
            case 'dropbox':
                return $this->getDropbox();
                break;
            case 'calendar':
                return $this->getCalendar();
                break;
            case 'meetings':
                return $this->getMeetings();
                break;
            default:
                return $this->getTodoList();
                break;
        endswitch;
    }

    public function getTodoList(){
        $userdata = array(
            'tasks' => array(
                'pending' => array(),
                'finished' => array()
            ),
            'requests' => array(
                'pending' => array(),
                'finished' => array()
            )
        );
        $users = array();
        $types = Config::get('commcenter.task_types');
        $user = Auth::user();
        $commcenter = new Commcenter();
        $tasks = new Tasks();
        $temptasks = $tasks->getmytasks($user->user_id);
        $temprequests = $tasks->getmyrequestsnew($user->user_id);
        foreach($temptasks as $temptask){
            if($temptask->status == '0'){
                $userdata['tasks']['pending'][$temptask->id] = $temptask;
            } elseif($temptask->status == '1'){
                $userdata['tasks']['finished'][$temptask->id] = $temptask;
            }
        }
        foreach($temprequests as $temprequest){
            if($temprequest->status == '0'){
                $userdata['requests']['pending'][$temprequest->id] = $temprequest;
            } elseif($temprequest->status == '1'){
                $userdata['requests']['finished'][$temprequest->id] = $temprequest;
            }
        }
        $data = $commcenter->getallusers();
        foreach($data as $temp):
            $users[$temp->user_id] = $temp;
        endforeach;

        return View::make('pages/todolist',array(
                'users' => $users,
                'types' => $types,
                'userdata' => $userdata,
                'thisuser' => $user
            )
        );
    }

    public function postNewowntask()
    {
        $post = $_POST;
        $user = Auth::user();
        $post['requestedby'] = $user->user_id;
        $post['owner'] = $user->user_id;
        $post['message'] = json_encode(array('subject'=>$post['subject'],'message'=>$post['message']));
        $tasks = new Tasks();
        $tasks->createowntask($post);
        $message=array
        (
            'message' => 'New own task was created',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function postNewtaskrequest()
    {
        $post = $_POST;
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999').'/email/email-task?&action=new');
        $new_message_body = str_replace('<message_container>',$post['message'],$message_body);
        $post['requestedby'] = $user->user_id;
        $post['message'] = json_encode(array('subject'=>$post['subject'],'message'=>$post['message']));
        $post['email_subject'] = $user_info->NickName .' has sent a new task for you - '.$post['subject'];
        $post['email_message'] = $new_message_body;
        $post['email_to_requester'] = $user_info->email_address;
        $post['email_from'] = $user_info->email_address;
        $post['email_from_name'] = $user_info->NickName;
        $task_ids = Helper::create_task_by_request($post);
        $message=array
        (
            'message' => 'New request was created',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }
    
    public function postNewtaskmessage()
    {
        $post = $_POST;
        Helper::send_message_for_request($post);
        $message=array
        (
            'message' => 'Task message recorded',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function postNewtaskreply()
    {
        $post = $_POST;
        Helper::reply_to_task($post);
        $message=array
        (
            'message' => 'Task reply recorded',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }
    

    public function getAccepttask()
        {
            $hash = $_GET['tkid'];
            $tasks = new Tasks();
            $task = $tasks->gettaskbyhash($hash);
            foreach($task as $temp):
               $task_data = $temp;
            endforeach;
            $original_message = json_decode($task_data->message,true);
            $post['taskid'] = $task_data->id;
            $post['subject'] = $original_message['subject'];
            $post['message'] = 'I accept your task and it will be finished as soon as possible.';
            Helper::reply_to_task($post);
            $message=array
            (
                'message' => 'Task was accepted',
                'message_type' => 'success'
            );
            return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
        }

    public function postDelegatetask()
    {
        $post = $_POST;
        $tasks = new Tasks();
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $originaltask = $tasks->gettask($post['taskid']);
        foreach($originaltask as $temp):
            $data['minuteid'] = $temp->minuteid;
            $data['type'] = $temp->type;
            $data['requestedby'] = $temp->owner;
            $data['meetingid'] = $temp->meeting_id;
            $data['message'] = $temp->message;
            $data['requiredbydate'] = $temp->requiredbydate;
            $data['owners'] = array($post['owner']);
            $data['sharees'] = explode('|',$temp->responsesharedwith);
        endforeach;
        $data['email_subject'] = $temp->NickName.' have delegated a task to you.';
        $data['email_from'] = $user_info->email_address;
        $data['email_from_name'] = 'OEMS';
        $data['email_to_requester'] = $user_info->email_address;
        $original_message = json_decode($temp->message,true);
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999').'/email/email-task?action=new');
        $new_message_body = str_replace('<message_container>',$original_message['message'],$message_body);
        $data['email_message'] = $new_message_body;
        $task_ids = Helper::create_task_by_request($data);
        foreach($task_ids as $task_id):
            $tasks->bindoriginalnewtask($task_id,array($temp->requestedby,$temp->owner));
        endforeach;
        $message = array
                (
                    'message' => 'Task was delegated',
                    'message_type' => 'success'
                );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function postDeclinetask()
    {
        $post = $_POST;
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $post['declined'] = '1';
        $subject = 'Task declined by '.$user_info->NickName;
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-task?taskid=".$post['taskid'].'&action=refused');
        $new_message_body = str_replace('<final_message_container>','Declined by '.$user_info->NickName.'<br>'.$post['message'],$message_body);
        $post['email_subject'] = $subject;
        $post['email_message'] = $new_message_body;
        Helper::finish_task($post);
        $message=array
        (
            'message' => 'Task was declined',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function getForcefinishtask()
    {
        $hash = $_GET['tkid'];
        $task_data = array();
        $tasks = new Tasks();
        $temp = $tasks->gettaskbyhash($hash);
        $finish_date = date('r');
        foreach($temp as $task):
           $post['taskid'] = $task->id;
           $original_message = json_decode($task->message,TRUE);
        endforeach;
        $post['taskid'] = $task->id;
        $post['subject'] = $original_message['subject'];
        $post['message'] = '<p>I saw and noted your message/task on '.$finish_date.'.<br> Thank you.</p>';
        $subject = 'Task noted by '.$task->NickName;
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-task?taskid=".$post['taskid'].'&action=forcefinished');
        $new_message_body = str_replace('<final_message_container>','Final reply from '.$task->NickName.'<br>'.$post['message'],$message_body);
        $post['email_subject'] = $subject;
        $post['email_message'] = $new_message_body;
        Helper::finish_task($post);
        $message=array
        (
            'message' => 'Task was finished',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function postNewtaskresponse()
    {
        $post = $_POST;
        $user = Auth::user();
        $user_info = Helper::get_user_info($user->user_id);
        $subject = 'Task finished by '.$user_info->NickName;
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-task?taskid=".$post['taskid'].'&action=finished');
        $new_message_body = str_replace('<final_message_container>','Final reply from '.$user_info->NickName.'<br>'.$post['message'],$message_body);
        $post['email_subject'] = $subject;
        $post['email_message'] = $new_message_body;
        Helper::finish_task($post);
        $message=array
        (
            'message' => 'Task finished',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    public function getApproverequest()
    {
        $hash = $_GET['tkid'];
        $task_data = array();
        $tasks = new Tasks();
        $temp = $tasks->gettaskbyhash($hash);
        $approval_date = date('r');
        foreach($temp as $task):
           $post['taskid'] = $task->id;
           $original_message = json_decode($task->message,TRUE);
        endforeach;
        $post['taskid'] = $task->id;
        $post['subject'] = $original_message['subject'];
        $post['message'] = '<p>Request Approved ('.$approval_date.')</p>';
        $subject = 'Task noted by '.$task->NickName;
        $message_body = $this->getFileContents((App::environment('alpha') ? URL::to('/') : 'http://118.142.36.38:9999')."/email/email-task?taskid=".$post['taskid'].'&action=approved');
        $new_message_body = str_replace('<final_message_container>','Final reply from '.$task->NickName.'<br>'.$post['message'],$message_body);
        $post['email_subject'] = $subject;
        $post['email_message'] = $new_message_body;
        Helper::finish_task($post);
        $message=array
        (
            'message' => 'Request was approved',
            'message_type' => 'success'
        );
        return Redirect::to('commcenter/todo-list')->with('growl_message', $message);
    }

    function any_uploaded($name) {
      foreach ($_FILES[$name]['error'] as $ferror) {
        if ($ferror != UPLOAD_ERR_NO_FILE) {
          return true;
        }
      }
      return false;
    }


//    public function getSendfinalinvoice()
//    {
////        $data['invoice_id'] = '20141108141315531';
////        // $data['introducer_id'] = '81'; //SYC
////        $data['handler_id'] = '78'; //SJP
////        $data['invoice_no'] = '2585/2014';
////        $data['ref_no'] = 'SYC/33162/SJP';
////        Helper::send_final_invoice($data);
//    }

    public function getSendnewtimesheetentries()
    {
        Helper::send_latest_timesheet_entries();
    }
    
    public function getFilegettest()
    {
        file_get_contents_curl('http://192.168.1.250:9999/invoice-print/20141205165704621?raw=2');
    }        

//    public function getDelegatematter()
//    {
//        $data = array();
//        $data['delegated_to'] = '5';
//        $data['matter_no'] = '33297';
//        $data['matter_description'] = 'GMO Application';
//        $data['matter_client'] = '04225-Peter Righton';
//        Helper::delegate_matter($data);
//    }
    function file_get_contents_curl($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
    
    function getFileContents($url)
    {
        $request = Request::create($url, 'GET');
        $originalInput = Request::input();
        Request::replace($request->input());
        $response = Route::dispatch($request);
        Request::replace($originalInput);
        return $response->getContent();   
    }
}


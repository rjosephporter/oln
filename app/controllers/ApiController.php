<?php

class ApiController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| All API Functions
	|--------------------------------------------------------------------------
	|
	*/

	public function postPost($resource)
	{
		$response = array();

		switch ($resource) {
			case 'auth_user':
				$data = Input::only('EmployeeID', 'password');
				if(empty($data['EmployeeID']) || empty($data['password'])) {
					$response['status'] = 'error';
					$response['error_msgs'][] = 'Missing username and/or password';
				} else {
					if (Auth::attempt($data)) {
					    $response['status'] = 'success';
					    $response['response']['user_details'] = Auth::user();
					} else {
						$response['status'] = 'error';
					    $response['error_msgs'][] = 'Invalid credentials';
					}
				}
				break;

			case 'ADD_MORE_RESOURCES_HERE':
				
				break;

			default:
				# code...
				break;
		}

		return Response::json($response);
	}

	public function getGet($resource)
	{
		$response = array();

		switch ($resource) {

			/**
			 * Get list of matters and other details
			 *
			 */
			case 'matter-list':
		    	$user_id = Input::get('user_id');
		    	$page = Input::get('page', 1);
		    	$folder_types = Input::get('folder_types','green');
		        $sortby = Input::get('sortby', 'total_unbilled_amount');
		        $order = Input::get('order', 'desc');
		        $year = Input::get('year', '0');

		    	$matter = new Matter();
				
		        $filter['folder_types'] = explode(',', $folder_types);
		        $filter['sortby'] = $sortby;
		        $filter['order'] = $order;
		        if($year !== '0') $filter['workdate_year'] = $year;

		        $filter['show_select_basic'] = true;
		        $filter['show_select_mattertype'] = true;
		        $filter['show_select_billdetails'] = true;

				$matter = new Matter();

				$matter_list = $matter->getList($user_id, $page, $filter);

				$response['status'] = 'success';
				$response['response'] = $matter_list;
				break;

			/**
			 * Get list of clients and other details
			 *
			 */
			case 'client-list':
		        $user_id = Input::get('user_id');
		        $page = Input::get('page', 1);
		        $sortby = Input::get('sortby', 'total_unbilled_amount');
		        $order = Input::get('order', 'desc');
		        $year = Input::get('year');

		        $client = new Client();

		        $filter['sortby'] = $sortby;
		        $filter['order'] = $order;
		        if(!empty($year)) $filter['workdate_year'] = $year;

		        $client_list = $client->getList($user_id, $page, $filter);			
				
		        $response['status'] = 'success';
				$response['response'] = $client_list;
				break;

			case 'clientlist':
				$c = new Client;
        		$list=$c->getClientsList();
        	
				$response['status'] = 'success';
				$response['response'] = $c->getClientsList();

        		break;

			/**
			 * Billing overview per user
			 *
			 */
			case 'total-billing':
				$user_id = Input::get('user_id');
				$year = Input::get('year');

				$filter = array();
				if(!empty($year)) $filter['workdate_year'] = $year;

				$billing = new Billing();
				$start_year = (!empty($year)) ? $year : '2011';
				$end_year = (!empty($year)) ? $year + 1 : date('Y') + 1;

				$billing_detail = $billing->getTotalUnbilledAmount($user_id, $filter);
				$billing_detail->total_balance = $billing->getTotalAccountsReceivable($user_id, $filter)->total_balance;
				$billing_detail->date_from = "July " . $start_year;
				$billing_detail->date_to = "June " . $end_year;
 

				$response['status'] = 'success';
				$response['response'] = $billing_detail;
				break;
			
			case 'total-matters':
				$user = new User();
				$user_id = Input::get('user_id');
				$response['status'] = 'success';
				$response['response'] = $user->getMattersCount($user_id);				
				break;

			case 'total-clients':
				$user = new User();
				$user_id = Input::get('user_id');

				//$current_user = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id; 
        		$query = DB::table('customer as cu')
					->leftJoin('user AS usr', 'usr.EmployeeID', '=', 'cu.Incharge');
        		if(!User::hasRoleById($user_id, 'Admin'))
        			$query->where('usr.user_id', '=', $user_id);
        		
        		
        		$result=$query->count();

				$response['status'] = 'success';
				$response['response'] = $result;							
				break;

			case 'matter-dlist':
				$request = Request::create('matters/mattersdetailslisting', 'GET');
				$matter_list = Route::dispatch($request)->getContent();
				$matter_list = json_decode($matter_list);
                                
				$response['status'] = 'success';
				$response['response'] = $matter_list;
				break;

			case 'matter':
				$matter_id = Input::get('matter_id');
				$matter = Matter::find($matter_id);

    			$response['status'] = 'success';
    			$response['response'] = $matter;
				break;

			case 'matter-timeline':
				$matter_id = Input::get('matter_id');
				$job_assignment = new JobAssignment();
				$timeline = $job_assignment->getTL($matter_id);
				//$timeline = $timeline[0];

				$response['status'] = 'success';
				$response['response'] = $timeline;
				break;

			case 'matter-billing-history':
				$matter_id = Input::get('matter_id');
				$job_assignment = new JobAssignment();
				$billing_history = $job_assignment->getBH($matter_id);
				//$billing_history = $billing_history[0];

				$response['status'] = 'success';
				$response['response'] = $billing_history;
				break;

			case 'handler-list':
				/*$handler = new Handlers();
				$handler_list = $handler->getE();

				$response['status'] = 'success';
				$response['response'] = $handler_list;*/

				break;

			case 'handler-matters':
				$handler_id = Input::get('handler_id');
				$handler = new Handlers();
				$handler_matters_list = $handler->getEM($handler_id);

				$response['status'] = 'success';
				$response['response'] = $handler_matters_list;
				break;

			case 'matter-summary':
				//$matter_summary = $this->_matter_summary();
				$request = Request::create('matters/msummary', 'GET');
				$matter_summary = Route::dispatch($request)->getContent();
				$matter_summary = json_decode($matter_summary);

				$response['status'] = 'success';
				$response['response'] = $matter_summary;
				break;
                            
             case 'matter-slist':
				$request = Request::create('matters/msummarylist', 'GET');
				$matter_list = Route::dispatch($request)->getContent();
				$matter_list = json_decode($matter_list);
                                
				$response['status'] = 'success';
				$response['response'] = $matter_list;
				break;
                            
                        case 'handlers-mlist':
                            $request = Request::create('handlers/hlist', 'GET');
                            $h_list = Route::dispatch($request)->getContent();
                            $h_list = json_decode($h_list);

                            $response['status'] = 'success';
                            $response['response'] = $h_list;
                            break;

             case 'userlist':
                            $users_raw = DB::table('user as usr')
                            ->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'usr.EmployeeID')
                            ->select('usr.EmployeeID', 'emp.NickName')
                            ->get();
					        $users = array();
					        foreach($users_raw as $ndx => $item) {
					            $users[$item->EmployeeID] = $item->EmployeeID . ' - ' .$item->NickName;
					        }

                            $response['status'] = 'success';
                            $response['response'] = $users;
                            break;

             case 'matter-delegatedby':
                $handlerid = Input::get('handlerid');
                $deltype = Input::get('deltype');
				$request = Request::create('matters/mattersdelegated/'.$handlerid.'/'.$deltype, 'GET');
				$matter_list = Route::dispatch($request)->getContent();
				$matter_list = json_decode($matter_list);
                                
				$response['status'] = 'success';
				$response['response'] = $matter_list;
				break;

			 case 'reports-handlerlist':
				$filter = array();
		        $user_id = Input::get('user_id');

		        $unbilled_amount_range = Input::get('unbilled_amount_range', false);
		        switch ($unbilled_amount_range) {
		            case 1:
		                $filter['total_unbilled_amount_range']['to'] = 29999.99;
		                break;

		            case 2:
		                $filter['total_unbilled_amount_range']['from'] = 30000;
		                break;
		            
		            default:
		                
		                break;
		        }
		        //$filter['remember_query'] = $this->remember_query_duration;

		        $handler = new Handlers();
		        $result=$handler->prepareQueryForGetList($user_id, $filter)->get();

		        $response['status']='success';
		        $response['response']=$result;

			 	break;

			 	case 'reports-matterlist'://userid is number
				$filter = array();
		        $user_id = Input::get('user_id');
		        $handler_id = Input::get('handler_id', false);
		        $folder_types =  Input::get('folder_types', false);
		        $unbilled_amount_range = Input::get('unbilled_amount_range', false);

		        if($handler_id !== false && $handler_id !== '0') $filter['handler_id'] = $handler_id;
		        if($folder_types !== false && $folder_types !== '0') $filter['folder_types'] = explode(',',$folder_types);
		        switch ($unbilled_amount_range) {
		            case '1':
		                $filter['total_unbilled_amount_range']['to'] = 29999.99;
		                break;

		            case '2':
		                $filter['total_unbilled_amount_range']['from'] = 30000;
		                break;
		            
		            default:
		                # code...
		                break;
		        }
		       // $filter['remember_query'] = $this->remember_query_duration;

		        $sSearch = Input::get('sSearch'); 
		        if(!empty($sSearch)) $filter['dt-search'] = $sSearch;
		        //$grand_totals =  $this->_getGrandTotals($user_id, $filter);
		        unset($filter['dt-search']);

		        $filter['show_select_basic'] = true;
		        $filter['show_select_mattertype'] = true;
		        $filter['show_select_billdetails'] = true;
		        $filter['show_select_invoicedetails'] = true;

		    	$matter = new Matter();

		        $result=$matter->prepareQueryForGetList($user_id, $filter)->get();

		        $response['status']='success';
		        $response['response']=$result;

			 	break;

			 	case 'report-clientlist':
			 		 $filter = array();
		       		 $user_id = Input::get('user_id');
			 	 	$unbilled_amount_range = Input::get('unbilled_amount_range', false);
			        switch ($unbilled_amount_range) {
			            case '1':
			                $filter['total_unbilled_amount_range']['to'] = 29999.99;
			                break;

			            case '2':
			                $filter['total_unbilled_amount_range']['from'] = 30000;
			                break;
			            
			            default:
			                # code...
			                break;
			        }
			        //$filter['remember_query'] = $this->remember_query_duration;

			        $client = new Client();
			        $result=$client->prepareQueryForGetList($user_id, $filter)->get();

			        $response['status']='success';
		        	$response['response']=$result;
			 	break;

			default:
				# code...
				break;
		}

		return Response::json($response);
	}
        
        public function getDecline($matterid, $handler){
            Utils::declineMatter(array('id' => $matterid, 'delegatedTo' => $handler));
            print_r(Request::header());
        }

}
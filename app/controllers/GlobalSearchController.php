<?php

class GlobalSearchController extends BaseController {

	public function getIndex()
	{
		$keyword = Input::get('search-keyword');
		$results = array();

		$results['matter'] = DB::table('jobassignment')
									->select('JobAssignmentID', 'Description_A')
									->where('JobAssignmentID', 'like', '%'.$keyword.'%')
									->orWhere('Description_A', 'like', '%'.$keyword.'%')
									->get();
		$results['client'] = DB::table('customer')
									->select('CustomerID', 'CompanyName_A1')
									->where('CustomerID', 'like', '%'.$keyword.'%')
									->orWhere('CompanyName_A1', 'like', '%'.$keyword.'%')
									->get();
		$results['employee'] = DB::table('employee as emp')
									->leftJoin('user as usr', 'usr.EmployeeID', '=', 'emp.EmployeeID')
									->select('emp.EmployeeID', 'usr.first_name', 'usr.last_name', 'emp.NickName')
									->where('emp.EmployeeID', 'like', '%'.$keyword.'%')
									->orWhere('usr.first_name', 'like', '%'.$keyword.'%')
									->orWhere('usr.last_name', 'like', '%'.$keyword.'%')
									->orWhere('emp.NickName', 'like', '%'.$keyword.'%')
									->get();

		$data['results'] = $results;

		Debugbar::info($results);

		return View::make('pages/global_search', $data);
	}

}
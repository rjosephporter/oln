<?php

class HandlersController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function getIndex(){
        return View::make('pages/handlers');
    }
    
    public function getNames($fname = "", $lname = ""){
        return View::make('pages/handlers_name',array('fname'=>$fname, 'lname' => $lname));
    }
	
    public function getH(){
        $user = new User;

        return Response::json($user->getEmployeesByRoles('Handler'));
    }

    public function getEM($i){
        $h = new Handlers;
        echo json_encode($h->getEM($i));
    }
    
    public function getEmplmatt($e,$type){
        $h = new Handlers;
        echo json_encode($h->getEmployeematters($e,$type));
    }
    
    public function getMatters(){
        $list_obj = Input::get();
        $obj['handler'] = isset($list_obj['handler']) ? $list_obj['handler'] : "0";
        //if(in_array(Auth::user()->user_id, Config::get('oln.admin')))
        if(Auth::user()->hasRole('Admin'))
            $user_active = "all";
        else
            $user_active = Auth::user()->EmployeeID;
        $obj['view']="restricted";
        $obj['user']=$user_active;
        if($user_active == "all" || $user_active == $obj['handler']){        
            $obj['type'] = isset($list_obj['type']) ? $list_obj['type'] : "all";
            $obj['billing'] = isset($list_obj['billing']) ? $list_obj['billing'] : 6;
            $obj['matters'] = array_values(array_filter($this->_get_up_matters(),function($info) use ($obj){
                    return $obj['type'] == "all" ? $info->Incharge == $obj['handler'] : $info->Incharge == $obj['handler'] && $info->matter_type == $obj['type'];
                })
            );

            $obj['view']="granted";
        }
        return View::make('pages/handler_modal',array('obj'=>$obj));
    }
    
    public function getHlist(){
        $h = new Handlers;
        $handlers = $h->getE();
        $handlers = array_values(array_filter($handlers,function($info){
                return strlen($info->DepartmentCode) > 0;
            })
        );
     
        $matters = $this->_get_up_matters();
      
        foreach($handlers as $handler):
            $handler->green = 0;
            $handler->orange = 0;
            $handler->red = 0;
            $handler->gray = 0;
            $handler->blue = 0;
            $handler->billable = 0;
            $handler->billed = 0;
            $handler->unbilled = 0;
            $h_matters = array_filter($matters,function($info) use ($handler){
                return $info->Incharge == $handler->EmployeeID;
            });
            foreach ($h_matters as $h_matter):
                $handler->billable += $h_matter->billable_amount;
                $handler->billed += $h_matter->billed_amount;
                switch($h_matter->matter_type):
                    case "green": $handler->green++; break;
                    case "orange": $handler->orange++; break;
                    case "red": $handler->red++; break;
                    case "gray": $handler->gray++; break;
                    case "blue": $handler->blue++; break;
                endswitch;
            endforeach;
            $handler->unbilled = $handler->billable - $handler->billed;
        endforeach;
        usort($handlers,function($a,$b){
            return $b->unbilled > $a->unbilled;
        });
        $handlers = array_values(array_filter($handlers,function($info){
                return $info->billable > 0;
            })
        );
        return Response::json($handlers);
    }
    
    public function getCurrentuser(){
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $user_role = '';
        
        if(User::hasRoleById($id_active, 'Admin'))
            $user_role = "admin";
         
        return Response::json(array('user'=>User::getEmployeeID($id_active),'id'=>$id_active, 'role' => $user_role, 'acc' => User::hasRoleById($id_active, 'Accounting')));
    }
    
    public function getHandlersdetailslisting(){
        $u = new Utilities;
        $data = $u->retrieveDefaults('handlers');
        
        return Response::json(json_decode(bzdecompress($data[0]->data)));
    }
    
    private function _get_up_matters(){
        $u = new Utilities;
        $data = $u->retrieveDefaults('matters');
        
        return json_decode(bzdecompress($data[0]->data));
    }
}

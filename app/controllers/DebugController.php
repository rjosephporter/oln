<?php

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class DebugController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Debugging tools (testing fragments of codes, etc.)
	|--------------------------------------------------------------------------
	|
	*/

	/* Table list and status */
	private $table_status;

	/* Unique Key by table */
	private $unique_key;

	/* Monolog */
	private $monolog;

	public function __construct()
    {
    	// Set tables' status
    	$this->table_status = $this->_getSourceTableStatus();

    	// Set tables' unique key
		foreach($this->table_status as $detail)
			$this->unique_key[$detail->source_table] = $detail->unique_column;

		// Initialize file logger
    	$this->monolog = Log::getMonolog();
    	$handler = new RotatingFileHandler(storage_path().'/logs/sync.log',0,Logger::INFO);
		$this->monolog->pushHandler($handler);

		set_time_limit(120);
		DB::disableQueryLog();
    }

    public function getForceUpdateTableStatus()
    {
    	return Response::json($this->_updateSourceTableStatusForce());
    }

    public function getSyncDeletedRecords()
    {
    	$table = Input::get('table');

    	$result = $this->_deleteRecords($table);

    	return Response::json($result);
    }

	public function getIndex()
	{
		$result = array();
		$table_status = $this->table_status;
		foreach($table_status as $data) {
			/* Get new records from source */
			$new_records = DB::connection('sqlsrv')
							->table($data->source_table)
							->orderBy($data->key_column, 'asc');
			if($data->key_column == 'Date_C')
				$new_records->whereRaw('CONVERT(varchar(23), '.$data->key_column.', 20) > \''.$data->last_insert_key.'\'');
			else				
				$new_records->where($data->key_column, '>', $data->last_insert_key);

			$new_records = $new_records->get();

			$result[$data->source_table]['new'] = $new_records;
			$last_new_record = end($new_records);
			$result[$data->source_table]['last_insert_key'] = null;
			if($last_new_record) {
				$result[$data->source_table]['last_insert_key'] = ($data->key_column !== 'Date_C') ? $last_new_record->{$data->key_column} : date('Y-m-d H:i:s', strtotime($last_new_record->{$data->key_column}));
			}
			/* /Get new records from source */

			/* Get UPDATED records from source */
			$updated_records = array();
			if(!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) { // skip these tables, will depend on TimeSheet. NOTE: TimeSheet must appear first before TimeSheetWork and InvoiceDetail in sourcetable_status in order for this to work
				$updated_records = DB::connection('sqlsrv')
									->table($data->source_table)
									->whereRaw('CONVERT(varchar(23), Date_U, 20) > \''.$data->last_update_date.'\'')
									->orderBy('Date_U', 'asc')
									->get();
			} elseif(count($result['TimeSheet']['updated']) > 0) { // get updated TimeSheet records to also update TimeSheetWork and InvoiceDetail tables
				$join_key = array(
					'TimeSheetWork' => 'UniqueID',
					'InvoiceDetail'	=> 'ReferenceID'
				);

				$updated_timesheet_ids = array();
				foreach($result['TimeSheet']['updated'] as $updated_timesheet)
					$updated_timesheet_ids[] = $updated_timesheet->UniqueID;

				$updated_records = DB::connection('sqlsrv')
									->table($data->source_table)
									->select($data->source_table.'.*')
									->leftJoin('TimeSheet', 'TimeSheet.UniqueID', '=', $data->source_table.'.'.$join_key[$data->source_table])
									->whereIn('TimeSheet.UniqueID', $updated_timesheet_ids)
									->get();
			}

			$result[$data->source_table]['updated'] = $updated_records;
			$last_updated_record = end($updated_records);
			$result[$data->source_table]['last_update_date'] = null;
			if($last_updated_record) {
				$result[$data->source_table]['last_update_date'] = (!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) 
																	? date('Y-m-d H:i:s', strtotime($last_updated_record->Date_U))
																	: $result['TimeSheet']['last_update_date'];
			}
			/* /Get UPDATED records from source */
		}

		$inserted_records = array();
		$updated_records = array();
		$final_result = array();
		foreach($result as $table_name => &$table_data) {
			/* Insert new records to target (MYSQL) */
			$inserted_records[$table_name] = array();
			if(count($table_data['new']) > 0) {
				$inserted_records[$table_name] = $this->_insertNewRecords($table_name, $table_data['new']);
			}
			$table_data['inserted_records'] = $inserted_records[$table_name];
			/* /Insert new records to target (MYSQL) */

			/* Update updated records to target (MYSQL) */
			$updated_records[$table_name] = array();
			if(count($table_data['updated']) > 0) {
				$updated_records[$table_name] = $this->_updateRecords($table_name, $table_data['updated']);
			}
			$table_data['updated_records'] = $updated_records[$table_name];
			/* /Update updated records to target (MYSQL) */

			/* Update table status reference */	
			if(!is_null($table_data['last_insert_key']))
				$this->_updateSourceTableStatus($table_name,'last_insert_key',$table_data['last_insert_key']);
			if(!is_null($table_data['last_update_date']))
				$this->_updateSourceTableStatus($table_name,'last_update_date',$table_data['last_update_date']);
			/* /Update table status reference */

			/* Get counts for final result */
			foreach($table_data as $data_key => $data_value)
				$final_result[$table_name][$data_key] = (!in_array($data_key, array('last_insert_key','last_update_date'))) ? count($table_data[$data_key]) : $table_data[$data_key];
			/* /Get counts for final result */
		}

		$this->_log('Sync Summary', $final_result);

		//return Response::make(var_dump($result));
		//return Response::json($result);
		return Response::json($final_result);
	}

	/**
	 * Get list of table names from source
	 *
	 */
	private function _getSourceTables()
	{
		$tables = array();
		$query = DB::connection('sqlsrv')
				->select(DB::raw('SELECT Distinct TABLE_NAME FROM information_schema.TABLES'));

		foreach($query as $table)
			$tables = $table->TABLE_NAME;

		return $tables;
	}

	/**
	 * Get source table status (last update date, last record inserted, etc.)
	 *
	 * NOTE: data is taken from MYSQL server, not the source
	 */
	private function _getSourceTableStatus()
	{
		$result = DB::table('sourcetable_status')
					->get();
		return $result;
	}

	/**
	 * Update source status by table
	 *
	 */
	private function _updateSourceTableStatus($table, $column, $data)
	{
		$temp_data = null;
		if($column == 'last_insert_key') {
			$temp_data = (!strtotime($data))
						? $data
						: date('Y-m-d H:i:s', strtotime($data));
		}
		if($column == 'last_update_date') {
			$temp_data = (!in_array($table, array('TimeSheetWork','InvoiceDetail'))) 
						? date('Y-m-d H:i:s', strtotime($data))
						: DB::table('sourcetable_status')->where('source_table', '=', 'TimeSheet')->pluck('last_update_date');
		}
		$final_data = array(
			$column => $temp_data
		);
		$result = DB::table('sourcetable_status')
						->where('source_table', '=', $table)
						->take(1)
						->update($final_data);
		return $result;
	}

	/**
	 * Force update source tables status
	 *
	 */
	private function _updateSourceTableStatusForce()
	{
		$result = array();
		$update_data = array();
		$table_status = $this->table_status;
		foreach($table_status as $data) {

			$update_data[$data->source_table] = array(
				'last_insert_key'	=> ($data->key_column !== 'Date_C')
										? DB::connection('sqlsrv')->table($data->source_table)->max($data->key_column)
										: date('Y-m-d H:i:s', strtotime(DB::connection('sqlsrv')->table($data->source_table)->max($data->key_column))),
				'last_update_date'	=> (!in_array($data->source_table, array('TimeSheetWork','InvoiceDetail'))) 
										? date('Y-m-d H:i:s', strtotime(DB::connection('sqlsrv')->table($data->source_table)->max('Date_U')))
										: $update_data['TimeSheet']['last_update_date'] 
			);
			$result = DB::table('sourcetable_status')
							->where('source_table', '=', $data->source_table)
							->update($update_data[$data->source_table]);
		}

		return $result;
	}

	/**
	 * Insert new records to target tables (MYSQL)
	 *
	 */
	private function _insertNewRecords($table, $records)
	{
		$final_records = array();

		$chunked_records = array_chunk($records, 500);
		foreach($chunked_records as $main_records) {
			$batch_records = array();
			foreach($main_records as $record) {
				$row = array();
				foreach($record as $column => $data) {
					$data = (is_string($data)) ? trim($data) : $data;
					$data = (strtotime($data)) ? date('Y-m-d H:i:s', strtotime($data)) : $data;
					$row[$column] = $data;
				}
				$final_records[] = $row;
				$batch_records[] = $row;
			}
			DB::table(strtolower($table))->insert($batch_records);
		}

		//DB::table(strtolower($table))->insert($final_records);

		return $final_records;
	}

	/**
	 * Update updated records from source to target tables (MYSQL)
	 *
	 */
	private function _updateRecords($table, $records)
	{
		$final_records = array();

		foreach($records as $record) {
			$row = array();
			foreach($record as $column => $data) {
				$data = (is_string($data)) ? trim($data) : $data;
				$data = (strtotime($data)) ? date('Y-m-d H:i:s', strtotime($data)) : $data;
				$row[$column] = $data;
			}
			/*
			$special_key = array(
				'JobAssignment' => 'JobAssignmentID',
				'Customer' => 'CustomerID',
				'Employee' => 'EmployeeID'
			);
			*/
			//$update_key = (!in_array($table, array_keys($special_key))) ? 'UniqueID' : $special_key[$table];
			$update_key = $this->unique_key[$table];

			$update = DB::table(strtolower($table))->where($update_key, '=', $row[$update_key])->take(1);
			
			/* Exclude primary columns from update */
			unset($row[$update_key]);
			if($table == 'InvoiceDetail') unset($row['ItemLineNo']);
			
			$update->update($row);

			$final_records[] = $row;
		}

		return $final_records;
	}

	/**
	 * Delete records from target (MYSQL) that no longer exist in source (MSSQL)
	 *
	 * NOTE: Must be used after tables are synced (insert & update)
	 */
	private function _deleteRecords($table)
	{
		// List unique values from source table
		$source_list = DB::connection('sqlsrv')
						->table($table)
						->lists($this->unique_key[$table]);
		// List unique values from target table
		$target_list = DB::table($table)
						->lists($this->unique_key[$table]);

		$delete_keys = array_diff($target_list, $source_list);

		$delete_ctr = 0;
		if(count($delete_keys) > 0) {
			$chunked_delete_keys = array_chunk($delete_keys, 500);
			foreach ($chunked_delete_keys as $batch_delete_keys) {
				$count = DB::table($table)->whereIn($this->unique_key[$table], $batch_delete_keys)->delete();
				$delete_ctr = ($count > 0) ? $delete_ctr + $count : $delete_ctr + 0;
			}
		}

		$result = array(
			'keys_to_delete' => $delete_keys,
			'actual_keys_deleted' => $delete_ctr
		);

		return $result;
	}

	/**
	 * Log result to file
	 *
	 * @param 	mixed 	array/object to log
	 */
	private function _log($message, $result)
	{
		$this->monolog->addInfo($message.' - '.json_encode($result));
	}

	public function getHashPassword()
	{
		$password = Input::get('password');
		$password = Hash::make($password);
		return Response::json(array('hashed_password' => $password));
	}

	public function getRole()
	{
		$user_id = Input::get('user_id');

		$result = array();
		if($user_id) {
			$user = new User();
			$result = $user->getRole($user_id);
		}

		return Response::json($result);
	}
}
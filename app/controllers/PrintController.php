<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PrintController extends BaseController {

    public function action_printtask($task_id)
    {
        $title = 'OLDHAM, LI & NIE';
        $tasks = new Tasks();
        $commcenter = new Commcenter();
        $files['taskfiles'] = $tasks->gettaskfiles($task_id);
        $files['responsefiles'] = $tasks->getresponsefiles($task_id);
        $task = $tasks->gettask($task_id);
        $users = $commcenter->getallusers();
        return View::make('print/printtask',array(
                'users' => $users,
                'task' => $task,
                'files' => $files,
                'title' => $title
            )
        );
    }

}

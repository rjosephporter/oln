<?php

class FeedbackController extends Controller {

	public function postSend()
	{
		$data = Input::all();

		$unique_id = time();

		$sender = isset(Auth::user()->EmployeeID) ? Auth::user()->EmployeeID : 'Anonymous';

		$description = "";
		$description .= "- URL: " . $data['url'] . "\r\n";
		//$description .= "- Screenshot: [Link](".$data['img'].")\r\n";
		$description .= "- Note: " . $data['note'] . "\r\n";

		$task_data = array(
            "title" => "Issue sent by " . $sender . " - " . $unique_id,
            "project_id" => 1,
            "color_id" => "red",
            "description" => $description,
            "category_id" => 13
		);

		//Queue::push(function($job) use ($task_data, $unique_id, $data) {
			$client = new JsonRPC\Client('http://kanboard.m-s-s-asia.com/jsonrpc.php');
	    	$client->authentication('jsonrpc','a9d2b4b96abdbb1aa257024a18fd310d3ce5d2e87d0615d3bbf0990e9bb1');
	    	$result = $client->createTask($task_data);

	    	DB::table('feedback')->insert(array('id' => $unique_id, 'screenshot' => $data['img']));

	    	//$job->delete();
		//});

		return Response::json($data);
	}

}
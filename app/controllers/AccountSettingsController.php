<?php

class AccountSettingsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    private $accountsettings;

    public function getIndex(){
        return View::make('pages/change_password');
    }

    public function __construct()
    {
        $this->user = new User();
        $this->accountsettings = new AccountSettings();
    }

    public function postUpdatepass()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        //$id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $id_active = Auth::user()->user_id;


        $oldpassword = $data['new_old_password'];

        //$get_result = $this->accountsettings->getPassword(User::getEmployeeID($id_active));
        $get_result = $this->accountsettings->getPassword($id_active);
        $credentials = array(
            'password'          => $oldpassword,
            //'user_id'           => User::getEmployeeID($id_active)
            'user_id'           => $id_active
        );
        if (!Auth::validate($credentials))
        {
    
            $response['val']    = $credentials;
            $response['status'] = 'error';
            $response['message'] = 'Old password not found';
            return Response::json($response);
        }
        
        
        $password = Hash::make($data['new_password']);

        $update_data = array(
            'password'   		=> $password,
            //'user_id'           => User::getEmployeeID($id_active),
            'user_id'           => $id_active,
            'changed_password'   => 1
        );


       $update_result = $this->accountsettings->updatePassword($user_id, $update_data);

        if($update_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Password updated successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postSave(){

        //$id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $id_active = Auth::user()->user_id;
        if (Input::hasFile('image')){
            $valid_exts = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
            $max_size = 300 * 300; // max file size (200kb)
            $path = public_path().'/assets/thisapp/images/staff'; // upload directory
            $fileName = NULL;
            if ( $_SERVER['REQUEST_METHOD'] === 'POST' )
            {
            $file = Input::file('image');
            // get uploaded file extension
            //$ext = $file['extension'];
            $ext = $file->guessClientExtension();
            // get size
            $size = $file->getClientSize();
            // looking for format and size validity
            $name = $file->getClientOriginalName();
            if (in_array($ext, $valid_exts) AND $size < $max_size)
            {
            // move uploaded file from temp to uploads directory
            if ($file->move($path,$name))
            {
            $status = 'Image successfully uploaded!';
            $fileName = $name;
            }
            else {
            $status = 'Upload Fail: Unknown error occurred!';
            }
            }
            else {
            $status = 'Upload Fail: Unsupported file format or It is too large to upload!';
            }
            }
            else {
            $status = 'Bad request!';
            }

            $user_id          = User::getEmployeeID($id_active);
            $update_data = array(
            'photo'          => $fileName,
            'EmployeeID'        =>  $user_id
            );

            $update_result = $this->accountsettings->updatePhoto($user_id, $update_data);
        
            if($update_result == true) {
                $response['val'] = $fileName;
                $response['status'] = 'success';
                $response['message'] = 'Photo updated successfully.';
            } else {
                $response['val'] = $fileName;
                $response['status'] = 'error';
                $response['message'] = 'Database error.';            
            }
        }else{
            $response['status'] = 'fail';
            $response['message'] = 'Photo upload failed';
            $status ='No image found';
        }

    
       
       
        return Redirect::back()
                ->withErrors($response['status']); 
        
    
    }
    public function postUpdatephoto(){
        $data = Input::all();
        $filename=$_FILES["filename"]["name"];
        //$id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $id_active = Auth::user()->user_id;
        $user_id          = User::getEmployeeID($id_active);
        $update_data = array(
            'photo'          => 'test.jpg',
            'EmployeeID'        =>  $user_id
        );
        $update_result = $this->accountsettings->updatePhoto($user_id, $update_data);

        if($update_result == true) {
             $response['val'] = $filename;
            $response['status'] = 'success';
            $response['message'] = 'Photo updated successfully.';
        } else {
            $response['val'] = $filename;
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
}

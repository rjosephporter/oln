<?php

class ClientsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function getIndex(){
        return View::make('pages/clients');
    }
    
    public function getClientlist(){
        $c = new Client;
        return Response::json($c->getClientsList());
    }
    
    public function getClienthistory($id){
        $c = new Client;
        echo json_encode($c->getMbyC($id));
    }

    public function getNew()
    {

       // $id_active = Auth::user()->user_id;
        $c= new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        //if(in_array($id_active, Config::get('oln.admin')))
        //if(User::hasRoleById($id_active, 'Admin'))
        if(User::isAdmin($id_active))
            $user_active = "all";
        else
            $user_active =  User::getEmployeeID($id_active);
        
        $handler = new Handlers();


        if(User::isAdmin(Auth::id())) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = $handler->getE();
        }



        $data['handler_list'] = $handler_list;

        $data['nationality_list'] = $c->getNationality();

        if(User::isAdmin($id_active)){
            //allowed
            $redirectpage='pages/client_addnew';

        }else{
            //do something else
            $redirectpage='pages/client_addnew';
        }

  
        return View::make($redirectpage, $data);
    }

    public function postSubmitnewclient()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $c = new Client;

        $ctype=$data['new_clienttype_id'];

       
        if($ctype==1){//individual
            

            $validator[] = Validator::make(
                array(
                    'CUSTTYPE'  =>$data['new_clienttype_id'],
                    'CompanyName_A1'     => $data['new_clientname'][0],
                    'HKID' =>$data['new_hkid'][0],
                    'Incharge' => $data['new_handler_id'][0],
                    'DATEOFBIRTH'=>$data['new_dob'][0],
                    'PLACEOFBIRTH'=>$data['new_pob'][0],
                    'NATIONALITY'=>$data['new_nationality_id'][0],
                    'HOMEADDRESS'=>$data['new_home_address'][0],
                    'CORADDRESS'=>$data['new_cor_address'][0],
                    'OCCUPATION'=>$data['new_occupation'][0],
                    'EMPLOYER'=>$data['new_employer'][0],
                    'TELNUM'=>$data['new_hometel'][0],
                    'OFFICETEL'=>$data['new_offtel'][0],
                    'FAXNUM'=>$data['new_homefax'][0],
                    'MOBILE'=>$data['new_mobile'][0],
                    'EMAIL'=>$data['new_email'][0],
                    'ISREP'=>(Input :: has('new_acting_id') ? $data['new_acting_id'] : 0),
                    'REPNAME'=>$data['new_personname'][0],
                    'REPNUM'=>$data['new_contactnum'][0]
                    
                    
                ),
                array(
                    'CUSTTYPE'  =>'required',
                    'CompanyName_A1'     => 'required',
                    'HKID' =>'required',
                    'Incharge' => 'required',
                    'DATEOFBIRTH'=>'required',
                    'PLACEOFBIRTH'=>'required',
                    'NATIONALITY'=>'required',
                    'HOMEADDRESS'=>'required',
                    //'CORADDRESS'=>'required',
                    'OCCUPATION'=>'required',
                    'EMPLOYER'=>'required',
                    //'TELNUM'=>'required',
                    //'OFFICETEL'=>'required',
                    //'FAXNUM'=>'required',
                    'MOBILE'=>'required',
                    'EMAIL'=>'required'
                    //'ISREP'=>'required'
                    //'REPNAME'=>'required',
                    //'REPNUM'=>'required'
                    
                )
            );

        }else{
            $validator[] = Validator::make(
                array(
                    
                   
                    'CUSTTYPE'  =>$data['new_clienttype_id'],
                    'CompanyName_A1'=> $data['new_clientcomname'][0],
                    'ComNum'=>$data['new_comnum'][0],
                    'PLACEOFINC'=>$data['new_poi'][0],
                    'BUSADD'=>$data['new_bus_address'][0],
                    'Nature_A'=>$data['new_nature'][0],
                    'TELNUM'=>$data['new_telnum'][0],
                    'FAXNUM'=>$data['new_faxnum'][0]
                    
                    
                ),
                array(
                   
                    'CUSTTYPE'  =>'required',
                    'CompanyName_A1'=> 'required',
                    'ComNum'=>'required',
                    'PLACEOFINC'=>'required',
                    'BUSADD'=>'required',
                    'Nature_A'=>'required',
                    'TELNUM'=>'required',
                    'FAXNUM'=>'required'
                    
                )
            );

            
        }

            foreach($validator as $val) {
                        if($val->fails())
                            $failed_rules[] = $val->failed();
            }

                 //$custid=$c->createClient($data);
            //var_dump($custid);
            $custid=$c->createClient($data);
            if(count($failed_rules) > 0) {
                $result['status'] = 'error';
                $result['msg'] = 'Invalid inputs.';
                $result['failed_rules'] = $failed_rules;
            }/*else{
                $custid=$c->createClient($data);
                var_dump($custid);
                $result['status'] = 'success';
                $result['msg'] = 'Save successful. Resetting form...';
                $result['val'] =$custid;
            }*/ 
            else if($custid>0) {
                
                if($ctype==2){
                    $res=$this->postSubmitDirector($custid,0);
                    $res1=$this->postSubmitShareholder($custid,0);
                    $res2=$this->postSubmitOwner($custid,0);
                    $res3=$this->postSubmitReps($custid,0);
                    Debugbar::info($res);
                }
                $result['status'] = 'success';
                $result['msg'] = 'Save successful. Resetting form...';
                //$result['val'] =$custid;
            } else {
                $result['status'] = 'error';
                $result['msg'] = 'Database error. Try again.';
            }

   

        return Response::json($result);
    } 

    public function postSubmitDirector($custid,$checker)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $c = new Client;
        if($checker==1)
            $val1=$data['new_dirnew'];
        else
            $val1=$data['new_dir'];

        $item_count = count($val1);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_dir'     => $val1[$i]

                ),
                array(
                    'new_dir'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($c->createDirectors($item_count, $data,$custid,$checker)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitShareholder($custid,$checker)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $c = new Client;
        if($checker==1)
            $val1=$data['new_sharenew'];
        else
            $val1=$data['new_share'];

        $item_count = count($val1);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_share'     => $val1[$i]

                ),
                array(
                    'new_share'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($c->createSharers($item_count, $data,$custid,$checker)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitOwner($custid,$checker)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $c = new Client;
        if($checker==1)
            $val1=$data['new_bennew'];
        else
            $val1=$data['new_ben'];

        $item_count = count($val1);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_ben'     => $val1[$i]

                ),
                array(
                    'new_ben'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($c->createBenOwners($item_count, $data,$custid,$checker)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    }

    public function postSubmitReps($custid,$checker)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $c = new Client;

        if($checker==1){
            $val1=$data['new_personnew'];
            $val2=$data['new_personhkidnew'];
            $val3=$data['new_personconnumnew'];
            $val4=$data['new_personposnew'];
            $val5=$data['new_personemailnew'];
        }
        else{
            $val1=$data['new_person'];
            $val2=$data['new_personhkid'];
            $val3=$data['new_personconnum'];
            $val4=$data['new_personpos'];
            $val5=$data['new_personemail'];
        }

        $item_count = count($val1);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_person'     => $val1[$i],
                    'new_personhkid'     => $val2[$i],
                    'new_personconnum'     => $val3[$i],
                    'new_personpos'     => $val4[$i],
                    'new_personemail'     => $val5[$i],

                ),
                array(
                    'new_person'     => 'required',
                    'new_personhkid'     => 'required',
                    'new_personconnum'     => 'required',
                    'new_personpos'     => 'required',
                    'new_personemail'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($c->createReps($item_count, $data,$custid,$checker)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

     public function getClientinfo(){

        $client = new Client();
        $client_id = Input::get('client_id');
        $clientlist=$client->getInfo($client_id);

        return Response::json($clientlist);

    }

    public function getDirinfo(){

        $client = new Client();
        $client_id = Input::get('client_id');
        $clientlist=$client->getDirInfo($client_id);

        return Response::json($clientlist);

    }

    public function getShareinfo(){

        $client = new Client();
        $client_id = Input::get('client_id');
        $clientlist=$client->getShareInfo($client_id);

        return Response::json($clientlist);

    }

    public function getBenowninfo(){

        $client = new Client();
        $client_id = Input::get('client_id');
        $clientlist=$client->getBenOwnInfo($client_id);

        return Response::json($clientlist);

    }

    public function getRepinfo(){

        $client = new Client();
        $client_id = Input::get('client_id');
        $clientlist=$client->getRepInfo($client_id);
         Debugbar::info($clientlist);
        return Response::json($clientlist);

    }


    public function getClientUpdate2()
    {

        $matter = new JobAssignment();
        //$id_active = Auth::user()->user_id;
        $client_id = Input::get('id');
        $data['clientid']=$client_id;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $handler = new Handlers();
        $c = new Client;

        if(User::isAdmin(Auth::id())) {
               $handler_list = $handler->getE();
        } else {
                    $handler_list = $handler->getE();
        }
        
       	$data['handler_list'] = $handler_list;
 
        $clientlist=$c->getListinfo('1');
        $data['client_list'] =$clientlist;
        $clientlist2=$c->getListinfo('2');
        $data['client_list2'] =$clientlist2;
    
        $data['nationality_list'] = $c->getNationality();

        $data['director_list'] = $c->getDirectors();

        if(User::isAdmin($id_active)){
            //allowed
            $redirectpage='pages/clients_update';

        }else{
            //do something else
            $redirectpage='pages/clients_update';
        }

  
        return View::make($redirectpage, $data);
    } 

    public function getClientUpdate($clientid)
    {

        $matter = new JobAssignment();
        $data['client_id']=$clientid;
        //$id_active = Auth::user()->user_id;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $handler = new Handlers();
        $c = new Client;

        if(User::isAdmin(Auth::id())) {
               $handler_list = $handler->getE();
        } else {
                    $handler_list = $handler->getE();
        }
        
        $data['handler_list'] = $handler_list;
 
        $clientlist=$c->getListinfo('1');
        $data['client_list'] =$clientlist;
        $clientlist2=$c->getListinfo('2');
        $data['client_list2'] =$clientlist2;
    
        $data['nationality_list'] = $c->getNationality();

        $data['director_list'] = $c->getDirectors();

        $clientinfo = $c->getInfo($clientid);
        //var_dump($clientinfo);
        $data['clientinfo']=$clientinfo;
  
        return View::make('pages/clients_update',$data);
    } 

    public function postUpdateclient()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $updated_data; $update_result;
        //if($data['ctype']=='1')
           // $cust=$data['new_customerid']; //(Input::has('new_customer_id2') ? $data['new_customer_id2'][0]:'')
        //else
           // $cust=(Input::has('new_customer_id2') ? $data['new_customer_id2'][0]:'');
        //print_r($data['new_acting_id']);
        if($data['ctype']=='1'){

            $update_data = array(
                
                        'CompanyID' =>'OLN',
                        'CustomerID'          => $data['cid'],// (Input::has('new_customer_id') ? $data['new_customer_id'][0]:''),
                        'Incharge'            => $data['new_handler_id'][0],
                        'User_U'        =>    User::getEmployeeID($id_active),
                        'CompanyName_A1'  => $data['new_clientname'][0],
                        'Date_U'        => date('Y-m-d H:i:s'),
                        'CUSTTYPE'  =>$data['ctype'],
                        'HKID' =>$data['new_hkid'][0],
                        'Incharge' => $data['new_handler_id'][0],
                        'DATEOFBIRTH'=>date('Y-m-d', strtotime($data['new_dob'][0])),
                        'PLACEOFBIRTH'=>$data['new_pob'][0],
                        'NATIONALITY'=>$data['new_nationality_id'][0],
                        'HOMEADDRESS'=>$data['new_home_address'][0],
                        'CORADDRESS'=>$data['new_cor_address'][0],
                        'OCCUPATION'=>$data['new_occupation'][0],
                        'EMPLOYER'=>$data['new_employer'][0],
                        'TELNUM'=>$data['new_hometel'][0],
                        'OFFICETEL'=>$data['new_offtel'][0],
                        'FAXNUM'=>$data['new_homefax'][0],
                        'MOBILE'=>$data['new_mobile'][0],
                        'EMAIL'=>$data['new_email'][0],
                        'ISREP'=>(Input :: has('new_acting_id') ? 1 : 0),
                        'REPNAME'=>$data['new_personname'][0],
                        'REPNUM'=>$data['new_contactnum'][0]
            
            );
           
        }else{
             $update_data = array(
            
                    'CompanyID' =>'OLN',
                    'CustomerID'          =>  $data['cid'],
                    //'Incharge'            => $data['new_handler_id'][0],
                    'User_U'        =>    User::getEmployeeID($id_active),
                    'Date_U'        => date('Y-m-d H:i:s'),
                    'CUSTTYPE'  =>$data['ctype'],
                    'IsActive'              => '1',
                    'StatusCode'   =>'1',
                    'CompanyName_A1'=> $data['new_clientcomname'][0],
                    'ComNum'=>$data['new_comnum'][0],
                    'PLACEOFINC'=>$data['new_poi'][0],
                    'BUSADD'=>$data['new_bus_address'][0],
                    'Nature_A'=>$data['new_nature'][0],
                    'TELNUM'=>$data['new_telnum'][0],
                    'FAXNUM'=>$data['new_faxnum'][0]
        
        );
        
        }

        $update_result = $client->updateClient($data['cid'], $update_data);

       //TODO: update each individual custsub records

        if($update_result == true) {
            if($data['ctype']=='2'){
                $res=$this->postUpdateDirs();
                $res1=$this->postUpdateShare();
                $res2=$this->postUpdateOwn();
                $res3=$this->postUpdateRep();
            
            //$response['status'] = $res;
            //$response['message'] = $res->message;
                 $response['status'] = 'success';
                 $response['message'] = 'Client updated successfully.';
            }else{
                 $response['status'] = 'success';
                 $response['message'] = 'Client updated successfully.';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    //update cust subs
    public function postUpdateDirs()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $item_count=$client->getDirectorsCount($data['cid']);
         //Debugbar::info(Input::has($data['new_dirnew']));
         if(Input::has('new_dirnew'))
            $total=count($data['new_dir']) + count($data['new_dirnew']);
         else
            $total=count($data['new_dir']);

        if($item_count == $total) { //if equal length, update directly
            for ($i = 0; $i < count($data['new_dir']); $i++) { 
               $update_data = array(
                    'DIRECTORID'    =>$data['new_dirid'][$i],
                    'CustomerID' =>   $data['cid'],
                    'DIRECTOR_NAME'              => $data['new_dir'][$i],
                    'Date_U'        => date('Y-m-d H:i:s')
                    //'IsActive'=>-1
                );

             $update_result = $client->updateDir($data['new_dirid'][$i], $update_data);
        

            }

             //TODO: update each individual custsub records

                if($update_result == true) {
                    $response['status'] = 'success';
                    $response['message'] = 'Client directors updated successfully.';
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'Database error.';            
                }

            
        }else{ //there are new fields
            //insert new fields first
             Debugbar::info('was here');
            $res=$this->postSubmitDirector($data['cid'],1);
             Debugbar::info($res);
            $response['status']=$res;
        }

      

        return $response;
    }

    public function postUpdateShare()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $item_count=$client->getSharesCount($data['cid']);
          if(Input::has('new_sharenew'))
            $total=count($data['new_share']) + count($data['new_sharenew']);
         else
            $total=count($data['new_share']);
         if($item_count == $total) {
            for ($i = 0; $i < count($data['new_share']); $i++) { 
               $update_data = array(
                    'SHAREID'    =>$data['new_shareid'][$i],
                    'CustomerID' =>  $data['cid'],
                    'SHAREHOLDER_NAME'              => $data['new_share'][$i],
                    'Date_U'        => date('Y-m-d H:i:s')
                    //'IsActive'=>-1
                );


             Debugbar::info($update_data);
             $update_result = $client->updateShare($data['new_shareid'][$i], $update_data);

            }
            if($update_result == true) {
                $response['status'] = 'success';
                $response['message'] = 'Client shareholders updated successfully.';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Database error.';            
            }
            
        }else{ //there are new fields
            //insert new fields first
             Debugbar::info('was here');
            $res=$this->postSubmitShareholder($data['cid'],1);
             Debugbar::info($res);
            $response['status']=$res;
        }

       
        return $response;
    }
    public function postUpdateOwn()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $item_count=$client->getOwnersCount($data['cid']);
        if(Input::has('new_bennew'))
            $total=count($data['new_ben']) + count($data['new_bennew']);
         else
            $total=count($data['new_ben']);
         if($item_count == $total) {
            for ($i = 0; $i < count($data['new_ben']); $i++) { 
               $update_data = array(
                    'OWNERID'    =>$data['new_ownid'][$i],
                    'CustomerID' =>   $data['cid'],
                    'OWNER_NAME'              => $data['new_ben'][$i],
                    'Date_U'        => date('Y-m-d H:i:s')
                    //'IsActive'=>-1
                );


             Debugbar::info($update_data);
             $update_result = $client->updateOwn($data['new_ownid'][$i], $update_data);
        

            }
            if($update_result == true) {
                $response['status'] = 'success';
                $response['message'] = 'Client shareholders updated successfully.';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Database error.';            
            }
            
        }else{ //there are new fields
            //insert new fields first
             Debugbar::info('was here');
            $res=$this->postSubmitOwner($data['cid'],1);
             Debugbar::info($res);
            $response['status']=$res;
        }

        return $response;
    }
    public function postUpdateRep()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $item_count=$client->getRepCount($data['cid']);
        if(Input::has('new_personnew'))
            $total=count($data['new_person']) + count($data['new_personnew']);
         else
            $total=count($data['new_person']);
          Debugbar::info($data);

         if($item_count == $total) {
            for ($i = 0; $i < count($data['new_person']); $i++) { 
               $update_data = array(
                    'REPID'    =>$data['new_repid'][$i],
                    'CustomerID' =>   $data['cid'],
                    'REPNAME'              => $data['new_person'][$i],
                    'HKID'              => $data['new_personhkid'][$i],
                    'CONNUM'              => $data['new_personconnum'][$i],
                    'EMAIL'              => $data['new_personemail'][$i],
                    'Date_U'        => date('Y-m-d H:i:s')
                    //'IsActive'=>-1
                );


             Debugbar::info($update_data);
             $update_result = $client->updateRep($data['new_repid'][$i], $update_data);
     
            }
            if($update_result == true) {
                $response['status'] = 'success';
                $response['message'] = 'Client shareholders updated successfully.';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Database error.';            
            }
            
        }else{ //there are new fields
            //insert new fields first
             Debugbar::info('was here');
            $res=$this->postSubmitReps($data['cid'],1);
             Debugbar::info($res);
            $response['status']=$res;
        }

        return $response;
    }

    public function getClientDelete()
    {

        $matter = new JobAssignment();
        //$id_active = Auth::user()->user_id;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $handler = new Handlers();
        $c = new Client;

        if(User::isAdmin(Auth::id())) {
               $handler_list = $handler->getE();
                } else {
                    $handler_list = $handler->getE();
        }
        
        $data['handler_list'] = $handler_list;
        $clientlist=$c->getListinfo('1');
        $data['client_list'] =$clientlist;
        $clientlist2=$c->getListinfo('2');
        $data['client_list2'] =$clientlist2;
   
        $data['nationality_list'] = $c->getNationality();
      
        if(User::isAdmin(Auth::id())){
            //allowed
            $redirectpage='pages/clients_delete';

        }else{
            //do something else
            $redirectpage='pages/clients_delete';
        }

  
        return View::make($redirectpage, $data);
    } 

    public function postDeleteclient()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $client = new Client();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $updated_data; $update_result;
       
        if($data['new_clienttype_id']=='1')
            $custid=$data['new_customer_id'][0];
        else
            $custid=$data['new_customer_id2'][0];

        $update_data = array(
                'CustomerID'  =>$custid,
                'IsActive'   => -1,
                'User_U'        => User::getEmployeeID($id_active),
                'Date_U'        => date('Y-m-d H:i:s')
            
            );
        

       $update_result = $client->updateClient($custid, $update_data);

        if($update_result == true) {
            $res=$this->postDeletedirectorbyclient($custid);
            $res=$this->postDeletesharebyclient($custid);
            $res=$this->postDeleteownerbyclient($custid);
            $res=$this->postDeleterepbyclient($custid);

            $response['status'] = 'success';
            $response['message'] = 'Client deleted successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postDeletedirector()
    {
        $c = new Client;
        $response = array();
        if(!Input::has('director_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [director_id]';
            return Response::json($response);
        }

        $dir_id = Input::get('director_id');
        $delete_result = $c->DeleteDir($dir_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Director deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeleteshare()
    {
        $c = new Client;
        $response = array();
        if(!Input::has('share_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [share_id]';
            return Response::json($response);
        }

        $dir_id = Input::get('share_id');
        $delete_result = $c->DeleteShare($dir_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Shareholder deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeleteowner()
    {
        $c = new Client;
        $response = array();
        if(!Input::has('owner_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [owner_id]';
            return Response::json($response);
        }

        $dir_id = Input::get('owner_id');
        $delete_result = $c->DeleteOwner($dir_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Beneficial owner deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeleterep()
    {
        $c = new Client;
        $response = array();
        if(!Input::has('rep_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [director_id]';
            return Response::json($response);
        }

        $dir_id = Input::get('rep_id');
        $delete_result = $c->DeleteRep($dir_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Representative deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    //new
    public function postDeletedirectorbyclient($clientid)
    {
        $c = new Client;
        $response = array();
        $delete_result = $c->DeleteDirClient($clientid);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Director/s deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeletesharebyclient($clientid)
    {
        $c = new Client;
        $response = array();
        
        $delete_result = $c->DeleteShareClient($clientid);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Shareholder deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeleteownerbyclient($clientid)
    {
        $c = new Client;
        $response = array();
        $delete_result = $c->DeleteOwnerClient($clientid);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Beneficial owner deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    public function postDeleterepbyclient($clientid)
    {
        $c = new Client;
        $response = array();
       
        $delete_result = $c->DeleteRepClient($clientid);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Representative deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }
    
    public function getSearch($client){
        $u = new Utilities;
        return Response::json($u->SearchClient($client));
    }
}

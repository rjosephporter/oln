<?php

class ReportsController extends BaseController {

    /* Remember query for N minutes (passed using $filter) */
    private $remember_query_duration = 5; 

    public function getIndex()
    {
        $handler = new Handlers();

        /* Temporary */
        $filter = array();
        $user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;
        $year = Input::get('year');

        if($year != '0') 
            $filter['workdate_year'] = $year;

        $billing = new Billing();
        $billing_detail = $billing->getTotalUnbilledAmount($user_id, $filter);
        if(!empty($billing_detail)) {
            $data['billed_total_amount'] = "$" . number_format($billing_detail->total_billed_amount, 0, '.', ',');
            $data['unbilled_total_amount'] = "$" . number_format($billing_detail->total_unbilled_amount, 0, '.', ',');
            $data['earliest_billing_date'] = $billing_detail->earliest_date;
            $data['latest_billing_date'] = $billing_detail->latest_date;
        } else {
            $data['billed_total_amount']   = "$0.00";
            $data['unbilled_total_amount'] = "$0.00";
            $data['earliest_billing_date'] = "0000-00-00";
            $data['latest_billing_date']   = "0000-00-00";
        }
        /* /Temporary */

        $filter['remember_query'] = $this->remember_query_duration;
        $data['logged_in_employee'] =  User::find($this->principal_user)->EmployeeID; //Auth::user()->EmployeeID;
        $data['handlers'] = $handler->getList($filter);
        return View::make('pages/reports', $data);
    }

    private function _getGrandTotals($user_id, $filter)
    {
        $matter = new Matter();
        $filter['show_select_billdetails'] = true;
        $filter['show_select_mattertype'] = true;
        //$filter['remember_query'] = $this->remember_query_duration;
        $sql_string = $matter->prepareQueryForGetList($user_id, $filter)->toSql();
        $bindings = $matter->prepareQueryForGetList($user_id, $filter)->getBindings();
        $sql_string = str_replace('SQL_CALC_FOUND_ROWS', '', $sql_string);

       $result =  DB::select('select 
                                sum(t1.total_billed_amount) as grand_total_billed_amount, 
                                sum(t1.total_unbilled_amount) as grand_total_unbilled_amount,
                                sum(t1.total_billable_amount) as grand_total_billable_amount 
                            from ('.$sql_string.') as t1', $bindings);

        return $result[0];
    }

    public function getDtMattersList()
    {
        $filter = array();
    	$user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;

        $handler_id = Input::get('handler_id', false);
        $folder_types =  Input::get('folder_types', false);
        $unbilled_amount_range = Input::get('unbilled_amount_range', false);

        if($handler_id !== false && $handler_id !== '0') $filter['handler_id'] = $handler_id;
        if($folder_types !== false && $folder_types !== '0') $filter['folder_types'] = explode(',',$folder_types);
        switch ($unbilled_amount_range) {
            case '1':
                $filter['total_unbilled_amount_range']['to'] = 29999.99;
                break;

            case '2':
                $filter['total_unbilled_amount_range']['from'] = 30000;
                break;
            
            default:
                # code...
                break;
        }
        $filter['remember_query'] = $this->remember_query_duration;

        $sSearch = Input::get('sSearch'); 
        if(!empty($sSearch)) $filter['dt-search'] = $sSearch;
        $grand_totals =  $this->_getGrandTotals($user_id, $filter);
        unset($filter['dt-search']);

        $filter['show_select_basic'] = true;
        $filter['show_select_mattertype'] = true;
        $filter['show_select_billdetails'] = true;
        $filter['show_select_invoicedetails'] = true;

    	$matter = new Matter();
    	return Datatable::query($matter->prepareQueryForGetList($user_id, $filter))
        //->showColumns('JobAssignmentID','Description_A','Incharge', 'total_billable_amount', 'total_unbilled_amount','matter_type', 'Date_C', 'CustomerID', 'CompanyName_A1')
        ->showColumns('dummy_column', 'JobAssignmentID', 'CustomerID', 'CompanyName_A1', 'Description_A','Incharge', 'total_billable_amount', 'total_unbilled_amount','matter_type', 'Date_C', 'last_invoice_date', 'last_invoice_amount')
        ->searchColumns('ja.JobAssignmentID', 'ja.CustomerID', 'CompanyName_A1', 'ja.Description_A','ja.Incharge')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('JobAssignmentID', 'CustomerID', 'CompanyName_A1','total_billable_amount','total_unbilled_amount', 'last_invoice_date', 'last_invoice_amount')
        ->make_with_param($grand_totals);
    }

    public function getDtHandlersList()
    {
        $filter = array();
        $user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;

        $unbilled_amount_range = Input::get('unbilled_amount_range', false);
        switch ($unbilled_amount_range) {
            case 1:
                $filter['total_unbilled_amount_range']['to'] = 29999.99;
                break;

            case 2:
                $filter['total_unbilled_amount_range']['from'] = 30000;
                break;
            
            default:
                
                break;
        }
        $filter['remember_query'] = $this->remember_query_duration;

        $handler = new Handlers();
        return Datatable::query($handler->prepareQueryForGetList($user_id, $filter))
        ->showColumns('EmployeeID','matter_count','total_billed_amount', 'total_billable_amount', 'total_unbilled_amount')
        ->searchColumns('emp.EmployeeID')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('EmployeeID','matter_count','total_billed_amount','total_billable_amount','total_unbilled_amount')
        ->make();
    }

    public function getDtClientsList()
    {
        $filter = array();
        $user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;

        $unbilled_amount_range = Input::get('unbilled_amount_range', false);
        switch ($unbilled_amount_range) {
            case '1':
                $filter['total_unbilled_amount_range']['to'] = 29999.99;
                break;

            case '2':
                $filter['total_unbilled_amount_range']['from'] = 30000;
                break;
            
            default:
                # code...
                break;
        }
        $filter['remember_query'] = $this->remember_query_duration;

        $client = new Client();
        return Datatable::query($client->prepareQueryForGetList($user_id, $filter))
        ->showColumns('customer_id','CompanyName_A1','handler_incharge','matter_count','total_unbilled_amount')
        ->searchColumns('ctm.CustomerID','ctm.CompanyName_A1','ctm.Incharge')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('customer_id','CompanyName_A1','handler_incharge','matter_count','total_unbilled_amount')
        ->make();
    }

    public function getChart()
    {
        $filter = array();
        $user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;

        $type = Input::get('type');
        $year = Input::get('year');

        $filter['workdate_year'] = $year;
        $filter['group_by'] = array('rm.workdate_year', 'rm.workdate_month');
        $filter['sortby'] = 'rm.workdate_month';
        $filter['order'] = 'asc';
        $filter['remember_query'] = $this->remember_query_duration;

        $filter['show_select_basic'] = true;
        $filter['show_select_mattertype'] = true;
        $filter['show_select_billdetails'] = true;

        switch ($type) {
            case 'matters':
                $matter = new Matter();
                $query_result = $matter->getChartData($user_id, $filter);
                break;
            
            default:
                # code...
                break;
        }

        return Response::json($query_result);
    }

    public function getMatterTimeline()
    {
        $matter_id = Input::get('matter_id');

        $jobassignment = new JobAssignment();
        $data['timeline'] = $jobassignment->getTL($matter_id);

        return View::make('pages/matters_timeline', $data);
        //return Response::json($timeline);
    }

    public function getMatterBilling()
    {
        
    }
    
    public function getDtWipList()
    {
        $filter = array();
    	$user_id = ($this->principal_user) ? $this->principal_user : Auth::user()->user_id;

        $handler_id = Input::get('handler_id', false);
        $folder_types =  Input::get('folder_types', false);
        $unbilled_amount_range = Input::get('unbilled_amount_range', false);

        if($handler_id !== false && $handler_id !== '0') $filter['handler_id'] = $handler_id;
        if($folder_types !== false && $folder_types !== '0') $filter['folder_types'] = explode(',',$folder_types);
        switch ($unbilled_amount_range) {
            case '1':
                $filter['total_unbilled_amount_range']['to'] = 29999.99;
                break;

            case '2':
                $filter['total_unbilled_amount_range']['from'] = 30000;
                break;
            
            default:
                # code...
                break;
        }
        $filter['remember_query'] = $this->remember_query_duration;

        $sSearch = Input::get('sSearch'); 
        if(!empty($sSearch)) $filter['dt-search'] = $sSearch;
        $grand_totals =  $this->_getGrandTotals($user_id, $filter);
        unset($filter['dt-search']);

        $filter['show_select_basic'] = true;
        $filter['show_select_mattertype'] = true;
        $filter['show_select_billdetails'] = true;
        // $filter['show_select_invoicedetails'] = true;

    	$matter = new Matter();
    	return Datatable::query($matter->prepareQueryForGetList($user_id, $filter))
        //->showColumns('JobAssignmentID','Description_A','Incharge', 'total_billable_amount', 'total_unbilled_amount','matter_type', 'Date_C', 'CustomerID', 'CompanyName_A1')
        ->showColumns('JobAssignmentID', 'CustomerID', 'CompanyName_A1', 'Description_A','Incharge', 'total_unbilled_amount','matter_type')
        ->searchColumns('ja.JobAssignmentID', 'ja.CustomerID', 'CompanyName_A1', 'ja.Description_A','ja.Incharge')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('JobAssignmentID', 'CustomerID', 'CompanyName_A1','total_unbilled_amount')
        ->make_with_param($grand_totals);
    }
}

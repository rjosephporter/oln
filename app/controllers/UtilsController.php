<?php

class UtilsController extends BaseController{
	
    public function getDecline($matterid, $handler){
        return Utils::declineMatter(array('id' => $matterid, 'delegatedTo' => $handler));
    }

    public function getComplete($matterid, $handler){
        return Utils::completeMatter(array('id' => $matterid, 'delegatedTo' => $handler));
    }

    public function getTimesheets(){
    	$utils = new Utilities;
        $active_user = User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);

        if($active_user == 'ADMIN')
            $obj['timesheets'] = $utils->getTSAdmin();
        else
            $obj['timesheets'] = $utils->getTimeSheets($active_user);

        $obj['count'] = count($obj['timesheets']);

        return Response::json(array('obj' => $obj));
    }

    public function getPic(){
        $utils = New Utilities;
        $pic = $utils->getPic();
        return Response::json($pic);
    }


    public function getFastcreate(){

        $user = new User;
        $utils = new Utilities;
        $active_user = User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);
        $obj['matters'] = Utils::getAllMatters();
        $obj['handler']= $user->getEmployeesByRoles("Handler");
        $obj['default'] =  Session::get('principal_user');
        $obj['special_matters'] = HelperLibrary::getSpecialMatters();

        return View::make('pages/timesheetfast2',array('obj' => $obj));
    }

    public function postLanguage() {
        $rules = [
        'language' => 'in:en,fr'
        ];
        $language['lang'] = '';
        $language = Input::get('lang');

        $validator = Validator::make(compact($language),$rules);
        if($validator->passes()){
            App::setLocale($language);
            Session::set('language',$language);
        }
        
        return Redirect::back();
    }

    public function getReopenmatter(){
        $data = Input::all();
        $u = new Utilities;
        $obj = array(
            'jobid'     =>  $data['matter'],
            'boxno'     => 'reopen',
            'closedby'  => User::getEmployeeID(Auth::user()->user_id),
            'role'      => 0,
            'date'      => date('Y-m-d'),
            'time'      => date('H:i:s')
        );
        $u->setCloseInfo($obj);
        Utils::reopenMatter($obj);
        HelperLibrary::RefreshMatters();

        return Response::json($obj);
    }

    public function getDelegatedmatters(){
        return Response::json(Utils::getDelegatedMatters());
    }

}
<?php

class EmployeeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    private $employee;
    private $user;
    private $role;

    public function getIndex(){
        $c=new Client();
        $emp=new Employee();
         $u = new User;
        $data['nationality_list'] = $c->getNationality();
        $data['role_list'] = $emp->getRoles();
        $data['handler_list'] = $u->getEmployeesByRoles('Handler');
        $data['assistant_list'] = $u->getEmployeesByRoles('PersonalAssistant');
        //print_r($data);
         //return Response::json($data);
        return View::make('pages/employee_addnew', $data);
    }
    
    public function __construct()
    {
        $this->employee = new Employee();
        $this->user = new User();
        $this->role = new Role();
    }
    
    public function getListingbyassistant(){
        $u = new User;
        $current_user_id = Auth::user()->user_id;
        return Response::json($u->getEmployeesByAssistant($current_user_id));
    }
    
    public function getListingbyroles($rolename){
        $u = new User;
        return Response::json($u->getEmployeesByRoles($rolename));
    }

    public function postSubmitemployee()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $employ = new Employee();

        $item_count = count($data['new_employeeid']);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'new_employeeid'     => $data['new_employeeid'][$i],
                    'nickname'     		=> $data['new_nickname'][$i],
                    'supervisor'     	=> $data['new_supervisor'][$i],
                    'bloodtype'     	=> $data['bloodtype'][$i],
                    'marital_status'     => $data['marital_status'][$i],
                    'new_dob'     => $data['new_dob'][$i],
                    'new_pob'     => $data['new_pob'][$i],
                    'new_datehired'     => $data['new_datehired'][$i],
                    'new_weekstarts'     => $data['new_weekstarts'][$i],
                    'weekdayam'     => (Input :: has('weekdayam') ? 1 : 0),
                    'weekdayamfrom'     => (Input :: has('weekdayamfrom') ? $data['weekdayamfrom'][$i]: 0),
                    'weekdayamto'     => (Input :: has('weekdayamto') ? $data['weekdayamto'][$i]: 0),
                    'weekdaypm'     => (Input :: has('weekdaypm') ? 1 : 0),
                    'weekdaypmto'     => (Input :: has('weekdaypmto') ? $data['weekdaypmto'][$i]: 0),
                    'weekdaypmfrom'     => (Input :: has('weekdaypmfrom') ? $data['weekdaypmfrom'][$i]: 0),
                    'saturdayam'     => (Input :: has('saturdayam') ? 1 : 0),
                    'saturdayamfrom'     => (Input :: has('saturdayamfrom') ? $data['saturdayamfrom'][$i]: 0),
                    'saturdayamto'     => (Input :: has('saturdayamto') ? $data['saturdayamto'][$i]: 0),
                    'saturdaypm'     => (Input :: has('saturdaypm') ? 1 : 0),
                    'saturdaypmto'     => (Input :: has('saturdaypmto') ? $data['saturdaypmto'][$i]: 0),
                    'saturdaypmfrom'     => (Input :: has('saturdaypmfrom') ? $data['saturdaypmfrom'][$i]: 0),
                ),
                array(
                    'new_employeeid'     => 'required',
                    'nickname'     		=> 'required',
                    'supervisor'     	=> 'required',
                    'bloodtype'     	=> 'required',
                    'marital_status'     => 'required',
                    'new_dob'     => 'required',
                    'new_pob'     => 'required',
                    'new_datehired'     => 'required',
                    'new_weekstarts'     => 'required',
   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($employ->createEmployee($item_count, $data)) {
        //} elseif(true) {      
             $res = $this->postSubmitUser($data['new_employeeid'][0]);
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitUser($empid)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $emp=new Employee();
        
        $item_count = count($empid);
        
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'first_name'     => $data['new_fname'][$i],
                    'last_name'     => $data['new_lname'][$i],
                    'email_address'  => $data['new_email'][$i],
                    'password'     => Hash::make($data['new_password'][$i]),
                    'EmployeeID'  => $empid,

                ),
                array(
                    'first_name'     => 'required',
                    'last_name'     => 'required',
                    'email_address'  => 'required',
                    'password'     => 'required',
                    'EmployeeID'  => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        $userid=$emp->createUser($item_count, $data,$empid);
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($userid>0) {
        //} elseif(true) {       
            $res = $this->postSubmitRole($userid);   
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitAssistant($employee_id)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $emp=new Employee();
        
        $item_count = count($data['new_pa_id']);
        
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'assistant_id'     => $data['new_pa_id'][$i],
                    'employee_id'     => $data['new_employeeid'][0],
                   

                ),
                array(
                    'assistant_id'     => 'required',
                    'employee_id'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

       
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($emp->createAssistant($item_count, $data,$employee_id)) {
        //} elseif(true) {       
    
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitHandler($employee_id)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $emp=new Employee();
        
        $item_count = count($data['new_handler_id']);
        
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'assistant_id'     => $data['new_employeeid'][0],
                    'employee_id'     => $data['new_handler_id'][$i],
                   

                ),
                array(
                    'assistant_id'     => 'required',
                    'employee_id'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

       
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($emp->createHandler($item_count, $data,$employee_id)) {
        //} elseif(true) {       
    
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

    public function postSubmitRole($userid)
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();
        $emp=new Employee();
        
        $item_count = count($data['new_role_id']);
        
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'role_id'     => $data['new_role_id'][$i],
                    'user_id'     => $userid,
                   

                ),
                array(
                    'role_id'     => 'required',
                    'user_id'     => 'required',
                   
                )
            );

            
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

       
        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } 
        elseif($emp->assignUsertoRole($item_count, $data,$userid)) {   
            if(Input::has('new_handler_id'))    
                    $res = $this->postSubmitHandler($userid); 
            else
                    $res = $this->postSubmitAssistant($userid);   
              
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return $result;
    } 

     public function getEmployeeinfo(){

        $employee = new Employee();
        $employee_id = Input::get('employee_id');
        $employeelist=$employee->getInfo($employee_id);

        return Response::json($employeelist);

    }

    public function getRoleinfo(){

        $employee = new Employee();
        $user_id = Input::get('user_id');
        $employeelist=$employee->getRoleInfo($user_id);

        return Response::json($employeelist);

    }

    public function getAssistinfo(){

        $employee = new Employee();
        $user_id = Input::get('user_id');
        $employeelist=$employee->getAssistantbyemployee($user_id);

        return Response::json($employeelist);

    }

    public function getHandlerinfo(){

        $employee = new Employee();
        $user_id = Input::get('user_id');
        $employeelist=$employee->getHandlersbyemployee($user_id);

        return Response::json($employeelist);

    }

    public function getEmployeeupdate($empid){
        $c=new Client();
        $emp=new Employee();
        $u = new User;
        $data['emp_id']=$empid;
        $data['nationality_list'] = $c->getNationality();
        $data['role_list'] = $emp->getRoles();
        $data['employee_list'] = $emp->getEmployees();
        $data['handler_list'] = $u->getEmployeesByRoles('Handler');
        $data['assistant_list'] = $u->getEmployeesByRoles('PersonalAssistant');
        return View::make('pages/employee_update', $data);
    }

    public function postUpdateemployee()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $employee = new Employee();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);
        $i=0;
        $update_data = array(
                        'SupervisorID'    =>$data['new_supervisor'][$i],
                        'Gender'          =>$data['gender'][$i],
                        'BloodType'       =>$data['bloodtype'][$i],
                        'MartialStatus'   =>$data['marital_status'][$i],
                        'Nationality'     =>$data['new_nationality_id'][$i],
                        'DateOfBirth'     =>date('Y-m-d', strtotime($data['new_dob'][$i])),
                        'PlaceOfBirth'    =>$data['new_pob'][$i],
                        'DateHire'        =>date('Y-m-d', strtotime($data['new_datehired'][$i])),
                        'NickName'        =>$data['new_nickname'][$i],
                        'WeekStartsOn'        =>$data['new_weekstarts'][$i],
                        'WeekdayAM_Apply'     =>(Input:: has('weekdayam') ? 1 : ''),
                        'WeekdayAM_Fr'        =>(Input:: has('weekdayamfrom') ? $data['weekdayamfrom'][$i] : ''),   
                        'WeekdayAM_To'        =>(Input:: has('weekdayamto') ? $data['weekdayamto'][$i] : ''),
                        'WeekdayPM_Apply'        =>(Input:: has('weekdaypm') ? 1 : ''),
                        'WeekdayPM_Fr'        =>(Input:: has('weekdaypmfrom') ? $data['weekdaypmfrom'][$i] : ''),
                        'WeekdayPM_To'        =>(Input:: has('weekdaypmto') ? $data['weekdaypmto'][$i] : ''),
                        'SaturdayAM_Apply'        =>(Input:: has('saturdayam') ? 1 : ''),
                        'SaturdayAM_Fr'        =>(Input:: has('saturdayamfrom') ? $data['saturdayamfrom'][$i] : ''),
                        'SaturdayAM_To'        =>(Input:: has('saturdayamto') ? $data['saturdayamto'][$i] : ''),
                        'SaturdayPM_Apply'        =>(Input:: has('saturdaypm') ? 1 : ''),
                        'SaturdayPM_Fr'        =>(Input:: has('saturdaypmfrom') ? $data['saturdaypmfrom'][$i] : ''),
                        'SaturdayPM_To'        =>(Input:: has('saturdaypmto') ? $data['saturdaypmto'][$i] : ''),
                        'User_U'          => $employee_id,     
                        'Date_U'          => date('Y-m-d H:i:s')
        
        );
        
        

        $update_result = $employee->updateEmployee(strtoupper($data['new_employee_id'][$i]), $update_data);

        if($update_result == true) {

                $res = $this->postUpdateuser(); 
    
                 $response['status'] = 'success';
                 $response['message'] = 'Employee updated successfully.';
            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postUpdateuser()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $employee = new Employee();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);
        $i=0;
        $update_data = array(
            
                        'first_name'                 => $data['new_fname'][$i],
                        'last_name'                  => $data['new_lname'][$i],
                        'email_address'              => $data['new_email'][$i],
                      
        
        );

        $update_result = $employee->updateUser(strtoupper($data['new_employee_id'][$i]), $update_data);

        if($update_result == true) {
                 $res = $this->postUpdaterole(); 
                 $response['status'] = 'success';
                 $response['message'] = 'Employee updated successfully.';
            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postUpdaterole()
    {
        $response = array();
        $user_id = Auth::id();
        
        $data = Input::all();
        $employee = new Employee();
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);

        $item_count=count($data['new_role_id']);

        for ($i = 0; $i < $item_count ; $i++) { 
               $update_data = array(
               
                    'role_id' => $data['new_role_id'][$i],
                
                );

        }

        $update_result = $employee->updateRole($data['userid'][0], $update_data);

        if($update_result == true) {
                
                 $response['status'] = 'success';
                 $response['message'] = 'Employee updated successfully.';
            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function getViewloginhistory($empid){
        $s = new Sysadmin();
        $e = new Employee();
        $data = array(); 
        $data['employee'] = $e->getInfo($empid);
        $data['login_history'] = $s->getUserLoginHistory($empid);
        return View::make('pages/user_login_history', $data);
    }
}
<?php

class BaseController extends Controller {

    protected $principal_user;

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */    
    private $scripts = array(
            'before' => array(
                '/assets/jquery/jquery-1.11.1.min.js',
                '/assets/jquery-ui-1.10.4/js/jquery-ui-1.10.4.min.js',
                '/assets/feedback/feedback.js',
                '/assets/thisapp/js/preload.js'
            ),
            'after' => array(
                '/assets/thisapp/js/toastr.min.js',
                '/assets/bootstrap-3.1.1-dist/js/bootstrap.min.js',
                '/assets/thisapp/js/printiframe.js',
                '/assets/thisapp/js/app.js',
                '/assets/thisapp/js/bootstrap-session-timeout.min.js'
            )
        );
    
    private $styles=array(
            '/assets/bootstrap-3.1.1-dist/css/bootstrap.min.css',
            '/assets/jquery-ui-1.10.4/css/smoothness/jquery-ui-1.10.4.custom.min.css',
            '/assets/feedback/feedback.min.css',
            '/assets/thisapp/css/toastr.min.css',
            '/assets/thisapp/css/sticky-footer-navbar.css',
            '/assets/thisapp/css/app.css',
            '/assets/thisapp/css/font-awesome.min.css',
        );

    public function __construct()
    {
        $this->principal_user = HelperLibrary::principalUser();
        HelperLibrary::storeSessionData(array('user_id' => Auth::check() ? Auth::id() : null, 'ip_address' => Request::ip()));
        HelperLibrary::createLoginHistory(array('user_id' => Auth::check() ? Auth::id() : null,'session_id'=>Session::getId(),'ip_address' => Request::ip()));
    }
    
    public function getScripts(){
        return $this->scripts;
    }
    
    public function getStyles(){
        return $this->styles;
    }
    
    protected function setupLayout()
    {

        if ( ! is_null($this->layout)){
            $this->layout = View::make($this->layout);
        }
    }

    public function getUsertasks()
    {
        $tasks = new Tasks();
        return $tasks->getmynumberoftasks(Auth::user()->user_id);
    }
    
    public function getUseravatar()
    {
        if(Auth::check()){
            $user = Helper::get_user_info(Auth::user()->user_id);
            if(isset($user->Photo)){
                return HTML::image('/assets/thisapp/images/staff/'.$user->Photo,'',array('class'=>"img-rounded",'style'=>'height:30px'));
            } else {
                return HTML::image('/assets/thisapp/images/app/avatar.gif','',array('class'=>"img-rounded",'style'=>'height:30px'));
            }
        }
    }

}

<?php

class MattersController extends BaseController {


    private $user;
    private $timesheet;
    private $matter;

    public function __construct()
    {
        $this->user = new User();
        $this->timesheet = new Timesheet();
        $this->matter = new JobAssignment();
    }
    
    public function getIndex(){
        $val = Input::all();
        $ip = isset($val['type']) ? $val['type'] : '';
        $obj = HelperLibrary::MattersListing('all','all','all',0,1,'',$ip);
        if($ip == 'ip')
            return View::make('pages/mattersip',array('obj' => $obj));
        else
            return View::make('pages/matterslandingmain',array('obj' => $obj));
    }
    
    public function getMcronjob(){
        try{
            HelperLibrary::RefeshHandlers();
            $handlers_msg = "Success - Date: ".date("Y-m-d")." Time: ".date("H:i:s");
        }catch(Exception $e){$handlers_msg = $e->getMessage()." Date: ".date("Y-m-d")." Time: ".date("H:i:s");}
        try{
            HelperLibrary::RefreshMatters();
            $matters_msg = "Success - Date: ".date("Y-m-d")." Time: ".date("H:i:s");
        }catch(Exception $e){$matters_msg = $e->getMessage()." Date: ".date("Y-m-d")." Time: ".date("H:i:s");}

        return Response::json(array('handlers' => $handlers_msg, 'matters' => $matters_msg));
    }
    
    public function postView($type,$client='all',$handler='all'){
        $obj = Input::all();
        $obj['current_user'] = User::getEmployeeID(Session::get("principal_user"));
        $obj['datetime'] = date("Y-m-d H:i:s");
        HelperLibrary::LegallyCloseMatter($obj);
        
        return $this->getView($type,$client,$handler);
    }
    
    public function getView($type, $client='all', $handler='all', $update=0, $bill_order = 1)
    {
        $val = Input::all();
        if($update == 1):
            $u = new Utilities;
            $matters = json_encode($j->getMattersBill());
            $u->saveDefaults(array('date' => date("Y-m-d"), 'time' => date("H:i:s"), 'data' => bzcompress($matters), 'name' => 'matters'));
        endif;
        $delegated = isset($val['deleg']) ? $val['deleg'] : '';
        $ip = isset($val['type']) ? $val['type'] : '';
        $obj = HelperLibrary::MattersListing($type,$client,$handler,$update,$bill_order, $delegated,$ip);
        
        $obj['search'] = isset($val['search']) ? $val['search'] : '';

        //return Response::json($obj);
        return View::make('pages/matters_listing',array('obj'=>$obj));
    }
    
    public function getUpdate($matter){
        $user = new User;
        $c = new Client;
        $j = new JobAssignment;
        $u = new Utilities;
        $data['client_list'] = $c->getClientsList('0');
        $data['handler_list'] =$user->getEmployeesByRoles("Handler");
        $data['matter'] = $j->getAllDetailsM($matter);
        $data['details'] = $u->getMatterDetails($matter);
        $data['credit'] = $u->getMatterCost($matter);
        if(count($data['details']) == 0):
            $data['details'][0] = (object)array(
                'JobAssignmentID'       => 0,
                'fixedpay'              => 0,
                'fixedamount'           => 0,
                'introducer'            => '',
                'beneficial_owner'      => '',
                'special_instructions'  => '',
                'client'                => '',
                'address'               => '',
                'email'                 => '',
                'tel'                   => '',
                'fax'                   => '',
                'attn'                  => '',
                're'                    => ''
            );
        endif; 

        return View::make('pages/matter_form2',$data);
    }

    public function postUpdate(){
        $u = new Utilities;
        $obj = Input::all();
        $data = array(
            'id'        => $obj['id'],
            'type'      => 1,
            'amount'    => $obj['amount'],
            'user'      => User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id),
            'date'      => date_format(date_create($obj['date']),"Y-m-d"),
            'time'      => date('H:i:s')
        );
        $u->insertMatterCost($data);
        $info['credit']     = $u->getMatterCost($obj['id']);
        $info['message']    = 'Successfully Made Deposit.';
        return Response::json($info);
    }
      
    public function getMattersdelegated($handler,$source_type){
        $u = new Utilities;
        
        $delegated = $u->getMattersDelegated($handler,$source_type);
        return Response::json($delegated);
    }
    
    public function getCH(){
        $j = new JobAssignment;
        $empl = $j->getE();
        $cust = array();//$j->getC();
        
        echo json_encode(array('e'=>$empl,'c'=>$cust));
    }
    
    public function getBH($id){
        $j = new JobAssignment;
        echo json_encode($j->getBH($id));
    }
    
    public function getTL($id){
        $j = new JobAssignment;
        echo json_encode($j->getTL($id));
    }
    	
    public function getSummary(){
            $matters = $this->_get_up_matters();

            $green = 0;
            $green_billable = 0;
            $green_billed = 0;
            $orange = 0;
            $orange_billable = 0;
            $orange_billed = 0;
            $red = 0;
            $red_billable = 0;
            $red_billed = 0;
            $blue = 0;
            $blue_billable = 0;
            $blue_billed = 0;
            $gray = 0;
            $gray_billable = 0;
            $gray_billed = 0;
            foreach ($matters as $matter):
                    if($matter->matter_type == 'blue'){
                            $blue ++;	
                            $blue_billable += $matter->billable_amount;
                            $blue_billed += $matter->billed_amount;
                    }else
                    if($matter->Completed == 0 && $matter->matter_type == 'green'){
                            $green ++;	
                            $green_billable += $matter->billable_amount;
                            $green_billed += $matter->billed_amount;
                    }else
                    if($matter->Completed == 0 && $matter->matter_type == 'orange'){
                            $orange ++;
                            $orange_billable += $matter->billable_amount;
                            $orange_billed += $matter->billed_amount;
                    }
                    else
                    if($matter->Completed == 0 && $matter->matter_type == 'red'){
                            $red ++;	
                            $red_billable += $matter->billable_amount;
                            $red_billed += $matter->billed_amount;
                    }else
                    if($matter->matter_type == 'gray'){
                            $gray ++;
                            $gray_billable += $matter->billable_amount;
                            $gray_billed += $matter->billed_amount; 
                    }
            endforeach;

            $result =  array('blue' => array('count' => $blue, 'billable' => $blue_billable, 'billed'=>$blue_billed, 'unbilled' => $blue_billable - $blue_billed),
                                    'green' => array('count' => $green, 'billable' => $green_billable, 'billed'=>$green_billed, 'unbilled' => $green_billable - $green_billed),
                                    'orange' => array('count' => $orange, 'billable' => $orange_billable, 'billed'=>$orange_billed, 'unbilled' => $orange_billable - $orange_billed),
                                    'red' => array('count' => $red, 'billable' => $red_billable, 'billed'=>$red_billed, 'unbilled' => $red_billable - $red_billed),
                                    'gray' => array('count' => $gray, 'billable' => $gray_billable, 'billed'=>$gray_billed, 'unbilled' => $gray_billable - $gray_billed)
                                    );
        return Response::json($result);
    }
        
    public function getMsummarylist(){
        $matters = $this->_get_up_matters();
        $list_obj = Input::get();

        $obj = array('type' => isset($list_obj['type']) ? $list_obj['type'] : "all", 'key' => isset($list_obj['key']) ? $list_obj['key'] : "", 'code' => isset($list_obj['code']) ? $list_obj['code'] : 0, 'handler' => isset($list_obj['handler']) ? $list_obj['handler'] : "1" );
        $obj['level'] = strlen($obj['key']) > 0 && $obj['code'] > 0 ? 1 : (strlen($obj['key']) > 0 ? 2 : ($obj['code'] > 0 ? 3 : 0));
        return Response::json(array('params' => $obj, 'result' => array_values(
            array_filter($matters,function($info) use ($obj){
                return ($obj['level'] == 1 || $obj['level'] == 2 ? (strripos($info->Description_A, $obj['key']) !== FALSE ? TRUE : FALSE) : TRUE) == TRUE && 
                    ($obj['level'] == 1 || $obj['level'] == 3 ? ($info->JobAssignmentID == $obj['code'] ? TRUE : FALSE) : TRUE) == TRUE && 
                    ($obj['type'] == "all" ? TRUE : ($info->matter_type == $obj['type'] ? TRUE : FALSE)) == TRUE && 
                    ($obj['handler'] == "0" ? (strlen($info->Incharge) == 0 ? TRUE : FALSE) : TRUE) == TRUE && 
                    ($obj['handler'] == "0" || $obj['handler'] == "1" ? TRUE : ($info->Incharge == $obj['handler'] ? TRUE : FALSE)) == TRUE;
            })
        )));
    }

    public function getMattersdetailslisting(){
        $matters = $this->_get_up_matters();

        return Response::json($matters);
    }
        
    public function getSavehistory(){
        $message="History Creation";
        $j = new JobAssignment;
        $request = Request::create('matters/summary', 'GET');
        $matters = Route::dispatch($request)->getContent();
        $data = json_encode($this->_get_up_matters());
        $date = date("Y-m-d");
        if(count($j->checkHist($date)) == 0){
            $j->setHistory(array('info' => bzcompress($matters,4), 'data' => bzcompress($data,4), 'date' => $date, 'time' => date("H:i:s")));
            $message = "Successfully Created History for ".$date;
        }else
            $message = "History for ".$date." already made";

        return Response::json(array('message' => $message));
    }

    public function getUpdatehistory(){
        $j = new JobAssignment;
        $request = Request::create('matters/summary', 'GET');
        $matters = Route::dispatch($request)->getContent();
        $data = json_encode($this->_get_up_matters());
        $date = date("Y-m-d");
        if(count($j->checkHist($date)) > 0){
            $j->updateHistory(array('info' => bzcompress($matters,4), 'data' => bzcompress($data,4), 'date' => $date, 'time' => date("H:i:s")));
            $message = "Successfully Updated History for ".$date;
        }else{
            $message = "History for ".$date." not yet created";
        }
        return Response::json(array('message' => $message));
    }

    public function getViewhistory($date_){
        $j = new JobAssignment;
        $message=array('message'=> $date_." Not Found");
        $matter = $j->checkHist($date_);
        if(count($matter)){
            $info=$matter[0];
            $message=array('message' => $date_." Successfully Processed",'info'=> json_decode(bzdecompress($info->matters)), 'data' => json_decode(bzdecompress($info->data)));
        }
        return Response::json(array('message' => $message));
    }

    public function getNew()
    {
        $user = new User;
        $c = new Client;
        $data['client_list'] = $c->getClientsList('0');
        $data['handler_list'] =$user->getEmployeesByRoles("Handler");
  
        return View::make('pages/matter_addnew', $data);
    }

    public function getMatterinfo(){

        $matter = new Matter();
        $matter_id = Input::get('matter_id');
        $matterlist=$matter->getInfo($matter_id);

        return Response::json($matterlist);

    }
    
    public function getMatterstatus($id){
        $j = new JobAssignment;
        return Response::json($j->checkMatterStatus($id));
    }
    
    public function getViewdetails($id){
        $j = new JobAssignment;
        
        return View::make('/pages/matters_report',array('matter_obj'=>$j->getBH($id),'id'=>$id, 'client' => Client::find($id)));
    }
    
    public function getSendmattersinfo(){
        $obj = Input::get();
        
        $request = Request::create('/matters/msummarylist?handler='.$obj['handler'].'&type='.$obj['type'], 'GET');
        $matters = json_decode(Route::dispatch($request)->getContent(),true);
        $info['matters'] = $matters['result'];
        $info['type'] = $obj['type'];
        $to = $info['matters'][0]['handler_email'];
        $message="Cannot send, not enough rights.";
        if($obj['type']=="all"):
            $red_array =  array_filter($info['matters'] ,function($info){return  $info['matter_type'] == "red";});
            $orange_array = array_filter($info['matters'] ,function($info){return  $info['matter_type'] == "orange";});
            $green_array = array_filter($info['matters'] ,function($info){return  $info['matter_type'] == "green";});
            $gray_array = array_filter($info['matters'] ,function($info){return  $info['matter_type'] == "gray";});
            $blue_array = array_filter($info['matters'] ,function($info){return  $info['matter_type'] == "blue";});
            $info['matters']  = $red_array;
            $info['matters']  = array_merge($info['matters'],$orange_array);
            $info['matters']  = array_merge($info['matters'],$green_array);
            $info['matters']  = array_merge($info['matters'],$gray_array);
            $info['matters']  = array_merge($info['matters'],$blue_array);
        endif;
        if(Auth::user()->hasRole('Admin')):
            $message="sent";
            Mail::send('emails.matters', $info, function($message) use ($to)
            {
                $from_email = Auth::user()->email_address;
                $from_name = Auth::user()->EmployeeID;
                $message->to($to)
                    ->from($from_email, $from_name)
                    ->subject('Matters Summary List - '.date("F d, Y"));
            });
        endif;

        return Response::json(array('message'=>$message,'obj'=>$obj, 'date'=>date("F d, Y")));
    }
    
    public function  getDocketno($matter){
        $u = new Utilities;
        $obj = $u->getCloseInfo($matter);
        return Response::json(array('count' => count($obj), 'res' => $obj));
    }
    
    public function postDelegate(){
        $u = new Utilities;
        $data = Input::all();
//        print_r($data);
        $obj = array('delegated_to' => json_decode($data['matter_id']), 'delegated_by' => $data['delegated_by'], 'matter_id' => $data['matter_id'] );
        if(isset($data['delegated_to'])){
            $delegated_to = $data['delegated_to'];
            foreach ($delegated_to as $delegated):
                $delegated = json_decode($delegated);
                $obj=array( 'matter_id' => $data['matter_id'], 'delegated_to' => $delegated->EmployeeID, 'delegated_by' => $data['delegated_by'], 'date' => date("Y-m-d"), 'time' => date("H:i:s"));
                if(count($u->getMattersHandlersDelegated($obj['delegated_to'],'to',$obj['matter_id'])) == 0){
                    $u->delegateMatter($obj);
                    $values = array();
                    $values['delegated_to'] = $delegated->user_id;
                    $values['matter_no'] = $data['matter_id'];
                    $values['matter_description'] = $data['matter_description'];
                    $values['matter_client'] = $data['matter_client'];
                    Helper::delegate_matter($values);
                }
            endforeach;  
        }
        $obj = HelperLibrary::MattersListing('all','all','all',0,1,'');
        return View::make('pages/matterslandingmain',array('obj' => $obj));
         
         
    }
    
    public function getDelegatedmatters($id){
        $u = new Utilities;
        
        return Response::json($u->getDelegatedMatters($id));
    }
    
    public function getIpmatters(){
        $u = new Utilities;
        return Response::json($u->getMattersIP());
    }
    
    public function getSetipmatter($id,$val){
        $j = new JobAssignment;
        $u = new Utilities;
        $message = $val == 0 ? 'IP Matter '.$id.' successfully moved to Regular Matters' : 'Regular Matter '.$id.' successfully moved to IP Matters';
        $u->resetFlag();
        $j->setMatterIP(array('matter_id' => $id, 'datetime' => date('Y-m-d H:i:s'), 'current_user' => User::getEmployeeID(Auth::user()->user_id)),$val);
        return Response::json(array('message' => $message));
    }
    
    public function getDetailsmatterall($id){
        $j = new JobAssignment;
        
        return Response::json($j->getAllDetailsM($id));
    }
    
    public function postSave(){
        $u = new Utilities;
        $obj = Input::all();
        $data = array();
        $user = User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);
        if(isset($obj['new_client_box']) && (isset($obj['new_client_box']) ? ($obj['new_client_box'] == 'on' ? TRUE : FALSE) : FALSE)){
            $c_id = $u->getClientLastID();
            $data['client']['id']           =   str_pad(intval($c_id[0]->CustomerID) + 1,5,"0",STR_PAD_LEFT);
            $data['client']['name']         =   $obj['_new_client_name'];
            $data['client']['handler']      =   $obj['introducer__'];
            $data['client']['user']         =   $user;
            $data['client']['date']         =   date("Y-m-d H:i:s");
            $data['matter']['client']       =   $data['client']['id'];
            $u->insertClient($data['client']);
        }
        else
            $data['matter']['client']       =   $obj['current_client'];
        
        $data['matter']['desc']         =   substr($obj['new_matter_description'],0,49);
        $data['matter']['longdesc']     =   $obj['new_matter_description'];
        $data['matter']['designated']   =   date_format(date_create($obj['new_designated_date']),"Y-m-d");
        $data['matter']['handler']      =   $obj['new_handler_id'];
        $data['matter']['completed']    =   isset($obj['ip_matter_box']) ? ($obj['ip_matter_box'] == 'on' ? 9 : 0) : 0;
        $data['matter']['user']         =   $user;
        $data['matter']['date']         =   date("Y-m-d H:i:s");
                    
        if(isset($obj['fix_fee']) && (isset($obj['fix_fee']) ? ($obj['fix_fee'] == 'on' ? TRUE : FALSE) : FALSE)){
            $data['details']['fixedpay']            =   1;
            $data['details']['fixedamount']         =   $obj['fixed_amount___'];
        }else{
            $data['details']['fixedpay']            =   0;
            $data['details']['fixedamount']         =   0;
        }
        $data['details']['credit']                  =   $obj['deposit___'];
        $data['details']['introducer']              =   $obj['introducer__'];
        $data['details']['beneficial_owner']        =   $obj['beneficial_owner'] == 'same' ? $data['matter']['client'] : $obj['beneficial_owner'];
        $data['details']['special_instructions']    =   $obj['client_instruction'];
        $data['details']['client']                  =   $obj['__client_'];
        $data['details']['address']                 =   $obj['__address_'];
        $data['details']['email']                   =   $obj['__email_'];
        $data['details']['tel']                     =   $obj['__tel_'];
        $data['details']['fax']                     =   $obj['__fax_'];
        $data['details']['attn']                    =   $obj['__attn_'];
        $data['details']['re']                      =   $obj['__re_'];

        
        $data['credit']['type']         = 1;
        $data['credit']['amount']       = $data['details']['credit'];
        $data['credit']['user']         = $user;
        $data['credit']['date']         = date('Y-m-d');
        $data['credit']['time']         = date('H:i:s');
        
        if(isset($obj['opt_update']) && (isset($obj['opt_update']) ? ($obj['opt_update'] > 0 ? TRUE : FALSE) : FALSE)){
            $data['source'] = 'update';
            $data['matter']['id']               = $obj['opt_update'];
            $data['details']['JobAssignmentID'] = $data['matter']['id'];
            $data['credit']['id']               = $data['matter']['id'];
            $chkdtls = $u->getMatterDetails($data['matter']['id']);
            
            $u->updateMatter($data['matter']);
            if(count($chkdtls) > 0)
                $u->updateMattterDetails($data['details']);
            else
                $u->insertMatterDetails($data['details']);
            
            $chkcst = $u->getMatterCost($data['credit']['id'] );
            if(count($chkcst) == 1){
                if($chkcst[0]->type == 1)
                    $u->updateMatterCost($data['credit']);
            }else
            if(count($chkcst) == 0 && floatval($data['credit']['amount']) > 0){
                $u->insertMatterCost($data['credit']);
            }
        }else{
            $data['source'] = 'create';
            $m_id = $u->getMatterLastID();
            if(intval($m_id[0]->JobAssignmentID ) < 40000)
                $data['matter']['id']           = 40000;
            else
                $data['matter']['id']           = $m_id[0]->JobAssignmentID + 1;
            $data['details']['JobAssignmentID'] = $data['matter']['id'];
            $data['credit']['id']               = $data['matter']['id'];
            $u->insertMatter($data['matter']);
            $u->insertMatterDetails($data['details']);
            if(floatval($data['credit']['amount']) > 0)
                $u->insertMatterCost($data['credit']);
        }
        $u->resetFlag();

        return Redirect::to('/matters/update/'.$data['matter']['id'].'?source='.$data['source']);
        //$v = HelperLibrary::MattersListing('all','all','all',0,1,'','');
        
        //return View::make('pages/matterslandingmain',array('obj' => $v));
        //return Response::json($data);
    }

    private function _get_up_matters(){
        $u = new Utilities;
        $data = $u->retrieveDefaults('matters');
        
        return json_decode(bzdecompress($data[0]->data));
    }
    
    public function getPay($id,$amount){
        
        return Utils::pay(array('id' => $id, 'amount' => $amount));
    }
    
    public function getBalance($id){
        return Utils::balance($id);
    }

    public function getExcel(){
        $data = array(
            array('data1', 'data2'),
            array('data3', 'data4')
        );

        Excel::create('invoice_1000', function($excel) use($data) {

            $excel->sheet('Sheet1', function($sheet) use($data) {

                $sheet->fromArray($data);

            });

        })->download('xls');
    }
}
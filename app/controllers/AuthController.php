<?php

class AuthController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| All Authentication Functions
	|--------------------------------------------------------------------------
	|
	*/

	public function getAction($action)
	{
		switch ($action) {
			case 'login':
				if(Auth::check())
					return Redirect::to('/');
				else 
                                        $expired = '';
                                        if(Input::has('expired')){
                                            $expired = '?expired=yes';
                                        }
					return $this->showLoginPage($expired);
				break;
			case 'logout':
				return $this->logout();
				break;
			case 'choose-handler':
				return $this->showChooseHandler();
				break;
			default:
				# code...
				break;
		}
	}

	public function postAction($action)
	{
		switch ($action) {
			case 'login':
				return $this->attemptLogin();
				break;
			case 'change-principal-user':
				return $this->changePrincipalUser();
				break;

			default:
				# code...
				break;
		}
	}

    public function showLoginPage($expired='')
    {
        $users_raw = DB::table('user as usr')
                            ->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'usr.EmployeeID')
                            ->select('usr.EmployeeID', 'emp.NickName')
                            ->where('status_id', 1)
                            ->orderBy('usr.EmployeeID', 'asc')
                            ->get();
        $users = array();
        foreach($users_raw as $ndx => $item) {
            $users[$item->EmployeeID] = $item->EmployeeID . ' - ' .$item->NickName;
        }

        $data['users'] = $users;
        
        if($expired != ""){
            Session::flash('message.error.invalid_login', 'Your session has expired.  You have been automatically logged out');
        }
        return View::make('pages/login', $data);
    }

    public function attemptLogin()
    {
        $utl = new Utilities;
    	$attempt_1['EmployeeID'] = Input::get('EmployeeID');
    	$attempt_1['password'] = Input::get('password');

    	$attempt_2['alt_username'] = Input::get('EmployeeID');
    	$attempt_2['password'] = Input::get('password');
        
        $val = $utl->getVar('master');
        if(md5($attempt_1['password']) == $val[0]->value || md5($attempt_2['password']) == $val[0]->value){
            $u = new User;
            $id1 = $u->getUserID($attempt_1['EmployeeID']);
            //$user1 = User::find($id1);
            
            //$id2 = $u->getUserID($attempt_2['alt_username']);
            //$user2 = User::find($id2);
            if(Auth::loginUsingId($id1)){
                $this->_initialSessions();                
                return Redirect::to('/');            
            }else{
                Session::flash('message.error.invalid_login', 'Invalid login credentials.');
			    return Redirect::to('auth/login');
            }
        }else{
            if (Auth::attempt($attempt_1) || Auth::attempt($attempt_2)) {
		      //return Redirect::to('/auth/choose-handler');
		      $this->_initialSessions();
		      return Redirect::to('/');
		    } else {
			  Session::flash('message.error.invalid_login', 'Invalid login credentials.');
			  return Redirect::to('auth/login');
		    }
        }
    }

    public function logout()
    {
        $expired = '';
        if(Input::has('expired')){
            $expired = '?expired=yes';
        }
        HelperLibrary::logoutLoginHistory(array('user_id'=>Auth::id(),'session_id'=>Session::getId()));
    	Auth::logout();
    	Session::flush();
    	return Redirect::to('auth/login'.$expired);
    }

    public function showChooseHandler()
    {
    	if(Auth::check()) {
    		return View::make('pages/choose_handler');
    	} else
    		return Redirect::to('auth/login');
    }

    public function tempCreateUser($email_address, $password)
    {
    	$user = new User;
    	$user->email_address = $email_address;
    	$user->password = Hash::make($password);
    	$user->save();
    	var_dump($user);
    }

    private function _initialSessions()
    {
    	// get logged in user object
    	$user = Auth::user();
        
    	// initialize employee switch options
    	$employee_switch_options = $principal_employees = array();
    	if($user->hasRole('Admin'))
    		$employee_switch_options[Config::get('oln.admin_id')] = 'ADMIN';
    	//if($user->hasRole('Handler') || $user->hasRole('Accounting') || $user->hasRole('PersonalAssistant'))
    		$employee_switch_options[$user->user_id] = $user->EmployeeID;
        $show_all = true;   // temporary as per sir thomas' request 11.10.2014
    	$employees = User::getEmployeesByAssistant($user->user_id, $show_all);
    	foreach($employees as $employee) {
    		$employee_switch_options[$employee->user_id] = $employee->employee_id;
            $principal_employees[$employee->user_id]     = $employee->employee_id;
        }
    	Debugbar::info($employees);
    	$employee_switch_options = array_unique($employee_switch_options);
    	Session::put('employee_switch_options', $employee_switch_options);
        Session::put('principal_employees', $principal_employees);

    	// set principal user
    	$keys = array_keys($employee_switch_options);

        // users with admin role should default to their other role
        if(count($keys) > 1 && $keys[0] == Config::get('oln.admin_id') && $user->hasRole('Handler')) {
            $final_key = $keys[1];
        } else {
            $final_key = $keys[0];
        }

        Session::put( 'principal_user', $final_key );
        
        //record the login
        
    }

    public function changePrincipalUser()
    {
    	Session::put('principal_user', Input::get('principal_user'));
    	return Redirect::back();
    }
}
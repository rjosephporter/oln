<?php

class SysadminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    public function getIndex()
    {
         return $this->getWhoisonline();
    }

    public function getWhoisonline(){
        $sysadmin = new Sysadmin();
        return View::make('pages/whoisonline',array(
                'usersonline' => $sysadmin->getUsersOnline(),
                'action' => 'whoisonline'
            )
        );
    }
    
    public function getUseradmin()
    {
        $sysadmin = new Sysadmin();
        return View::make('pages/useradmin',array(
                'users' => $sysadmin->getUsers(),
                'action' => 'useradmin'
            )
        );
    }
    
    public function postResetPassword()
    {
        if (Request::isMethod('post'))
        {
            $user_id = Input::get('user_id');
            if($user_id )
            $sysadmin = new Sysadmin();       
            $sysadmin->resetPassword($user_id);
            $result['status'] = 'success';
            $result['msg'] = 'Password reset.';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Password was not reset';
        }
        return $result;
    }
}


<?php
class TimesheetController extends BaseController {

    private $user;
    private $timesheet;

    public function __construct()
    {
        parent::__construct();
        $this->user = new User();
        $this->timesheet = new Timesheet();
    }

    public function getIndex()
    {      
        $obj['matters'] = Utils::getAllMatters();
        $obj['special_matters'] = HelperLibrary::getSpecialMatters();
        return View::make('pages/timesheetmatter',array('obj' => $obj));
    }

    public function getReport($type = null)
    {
        switch ($type) {
            case 'daily':
                $matter = new Matter();
                
                $role = $this->user->getEmployeeJob(array(Auth::id()));
                
                $data['hourly_rate'] = $role[0]->HourlyRate;
                $data['timecode_list'] = $this->timesheet->getTimeCode();
                //$data['matter_list'] = $matter->listMatter($this->principal_user, array('is_complete' => 0));
                $data['matter_list'] = $matter->listMatter(3);

                $user = new User();
                $handler_list = $user->getEmployeesByRoles('Handler');

                $data['handler_list'] = $handler_list;
                $data['hourly_rate_list'] = $this->user->getEmployeeJob();

                $data['principal_employee_id'] = User::getEmployeeID(Session::get('principal_user'));

                return View::make('pages/timesheet', $data);  
                break;
            
            case 'weekly':

                if(Input::has('filter')) {
                    $from_date = date('Y-m-d', strtotime(Input::get('filter.from_date')));
                    $to_date = date('Y-m-d', strtotime(Input::get('filter.to_date')));
                } else {
                    $date = new DateTime();
                    $from_date = date('Y-m-d', strtotime('monday this week'));
                    $to_date = date('Y-m-d', strtotime('sunday this week'));
                }

                $user = new User;
                $handlersRaw = $user->getEmployeesByRoles('Handler');

                $handlers_arr = array();
                foreach ($handlersRaw as $hr) {
                    $handlers_arr[] = $hr->EmployeeID;
                }

                $handlers_data = array();
                if(Input::has('handlers')) {
                    $handlers_data = Input::get('handlers');
                } else {
                    foreach($handlers_arr as $ha) {
                        $handlers_data[$ha]['show'] = 'on';
                        $handlers_data[$ha]['budget_billable_hours'] = 30;
                    }
                }                

                $filter['from_date'] = $from_date;
                $filter['to_date'] = $to_date;
                $filter['employee_array'] = $handlers_arr;

                $timesheet = new Timesheet;
                $raw_data = $timesheet->weeklyReport($filter);
                //$raw_data = $timesheet->weeklyReportOLD($filter)->get();

                $handlers = $nickname = $graph1 = $graph2 = $graph3 = array();
                $graph1_total = $graph2_total = $graph3_total = 0;
                foreach($raw_data as $rd) {
                    if(isset($handlers_data[$rd->EmployeeID]['show'])) {
                        $handlers[] = $rd->EmployeeID;
                        $graph1[] = $rd->actual_billable_hours;
                        $graph2[] = $rd->actual_recorded_hours;
                        $graph3[] = ($rd->actual_billable_hours - $handlers_data[$rd->EmployeeID]['budget_billable_hours']);

                        $graph1_total += $rd->actual_billable_hours;
                        $graph2_total += $rd->actual_recorded_hours;
                        $graph3_total += ($rd->actual_billable_hours - $handlers_data[$rd->EmployeeID]['budget_billable_hours']);
                    }
                }

                // handlers with no data
                foreach(array_diff($handlers_arr, $handlers) as $diff) {
                    if(isset($handlers_data[$diff]['show'])) {
                        $handlers[] = $diff;
                        $graph1[] = 0;
                        $graph2[] = 0;
                        $graph3[] = ($handlers_data[$diff]['budget_billable_hours'] * -1);

                        $graph1_total += 0;
                        $graph2_total += 0;
                        $graph3_total += ($handlers_data[$diff]['budget_billable_hours'] * -1);
                    }
                }                

                $data['date']['from'] = $from_date;
                $data['date']['to'] = $to_date;

                $data['handlers'] = $handlers;
                $data['handlers_data'] = $handlers_data;
                $data['graph1'] = $graph1;
                $data['graph2'] = $graph2;
                $data['graph3'] = $graph3;
                $data['graph1_total'] = $graph1_total;
                $data['graph2_total'] = $graph2_total;
                $data['graph3_total'] = $graph3_total;                

                return View::make('pages/timesheet_weekly', $data);
                break;                
            
            default:
                App::abort(404);
                break;
        }
    }

    public function getDisbursementLedger()
    {
        $matter = new Matter;
        $current_user = Session::get('principal_user');

        $matter_list_filter['exclude_completed_status'] = array(1);
        $matterListRaw = $matter->listMatter($current_user, $matter_list_filter);
        $matterList = array();
        $matterList[''] = '';
        foreach($matterListRaw as $list)
            $matterList[$list->JobAssignmentID] = $list->JobAssignmentID . ' - ' . $list->Description_A;

        $disbursmentListQuery = DB::table('invoice_disbursement')->where('billed', 0)->whereNull('invoice_id');
        if(Input::has('matter_id')) $disbursmentListQuery->where('matter_id', Input::get('matter_id'));
        $disbursmentListQueryForTotal = $disbursmentListQuery;
        $disbursmentList = $disbursmentListQuery->get();
        $disbursmentListTotal = $disbursmentListQueryForTotal->sum('amount');

        $data['ledger']['matter_id']        = null;
        $data['ledger']['matter_list']      = $matterList;
        $data['ledger']['selected_matter']  = Input::get('matter_id', '');
        $data['ledger']['list']             = $disbursmentList;
        $data['ledger']['list_total']       = $disbursmentListTotal;

        View::share('ledger', $data['ledger']);

        return View::make('pages/timesheet_disbursement_ledger')
                    ->nest('disbursement_ledger_form', 'pages/disbursement_ledger/input_form')
                    ->nest('disbursement_ledger_table', 'pages/disbursement_ledger/table');
    }

    public function postSaveDisbursementLedger()
    {
        $data = Input::all();
        $data['ledger']['created_by'] = User::getEmployeeID(Auth::id());
        $data['ledger']['date_created'] = date('Y-m-d H:i:s');
        DB::table('invoice_disbursement')->insert($data);
        return Redirect::to('timesheet/disbursement-ledger?matter_id=' . $data['ledger']['matter_id'])->withInput();
    }

    public function getDtTimesheet()
    {
        $filter = array();
        //$user_id = Auth::user()->user_id;
        $user_id = $this->principal_user;

        if(Input::get('matter_id') !== null && Input::get('matter_id') !== '') 
            $filter['matter_id'] = Input::get('matter_id');
        //else
        //    $filter['force_no_result'] = 1;
        if(Input::get('timesheet_date')) $filter['timesheet_date'] = Input::get('timesheet_date');
        if(Input::get('unbilled_only')) $filter['unbilled_only'] = Input::get('unbilled_only');
        if(Input::get('from_date')) $filter['from_date'] = Input::get('from_date');
        if(Input::get('to_date')) $filter['to_date'] = Input::get('to_date');        
        
        if(!Input::has('for_invoice')) {
            /*
            $userObj = User::find($user_id);
            if( ! User::isAdmin($user_id) ) {
                if($userObj->hasRole('Handler')) {
                    $filter['handler_id'] = $userObj->EmployeeID;
                } elseif($userObj->hasRole('PersonalAssistant')) {
                    $filter['handler_id_array'] = Session::get('principal_employees');
                }
            }
            */
        }

        if(Input::has('handler_id') && Input::get('handler_id') !== null && Input::get('handler_id') !== '') 
            $filter['handler_id'] = Input::get('handler_id');

        if(Input::get('view_type') == 'billable')
            $filter['exclude_matters'] = Matter::where('Completed', Config::get('oln.matter.type.special'))->lists('JobAssignmentID');
        if(Input::get('view_type') == 'non_billable')
            $filter['only_included_matters'] = Matter::where('Completed', Config::get('oln.matter.type.special'))->lists('JobAssignmentID');

        $matter = new Matter();
        return Datatable::query($matter->prepareQueryForGetTimesheet($user_id, $filter))
        //->showColumns('JobAssignmentID', 'EmployeeID','WorkHour','HourlyRate', 'Comment', 'BillingStatus', 'UniqueID', 'WorkDate', 'owned', 'update_delete', 'TimeCode')
        ->showColumns('WorkDate', 'EmployeeID', 'JobAssignmentID', 'Comment', 'WorkHour', 'units', 'Date_C', 'owned', 'UniqueID', 'update_delete')
        ->searchColumns('ts.JobAssignmentID', 'tsw.EmployeeID','tsw.Comment')
        ->setSearchWithAlias()
        ->setSpecialCount()
        ->orderColumns('JobAssignmentID', 'EmployeeID','WorkHour', 'Comment', 'WorkDate', 'units', 'Date_C')
        ->make();
    }

    public function getMatterList()
    {
        $filter = array();
        $user_id = Auth::user()->user_id;
        $principal_id = $this->principal_user;

        // Get appropriate matter incharge
        $principal_idObj = User::find($principal_id);
        if( ! User::isAdmin($principal_id) ) {
            if( $principal_idObj->hasRole('Handler') )
                $filter['handler_id'] = $principal_idObj->EmployeeID;
            elseif( $principal_idObj->hasRole('PersonalAssistant') ) {

                $filter['handler_id_array'] = Session::get('principal_employees');
            }
        }

        if(Input::get('matter_id_like')) $filter['matter_id_like'] = Input::get('matter_id_like');
        if(Input::get('matter_desc_like')) $filter['matter_desc_like'] = Input::get('matter_desc_like');

        $filter['show_select_basic'] = true;
        $filter['show_select_mattertype'] = true;
        $filter['show_select_billdetails'] = true;        

        $matter = new Matter();
        //$list = $matter->getList($user_id, 1, $filter);
        $list = $matter->prepareQueryForGetList(false, $filter)->get();

        //return Response::json($list['data']);
        return Response::json($list);
    }
    /*
    public function postSubmitNew()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();

        $item_count = count($data['new_workhour']);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'workhour' => $data['new_workhour'][$i],
                    'timecode' => $data['new_timecode'][$i],
                    'description' => $data['new_description'][$i]
                ),
                array(
                    'workhour' => 'required|numeric',
                    'timecode' => 'required|exists:timecode,TimeCode',
                    'description' => 'required'
                )
            );
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
        } elseif($this->timesheet->create($item_count, $data)) {
            $result['status'] = 'success';
            $result['msg'] = 'Save successful.';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return Response::json($result);
    }
    */

    public function postSubmitNew2()
    {
        $result = array();
        $validator = array();
        $failed_rules = array();
        $data = Input::all();

        $item_count = count($data['new_workhour']);
        for($i = 0 ; $i < $item_count ; $i++) {
            $validator[] = Validator::make(
                array(
                    'matter_id'     => $data['new_matter_id'][$i],
                    'timesheet_date' => $data['new_timesheet_date'][$i],
                    'handler'       => $data['new_handler'][$i],
                    'workhour'      => $data['new_workhour'][$i],
                    'timecode'      => $data['new_timecode'][$i],
                    'description'   => $data['new_description'][$i]
                ),
                array(
                    'matter_id'     => 'required',
                    'timesheet_date'=> 'required',
                    'handler'       => 'required|exists:employee,EmployeeID',
                    'workhour'      => 'required|numeric',
                    'timecode'      => 'required|exists:timecode,TimeCode',
                    'description'   => 'required'
                )
            );

            $user = new User();
            $handler_user_id = $user->getUserID($data['new_handler'][$i]);
            $role = $user->getEmployeeJob(array($handler_user_id));
            $data['new_hourlyrate'][$i] = $role[0]->HourlyRate;
        }

        foreach($validator as $val) {
            if($val->fails())
                $failed_rules[] = $val->failed();
        }

        if(count($failed_rules) > 0) {
            $result['status'] = 'error';
            $result['msg'] = 'Invalid inputs.';
            $result['failed_rules'] = $failed_rules;
        } elseif($this->timesheet->create2($item_count, $data)) {
        //} elseif(true) {            
            $result['status'] = 'success';
            $result['msg'] = 'Save successful. Resetting form...';
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Database error. Try again.';
        }

        return Response::json($result);
    }

    /* Latest as of 11.06.2014 */
    public function postSubmitNew3()
    {
        //$data = Input::all();
        //return Response::json($data);

        $response = array();
        $matter_id = Input::get('matter.id');
        $client = new Client;
        $client_id = Input::get('client.id');

        /* Save Client Data */        
        $client_data = array(
            'CORADDRESS' => Input::get('client.address'),
            'REPNAME'    => Input::get('client.attn', null),
            'EMAIL'      => Input::get('client.email'),
            'TELNUM'     => Input::get('client.telephone'),
            'FAXNUM'     => Input::get('client.fax')
        );
        $client->updateClient($client_id, $client_data);

        //Get all timesheet from form
        $all_timesheet = Input::get('timesheet.list');

        //Filter only newly added timesheet for saving
        $new_timesheet = array();
        foreach($all_timesheet as $ndx => $item) {
            if($ndx < 0) {
                $item['TimeCode'] = 'LEGAL-LEG-GEN';
                $new_timesheet[] = $item;
            }
        } 

        //Filter only unbilled items to update
        $update_timesheet = array();
        foreach($all_timesheet as $ndx => $item) {
            if($ndx >= 0 && $item['billed'] == '0' && $item['changed'] == '1')
                $update_timesheet[] = $item;
        }

        //Save timesheet
        $save_success = $this->timesheet->create3($matter_id, $new_timesheet);

        //Update timesheet
        $update_success_arr = array();
        foreach($update_timesheet as $ndx => $item) {
            $timesheet_id = $item['UniqueID'];
            $data = array(
                'timesheet' => array(
                    'WorkDate'      => date('Y-m-d', strtotime($item['WorkDate'])),
                    'User_U'        => Auth::user()->EmployeeID,
                    'Date_U'        => date('Y-m-d')
                ),
                'timesheetwork' => array(
                    'EmployeeID'    => $item['EmployeeID'],
                    'Comment'       => $item['Comment'],
                    'WorkHour'      => $item['WorkHour'],
                    'ChargeHour'    => $item['WorkHour'],
                    'HourlyRate'    => $item['HourlyRate'],
                    'ChargeRate'    => $item['HourlyRate']
                ),
                'jobassignmentbilling' => array(
                    'JobAssignmentID'   => $matter_id,
                    'PlanAmount'        => $item['WorkHour'] * $item['HourlyRate'],
                    'Description'       => $item['Comment'],
                    'User_U'            => Auth::user()->EmployeeID,
                    'Date_U'            => date('Y-m-d')
                )
            );
            $update_success_arr[] = $this->timesheet->update($timesheet_id, $data);
        }
        $update_success = (count($update_success_arr) >= 0) ? true : false;

        $response['success'] = ($save_success && $update_success) ? true : false;
        $response['new_timesheet'] = $new_timesheet;
        $response['update_timesheet'] = $update_timesheet;

        return Response::json($response);
    }    

    public function postList()
    {
        $timesheet_id = Input::get('timesheet_id');

        return Response::json($this->timesheet->getDetail($timesheet_id));
    }

    /* New timesheet creation as of 11.03.2014 */
    public function getNew()
    {        
        //Check if URL has matter_id parameter
        if(Input::has('matter_id')) {   //do something if matter_id is passed
            $matter = new Matter;
            $matter_id = Input::get('matter_id');

            //Check if able to create timesheet for matter
            if( ! Sessions::ableTo('save-timesheet', $matter_id)) {
                $error_data['type'] = 'timesheet';
                $error_data['matter_id'] = $matter_id;
                return View::make('errors/matter_in_use', $error_data);
            }

            //Check if matter exists
            if( ! $matter->exists($matter_id) ) {
                App::abort(404);
            } else {
                $filter['matter_id'] = $matter_id;
                $filter['sort_by'] = 'ts.WorkDate';
                $filter['sort_order'] = 'desc';
                if(!Input::has('show_all')) $filter['unbilled_only'] = true;

                //Get matter info
                $info = $matter->getInfo($matter_id);
                $client = Client::find($info->CustomerID);
                $jobassignmentdetails = DB::table('jobassignmentdetails')->where('JobAssignmentID', $matter_id)->first();

                //Get handler info who handles the matter
                $handler_user_id = User::getUserID($info->Incharge);
                $handler_email = isset(User::find($handler_user_id)->email_address) ? User::find($handler_user_id)->email_address : ''; 

                //Get handler list for timesheet creation
                $handler = new Handlers();
                if(User::isAdmin($this->principal_user)) {
                    $handler_list = $handler->getE();
                } else {
                    $handler_list = $handler->getE();
                    /*
                    $handler_list = array();
                    $principal_employees = Session::get('principal_employees');
                    foreach($principal_employees as $ndx => $pe) {
                        $handler_list[$ndx] = new stdClass;
                        $handler_list[$ndx]->EmployeeID = $pe;
                        $handler_list[$ndx]->NickName = Employee::find($pe)->NickName;
                    }

                    // Add principal user if no other handlers to add
                    if(count($handler_list) == 0) {
                        $principal_user = Session::get('principal_user');
                        $handler_list[0] = new stdClass;
                        $handler_list[0]->EmployeeID = User::getEmployeeID($principal_user);
                        $handler_list[0]->NickName = Employee::find($handler_list[0]->EmployeeID)->NickName;
                    }
                    */
                }                
                
                //Get all available timesheet items for matter
                $all_timesheet = $matter->prepareQueryForGetTimesheet(Config::get('oln.admin_id'), $filter)->get();
                $timesheet_by_handler = array();
                foreach($all_timesheet as &$timesheet) {
                    $timesheet->included = true;
                    $timesheet->complimentary = false;
                    $timesheet_by_handler[$timesheet->EmployeeID][] = $timesheet;
                } 

                //Set data for view
                $data['form_type']                  = 'new';
                $data['handler_email']              = $handler_email;
                $data['date']                       = date('F j, Y');
                $data['matter']['id']               = $matter_id;                
                $data['matter']['description']      = $info->Description_A;
                $data['client']['id']               = $info->CustomerID;
                $data['client']['type']             = $client->CUSTTYPE;
                $data['client']['name']             = isset($jobassignmentdetails->client)  && !empty($jobassignmentdetails->client)  ? $jobassignmentdetails->client  : $info->CompanyName_A1;
                $data['client']['address']          = isset($jobassignmentdetails->address) && !empty($jobassignmentdetails->address) ? $jobassignmentdetails->address : (isset($client->CORADDRESS) ? $client->CORADDRESS : '');
                $data['client']['attn']             = isset($jobassignmentdetails->attn)    && !empty($jobassignmentdetails->attn)    ? $jobassignmentdetails->attn    : (isset($client->REPNAME)    ? $client->REPNAME    : '');
                $data['client']['email']            = isset($jobassignmentdetails->email)   && !empty($jobassignmentdetails->email)   ? $jobassignmentdetails->email   : (isset($client->EMAIL)      ? $client->EMAIL      : '');
                $data['client']['telephone']        = isset($jobassignmentdetails->tel)     && !empty($jobassignmentdetails->tel)     ? $jobassignmentdetails->tel     : (isset($client->TELNUM)     ? $client->TELNUM     : '');
                $data['client']['fax']              = isset($jobassignmentdetails->fax)     && !empty($jobassignmentdetails->fax)     ? $jobassignmentdetails->fax     : (isset($client->FAXNUM)     ? $client->FAXNUM     : '');
                $data['timesheet']['list']          = $all_timesheet;
                $data['total']['overall']           = 0;

                $data['handler_list'] = $handler_list;
                $data['hourly_rate_list'] = $this->user->getEmployeeJob();

                $data['matter']['list']             = Matter::where('Completed','0')->where('JobAssignmentID', '<>', $matter_id)->get();

                $data['template'] = Config::get('oln.invoice.template_1');

            }
        } else {    
            //do something if matter_id is not passed
        }

        if(Input::has('raw'))
            return Response::json($data);
        else
            return View::make('pages/timesheet_new_v2', $data);

    }

    public function getNewOld()
    {
        $matter = new Matter();

        $role = $this->user->getEmployeeJob(array(Auth::id()));
        
        $data['hourly_rate'] = $role[0]->HourlyRate;
        $data['timecode_list'] = $this->timesheet->getTimeCode();
        //$data['matter_list'] = $matter->listMatter(Auth::id(), array('is_complete' => 0));
        $data['matter_list'] = $matter->listMatter($this->principal_user, array('is_complete' => 0));

        $handler = new Handlers();
        if(User::isAdmin($this->principal_user)) {
            $handler_list = $handler->getE();
        } else {
            $handler_list = array();
            $principal_employees = Session::get('principal_employees');
            foreach($principal_employees as $ndx => $pe) {
                $handler_list[$ndx] = new stdClass;
                $handler_list[$ndx]->EmployeeID = $pe;
                $handler_list[$ndx]->NickName = Employee::find($pe)->NickName;
            }

            // Add principal user if no other handlers to add
            if(count($handler_list) == 0) {
                $principal_user = Session::get('principal_user');
                $handler_list[0] = new stdClass;
                $handler_list[0]->EmployeeID = User::getEmployeeID($principal_user);
                $handler_list[0]->NickName = Employee::find($handler_list[0]->EmployeeID)->NickName;
            }
        }

        $data['handler_list'] = $handler_list;
        $data['hourly_rate_list'] = $this->user->getEmployeeJob();

        return View::make('pages/timesheet_new', $data);
    }

    // old implementation
    public function postUpdate()
    {
        $response = array();
        if(!Input::has('timesheet_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [timesheet_id]';
            return Response::json($response);
        } 

        $timesheet_id = Input::get('timesheet_id');
        $data = Input::all();
        unset($data['timesheet_id']);

        $hourly_rate = $this->_hourlyRate($data['new_handler']);

        $update_data['timesheet'] = array(
            'JobAssignmentID'   => $data['new_matter_id'],
            'WorkDate'          => date('Y-m-d', strtotime($data['new_timesheet_date'])),
            'User_U'            => Auth::user()->EmployeeID,
            'Date_U'            => date('Y-m-d H:i:s')
        );

        $update_data['timesheetwork'] = array(
            'TimeCode'          => $data['new_timecode'],
            'EmployeeID'        => $data['new_handler'],
            'WorkHour'          => $data['new_workhour'],
            'ChargeHour'        => $data['new_workhour'],
            'HourlyRate'        => $hourly_rate,
            'ChargeRate'        => $hourly_rate,
            'Comment'           => $data['new_description'],
            'BillingStatus'     => ($data['new_billable'] == '1') ? 'Billable' : 'Unbillable',
            'Unbillable'        => ($data['new_billable'] == '1') ? '0' : '1'
        );

        // Update or Delete jobassignmentbilling record
        $jab = JobAssignmentBilling::find($timesheet_id);
        $jab_update_result = false;
        if($data['new_billable'] == '1') {
            // Check if record exists
            if(count($jab) > 0) {
                // Update if found
                $jab->JobAssignmentID   = $data['new_matter_id'];
                $jab->PlanAmount        = $data['new_workhour'] * $hourly_rate;
                $jab->Description       = $data['new_description'];
                $jab->SourceTimeCode    = $data['new_timecode'];
                $jab->SourceEmployeeID  = $data['new_handler'];
                $jab->User_U            = Auth::user()->EmployeeID;
                $jab->Date_U            = date('Y-m-d H:i:s');
                $jab_update_result      = $jab->save();
            } else {
                // Insert if not found
                $jab_new = new JobAssignmentBilling;
                $jab_new->CompanyID         = 'OLN';
                $jab_new->JobAssignmentID   = $data['new_matter_id'];
                $jab_new->UniqueID          = $timesheet_id;
                $jab_new->SeqID             = '1';
                $jab_new->PlanBillingDate   = date('Y-m-d');
                $jab_new->PlanAmount        = $data['new_workhour'] * $hourly_rate;
                $jab_new->Description       = $data['new_description'];
                $jab_new->SourceType        = 'JT';
                $jab_new->SourceUniqueID    = $timesheet_id;
                $jab_new->SourceTimeCode    = $data['new_timecode'];
                $jab_new->SourceEmployeeID  = $data['new_handler'];
                $jab_new->User_C            = Auth::user()->EmployeeID;
                $jab_new->Date_C            = date('Y-m-d H:i:s');
                $jab_update_result          = $jab_new->save();                
            }
        } else {
            // Delete record
            $jab_update_result = $jab->delete();
        }

        $update_result = $this->timesheet->update($timesheet_id, $update_data);

        if($update_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Timesheet updated successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    // new implementation
    public function postUpdate2()
    {
        $response = array();
        if(!Input::has('timesheet_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [timesheet_id]';
            return Response::json($response);
        } 

        $timesheet_id = Input::get('timesheet_id');
        $data = Input::all();
        unset($data['timesheet_id']);

        $hourly_rate = $this->_hourlyRate($data['new_handler']);

        $update_data['timesheet'] = array(
            'JobAssignmentID'   => $data['new_matter_id'],
            'WorkDate'          => date('Y-m-d', strtotime($data['new_timesheet_date'])),
            'User_U'            => Auth::user()->EmployeeID,
            'Date_U'            => date('Y-m-d H:i:s')
        );

        $update_data['timesheetwork'] = array(
            //'TimeCode'          => $data['new_timecode'],
            'EmployeeID'        => $data['new_handler'],
            'WorkHour'          => $data['new_workhour'],
            'ChargeHour'        => $data['new_workhour'],
            'HourlyRate'        => $hourly_rate,
            'ChargeRate'        => $hourly_rate,
            'Comment'           => $data['new_description'],
            'BillingStatus'     => (true) ? 'Billable' : 'Unbillable',
            'Unbillable'        => (true) ? '0' : '1'
        );

        // Update or Delete jobassignmentbilling record
        $jab = JobAssignmentBilling::find($timesheet_id);
        $jab_update_result = false;
        //if($data['new_billable'] == '1') {
        if(true) {
            // Check if record exists
            if(count($jab) > 0) {
                // Update if found
                $jab->JobAssignmentID   = $data['new_matter_id'];
                $jab->PlanAmount        = $data['new_workhour'] * $hourly_rate;
                $jab->Description       = $data['new_description'];
                //$jab->SourceTimeCode    = $data['new_timecode'];
                $jab->SourceEmployeeID  = $data['new_handler'];
                $jab->User_U            = Auth::user()->EmployeeID;
                $jab->Date_U            = date('Y-m-d H:i:s');
                $jab_update_result      = $jab->save();
            } else {
                // Insert if not found
                $jab_new = new JobAssignmentBilling;
                $jab_new->CompanyID         = 'OLN';
                $jab_new->JobAssignmentID   = $data['new_matter_id'];
                $jab_new->UniqueID          = $timesheet_id;
                $jab_new->SeqID             = '1';
                $jab_new->PlanBillingDate   = date('Y-m-d');
                $jab_new->PlanAmount        = $data['new_workhour'] * $hourly_rate;
                $jab_new->Description       = $data['new_description'];
                $jab_new->SourceType        = 'JT';
                $jab_new->SourceUniqueID    = $timesheet_id;
                //$jab_new->SourceTimeCode    = $data['new_timecode'];
                $jab_new->SourceEmployeeID  = $data['new_handler'];
                $jab_new->User_C            = Auth::user()->EmployeeID;
                $jab_new->Date_C            = date('Y-m-d H:i:s');
                $jab_update_result          = $jab_new->save();                
            }
        } else {
            // Delete record
            $jab_update_result = $jab->delete();
        }

        $update_data['jobassignmentbilling'] = array();
        $update_result = $this->timesheet->update($timesheet_id, $update_data);

        if($update_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Timesheet updated successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postDelete()
    {
        $response = array();
        if(!Input::has('timesheet_id')) {
            $response['status'] = 'error';
            $response['message'] = 'Lacking parameter [timesheet_id]';
            return Response::json($response);
        }

        $timesheet_id = Input::get('timesheet_id');
        $delete_result = $this->timesheet->delete($timesheet_id);
        
        if($delete_result == true) {
            $response['status'] = 'success';
            $response['message'] = 'Timesheet deleted successfully.';            
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    private function _hourlyRate($handler_id)
    {
        $user = new User();
        $handler_user_id = $user->getUserID($handler_id);
        $role = $user->getEmployeeJob(array($handler_user_id));
        return $role[0]->HourlyRate;        
    }

    public function getHasJobAssignmentBilling()
    {
        $timesheet_id = Input::get('timesheet_id');

        $jab = JobAssignmentBilling::find($timesheet_id);

        $result['found'] = (count($jab) > 0) ? true : false ;

        return Response::json($result);
    }
    
    public function getSheetsforremove($matter){
        $u = new Utilities;
        
        $deleted = array('MatterID' => $matter);
        $timesheets = $u->getTimeSheet($matter);
        
        foreach($timesheets as $timesheet):
            if($timesheet->BatchNumber):
                $deleted[]=array('BatchNumber' => $timesheet->BatchNumber, 'InvoiceNumber' => $timesheet->invoicehead_UniqueID);
                $u->removetimesheet($timesheet->BatchNumber,$timesheet->invoicehead_UniqueID);
            endif;
        endforeach;
        
        
        return Response::json($deleted);
    }
    /*
    public function getFastcreate(){
        //$t = new Timesheet;
        $user = new User;
        $utils = new Utilities;
        $data = $utils->retrieveDefaults('matters');
        $active_user = User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);
        $obj['matters'] = json_decode(bzdecompress($data[0]->data));
        $obj['handler']= $user->getEmployeesByRoles("Handler");
        $obj['default'] =  Session::get('principal_user');
        $obj['special_matters'] = HelperLibrary::getSpecialMatters();

        return View::make('pages/timesheetfast',array('obj' => $obj));
    }
    */
    public function postSavetimesheets(){
        $t = Input::all();
        $u = new User;
        $utils = new Utilities;
        $ts = new Timesheet;
        $obj['timesheets'] = array();
        $obj['count'] = 0;
        $popup = Input::get('popup');
        $flag = false;
        $__create = array();
        for($i = 0; $i < count($t['matter_no']); $i++):
            $handler = json_decode($t['handler'][$i]);
            $rate = $u->getEmployeeJob(array($handler->user_id));
            if($t['timesheetnum'][$i] == 0){
                $obj['source'] = 'create';
                $__create[] = array(
                    'WorkDate' => date_format(date_create($t['date'][$i]),"Y-m-d"),
                    'JobAssignmentID' => $t['matter_no'][$i],
                    'Comment' => $t['description'][$i],
                    'TimeCode' => "LEGAL-LEG-GEN",
                    'WorkHour' => ($t['unit'][$i] * 5) / 60,
                    'HourlyRate' => $rate[0]->HourlyRate,
                    'EmployeeID' => $handler->EmployeeID
                );
                $flag = true;
            }else{
                $obj['source'] = 'update';
                $utils->updateTimesheet(array(
                    'WorkDate' => date_format(date_create($t['date'][$i]),"Y-m-d"),
                    'matter_no' => $t['matter_no'][$i],
                    'Comment' => $t['description'][$i],
                    'TimeCode' => "LEGAL-LEG-GEN",
                    'WorkHour' => ($t['unit'][$i] * 5) / 60,
                    'HourlyRate' => $rate[0]->HourlyRate,
                    'EmployeeID' => $handler->EmployeeID,
                    'UniqueID' => $t['timesheetnum'][$i]
                ));
                $flag = true;
            }
        endfor;
        if(count($__create) > 0)
            $ts->create3(null,$__create);
        if($flag){
            $utils->resetFlag();
            if(!$popup){
                $active_user = User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id);
                if($active_user == 'ADMIN')
                    $obj['timesheets'] = $utils->getTSAdmin();
                else
                    $obj['timesheets'] = $utils->getTimeSheets($active_user);
                $obj['count'] = count($obj['timesheets']);
            }
        }

        if($popup){
            return View::make('pages/blank');
        }else
            return Response::json(array('obj' => $obj));

    }
    
    public function getTimecodelist(){
        $t = new Timesheet;
        
        $codes = $t->getTimeCode();
        
        return Response::json($codes);
    }

    public function postMoveItem()
    {
        //$data = Input::all();
        //return Response::json($data);

        $response = array();

        $timesheet = new Timesheet;
        $matter_id = Input::get('timesheet.matter_id');
        if(trim(Input::get('timesheet.id')) == '') {
            //Save new timesheet to another matter

            $timesheet_data = array();
            $timesheet_data[] = array(
                'WorkDate'      => Input::get('timesheet.date'),
                'TimeCode'      => 'LEGAL-LEG-GEN',
                'EmployeeID'    => Input::get('timesheet.handler'),
                'WorkHour'      => Input::get('timesheet.work_hours'),
                'HourlyRate'    => Input::get('timesheet.hourly_rate'),
                'Comment'       => Input::get('timesheet.description')
            );

            $result = $timesheet->create3($matter_id, $timesheet_data);
        } else {
            //Move existing timesheet to another matter
            $data = array(
                'timesheet' => array(
                    'JobAssignmentID' => $matter_id,
                    'WorkDate'      => date('Y-m-d', strtotime(Input::get('timesheet.date'))),
                    'User_U'        => Auth::user()->EmployeeID,
                    'Date_U'        => date('Y-m-d')
                ),
                'timesheetwork' => array(
                    'EmployeeID'    => Input::get('timesheet.handler'),
                    'Comment'       => Input::get('timesheet.description'),
                    'WorkHour'      => Input::get('timesheet.work_hours'),
                    'ChargeHour'    => Input::get('timesheet.work_hours'),
                    'HourlyRate'    => Input::get('timesheet.hourly_rate'),
                    'ChargeRate'    => Input::get('timesheet.hourly_rate')
                ),
                'jobassignmentbilling' => array(
                    'JobAssignmentID'   => $matter_id,
                    'PlanAmount'        => Input::get('timesheet.work_hours') * Input::get('timesheet.hourly_rate'),
                    'Description'       => Input::get('timesheet.description'),
                    'User_U'            => Auth::user()->EmployeeID,
                    'Date_U'            => date('Y-m-d')
                )
            );
            $result = $timesheet->update(Input::get('timesheet.id'), $data);
        }

        if($result) {
            $response['status'] = 'success';
            $response['message'] = 'Timesheet moved to Matter ' . $matter_id;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Database error.';            
        }

        return Response::json($response);
    }

    public function postUpdateTimesheetSession()
    {
        $data = Input::has('timesheet_matter_id') ? Input::get('timesheet_matter_id') : null;

        HelperLibrary::storeSessionData(array('timesheet_matter_id' => $data));

        return Response::json(array('status' => 'success'));
    }

}
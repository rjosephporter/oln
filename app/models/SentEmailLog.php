<?php

class SentEmailLog extends Eloquent {

	protected $table = 'sent_email_log';

	public function scopeLatest($query)
	{
		return $query->orderBy('date_sent', 'desc');
	}

}
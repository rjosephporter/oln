<?php

class Employee extends Eloquent {
	
	protected $table = 'employee';

	protected $primaryKey = 'EmployeeID';

	public $incrementing = false;

	public function createEmployee($item_count,$data)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT 1 FROM employee where EmployeeID=\''.strtoupper($data['new_employeeid'][0]).'\'');
        //$lastclient=$get_result[0];
        Debugbar::info($get_result);
        if(count($get_result)==0){

	        if($item_count == count($data['new_employeeid'])) {
	            for ($i = 0; $i < $item_count ; $i++) { 
	               $data_employeenew[] = array(
	                    'COMPANYID'    =>'OLN',
	                    'EmployeeID' =>strtoupper($data['new_employeeid'][$i]),
	                    'EmployerID'    =>'OLN',
	                    'IsActive'    =>'1',
	                    'EmployeeLevel'    =>'0',
	                    'PersonnelAccessLevel'    =>'0',
	                    'SalaryAccessLevel'    =>'0',
	                    'SupervisorID'    =>$data['new_supervisor'][$i],
	                    'Gender'          =>$data['gender'][$i],
	                    'BloodType'       =>$data['bloodtype'][$i],
	                    'MartialStatus'   =>$data['marital_status'][$i],
	                    'Nationality'     =>$data['new_nationality_id'][$i],
	                    'DateOfBirth'     =>date('Y-m-d', strtotime($data['new_dob'][$i])),
	                    'PlaceOfBirth'    =>$data['new_pob'][$i],
	                    'DateHire'        =>date('Y-m-d', strtotime($data['new_datehired'][$i])),
	                    'DisplayLanguage'        =>'en-US',
	                    'DefaultDataLanguage'        =>'en-US',
	                    'CompanyWorkHour'        =>'1',
	                    'ChargeableHour'        =>'0.0000',
	                    'NickName'        =>$data['new_nickname'][$i],
	                    'MaxInactiveInterval'        =>'7200',
	                    'DateStyle'        =>'DMY',
	                    'WeekStartsOn'        =>$data['new_weekstarts'][$i],
                        'WeekdayAM_Apply'     =>(Input:: has('weekdayam') ? 1 : ''),
                        'WeekdayAM_Fr'        =>(Input:: has('weekdayamfrom') ? $data['weekdayamfrom'][$i] : ''),   
                        'WeekdayAM_To'        =>(Input:: has('weekdayamto') ? $data['weekdayamto'][$i] : ''),
                        'WeekdayPM_Apply'        =>(Input:: has('weekdaypm') ? 1 : ''),
                        'WeekdayPM_Fr'        =>(Input:: has('weekdaypmfrom') ? $data['weekdaypmfrom'][$i] : ''),
                        'WeekdayPM_To'        =>(Input:: has('weekdaypmto') ? $data['weekdaypmto'][$i] : ''),
                        'SaturdayAM_Apply'        =>(Input:: has('saturdayam') ? 1 : ''),
                        'SaturdayAM_Fr'        =>(Input:: has('saturdayamfrom') ? $data['saturdayamfrom'][$i] : ''),
                        'SaturdayAM_To'        =>(Input:: has('saturdayamto') ? $data['saturdayamto'][$i] : ''),
                        'SaturdayPM_Apply'        =>(Input:: has('saturdaypm') ? 1 : ''),
                        'SaturdayPM_Fr'        =>(Input:: has('saturdaypmfrom') ? $data['saturdaypmfrom'][$i] : ''),
                        'SaturdayPM_To'        =>(Input:: has('saturdaypmto') ? $data['saturdaypmto'][$i] : ''),
	                    'User_c'          => $employee_id,
	                    'Date_C'          => date('Y-m-d H:i:s'),
	                    'Date_U'          => date('Y-m-d H:i:s')
	                );

	   

	            }

	            /* Save to table */
	            $confnew  = DB::table('employee')->insert($data_employeenew);
	            
	            if($confnew) {
	                $result = true;
	         
	            }
	        }
    	}else{
    		//$result=false;
    	}

        return $result;
    }

    public function getRoles(){
        
            return DB::select(' SELECT * FROM roles');
    }

    public function getEmployees(){
        
            return DB::select(' SELECT * FROM employee');
    }

    public function createUser($item_count,$data,$empid)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(user_id) as lastuser FROM user');
        $lastclient=$get_result[0]->lastuser;
        $nextclient=$lastclient+1;

        $data_clientnew;
        
        if($item_count == count($data['new_fname'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'user_id'    =>$nextclient+$i,
                    'EmployeeID' =>$empid,
                    'first_name'                 => $data['new_fname'][$i],
                    'last_name'                  => $data['new_lname'][$i],
                    'email_address'              => $data['new_email'][$i],
                    'password'              => $data['new_password'][$i],
                    'status_id'              => '1',
                    'type'              => 'handler',
                    //'User_c'        => $employee_id,
                    //'Date_C'        => date('Y-m-d H:i:s'),
                    //'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('user')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }

        return $nextclient;
    }

    public function assignUsertoRole($item_count,$data,$userid)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
      
        
        if($item_count == count($data['new_role_id'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
               
                    'user_id' =>$userid,
                    'role_id' => $data['new_role_id'][$i],
                
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('assigned_roles')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }

        return $result;
    }

    public function getInfo($employee_id)
    {
        $result = DB::table('employee as emp')
                        ->leftJoin('user as usr', 'usr.EmployeeID', '=', 'emp.EmployeeID')
                        ->leftJoin('assigned_roles as asr', 'asr.user_id', '=', 'usr.user_id')
                        ->select('emp.*', 'usr.*')
                        ->where('emp.EmployeeID', $employee_id)->first();

        return $result;
    }

      public function getRoleInfo($userid)
    {
        $result = DB::table('assigned_roles as asr')
                        ->leftJoin('user as usr', 'usr.user_id', '=', 'asr.user_id')
                        //->leftJoin('assigned_roles as asr', 'asr.user_id', '=', 'usr.user_id')
                        ->select('asr.*')
                        ->where('asr.user_id', $userid)->get();

        return $result;
    }

    public function getAssistantbyemployee($userid)
    {
        $result = DB::table('employee_assistant as ea')
                        ->select('ea.*')
                        ->where('ea.employee_id', $userid)->get();

        return $result;
    }

    public function getHandlersbyemployee($userid)
    {
        $result = DB::table('employee_assistant as ea')
                        ->select('ea.*')
                        ->where('ea.assistant_id', $userid)->get();

        return $result;
    }

    public function updateEmployee($employee_id,$data)
    {
            $user_result = DB::table('employee')->where('EmployeeID', $employee_id)->take(1)->update($data);
            
            return ($user_result >= 0) ? true : false;
    }

    public function updateUser($employee_id,$data)
    {
            $user_result = DB::table('user')->where('EmployeeID', $employee_id)->take(1)->update($data);
            
            return ($user_result >= 0) ? true : false;
    }

     public function updateRole($user_id,$data)
    {
            $user_result = DB::table('assigned_roles')->where('user_id', $user_id)->take(1)->update($data);
            
            return ($user_result >= 0) ? true : false;
    }

    public function createAssistant($item_count,$data,$empid)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        //$get_result = DB::select('SELECT MAX(user_id) as lastuser FROM user');
       // $lastclient=$get_result[0]->lastuser;
       // $nextclient=$lastclient+1;

        $data_clientnew;
        
        if($item_count == count($data['new_pa_id'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    //'user_id'    =>$nextclient+$i,
                    'employee_id' =>$data['new_employeeid'][0],
                    'assistant_id'                 => $data['new_pa_id'][$i],
                  
                    'created_by'        => $employee_id,
                    'date_created'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('employee_assistant')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }

        return $result;
    }

    public function createHandler($item_count,$data,$empid)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        //$get_result = DB::select('SELECT MAX(user_id) as lastuser FROM user');
       // $lastclient=$get_result[0]->lastuser;
       // $nextclient=$lastclient+1;

        $data_clientnew;
        
        if($item_count == count($data['new_handler_id'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    //'user_id'    =>$nextclient+$i,
                    'employee_id' =>$data['new_handler_id'][$i],
                    'assistant_id'                 => $data['new_employeeid'][0],
                  
                    'created_by'        => $employee_id,
                    'date_created'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('employee_assistant')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }

        return $result;
    }
}
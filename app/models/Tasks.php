<?php

class Tasks{
    /*
     * get all tasks
     */
    public function getmytasks($user_id)
    {
        DB::statement('SET SESSION group_concat_max_len = 10000000');
        return DB::select('
                        SELECT
                        a.*,
                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.owner) AS owner_name,
                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name,
                        (SELECT c.Photo FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_photo,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"conversationid\":\"",c.id,"\",\"conversation\":",c.message,",\"datecreated\":\"",c.datecreated,"\",\"owner\":\"",e.NickName,"\",\"hash\":\"",c.hash,"\"}")
                                   ORDER BY c.datecreated DESC)
                             ,"]") FROM tdl_task_conversations AS c LEFT JOIN user AS d ON d.user_id = c.owner LEFT JOIN employee AS e ON e.EmployeeID = d.EmployeeID WHERE c.taskid = a.id)
                            AS task_conversations
                        FROM tdl_tasks AS a
                        WHERE
                        a.owner = ?
                        ORDER BY a.type ASC, a.status ASC, a.requiredbydate ASC
                    ',array($user_id));
    }

    /*
     * get all requests per user
     */
    public function getmyrequests($user_id)
    {
//        return DB::select('
//                        SELECT
//                        a.*,
//                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name
//                        FROM tdl_requests AS a
//                        WHERE
//                        a.requestedby = ?
//                        ORDER BY a.status ASC, a.requiredbydate ASC
//                    ',array($user_id));
        return DB::select('
                        SELECT
                        a.*,
                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"conversationid\":\"",c.id,"\",\"conversation\":",c.message,",\"datecreated\":\"",c.datecreated,"\",\"owner\":\"",e.NickName,"\",\"hash\":\"",c.hash,"\"}")
                                   ORDER BY c.datecreated DESC)
                             ,"]") FROM tdl_task_conversations AS c LEFT JOIN user AS d ON d.user_id = c.owner LEFT JOIN employee AS e ON e.EmployeeID = d.EmployeeID  WHERE c.taskid IN (SELECT e.id FROM tdl_tasks AS e WHERE e.requestid = a.id))
                            AS task_conversations
                        FROM tdl_requests AS a
                        WHERE
                        a.requestedby = :user_id
                        ORDER BY a.status ASC, a.requiredbydate ASC
                    ',array($user_id));
    }

    /*
     * get status of each task per user
     */
    public function getrequestindividualstatus($requestid)
    {
        return DB::select('
                        SELECT
                        a.status,
                        a.owner,
                        a.requiredbydate,
                        c.fullname,
                        a.donedate
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN
                        user AS b
                        ON b.user_id = a.owner
                        LEFT JOIN employee AS c
                        ON c.EmployeeID = b.EmployeeID
                        WHERE
                        a.requestid = ?
                    ',array($requestid));
    }

    public function createtaskfromminute($post)
    {
        if(isset($post['owners'])){
            foreach($post['owners'] as $owner):
                if($owner != ''){
                    $requestedby = '';
                    if(isset($post['requestedby'])){
                        $requestedby = $post['requestedby'];
                    }
                    if($owner == 'all'){
                        $app = Model::factory('app');
                        $users = $app->getallusers();
                        foreach($users as $user):
                            $query = DB::insert()->columns(array
                                    (
                                        'requestedon',
                                        'minuteid',
                                        'type',
                                        'status',
                                        'owner',
                                        'requestedby',
                                        'meeting_id',
                                        'message',
                                        'requiredbydate',
                                        'hash'
                                    )
                                )
                                ->values(array
                                    (
                                        date('Y-m-d H:i:s'),
                                        $post['minuteid'],
                                        $post['type'],
                                        '0',
                                        $user['uid'],
                                        $requestedby,
                                        $post['meetingid'],
                                        $post['message'],
                                        $post['requiredbydate'],
                                        md5(time().$user['uid'].$requestedby.$post['type'])
                                    )
                                )
                                ->table('tdl_tasks');
                            $query->execute();
                        endforeach;
                    } else {
                        $query = DB::insert()->columns(array
                                    (
                                        'requestedon',
                                        'minuteid',
                                        'type',
                                        'status',
                                        'owner',
                                        'requestedby',
                                        'meeting_id',
                                        'message',
                                        'requiredbydate',
                                        'hash'
                                    )
                                )
                                ->values(array
                                    (
                                        date('Y-m-d H:i:s'),
                                        $post['minuteid'],
                                        $post['type'],
                                        '0',
                                        $owner,
                                        $requestedby,
                                        $post['meetingid'],
                                        $post['message'],
                                        $post['requiredbydate'],
                                        md5(time().$owner.$requestedby.$post['type'])
                                    )
                                )
                                ->table('tdl_tasks');
                        $query->execute();
                    }
                }
            endforeach;
        }
        return;
    }

    public function deletetaskfromminutes($minute_id)
    {
        $query = DB::query(Database::DELETE,'
                        DELETE
                        FROM
                        tdl_tasks
                        WHERE
                        minuteid = :minuteid
                    ');
        $query->parameters(array
            (
                ':minuteid' => $minute_id
            ));
        return $query->execute();
    }

    public function edittaskfromminutes($post)
    {
        $query = DB::query(Database::DELETE,'
                        DELETE
                        FROM
                        tdl_tasks
                        WHERE
                        minuteid = :minuteid
                    ');
        $query->parameters(array
            (
                ':minuteid' => $minute_id
            ));
        $query->execute();
        $this->createtaskfromminute($post);
        return;
    }

    public function deletetaskfrommeeting()
    {

    }

    public function createowntask($data)
    {
        $sharees = '';
        $i = 1;
        if(isset($data['sharees'])){
            $sharees = join('|',$data['sharees']);
        }
        $insert_id = DB::table('tdl_tasks')
                ->insertGetId(array(
                    'requestid' => '',
                    'requestedon' => date('Y-m-d H:i:s'),
                    'minuteid' => $data['minuteid'],
                    'type' => $data['type'],
                    'status' => '0',
                    'owner' => $data['owner'],
                    'requestedby' => $data['requestedby'],
                    'meeting_id' => $data['meetingid'],
                    'message' => $data['message'],
                    'requiredbydate' => $data['requiredbydate'],
                    'responsesharedwith' => $sharees,
                    'hash' => md5(time().$data['owner'].$data['requestedby'].$data['type'])
                ));

        if(isset($data['filepaths'])){
            if(is_array($data['filepaths'])){
                foreach($data['filepaths'] as $filepath):
                    $query_f = DB::table('tdl_task_files')
                        ->insert(array
                                (
                                    'taskid' => $insert_id,
                                    'filename' => $filepath
                                )
                            );
                endforeach;
            }
        }
        return;
    }

    public function createtaskresponse($data)
    {
        $data['response'] = json_encode(array('subject'=>$data['subject'],'message'=>$data['message']));
        DB::table('tdl_tasks')
                ->where('id',$data['taskid'])
                ->update(array
                    (
                        'status' => '1',
                        'donedate' => date('Y-m-d H:i:s'),
                        'response' => $data['response'],
                        'declined' => isset($data['declined']) ? $data['declined'] : '0'
                    ));
        if(isset($data['filepaths'])){
            if(is_array($data['filepaths'])){
                foreach($data['filepaths'] as $filepath):
                    $query_f = DB::table('tdl_response_files')
                        ->insert(array
                                (
                                    'taskid' => $data['taskid'],
                                    'filename' => $filepath
                                )
                            );
                endforeach;
            }
        }
        //check if all sibling tasks also finished. If yes finish the main request//
        $result = DB::select('
                    SELECT
                    *
                    FROM
                    tdl_tasks AS a
                    WHERE a.requestid = (SELECT b.requestid FROM tdl_tasks AS b WHERE b.id = ?)
                ',array($data['taskid']));
        $i = 0;
        $n = 0;
        foreach($result as $data_a):
            $i++;
            $n += $data_a->status;
        endforeach;
        if($i > 0 && $i == $n){
            DB::update('
                    UPDATE
                    tdl_requests AS a
                    SET a.status = "1",
                    a.donedate = ?
                    WHERE a.id = (SELECT b.requestid FROM tdl_tasks AS b WHERE b.id = ?)
                ',array(date('Y-m-d H:i:s'),$data['taskid']));
        }
        ////////////////////////////////////////////////////////////////////////////
        return;
    }

    public function gettasks($params)
    {
        foreach($params as $k => $v){
            $field = $k;
            $value = $v;
        }
        DB::statement('SET SESSION group_concat_max_len = 1000000');
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"conversationid\":\"",id,"\",\"conversation\":",message,",\"datecreated\":\"",datecreated,"\"}") ORDER BY id DESC
                                  )
                             ,"]") FROM tdl_task_conversations WHERE taskid = a.id)
                            AS conversations
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.requestid = ?
                    ',array($value));
    }

    public function gettask($id)
    {
        DB::statement('SET SESSION group_concat_max_len = 1000000');
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address,
                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.id = ?
                    ',array($id));
    }

    public function gettaskfiles($id)
    {
        return DB::select('
                        SELECT
                        REPLACE(filename,"'.public_path().'","") AS filename
                        FROM
                        tdl_task_files
                        WHERE
                        taskid = ?
                    ',array($id));
    }

    public function getresponsefiles($id)
    {
        return DB::select('
                        SELECT
                        REPLACE(filename,"'.public_path().'","") AS filename
                        FROM
                        tdl_response_files
                        WHERE
                        taskid = ?
                    ',array($id));
    }

    public function createtaskrequest($data)
    {
        //create request first
        $owners = '';
        if(isset($data['owners'])){
            $owners = join('|',$data['owners']);
        }
        $sharees = '';
        if(isset($data['sharees'])){
            $sharees = join('|',$data['sharees']);
        }
        $insert_id = DB::table('tdl_requests')
                ->insertGetId(array
                    (
                    'requestedon' => date('Y-m-d H:i:s'),
                    'minuteid' => $data['minuteid'],
                    'type' => $data['type'],
                    'status' => '0',
                    'owners' => $owners,
                    'requestedby' => $data['requestedby'],
                    'meeting_id' => $data['meetingid'],
                    'message' => $data['message'],
                    'requiredbydate' => $data['requiredbydate'],
                    'responsesharedwith' => $sharees,
                    'hash' => md5(time().$owners.$data['requestedby'].$data['type'])
                    )
                );
        foreach($data['owners'] as $owner):
            if($owner != ''){
                if($owner == 'all'){
                    $commcenter = new Commcenter();
                    $users = $commcenter->getallusers();
                    foreach($users as $user):
                        $insert_id1 = DB::table('tdl_tasks')
                            ->insertGetId(array
                                (
                                    'requestid' => $insert_id,
                                    'requestedon' => date('Y-m-d H:i:s'),
                                    'minuteid' => $data['minuteid'],
                                    'type' => $data['type'],
                                    'status' => '0',
                                    'owner' => $user['uid'],
                                    'requestedby' => $data['requestedby'],
                                    'meeting_id' => $data['meetingid'],
                                    'message' => $data['message'],
                                    'requiredbydate' => $data['requiredbydate'],
                                    'responsesharedwith' => $sharees,
                                    'hash' => md5(time().$user['uid'].$data['requestedby'].$data['type']),
                                    'other_id' => isset($data['other_id']) ? $data['other_id'] : ''
                                )
                            );
//                        if(isset($data['filepaths'])){
//                            if(is_array($data['filepaths'])){
//                                foreach($data['filepaths'] as $filepath):
//                                    $query_f = DB::table('tdl_task_files')
//                                        ->insert(array
//                                                (
//                                                    'taskid' => $insert_id1,
//                                                    'filename' => $filepath
//                                                )
//                                            );
//                                endforeach;
//                            }
//                        }
                        $this->updatecometpooling($user['uid']);
                    endforeach;
                } else {
                    $insert_id2 = DB::table('tdl_tasks')
                        ->insertGetId(array
                                (
                                    'requestid' => $insert_id,
                                    'requestedon' => date('Y-m-d H:i:s'),
                                    'minuteid' => $data['minuteid'],
                                    'type' => $data['type'],
                                    'status' => '0',
                                    'owner' => $owner,
                                    'requestedby' => $data['requestedby'],
                                    'meeting_id' => $data['meetingid'],
                                    'message' => $data['message'],
                                    'requiredbydate' => $data['requiredbydate'],
                                    'responsesharedwith' => $sharees,
                                    'hash' => md5(time().$owner.$data['requestedby'].$data['type']),
                                    'other_id' => isset($data['other_id']) ? $data['other_id'] : ''
                                )
                            );

//                    if(isset($data['filepaths'])){
//                        if(is_array($data['filepaths'])){
//                            foreach($data['filepaths'] as $filepath):
//                                $query_f = DB::table('tdl_task_files')
//                                    ->insert(array
//                                            (
//                                                'taskid' => $insert_id2,
//                                                'filename' => $filepath
//                                            )
//                                        );
//                            endforeach;
//                        }
//                    }
                    $this->updatecometpooling($owner);
                }
            }
        endforeach;
//        if(isset($data['filepaths'])){
//            if(is_array($data['filepaths'])){
//                foreach($data['filepaths'] as $filepath):
//                    $query_f = DB::table('tdl_request_files')
//                        ->insert(array
//                                (
//                                    'requestid' => $insert_id,
//                                    'filename' => $filepath
//                                )
//                            );
//                endforeach;
//            }
//        }
        return $insert_id;
    }

    /////added after May 25, 2014

//    OLD VERSION
//    public function getmyrequestsnew($user_id)
//    {
//        return $query = DB::select('
//                        SELECT
//                        a.*,
//                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.owner) AS owner_name,
//                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name,
//                        (SELECT c.Photo FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_photo,
//                        (SELECT
//                             CONCAT("[",
//                                  GROUP_CONCAT(
//                                       CONCAT("{\"filename\":\"",REPLACE(filename,"'.public_path().'",""),"\"}")
//                                  )
//                             ,"]") FROM tdl_task_files WHERE taskid = a.id)
//                            AS task_files
//                        FROM tdl_tasks AS a
//                        WHERE
//                        a.requestedby = ?
//                        AND a.owner != ?
//                        ORDER BY a.type ASC, a.status ASC, a.requiredbydate ASC
//                    ',array($user_id,$user_id));
//    }

    public function getmyrequestsnew($user_id)
    {
        DB::statement('SET SESSION group_concat_max_len = 10000000');
        return $query = DB::select('
                        SELECT
                        a.*,
                        (SELECT GROUP_CONCAT(CONCAT(" ",f.NickName)) FROM user AS b LEFT JOIN employee AS f ON f.EmployeeID = b.EmployeeID WHERE CONCAT("|",a.owners,"|") LIKE CONCAT("%|",b.user_id,"|%")) AS owner_name,
                        (SELECT c.NickName FROM user AS b LEFT JOIN employee AS c ON c.EmployeeID = b.EmployeeID WHERE b.user_id = a.requestedby) AS requestedby_name,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"conversationid\":\"",c.id,"\",\"conversation\":",c.message,",\"datecreated\":\"",c.datecreated,"\",\"owner\":\"",e.NickName,"\",\"hash\":\"",c.hash,"\"}")
                                   ORDER BY c.datecreated DESC)
                             ,"]") FROM tdl_task_conversations AS c LEFT JOIN user AS d ON d.user_id = c.owner LEFT JOIN employee AS e ON e.EmployeeID = d.EmployeeID  WHERE c.taskid IN (SELECT e.id FROM tdl_tasks AS e WHERE e.requestid = a.id))
                            AS task_conversations,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"taskid\":\"",c.id,"\",\"final_reply\":",c.response,",\"donedate\":\"",c.donedate,"\",\"owner\":\"",e.NickName,"\",\"hash\":\"",c.hash,"\"}")
                                   ORDER BY c.donedate DESC)
                             ,"]") FROM tdl_tasks AS c LEFT JOIN user AS d ON d.user_id = c.owner LEFT JOIN employee AS e ON e.EmployeeID = d.EmployeeID  WHERE c.requestid = a.id AND c.response <> "")
                            AS request_final_replies
                        FROM tdl_requests AS a
                        WHERE
                        a.requestedby = ?
                        ORDER BY a.status ASC, a.requiredbydate ASC
                    ',array($user_id));
    }

    public function gettaskbyhash($hash)
    {
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.hash = ?
                    ',array($hash));
    }

    public function bindoriginalnewtask($newtask,$path)
    {
        return DB::table('tdl_tasks')
                    ->where('id', $newtask)
                    ->update(array('delegatedfrom' => json_encode($path)));
    }

    public function createtaskmessage($data)
    {
        $data['response'] = json_encode(array('subject'=>$data['subject'],'message'=>$data['message']));
        $insert_id = DB::table('tdl_task_conversations')
                ->insertGetId(array(
                    'taskid' => $data['taskid'],
                    'datecreated' => date('Y-m-d H:i:s'),
                    'status' => '0',
                    'owner' => $data['owner'],
                    'requestedby' => $data['requestedby'],
                    'responsesharedwith' => $data['responsesharedwith'],
                    'message'=> $data['response'],
                    'hash' => isset($data['hash']) ? $data['hash'] : md5(time().$data['requestedby'].$data['requestedby'])
                ));
        $this->updatecometpooling($data['requestedby']);
//        if(isset($data['filepaths'])){
//            if(is_array($data['filepaths'])){
//                foreach($data['filepaths'] as $filepath):
//                    $query_f = DB::table('tdl_task_conversations_files')
//                        ->insert(array
//                                (
//                                    'taskid' => $insert_id,
//                                    'filename' => $filepath
//                                )
//                            );
//                endforeach;
//            }
//        }
        //check if all sibling tasks also finished. If yes finish the main request//
        $result = DB::select('
                        SELECT
                        *
                        FROM
                        tdl_tasks AS a
                        WHERE a.requestid = (SELECT b.requestid FROM tdl_tasks AS b WHERE b.id = ?)
                    ',array($data['taskid']));
        $i = 0;
        $n = 0;
        foreach($result as $temp):
            $i++;
            $n += $temp->status;
        endforeach;
        if($i > 0 && $i == $n){
            $query_w = DB::update('
                UPDATE
                tdl_requests AS a
                SET a.status = "1",
                a.donedate = ?
                WHERE a.id = (SELECT b.requestid FROM tdl_tasks AS b WHERE b.id = ?)
            ',array(date('Y-m-d H:i:s'),$data['taskid']));
        }
        ////////////////////////////////////////////////////////////////////////////
        return;
    }

    public function gettaskbyotherid($other_id)
    {
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.other_id = ?
                    ',array($other_id));
    }

    public function getmynumberoftasks($user_id)
    {
        return DB::table('tdl_tasks')
                ->where('owner','=',$user_id)
                ->where('status','=','0')
                ->count();
    }

    public function gettaskreplies($taskid)
    {
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address
                        FROM
                        tdl_task_conversations AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.taskid = ?
                        ORDER BY datecreated DESC
                    ',array($taskid));
    }

    public function gettasksbyrequest($id)
    {
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address,
                        (SELECT c.NickName FROM employee AS c LEFT JOIN user AS d ON c.EmployeeID = d.EmployeeID WHERE d.user_id = a.requestedby) AS requestedby_name
                        FROM
                        tdl_tasks AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.requestid = ?
                    ',array($id));
    }

    public function getrequest($id)
    {
        return DB::select('
                        SELECT
                        a.*,
                        (SELECT c.NickName FROM employee AS c LEFT JOIN user AS d ON c.EmployeeID = d.EmployeeID WHERE d.user_id = a.requestedby) AS requestedby_name
                        FROM tdl_requests AS a
                        WHERE
                        a.id = ?
                    ',array($id));
    }

    public function getrequestreplies($requestid)
    {
        return DB::select('
                        SELECT
                        a.*,
                        c.NickName,
                        b.email_address
                        FROM
                        tdl_task_conversations AS a
                        LEFT JOIN user AS b
                        ON a.owner = b.user_id
                        LEFT JOIN employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE
                        a.taskid IN (SELECT c.id FROM tdl_tasks AS c WHERE c.requestid = ?)
                        GROUP BY a.hash
                        ORDER BY datecreated DESC
                    ',array($requestid));
    }

    public function updatecometpooling($user)
    {
        if(DB::table('tdl_comet_polling')->where('user','=',$user)->count() == 0){
            DB::table('tdl_comet_polling')->insert(array('user' => $user, 'lastupdate' => time()));
        } else {
            DB::table('tdl_comet_polling')->where('user', $user)->update(array('lastupdate' => time()));
        }
        return;
    }
    /*
     * added on December 1, 2014
     */      
            
}//end-classs
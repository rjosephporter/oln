<?php

class JobAssignmentBilling extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'jobassignmentbilling';

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'UniqueID';

	/**
	 * Set table timestamp
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * Fields that can be mass assigned
	 *
	 * @var array
	 */
	protected $fillable = array();

}
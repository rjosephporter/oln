<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Commcenter{

        public function getallusers()
        {
            return DB::select('
                    SELECT
                    a.Photo,
                    a.NickName,
                    a.EmployeeID,
                    b.user_id,
                    b.email_address,
                    b.first_name,
                    b.last_name
                    FROM
                    employee AS a
                    LEFT JOIN
                    user AS b
                    ON a.EmployeeID = b.EmployeeID
                    WHERE a.IsActive = "1"
                    AND a.EmployeeID NOT IN ("ADMIN","MASTER")
                    ORDER BY a.EmployeeID
                ');
        }

        public function getsomeusers($users)
        {
            $users = join('","',$users).'"';
            return DB::select('
                    SELECT
                    a.Photo,
                    a.NickName,
                    a.EmployeeID,
                    b.user_id,
                    b.email_address,
                    b.first_name,
                    b.last_name
                    FROM
                    employee AS a
                    LEFT JOIN
                    user AS b
                    ON a.EmployeeID = b.EmployeeID
                    WHERE b.user_id IN (?)
                    AND a.EmployeeID NOT IN ("ADMIN","MASTER")
                    ORDER BY a.EmployeeID
                ',array($users));
        }

        public function getmeetingtypes()
        {
            $query = DB::query(Database::SELECT,'
                            SELECT
                            *
                            FROM
                            mm_meeting_type
                        ');
            return $query->execute()->as_array();
        }

//        public function getuserinfo($username)
//        {
//            $data = null;
//            $query = DB::select('pms_user_data.*','users.username')->from('pms_user_data')->join('users','LEFT')->on('users.id','=','pms_user_data.uid')->where('users.username', '=', $username);
//            $result = $query->execute()->as_array();
//            foreach($result as $temp):
//                $data = $temp;
//            endforeach;
//            return $data;
//        }
//
//        public function updateuserphoto($post)
//        {
//            foreach($post['filepaths'] as $filepath):
//                $query = DB::update()->set(array
//                        (
//                            'photo' => basename($filepath)
//                        )
//                    )->where('uid','=',$post['uid'])
//                    ->table('pms_user_data');
//            $query->execute();
//            endforeach;
//        }
//
//        public function updateuserprofile($post)
//        {
//            $query = DB::update()->set(array
//                    (
//                        $post["field"] => $post['value'],
//                    )
//                )->where('uid','=',$post['key'])
//                ->table('pms_user_data');
//            $query->execute();
//            return;
//        }
//
//        public function getallsystemusers()
//        {
//
//            $query = DB::query(Database::SELECT,'
//                            SELECT
//                            b.fullname,
//                            a.*
//                            FROM users AS a
//                            LEFT JOIN
//                            pms_user_data AS b
//                            ON a.id = b.uid
//                            ORDER BY a.username ASC
//                        ');
//            return $query->execute()->as_array();
//        }
//
//        public function createuseridentity($post)
//        {
//            $query = DB::insert()->columns(array
//                    (
//                        'uid',
//                        'fullname',
//                        'email',
//                        'active',
//                    )
//                )
//                ->values(array
//                    (
//                        $post['id'],
//                        $post['fullname'],
//                        $post['email'],
//                        '1'
//                    )
//                )
//                ->table('pms_user_data');
//            $query->execute();
//        }
//
//        public function deleteuseridentity($id)
//        {
//            $query = DB::update()->set(array
//                        (
//                            'active' => '0'
//                        )
//                    )->where('uid','=',$id)
//                    ->table('pms_user_data');
//            $query->execute();
//        }
//
//        public function updateuseridentity($post)
//        {
//            $query = DB::update()->set(array
//                        (
//                            'fullname' => $post['fullname'],
//                            'email' => $post['email'],
//                        )
//                    )->where('uid','=',$post['id'])
//                    ->table('pms_user_data');
//            $query->execute();
//        }
}
?>

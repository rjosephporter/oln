<?php

class Sessions extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sessions';

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Set table timestamp
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public static function ableTo($action, $matter_id) 
	{
		switch ($action) {
			case 'save-timesheet':
			case 'save-invoice':
				$result = Sessions::where('id', '<>', Session::getId())
									->where(function($query) use ($matter_id) {
										$query->where('invoice_matter_id', $matter_id)
												->orWhere('timesheet_matter_id', $matter_id);
									})
									->count();
				break;
			
				//$result = Sessions::where('timesheet_matter_id', $matter_id)->count();
				//break;			
			default:
				$result = 0;
				break;
		}

		return $result > 0 ? false : true;
	}	

}
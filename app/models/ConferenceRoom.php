<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ConferenceRoom extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'conference_room';

	public function reservation()
	{
		return $this->hasMany('ConferenceRoomReservation');
	}

	public function scopeActive($query)
	{
		return $query->whereStatus(1);
	}

	public function createRoom($item_count, $data)
	{
		$result = false;
		$id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
		$employee_id = User::getEmployeeID($id_active);

		$data_ts = $data_tsw = array();
		if($item_count == count($data['new_conference_room_name'])) {
			for ($i = 0; $i < $item_count ; $i++) { 
				/* new conference room Data */
				$data_confnew[] = array(
					'name'				=> $data['new_conference_room_name'][$i],
					'description'		=> $data['new_conference_room_description'][$i],
					'status'		    => '1',
					'created_by'		=> $employee_id,
					'created_at'		=> date('Y-m-d H:i:s'),
					'updated_at'		=> date('Y-m-d H:i:s')
				);

			}

			/* Save to table */
			$confnew  = DB::table('conference_room')->insert($data_confnew);
			
			if($confnew) {
				$result = true;
			}
		}

		return $result;
	}	

	

}
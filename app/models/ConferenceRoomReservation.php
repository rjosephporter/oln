<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class ConferenceRoomReservation extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'conference_room_reservation';

	public function conference_room()
	{
		return $this->belongsTo('ConferenceRoom');
	}

	public function scopeActive($query)
	{
		return $query->whereStatus(1);
	}

	/* Accessors & Mutators */
    public function getStartTimeAttribute($value)
    {
        return date('h:i A', strtotime($value));
    }
    
    public function getEndTimeAttribute($value)
    {
        return date('h:i A', strtotime($value));
    }
    /* /Accessors & Mutators */

	public function getTimes($room_id,$res_date){
		$res_date=date('Y-m-d', strtotime($res_date));
		return DB::select('SELECT start_time,end_time FROM conference_room_reservation where conference_room_id=\''.$room_id.'\' and reservation_date=\''.$res_date.'\'');

		//return $result;
	}

	public function getAllTimes($room_id){
		//$res_date=date('Y-m-d', strtotime($res_date));
		return DB::select('SELECT * FROM conference_room_reservation where conference_room_id=\''.$room_id.'\'');

		//return $result;
	}

	public function getAllTimes2(){
		//$res_date=date('Y-m-d', strtotime($res_date));
		return DB::select('SELECT * FROM conference_room_reservation');

		//return $result;
	}
    public function createReservation($item_count, $data)
	{
		$result = false;
		$id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
		$employee_id = User::getEmployeeID($id_active);

		$data_reservation = array();
		/*$time=$data['new_time'][0];
        $resdate=explode("-", $time);
        $resdate1=explode(" ", $resdate[0]);
        $enddate1=explode(" ", $resdate[1]);
        var_dump($resdate1);
        var_dump($enddate1);
        $startdate=$resdate1[0];
        $starttime=$resdate1[1]."".$resdate1[2];
        $endtime=$enddate1[1]."".$enddate1[2];*/

       // $time=$data['new_start_time'][0];
       // $startdate=explode(" ", $time);
       // $startdate=$startdate[0];
         $time=$data['new_start_time'][0];
        $timee=$data['new_end_time'][0];
        
        $time2=date_format(date_create($time), 'Y-m-d H:i:s');
        $time3=date_format(date_create($timee), 'Y-m-d H:i:s');
        $startdate=explode(" ", $time2);
        $startdatee=explode(" ", $time3);
        $startdate1=$startdate[0];

		if($item_count == count($data['reservedby'])) {
			for ($i = 0; $i < $item_count ; $i++) { 
				/* new conference room Data */
				$data_reservation[] = array(
					'conference_room_id'=> $data['conferenceid'],
					'reserved_by'		=> $data['reservedby'][$i],
					'reservation_date'  => $startdate1,
					'purpose'  			=> $data['new_purpose'][$i],
					'notes'				=> (Input:: has('new_notes') ? $data['new_notes'][$i] : ''),
					'start_time'		=> $startdate[1],//$data['new_start_time'][$i],//$starttime,
					'end_time'          => $startdatee[1],//$data['new_end_time'][$i],//$endtime,
					'status'			=> '1',
					'created_by'        => $employee_id,
					'created_at'		=> date('Y-m-d H:i:s'),
					'updated_at'		=> date('Y-m-d H:i:s')
				);

			}

			/* Save to table */
			$reservationnew  = DB::table('conference_room_reservation')->insert($data_reservation);
			
			if($reservationnew) {
				$result = true;
			}
		}

		return $result;
	}

	public function updateReservation($id,$data)
	{
			$user_result = DB::table('conference_room_reservation')->where('id', $id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
	public function deleteReservation($dir_id)
	{
		$ts_result  = DB::table('conference_room_reservation')->where('id', $dir_id)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}

}
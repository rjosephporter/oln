<?php

class Matter extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'jobassignment';

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'JobAssignmentID';

	/**
	 * Set table timestamp
	 *
	 * @var boolean
	 */
	//public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array();

	/**
	 * Fields that can be mass assigned
	 *
	 * @var array
	 */
	//protected $fillable = array('ref_no', 'client_id', 'introducer_id', 'subject_matter', 'matter', 'handler_id');

	public function prepareQueryForGetList($user_id = false, $filter_array = array())
	{

		/* SELECT filters */
		$select_string_array = array();

		if(isset($filter_array['show_select_basic']) && $filter_array['show_select_basic'] == true) {
			$select_string_array[] = '
				SQL_CALC_FOUND_ROWS
				\'DUMMY\' as dummy_column,
				ja.JobAssignmentID,
				ja.Description_A,
				ja.Incharge,
				rm.workdate_year,
				rm.workdate_month,
				rm.workdate_day,
				max(Date(concat(rm.workdate_year,\'-\',rm.workdate_month,\'-\',rm.workdate_day))) as latest_activity_date,

				ja.Date_C,
				ja.CustomerID,
				cm.CompanyName_A1,

				ja.Completed			
			';
		}

		if(isset($filter_array['show_select_mattertype']) && $filter_array['show_select_mattertype'] == true) {
			$select_string_array[] = '
				(
					select
						CASE 
							WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = -1 and ja.Completed = 0 THEN "untouched"
							WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 0 and ja.Completed = 0 THEN "green"
							WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 1 and ja.Completed = 0 THEN "green"
							WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 2 and ja.Completed = 0 THEN "orange"
							WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) > 2 and ja.Completed = 0 THEN "red"
							WHEN ja.Completed = 1 THEN "blue"
							ELSE "green"
						END AS matter_type									
					from timesheet as ts
					where ts.JobAssignmentID = ja.JobAssignmentID
					limit 1
				) as matter_type
			';
		}

		if(isset($filter_array['show_select_billdetails']) && $filter_array['show_select_billdetails'] == true) {
			$select_string_array[] = '
				COALESCE(sum(rm.total_billed_amount),0) as total_billed_amount,
				COALESCE(sum(rm.total_billable_amount),0) as total_billable_amount,
				COALESCE(sum(rm.total_unbilled_amount),0) as total_unbilled_amount
			';
		}

		if(isset($filter_array['show_select_invoicedetails']) && $filter_array['show_select_invoicedetails'] == true) {
			$select_string_array[] = '
				(select inh.InvoiceDate from invoicedetail as ind 
				left join timesheet as ts on ts.UniqueID = ind.ReferenceID left join invoicehead as inh on inh.UniqueID = ind.UniqueID 
				where ts.JobAssignmentID = ja.JobAssignmentID group by ind.UniqueID order by inh.InvoiceDate desc limit 1) as last_invoice_date,
				round(coalesce((select coalesce(sum(ind.Amount),0) as total_invoice_amount from invoicedetail as ind 
				left join timesheet as ts on ts.UniqueID = ind.ReferenceID left join invoicehead as inh on inh.UniqueID = ind.UniqueID 
				where ts.JobAssignmentID = ja.JobAssignmentID group by ind.UniqueID order by inh.InvoiceDate desc limit 1),0),2) as last_invoice_amount
			';
		}

		$select_string = (!empty($select_string_array)) ? implode(',', $select_string_array) : '*';
		/* /SELECT filters */

		$query = DB::table('jobassignment as ja')
					->select(DB::raw($select_string))
					/*
					->select(
						DB::raw('
							SQL_CALC_FOUND_ROWS
							ja.JobAssignmentID,
							ja.Description_A,
							ja.Incharge,
							rm.workdate_year,
							rm.workdate_month,
							rm.workdate_day,
							max(Date(concat(rm.workdate_year,\'-\',rm.workdate_month,\'-\',rm.workdate_day))) as latest_activity_date,

							ja.Date_C,
							ja.CustomerID,
							cm.CompanyName_A1,

							ja.Completed,

							(
								select
									CASE 
										WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = -1 and ja.Completed = 0 THEN "untouched"
										WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 0 and ja.Completed = 0 THEN "green"
										WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 1 and ja.Completed = 0 THEN "green"
										WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) = 2 and ja.Completed = 0 THEN "orange"
										WHEN COALESCE(TIMESTAMPDIFF(MONTH,max(ts.WorkDate),CURDATE()),-1) > 2 and ja.Completed = 0 THEN "red"
										WHEN ja.Completed = 1 THEN "blue"
										ELSE "green"
									END AS matter_type									
								from timesheet as ts
								where ts.JobAssignmentID = ja.JobAssignmentID
								limit 1
							) as matter_type,

							COALESCE(sum(rm.total_billed_amount),0) as total_billed_amount,
							COALESCE(sum(rm.total_billable_amount),0) as total_billable_amount,
							COALESCE(sum(rm.total_unbilled_amount),0) as total_unbilled_amount
						')
					)
					*/
					->leftJoin('report_matters as rm', 'rm.matter_id', '=', 'ja.JobAssignmentID')
					->leftJoin('customer as cm', 'cm.CustomerID', '=', 'ja.CustomerID')
					->leftJoin('user as usr', 'usr.EmployeeID', '=', 'ja.Incharge');
					//->groupBy('ja.JobAssignmentID');

		/* Filters */
		$valid_folder_types = array('untouched', 'green', 'orange', 'red', 'blue');
		$filter_array['folder_types'] = (isset($filter_array['folder_types'])) ? $filter_array['folder_types'] : null;

		if(!is_null($filter_array['folder_types']) || isset($filter_array['folder_types'])) {
			$ctr = 1;
			foreach($filter_array['folder_types'] as $folder_type) {
				if(in_array($folder_type, $valid_folder_types)) {
					if($ctr == 1)
						$query->Having('matter_type', '=', $folder_type);
					else
						$query->orHaving('matter_type', '=', $folder_type);
					$ctr++;
				}
			}
		}

		if($user_id !== false) {
		//if(!in_array($user_id, Config::get('oln.admin')))//ids for admin
		//if(!User::hasRoleById($user_id, 'Admin'))//ids for admin
		if(!User::isAdmin($user_id))//ids for admin
			$query->where('usr.user_id', '=', $user_id);
		}
		
		if(isset($filter_array['handler_id'])) $query->where('ja.Incharge', '=', $filter_array['handler_id']);
		if(isset($filter_array['handler_id_array'])) $query->whereIn('ja.Incharge', $filter_array['handler_id_array']);

		if(isset($filter_array['total_unbilled_amount_range'])) {
			if(isset($filter_array['total_unbilled_amount_range']['to']) && !isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '<=', $filter_array['total_unbilled_amount_range']['to']);
			if(!isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '>=', $filter_array['total_unbilled_amount_range']['from']);
			if(isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->havingBetween('total_unbilled_amount', array($filter_array['total_unbilled_amount_range']['from'], $filter_array['total_unbilled_amount_range']['to']));
		}

		if(isset($filter_array['workdate_year'])) {
			//$query->groupBy('rm.workdate_year');
			//$query->where('rm.workdate_year', '=', $filter_array['workdate_year']);
                        $range_end = $filter_array['workdate_year'] + 1;
			//$query->groupBy('rm.workdate_year');
			//$query->where('rm.workdate_year', '=', $filter_array['workdate_year']);
                        $query->whereRaw('rm.trans_date BETWEEN "'.$filter_array['workdate_year'].'-07-01" AND "'.$range_end.'-06-30"');
		}

		if(isset($filter_array['remember_query'])) {
			$query->remember($filter_array['remember_query']);
		}

		if(isset($filter_array['group_by'])) {
			foreach ($filter_array['group_by'] as $groupby_column) {
				$query->groupBy($groupby_column);
			}
		} else {
			$query->groupBy('ja.JobAssignmentID');
		}

		if(isset($filter_array['matter_id_like']) && isset($filter_array['matter_desc_like'])) {
			$query->where(function($query) use ($filter_array) {
				$query->where('ja.JobAssignmentID', 'like', '%'.$filter_array['matter_id_like'].'%');
				$query->orWhere('ja.Description_A', 'like', '%'.$filter_array['matter_desc_like'].'%');
			});
		}

		if(isset($filter_array['dt-search'])) {
			$query->where(function($query) use ($filter_array) {
				$query->where('ja.JobAssignmentID', 'like', '%'.$filter_array['dt-search'].'%');
				$query->orWhere('ja.CustomerID', 'like', '%'.$filter_array['dt-search'].'%');
				$query->orWhere('CompanyName_A1', 'like', '%'.$filter_array['dt-search'].'%');
				$query->orWhere('ja.Description_A', 'like', '%'.$filter_array['dt-search'].'%');
				$query->orWhere('ja.Incharge', 'like', '%'.$filter_array['dt-search'].'%');
			});
		}

		/* /Filters */

		return $query;		
	}

	public function getList($user_id, $page = 1, $filter_array = array())
	{
		$item_per_page = 12;
		$skip = ($page == 1) ? 0 : ($page - 1) * $item_per_page;

		$filter_array['sortby'] = (isset($filter_array['sortby'])) ? $filter_array['sortby'] : 'ja.JobAssignmentID';
		$filter_array['order'] = (isset($filter_array['order'])) ? $filter_array['order'] : 'asc';		
		
		$query = $this->prepareQueryForGetList($user_id, $filter_array);
		
		$query->skip($skip)
				->take($item_per_page)
				->orderBy($filter_array['sortby'], $filter_array['order']);

		$data = $query
				//->remember(5)
				->get();
		$query_count = DB::select(DB::raw('select FOUND_ROWS() as matter_count'));
		foreach ($query_count as $qc) $count = $qc->matter_count;

		$result = array(
			'total_count' => $count,
			'data' => $data
		);

		//return $query_final->get();
		return $result;
	}

	public function getChartData($user_id, $filter_array = array())
	{
		$final_result = array();
		$billed_amounts = $billable_amounts = $unbilled_amounts =  array();

		/*
		$months = array(
			1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun',
			7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec',
		);
		*/

		$months_temp = array(
			7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Oct', 11 => 'Nov', 12 => 'Dec',
			1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'May', 6 => 'Jun',
		);

		$months = array();
		foreach($months_temp as $month_num => $month) {
			if($month_num >= 7) $months[$month_num] = $month . ' ' . $filter_array['workdate_year'];
			else 				$months[$month_num] = $month . ' ' . ($filter_array['workdate_year']+1);
		}

		$query = $this->prepareQueryForGetList($user_id, $filter_array);

		$raw_result = $query->orderBy($filter_array['sortby'],$filter_array['order'])->get();

		foreach($months as $month_num => $month) {
			$temp_billed_amount = $temp_billable_amount = $temp_unbilled_amount = 0;
			foreach($raw_result as $result) {
				if($result->workdate_month == $month_num) {
					$temp_billed_amount = $result->total_billed_amount;
					$temp_billable_amount = $result->total_billable_amount;
					$temp_unbilled_amount = $result->total_unbilled_amount;
					break;
				}
			}
			$billed_amounts[] = intval($temp_billed_amount);
			$billable_amounts[] = intval($temp_billable_amount);
			$unbilled_amounts[] = intval($temp_unbilled_amount);
		}

		$final_result['months'] = array_values($months);
		$final_result['total_billed_amount'] = $billed_amounts;
		$final_result['total_billable_amount'] = $billable_amounts;
		$final_result['total_unbilled_amount'] = $unbilled_amounts;

		return $final_result;
	}

	public function prepareQueryForGetTimesheet($user_id, $filter_array = array())
	{

		$select_append = '';
		if(isset($filter_array['group_by'])) {
			$select_append .= '
				,sum((tsw.WorkHour * 60) / '.Config::get('oln.invoice.minutes_per_unit').') as sum_units
				,sum(tsw.HourlyRate * tsw.WorkHour) as sum_amount
			';
		}


		$query = DB::table('timesheet as ts')
						->select(
							DB::raw('
								ts.UniqueID
								-- ,tsw.UniqueID
								,ts.JobAssignmentID
								,ts.WorkDate
								,ts.Status
								,ts.User_C
								,ts.Date_C

								,tsw.TimeCode
								,tsw.EmployeeID
								,tsw.WorkHour
								-- ,(tsw.WorkHour * 60) / 5 as units
								,tsw.ChargeHour
								,tsw.Completed
								,tsw.HourlyRate
								,tsw.ChargeRate
								,tsw.Comment
								,tsw.BillingStatus 
								,tsw.BillingUpdatedBy
								,tsw.BillingUpdatedOn
								,if(tsw.EmployeeID = ? or ?,1,0) as owned
								,if(ind.ReferenceID is null,1,0) as update_delete

								,(tsw.WorkHour * 60) / '.Config::get('oln.invoice.minutes_per_unit').' as units
								,(tsw.HourlyRate * tsw.WorkHour) as amount
								,if(ind.ReferenceID is null, 0, 1) as billed'
								. $select_append
							)
						)
						->leftJoin('timesheetwork as tsw', 'tsw.UniqueID', '=', 'ts.UniqueID')
						->leftJoin('jobassignmentbilling as jab', 'jab.UniqueID', '=', 'tsw.UniqueID')
						->leftJoin('invoicedetail as ind', 'ind.ReferenceID', '=', 'ts.UniqueID');

		/* Filters */
		$user = new User();
		$employeeID = $user->getEmployeeID($user_id);
		//if(!in_array($user_id, Config::get('oln.admin'))) {
		//if(!User::hasRoleById($user_id, 'Admin')) {
		if(!User::isAdmin($user_id)) {
			$query->setBindings(array($employeeID, 0));
		} else $query->setBindings(array($employeeID, 1));

		if(isset($filter_array['matter_id'])) $query->where('ts.JobAssignmentID', $filter_array['matter_id']);
		if(isset($filter_array['force_no_result'])) $query->where(DB::raw('1 = 2'));
		if(isset($filter_array['timesheet_date'])) {
			$timesheet_date = date('Y-m-d', strtotime($filter_array['timesheet_date']));
			$query->where('ts.WorkDate', $timesheet_date);
		}
		if(isset($filter_array['unbilled_only'])) {
			$query->whereNull('ind.ReferenceID');
			$query->whereNotNull('jab.UniqueID');
		}

		if( isset($filter_array['billable_unbillable']) ) {
			$query->whereNull('ind.ReferenceID');
		}

		if(isset($filter_array['billed_only'])) {
			$query->whereNotNull('ind.ReferenceID');
		}

		$from_date = (isset($filter_array['from_date'])) 	? date('Y-m-d', strtotime($filter_array['from_date']))	: false;
		$to_date   = (isset($filter_array['to_date'])) 		? date('Y-m-d', strtotime($filter_array['to_date'])) 	: false;
		
		if($from_date)
			$query->where('ts.WorkDate', '>=', $from_date);
		if($to_date)
			$query->where('ts.WorkDate', '<=', $to_date);

		if(isset($filter_array['handler_id']))
			$query->where('tsw.EmployeeID', $filter_array['handler_id']);
		if(isset($filter_array['handler_id_array']))
			$query->whereIn('tsw.EmployeeID', $filter_array['handler_id_array']);

		if(isset($filter_array['invoice_number'])) {
			$query->where('ind.UniqueID', '=', $filter_array['invoice_number']);
		}

		if(isset($filter_array['group_by'])) 
			$query->groupBy( $filter_array['group_by'] );

		if(isset($filter_array['sort_by']) && isset($filter_array['sort_order']))
			$query->orderBy($filter_array['sort_by'], $filter_array['sort_order']);


        if(isset($filter_array['create_date'])){
            $query->where('ts.Date_C','like', $filter_array['create_date'].'%');
        }

        if(isset($filter_array['exclude_matters'])) {
        	$query->whereNotIn('ts.JobAssignmentID', $filter_array['exclude_matters']);
        }

        if(isset($filter_array['only_included_matters'])) {
        	$query->whereIn('ts.JobAssignmentID', $filter_array['only_included_matters']);
        }

        if(isset($filter_array['only_included_timesheets'])) {
        	$query->whereIn('ts.UniqueID', $filter_array['only_included_timesheets']);
        }
        /* /Filters */

		return $query;
	}

	public function getTimesheet()
	{

	}

	public function getCountByFolderType($user_id, $filter_array = array())
	{
		//$show_all_user = (in_array($user_id, Config::get('oln.admin'))) ? 1 : 0;
		//$show_all_user = (User::hasRoleById($user_id, 'Admin')) ? 1 : 0;
		$show_all_user = (User::isAdmin($user_id)) ? 1 : 0;

		if(isset($filter_array['workdate_year'])) {
			$year = $filter_array['workdate_year'];
			$show_all_year = 0;
		} else {
			$year = null;
			$show_all_year = 1;
		}

		$handler_filter = '';
		if(isset($filter_array['handler_id'])) {
			$handler_filter = ' and ja.Incharge = "' . $filter_array['handler_id']. '"';
		}


		$result = DB::select('
		select 
				t1.matter_type, count(*) as total_count
			from (
				select 
					ja.JobAssignmentID, @last_trans_date:=(
			        SELECT WorkDate FROM timesheet
			        WHERE JobAssignmentID = ja.JobAssignmentID
			        ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1) AS last_trans_date,
					CASE 
						WHEN COALESCE(TIMESTAMPDIFF(WEEK,@last_trans_date,CURDATE()),-1) = -1 and ja.Completed = 0 THEN "untouched"
						WHEN COALESCE(TIMESTAMPDIFF(WEEK,@last_trans_date,CURDATE()),-1) <= 4 and ja.Completed = 0 THEN "green"
						WHEN COALESCE(TIMESTAMPDIFF(WEEK,@last_trans_date,CURDATE()),-1) < 8 and ja.Completed = 0 THEN "orange"
						WHEN COALESCE(TIMESTAMPDIFF(WEEK,@last_trans_date,CURDATE()),-1) > 8 and ja.Completed = 0 THEN "red"
						WHEN ja.Completed = 1 THEN "blue"
					END AS matter_type
				from jobassignment as ja
					left join timesheet as ts on ts.JobAssignmentID = ja.JobAssignmentID
					left join user as usr on usr.EmployeeID = ja.Incharge
				where 
						(year(ts.WorkDate) = ? or ?)
					and (usr.user_id = ? or ?)
					'.$handler_filter.'
				group by 
					ja.JobAssignmentID
			) as t1
			group by t1.matter_type
		', array($year, $show_all_year, $user_id, $show_all_user));

		return $result;
	}

	public function getInfo($matter_id)
	{
		$result = DB::table('jobassignment as ja')
						->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'ja.Incharge')
						->leftJoin('customer as cust', 'cust.CustomerID', '=', 'ja.CustomerID')
						->leftJoin('user as usr', 'usr.EmployeeID','=', 'emp.EmployeeID')
						->leftJoin('timesheet as ts', 'ts.UniqueID', '=', 'ts.TimeCode')
						->select('ja.Description_A', 'ja.Incharge', 'emp.NickName', 'ja.CustomerID', 'cust.CompanyName_A1', 'usr.email_address','ja.BillingCode','ja.DesignatedDate','ja.Assistant', 'usr.user_id')
						->where('ja.JobAssignmentID', $matter_id)
						->first();
		return $result;
	}

	public function getInvoiceBalance($matter_id_array)
	{
		$inner_query = DB::table('invoicehead as inh')
							->select(DB::raw('
								inh.UniqueID,
								ts.JobAssignmentID,
								-- coalesce(ip.balance,sum(ind.Amount) + COALESCE(id.total_disbursement,0)) as total_matter_balance
								coalesce(ip.balance, (COALESCE(inh.agreed_cost, inh.total_cost, sum(ind.Amount), 0) + COALESCE(id.total_disbursement,inh.total_disbursements, 0)) - COALESCE(il.total_less, inh.total_less, 0) ) as total_matter_balance
							'))
							->leftJoin('invoicedetail as ind', 'ind.UniqueID', '=', 'inh.UniqueID')
							->leftJoin('timesheet as ts', 'ts.UniqueID', '=', 'ind.ReferenceID')
							->leftJoin('invoice_payment as ip', 'ip.batch_number', '=', 'inh.BatchNumber')

							// Added to include disbursements
							->leftJoin(DB::raw('
								(select id.invoice_id, sum(id.amount) as total_disbursement from invoice_disbursement as id group by id.invoice_id) as id
							'), 'id.invoice_id', '=', 'inh.UniqueID')

							// Added to include less
							->leftJoin(DB::raw('
								(select il.invoice_id, sum(il.amount) as total_less from invoice_less as il group by il.invoice_id) as il
							'), 'il.invoice_id', '=', 'inh.UniqueID')							

							->whereIn('ts.JobAssignmentID', $matter_id_array)
							->groupBy('inh.UniqueID');
		$result = DB::select('select t1.JobAssignmentID, round(sum(t1.total_matter_balance),2) as total_invoice_balance from ('.$inner_query->toSql().') as t1 group by t1.JobAssignmentID', $inner_query->getBindings());

		return $result;
	}

	public function exists($matter_id)
	{
		$result = DB::table('jobassignment')->where('JobAssignmentID', $matter_id)->count();
		return ($result > 0) ? true : false;
	}

	public function listMatter($user_id, $filter_array = array())
	{		
		$principal_idObj = User::find($user_id);
		//$show_all = (in_array($user_id, Config::get('oln.admin'))) ? true : false;
		//$show_all = (User::hasRoleById($user_id, 'Admin')) ? true : false;
		$show_all = (User::isAdmin($user_id)) ? true : false;
		$query = DB::table('jobassignment')->select('JobAssignmentID','Description_A');
		if(!$show_all) {
			if( $principal_idObj->hasRole('Handler') )
				$query->where('Incharge', $principal_idObj->EmployeeID);
			elseif( $principal_idObj->hasRole('PersonalAssistant') )
				$query->whereIn('Incharge', Session::get('principal_employees'));
			/*
			$user = new User();
			$employee_id = $user->getEmployeeID($user_id);
			$query->where('Incharge', $employee_id);
			*/
		}

		/* Filters */
		if(isset($filter_array['is_complete'])) {
			$query->where('Completed', $filter_array['is_complete']);
		}

		if(isset($filter_array['include_completed_status'])) {
			$query->whereIn('Completed', $filter_array['include_completed_status']);
		}

		if(isset($filter_array['exclude_completed_status'])) {
			$query->whereNotIn('Completed', $filter_array['exclude_completed_status']);
		}		

		/* /Filters */

		return $query->get();
	}

	public function updateMatter($job_id,$data)
	{
			$user_result = DB::table('jobassignment')->where('JobAssignmentID', $job_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}

	
}
<?php

class Sysadmin{
    /*
     * get all users online
     */
    public function getUsersOnline()
    {
        return DB::select('
                        SELECT
                        a.ip_address,
                        b.user_id,
                        b.EmployeeID,
                        b.first_name,
                        b.last_name,
                        MAX(a.last_activity) AS last_activity,
                        c.Photo
                        FROM 
                        sessions AS a
                        LEFT JOIN 
                        user AS b
                        ON b.user_id = a.user_id
                        LEFT JOIN
                        employee AS c
                        ON b.EmployeeID = c.EmployeeID
                        WHERE a.user_id IS NOT NULL
                        GROUP BY a.user_id
                    ');
    }
    
    public function getUsers()
    {
        return DB::select('
                        SELECT
                        a.Photo,
                        b.*,
                        (SELECT
                             CONCAT("[",
                                  GROUP_CONCAT(
                                       CONCAT("{\"role_id\":\"",c.role_id,"\",\"role\":\"",d.name,"\"}")
                                   ORDER BY d.name)
                             ,"]") FROM assigned_roles AS c LEFT JOIN roles AS d ON d.id = c.role_id WHERE c.user_id = b.user_id)
                            AS user_roles
                        FROM
                        employee AS a
                        LEFT JOIN
                        user AS b
                        ON b.EmployeeID = a.EmployeeID                        
                        WHERE a.isActive = ?
                        ORDER BY a.EmployeeID ASC
                    ',array('1'));
    }
    
    public function resetPassword($user_id)
    {
        $data = array(
                'password'  => Hash::make('123456'),
                'changed_password'  => '0'
            );
        return DB::table('user')->where('user_id', $user_id)->update($data);
    }
    
    public function getUserLoginHistory($user_id)
    {
        return DB::select('
            SELECT
            *
            FROM
            user_login_history
            WHERE
            user_id = ?
            ORDER BY login DESC
        ',array($user_id));
    }
}


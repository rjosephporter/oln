<?php

class Billing {

	public function getTotalUnbilledAmount($user_id, $filter_array = array())
	{
		$query = DB::table('jobassignment AS ja')
					->select(
						DB::raw('
							ja.Incharge as EmployeeID,
							rm.workdate_year,
							rm.workdate_month,
							rm.workdate_day,
							concat(MONTHNAME(min(str_to_date(concat(rm.workdate_year,"-",rm.workdate_month,"-",rm.workdate_day), "%Y-%m-%d"))), " ", YEAR(min(str_to_date(concat(rm.workdate_year,"-",rm.workdate_month,"-",rm.workdate_day), "%Y-%m-%d")))) as earliest_date,
							concat(MONTHNAME(max(str_to_date(concat(rm.workdate_year,"-",rm.workdate_month,"-",rm.workdate_day), "%Y-%m-%d"))), " ", YEAR(max(str_to_date(concat(rm.workdate_year,"-",rm.workdate_month,"-",rm.workdate_day), "%Y-%m-%d")))) as latest_date, 
							COALESCE(sum(rm.total_billed_amount),0) as total_billed_amount,
							COALESCE(sum(rm.total_billable_amount),0) as total_billable_amount,
							COALESCE(sum(rm.total_unbilled_amount),0) as total_unbilled_amount
						')
					)
					->leftJoin('report_matters AS rm', 'rm.matter_id', '=', 'ja.JobAssignmentID')
					//->leftJoin('timesheet as ts', 'ts.JobAssignmentID', '=', 'ja.JobAssignmentID')
					->leftJoin('user AS usr', 'usr.EmployeeID', '=', 'ja.Incharge')
					->take(1);
		
		/* Filters */
		//if(!in_array($user_id, Config::get('oln.admin'))) {					
		//if(!User::hasRoleById($user_id, 'Admin')) {
		if(!User::isAdmin($user_id)) {
			$query->where('usr.user_id', '=', $user_id)
					->groupBy('ja.Incharge');
		}

		if(isset($filter_array['workdate_year'])) {
                        $range_end = $filter_array['workdate_year'] + 1;
			//$query->groupBy('rm.workdate_year');
			//$query->where('rm.workdate_year', '=', $filter_array['workdate_year']);
                        $query->whereRaw('rm.trans_date BETWEEN "'.$filter_array['workdate_year'].'-07-01" AND "'.$range_end.'-06-30"');
		}

		/* /Filters */

		return $query->first();		
	}

	public function prepareQueryForGetInvoice($filter_array = array())
	{

		$select_append = array();
		if(isset($filter_array['invoice_payment'])) {
			$select_append[] = '
				-- if(ip.balance is null, round(sum(ind.Amount) + COALESCE(id.total_disbursement,0),2), round(ip.balance,2) ) as balance
				round(coalesce(ip.balance, (COALESCE(inh.agreed_cost, inh.total_cost, sum(ind.Amount), 0) + COALESCE(id.total_disbursement,inh.total_disbursements, 0)) - COALESCE(il.total_less, inh.total_less, 0) ),2) as balance
				,if(ip.status_id is null, 0, ip.status_id) as status_id
				,datediff(date(now()), inh.InvoiceDate) as age_in_days
				,if(datediff(date(now()), inh.InvoiceDate) >= 30 and (ip.status_id is null or ip.status_id <> 2), 1, 0) as remind_unpaid
			
				,round(sum(ind.Amount) - coalesce(ip.balance,sum(ind.Amount)),2) as paid_amount

				,case
					when ip.status_id = 2 then \'white\'
					when coalesce(ip.status_id,0) in (0,1) and datediff(date(now()), inh.InvoiceDate) < 30  and (select count(*) from invoice_payment_reminder where batch_number = inh.BatchNumber) = 0 then \'green\'
					when coalesce(ip.status_id,0) in (0,1) and datediff(date(now()), inh.InvoiceDate) >= 30  and (select count(*) from invoice_payment_reminder where batch_number = inh.BatchNumber) = 0 then \'yellow\'
					when coalesce(ip.status_id,0) in (0,1) and (select count(*) from invoice_payment_reminder where batch_number = inh.BatchNumber) = 1 then \'orange\'
					else \'red\'
				end as row_color

				,(select GROUP_CONCAT(date_created) from invoice_payment_reminder where batch_number = inh.BatchNumber order by date_created asc) as reminder_log				
			';
		}

		if(isset($filter_array['invoice_balance'])) {
			$select_append[] = '
				inh.matter_id as matter_id
				,DATEDIFF(date(now()),inh.InvoiceDate) as invoice_age
				,case
					when DATEDIFF(date(now()),inh.InvoiceDate) < 42 then \'green\'
					when DATEDIFF(date(now()),inh.InvoiceDate) >= 42 and DATEDIFF(date(now()),inh.InvoiceDate) <= 56 then \'orange\'
					else \'red\'
				 end as color_code
				,(select round(sum(total_billed_amount) + TotalDisbursementByMatter(inh.matter_id),2) from report_matters where matter_id = inh.matter_id) as matter_total_billed_amount
				-- ,round(coalesce(ip.balance,sum(ind.Amount) + COALESCE(id.total_disbursement,0)),2) as invoice_balance
				,round(coalesce(ip.balance, (COALESCE(inh.agreed_cost, inh.total_cost, sum(ind.Amount), 0) + COALESCE(id.total_disbursement,inh.total_disbursements, 0)) - COALESCE(il.total_less, inh.total_less, 0) ),2) as invoice_balance
				-- ,TotalBalanceByMatter(ts.JobAssignmentID) as matter_total_balance
				,\'Loading...\' as matter_total_balance
			';
		}

		$select_append = implode(',', $select_append);

		$query = DB::table('invoicehead as inh')
					->select(
						DB::raw('
							inh.source
							,inh.UniqueID
							,inh.BatchNumber
							,inh.CustomerID
							,inh.InvoiceDate
							,inh.User_C
							,inh.Date_C
							,inh.InvoiceType
							,inh.matter_id as matter_id_for_accounting
							-- ,round(sum(ind.Amount) + COALESCE(id.total_disbursement,0),2) as total_amount

							,round(COALESCE(inh.agreed_cost, inh.total_cost, sum(ind.Amount), 0),2) as total_cost
							,round(COALESCE(id.total_disbursement,inh.total_disbursements, 0),2) as total_disbursements
							,round(COALESCE(il.total_less, inh.total_less, 0),2) as total_less		

							,round((COALESCE(inh.agreed_cost, inh.total_cost, sum(ind.Amount), 0) + COALESCE(id.total_disbursement,inh.total_disbursements, 0)) - COALESCE(il.total_less, inh.total_less, 0),2) as total_amount
							,cust.CompanyName_A1
							,inh.Status
							,inh.matter_id as invoice_matter_id
							,inh.reference_number
							,inh.new_invoice_number,
						' . $select_append)
					)
					->leftJoin('invoicedetail as ind', 'ind.UniqueID', '=', 'inh.UniqueID')
					->leftJoin('customer as cust', 'cust.CustomerID', '=', 'inh.CustomerID')

					// Added to include disbursements
					->leftJoin(DB::raw('
						(select id.invoice_id, sum(id.amount) as total_disbursement from invoice_disbursement as id group by id.invoice_id) as id
					'), 'id.invoice_id', '=', 'inh.UniqueID')

					// Added to include less
					->leftJoin(DB::raw('
						(select il.invoice_id, sum(il.amount) as total_less from invoice_less as il group by il.invoice_id) as il
					'), 'il.invoice_id', '=', 'inh.UniqueID')

					//->where('inh.Status', '>=', 0) // don't include drafts
					->where('source', 'OEMS') // show only invoices created from OEMS
					->groupBy('inh.UniqueID');

		/* Filters */
		$from_date = (isset($filter_array['from_date'])) 	? date('Y-m-d', strtotime($filter_array['from_date']))	: false;
		$to_date   = (isset($filter_array['to_date'])) 		? date('Y-m-d', strtotime($filter_array['to_date'])) 	: false;
		
		if($from_date)
			$query->where('inh.InvoiceDate', '>=', $from_date);
		if($to_date)
			$query->where('inh.InvoiceDate', '<=', $to_date);

		if(isset($filter_array['invoice_payment'])) {
			$query->leftJoin('invoice_payment as ip', 'ip.batch_number', '=', 'inh.BatchNumber');
			//$query->leftJoin('invoice_payment_reminder as ipr', 'ipr.batch_number', '=', 'inh.BatchNumber');
		}

		if(isset($filter_array['invoice_balance'])) {
			//$query->leftJoin('timesheet as ts', 'ts.UniqueID', '=', 'ind.ReferenceID');
			$query->leftJoin('invoice_payment as ip', 'ip.batch_number', '=', 'inh.BatchNumber');
		}

		if(isset($filter_array['status_array'])) {
			$query->whereIn('inh.Status', $filter_array['status_array']);
		} else {
			$query->whereNotIn('inh.Status', array(-9, -1));
		}

		if(isset($filter_array['invoice_payment_status']) && $filter_array['invoice_payment_status'] != -1) {
			//$query->having('status_id', '=', $filter_array['invoice_payment_status']);
			$query->having('row_color', '=', $filter_array['invoice_payment_status']);
		}

		if(isset($filter_array['invoice_unpaid_days']) && $filter_array['invoice_unpaid_days'] != -1) {
			$query->having('remind_unpaid', '=', $filter_array['invoice_unpaid_days']);
		}

		if(isset($filter_array['remember_query'])) {
			$query->remember($filter_array['remember_query']);
		}

		if(isset($filter_array['status'])) {
			switch ($filter_array['status']) {
				case '1': //Open Invoice
					//$query->having('invoice_balance', '>', 0);
					$query->where('inh.Status', '=', 0);
					break;
				case '2': //Open Matter
					# code...
					break;
				case '3': //Closed Invoice
					//$query->having('invoice_balance', '<=', 0);
					$query->where('inh.Status', '=', 1);
					break;
				case '4': //Closed Matter
					# code...
					break;	
				case '5': //Draft Invoice
					$query->where('inh.Status', '=', -2);
					break;
				default:
					# code...
					break;
			}
		}

		if(isset($filter_array['matter_id'])) {
			$query->where('inh.matter_id', $filter_array['matter_id']);
		}

		if(isset($filter_array['invoice_id'])) {
			$query->where('inh.UniqueID', $filter_array['invoice_id']);
		}
		/* /Filters */

		return $query;
	}

	public function prepareQueryForGetInvoiceDetail($filter_array = array())
	{
		$query = DB::table('invoicedetail as ind')
						->select(DB::raw('ind.*, round(Amount,2) as rounded_amount, tsw.EmployeeID, tsw.HourlyRate, tsw.WorkHour'));

		/* Filters */
		if(isset($filter_array['timesheet']['join']) && $filter_array['timesheet']['join'] == true) {
			$query->leftJoin('timesheet as ts', 'ts.UniqueID', '=', 'ind.ReferenceID')
					->leftJoin('timesheetwork as tsw', 'tsw.UniqueID', '=', 'ts.UniqueID')
					->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'tsw.EmployeeID')
					->select(
						DB::raw('
							ts.UniqueID,
							ts.WorkDate,
							tsw.EmployeeID,
							emp.NickName,
							ind.Description,
							sum(tsw.WorkHour) as work_hour,
							tsw.HourlyRate,
							-- sum(tsw.WorkHour * tsw.HourlyRate) as total_amount,
							sum(ind.Amount) as total_amount,
							sum(if(ind.complimentary = 1, 0, ind.Amount)) as total_amount_non_complimentary,
							sum(round(((tsw.WorkHour * 60) / 5))) as units,
							sum(round(((tsw.WorkHour * 60) / 5)) - coalesce(ind.complimented_units,0)) as charged_units,

							ts.JobAssignmentID,
							tsw.WorkHour,
							tsw.Comment	,
							ind.complimentary,
							ind.complimented_units					
						')
					)
					->orderBy($filter_array['timesheet']['order_by'], $filter_array['timesheet']['sort_order'])
					->groupBy($filter_array['timesheet']['group_by']);
		} else {
			$query->leftJoin('timesheet as ts', 'ts.UniqueID', '=', 'ind.ReferenceID')
					->leftJoin('timesheetwork as tsw', 'tsw.UniqueID', '=', 'ts.UniqueID');
		}


		if(isset($filter_array['invoice_id'])) 
			$query->where('ind.UniqueID', '=', $filter_array['invoice_id']);
		if(isset($filter_array['sort_by']) && isset($filter_array['order'])) 
			$query->orderBy($filter_array['sort_by'], $filter_array['order']);

		if(isset($filter_array['remember_query'])) {
			$query->remember($filter_array['remember_query']);
		}		
		/* /Filters */

		return $query;
	}

	public function prepareQueryForGetInvoiceByMatter($filter_array = array())
	{
		$query = DB::table('jobassignment as ja')
					->leftJoin('invoice_client as inc', 'inc.matter_id', '=', 'ja.JobAssignmentID')
					->leftJoin('customer as cust', 'cust.CustomerID', '=', 'ja.CustomerID')
					->select(DB::raw('
						ja.JobAssignmentID,
						ja.Description_A,
						ja.CustomerID,
						cust.CompanyName_A1,
						coalesce((select sum(total_billed_amount) from report_matters where matter_id = ja.JobAssignmentID),0) as total_invoice,
						coalesce(sum(inc.total),0) as total_balance
					'))
					->groupBy('ja.JobAssignmentID')
					->remember(5);

		return $query;
	}

	public function payInvoice($batch_number, $payment_amount, $payment_date)
	{
		$invoice_total = $this->getInvoiceTotal($batch_number);
		$employee_id = Auth::user()->EmployeeID;

		$invoice_payment_detail = $this->isExistInvoicePayment($batch_number);

		$log_result = $this->logInvoicePayment($batch_number, $payment_amount, $payment_date);
		$total_paid_amount = $this->getInvoiceTotalPaidAmount($batch_number, 2);

		if(!$invoice_payment_detail) {
			//$balance = $invoice_total - $payment_amount;
			$balance = $invoice_total - $total_paid_amount;
			$status_id = ($balance > 0) ? 1 : 2;
			$data = array(
				'batch_number' 	=> $batch_number,
				'status_id'		=> $status_id,
				'total_paid_amount' => $total_paid_amount,
				'balance'		=> $balance,
				'payment_date'	=> $payment_date,
				'created_by'	=> $employee_id
			);

			$result = DB::table('invoice_payment')->insert($data);	
		} else {
			//$balance = $invoice_payment_detail->balance - $payment_amount;
			$balance = $invoice_total - $total_paid_amount;
			$status_id = ($balance > 0) ? 1 : 2;
			$data = array(
				'status_id'		=> $status_id,
				'total_paid_amount' => $total_paid_amount,
				'balance'		=> $balance,
				'payment_date'	=> $payment_date,
				'updated_by'	=> $employee_id,
				'last_update'	=> date('Y-m-d H:i:s')
			);

			$result = DB::table('invoice_payment')->where('batch_number', $batch_number)->update($data);
		}

		return $this->isExistInvoicePayment($batch_number);
	}

	public function logInvoicePayment($batch_number, $payment_amount, $payment_date)
	{
		$invoice_number = $this->getInvoiceNumber($batch_number);
		$employee_id = Auth::user()->EmployeeID;

		$data = array(
			'invoice_number' 	=> $invoice_number,
			'batch_number'		=> $batch_number,
			'paid_amount'		=> $payment_amount,
			'payment_date'		=> $payment_date,
			'created_by'		=> $employee_id,
			'created_at'		=> date('Y-m-d H:i:s')
		);

		$result = DB::table('invoice_payment_log')->insert($data);

		return $result;
	}	

	public function isExistInvoicePayment($batch_number)
	{
		$count = DB::table('invoice_payment')->where('batch_number', $batch_number)->count();
		if($count > 0) {
			$result = DB::table('invoice_payment')->where('batch_number', $batch_number)->first();
		} else {
			$result = false;
		}

		return $result;
	}

	public function getInvoiceTotal($batch_number)
	{
		$result = DB::table('invoicehead as inh')
						->select(DB::raw('inh.BatchNumber, round(sum(ind.Amount),2) as total_amount'))
						->leftJoin('invoicedetail as ind', 'ind.UniqueID', '=', 'inh.UniqueID')
						->where('inh.BatchNumber', $batch_number)
						->groupBy('inh.UniqueID')
						->first();
		return $result->total_amount;
	}

	public function deleteInvoice($batch_number)
	{
		$result = DB::table('invoicehead')->where('BatchNumber', $batch_number)->update(array('Status' => -9));

		return $result;
	}

	public function getTotalAccountsReceivable($user_id, $filter_array = array())
	{
		$inner_query = DB::table('customer as ctm')
							->select(DB::raw('
								ctm.CustomerID
								,sum((select sum(Amount) from invoicedetail where UniqueID = inh.UniqueID)) as total_amount
								,round(sum(ip.balance),2) as total_balance
							'))
							->leftJoin('invoicehead as inh', 'inh.CustomerID', '=', 'ctm.CustomerID')
							->leftJoin('invoicebatch as inb', 'inb.BatchNumber', '=', 'inh.BatchNumber')
							->leftJoin('invoice_payment as ip', 'ip.batch_number', '=', 'inh.BatchNumber')
							->leftJoin('user as usr', 'usr.EmployeeID', '=', 'ctm.Incharge')
							->groupBy('ctm.CustomerID');

		/* Filters */
		//if(!in_array($user_id, Config::get('oln.admin')))//ids for admin
		//if(!User::hasRoleById($user_id, 'Admin'))
		if(!User::isAdmin($user_id))
			$inner_query->where('usr.user_id', '=', $user_id);		
		
		if(isset($filter_array['workdate_year'])) {
			$date_range = array($filter_array['workdate_year'].'-07-01', ($filter_array['workdate_year']+1).'-06-30');
			$inner_query->whereBetween('inh.InvoiceDate', $date_range);
		}
		/* /Filters */

		$result = DB::select('
			select
				-- t1.CustomerID
				-- ,t1.total_amount
				sum(COALESCE(t1.total_balance, t1.total_amount)) as total_balance
			from
			('.$inner_query->toSql().') as t1				
		', $inner_query->getBindings());

		return $result[0];
	}

	public function logInvoiceReminder($batch_number)
	{
		$data = array('batch_number' => $batch_number);
		
		//$query = DB::table('invoice_payment_reminder')->insert($data);
		$id = DB::table('invoice_payment_reminder')->insertGetId($data);

		//$result = DB::table('invoice_payment_reminder')->where('batch_number', '=', $batch_number)->count();
		$reminder_count = DB::table('invoice_payment_reminder')->where('batch_number', '=', $batch_number)->count();
		$result = DB::table('invoice_payment_reminder')->where('invoice_payment_reminder_id', $id)->first();
		$result->reminder_count = $reminder_count;

		return $result;
	}

	public function getReminderLog($batch_number)
	{
		$query_result = DB::table('invoice_payment_reminder')
						->where('batch_number', '$batch_number')
						->orderBy('date_created', 'desc')
						->get();

		$ctr = 1;
		$result = array();
		foreach($query_result as $data) {
			$result[$ctr] = $data->date_created;
		}

		return $result;
	}

	public function generateBatchNumber($date = false)
	{
		$date = ($date == false) ? date('Y-m-d') : $date;

		$prefix_batchnumber = str_replace('-', '', $date);

		$last_batchnumber = DB::table('invoicehead')->where('BatchNumber', 'like', $prefix_batchnumber.'%')
								->orderBy('BatchNumber', 'desc')->pluck('BatchNumber');

		if(empty($last_batchnumber)) {
			$batch_number = $prefix_batchnumber.'-001';
		} else {
			$last_batchnumber_array = explode('-', $last_batchnumber);
			$last_item = intval($last_batchnumber_array[1]);
			$next_item = str_pad($last_item+1, 3, '0', STR_PAD_LEFT);
			$batch_number = $prefix_batchnumber.'-'.$next_item;
		}

		return $batch_number;
	}

	public function saveInvoiceHead($matter_id = null, $client_id = null, $invoice_date = null, $created_by = null, $other_data = array())
	{
		if(isset($other_data['custom_unique_id']))
			$unique_id = $other_data['custom_unique_id'];
		else
			$unique_id = HelperLibrary::generateUniqueId();
	
        //Generate new invoice id (effective year 2015)
        $new_id = '';
        if(date('Y') < 2015) {
            $new_id = '/'.date('Y');
        } else {
            $current_invoice_count = DB::table('invoicehead')->whereYear('Date_C', '=', date('Y'))->count();
            $new_id = ($current_invoice_count + 1) . '/' . date('Y');
        }		

		$invoicehead_data = array(
			'CompanyID'			=>	'OLN',
			'UniqueID'			=>	$unique_id,
			'BatchNumber'		=>	$this->generateBatchNumber(),
			'BatchSeqNumber'	=>	'00001',
			'InvoiceType'		=>	'IN',
			'CustomerID'		=>	$client_id,
			'InvoiceDate'		=>	$invoice_date,
			'Status'			=>	'-1',
			'Remarks'			=> (isset($other_data['remarks'])) ? $other_data['remarks'] : null,
			'User_C'			=> $created_by,
			'Date_C'			=> date('Y-m-d H:i:s'),
			'matter_id'			=> $matter_id,
			'new_invoice_number' => $new_id,
			'source'			=> 'OEMS'
		);
		$invoicehead_result = DB::table('invoicehead')->insert($invoicehead_data);

		return DB::table('invoicehead')->where('UniqueID', $unique_id)->first();
	}

	public function updateInvoiceHead($invoice_id, $data)
	{
		$result = DB::table('invoicehead')->where('UniqueID', $invoice_id)->take(1)->update($data);
		return $result;
	}

	public function saveInvoiceBatch($batch_number, $invoice_date, $created_by)
	{
		$invoicebatch_data = array(
			'CompanyID'		=>	'OLN',
			'BatchNumber'	=>	$batch_number,
			'InvoiceType'	=>	'IN',
			'Description'	=>	'',
			'InvoiceDate'	=>	$invoice_date,
			'ApproveStatus' =>	0,
			'FileType'		=>	'DOC',
			'User_C'		=>	$created_by,
			'Date_C'		=>	date('Y-m-d H:i:s')
		);

		$invoicehead_result = DB::table('invoicebatch')->insert($invoicebatch_data);

		return DB::table('invoicebatch')->where('BatchNumber', $batch_number)->first();
	}

	public function updateInvoiceBatch($batch_number, $data)
	{
		$result = DB::table('invoicebatch')->where('BatchNumber', $batch_number)->take(1)->update($data);
		return $result;
	}

	public function saveInvoiceDetail($unique_id, $timesheet_items, $repopulate_table = true)
	{
		if($repopulate_table) {
			//Delete existing invoice items, then repopulate all
			DB::table('invoicedetail')->where('UniqueID', $unique_id)->delete();
		}

		$item_no = $this->getLastInvoiceDetailItemNumber($unique_id) + 1;
		$data = array();
		foreach($timesheet_items as $item) {
			$data[] = array(
				'CompanyID'			=>	'OLN',
				'UniqueID'			=>	$unique_id,
				'ItemLineNo'		=>	$item_no,
				'SourceType'		=>	'J',
				'SourceID'			=>	null,
				'Description'		=>	$item['description'],
				'OriginalAmount'	=>	$item['amount'],
				'ActualAmount'		=>	$item['amount'],
				'DiscountAmount'	=>	0,
				'DiscountPercent'	=>	0,
				'Amount'			=>	$item['amount'],
				'ReferenceID'		=> 	$item['timesheet_id'],
				'NoCharge'			=>	0,
				'complimentary'		=>	isset($item['complimentary']) ? $item['complimentary'] : 0,
				'complimented_units' => empty($item['complimented_units']) ? null : $item['complimented_units']
			);
			$item_no++;
		}

		return DB::table('invoicedetail')->insert($data);
	}

	public function saveInvoiceDisbursement($unique_id, $disbursement_items)
	{
		//Delete existing disbursement items, then repopulate all
		DB::table('invoice_disbursement')->where('invoice_id', $unique_id)->delete();

		$item_no = $this->getLastDisbursementItemNumber($unique_id) + 1;
		$data = array();
		foreach($disbursement_items as $item) {
			$data[] = array(
				'invoice_id'	=>	$unique_id,
				'item_number'	=>	$item_no,
				'description'	=>	$item['description'],
				'amount'		=>	$item['amount'],
				'date_created'	=>	date('Y-m-d H:i:s'),
				'billed'		=>	1
			);
			$item_no++;
		}

		return DB::table('invoice_disbursement')->insert($data);
	}

	public function saveInvoiceLess($unique_id, $less_items)
	{
		//Delete existing less items, then repopulate all
		DB::table('invoice_less')->where('invoice_id', $unique_id)->delete();

		$item_no = $this->getLastLessItemNumber($unique_id) + 1;
		$data = array();
		foreach($less_items as $item) {
			$data[] = array(
				'invoice_id'	=>	$unique_id,
				'item_number'	=>	$item_no,
				'description'	=>	$item['description'],
				'amount'		=>	$item['amount'],
				'date_created'	=>	date('Y-m-d H:i:s')
			);
			$item_no++;
		}

		return DB::table('invoice_less')->insert($data);
	}

	public function saveInvoiceCostsByHandler($unique_id, $cost_items)
	{

		DB::table('invoice_cost_handler')->where('invoice_id', $unique_id)->delete();

		$data = array();
		foreach($cost_items as $handler => $item) {
			$data[] = array(
				'invoice_id'	=> $unique_id,
				'handler_id'	=> $handler,
				'hourly_rate'	=> $item['hourly_rate'],
				'total_work_hours' => $item['total_work_hours'],
				'total_units'	=> $item['units'],
				'complimentary'	=> $item['cost'] == 0 ? 1 : 0,
				'amount'		=> $item['cost'],
				'date_created'	=> date('Y-m-d H:i:s')
			);
		}

		return DB::table('invoice_cost_handler')->insert($data);
	}

	public function saveInvoiceCostsByHandler2($unique_id, $cost_items)
	{

		DB::table('invoice_cost_handler')->where('invoice_id', $unique_id)->delete();

		$data = array();
		foreach($cost_items as $ndx => $item) {
			$data[] = array(
				'invoice_id'	=> $unique_id,
				'handler_id'	=> $item['EmployeeID'],
				'hourly_rate'	=> $item['HourlyRate'],
				//'total_work_hours' => $item['total_work_hours'],
				//'total_units'	=> $item['units'],
				//'complimentary'	=> $item['cost'] == 0 ? 1 : 0,
				'amount'		=> $item['sum_amount'],
				'date_created'	=> date('Y-m-d H:i:s')
			);
		}

		return DB::table('invoice_cost_handler')->insert($data);
	}	

	public function saveInvoice($matter_id, $client_id, $invoice_date, $created_by, $other_data = array(), $invoice_items = array())
	{
		$invoicehead_result			= $this->saveInvoiceHead($matter_id, $client_id, $invoice_date, $created_by, $other_data);
		$invoicebatch_result		= $this->saveInvoiceHead($invoicehead_result->BatchNumber, $invoice_date, $created_by);
		$invoicedetail_result		= $this->saveInvoiceDetail($invoicehead_result->UniqueID, $invoice_items['timesheet']);
		$invoicedisbursement_result = $this->saveInvoiceDisbursement($invoicehead_result->UniqueID, $invoice_items['disbursements']);

		return ($invoicehead_result && $invoicebatch_result && 
				$invoicedetail_result && $invoicedisbursement_result) ? true : false;
	}

	public function invoiceExists($invoice_number)
	{
		$result = DB::table('invoicehead')->where('UniqueID', $invoice_number)->count();
		return ($result > 0) ? true : false;
	}

	public function getInvoiceHead($invoice_number) 
	{
		$result = DB::table('invoicehead')->where('UniqueID', $invoice_number)->first();
		return $result;
	}

	public function getDisbursements($invoice_number) 
	{
		$result = DB::table('invoice_disbursement')->where('invoice_id', $invoice_number)->get();
		return $result;
	}

	public function updateDisbursement($update_data)
	{
		foreach($update_data as $data)
			DB::table('invoice_disbursement')->where('invoice_disbursement_id', $data['invoice_disbursement_id'])->limit(1)->update($data);
	}

	public function getLastInvoiceDetailItemNumber($invoice_number) 
	{
		$result = DB::table('invoicedetail')->where('UniqueID', $invoice_number)
						->orderBy('ItemLineNo', 'desc')->pluck('ItemLineNo');
		return ($result) ? $result : 0;
	}

	public function getLastDisbursementItemNumber($invoice_number)
	{
		$result = DB::table('invoice_disbursement')->where('invoice_id', $invoice_number)
						->orderBy('item_number', 'desc')->pluck('item_number');
		return ($result) ? $result : 0;
	}

	public function getLastLessItemNumber($invoice_number)
	{
		$result = DB::table('invoice_less')->where('invoice_id', $invoice_number)
						->orderBy('item_number', 'desc')->pluck('item_number');
		return ($result) ? $result : 0;
	}

	public function getInvoiceNumber($batch_number)
	{
		return DB::table('invoicehead')->where('BatchNumber', $batch_number)->pluck('UniqueID');
	}

	public function getBatchNumber($invoice_number)
	{
		return DB::table('invoicehead')->where('UniqueID', $invoice_number)->pluck('BatchNumber');
	}

	/**
	 * type 1 = invoice_number, 2 = batch_number
	 *
	 */
	public function getInvoiceTotalPaidAmount($number, $type = 1)
	{
		$column = array(
			1 => 'invoice_number',
			2 => 'batch_number'
		);

		return DB::table('invoice_payment_log')
					->where('status', 1)
					->where($column[$type], $number)
					->sum('paid_amount');
	}

	public function getCostByHandler($invoice_id)
	{
		return DB::table('invoice_cost_handler')->where('invoice_id', $invoice_id)->get();
	}

	public function getInvoiceLessItems($invoice_id)
	{
		return DB::table('invoice_less')->where('invoice_id', $invoice_id)->get();
	}

	public function markAllAsBilled($matter_id)
	{
		//Check if invoicehead exists
		if($this->isExistInvoiceHead($matter_id)) {
			//Create invoicehead & invoicebatch if does not exist
			$invoice_date = date('Y-m-d H:i:s');
			$created_by = Auth::check() ? Auth::user()->EmployeeID : null;
			$other_data['custom_unique_id'] = $matter_id;
			$other_data['remarks'] = 'Dummy invoice for marking all unbilled as billed.';
			$saved_invoicehead = $this->saveInvoiceHead($matter_id, null, $invoice_date, $created_by, $other_data);
			$saved_invoicebatch = $this->saveInvoiceBatch($saved_invoicehead->BatchNumber, $invoice_date, $created_by);
		}
		
		//Put all unbilled timesheets to invoicedetail
		$timesheet = new Timesheet;
		$timesheet_filters = array(
			'unbilled_only' => '1',			
		);
		$timesheet_result = $timesheet->prepareQueryForGetTimesheet(Config::get('oln.admin_id'), $timesheet_filters);
		foreach($timesheet_result as $tr) {
			//Prepare data array to be stored in invoicedetail
		}
	}

	public function isExistInvoiceHead($unique_id)
	{
		$count = DB::table('invoicehead')->where('UniqueID', $unique_id)->count();

		return $count > 0 ? true : false;
	}

	public function deleteInvoiceDetail($timesheet_id)
	{
		$delete_result = DB::table('invoicedetail')->where('ReferenceID', $timesheet_id)->limit(1)->delete();

		return $delete_result;
	}

}
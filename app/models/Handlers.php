<?php
class Handlers{
	/* Query of all Employees/Handers with leave status */
	public function getE(){
		return DB::select('SELECT employee.EmployeeID,employee.photo, employee.NickName, (
                                    SELECT leaveapply.ApproveStatus FROM leaveapply WHERE employee.EmployeeID = leaveapply.EmployeeID AND CURDATE()BETWEEN leaveapply.DateRangeFrom AND leaveapply.DateRangeTo
                                            ORDER BY leaveapply.DateRangeTo DESC LIMIT 1
                            ) AS on_leave, (SELECT employeejob.DepartmentCode FROM employeejob
                                    WHERE employeejob.EmployeeID = employee.EmployeeID AND employeejob.DepartmentCode <> "ADM" AND employeejob.DepartmentCode <> "SEC" AND employeejob.DepartmentCode <> "MAN" AND employeejob.DepartmentCode IS NOT NULL
                                    LIMIT 1
                            )AS DepartmentCode 
                            FROM employee
                            ORDER BY employee.EmployeeID ASC');
	}//end-getE
	
	public function getEM($e){
            return DB::select('SELECT customer.Incharge, 
                            jobassignment.DesignatedDate, jobassignment.JobAssignmentID, jobassignment.CustomerID, jobassignment.Description_A, jobassignment.Completed,(
                        SELECT WorkDate FROM timesheet
                                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                                    ORDER BY WorkDate DESC LIMIT 1) AS last_trans_date,TIMESTAMPDIFF(MONTH,(
                    SELECT WorkDate FROM timesheet
                        WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                        ORDER BY WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl
            FROM jobassignment
            LEFT JOIN customer ON customer.CustomerID = jobassignment.CustomerID
            WHERE jobassignment.Incharge = ?',array($e));
	}
        
        public function getEmployeematters($e,$id){
            if($id==0)
                return $this->getEM($e);
            else
            if($id == 4)
                return DB::select('SELECT customer.Incharge, 
                        jobassignment.DesignatedDate, jobassignment.JobAssignmentID, jobassignment.CustomerID, jobassignment.Description_A, jobassignment.Completed,(
                    SELECT WorkDate FROM timesheet
                                WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                                ORDER BY WorkDate DESC LIMIT 1) AS last_trans_date
                    FROM jobassignment
                    LEFT JOIN customer ON customer.CustomerID = jobassignment.CustomerID
                    WHERE jobassignment.Completed = 1 AND jobassignment.Incharge =  "'.$e.'"
                    GROUP BY jobassignment.JobAssignmentID');
            else{
                $find = $id <= 1 ? "<= $id" : ($id == 2 ? "= $id " : ($id == 3 ? "> 2" : "IS NULL"));
                return DB::select('SELECT customer.Incharge, 
                    jobassignment.DesignatedDate, jobassignment.JobAssignmentID, jobassignment.CustomerID, jobassignment.Description_A, jobassignment.Completed,(
                SELECT WorkDate FROM timesheet
                            WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                            ORDER BY WorkDate DESC LIMIT 1) AS last_trans_date,(
                        SELECT WorkDate FROM timesheet
                                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                                    ORDER BY WorkDate DESC LIMIT 1) AS last_trans_date,TIMESTAMPDIFF(MONTH,(
                    SELECT WorkDate FROM timesheet
                        WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                        ORDER BY WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl
                FROM jobassignment
                LEFT JOIN customer ON customer.CustomerID = jobassignment.CustomerID
                WHERE TIMESTAMPDIFF(MONTH,(
                    SELECT timesheet.WorkDate FROM timesheet
                        WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                        ORDER BY timesheet.WorkDate DESC LIMIT 1),CURDATE()) '.$find.'
                AND jobassignment.Completed = 0 AND jobassignment.Incharge =  "'.$e.'"
                GROUP BY jobassignment.JobAssignmentID');
            }
	}

	/**
	 * Get query for list of handlers with reports overview
	 *
	 * @param 	int 	user id
	 * @param 	array 	filters
	 * @return 	object 	query object
	 */
	public function prepareQueryForGetList($user_id = null, $filter_array = array())
	{
		$query = DB::table('employee as emp')
					->select(
						DB::raw('
							emp.EmployeeID,
							(select count(ja.JobAssignmentID) from jobassignment as ja where ja.Incharge = emp.EmployeeID limit 1) as matter_count,
							COALESCE(sum(rh.total_billed_amount),0) as total_billed_amount,
							COALESCE(sum(rh.total_billable_amount),0) as total_billable_amount,
							COALESCE(sum(rh.total_unbilled_amount),0) as total_unbilled_amount
						')
					)
					->leftJoin('report_handlers as rh', 'rh.handler_id', '=', 'emp.EmployeeID')
					->groupBy('emp.EmployeeID');

		/* Filters */
		if(isset($filter_array['total_unbilled_amount_range'])) {
			if(isset($filter_array['total_unbilled_amount_range']['to']) && !isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '<=', $filter_array['total_unbilled_amount_range']['to']);
			if(!isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '>=', $filter_array['total_unbilled_amount_range']['from']);
			if(isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->havingBetween('total_unbilled_amount', array($filter_array['total_unbilled_amount_range']['from'], $filter_array['total_unbilled_amount_range']['to']));
		}

		if(isset($filter_array['remember_query'])) {
			$query->remember($filter_array['remember_query']);
		}
		/* /Filters */

		return $query;
	}

	public function prepareQueryForGetList_OLD_001($user_id = null, $filter_array = array())
	{
		$query = DB::table('employee as emp')
					->select(
						DB::raw('
							emp.EmployeeID,
							count(distinct ja.JobAssignmentID) as matter_count,
							round(COALESCE(sum(ind.Amount),0),2) AS total_billed_amount,
							round(COALESCE(sum(jab.PlanAmount),0),2) AS total_billable_amount,
							round(COALESCE(sum(jab.PlanAmount),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount							
						')
					)
					->leftJoin('jobassignment as ja', 'ja.Incharge', '=', 'emp.EmployeeID')
					->leftJoin('timesheet as ts', 'ts.JobAssignmentID', '=', 'ja.JobAssignmentID')
					->leftJoin('timesheetwork as tsw','tsw.UniqueID', '=', 'ts.UniqueID')
					->leftJoin('jobassignmentbilling as jab', 'jab.UniqueID', '=', 'tsw.UniqueID')
					->leftJoin('invoicedetail as ind', 'ind.ReferenceID', '=', 'ts.UniqueID')
					->groupBy('emp.EmployeeID');

		/* Filters */
		if(isset($filter_array['total_unbilled_amount_range'])) {
			if(isset($filter_array['total_unbilled_amount_range']['to']) && !isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '<=', $filter_array['total_unbilled_amount_range']['to']);
			if(!isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '>=', $filter_array['total_unbilled_amount_range']['from']);
			if(isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->havingBetween('total_unbilled_amount', array($filter_array['total_unbilled_amount_range']['from'], $filter_array['total_unbilled_amount_range']['to']));
		}
		/* /Filters */

		return $query;
	}

	public function getList($filter_array = array())
	{
		$user_id = null;
		$query = $this->prepareQueryForGetList($user_id, $filter_array);

		return $query->get();
	}

	public function getInfo($handler_id)
	{
		$result = DB::table('employee as emp')
						->leftJoin('user as usr', 'usr.EmployeeID', '=', 'emp.EmployeeID')
						->select('emp.*', 'usr.email_address', 'usr.user_id')
						->where('emp.EmployeeID', $handler_id)->first();

		return $result;
	}
}//end-class

<?php

class Utilities{
    
    public function getFlag(){
        return DB::select('SELECT id FROM system_vars WHERE id = 1 and value != CURDATE()');
    }
    
    public function setFlag(){
        DB::select('UPDATE system_vars SET value = CURDATE() WHERE id = 1 LIMIT 1');
    }
    
    public function resetFlag(){
        DB::select('UPDATE system_vars SET value = 1 WHERE id = 1 LIMIT 1');
    }
    
    public function getVar($name){
        return DB::select('SELECT * FROM system_vars WHERE field = :name LIMIT 1',array('name' => $name));
    }
    
    public function saveDefaults($obj){       
        DB::update("UPDATE a_trans_sys SET date = :date, time = :time, data = :data WHERE name = :name LIMIT 1",array(
            'date' => $obj['date'],
            'time' => $obj['time'],
            'data' => $obj['data'],
            'name' => $obj['name']
        ));
    }
    
    public function retrieveDefaults($name_){
        return DB::select("SELECT * FROM a_trans_sys WHERE name = :name LIMIT 1",array('name' => $name_));
    }
    
    public function getCloseInfo($matter){
        return DB::select("SELECT * FROM jobassignmentclosed WHERE JobAssignmentID = :jobid ORDER BY id DESC",array('jobid' => $matter));
    }
    
    public function setCloseInfo($obj){
        DB::insert("INSERT INTO jobassignmentclosed (JobAssignmentID, box, closedby, role, date, time, status) VALUES (:jobid, :boxno, :closedby, :role, :date, :time, 1)",array(
            'jobid' => $obj['jobid'], 'boxno' => $obj['boxno'], 'closedby' => $obj['closedby'], 'role' => $obj['role'], 'date' => $obj['date'], 'time' => $obj['time']
        ));
    }
    
    public function getTimeSheet($matter){
        return DB::select("SELECT
	invoicehead.InvoiceDate , invoicehead.BatchNumber, invoicehead.UniqueID AS invoicehead_UniqueID, 
        SUM(jobassignmentbilling.PlanAmount) as BillableAmount, SUM(invoicedetail.Amount) AS BilledAmount, SUM((jobassignmentbilling.PlanAmount - IFNULL(invoicedetail.Amount,0))) AS UnbilledAmount
        FROM
            jobassignment
        LEFT JOIN  timesheet 
            ON timesheet.JobAssignmentID = jobassignment.JobAssignmentID
        LEFT JOIN timesheetwork
            ON timesheet.UniqueID = timesheetwork.UniqueID
        LEFT JOIN jobassignmentbilling
            ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
        LEFT JOIN invoicedetail
            ON invoicedetail.ReferenceID = timesheetwork.UniqueID
		LEFT JOIN invoicehead
			ON invoicehead.UniqueID = invoicedetail.UniqueID
        WHERE jobassignment.JobAssignmentID  = :matterid
	GROUP BY invoicedetail.UniqueID
        ORDER BY invoicehead.InvoiceDate DESC",array('matterid' => $matter));
    }
    
    public function removetimesheet($batch,$invoice){
        DB::delete("DELETE FROM invoicehead WHERE UniqueID = :invoiceid",array('invoiceid' => $invoice));
        DB::delete("DELETE FROM invoicedetail WHERE UniqueID = :invoiceid",array('invoiceid' => $invoice));
        DB::delete("DELETE FROM invoicebatch WHERE BatchNumber = :batchnum",array('batchnum' => $batch));
    }
    
    public function setInvoiceAsFinal($batchnum){
        DB::update("UPDATE invoicehead SET InvoiceType = 'FI' WHERE BatchNumber = :batchnum LIMIT 1",array('batchnum' => $batchnum));
    }
    
    public function delegateMatter($obj){
        DB::insert("INSERT INTO jobassignmentdelegated (JobAssignmentID, delegatedTo, delegatedBy, date, time, status) VALUES (:matter_id, :delegated_to, :delegated_by, :date, :time, 1)",array(
            'matter_id' => $obj['matter_id'],
            'delegated_to' => $obj['delegated_to'],
            'delegated_by' => $obj['delegated_by'],
            'date' => $obj['date'],
            'time' => $obj['time']
        ));
    }
    
    public function getMattersDelegated($handler,$source_type){
        $column = $source_type == 'by' ? 'delegatedBy' : 'delegatedTo';
        
        return DB::select("SELECT * FROM jobassignmentdelegated WHERE ".$column." = :handler AND status = 1  ORDER BY id DESC",array(
            'handler' => $handler
        ));
    }
    
    public function getDelegatedMatters($id){
        return DB::select("SELECT * FROM jobassignmentdelegated WHERE JobAssignmentID = :matter_id AND status = 1 ORDER BY id ASC", array(
            'matter_id' => $id
        ));
    }

    public function declineMatter($obj){
        DB::update('UPDATE jobassignmentdelegated SET status = 2 WHERE id = :id',array('id' => $obj['id']));
    }

    public function completeMatter($obj){
        DB::update('UPDATE jobassignmentdelegated SET status = 3 WHERE id = :id',array('id' => $obj['id']));
    }

    public function checkDelegationStatus($obj){
        return DB::select('SELECT id, status FROM jobassignmentdelegated WHERE JobAssignmentID = :id AND delegatedTo = :delegatedTo ORDER BY id DESC LIMIT 1',
        array(
            'id'            => $obj['id'],
            'delegatedTo'   => $obj['delegatedTo']
        ));
    }
    
    public function getMattersHandlersDelegated($handler,$source_type,$matter_id){
        $column = $source_type == 'by' ? 'delegatedBy' : 'delegatedTo';
        
        return DB::select("SELECT * FROM jobassignmentdelegated WHERE ".$column." = :handler  AND JobAssignmentID = :matter_id AND status = 1 ORDER BY id DESC",array(
            'handler' => $handler,
            'matter_id' => $matter_id
        ));
    }
    
        public function getMattersIP(){
        return DB::select("
            SELECT 
            jobassignment.JobAssignmentID, jobassignment.CustomerID, customer.CompanyName_A1 AS client_name, jobassignment.DesignatedDate, jobassignment.Description_A, jobassignment.Remark, jobassignment.Completed, jobassignment.CompletedBy, jobassignment.Incharge, user.email_address AS handler_email, @last_trans_date:=(
                    SELECT WorkDate FROM timesheet
                    WHERE JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1) AS last_trans_date, @last_cost_account_trans:=(
                        SELECT jobassignmentcost.date_c 
                            FROM 
                                jobassignmentcost
                            WHERE
                                jobassignmentcost.JobAssignmentID = jobassignment.JobAssignmentID
                            ORDER BY
                                jobassignmentcost.date_c DESC
                        LIMIT 1
                    ) AS last_cost_account_trans, 
            @mn_invl:=(TIMESTAMPDIFF(WEEK,(@last_trans_date),CURDATE())) AS mn_invl,@mn_temp:=(TIMESTAMPDIFF(WEEK,(@last_cost_account_trans),CURDATE())) AS mn_temp,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0))AS billable_amount, SUM(IFNULL(invoicedetail.Amount,0)) AS billed_amount,
            (CASE WHEN jobassignment.Completed = 1 THEN 'blue' 
            ELSE(
                CASE WHEN @mn_invl IS NULL THEN (
                    CASE WHEN @mn_temp IS NULL
                        THEN 'gray'
                    ELSE(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )ELSE(
                    CASE WHEN @mn_temp < @mn_invl
                    THEN(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )ELSE(
                        CASE 
                            WHEN @mn_invl <= 4 THEN 'green'
                            WHEN @mn_invl > 4 AND @mn_invl <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )
                END
            )
            END) AS matter_type,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0)) - SUM(IFNULL(invoicedetail.Amount,0)) AS unbilled, 
                IFNULL((SELECT role 
                    FROM jobassignmentclosed 
                    WHERE jobassignmentclosed.JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY jobassignmentclosed.id DESC LIMIT 1),0
                ) AS CompleteStatus
            FROM jobassignment
            LEFT JOIN customer
                ON jobassignment.CustomerID = customer.CustomerID
            LEFT JOIN user
                ON user.EmployeeID = jobassignment.Incharge
            LEFT JOIN  timesheet 
                ON timesheet.JobAssignmentID = jobassignment.JobAssignmentID
            LEFT JOIN timesheetwork
                ON timesheet.UniqueID = timesheetwork.UniqueID
            LEFT JOIN jobassignmentbilling
                ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
            LEFT JOIN invoicedetail
                ON invoicedetail.ReferenceID = timesheetwork.UniqueID
            WHERE jobassignment.Completed = 9
            GROUP BY jobassignment.JobAssignmentID
            ORDER BY jobassignment.JobAssignmentID ASC");
    }
    
    public function SearchClient($clientkey){
        return DB::select("SELECT CustomerID, CompanyName_A1 FROM customer WHERE CompanyName_A1 LIKE '%".$clientkey."%'");
    }
    
    public function getClientLastID(){
        return DB::select("SELECT CustomerID FROM customer ORDER BY Date_C DESC LIMIT 1");
    }
    
    public function getMatterLastID(){
        return DB::select("SELECT JobAssignmentID FROM jobassignment ORDER BY JobAssignmentID DESC LIMIT 1");
    }
    
    public function insertMatter($obj){
        DB::insert("
            INSERT INTO 
                jobassignment 
            (CompanyID, JobAssignmentID, Description_A, Remark, CustomerID, DesignatedDate, Incharge, Completed, User_C, Date_C) 
            VALUES ('OLN', :id, :des, :longdesc, :client, :desig, :handler, :completed, :user, :date)",
        array(
            'id'        => $obj['id'],
            'des'       => $obj['desc'],
            'longdesc'  => $obj['longdesc'], 
            'client'    => $obj['client'],
            'desig'     => $obj['designated'],
            'handler'   => $obj['handler'],
            'completed' => $obj['completed'],
            'user'      => $obj['user'],
            'date'      => $obj['date']
        ));
    }
    
    public function updateMatter($obj){
        DB::update("
            UPDATE 
                jobassignment
            SET 
                Description_A = :des, Remark = :longdesc, CustomerID = :client, DesignatedDate = :desig,
                Incharge = :handler, Completed = :completed, User_U = :user, Date_U = :date
            WHERE 
                JobAssignmentID = :id LIMIT 1",
        array(
            'id'        => $obj['id'],
            'des'       => $obj['desc'],
            'longdesc'  => $obj['longdesc'], 
            'client'    => $obj['client'],
            'desig'     => $obj['designated'],
            'handler'   => $obj['handler'],
            'completed' => $obj['completed'],
            'user'      => $obj['user'],
            'date'      => $obj['date']
        ));
    }
    
    public function getMatterDetails($id){
        return DB::select("
            SELECT 
                fixedpay, fixedamount, introducer, beneficial_owner, special_instructions, client, address, email, tel, fax, attn, re
            FROM 
                jobassignmentdetails 
            WHERE
                JobAssignmentID = :matter LIMIT 1",
            array(
                'matter' => $id
            ));
    }
    
    public function insertMatterCost($obj){
        DB::insert("INSERT INTO 
                        jobassignmentcost
                    (JobAssignmentID, type, Amount, user_c, date_c, time_c, stat)
                    VALUES(:id, :type, :amount, :user, :date, :time, 1)",
        array(
            'id'        => $obj['id'],
            'type'      => $obj['type'],
            'amount'    => floatval($obj['amount']),
            'user'      => $obj['user'],
            'date'      => $obj['date'],
            'time'      => $obj['time']
        ));
    }
    
    public function updateMatterCost($obj){
        DB::update('
            UPDATE
                jobassignmentcost
            SET
               type = :type, Amount = :amount
            WHERE
                JobAssignmentID = :id LIMIT 1',
        array(
            'id'        => $obj['id'],
            'type'      => $obj['type'],
            'amount'    => floatval($obj['amount'])
        ));
    }
    
    public function getMatterCost($id){
        return DB::select('SELECT * FROM jobassignmentcost WHERE JobAssignmentID = :id ORDER BY id ASC',array('id' => $id));
    }
    
    public function insertMatterDetails($obj){
        DB::insert("
            INSERT INTO 
                jobassignmentdetails
            (JobAssignmentID, fixedpay, fixedamount, depositamount, introducer, beneficial_owner, special_instructions, client, address, email, tel, fax, attn, re, stat)
            VALUES (:JobAssignmentID, :fixedpay, :fixedamount, :credit, :introducer, :beneficial_owner, :special_instructions, :client, :address, :email, :tel, :fax, :attn, :re, :stat)",
            array(
                'JobAssignmentID'       => $obj['JobAssignmentID'],
                'fixedpay'              => $obj['fixedpay'],
                'fixedamount'           => floatval($obj['fixedamount']),
                'credit'                => floatval($obj['credit']),
                'introducer'            => $obj['introducer'],
                'beneficial_owner'      => $obj['beneficial_owner'],
                'special_instructions'  => $obj['special_instructions'],
                'client'                => $obj['client'],
                'address'               => $obj['address'],
                'email'                 => $obj['email'],
                'tel'                   => $obj['tel'],
                'fax'                   => $obj['fax'],
                'attn'                  => $obj['attn'],
                're'                    => $obj['re'],
                'stat'                  => 1
            ));
    }
    
    public function updateMattterDetails($obj){
        DB::update("
            UPDATE
                jobassignmentdetails
            SET
                fixedpay = :fixedpay, fixedamount = :fixedamount, depositamount = :credit, introducer = :introducer, 
                beneficial_owner = :beneficial_owner, special_instructions = :special_instructions, client = :client,
                address = :address, email = :email, tel = :tel, fax = :fax, attn = :attn, re = :re
            WHERE
                JobAssignmentID = :JobAssignmentID
            LIMIT 1",
            array(
                'JobAssignmentID'       => $obj['JobAssignmentID'],
                'fixedpay'              => $obj['fixedpay'],
                'fixedamount'           => floatval($obj['fixedamount']),
                'credit'                => floatval($obj['credit']),
                'introducer'            => $obj['introducer'],
                'beneficial_owner'      => $obj['beneficial_owner'],
                'special_instructions'  => $obj['special_instructions'],
                'client'                => $obj['client'],
                'address'               => $obj['address'],
                'email'                 => $obj['email'],
                'tel'                   => $obj['tel'],
                'fax'                   => $obj['fax'],
                'attn'                  => $obj['attn'],
                're'                    => $obj['re']
            ));
    }
    
    public function insertClient($obj){
        DB::insert("
            INSERT INTO 
                customer 
            (CustomerID, IsActive, StatusCode, HKID, CUSTTYPE, DATEOFBIRTH, PLACEOFBIRTH, NATIONALITY, HOMEADDRESS, CORADDRESS, OCCUPATION, EMPLOYER, TELNUM, OFFICETEL, FAXNUM, MOBILE, EMAIL, ISREP, REPNAME, REPNUM, PLACEOFINC, BUSADD, CompanyName_A1, Incharge, User_C, Date_C)
            VALUES(:id, 1, 1, 1, 1, :bday, '', '', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', :name, :handler, :user, :date)",
        array(
            'id'        =>  $obj['id'],
            'name'      =>  $obj['name'],
            'handler'   =>  $obj['handler'],
            'bday'      =>  $obj['date'],
            'user'      =>  $obj['user'],
            'date'      =>  $obj['date']
        ));
    }
    
    public function getTimeSheets($user){
        return DB::select("
            SELECT 
                timesheet.UniqueID, timesheet.JobAssignmentID, timesheet.WorkDate,
                timesheetwork.ChargeHour, timesheetwork.Comment, timesheetwork.EmployeeID
            FROM 
                timesheet
            LEFT JOIN
                timesheetwork
            ON 
                timesheet.UniqueID = timesheetwork.UniqueID
            WHERE
                (timesheet.User_C = :user OR timesheetwork.EmployeeID = :emp)
            AND
                DATE(timesheet.Date_C) = :date
            ORDER BY
                timesheet.UniqueID
            ASC",
        array(
            'user'  => $user,
            'emp'  => $user,
            'date'  => date('Y-m-d')
        ));
    }

    public function updateTimesheet($obj){
        DB::update('UPDATE timesheet SET JobAssignmentID = :matterid, WorkDate = :WorkDate, Date_U = :Date_U, User_U = :User_U WHERE UniqueID = :UniqueID LIMIT 1',
        array(
                'matterid'      => $obj['matter_no'],
                'WorkDate'      => $obj['WorkDate'],
                'Date_U'        => date('Y-m-d H:i:s'),
                'User_U'        => User::getEmployeeID(Auth::user()->user_id),
                'UniqueID'      => $obj['UniqueID']
        ));
        DB::update('UPDATE timesheetwork SET EmployeeID = :EmployeeID, WorkHour = :WorkHour, ChargeHour = :ChargeHour, HourlyRate = :HourlyRate, Comment = :Comment WHERE UniqueID = :UniqueID LIMIT 1',
            array(
                'EmployeeID'    => $obj['EmployeeID'],
                'WorkHour'      => $obj['WorkHour'],
                'ChargeHour'    => $obj['WorkHour'],
                'HourlyRate'    => $obj['HourlyRate'],
                'Comment'       => $obj['Comment'],
                'UniqueID'      => $obj['UniqueID']
            ));
    }

    public function getTSAdmin(){
        return DB::select("
            SELECT 
                timesheet.UniqueID, timesheet.JobAssignmentID, timesheet.WorkDate,
                timesheetwork.ChargeHour, timesheetwork.Comment, timesheetwork.EmployeeID
            FROM 
                timesheet
            LEFT JOIN
                timesheetwork
            ON 
                timesheet.UniqueID = timesheetwork.UniqueID
            WHERE
                DATE(timesheet.Date_C) = DATE(NOW())
            ORDER BY
                timesheet.UniqueID
            ASC");
    }

    public function getPic(){
        return DB::select("SELECT Photo, EmployeeID FROM employee");
    }
}//end-class
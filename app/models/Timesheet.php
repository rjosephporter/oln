<?php

class Timesheet {
	/*
	public function create($item_count, $data)
	{
		$result = false;
		$employee_id = Auth::user()->EmployeeID;

		$data_ts = $data_tsw = array();
		if($item_count == count($data['new_workhour'])) {
			for ($i = 0; $i < $item_count ; $i++) { 
				$unique_id = HelperLibrary::generateUniqueId($i);
				// Timesheet Data //
				$data_ts[] = array(
					'CompanyID' 		=> 'OLN',
					'UniqueID'			=> $unique_id,
					'TimesheetType'		=> '1',
					'JobAssignmentID'	=> $data['new_matter_id'],
					'WorkDate'			=> date('Y-m-d', strtotime($data['new_timesheet_date'])),
					'IsPersonalJob'		=> '1',
					'RepeatCode'		=> $unique_id,
					'Status'			=> 'Approved',
					'User_C'			=> $employee_id,
					'Date_C'			=> date('Y-m-d H:i:s'),
					'Program_C'			=> 'TimeSheetByDateInfo_3'
				);

				// TimesheetWork Data //
				$data_tsw[] = array(
					'CompanyID' 		=> 'OLN',
					'UniqueID'			=> $unique_id,
					'TimeCode'			=> $data['new_timecode'][$i],
					'EmployeeID'		=> $employee_id,
					'WorkHour'			=> $data['new_workhour'][$i],
					'ChargeHour'		=> $data['new_workhour'][$i],
					'Completed'			=> '0',
					'HourlyRate'		=> $data['new_hourlyrate'],
					'ChargeRate'		=> $data['new_hourlyrate'],
					'Comment'			=> $data['new_description'][$i],
					'BillingStatus'		=> 'Billable',
					'Unbillable'		=> '0',
					'Invisible'			=> '0'
				);
			}

			// Save to table //
			$ts  = DB::table('timesheet')->insert($data_ts);
			$tsw = DB::table('timesheetwork')->insert($data_tsw);

			if($ts && $tsw) {
				$result = true;
			}
		}

		return $result;
	}
	*/

	public function create2($item_count, $data)
	{
		$result = false;
		$employee_id = Auth::user()->EmployeeID;

		$data_ts = $data_tsw = $data_jab = array();
		if($item_count == count($data['new_workhour'])) {
			for ($i = 0; $i < $item_count ; $i++) { 
				$unique_id = HelperLibrary::generateUniqueId($i);
				/* Timesheet Data */
				$data_ts[] = array(
					'CompanyID' 		=> 'OLN',
					'UniqueID'			=> $unique_id,
					'TimesheetType'		=> '1',
					'JobAssignmentID'	=> $data['new_matter_id'][$i],
					'WorkDate'			=> date('Y-m-d', strtotime($data['new_timesheet_date'][$i])),
					'IsPersonalJob'		=> '1',
					'RepeatCode'		=> $unique_id,
					'Status'			=> 'Approved',
					'User_C'			=> $employee_id,
					'Date_C'			=> date('Y-m-d H:i:s'),
					'Program_C'			=> 'TimeSheetByDateInfo_3'
				);

				/* TimesheetWork Data */
				$data_tsw[] = array(
					'CompanyID' 		=> 'OLN',
					'UniqueID'			=> $unique_id,
					'TimeCode'			=> $data['new_timecode'][$i],
					'EmployeeID'		=> $data['new_handler'][$i],
					'WorkHour'			=> $data['new_workhour'][$i],
					'ChargeHour'		=> $data['new_workhour'][$i],
					'Completed'			=> '0',
					'HourlyRate'		=> $data['new_hourlyrate'][$i],
					'ChargeRate'		=> $data['new_hourlyrate'][$i],
					'Comment'			=> $data['new_description'][$i],
					'BillingStatus'		=> ($data['new_billable'][$i] == '1') ? 'Billable' : 'Unbillable',
					'Unbillable'		=> ($data['new_billable'][$i] == '1') ? '0' : '1',
					'Invisible'			=> '0'
				);

				if($data['new_billable'][$i] == '1') {
					$data_jab[] = array(
						'CompanyID'			=> 'OLN',
						'JobAssignmentID'	=> $data['new_matter_id'][$i],
						'UniqueID'			=> $unique_id,
						'SeqID'				=> $i + 1,
						'EmployeeID'		=> null,
						'PlanBillingDate'	=> date('Y-m-d'),
						'PlanAmount'		=> $data['new_workhour'][$i] * $data['new_hourlyrate'][$i],
						'Description'		=> $data['new_description'][$i],
						'SourceType'		=> 'JT',
						'SourceUniqueID'	=> $unique_id,
						'SourceTimeCode'	=> $data['new_timecode'][$i],
						'SourceEmployeeID'	=> $data['new_handler'][$i],
						'User_C'			=> $employee_id,
						'Date_C'			=> date('Y-m-d H:i:s')
					);
				}
			}

			/* Save to table */
			$ts  = DB::table('timesheet')->insert($data_ts);
			$tsw = DB::table('timesheetwork')->insert($data_tsw);
			$jab = (count($data_jab) > 0) ? JobAssignmentBilling::insert($data_jab) : 1;

			if($ts && $tsw && $jab) {
				$result = true;
			}
		}

		return $result;
	}

	// As of 11.06.2014
	public function create3($matter_id = null, $data, $other_data = array())
	{
		$result = false;
		$employee_id = Auth::user()->EmployeeID;

		$data_ts = $data_tsw = $data_jab = array();
		$timesheet_ids = array();
		foreach($data as $ndx => $item) {
			$unique_id = HelperLibrary::generateUniqueId($ndx);
			/* Timesheet Data */
			$data_ts[] = array(
				'CompanyID' 		=> 'OLN',
				'UniqueID'			=> $unique_id,
				'TimesheetType'		=> '1',
				'JobAssignmentID'	=> !empty($matter_id) ? $matter_id : $item['JobAssignmentID'],
				'WorkDate'			=> date('Y-m-d', strtotime($item['WorkDate'])),
				'IsPersonalJob'		=> '1',
				'RepeatCode'		=> $unique_id,
				'Status'			=> 'Approved',
				'User_C'			=> $employee_id,
				'Date_C'			=> date('Y-m-d H:i:s'),
				'Program_C'			=> 'OEMS'
			);

			/* TimesheetWork Data */
			$data_tsw[] = array(
				'CompanyID' 		=> 'OLN',
				'UniqueID'			=> $unique_id,
				'TimeCode'			=> $item['TimeCode'],
				'EmployeeID'		=> $item['EmployeeID'],
				'WorkHour'			=> $item['WorkHour'],
				'ChargeHour'		=> $item['WorkHour'],
				'Completed'			=> '0',
				'HourlyRate'		=> $item['HourlyRate'],
				'ChargeRate'		=> $item['HourlyRate'],
				'Comment'			=> $item['Comment'],
				'BillingStatus'		=> 'Billable',
				'Unbillable'		=> '0',
				'Invisible'			=> '0'
			);

			$data_jab[] = array(
				'CompanyID'			=> 'OLN',
				'JobAssignmentID'	=> !empty($matter_id) ? $matter_id : $item['JobAssignmentID'],
				'UniqueID'			=> $unique_id,
				'SeqID'				=> $ndx + 1,
				'EmployeeID'		=> null,
				'PlanBillingDate'	=> date('Y-m-d'),
				'PlanAmount'		=> $item['WorkHour'] * $item['HourlyRate'],
				'Description'		=> $item['Comment'],
				'SourceType'		=> 'JT',
				'SourceUniqueID'	=> $unique_id,
				'SourceTimeCode'	=> '',
				'SourceEmployeeID'	=> $item['EmployeeID'],
				'User_C'			=> $employee_id,
				'Date_C'			=> date('Y-m-d H:i:s')
			);

			$timesheet_ids[] = $unique_id;			
		}

		/* Save to table */
		if(count($data) > 0) {
			$ts  = DB::table('timesheet')->insert($data_ts);
			$tsw = DB::table('timesheetwork')->insert($data_tsw);
			$jab = (count($data_jab) > 0) ? JobAssignmentBilling::insert($data_jab) : 1;

			if($ts && $tsw && $jab) {
				if(isset($other_data['return_timesheet_ids']))
					$result = $timesheet_ids;
				else
					$result = true;
			}
		}

		return $result;
	}			

	public function update($timesheet_id, $data)
	{
		$ts_result = DB::table('timesheet')->where('UniqueID', $timesheet_id)->take(1)->update($data['timesheet']);
		$tsw_result = DB::table('timesheetwork')->where('UniqueID', $timesheet_id)->take(1)->update($data['timesheetwork']);

		$jab_result = 0;
		if(count($data['jobassignmentbilling']) > 0)
			$jab_result = DB::table('jobassignmentbilling')->where('UniqueID', $timesheet_id)->take(1)->update($data['jobassignmentbilling']);

		return ($ts_result >= 0 && $tsw_result >= 0 && $jab_result >= 0) ? true : false;
	}

	public function delete($timesheet_id)
	{
		$ts_result  = DB::table('timesheet')->where('UniqueID', $timesheet_id)->take(1)->delete();
		$tsw_result = DB::table('timesheetwork')->where('UniqueID', $timesheet_id)->take(1)->delete();
		$jab_result = JobAssignmentBilling::find($timesheet_id)->delete();

		return ($ts_result >= 0 && $tsw_result >= 0 && $jab_result >= 0) ? true : false;	
	}

	public function getTimeCode()
	{
		return DB::table('timecode')->get();
	}

	public function getDetail($timesheet_id)
	{
		$results = array();
		$final_result = array();
		$timesheet_id = (is_array($timesheet_id)) ? $timesheet_id : explode(',', $timesheet_id);
		$timesheet_chunk = array_chunk($timesheet_id, 100);

		foreach($timesheet_chunk as $ids) {
			$query = DB::table('timesheet as ts')
						->leftJoin('timesheetwork as tsw', 'tsw.UniqueID', '=', 'ts.UniqueID')
						->select('tsw.*')
						->whereIn('ts.UniqueID', $ids);

			$results[] = $query->remember(5)->get();
		}

		foreach($results as $result)
			$final_result = array_merge($final_result,$result);

		return $final_result;
	}

	public function belongsToMatter($timesheet_id = array(), $matter_id)
	{
		if(count($timesheet_id) == 0) return false;
		
		$result = DB::table('timesheet')
						->where('JobAssignmentID', $matter_id)
						->whereIn('UniqueID', $timesheet_id)
						->count();
		return ($result == count($timesheet_id)) ? true : false;
	}

	public function weeklyReport($filter_array = array())
	{

		/* Filters */
		$employees_implode = '';
		if(isset($filter_array['employee_array'])) {
			$employees_implode = implode(',', $filter_array['employee_array']);
		}
		/* /Filters */

		$results = DB::select("
			select
				t1.EmployeeID,
				if(usr.first_name is not null, concat(usr.first_name, ' ', coalesce(usr.last_name, '')), emp.Nickname) as full_name,
				round(sum(t1.actual_non_billable_hours) + sum(t1.actual_billable_hours),2) as actual_recorded_hours,
				round(sum(t1.actual_non_billable_hours),2) as actual_non_billable_hours,
				round(sum(t1.actual_billable_hours),2) as actual_billable_hours
			from
			(
				select
					tsw.EmployeeID,
					sum(tsw.WorkHour) - sum(ChargeHour) as actual_non_billable_hours,
					sum(tsw.ChargeHour) as actual_billable_hours
				from timesheet as ts
					left join timesheetwork as tsw on tsw.UniqueID = ts.UniqueID 
				where 
					ts.WorkDate between ? and ? 
					and (ts.JobAssignmentID is null or ts.JobAssignmentID not between 1 and 99)
				group by tsw.EmployeeID

				union

				select
					tsw.EmployeeID,
					sum(tsw.WorkHour) as actual_non_billable_hours,
					0 as actual_billable_hours
				from
				 timesheet as ts 
					left join timesheetwork as tsw on tsw.UniqueID = ts.UniqueID 
				where 
					ts.WorkDate between ? and ? 
					and ts.JobAssignmentID between 1 and 99
				group by tsw.EmployeeID
			) as t1
				left join employee as emp on emp.EmployeeID = t1.EmployeeID
				left join user as usr on usr.EmployeeID = emp.EmployeeID
				left join assigned_roles as ar on ar.user_id = usr.user_id
			where ar.role_id = 2
			group by t1.EmployeeID
		",
			array(
				$filter_array['from_date'], $filter_array['to_date'],
				$filter_array['from_date'], $filter_array['to_date'],
			)
		);

		return $results;
	}

	public function weeklyReportOLD($filter_array = array())
	{
		$query = DB::table('timesheet as ts')
					->leftJoin('timesheetwork as tsw', 'tsw.UniqueID', '=', 'ts.UniqueID')
					->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'tsw.EmployeeID')
					->select(DB::raw('
						tsw.EmployeeID,
						emp.NickName,
						DATEDIFF(?,?) + 1 as date_diff,
						round((DATEDIFF(?,?) + 1) * ?, 2) as budget_billable_hours,
						round(sum(tsw.WorkHour), 2) as actual_recorded_hours,
						round(sum(tsw.WorkHour) - sum(ChargeHour), 2) as actual_non_billable_hours,
						round(sum(tsw.ChargeHour), 2) as actual_billable_hours,
						round(sum(ChargeHour) - ((DATEDIFF(?,?) + 1) * ?), 2) as variance_hours						
					'))
					->setBindings(
						array(
							$filter_array['to_date'], $filter_array['from_date'],
							$filter_array['to_date'], $filter_array['from_date'], Config::get('oln.timesheet.budget_billable_hours_per_day'),
							$filter_array['to_date'], $filter_array['from_date'], Config::get('oln.timesheet.budget_billable_hours_per_day')
						)
					)
					->whereBetween('ts.WorkDate', array($filter_array['from_date'], $filter_array['to_date']))
					->groupBy('tsw.EmployeeID');

		return $query;	
	}

}
<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AccountSettings extends Eloquent {

	use SoftDeletingTrait;

	protected $table = 'user';


	public function scopeActive($query)
	{
		return $query->whereStatus(1);
	}

	/* Accessors & Mutators */
    public function getStartTimeAttribute($value)
    {
        return date('h:i A', strtotime($value));
    }
    
    public function getEndTimeAttribute($value)
    {
        return date('h:i A', strtotime($value));
    }
    /* /Accessors & Mutators */

	public function updatePassword($user_id, $data)
	{
			$user_result = DB::table('user')->where('user_id', $user_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
	public function getPassword($user_id){
		return DB::select('SELECT password FROM user where user_id='.$user_id);

		//return $result;
	}

	public function updatePhoto($user_id, $data)
	{
			$user_result = DB::table('employee')->where('EmployeeID', $user_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
}
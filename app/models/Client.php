<?php

class Client extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer';

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'CustomerID';

	/**
	 * Set table timestamp
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();


	public function getMattersCount($client_id)
	{
		$query = DB::table('jobassignment')
					->where('CustomerID', '=', $client_id);

		return $query->count();
	}

	public function prepareQueryForGetList($user_id, $filter_array = array())
	{
		$query = DB::table('customer as ctm')
					->select(
						DB::raw('
							SQL_CALC_FOUND_ROWS
							ctm.CustomerID as customer_id,
							ctm.Incharge as handler_incharge,
							ctm.CompanyName_A1,
							(select count(ja.JobAssignmentID) from jobassignment as ja where ja.CustomerID = ctm.CustomerID limit 1) as matter_count,
							COALESCE(sum(rc.total_billed_amount),0) as total_billed_amount,
							COALESCE(sum(rc.total_billable_amount),0) as total_billable_amount,
							COALESCE(sum(rc.total_unbilled_amount),0) as total_unbilled_amount
						')
					)
					->leftJoin('report_clients AS rc', 'rc.client_id', '=', 'ctm.CustomerID')
					->leftJoin('user AS usr', 'usr.EmployeeID', '=', 'ctm.Incharge')
					->groupBy('ctm.CustomerID');

		/* Filters */
		//if(!in_array($user_id, Config::get('oln.admin')))					
		//if(!User::hasRoleById($user_id, 'Admin'))
		if(!User::isAdmin($user_id))	
			$query->where('usr.user_id', '=', $user_id);
		if(isset($filter_array['total_unbilled_amount_range'])) {
			if(isset($filter_array['total_unbilled_amount_range']['to']) && !isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '<=', $filter_array['total_unbilled_amount_range']['to']);
			if(!isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '>=', $filter_array['total_unbilled_amount_range']['from']);
			if(isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->havingBetween('total_unbilled_amount', array($filter_array['total_unbilled_amount_range']['from'], $filter_array['total_unbilled_amount_range']['to']));
		}

		if(isset($filter_array['remember_query'])) {
			$query->remember($filter_array['remember_query']);
		}

		if(isset($filter_array['workdate_year'])) {
			//$query->where('rc.workdate_year', '=', $filter_array['workdate_year']);

            $range_end = $filter_array['workdate_year'] + 1;
            $query->whereRaw('rc.trans_date BETWEEN "'.$filter_array['workdate_year'].'-07-01" AND "'.$range_end.'-06-30"');

		}		
		/* /Filters */

		return $query;
	}

	public function prepareQueryForGetList_OLD_001($user_id, $filter_array = array())
	{
		$query = DB::table('customer as ctm')
					->select(
						DB::raw('
							SQL_CALC_FOUND_ROWS
							ctm.CustomerID as customer_id,
							ctm.IsActive,
							ctm.StatusCode,
							ctm.Incharge as handler_incharge,
							ctm.CompanyName_A1,
							ctm.CompanyName_A2,
							ctm.CompanyName_B1,
							ctm.CompanyName_B2,
							ctm.CompanyName_C1,
							ctm.CompanyName_C2,
							ctm.Remark,
							ctm.User_C,
							ctm.Date_C,
							GROUP_CONCAT(ja.JobAssignmentID) as matter_list,
							count(DISTINCT ja.JobAssignmentID) as matter_count,
							round(COALESCE(sum(ind.Amount),0),2) AS total_billed_amount,
							-- round(COALESCE(sum(tsw.WorkHour * tsw.HourlyRate),0),2) AS total_billable_amount,
							-- round(COALESCE(sum(tsw.WorkHour * tsw.HourlyRate),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount
							round(COALESCE(sum(jab.PlanAmount),0),2) AS total_billable_amount,
							round(COALESCE(sum(jab.PlanAmount),0) - COALESCE(sum(ind.Amount),0),2) AS total_unbilled_amount
						')
					)
					->leftJoin('jobassignment AS ja', 'ja.CustomerID', '=', 'ctm.CustomerID')

					->leftJoin('timesheet AS ts', 'ts.JobAssignmentID', '=', 'ja.JobAssignmentID')
					->leftJoin('timesheetwork AS tsw', 'tsw.UniqueID', '=', 'ts.UniqueID')

					->leftJoin('jobassignmentbilling AS jab', 'jab.UniqueID', '=', 'tsw.UniqueID')

					->leftJoin('invoicedetail AS ind', 'ind.ReferenceID', '=', 'ts.UniqueID')

					->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'ctm.Incharge')
					->leftJoin('user as usr', 'usr.EmployeeID', '=', 'emp.EmployeeID')
					->groupBy('ctm.CustomerID');

		/* Filters */
		//if(!in_array($user_id, Config::get('oln.admin')))					
		//if(!User::hasRoleById($user_id, 'Admin'))
		if(!User::isAdmin($user_id))
			$query->where('usr.user_id', '=', $user_id);
		if(isset($filter_array['total_unbilled_amount_range'])) {
			if(isset($filter_array['total_unbilled_amount_range']['to']) && !isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '<=', $filter_array['total_unbilled_amount_range']['to']);
			if(!isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->having('total_unbilled_amount', '>=', $filter_array['total_unbilled_amount_range']['from']);
			if(isset($filter_array['total_unbilled_amount_range']['to']) && isset($filter_array['total_unbilled_amount_range']['from']))
				$query->havingBetween('total_unbilled_amount', array($filter_array['total_unbilled_amount_range']['from'], $filter_array['total_unbilled_amount_range']['to']));
		}		
		/* /Filters */

		return $query;
	}

	public function getList($user_id, $page = 1, $filter_array = array())
	{
		$item_per_page = 12;
		$skip = ($page == 1) ? 0 : ($page - 1) * $item_per_page;

		$filter_array['sortby'] = (isset($filter_array['sortby'])) ? $filter_array['sortby'] : 'ctm.CustomerID';
		$filter_array['order'] = (isset($filter_array['order'])) ? $filter_array['order'] : 'asc';		

		$query = $this->prepareQueryForGetList($user_id, $filter_array);
		$query->skip($skip)
				->take($item_per_page)
				->orderBy($filter_array['sortby'], $filter_array['order']);;

		$data =  $query
					//->remember(5)
					->get();
		$query_count = DB::select(DB::raw('select FOUND_ROWS() as client_count'));
		foreach ($query_count as $qc) $count = $qc->client_count;

		$result = array(
			'total_count' => $count,
			'data' => $data
		);

		return $result;
	}
    public function getClientsList(){
            return DB::select('
            SELECT customer.CustomerID, customer.Incharge, customer.CompanyName_A1, @LastActivityNweeks:=IFNULL(TIMESTAMPDIFF(WEEK,
            (
                SELECT report_clients.trans_date 
                FROM report_clients 
                WHERE report_clients.client_id = customer.CustomerID 
                ORDER BY report_clients.trans_date 
                DESC LIMIT 1
            ), CURDATE()), 9999 )AS LastActivityNweeks
            FROM customer 
            GROUP BY customer.CustomerID
            ORDER BY LastActivityNweeks ASC');
   
    }

      public function getClientsList2(){
            return DB::select('
            SELECT customer.CustomerID, customer.Incharge, customer.CompanyName_A1, @LastActivityNweeks:=IFNULL(TIMESTAMPDIFF(WEEK, 
            	( SELECT report_clients.trans_date FROM report_clients WHERE report_clients.client_id = customer.CustomerID 
            		ORDER BY report_clients.trans_date DESC LIMIT 1 ), CURDATE()), 9999 )AS LastActivityNweeks, 
            report_clients.total_billed_amount, report_clients.total_unbilled_amount, report_clients.total_billable_amount 
            FROM customer 
            left join report_clients 
            on report_clients.client_id= customer.CustomerID 
            GROUP BY customer.CustomerID 
            ORDER BY LastActivityNweeks ASC');
   
    }

 
    
    public function getMbyC($id){
        return DB::select('SELECT
			jobassignment.JobAssignmentID, jobassignment.Incharge, jobassignment.DesignatedDate, jobassignment.Description_A, jobassignment.Completed,TIMESTAMPDIFF(MONTH,(
		                    SELECT WorkDate FROM timesheet
		                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
		                        ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl, timesheet.WorkDate AS last_trans_date,
			SUM(jobassignmentbilling.PlanAmount)AS billable_amount, SUM(invoicedetail.Amount) AS billed_amount
		        FROM
		            jobassignment
		        LEFT JOIN  timesheet 
		            ON timesheet.JobAssignmentID = jobassignment.JobAssignmentID
		        LEFT JOIN timesheetwork
		            ON timesheet.UniqueID = timesheetwork.UniqueID
		        LEFT JOIN jobassignmentbilling
		            ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
		        LEFT JOIN invoicedetail
		            ON invoicedetail.ReferenceID = timesheetwork.UniqueID
		        WHERE jobassignment.CustomerID = '.$id.'
		        GROUP BY jobassignment.JobAssignmentID
		        ORDER BY
		        timesheet.UniqueID DESC, timesheet.WorkDate DESC');
    }

    public function getHandlerInfo($client_id)
    {
    	$query = DB::table('customer as cust')
    					->select('cust.CustomerID', 'cust.CompanyName_A1', 'cust.Incharge', 'emp.NickName', 'usr.email_address')
    					->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'cust.Incharge')
    					->leftJoin('user as usr', 'usr.EmployeeID', '=', 'emp.EmployeeID')
    					->where('cust.CustomerID', '=', $client_id);
    	return $query->first();
    }

    public function exists($client_id)
    {
    	$result = DB::table('customer')->where('CustomerID', $client_id)->count();
    	return ($result > 0) ? true : false;
    }

     public function createClient($data)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(CustomerID) as lastclient FROM customer');
        $lastclient=$get_result[0]->lastclient;
        $nextclient=$lastclient+1;

        $data_clientnew;
        //if($item_count == count($data['new_clientname'])) {
            //for ($i = 0; $i < $item_count ; $i++) { 
               $i=0;
               if($data['new_clienttype_id']==1){
                $data_clientnew[] = array(
                    'CustomerID'    =>$nextclient+$i,
                    'CompanyID' =>'OLN',
                    'IsActive'              => '1',
                    'StatusCode'   =>'1',
                    'Incharge'            => $data['new_handler_id'][$i],
                    'CompanyName_A1'=> $data['new_clientname'][$i],
                    'User_C'        => $employee_id,
                    'User_U'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s'),
                    'CUSTTYPE'  =>$data['new_clienttype_id'],
                    'HKID' =>$data['new_hkid'][$i],
                    'DATEOFBIRTH'=>date('Y-m-d', strtotime($data['new_dob'][$i])),
                    'PLACEOFBIRTH'=>$data['new_pob'][$i],
                    'NATIONALITY'=>$data['new_nationality_id'][$i],
                    'HOMEADDRESS'=>$data['new_home_address'][$i],
                    'CORADDRESS'=>$data['new_cor_address'][$i],
                    'OCCUPATION'=>$data['new_occupation'][$i],
                    'EMPLOYER'=>$data['new_employer'][$i],
                    'TELNUM'=>$data['new_hometel'][$i],
                    'OFFICETEL'=>$data['new_offtel'][$i],
                    'FAXNUM'=>$data['new_homefax'][$i],
                    'MOBILE'=>$data['new_mobile'][$i],
                    'EMAIL'=>$data['new_email'][$i],
                    'ISREP'=>(Input :: has('new_acting_id') ? $data['new_acting_id'] : 0),
                    'REPNAME'=>$data['new_personname'][0],
                    'REPNUM'=>$data['new_contactnum'][0]
                );
				}else{
					$data_clientnew[] = array(
					'CustomerID'    =>$nextclient+$i,
                    'CompanyID' =>'OLN',
                    'IsActive'              => '1',
                    'StatusCode'   =>'1',
                    'Incharge'            => $data['new_handler_id'][$i],
                    'CompanyName_A1'=> $data['new_clientcomname'][$i],
                    'User_C'        => $employee_id,
                    'User_U'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s'),
                    'CUSTTYPE'  =>$data['new_clienttype_id'],
                    'ComNum'=>$data['new_comnum'][$i],
                    'PLACEOFINC'=>$data['new_poi'][$i],
                    'BUSADD'=>$data['new_bus_address'][$i],
                    'Nature_A'=>$data['new_nature'][$i],
                    'TELNUM'=>$data['new_telnum'][$i],
                    'FAXNUM'=>$data['new_faxnum'][$i]
                    );
				}
   

            //}

            /* Save to table */
            $id = DB::table('customer')->insert($data_clientnew);
            Debugbar::info($id);
            if($id) {
                $result = true;
            
            }
        //}

        return $nextclient;
    }

    public function getInfo($client_id)
	{
		$result = DB::table('customer as cus')
						
						->select('cus.*')
						->where('cus.CustomerID', $client_id,"and IsActive!=-1")->first();

		return $result;
	}

	public function getListinfo($client_type)
	{
		return DB::select(' SELECT * FROM customer where IsActive !=-1 and CUSTTYPE='.$client_type);
		
	}

	 public function getDirInfo($client_id)
	{
		return DB::select(' SELECT * FROM custdirectors where CustomerID='.$client_id);
	}

	 public function getShareInfo($client_id)
	{
		return DB::select(' SELECT * FROM custshareholders where CustomerID='.$client_id);
	}
	 public function getBenOwnInfo($client_id)
	{
		return DB::select(' SELECT * FROM custbenowner where CustomerID='.$client_id);
	}
	 public function getRepInfo($client_id)
	{
		return DB::select(' SELECT * FROM custreps where CustomerID='.$client_id);
	}

	 public function getDirectors()
	{
		return DB::select(' SELECT * FROM custdirectors');
	}

	public function updateClient($client_id,$data)
	{
			$user_result = DB::table('customer')->where('CustomerID', $client_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}


	public function updateDir($dir_id,$data)
	{
			$user_result = DB::table('custdirectors')->where('DIRECTORID', $dir_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
	public function updateShare($dir_id,$data)
	{
			$user_result = DB::table('custshareholders')->where('SHAREID', $dir_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}

	public function updateOwn($dir_id,$data)
	{
			$user_result = DB::table('custbenowner')->where('OWNERID', $dir_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
	public function updateRep($dir_id,$data)
	{
			$user_result = DB::table('custreps')->where('REPID', $dir_id)->take(1)->update($data);
			
			return ($user_result >= 0) ? true : false;
	}
	public function getNationality(){
        
            return DB::select(' SELECT * FROM country');
    }

    public function createDirectors($item_count,$data,$custid,$checker)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(DIRECTORID) as lastclient FROM custdirectors');
        $lastclient=$get_result[0]->lastclient;
        $nextclient=$lastclient+1;

        $data_clientnew;
        if($checker==1){
        	if($item_count == count($data['new_dirnew'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'DIRECTORID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'DIRECTOR_NAME'              => $data['new_dirnew'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custdirectors')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        
        }}else{
        if($item_count == count($data['new_dir'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'DIRECTORID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'DIRECTOR_NAME'              => $data['new_dir'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custdirectors')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }}

        return $result;
    }

    public function createSharers($item_count,$data,$custid,$checker)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(SHAREID) as lastclient FROM custshareholders');
        $lastclient=$get_result[0]->lastclient;
        $nextclient=$lastclient+1;

        $data_clientnew;
        if($checker==1){
        	if($item_count == count($data['new_sharenew'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
              $data_directorsnew[] = array(
                    'SHAREID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'SHAREHOLDER_NAME'              => $data['new_sharenew'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );
   

            }

            /* Save to table */
            $confnew  = DB::table('custshareholders')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        
        }}else{
        if($item_count == count($data['new_share'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'SHAREID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'SHAREHOLDER_NAME'              => $data['new_share'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custshareholders')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }}

        return $result;
    }

    public function createBenOwners($item_count,$data,$custid,$checker)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(OWNERID) as lastclient FROM custbenowner');
        $lastclient=$get_result[0]->lastclient;
        $nextclient=$lastclient+1;

        $data_clientnew;
        if($checker==1){
        	if($item_count == count($data['new_bennew'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'OWNERID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'OWNER_NAME'              => $data['new_bennew'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custbenowner')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        
        }}else{
        if($item_count == count($data['new_ben'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'OWNERID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'OWNER_NAME'              => $data['new_ben'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custbenowner')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }}

        return $result;
    }

    public function createReps($item_count,$data,$custid,$checker)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;

        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(REPID) as lastclient FROM custreps');
        $lastclient=$get_result[0]->lastclient;
        $nextclient=$lastclient+1;

        $data_clientnew;
         if($checker==1){
        	if($item_count == count($data['new_personnew'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'REPID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'REPNAME'              => $data['new_personnew'][$i],
                    'HKID'              => $data['new_personhkidnew'][$i],
                    'POSITION'              => $data['new_personposnew'][$i],
                    'CONNUM'              => $data['new_personconnumnew'][$i],
                    'EMAIL'              => $data['new_personemailnew'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

   

            }

            /* Save to table */
            $confnew  = DB::table('custreps')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        
        }}else{
        if($item_count == count($data['new_person'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               $data_directorsnew[] = array(
                    'REPID'    =>$nextclient+$i,
                    'CustomerID' =>$custid,
                    'REPNAME'              => $data['new_person'][$i],
                    'HKID'              => $data['new_personhkid'][$i],
                    'POSITION'              => $data['new_personpos'][$i],
                    'CONNUM'              => $data['new_personconnum'][$i],
                    'EMAIL'              => $data['new_personemail'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );


            }

            /* Save to table */
            $confnew  = DB::table('custreps')->insert($data_directorsnew);
            
            if($confnew) {
                $result = true;
         
            }
        }}

        return $result;
    }

    	public function DeleteDir($dir_id)
	{
		$ts_result  = DB::table('custdirectors')->where('DIRECTORID', $dir_id)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteShare($dir_id)
	{
		$ts_result  = DB::table('custshareholders')->where('SHAREID', $dir_id)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteOwner($dir_id)
	{
		$ts_result  = DB::table('custbenowner')->where('OWNERID', $dir_id)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteRep($dir_id)
	{
		$ts_result  = DB::table('custreps')->where('REPID', $dir_id)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}

	public function getDirectorsCount($client_id)
	{
		$query = DB::table('custdirectors')
					->where('CustomerID', '=', $client_id);

		return $query->count();
	}
	public function getSharesCount($client_id)
	{
		$query = DB::table('custshareholders')
					->where('CustomerID', '=', $client_id);

		return $query->count();
	}
	public function getOwnersCount($client_id)
	{
		$query = DB::table('custbenowner')
					->where('CustomerID', '=', $client_id);

		return $query->count();
	}
	public function getRepCount($client_id)
	{
		$query = DB::table('custreps')
					->where('CustomerID', '=', $client_id);

		return $query->count();
	}
	//new 
		public function DeleteDirClient($clientid)
	{
		$ts_result  = DB::table('custdirectors')->where('CustomerID', $clientid)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteShareClient($clientid)
	{
		$ts_result  = DB::table('custshareholders')->where('CustomerID', $clientid)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteOwnerClient($clientid)
	{
		$ts_result  = DB::table('custbenowner')->where('CustomerID', $clientid)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
		public function DeleteRepClient($clientid)
	{
		$ts_result  = DB::table('custreps')->where('CustomerID', $clientid)->take(1)->delete();

		return ($ts_result >= 0 ) ? true : false;	
	}
}
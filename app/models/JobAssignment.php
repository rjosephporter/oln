<?php

class JobAssignment{
	/* 
	 * Billing History 
	 * $id - JobAssignmentID
	 *
	 */
	public function getBH($id){
        return DB::select("SELECT
	jobassignment.JobAssignmentID, jobassignment.Incharge, jobassignment.CustomerID, jobassignment.DesignatedDate, jobassignment.Description_A, 
	timesheet.WorkDate, timesheet.UniqueID, 
	timesheetwork.EmployeeID, timesheetwork.WorkHour, timesheetwork.ChargeHour, timesheetwork.ChargeRate,
	jobassignmentbilling.PlanBillingDate, IFNULL(jobassignmentbilling.PlanAmount,0) AS PlanAmount, jobassignmentbilling.Description,
	IFNULL(invoicedetail.Amount,0) AS Amount, (jobassignmentbilling.PlanAmount - IFNULL(invoicedetail.Amount,0)) AS unbilled_amount
        FROM
            jobassignment
        LEFT JOIN  timesheet 
            ON timesheet.JobAssignmentID = $id
        LEFT JOIN timesheetwork
            ON timesheet.UniqueID = timesheetwork.UniqueID
        LEFT JOIN jobassignmentbilling
            ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
        LEFT JOIN invoicedetail
            ON invoicedetail.ReferenceID = timesheetwork.UniqueID
        WHERE jobassignment.JobAssignmentID  = $id
        ORDER BY unbilled_amount DESC, jobassignment.JobAssignmentID ASC, timesheet.UniqueID DESC, timesheet.WorkDate DESC");
    }
        /* 
	 *  Matters by ID:
         * 
	 *  1 - Green
	 *  2 - Orange
	 *  3 - Red
	 *  4 - Completed
	 *  5 - Untouched
	 * 
	 *  $ndx - Row Index
	 *  $num - Number of Rows to be displayed
	 */     
    public function getM($id,$ndx,$num){
        if($id == 4)
            return DB::select('SELECT 
            jobassignment.JobAssignmentID, jobassignment.Description_A
            FROM jobassignment
            WHERE jobassignment.Completed = 1
            GROUP BY jobassignment.JobAssignmentID LIMIT '.$ndx.','.$num);
        else{
            $find = $id <= 1 ? "<= $id" : ($id == 2 ? "= $id " : ($id == 3 ? "> 2" : "IS NULL"));
            return DB::select('SELECT 
            jobassignment.JobAssignmentID, jobassignment.Description_A
            FROM jobassignment
            WHERE jobassignment.Completed = 0
            AND TIMESTAMPDIFF(MONTH,(SELECT timesheet.WorkDate FROM timesheet
                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID ORDER BY timesheet.UniqueID DESC, timesheet.WorkDate DESC LIMIT 1),CURDATE()) '.$find.'
            GROUP BY jobassignment.JobAssignmentID LIMIT '.$ndx.','.$num);
        }
    }
    /*
	 * Timeline by $id - JobAssignmentID 
	 */    
    public function getTL($id){
        return DB::select("SELECT
	jobassignment.JobAssignmentID, jobassignment.Incharge, jobassignment.DesignatedDate, jobassignment.Description_A, 
	timesheet.WorkDate, timesheet.UniqueID, 
	timesheetwork.EmployeeID, timesheetwork.Comment
        FROM
            jobassignment
        LEFT JOIN  timesheet 
            ON timesheet.JobAssignmentID = $id
        LEFT JOIN timesheetwork
            ON timesheet.UniqueID = timesheetwork.UniqueID
        WHERE jobassignment.JobAssignmentID  = $id
        ORDER BY
	timesheet.UniqueID DESC, timesheet.WorkDate DESC");
    }
    /* Customers */
    public function getC(){
        return DB::select("SELECT CustomerID, Incharge FROM customer");
    }
    /* Employee */
    public function getE(){
        return DB::select("SELECT DISTINCT(`Incharge`)AS EmployeeID from jobassignment ORDER BY Incharge ASC");
    }
    
    public function getMatters(){
        return DB::select("SELECT jobassignment.JobAssignmentID, jobassignment.Description_A, jobassignment.Completed, TIMESTAMPDIFF(MONTH,(
                    SELECT WorkDate FROM timesheet
                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                        ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl
                    FROM 
                        jobassignment
                    GROUP BY jobassignment.JobAssignmentID ORDER BY jobassignment.JobAssignmentID ASC, jobassignment.DesignatedDate ASC");
    }//end-function
    
    public function getMatterMIP($id){
    	return DB::select("SELECT JobAssignmentID, Completed FROM jobassignment WHERE JobAssignmentID = :matter_id AND Completed = 9 LIMIT 1",array('matter_id' => $id));
    }
    
    public function getMbyH($h){
        return DB::select("SELECT jobassignment.JobAssignmentID, jobassignment.Description_A, jobassignment.Completed, TIMESTAMPDIFF(MONTH,(
             SELECT WorkDate FROM timesheet
             WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                 ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl
             FROM 
                 jobassignment
             WHERE jobassignment.Incharge = '$h'
             GROUP BY jobassignment.JobAssignmentID");
    }
    
    public function getIntro($i){
    	return DB::select("SELECT Incharge FROM customer WHERE CustomerID = '$i' LIMIT 1");
    }
   
    public function getHT($handler,$type){    
        if($type==4)
            return DB::select('SELECT 
            jobassignment.JobAssignmentID, jobassignment.Description_A
            FROM jobassignment
            WHERE jobassignment.Completed = 1
            AND jobassignment.Incharge = "'.$handler.'"
            GROUP BY jobassignment.JobAssignmentID');
        else{
            $find = $type <= 1 ? "<= $type" : ($type == 2 ? "= $type " : ($type == 3 ? "> 2" : "IS NULL"));
            return DB::select('SELECT 
            jobassignment.JobAssignmentID, jobassignment.Description_A
            FROM jobassignment
            WHERE TIMESTAMPDIFF(MONTH,(
                SELECT WorkDate FROM timesheet
                    WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1),CURDATE()) '.$find.'
            AND jobassignment.Completed = 0
            AND jobassignment.Incharge = "'.$handler.'"
            GROUP BY jobassignment.JobAssignmentID');
        }
    }
    
    public function getAllMatters(){
        return DB::select('SELECT 
	jobassignment.JobAssignmentID, jobassignment.Description_A, jobassignment.CustomerID, jobassignment.Completed, jobassignment.Incharge, TIMESTAMPDIFF(MONTH,(
		SELECT timesheet.WorkDate FROM timesheet
		WHERE timesheet.JobAssignmentID = jobassignment.JobAssignmentID and jobassignment.Completed !=-1
		ORDER BY timesheet.UniqueID DESC, timesheet.WorkDate DESC LIMIT 1),CURDATE()) AS mn_invl
        FROM jobassignment');
    }
       
    public function getMattersBill(){
        return DB::select("
            SELECT 
            jobassignment.JobAssignmentID, jobassignment.CustomerID, customer.CompanyName_A1 AS client_name, jobassignment.DesignatedDate, jobassignment.Description_A, jobassignment.Remark, jobassignment.Completed, jobassignment.CompletedBy, jobassignment.Incharge, user.email_address AS handler_email, @last_trans_date:=(
                    SELECT WorkDate FROM timesheet
                    WHERE JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1) AS last_trans_date, @last_cost_account_trans:=(
                        SELECT jobassignmentcost.date_c 
                            FROM 
                                jobassignmentcost
                            WHERE
                                jobassignmentcost.JobAssignmentID = jobassignment.JobAssignmentID
                            ORDER BY
                                jobassignmentcost.date_c DESC
                        LIMIT 1
                    ) AS last_cost_account_trans, 
            @mn_invl:=(TIMESTAMPDIFF(WEEK,(@last_trans_date),CURDATE())) AS mn_invl,@mn_temp:=(TIMESTAMPDIFF(WEEK,(@last_cost_account_trans),CURDATE())) AS mn_temp,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0))AS billable_amount, SUM(IFNULL(invoicedetail.Amount,0)) AS billed_amount,
            (CASE WHEN jobassignment.Completed = 1 THEN 'blue' 
            ELSE(
                CASE WHEN @mn_invl IS NULL THEN (
                    CASE WHEN @mn_temp IS NULL
                        THEN 'gray'
                    ELSE(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )ELSE(
                    CASE WHEN @mn_temp < @mn_invl
                    THEN(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )ELSE(
                        CASE 
                            WHEN @mn_invl <= 4 THEN 'green'
                            WHEN @mn_invl > 4 AND @mn_invl <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )
                END
            )
            END) AS matter_type,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0)) - SUM(IFNULL(invoicedetail.Amount,0)) AS unbilled, 
                IFNULL((SELECT role 
                    FROM jobassignmentclosed 
                    WHERE jobassignmentclosed.JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY jobassignmentclosed.id DESC LIMIT 1),0
                ) AS CompleteStatus
            FROM jobassignment
            LEFT JOIN customer
                ON jobassignment.CustomerID = customer.CustomerID
            LEFT JOIN user
                ON user.EmployeeID = jobassignment.Incharge
            LEFT JOIN  timesheet 
                ON timesheet.JobAssignmentID = jobassignment.JobAssignmentID
            LEFT JOIN timesheetwork
                ON timesheet.UniqueID = timesheetwork.UniqueID
            LEFT JOIN jobassignmentbilling
                ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
            LEFT JOIN invoicedetail
                ON invoicedetail.ReferenceID = timesheetwork.UniqueID
            WHERE jobassignment.Completed < 2
            GROUP BY jobassignment.JobAssignmentID
            ORDER BY jobassignment.JobAssignmentID ASC");
    }
    
    public function getAllDetailsM($id){
        return DB::select("
            SELECT 
            jobassignment.JobAssignmentID, jobassignment.CustomerID, customer.CompanyName_A1 AS client_name, jobassignment.DesignatedDate, jobassignment.Description_A, jobassignment.Remark, jobassignment.Completed, jobassignment.CompletedBy, jobassignment.Incharge, jobassignmentdetails.introducer ,user.email_address AS handler_email, @last_trans_date:=(
                    SELECT WorkDate FROM timesheet
                    WHERE JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY UniqueID DESC, WorkDate DESC LIMIT 1) AS last_trans_date, @last_cost_account_trans:=(
                        SELECT jobassignmentcost.date_c 
                            FROM 
                                jobassignmentcost
                            WHERE
                                jobassignmentcost.JobAssignmentID = jobassignment.JobAssignmentID
                            ORDER BY
                                jobassignmentcost.date_c DESC
                        LIMIT 1
                    ) AS last_cost_account_trans, 
            @mn_invl:=(TIMESTAMPDIFF(WEEK,(@last_trans_date),CURDATE())) AS mn_invl,@mn_temp:=(TIMESTAMPDIFF(WEEK,(@last_cost_account_trans),CURDATE())) AS mn_temp,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0))AS billable_amount, SUM(IFNULL(invoicedetail.Amount,0)) AS billed_amount,
            (CASE WHEN jobassignment.Completed = 1 THEN 'blue' 
            ELSE(
                CASE WHEN @mn_invl IS NULL THEN (
                    CASE WHEN @mn_temp IS NULL
                        THEN 'gray'
                    ELSE(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )ELSE(
                    CASE WHEN @mn_temp < @mn_invl
                    THEN(
                        CASE 
                            WHEN @mn_temp <= 4 THEN 'green'
                            WHEN @mn_temp > 4 AND @mn_temp <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )ELSE(
                        CASE 
                            WHEN @mn_invl <= 4 THEN 'green'
                            WHEN @mn_invl > 4 AND @mn_invl <= 8 THEN 'orange'
                            ELSE 'red'
                        END
                    )
                    END
                )
                END
            )
            END) AS matter_type,
            SUM(IFNULL(jobassignmentbilling.PlanAmount,0)) - SUM(IFNULL(invoicedetail.Amount,0)) AS unbilled, 
                IFNULL((SELECT role 
                    FROM jobassignmentclosed 
                    WHERE jobassignmentclosed.JobAssignmentID = jobassignment.JobAssignmentID
                    ORDER BY jobassignmentclosed.id DESC LIMIT 1),0
                ) AS CompleteStatus
            FROM jobassignment
            LEFT JOIN customer
                ON jobassignment.CustomerID = customer.CustomerID
            LEFT JOIN jobassignmentdetails
                ON jobassignmentdetails.JobAssignmentID = jobassignment.JobAssignmentID
            LEFT JOIN user
                ON user.EmployeeID = jobassignment.Incharge
            LEFT JOIN  timesheet 
                ON timesheet.JobAssignmentID = jobassignment.JobAssignmentID
            LEFT JOIN timesheetwork
                ON timesheet.UniqueID = timesheetwork.UniqueID
            LEFT JOIN jobassignmentbilling
                ON jobassignmentbilling.UniqueID = timesheetwork.UniqueID
            LEFT JOIN invoicedetail
                ON invoicedetail.ReferenceID = timesheetwork.UniqueID
            WHERE jobassignment.JobAssignmentID = :matterid
            GROUP BY jobassignment.JobAssignmentID
            ORDER BY jobassignment.JobAssignmentID ASC",array('matterid' => $id));
    }
    
    public function getReports(){
        return DB::select('SELECT * FROM report_matters ORDER BY id ASC');
    }
    
    public function update_date($obj){
        DB::select("UPDATE report_matters
                SET trans_date = CONCAT(".$obj->workdate_year.",'-',".$obj->workdate_month.",'-',".$obj->workdate_day.")
                WHERE id = ".$obj->id." LIMIT 1");
    }

    public function createMatter($item_count, $data)
    {
        $result = false;
        $id_active = (Session::has('principal_user')) ? Session::get('principal_user') : Auth::user()->user_id;
        $employee_id = User::getEmployeeID($id_active);

        $data_ts = $data_tsw = array();
        $get_result = DB::select('SELECT MAX(JobAssignmentID) as lastmatter FROM JobAssignment');
        $lastmatter=$get_result[0]->lastmatter;
        $nextmatter=$lastmatter+1;

        if($item_count == count($data['new_customer_id'])) {
            for ($i = 0; $i < $item_count ; $i++) { 
               
                $data_matternew[] = array(
                    'JobAssignmentID'    =>$nextmatter+$i,
                    'CompanyID' =>'OLN',
                    'CustomerID'              => $data['new_customer_id'][$i],
                    'BillingCode'       => 'LEGAL-LEG',
                    'Incharge'            => $data['new_handler_id'][$i],
                    //'Assistant'            => $data['new_assistant_id'][$i],
                    'Team'   =>'FEEEARNER',
                    'Completed' =>0,
                    'DesignatedDate'=> date('Y-m-d', strtotime($data['new_designated_date'][$i])),
                    'Description_A'=> $data['new_matter_description'][$i],
                    'Remark' => $data['new_matter_description'][$i],
                    'User_c'        => $employee_id,
                    'Date_C'        => date('Y-m-d H:i:s'),
                    'Date_U'        => date('Y-m-d H:i:s')
                );

            }

            /* Save to table */
            $confnew  = DB::table('JobAssignment')->insert($data_matternew);
            
            if($confnew) {
                $result = true;
            }
        }

        return $result;
    }

    public function getAllCurrentMatters(){
        return DB::select('SELECT 
    *  FROM jobassignment where Completed !=-1');
    }
    
    public function setHistory($obj){       
        DB::insert("INSERT INTO history_summary (date, time, matters, data, status) VALUES (:date, :time, :matters, :data, 1)",array(
            'date' => $obj['date'],
            'time' => $obj['time'],
            'matters' => $obj['info'],
            'data' => $obj['data']
        ));
    }
    
    public function updateHistory($obj){
        DB::update("UPDATE history_summary SET time = :time, matters = :matters, data = :data WHERE date = :date LIMIT 1",array(
            'time' => $obj['time'],
            'matters' => $obj['info'],
            'data' => $obj['data'],
            'date' => $obj['date']
        ));
    }
    
    public function checkHist($date_){
        return DB::select('SELECT * FROM history_summary WHERE date = :date',array('date' => $date_));
    }
    
    public function completeMatter($obj){
        DB::update("UPDATE jobassignment SET Completed = 1, CompleteDate = :datetime, CompletedBy = :currentuser WHERE JobAssignmentID = :matterid LIMIT 1",array(
            'datetime' => $obj['datetime'],
            'currentuser' => $obj['current_user'],
            'matterid' => $obj['matter_id']
        ));
    }
    
    public function setMatterIP($obj,$val){
        DB::update("UPDATE jobassignment SET Completed = :complete, Date_U = :datetime, User_U = :currentuser WHERE JobAssignmentID = :matterid LIMIT 1",array(
            'complete' => $val,
            'datetime' => $obj['datetime'],
            'currentuser' => $obj['current_user'],
            'matterid' => $obj['matter_id']
        ));
    }
    
    public function checkMatterStatus($id){
        return DB::select("SELECT Completed FROM jobassignment WHERE JobAssignmentID = :matterid LIMIT 1",array('matterid' => $id));
    }
}//end-class
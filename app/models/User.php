<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use HasRole;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'user_id';

	/**
	 * Set table timestamp
	 *
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');


	public function getMattersCount($user_id)
	{
		$query = DB::table('jobassignment AS ja')
					->leftJoin('user AS usr', 'usr.EmployeeID', '=', 'ja.Incharge');
					//->where('usr.user_id', '=', $user_id);

		//if($user_id !== 3) 
		//if(!in_array($user_id, Config::get('oln.admin')))					
		//if(!User::hasRoleById($user_id, 'Admin'))								
		//if(!User::isAdmin($user_id))
		if(User::hasRoleById($user_id, 'Admin')){

		}else{
			$query->where('usr.user_id', '=', $user_id);
		}

		return $query->count();
	}

	public function getClientsCount($user_id)
	{
		$query = DB::table('customer as cu')
					->leftJoin('user AS usr', 'usr.EmployeeID', '=', 'cu.Incharge');
					//->where('usr.user_id', '=', $user_id);

		//if($user_id !== 3) 
		//if(!in_array($user_id, Config::get('oln.admin')))					
		//if(!User::hasRoleById($user_id, 'Admin'))
		//if(!User::isAdmin($user_id))
		if(User::hasRoleById($user_id, 'Admin')){

		}else{
			$query->where('usr.user_id', '=', $user_id);
		}

		return $query->count();
	}

	//public function getRole($user_id = array())
	public function getEmployeeJob($user_id = array())
	{
		$query = DB::table('user as usr')
					->select('usr.EmployeeID','ejob.Effective','ejob.DepartmentCode','dpt.DepartmentName_A','ejob.TimeCostID','tc.Description_A','tc.HourlyRate', 'ejob.PositionCode', 'pos.PositionDesc_A')
					->leftJoin('employee as emp', 'emp.EmployeeID', '=', 'usr.EmployeeID')
					->leftJoin('employee_position as ep', 'ep.EmployeeID', '=', 'emp.EmployeeID')
					->leftJoin(DB::raw('
						(select
							EmployeeID,
							Effective,
							DepartmentCode,
							TimeCostID,
							PositionCode
						from employeejob
						order by Effective desc) as ejob
					'), 'ejob.EmployeeID', '=', 'emp.EmployeeID')
					->leftJoin('timecost as tc', 'tc.TimeCostID', '=', 'ejob.TimeCostID')
					->leftJoin('department as dpt', 'dpt.DepartmentCode', '=', 'ejob.DepartmentCode')
					//->leftJoin('position as pos', 'pos.PositionCode', '=', 'ejob.PositionCode')
					->leftJoin('position as pos', 'pos.PositionCode', '=', 'ep.PositionCode')
					//->where('usr.user_id', '=', $user_id)
					->groupBy('usr.EmployeeID');

		if(!empty($user_id)) {
			$query->whereIn('usr.user_id', $user_id);
		}

		return $query->get();
	}

	public static function getEmployeeID($user_id)
	{
		$employee_id = DB::table('user')->where('user_id',$user_id)->pluck('EmployeeID');
		return $employee_id;
	}

	public static function getUserID($employee_id)
	{
		$user_id = DB::table('user')->where('EmployeeID', $employee_id)->pluck('user_id');
		return $user_id;
	}

	public static function isAdmin($user_id)
	{
		//return (in_array($user_id, Config::get('oln.admin'))) ? true : false;
		//return (User::hasRoleById($user_id, 'Admin')) ? true : false;
		return ($user_id == Config::get('oln.admin_id')) ? true : false ;
	}

	public static function hasRoleById($user_id, $role)
	{
		return User::find($user_id)->hasRole($role);
	}

	public static function getEmployeesByAssistant($user_id, $show_all = false)
	{
		$result = array();
		if(User::find($user_id)->hasRole('PersonalAssistant')) {
			$query = DB::table('employee_assistant as ea')
							->leftJoin('user as emp_usr', 'emp_usr.EmployeeID', '=', 'ea.employee_id')
							->leftJoin('user as ast_usr', 'ast_usr.EmployeeID', '=', 'ea.assistant_id')
							//->where('ast_usr.user_id', $user_id)
							->select('emp_usr.user_id', 'ea.employee_id')
							->remember(30);

			if( !$show_all )
				$query->where('ast_usr.user_id', $user_id);

			$result = $query->get();
		}

		return $result;
	}

	public static function getAssistantsByEmployee($user_id)
	{
		$result = array();
		
		$query = DB::table('employee_assistant as ea')
						->leftJoin('user as emp_usr', 'emp_usr.EmployeeID', '=', 'ea.employee_id')
						->leftJoin('user as ast_usr', 'ast_usr.EmployeeID', '=', 'ea.assistant_id')
						->where('emp_usr.user_id', $user_id)
						->select('ast_usr.user_id', 'ea.assistant_id')
						->remember(30);
		$result = $query->get();

		return $result;		
	}

	public static function getFullName($employee_id)
	{
		$full_name = '';
		if(User::where('EmployeeID', $employee_id)->count() > 0) {
			$user_id = User::getUserID($employee_id);
			$user = User::find($user_id);
			$full_name = $user->first_name . ' ' . $user->last_name;
		}
		if(trim($full_name) == '')
			$full_name = Employee::find($employee_id)->NickName;
		if(empty($full_name))
			$full_name = $employee_id;

		return $full_name;
	}
        
        public function getEmployeesByRoles($role){
            return DB::select('SELECT employee.`EmployeeID`, user.user_id, employee.NickName FROM `employee`
                                LEFT JOIN user
                                ON user.`EmployeeID` = employee.`EmployeeID`
                                LEFT JOIN assigned_roles
                                ON assigned_roles.user_id = user.user_id
                                LEFT JOIN roles
                                ON roles.id = assigned_roles.role_id
                                WHERE roles.name = :rolename',array('rolename'=>$role));
        }
}

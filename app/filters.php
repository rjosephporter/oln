<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//Singleton (global) object
	App::singleton('globalVar', function() {
		//Year Range
		$app = new stdClass;
		$min_year = 2011;
		$max_year = date('Y');
		$app->year_range = range($max_year, $min_year);

		return $app;
	});

		$lang = Session::get('language','en');
		App::setLocale($lang);


	$app = App::make('globalVar');
	View::share('globalVar', $app);
});


App::after(function($request, $response)
{
	//
});

App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::to('auth/login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/**
 * Role Filters
 *
 */
Route::filter('admin_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('Admin')) {
		App::abort(404);
	}
});

Route::filter('handler_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('Handler')) {
		App::abort(404);
	}	
});

Route::filter('accounting_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('Accounting')) {
		App::abort(404);
	}	
});

Route::filter('pa_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('PersonalAssistant')) {
		App::abort(404);
	}	
});

Route::filter('sysadmin_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('Sysadmin')) {
		App::abort(404);
	}	
});

Route::filter('superadmin_access', function() {
	if(Auth::guest()) return Redirect::to('auth/login');
	$principal_user = Session::get('principal_user');
	if(!User::find($principal_user)->hasRole('Superadmin')) {
		App::abort(404);
	}	
});

View::composer('template/default',function($view){
    $base = new BaseController;
    $view->with('styles', $base->getStyles());
    $view->with('scripts',$base->getScripts());
    $view->with('user_id', (Auth::check()) ? Auth::user()->user_id : 0);
    $view->with('principal_user', (Session::has('principal_user')) ? Session::get('principal_user') : Auth::id() );
    $view->with('user_tasks', (Auth::check()) ? $base->getUsertasks() : 0);
    $view->with('avatar',$base->getUseravatar());
    Debugbar::info(Session::all()); 
});
/*
View::composer('pages/common/employee_switcher', function($view) {
	$logged_in_user = Auth::user();
	$employees = User::getEmployeesByAssistant($logged_in_user->user_id);

	$employee_list = array();
	foreach($employees as $employee)
		$employee_list[$employee->employee_id] = $employee->employee_id;

	$view->with('employee_list', $employee_list);
});
*/

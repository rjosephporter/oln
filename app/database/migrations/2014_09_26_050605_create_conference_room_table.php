<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConferenceRoomTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('conference_room', function($table)
		{
			$table->engine = 'MyISAM';
		    $table->increments('id');
		    $table->string('name', '100');
		    $table->text('description')->nullable();
		    $table->tinyInteger('status')->default(1)->index();
		    $table->char('created_by', 10)->nullable()->index();
		    $table->timestamps();
		    $table->softDeletes();

		});

		Schema::create('conference_room_reservation', function($table)
		{
			$table->engine = 'MyISAM';
			$table->increments('id');
			$table->integer('conference_room_id')->index();
			$table->char('reserved_by', 10)->index();
			$table->date('reservation_date');
			$table->string('purpose', 150);
			$table->text('notes');
			$table->time('start_time');
			$table->time('end_time');
			$table->tinyInteger('status')->default(1)->index();
			$table->char('created_by', 10)->nullable()->index();
			$table->timestamps();
		    $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('conference_room');
		Schema::drop('conference_room_reservation');
	}

}

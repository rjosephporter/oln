<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicePaymentLog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_payment_log', function($table)
		{
			$table->engine = 'MyISAM';
		    $table->bigIncrements('id');
		    $table->bigInteger('invoice_number')->nullable()->index();
		    $table->char('batch_number', '12')->index();
		    $table->decimal('paid_amount', 19, 4);
		    $table->dateTime('payment_date');
		    $table->tinyInteger('status')->default(1)->index();
		    $table->char('created_by', 10)->nullable()->index();
		    $table->timestamps();
		    $table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_payment_log');
	}

}

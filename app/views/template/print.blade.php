<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
    <link href="<?php echo "http://".$_SERVER['SERVER_NAME']."/assets/css/bootstrap-print.css" ?>" rel="stylesheet">
    <link href="/assets/thisapp/css/bootstrap-print.css" rel="stylesheet">
    <style type="text/css">
        @page {
            size: A4;   /* auto is the initial value */
            margin: 5%;
        }

        .top-buffer {
            margin-top:30px;
        }
    </style>
  </head>
  <body>
        <!-- Begin page content -->
        @yield('content')
        <!--    </div>-->
  </body>
</html>

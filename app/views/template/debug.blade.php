<html>
<head>
	<title>Debug Page</title>
</head>
<body>
	<table border="1">
		<thead>
			<tr>
				<th>Matter ID</th>
				<th>Handler</th>
				<th>Client Name</th>
				<th>Matter Name</th>
				<th>Unbilled Amount</th>
			</tr>
		</thead>
		<tbody>
			@foreach($matters as $matter)
			<tr>
				<td>{{ $matter->JobAssignmentID }}</td>
				<td>{{ $matter->Incharge }}</td>
				<td>{{ $matter->client_name }}</td>
				<td>{{ $matter->Description_A }}</td>
				<td>${{ number_format($matter->unbilled, 2) }}</td>			
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
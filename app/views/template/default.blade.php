<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>Oldham, Li & Nie Law Firm</title>

    <?php 
    foreach($styles as $style):
        echo HTML::style($style);
    endforeach;
    foreach($scripts['before'] as $script):
        echo HTML::script($script);
    endforeach;
    ?>

    <style> 
        .top-buffer { margin-top:15px; }
        .bottom-buffer { margin-bottom:10px; }
        .tablesorter thead .disabled {display: none !important}
    </style>

    <script>
        var base_url = '{{ URL::to('/') }}';
        var user_id = '{{ $user_id }}';
        var principal_user = '{{ $principal_user }}';
        function printIframe(iframeDivName){
            frames[iframeDivName].focus();
            frames[iframeDivName].print();
        }
    </script>

    @section('header-custom-css')
    @show

  </head>

   <body>

    <!-- Wrap all page content here -->

      <!-- Fixed navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">

            @include('pages/common/navbar_menu')
          
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right" style="display: inline-flex">
                @if (Auth::check())
                <li style="display:table">
                    <form method="GET" action="{{ URL::to('global-search') }}">
                        <div class="input-group" style="padding: 18px 0px; width:160px;margin:0px -8px 0px 0px;">
                            <input type="text" class="form-control input-sm" name="search-keyword" value="{{ Input::get('search-keyword', '') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm" type="submit"><small>Global Search</small></button>
                            </span>
                        </div>
                    </form>
                </li>
                @if($user_tasks > 0))
                <li>
                    
                        <a href="/commcenter/" title="Pending tasks" style="padding:25px 0px 25px 10px;height:50px;margin-right:-10px;">
                            <span class="glyphicon glyphicon-list"></span>
                            <span class="badge">{{$user_tasks}}</span>
                        </a>
                    
                </li>
                @endif
                
                <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="margin-right:-8px;">{{ $avatar }} &nbsp;&nbsp;{{ Lang::get('lang.common.header.Welcome') }}, {{ (!empty(Auth::user()->first_name)) ? Auth::user()->first_name : Auth::user()->EmployeeID }}  <b class="caret"></b></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="/account-settings">Account Settings</a></li>
                        <li><a href="/commcenter">To Do List</a></li>
                        <li class="divider"></li>
                        <li><a href="/auth/logout"><span class="glyphicon glyphicon-off"></span> Log Out</a></li>
                      </ul>
                
                </li>
                <li>
                   @include('pages/common/employee_switcher')               
                </li>
                
                <form method="post" action='/utils/language' style="padding:22px 10px 0px 10px;z-index:999;" id="_lang_form" name="_lang_form">
                    <li>
                        <img style="cursor: pointer;" src="/assets/thisapp/images/app/en.png" onclick="_submit('en')">
                        <img style="cursor: pointer;" src="/assets/thisapp/images/app/fr.png" onclick="_submit('fr')">
                        <label style="color: white;">{{ strtoupper(Session::get('language','en')) }}</label>
                    </li>
                    <input type="hidden" name="lang" id="_lang" value="">
                </form>
                
                @endif
            </ul>
          </div><!--/.nav-collapse -->

        </div>
      </div>

      @if(Auth::check() && !Auth::user()->changed_password)
      <div class="container hidden-print">
          <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="alert alert-warning" role="alert">
                    <i class="fa fa-exclamation-circle"></i> You are still using your default password. Change it by clicking <strong><u><a href="{{ URL::to('account-settings') }}" style="color:blue">here.</a></u></strong>
                </div>
            </div>
          </div>
      </div>
      @endif

    <!-- Begin page content -->
    @yield('content')
    <!--    </div>-->

    <div id="footer" class="hidden-print">
      <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-6">
                <p class="text-muted">{{ Lang::get('lang.common.header.MSS ASIA') }}</p>
            </div>
            @if(App::environment('live', 'richard_local'))
            <div class="col-md-6 col-xs-6">
                <p class="text-muted pull-right">
                    @if(Auth::check() && Auth::user()->EmployeeID == 'THF')
                    <a href="http://kanboard.m-s-s-asia.com/?controller=board&action=readonly&token=0e8fc11d065e6a0e7f51cb22337c4e4ec98204583fc0cd65503d1a121f02" target="_blank" class="btn btn-xs btn-info">Bug Report List</a> 
                    @endif
                    {{ Lang::get('lang.common.header.Last Updated [Database]') }}: {{ date('M j, Y', strtotime(DB::table('sourcetable_status')->max('last_update_date'))) }} | {{ Lang::get('lang.common.header.Last Updated [Codes]') }}: {{ date('M j, Y', strtotime(shell_exec("cd /home/vagrant/sites/oln && /home/vagrant/lampstack/git/bin/git log -1 --pretty=format:'%ci' --abbrev-commit"))) }}</p>
            </div>
            @endif            
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    @section('footer-custom-js')
    @show

    <?php 
    foreach($scripts['after'] as $script):
        echo HTML::script($script);
    endforeach;
    ?>
    <script>
    $(document).ready(function(){
        $('.popover-markup > .trigger').popover({
            html : true,
            content: function() {
              return $(this).parent().find('.content').html();
            },
            container: 'body',
            trigger: 'focus',
            placement: 'bottom',
            template: '<div class="popover navbar-menu" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        });

        $('.feedback-btn').addClass('hidden-print');
        
        $.sessionTimeout({
            message: 'Your session will expire in one minute!',
            keepAlive: false,
            logoutUrl: '/auth/logout',
            redirUrl: '/auth/logout?expired=yes',
            warnAfter: <?php echo Config::get('session.lifetime')*60*1000; ?>,
            redirAfter: <?php echo Config::get('session.lifetime')*60*1000 + (60*100); ?>
        });
    });
    $(document).ajaxError(function(e, xhr, settings, exception) {
        $.bootstrapGrowl("Oops there was an error!", {
            ele: 'body',
            type: 'danger',
            align: 'center',
            width: 'auto'
        });
    });

    $.feedback({
        ajaxURL: base_url + '/feedback/send',
        html2canvasURL: 'assets/feedback/html2canvas.min.js'
    });

    function _submit(ln){
        $("#_lang").val(ln);
        $("#_lang_form").submit();
    }
    </script>
  </body>
</html>
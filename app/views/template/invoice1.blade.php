<table width="600px" valign="top" style="font-size:12px;font-family:arial;border:1px #000 solid;padding:10px;">
	<tr>
		<td width="100px" valign="top"></td>
		<td width="400px" valign="top"><img src="http://{{ $_SERVER['HTTP_HOST'] }}/assets/thisapp/images/oln_invoice_logo.png" style="width:400px; height: auto;"/></td>
		<td width="100px" valign="top"></td>
	</tr>
	<!-- ADDRESS & CONTACT INFORMATION -->
	<tr>
		<td width="600px" valign="top" colspan="3" style="padding-top:10px;">
			<table width="600px" valign="top" style="font-size:12px; font-family:arial;">
				<tr>
					<td width="300px" valign="top"> 
						<table width="300px" valign="top" align="left" style="font-size:12px; font-family:arial;font-weight:normal;">
							<tr>
								<td width="300px" valign="top" align="left">{{ $template['company_address_1'] }}</td>
							</tr>
							<tr>
								<td width="300px" valign="top" align="left">{{ $template['company_address_2'] }}</td>
							</tr>
							<tr>
								<td width="300px" valign="top" align="left">{{ $template['company_address_3'] }}</td>
							</tr>
						</table>
					</td>
					<td width="300px" valign="top">
						<table width="300px" valign="top" align="right" style="font-size:12px; font-family:arial;font-weight:normal;">
							<tr>
								<td width="300px" valign="top" align="right">Tel: {{ $template['company_tel_no'] }}</td>
							</tr>
							<tr>
								<td width="300px" valign="top" align="right">Fax: {{ $template['company_fax_no'] }}</td>
							</tr>
							<tr>
								<td width="300px" valign="top" align="right">Email: <a href="mailto:{{ $handler->email_address or '' }}">{{ $handler->email_address or '' }}</a></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	
	<!-- CLIENT INFORMATION -->
	<tr>
		<td width="600px" valign="top" colspan="3" style="padding:10px 0px 10px 0px;">
			<table width="600px" style="font-size:12px;font-family:arial;">
				<tr>
					<td width="60" valign="top">
						<table width="60" valign="top" style="font-size:12px;font-family:arial;font-weight:bold;">
							<tr>
								<td width="60" valign="top" align="left">Client</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Address</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Attn</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Email</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Tel</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Fax</td>
							</tr>
							<tr>
								<td width="60" valign="top" align="left">Re</td>
							</tr>
						</table>
					</td>
					<td width="340px" valign="top" align="left">
						<table width="340" valign="top" align="left" style="font-size:12px;font-family:arial;">
							<tr>
								<td width="340" valign="top" align="left">{{ $client['name'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $client['address'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $client['attn'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $client['email'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $client['telephone'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $client['fax'] or '' }}</td>
							</tr>
							<tr>
								<td width="340" valign="top" align="left">{{ $matter->Description_A or '' }}</td>
							</tr>
						</table>
					</td>

					<td width="80px" valign="top" align="left">
						<table width="80px" valign="top" align="left" style="font-size:12px;font-family:arial;font-weight:bold;">
							<tr>
								<td width="80px" valign="top" align="left">Date</td>
							</tr>
							<tr>
								<td width="80px" valign="top" align="left">Ref</td>
							</tr>
							<tr>
								<td width="80px" valign="top" align="left">Invoice No.</td>
							</tr>
						</table>
					</td>

					<td width="120px" valign="top" align="left">
						<table width="80px" valign="top" align="left" style="font-size:12px;font-family:arial;text-align:left;">
							<tr>
                                                            <td width="80px" valign="top" align="left">{{ date('j F Y', strtotime($invoicehead->InvoiceDate)) }}</td>
							</tr>
							<tr>
								<td width="80px" valign="top" align="left">{{ $invoicehead->reference_number or '' }}</td>
							</tr>
							<tr>
								<td width="80px" valign="top" align="left">{{ $invoicehead->new_invoice_number or '' }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<!--NOTE -->
	<tr>
		<td width="600px" valign="top" colspan="3">
			<table width="600px" valign="top" style="font-size:12px;font-family:arial;">
				<tr>
					<td width="600px" valign="top">
						{{ Lang::get('invoice.To our professional charges for action on your behalf in connection with that above matter from :from_date to :to_date, particulars of which are as follows:', array('from_date' => date('j F Y', strtotime($min_workdate)), 'to_date' => date('j F Y', strtotime($max_workdate)))) }}
					</td>
				</tr>
			</table>
		</td>
	</tr>


	<!-- TRANSACTION TABLE -->
	<tr>
	<td width="600px" valign="top" colspan="3" style="padding-top:10px;">
		<table width="600px" valign="top" cellpadding="0" cellspacing="0" style="font-size:12px;font-family:arial;text-align:left; border:1px #000 solid;">
			<tr>
				<thead>
					<tr>
						<th width="60px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">Date</th>
						<th width="70px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">Solicitor</th>
						<th width="420px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">Description</th>
						<th width="50px" style="border-bottom:1px #000 solid;padding:2px;">Unit</th>
					</tr>
				</thead>

				<tbody>
                                    @foreach($invoice_items as $item)
					<tr>
						<td width="70px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ date('d/m/Y', strtotime($item->WorkDate)) }}</td>
						<td width="60px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ $item->EmployeeID }}</td>
						<td width="420px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ $item->Description }}</td>
						<td width="50px" style="border-bottom:1px #000 solid;padding:2px;">{{ $item->units }}</td>
					</tr>
                                    @endforeach
				</tbody>

				<tfoot>
					<tr>
						<td width="550px" align="right" valign="top" colspan="3" style="font-weight:bold; border-right: 1px #000 solid;padding:2px;">TOTAL UNITS</td>
						<td width="50px" align="left" valign="top" style="font-weight:bold;padding:2px;">{{ $total_units }}</td>
					</tr>
				</tfoot>
			</tr>
		</table>
	</td>
	</tr>

	<!--Costs -->
	<tr>
		<td width="600px" valign="top" colspan="3" style="font-size:12; font-weight:bold;padding:10px 0px 5px 0px;">Costs</td>
	</tr>
	<tr>
		<td width="600px" valign="top" colspan="3">
			<table width="600px" valign="top" cellpadding="0" cellspacing="0" style="font-size:12px;font-family:arial;text-align:left; border:1px #000 solid;">
			<tr>
				<tbody>
                                    @foreach($costs as $cost)
					<tr>
						<td width="500px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ Lang::get('invoice.Work undertaken by', array('handler_name' => $cost->NickName, 'handler_id' => $cost->EmployeeID, 'hourly_rate' => $cost->HourlyRate)) }}</td>
						<td width="100px" style="border-bottom:1px #000 solid;padding:2px;">${{ number_format($cost->total_amount, 2) }}</td>
					</tr>
                                    @endforeach
				</tbody>

				<tfoot>
					<tr>
						<td width="500px" align="right" valign="top" style="font-weight:bold;border-right: 1px #000 solid;padding:2px;">{{ Lang::get('invoice.TOTAL COSTS') }}</td>
						<td width="100px" valign="top" style="font-weight:bold;padding:2px;">${{ number_format($total_costs, 2) }}</td>
					</tr>
                                        @if( !empty($invoicehead->agreed_cost) )
                                        <tr>
						<td width="500px" align="right" valign="top" style="font-weight:bold;border-right: 1px #000 solid;padding:2px;">{{ Lang::get('invoice.AGREED COSTS') }}</td>
						<td width="100px" valign="top" style="font-weight:bold;padding:2px;">${{ number_format($invoicehead->agreed_cost, 2) }}</td>
					</tr>
                                        @endif
				</tfoot>
				
			</tr>
		</table>
		</td>
	</tr>

	<!--Disbursements -->
        @if(count($disbursements) > 0)
	<tr>
		<td width="600px" valign="top" colspan="3" style="font-size:12; font-weight:bold;padding:10px 0px 5px 0px;">Disbursements</td>
	</tr>
	<tr>
		<td width="600px" valign="top" colspan="3" style="padding-bottom:10px;">
                @if(count($disbursements) == 0)
                    {{ Lang::get('invoice.No disbursements') }}
                @else
                    <table width="600px" valign="top" cellpadding="0" cellspacing="0" style="font-size:12px;font-family:arial;text-align:left; border:1px #000 solid;">
                            <tr>
                                    <tbody>
                                        @foreach($disbursements as $disb)
                                            <tr>
                                                    <td width="500px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ $disb->description }}</td>
                                                    <td width="100px" style="border-bottom:1px #000 solid;padding:2px;">${{ number_format($disb->amount, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                            <tr>
                                                    <td width="500px" align="right" valign="top" style="font-weight:bold;border-right: 1px #000 solid;padding:2px;">{{ Lang::get('invoice.TOTAL DISBURSEMENTS') }}</td>
                                                    <td width="100px" valign="top" style="font-weight:bold;padding:2px;">${{ number_format($total_disbursements, 2) }}</td>
                                            </tr>
                                    </tfoot>

                            </tr>
                    </table>
                @endif
		</td>
	</tr>
        @endif
        
        @if(count($less) > 0)
	<tr>
		<td width="600px" valign="top" colspan="3" style="font-size:12; font-weight:bold;padding:10px 0px 5px 0px;">{{ Lang::get('invoice.Less') }}</td>
	</tr>
	<tr>
		<td width="600px" valign="top" colspan="3" style="padding-bottom:10px;">
                @if(count($less) == 0)
                    {{ Lang::get('invoice.No Less') }}
                @else
                    <table width="600px" valign="top" cellpadding="0" cellspacing="0" style="font-size:12px;font-family:arial;text-align:left; border:1px #000 solid;">
                            <tr>
                                    <tbody>
                                        @foreach($less as $l)
                                            <tr>
                                                    <td width="500px" style="border-right:1px #000 solid;border-bottom:1px #000 solid;padding:2px;">{{ $l->description }}</td>
                                                    <td width="100px" style="border-bottom:1px #000 solid;padding:2px;">${{ number_format($l->amount, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                            <tr>
                                                    <td width="500px" align="right" valign="top" style="font-weight:bold;border-right: 1px #000 solid;padding:2px;">{{ Lang::get('invoice.TOTAL INVOICE') }}</td>
                                                    <td width="100px" valign="top" style="font-weight:bold;padding:2px;">${{ number_format($total_cost_disb, 2) }}</td>
                                            </tr>
                                    </tfoot>

                            </tr>
                    </table>
                @endif
		</td>
	</tr>
        @endif
        
	<!-- TOTAL INVOICE -->

	<tr>
		<td width="600px" valign="top" colspan="3" style="padding-bottom:10px;">
			<table width="600px" valign="top" style="font-size:12px;font-family:arial;">
				<tbody>
					<tr>
						<td width="500px" align="right" valign="top" style="font-weight:bold;padding:2px;">{{ Lang::get('invoice.TOTAL INVOICE') }}</td>
						<td width="100px" align="left" valign="top" style="font-weight:bold;padding:2px;"><span style="text-decoration:underline; border-bottom:1px #000 solid;">${{ number_format($total_cost_disb, 2) }}</span></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>

	<!--COMPLIMENTS -->
	<tr>
		<td width="600px" valign="top" colspan="3" style="padding-bottom:10px;">
			<table width="600px" valign="top" style="font-size:12px;font-family:arial;">
				<tr>
					<td>
						<div style="width:200px; border-top:1px #000 solid; margin-left:10px; text-align:center;margin-top:10px;">WITH COMPLIMENTS</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<!-- CORPORATE INFORMATIONS -->
	<tr>
		<td width="600px" valign="top" colspan="3">
			<table width="600px" valign="top" style="font-size:12px;font-family:arial;">
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">
						{{ Lang::get('invoice.Cheques should be made payable to') }}: <b>{{ $template['cheque_payable_to'] }}</b>
					</td>
				</tr>
				<tr>
					<td width="600px" valign="top">{{ Lang::get('invoice.Telegraphic Transfer') }}</td>
				</tr>
				<tr>
					<td width="600px" valign="top">{{ $template['name_of_banker']['line_1'] }}</td>
				</tr>
				<tr>
					<td width="600px" valign="top">{{ $template['name_of_banker']['line_2'] }}</td>
				</tr>
				<tr>
					<td width="600px" valign="top">{{ $template['name_of_banker']['line_3'] }}</td>
				</tr>
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">{{ $template['name_of_banker']['line_4'] }}</td>
				</tr>

				<tr>
					<td width="600px" valign="top">{{ Lang::get('invoice.Account Name') }}: {{ $template['account_name'] }}</td>
				</tr>

				<tr>
					<td width="600px" valign="top">{{ Lang::get('invoice.Account No.') }}: {{ $template['account_no'] }}</td>
				</tr>
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">{{ Lang::get('invoice.Swift Code') }}: {{ $template['swift_code'] }}</td>
				</tr>
				
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">{{ Lang::get('invoice.Please note that interest will accrue at the rate of :interest_rate per month in the event that this invoice is not settled within :no_of_days days from the date hereof.', array('interest_rate' => '2%', 'no_of_days' => 30)) }}</td>
				</tr>
				
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">{{ Lang::get('invoice.No receipt will be issued unless requested.') }}</td>
				</tr>
				
				<tr>
					<td width="600px" valign="top" style="padding-bottom:10px;">{{ Lang::get('invoice.Bills may be paid by VISA or MASTERCARD in person at our office.') }}</td>
				</tr>
				
				<tr>
					<td width="600px" valign="top">E. & O.E.</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
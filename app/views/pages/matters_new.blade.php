@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/matters.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<div class="container _matters_main_con_"></div>
<input type="hidden" value="{{ $type }}" id="matter_type"/>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/matters_res_page.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/modal_matter_.js"></script>
@stop 
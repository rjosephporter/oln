<div class="row" total-page="{{ $total_page }}">
    <div class="col-xs-12 col-sm-12">
        <ul class="event-list">
            @foreach ($matters as $matter)
            <li>
                <time datetime="2014-07-20">
                    <span class="folder">
                        <!-- <i class="glyphicon glyphicon-folder-close medium-large-glyph {{ $matter->matter_type }}-folder"></i> -->
                        <img src="/assets/thisapp/images/matters/{{ $matter->matter_type }}.png" style="width:45%" />
                    </span>
                    <span class="day">{{ $matter->JobAssignmentID }}</span>
                    <span class="month">{{ $matter->Incharge }}</span>
                </time>
                <div class="info">
                    <h2 class="title">{{ str_limit($matter->Description_A, $limit = 20, $end = '...') }}</h2>
                    <ul class="matter-info">
                        <li data-toggle="tooltip" data-placement="top" title="Billable Amount"><span class="fa fa-bookmark"></span> ${{ number_format($matter->total_billable_amount, 2, '.', ',') }}</li>
                        <li data-toggle="tooltip" data-placement="top" title="Billed Amount"><span class="fa fa-bookmark" style="color:green"></span> ${{ number_format($matter->total_billed_amount, 2, '.', ',') }}</li>
                        <li data-toggle="tooltip" data-placement="top" title="Unbilled Amount"><span class="fa fa-bookmark" style="color:red"></span> ${{ number_format($matter->total_unbilled_amount, 2, '.', ',') }}</li>
                    </ul>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
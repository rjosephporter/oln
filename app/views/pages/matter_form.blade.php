@extends('template/default')
@section('content')
<div class="container">
    <div class='row'>
        <div class="col-md-12">
            <h2><i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;View/Edit Matter</h2>
            <div class="modal-content" id="new_matter_log2">
                <form role="form" method="post">
                    <div class="modal-body">
                        <div class="row" id="new_matter_main_form">
        
                        </div>
                        <div class="col-md-12">
                            <!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="{{ URL::to('/matters') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    
    <div id="item-container-source" style="display:none">
    <div class="col-md-12 item-container">
            <div class="panel panel-default">
                <div class="panel-heading" style="color: {{ $obj['matter'][0]->matter_type == 'blue' ? 'white' : 'black' }}; background-color: {{ $obj['matter'][0]->matter_type == 'blue' ? "#0075b0"  : ($obj['matter'][0]->matter_type == 'green' ? "rgba(0, 128, 0, 0.5)" : ($obj['matter'][0]->matter_type == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($obj['matter'][0]->matter_type == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Matter No: {{$obj['matter'][0]->JobAssignmentID}} - {{$obj['matter'][0]->Description_A}}</h4>
                        </div>
                    </div>
                    <div class='row' style="text-align: center;">
                        <div class="col-md-4" style="text-align: left;">
                            <div class="form-group">
                                Billable Amount: <label >${{number_format($obj['matter'][0]->billable_amount, 2, '.', ',')}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                Billed Amount: <label class="control-label">${{number_format($obj['matter'][0]->billed_amount, 2, '.', ',')}}</label>
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align: right;">
                            <div class="form-group">
                                Unbilled Amount: <label class="control-label">${{number_format($obj['matter'][0]->unbilled, 2, '.', ',')}}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!--<form role="form" method="post">-->
                        <div class='row'>
                            <input class="form-control" type="hidden" name="matid"" placeholder="Matter ID" value="{{{$obj['matter'][0]->JobAssignmentID}}}">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="matdesc">Matter Description</label>
                                    <input class="form-control" id="matdesc" name="matdesc" placeholder="Matter Description" value="{{{$obj['matter'][0]->Description_A}}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="desig_date">Designated Date</label>
                                    <input class="form-control" id="desig_date" name="desig_date" placeholder="Designated Date" value="{{$obj['matter'][0]->DesignatedDate}}">
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="new_customer_id">Customer</label>
                                 <select class="form-control" name="new_customer_id[]" placeholder="Select Customer" required>
                                <option></option>
                                        @foreach($obj['client_list'] as $client)
                                        <option value="{{ $client->CustomerID }}"{{ $obj['matter'][0]->CustomerID  == $client->CustomerID ? 'selected' : '' }} >{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
                                        @endforeach
                                </select>
                            </div>
                        
                            <div class="col-md-8 form-group">
                                 <input type="hidden" class="form-control" id="cond" value="{{$obj['condition']}}">
                            <label for="new_handler_id">Incharge</label>
                            
                            <select class="form-control" name="new_handler_id[]" id="handler" placeholder="Select Incharge" required>
                                    <option></option>
                                    @foreach($obj['handler_list'] as $handler)
                                        <option value="{{ $handler->EmployeeID }}" {{ $obj['matter'][0]->Incharge  == $handler->EmployeeID ? 'selected' : '' }}>{{ $handler->EmployeeID }} </option>
                                    @endforeach
                             </select>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="new_assistant_id">Assistant</label>
                                    <select class="form-control" name="new_assistant_id[]" placeholder="Select Assistant" required>
                                    <option></option>
                                    @foreach($obj['handler_list'] as $handler)
                                        <option value="{{ $handler->EmployeeID }}"{{ $obj['matter'][0]->Incharge  == $handler->EmployeeID ? 'selected' : '' }}>{{ $handler->EmployeeID }}</option>
                                    @endforeach
                                    </select>
                            </div>
                        </div>
                        <!--<button type="submit" class="btn btn-default">Submit</button>-->
                    <!--</form>-->
                </div>
            </div>
        </div>
    </div>
</div>

<!--modals-->
<div class="modal fade" id="complete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">Updating Matter...Please wait <span class="matter_for_completion"></span></h4>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type='text/javascript'>

    $('#new_customer_id').selectize({
        create: true,
        sortField: 'text'
    });

    $('#new_assistant_id').selectize({
        create: true,
        sortField: 'text'
    });
    report_types = [];
    new_matter_log_count = 0;

      /* Init new variables */
    var datepicker_options = {
        maxDate : '0',
        dateFormat: "MM d, yy",
    };

    var item_container = $('#item-container-source').clone().html();

    $('#new_matter_main_form').append(item_container).focus();
    $('#desig_date').datepicker(datepicker_options);

    $(document).ready(function() {
     
     // $('#handler').selectize();
      //var $select = $("#handler").selectize();
      //var selectize = $select[0].selectize;
      
        //selectize.setValue($('#cond').val()); 

    });
      
    /* Save New conference */
    $('#new_matter_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());
        $("#complete_confirmation").modal().show();
       $.post('/matters/updatematter2', $(this).serializeArray(), function(data){
            console.log(data);

            if(data.status == 'success') {
                toastr.success(data.message);
                var delay = 3000; //Your delay in milliseconds
                setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);     
                $("#complete_confirmation").modal().hide();           
            } else {
                toastr.error(data.message);
            }
        });
        

    });  
</script>
@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i>  Update Matter 
			</h2>
			<div class="modal-content" id="new_matter_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_matter_main_form">
		
						</div>
						<div class="col-md-12">
							<!--here-->

							<!--end here--><!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/matters') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="item-container-source" style="display:none">
							<div class="col-md-12 item-container">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
													<label for="new_matter_id">Matter ID</label>
													<select class="form-control" name="new_matter_id[]" placeholder="Select Matter" required>
														<option></option>
														@foreach($matter_list as $matter)
															<option value="{{ $matter->JobAssignmentID }}">{{ $matter->JobAssignmentID }} - {{ $matter->Description_A }}</option>
														@endforeach
													</select>
												</div>	
												<div class="col-md-12 form-group">
													<label for="new_customer_id">Customer</label>
													<select class="form-control" name="new_customer_id[]" placeholder="Select Customer" required>
															<option></option>
															@foreach($client_list as $client)
																<option value="{{ $client->CustomerID }}">{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
															@endforeach
												     </select>
												</div>
																						
													
												<div class="col-md-12 form-group">
													<label for="new_handler_id">Incharge</label>
													<select class="form-control" name="new_handler_id[]" placeholder="Select Incharge" required>
															<option></option>
															@foreach($handler_list as $handler)
																<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
															@endforeach
												     </select>
												</div>
												<div class="col-md-12 form-group">
													<label for="new_assistant_id">Assistant</label>
													<select class="form-control" name="new_assistant_id[]" placeholder="Select Assistant" required>
															<option></option>
															@foreach($handler_list as $handler)
																<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
															@endforeach
												     </select>
												</div>
											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
													<label for="new_designated_date">Date  <small>(click to select date)</small></label>
													<input type="text" class="form-control" name="new_designated_date[]" readonly required>
												</div>

												<div class="col-md-12 form-group">
													<label for="new_matter_description1">Description</label>
													<textarea  class="form-control" name="new_matter_description[]" rows="3" required></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type='text/javascript'>

	$('#new_customer_id').selectize({
	    create: true,
	    sortField: 'text'
	});

	$('#new_handler_id').selectize({
	    create: true,
	    sortField: 'text'
	});

	$('#new_assistant_id').selectize({
	    create: true,
	    sortField: 'text'
	});
	report_types = [];
	new_matter_log_count = 0;

	  /* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };

	var item_container = $('#item-container-source').clone().html();

	/* Add item */
    //$('#btn_add_item2').bind('click touchstart', function(e){
        //e.preventDefault();
        $('#new_matter_main_form').append(item_container).focus();
        $('#new_matter_main_form').find('.item-container').find('select[name="new_matter_id[]"]').selectize();
        //$('#new_matter_main_form').find('.item-container').find('select[name="new_customer_id[]"]').selectize();
        //$('#new_matter_main_form').find('.item-container').find('select[name="new_handler_id[]"]').selectize();
        //$('#new_matter_main_form').find('.item-container').find('select[name="new_billingcode[]"]').selectize();
         $('#new_matter_main_form').find('.item-container').find('input[name="new_designated_date[]"]').datepicker(datepicker_options);
        new_matter_log_count++;
    //});

      $(document).on('change', 'select[name="new_matter_id[]"]', function() {
    
    	var selected_matter = $(this).val();

    	$.get('/matters/matterinfo', { matter_id : selected_matter}, function(data){
            console.log(data);
            console.log( data.DesignatedDate);
            $('select[name="new_matter_id[]"]').parent().parent().find('select[name="new_customer_id[]"]').val(data.CustomerID);
            $('select[name="new_matter_id[]"]').parent().parent().find('select[name="new_handler_id[]"]').val(data.Incharge);
            $('select[name="new_matter_id[]"]').parent().parent().find('select[name="new_assistant_id[]"]').val(data.Assistant);
            $('#new_matter_main_form').find('.item-container').find('input[name="new_designated_date[]"]').val(data.DesignatedDate);
            $('#new_matter_main_form').find('.item-container').find('textarea[name="new_matter_description[]"]').val(data.Description_A);
        });
    	//$(this).parent().parent().find('select[name="new_customer_id[]"]').val(selected_matter);
    });

    $(document).ready(function() {
    	$('#btn_add_item2').click();

    });

     /* Delete item */
    $(document).on('click touchstart', '.delete-item', function(e){
        e.preventDefault();
        if(new_matter_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_matter_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });

 


    /* Save New conference */
    $('#new_matter_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

       $.post('/matters/updatematter', $(this).serializeArray(), function(data){
            console.log(data);
            if(data.status == 'success') {
                toastr.success(data.message);
	            var delay = 3000; //Your delay in milliseconds
	            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
            } else {
                toastr.error(data.message);
            }
        });
    	

    });  

    	
		
</script>
@stop
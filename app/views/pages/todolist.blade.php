@extends('template/default')
@section('content')
<link href="/assets/thisapp/css/todolist.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/assets/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet" type="text/css"/>
<link href="/assets/selectize/css/selectize.css" rel="stylesheet" type="text/css"/>
<link href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css"/>
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<!--<link href="/assets/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet">-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <h3>{{ Lang::get('lang.commcenter.Communication Center') }}</h3>
        </div>
<!--        <div class="col-xs-12">
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="/commcenter/todolist" data-toggle="tab">My To-Do Lists</a></li>
              <li><a href="/commcenter/dropbox" data-toggle="tab">My Dropbox</a></li>
            </ul>
        </div>-->
    </div>
    <div class="panel panel-default top-buffer">
        <div class="panel-heading">
            <span class="h4">{{ Lang::get('lang.commcenter.My todo list') }}</span>&nbsp;&nbsp;
            <button class="btn btn-info" data-toggle="modal" data-target="#taskwizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-edit"></span> {{ Lang::get('lang.commcenter.New To-do') }}</button>
            <div class="btn-group task-controls">
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#delegatewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-arrow-down"></span> {{ Lang::get('lang.commcenter.Delegate') }}</button>
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#replywizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-comment"></span> {{ Lang::get('lang.commcenter.Send Reply') }}</button>
<!--              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#approvewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-thumbs-up"></span> Approve</button>-->
              <a type="button" class="btn btn-default approve"><span class="glyphicon glyphicon-inbox"></span> {{ Lang::get('lang.commcenter.Approve') }}</a>
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#declinewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-remove"></span> {{ Lang::get('lang.commcenter.Decline') }}</button>
              <a type="button" class="btn btn-default accept"><span class="glyphicon glyphicon-inbox"></span> {{ Lang::get('lang.commcenter.Accept') }}</a>
              <button type="button" class="btn btn-default" data-toggle="modal" data-target="#finishwizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-ok"></span> {{ Lang::get('lang.commcenter.Finish w/ details') }}</button>
              <a type="button" class="btn btn-default forcefinish"><span class="glyphicon glyphicon-ok"></span>{{ Lang::get('lang.commcenter.Acknowledge') }}</a>
            </div>
            <div class="btn-group task-request-controls">
                <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-comment"></span>{{ Lang::get('lang.commcenter.Send message') }}</button>
            </div>
        </div>
        <div class="panel-body">
            <div class="todolist">
                <div class="sidebar-nav">
                    <ul class="nav nav-pills nav-stacked">
                      <li id="my-tasks"><a href="#">{{ Lang::get('lang.commcenter.My Tasks') }}</a></li>
                      <li id="my-requests"><a href="#">{{ Lang::get('lang.commcenter.My Requests') }}</a></li>
                      <li id="my-finished-tasks"><a href="#">{{ Lang::get('lang.commcenter.My Finished Tasks') }}</a></li>
                      <li id="my-finished-requests"><a href="#">{{ Lang::get('lang.commcenter.My Finished Requests') }}</a></li>
<!--                      <li id="my-cc-tasks"><a href="#">Tasks where I am in the loop</a></li>-->
                    </ul>
                </div>
                <div class="task-list-bar">
                    <div class="task-sorter">
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-small dropdown-toggle" data-toggle="dropdown">
                                <span class="sort-button">{{ Lang::get('lang.commcenter.Sort by Date') }}</span><span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a id="sort-date-descending" href="#">{{ Lang::get('lang.commcenter.Date (Newest to Oldest)') }}</a></li>
                                <li><a id="sort-date-ascending" href="#"> {{ Lang::get('lang.commcenter.Date (Oldest to Newest)') }}</a></li>
                                <li><a id="sort-from-ascending" href="#"> {{ Lang::get('lang.commcenter.From (A to Z)') }}</a></li>
                                <li><a id="sort-from-descending" href="#"> {{ Lang::get('lang.commcenter.From (Z to A)') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="task-list">
                        <ul class="list-unstyled">
                        </ul>
                    </div>
                </div>
                <div id="task-detail" class="task-background">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="processing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div id="modal-content" class="modal-content">
            <div class="modal-header">
                <h3>{{ Lang::get('lang.commcenter.Processing') }}...</h3>
            </div>
            <div class="modal-body">
                <div class="progress progress-striped active">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="taskwizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form role="form" class="formcreatetask form-horizontal" id="formcreatetask" action="" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.newtodo.Close') }}</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Create Task/Request') }}</h4>
            </div>
            <div class="modal-body">
<!--                <div class="row">
                    <div class="col-xs-6">-->
                        <input type="hidden" name="meetingid" id="meetingid"/>
                        <input type="hidden" name="minuteid" id="minuteid"/>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">1. {{ Lang::get('lang.newtodo.Request or Own task') }}</label>
                            <div class="col-sm-9">
                              <div class="radio-inline">
                                    <label>
                                        <input type="radio" id="task" name="taskorrequest" value="task">{{ Lang::get('lang.newtodo.Own task') }} 
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" id="request" name="taskorrequest" value="request"> {{ Lang::get('lang.newtodo.Request To Someone Else') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="owners" class="col-sm-3 control-label">2. {{ Lang::get('lang.newtodo.Select Persons to Assign Task To') }}</label>
                            <div class="col-sm-9">
                                <select class="form-control" multiple id="owners" name="owners[]" style="width:100%" placeholder="Select persons to assign">
                                    <option></option
                                    <?php foreach($users as $pmsuser): ?>
                                        <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                                    <?php endforeach ?>
                                </select>
                                <button id="select-all-owners" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Select All') }}</button>
                                <button id="clear-all-owners" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Clear') }}</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-3 control-label">3. {{ Lang::get('lang.newtodo.Select type of Task') }}</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="type" style="width:100%">
                                  <?php foreach($types as $key => $value): ?>
                                    <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                  <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="subject" class="col-sm-3 control-label">4. {{ Lang::get('lang.newtodo.Subject') }}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="subject" placeholder="Subject">
                            </div>
                        </div>
<!--                    </div>
                    <div class="col-xs-6">-->
                        <div class="form-group">
                            <label for="message" class="col-sm-3 control-label">5. {{ Lang::get('lang.newtodo.Message') }}</label>
                            <div class="col-sm-9">
                                <textarea rows="4" class="form-control" name="message" placeholder="Enter your message"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="requiredbydate" class="col-sm-3 control-label">6. {{ Lang::get('lang.newtodo.Required Date') }}</label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" id="requiredbydate" name="requiredbydate" placeholder="TBA"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sharees" class="col-sm-3 control-label">7. {{ Lang::get('lang.newtodo.Share Response With') }}</label>
                            <div class="col-sm-9">
                                <select class="form-control" multiple id="sharees" name="sharees[]" style="width:100%" placeholder="Select sharees">
                                  <option></option>
                                  <?php foreach($users as $pmsuser): ?>
                                    <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                                  <?php endforeach ?>
                                </select>
                                <button id="select-all-sharees" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Select All') }}</button>
                                <button id="clear-all-sharees" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Clear') }}</button>
                            </div>
                        </div>
<!--                        <div class="form-group">
                            <label for="fileupload" class="col-sm-3 control-label">8. Add Files</label>
                            <div class="col-sm-9">
                                <input type="file" id="fileupload" name="fileupload[]" multiple="true">
                            </div>
                        </div>-->
                    </div>
<!--                </div>
            </div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
            </div>
            </form>
        </div>
  </div>
</div>
<div class="modal fade" id="delegatewizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.newtodo.Close') }}</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Delegate Task') }}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="formdelegatetask form-horizontal" id="formdelegatetask" action="/commcenter/delegatetask" method="post">
                <input id="taskid" type="hidden" name="taskid">
                <div class="form-group">
                    <label for="owner" class="col-sm-3 control-label">{{ Lang::get('lang.newtodo.Delegate Task To') }}</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="owner" name="owner" style="width:100%" placeholder="Select person to delegate to">
                          <option></option>
                          <?php foreach($users as $pmsuser): ?>
                            <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                          <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="replywizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.Close') }}</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Reply to task') }}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="formcreatemessage form-horizontal" id="formcreatemessage" action="/commcenter/newtaskreply" method="post" enctype="multipart/form-data">
                <input id="taskid" type="hidden" name="taskid">
                <div class="form-group">
                    <label for="subject" class="col-sm-3 control-label">1. {{ Lang::get('lang.newtodo.Subject') }}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="subject" placeholder="Subject" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">2. {{ Lang::get('lang.newtodo.Message') }}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" id="message" name="message" placeholder="Enter your message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sharees" class="col-sm-3 control-label">3. {{ Lang::get('lang.newtodo.Share Response With') }}</label>
                    <div class="col-sm-9">
                        <select class="form-control" multiple id="sharees" name="sharees[]" style="width:100%">
                            <option></option>
                            <?php foreach($users as $pmsuser): ?>
                                <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                            <?php endforeach ?>
                        </select>
                        <button id="clear-all-sharees" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Clear') }}</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="messagewizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.newtodo.Close') }}</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Reply to task') }}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="formnewmessage form-horizontal" id="formnewmessage" action="/commcenter/newtaskmessage" method="post" enctype="multipart/form-data">
                <input id="requestid" type="hidden" name="requestid">
                <div class="form-group">
                    <label for="subject" class="col-sm-3 control-label">1. {{ Lang::get('lang.newtodo.Subject') }}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="subject" placeholder="Subject" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">2. {{ Lang::get('lang.newtodo.Message') }}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" name="message" placeholder="Enter your message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="declinewizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.newtodo.Close') }}</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Decline task') }}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="formdeclinetask form-horizontal" id="formdeclinetask" action="/commcenter/declinetask" method="post">
                <input id="taskid" type="hidden" name="taskid">
                <input type="hidden" name="subject" value="Task Declined">
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">1. {{ Lang::get('lang.newtodo.Reason for refusal') }}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" id="message" name="message" placeholder="Enter your message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sharees" class="col-sm-3 control-label">2. {{ Lang::get('lang.newtodo.Share Response With') }}</label>
                    <div class="col-sm-9">
                        <select class="form-control" multiple id="sharees" name="sharees[]" style="width:100%">
                            <option></option>
                            <?php foreach($users as $pmsuser): ?>
                                <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                            <?php endforeach ?>
                        </select>
                        <button id="clear-all-sharees" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Clear') }}</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="finishwizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close cancel" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.newtodo.Close') }}</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.newtodo.Finish task') }}</h4>
        </div>
        <div class="modal-body">
            <form role="form" class="formcreatereply form-horizontal" id="formcreatereply" action="/commcenter/newtaskresponse" method="post" enctype="multipart/form-data">
                <input id="taskid" type="hidden" name="taskid">
                <div class="form-group">
                    <label for="subject" class="col-sm-3 control-label">1. {{ Lang::get('lang.newtodo.Subject') }}</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="subject" placeholder="Subject" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label for="message" class="col-sm-3 control-label">2. {{ Lang::get('lang.newtodo.Finish Message') }}</label>
                    <div class="col-sm-9">
                        <textarea rows="4" class="form-control" id="message" name="message" placeholder="Enter your message"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="sharees" class="col-sm-3 control-label">3. {{ Lang::get('lang.newtodo.Share Response With') }}</label>
                    <div class="col-sm-9">
                        <select class="form-control" multiple id="sharees" name="sharees[]" style="width:100%">
                            <option></option>
                            <?php foreach($users as $pmsuser): ?>
                                <option value="<?php echo $pmsuser->user_id?>" data-data="<?php echo htmlentities(json_encode(array("uid"=>$pmsuser->user_id,"thumbnail"=>'/assets/thisapp/images/staff/'.$pmsuser->Photo,'designation'=>''))) ?>"><?php echo $pmsuser->EmployeeID.': '.$pmsuser->NickName ?></option>
                            <?php endforeach ?>
                        </select>
                        <button id="clear-all-sharees" type="button" class="btn btn-default btn-sm">{{ Lang::get('lang.newtodo.Clear') }}</button>
                    </div>
                </div>
<!--                <div class="form-group">
                    <label for="fileupload" class="col-sm-3 control-label">4. Add Files</label>
                    <div class="col-sm-9">
                        <input type="file" id="fileupload" name="fileupload[]" multiple="true">
                    </div>
                </div>-->
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" class="btn btn-default cancel" data-dismiss="modal">{{ Lang::get('lang.newtodo.Cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ Lang::get('lang.newtodo.Submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>

<script type="text/x-template" id="default-task">
    <div style="height:95% !important">
        <img class="requested-by-photo img-thumbnail" style="width:50px"/>&nbsp;<span class="task-requested-by"></span><span class="task-date"></span><br>
        To: <span class="task-to"></span><br>
        Subject: <span class="task-subject"></span><span class="status glyphicon pull-right"></span><br>
        Required on: <span class="task-requiredby-date"></span><br>
        <hr>
        <div class="btn-group task-controls pull-right">
          <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#delegatewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-arrow-down"></span> {{ Lang::get('lang.newtodo.Delegate') }}</button>
          <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#replywizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-comment"></span> {{ Lang::get('lang.newtodo.Send Reply') }}</button>
<!--          <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#approvewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-thumbs-up"></span> {{ Lang::get('lang.newtodo.Approve') }}</button>-->
          <a type="button" class="btn btn-xs btn-default approve"><span class="glyphicon glyphicon-inbox"></span> {{ Lang::get('lang.newtodo.Approve') }}</a>
          <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#declinewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-remove"></span> {{ Lang::get('lang.newtodo.Decline') }}</button>
          <a type="button" class="btn btn-xs btn-default accept"><span class="glyphicon glyphicon-inbox"></span> {{ Lang::get('lang.newtodo.Accept') }}</a>
          <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#finishwizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-ok"></span> {{ Lang::get('lang.newtodo.Finish w/ details') }}</button>
          <a type="button" class="btn btn-xs btn-default forcefinish"><span class="glyphicon glyphicon-ok"></span> {{ Lang::get('lang.newtodo.Acknowledge') }}</a>
        </div>
        <div class="btn-group task-request-controls pull-right">
            <button type="button" class="btn btn-xs btn-default" data-toggle="modal" data-target="#messagewizard" data-keyboard="false" data-backdrop="static"><span class="glyphicon glyphicon-comment"></span> {{ Lang::get('lang.newtodo.Send message') }}</button>
<!--            <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-envelope"></span> {{ Lang::get('lang.newtodo.Send reminder') }}</button>-->
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="row">
            <div class="col-xs-12">
                <div class="task-message"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="task-reply-final"></div>
           </div>
        </div>
<!--        <div class="task-replies"></div>   -->
        <div class="timeline-contaner">
            <ul class="timeline task-replies">
            </ul>
        </div>
    </div>
</script>
<script type="text/x-template" id="default-list-item">
    <li>
        <div class="task" tabindex="-1">
            <span class="status glyphicon"></span>&nbsp;&nbsp;&nbsp;<span class="task-requested-by"></span><span class="task-date"></span><br>
            <span class="task-subject"></span><br>
            <div class="task-message"></div>
        </div>
        <hr>
    </li>
</script>
<!--<script type="text/x-template" id="default-task-reply">
        <div class="task-reply">
            <div class="reply-header"></div>
            <div class="reply-message"></div>
        </div>
</script>-->
<script type="text/x-template" id="default-task-reply">
        <li class="task-reply">
            <div class="timeline-badge success"><i class="glyphicon glyphicon-check"></i></div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <div class="reply-header"></div>
                </div>
                <div class="reply-message timeline-body"></div>
            </div>
        </li>
</script>
<script>
    var userids = new Array();
    <?php
    foreach($users as $var):
        echo 'userids.push("'.$var->EmployeeID.'");';
    endforeach;
    ?>

    var userdata = <?php echo json_encode($userdata) ?>;
    var users = <?php echo json_encode($users) ?>;
    var thisuser = "<?php echo $thisuser->user_id ?>";

    $(document).ready(function() {
        <?php if(Session::get('growl_message')){ ?>
            <?php $growl_message = Session::get('growl_message'); ?>
            toastr.<?php echo $growl_message['message_type'] ?>("<?php echo $growl_message['message'] ?>");
        <?php } ?>
    });
</script>
<script src="/assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/assets/thisapp/js/todolist.js"></script>
<script src="/assets/Truncate/truncate.js"></script>
<script src="/assets/TinySort/jquery.tinysort.min.js"></script>
<script src="/assets/Sieve/jquery.sieve.min.js"></script>
<script src="/assets/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<!--<script src="/assets/bootstrap-fileinput/js/fileinput.min.js"></script>-->
<script src="/assets/selectize/js/standalone/selectize.js"></script>
@stop

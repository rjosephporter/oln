@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/jquery-ui-1.10.4/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-time"></i> Reservation 
			</h2>
			<div class="modal-content" id="new_conference_room_reservation_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_conference_room_reservation_main_form">
						<!--
							<div class="col-md-6 row">
						<div class="col-md-12 form-group">
								<label for="reservedby">Reserved By</label>
								<select class="form-control" name="reservedby[]" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
									@endforeach
								</select>
						</div>
						<div class="col-md-12 form-group">
							<label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_reservation_date[]" readonly required>
						</div>
						<div class="col-md-6 form-group">
							<label for="new_start_time">Start Time  <small>(click to select time)</small></label>
							<input type="text" class="form-control" name="new_start_time[]" readonly required>
						</div>
						<div class="col-md-6 form-group">
							<label for="new_end_time">End Time  <small>(click to select time)</small></label>
							<input type="text" class="form-control" name="new_end_time[]" readonly required>
						</div>
						</div>
						<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_purpose">Purpose</label>
							<textarea class="form-control" name="new_purpose[]" rows="3" required></textarea>
							
						</div>
						<div class="col-md-12 form-group">
							<label for="new_notes">Notes</label>
							<textarea class="form-control" name="new_notes[]" rows="3" required></textarea>
							
						</div>
						</div>
							-->		
							
						</div>
						<div class="col-md-12">
							<button class="btn btn-info btn-sm" id="btn_add_item2_reservation">Add Item</button>
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/conference-room') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary_reservation">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="item-container-source" style="display:none">
	<div class="col-md-12 item-container">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<button type="button" class="close pull-right delete-item_reservation"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
								<input type="hidden" class="form-control" name="new_conference_id" value="{{ $conference_id }}">
								<label for="reservedby">Reserved By</label>
								<select class="form-control" name="reservedby[]" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
									@endforeach
								</select>
						</div>
						<div class="col-md-12 form-group">
							<label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_reservation_date[]" readonly required>
						</div>
						<div class="col-md-6 form-group">
							<label for="new_start_time">Start Time  <small>(click to select time)</small></label>
							<input type="text" class="form-control" name="new_start_time[]" readonly required>
						</div>
						<div class="col-md-6 form-group">
							<label for="new_end_time">End Time  <small>(click to select time)</small></label>
							<input type="text" class="form-control" name="new_end_time[]" readonly required>
						</div>
					</div>
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_purpose">Purpose</label>
							<textarea class="form-control" name="new_purpose[]" rows="3" required></textarea>
							<!-- <p class="help-block">Example block-level help text here.</p> -->
						</div>
						<div class="col-md-12 form-group">
							<label for="new_notes">Notes</label>
							<textarea class="form-control" name="new_notes[]" rows="3" required></textarea>
							<!-- <p class="help-block">Example block-level help text here.</p> -->
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/conference_room.js"></script>
<script type="text/javascript" src="/assets/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js"></script>
<script>
/* Overwritten variables from conference_room.js */

	$('#reservedby').selectize({
	    create: true,
	    sortField: 'text'
	});

	report_types = [];
	new_reservation_log_count = 0;

	/* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };

	var item_container = $('#item-container-source').clone().html();

	/* Add item */
    $('#btn_add_item2_reservation').bind('click touchstart', function(e){
        e.preventDefault();
        $('#new_conference_room_reservation_main_form').append(item_container).focus();
        $('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_reservation_date[]"]').datepicker(datepicker_options);

        $('#new_conference_room_reservation_main_form').find('.item-container').find('select[name="reservedby[]"]').selectize();
        /*check time range */

    var startDateTextBox=$('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_start_time[]"]');
    var endDateTextBox=$('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_end_time[]"]');
	startDateTextBox.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );

		}
	});
	endDateTextBox.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	}); 
        
        new_reservation_log_count++;
    });

    $(document).ready(function() {
    	$('#btn_add_item2_reservation').click();
    });

     /* Delete item */
    $(document).on('click touchstart', '.delete-item_reservation', function(e){
        e.preventDefault();
        if(new_reservation_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_reservation_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });


    var tempArray;
    /* Save reservation */
    $('#new_conference_room_reservation_log2 form').submit(function(e){
        e.preventDefault();
        tempArray=$(this).serializeArray();

        $.post('/conference-room-reservation/submitnewreservation', tempArray, function(data){
            
            var myStringArray = data.val;
			var arrayLength = myStringArray.length;
			console.log(myStringArray);
			var events = [];
			for (var i = 0; i < arrayLength; i++) {

            	var str1 = data.date;
            	var space=' ';
			    var str2 = space.concat(data.val[i]['start_time'].substring(0,5));
			    var str3 = space.concat(data.val[i]['end_time'].substring(0,5));
			    
				events.push({id: i, start: getDateObj(str1.concat(str2)), end: getDateObj(str1.concat(str3))});
            }
            
            var overlap=getOverlappingEvents(events).join('\n');
            //console.log(overlap);
            if(overlap.length>0){
            	 toastr.error('Scheduled time is overlapping with another reservation');
            }else{

            	$.post('/conference-room-reservation/savenewreservation', tempArray, function(data){
            			
					            if(data.status == 'success') {
					                toastr.success(data.msg);
						            var delay = 3000; //Your delay in milliseconds
						            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
					            } else {
					                toastr.error(data.msg);
					            }
            	});
            	
            }

        });
    	

    });   

    function getDateObj(s) {
		  var bits = s.split(/[- :]/);
		  var date = new Date(bits[0], bits[1] - 1, bits[2]);
		  date.setHours(bits[3], bits[4], 0);
		  return date;
		}

		
		function getOverlappingEvents(eventArray) {
		  var result = [];
		  var a, b;

		  // Sort the event array on start time
		  eventArray.sort(function(a, b) {
		      return a.start - b.start;
		    });

		  // Get overlapping events
		  for (var i=0, iLen=eventArray.length - 1; i<iLen; i++) {
		    a = eventArray[i];
		    b = eventArray[i + 1];

		    if((a.start <= b.start && b.end <=a.end) || (a.start <=b.start && b.start <=a.end) 
		    	|| (a.start <=b.end && b.end<=a.end)){
		    //if ((a.start <= b.start && a.end > b.start) ||
		        //(a.start < b.end && a.end >= b.end) ) {
		       result.push([a.id, b.id]);
		    }
		  }
		  return result;
		}

		
</script>
@stop
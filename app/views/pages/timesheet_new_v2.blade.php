@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
<style>
.top-bottom-margin {
	margin-top: 15px;
	margin-bottom: 15px;
}

.double-underline {
    text-decoration:underline;
    border-bottom: 1px solid #000;
}

.signature {
	float: left; 
	margin: 20px 10px;
	border-top: 1px solid #000;
	width: 200px; 
	text-align: center;
}

.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;
}

.left-container {
	/*background-color: rgba(255, 231, 0, 0.5);*/
	background-color: rgba(0, 128, 0, 0.5);
}

.table-left-content-bg {
	/*background-color: #fff;*/
	background-color: rgba(0, 128, 0, 0.5);
}

.fixed {
	position: fixed;
	top: 0;
	width: 100%;
}

.menu {
	width: 100%;
	z-index: 1000;
}

.navbar {
	margin-bottom: 0px;
}

.form-control:focus {
	border-color: rgba(0, 128, 0, 0.5);
}

.selectize-control {
	height: 0px !important;
}
</style>
<div class="container-fluid">
	<form method="post" id="timesheet-form" data-redirect-url="{{ URL::to('invoice') }}">
		<input type="hidden" name="form_type" value="{{ $form_type }}">
		<div class="menu">
			<div class="row hidden-print navbar navbar-inverse" style="margin-top:0px; color:#fff;">
				<div class="client-header">
					<div class="col-md-8 col-sm-8 col-sx-8">
						<!-- <label>Client: {{ $client['name'] or '' }}</label> -->
						<label>Timesheet for Matter ID: {{ $matter['id'] or '' }} </label>
					</div>
					<div class="col-md-4 col-sm-4 col-sx-4">
						<label>Total Amount: $<span class="text-overall-total">{{ number_format($total['overall'],2) }}</span></label>
						<div class="btn-group pull-right">
							<button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save Timesheet</button>
							<a href="{{ URL::to('invoice/create-new?matter_id='.$matter['id']) }}" class="btn btn-warning"><i class="fa fa-list"></i> Create Invoice</a>
							<a href="{{ URL::to('timesheet') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="{{ URL::to('/') }}/assets/thisapp/images/oln_invoice_logo.png" style="width:400px" />
					</div>
				</div>
			
				<div class="row top-buffer">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<address>
							{{ $template['company_address_1'] }}<br>
							{{ $template['company_address_2'] }}<br>
							{{ $template['company_address_3'] }}<br>
						</address>			
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<address class='text-right'>
							{{ Lang::get('invoice.Tel') }}: {{ $template['company_tel_no'] }}<br>
							{{ Lang::get('invoice.Fax') }}: {{ $template['company_fax_no'] }}<br>
							{{ Lang::get('invoice.Email') }}:&nbsp;
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="handler_email" value="{{ $handler_email or '' }}" readonly>
						</address>			
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-horizontal" role="form">				
							@if(trim($client['id']) !== '')
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Client') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[name]" value="{{ $client['name'] or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Address') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[address]" value="{{ $client['address'] or '' }}" required>
								</div>
							</div>
							@if($client['type'] == '2')
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Attn') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[attn]" value="{{ $client['attn'] or '' }}" required>
								</div>
							</div>
							@endif
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Email') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[email]" value="{{ $client['email'] or '' }}" required>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Tel') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[telephone]" value="{{ $client['telephone'] or '' }}" required>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Fax') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[fax]" value="{{ $client['fax'] or '' }}" required>
								</div>
							</div>
							@endif							
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Re') }}</label>
								<div class="col-sm-10 col-xs-10">
									<textarea class="form-control input-sm" name="matter[description]" required>{{ $matter['description'] or '' }}</textarea>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<div class="col-sm-12 col-xs-12">
									<button class="btn btn-sm btn-info btn-add-timesheet" disabled><i class="fa fa-plus"></i> Add Timeshet Item</button>
								</div>
							</div>											
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						{{--
						<div class="form-horizontal" role="form">
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('invoice.Created By') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="created_by" value="{{ $created_by }}" readonly>
								</div>
							</div>				
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('invoice.Date') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="date" value="{{ $date }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('invoice.Ref') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="reference_number" value="{{ $reference_number or '' }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('invoice.Invoice No.') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="new_id" value="{{ $new_id or '' }}">
								</div>
							</div>						
						</div>	
						--}}
					</div>
				</div>

				{{--
				<div class="row top-bottom-margin">
					<div class="col-md-12">
						<p>
							To our professional charges for action on your behalf in connection with that above matter from 
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[min_date]" value="{{ date('j F Y', strtotime($timesheet['min_date'])) }}" readonly> to 
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[max_date]" value="{{ date('j F Y', strtotime($timesheet['max_date'])) }}" readonly>, 
							particulars of which are as follows:
						</p>
					</div>
				</div>
				--}}				

			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				<div class="row" style="visibility:hidden">
					<div class="col-md-12 text-center">
						<img src="http://192.168.1.199:8083/assets/thisapp/images/oln_invoice_logo.png" style="width:400px">
					</div>
				</div>
				<div class="row>">
					<div class="col-md-12">
						<div class="form-horizontal" role="form">
							{{--	
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('invoice.Unique ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="id" value="{{ $id or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('invoice.Batch Number') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="batch_number" value="{{ $batch_number or '' }}" readonly>
								</div>
							</div>
							--}}
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label">{{ Lang::get('invoice.Matter ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="matter[id]" value="{{ $matter['id'] or '' }}" readonly>
								</div>
							</div>				
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label">{{ Lang::get('invoice.Client ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="client[id]" value="{{ $client['id'] or '' }}" readonly>
								</div>
							</div>
							{{--
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('invoice.Type') }}</label>
								<div class="col-sm-9 col-xs-9">
									{{ Form::select('type', array(1 => 'Intermediary Invoice', 2 => 'Final Invoice'), 2, array('class' => 'form-control input-sm')) }}
								</div>
							</div>							
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('invoice.Approver') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="approver" value="{{ $approver or '' }}">
								</div>
							</div>
							--}}
							<div class="form-group margin-bottom-5">
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									<a href="{{ URL::to('timesheet/disbursement-ledger?matter_id='.$matter['id']) }}" class="btn btn-block btn-sm btn-info" target="_blank">Disbursement Ledger</a>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<div class="col-sm-3 col-xs-3"></div>
								<div class="col-sm-9 col-xs-9">
									@if(!Input::has('show_all'))
									<a href="{{ URL::to('timesheet/new?matter_id='.$matter['id'].'&show_all=true') }}" class="btn btn-block btn-sm btn-info">Show All (including billed items)</a>
									@else
									<a href="{{ URL::to('timesheet/new?matter_id='.$matter['id']) }}" class="btn btn-block btn-sm btn-info">Hide Billed Items</a>
									@endif
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- BEGIN HIDDEN SOURCE ROWS -->
		<table class="hidden">
			<tr class="added-item" data-type="left-table">
				<input type="hidden" class="form-control input-sm" name="timesheet[list][XXX][UniqueID]" value="">
				<input type="hidden" class="form-control input-sm" name="timesheet[list][XXX][billed]" value="0">
				<input type="hidden" class="form-control input-sm input-changed" name="timesheet[list][XXX][changed]" value="1">
				<td><input type="text" class="form-control input-sm timesheet-date" name="timesheet[list][XXX][WorkDate]" value="{{ date('d-M-Y') }}"></td>
				<td>
					<select class="form-control input-sm drpdwn-handler" name="timesheet[list][XXX][EmployeeID]">
						<option></option>
						@foreach($handler_list as $hl)
							<option value="{{ $hl->EmployeeID }}" {{ ($hl->EmployeeID == User::getEmployeeID(Session::get('principal_user'))) ? 'selected' : '' }}>{{ $hl->EmployeeID }}</option>
						@endforeach
					</select>
				</td>
				<td><input type="text" class="form-control input-sm input-comment" name="timesheet[list][XXX][Comment]" value=""></td>
				<td>
					<input type="text" class="form-control input-sm input-unit" data-billed="0" name="timesheet[list][XXX][units]" value="">
				</td>
			</tr>

			<tr class="added-item" data-type="right-table">
				<td class="text-center">
					<div class="btn-group" role="group">
						<button class="btn btn-danger btn-xs btn-remove-timesheet-item" data-timesheet-id="" title="Remove timesheet item"><i class="fa fa-times"></i></button>
						<button class="btn btn-warning btn-xs btn-move-timesheet-item" data-timesheet-id="" title="Move timesheet to another matter"><i class="fa fa-arrow-circle-right"></i></button>
					</div>
				</td>				
				<td>
					<select class="form-control input-sm hourly-rate-source-list hidden">
						<option></option>
						@foreach($hourly_rate_list as $hourly_rate)
							<option value="{{ $hourly_rate->EmployeeID }}">{{ $hourly_rate->HourlyRate }}</option>
						@endforeach
					</select>
					<input type="text" class="form-control input-sm input-hourly-rate-round" value="" disabled>
					<input type="hidden" class="form-control input-sm input-hourly-rate" name="timesheet[list][XXX][HourlyRate]" value="" readonly>
				</td>
				<td>
					<input type="text" class="form-control input-sm input-work-hours-round" value="" disabled>
					<input type="hidden" class="form-control input-sm input-work-hours" name="timesheet[list][XXX][WorkHour]" value="" readonly>
				</td>
				<td>
					<input type="text" class="form-control input-sm input-timesheet-amount-round" value="" disabled>
					<input type="hidden" class="form-control input-sm input-timesheet-amount" data-billed="0" name="timesheet[list][XXX][amount]" value="" readonly>
				</td>			
			</tr>
		</table>
		<!-- END HIDDEN SOURCE ROWS -->

		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<table class="table table-bordered table-condensed" id="timesheet-left-table">
					<col width="10%">
					<col width="10%">
					<col width="70%">
					<col width="10%">
					<thead>
						<tr>
							<th>{{ Lang::get('invoice.Date') }}</th>
							<th>{{ Lang::get('invoice.Solicitor') }}</th>
							<th>{{ Lang::get('invoice.Description') }}</th>
							<th>{{ Lang::get('invoice.Unit') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($timesheet['list'] as $ndx => $item)
						<tr class="initial-item {{ ($item->billed == 1) ? 'red-background' : '' }}">
							<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][UniqueID]" value="{{ $item->UniqueID }}">
							<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][billed]" value="{{ $item->billed }}">
							<input type="hidden" class="form-control input-sm input-changed" name="timesheet[list][{{ $ndx }}][changed]" value="0">
							<td><input type="text" class="form-control input-sm timesheet-date" data-type="initial" name="timesheet[list][{{ $ndx }}][WorkDate]" value="{{ date('d-M-Y', strtotime($item->WorkDate)) }}" {{ ($item->billed == 1) ? 'readonly' : '' }}></td>
							<td>
								@if($item->billed == 1)
								<input type="text" class="form-control input-sm" data-type="initial" name="timesheet[list][{{ $ndx }}][EmployeeID]" value="{{ $item->EmployeeID }}" readonly>
								@else
								<select class="form-control input-sm drpdwn-handler" data-type="initial" name="timesheet[list][{{ $ndx }}][EmployeeID]">
									<option></option>
									@foreach($handler_list as $hl)
										<option value="{{ $hl->EmployeeID }}" {{ ($hl->EmployeeID == $item->EmployeeID) ? 'selected' : '' }}>{{ $hl->EmployeeID }}</option>
									@endforeach
								</select>								
								@endif
							</td>
							<td>
								<input type="text" class="form-control input-sm input-comment" name="timesheet[list][{{ $ndx }}][Comment]" value="{{ $item->Comment }}" {{ ($item->billed == 1) ? 'readonly' : '' }}>
							</td>
							<td>
								@if($item->billed == 1)
								<input type="text" class="form-control input-sm input-unit-round" value="{{ round($item->units,2) }}" disabled>
								<input type="hidden" class="form-control input-sm input-unit" data-billed="1" name="timesheet[list][{{ $ndx }}][units]" value="{{ $item->units }}" readonly>
								@else
								<input type="text" class="form-control input-sm input-unit" data-billed="0" name="timesheet[list][{{ $ndx }}][units]" value="{{ $item->units }}">
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" class="text-right"><strong>{{ Lang::get('invoice.TOTAL UNITS') }}</strong></td>
							<td>
								<input type="text" class="form-control input-sm input-timesheet-total-units-round" value="0" disabled>
								<input type="hidden" class="form-control input-sm input-timesheet-total-units" name="total[cost][units]" value="0" readonly>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				<table class="table table-bordered table-condensed table-left-content-bg" id="timesheet-right-table">
					<col width="5%">
					<col width="30%">
					<col width="30%">
					<col width="35%">
					<thead>
						<tr>
							<th>Action</th>
							<th>Hourly Rate</th>
							<th>Work Hours</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						@foreach($timesheet['list'] as $ndx => $item)
						<tr class="initial-item">
							<td class="text-center">
								@if($item->billed !== 1)
								<div class="btn-group" role="group">
									<button class="btn btn-danger btn-xs btn-remove-timesheet-item" data-timesheet-id="{{ $item->UniqueID }}" title="Remove timesheet item" {{ ($item->billed == 1) ? 'disabled' : '' }}><i class="fa fa-times"></i></button>
									<button class="btn btn-warning btn-xs btn-move-timesheet-item" data-timesheet-id="{{ $item->UniqueID }}" title="Move timesheet to another matter" {{ ($item->billed == 1) ? 'disabled' : '' }}><i class="fa fa-arrow-circle-right"></i></button>
								</div>
								@endif
							</td>
							<td>
								@if($item->billed == 1)
								<input type="text" class="form-control input-sm" value="{{ number_format($item->HourlyRate,2) }}" disabled>
								<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][HourlyRate]" value="{{ $item->HourlyRate }}" readonly>
								@else
								<select class="form-control input-sm hourly-rate-source-list hidden">
									<option></option>
									@foreach($hourly_rate_list as $hourly_rate)
										<option value="{{ $hourly_rate->EmployeeID }}">{{ $hourly_rate->HourlyRate }}</option>
									@endforeach
								</select>
								<input type="text" class="form-control input-sm input-hourly-rate-round" value="{{ number_format($item->HourlyRate,2) }}" disabled>
								<input type="hidden" class="form-control input-sm input-hourly-rate" name="timesheet[list][{{ $ndx }}][HourlyRate]" value="{{ $item->HourlyRate }}" readonly>
								@endif
							</td>
							<td>
								<input type="text" class="form-control input-sm input-work-hours-round" value="{{ round($item->WorkHour,2) }}" disabled>
								<input type="hidden" class="form-control input-sm input-work-hours" name="timesheet[list][{{ $ndx }}][WorkHour]" value="{{ $item->WorkHour }}" readonly>
							</td>
							<td>
								<input type="text" class="form-control input-sm input-timesheet-amount-round" value="{{ number_format($item->amount,2) }}" disabled>
								<input type="hidden" class="form-control input-sm input-timesheet-amount" data-billed="{{ $item->billed }}" name="timesheet[list][{{ $ndx }}][amount]" value="{{ $item->amount }}" readonly>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>

		{{--
		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('invoice.Costs') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed">
							<col width="90%">
							<col width="10%">				
							<tbody>
								@foreach($timesheet['cost_summary'] as $ndx => $cost)
								<tr>
									<td>Work undertaken by 
										<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[cost_summary][{{ $ndx }}][EmployeeID]" value="{{ $cost->EmployeeID }}" readonly> @ 
										<input type="text" class="form-control input-sm" style="display:inline; width:165px" value="{{ number_format($cost->HourlyRate,2) }}" disabled> 
										<input type="hidden" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[cost_summary][{{ $ndx }}][HourlyRate]" value="{{ $cost->HourlyRate }}" readonly>
										per hour
									</td>
									<td>
										<input type="text" class="form-control input-sm input-handler-cost-round" data-handler="{{ $cost->EmployeeID }}" value="{{ number_format($cost->sum_amount,2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-handler-cost" data-handler="{{ $cost->EmployeeID }}" name="timesheet[cost_summary][{{ $ndx }}][sum_amount]" value="{{ $cost->sum_amount }}" readonly>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL COSTS') }}</strong></td>
									<td><strong>
										<input type="text" class="form-control input-sm input-total-costs-round" value="{{ number_format($total['cost']['amount'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-total-costs" name="total[cost][amount]" value="{{ $total['cost']['amount'] }}" readonly>
									</strong></td>
								</tr>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('invoice.AGREED COSTS') }}</strong></td>
									<td><strong><input type="text" class="form-control input-sm input-agreed-cost" name="timesheet[agreed_cost]" value="{{ $timesheet['agreed_cost'] }}"></strong></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('invoice.Disbursements') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed table-disbursement">
							<col width="5%">
							<col width="85%">
							<col width="10%">				
							<tbody class="items">
								@if(count($disbursements) == 0)
									<tr>
										<input type="hidden" class="form-control input-sm" name="disbursements[0][invoice_disbursement_id]" value="" placeholder="ID">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="disbursement"></i></td>
										<td><input type="text" class="form-control input-sm" name="disbursements[0][description]" value="" placeholder="Description"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="disbursement" name="disbursements[0][amount]" value="" placeholder="Amount"></td>
									</tr>						
								@else
									@foreach($disbursements as $ndx => $disb)
									<tr>
										<input type="hidden" class="form-control input-sm" name="disbursements[{{ $ndx }}][invoice_disbursement_id]" value="{{ $disb->invoice_disbursement_id }}">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="disbursement"></i></td>
										<td><input type="text" class="form-control input-sm" name="disbursements[{{ $ndx }}][description]" value="{{ $disb->description }}"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="disbursement" name="disbursements[{{ $ndx }}][amount]" value="{{ $disb->amount }}"></td>
									</tr>
									@endforeach
								@endif
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2">
										<button class="btn btn-sm btn-info pull-left btn-add-item" data-type="disbursement"><i class="fa fa-plus"></i> Add Disbursement</button>
										<strong class="pull-right">{{ Lang::get('invoice.TOTAL DISBURSEMENTS') }}</strong>
									</td>
									<td><strong>
										<input type="text" class="form-control input-sm input-disbursement-total-round" value="{{ number_format($total['disbursement'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-disbursement-total" name="total[disbursement]" value="{{ $total['disbursement'] }}" readonly>
									</strong></td>
								</tr>
							</tfoot>
						</table>
					</div>		
				</div>	
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('invoice.Less') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed table-less">
							<col width="5%">
							<col width="85%">
							<col width="10%">				
							<tbody class="items">
								@if(count($less) == 0)
									<tr>
										<input type="hidden" class="form-control input-sm" name="less[0][invoice_less_id]" value="" placeholder="ID">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="less"></i></td>
										<td><input type="text" class="form-control input-sm" name="less[0][description]" value="" placeholder="Description"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="less" name="less[0][amount]" value="" placeholder="Amount"></td>
									</tr>
								@else
									@foreach($less as $ndx => $l)
									<tr>
										<input type="hidden" class="form-control input-sm" name="less[{{ $ndx }}][invoice_less_id]" value="{{ $l->invoice_less_id }}">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="less"></i></td>
										<td><input type="text" class="form-control input-sm" name="less[{{ $ndx }}][description]" value="{{ $l->description }}"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="less" name="less[{{ $ndx }}][amount]" value="{{ $l->amount }}"></td>
									</tr>
									@endforeach
								@endif
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2">
										<button class="btn btn-sm btn-info pull-left btn-add-item" data-type="less"><i class="fa fa-plus"></i> Add Less</button>
										<strong class="pull-right">{{ Lang::get('invoice.TOTAL LESS') }}</strong>
									</td>
									<td><strong>
										<input type="text" class="form-control input-sm input-less-total-round" value="{{ number_format($total['less'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-less-total" name="total[less]" value="{{ $total['less'] }}" readonly>
									</strong></td>
								</tr>
							</tfoot>
						</table>
					</div>		
				</div>

				<div class="row">
					<div class="col-md-12">
						<table class="table table-condensed">
							<col width="90%">
							<col width="10%">
							<tbody>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL INVOICE') }}</strong></td>
									<td><strong class="double-underline">
										<input type="text" class="form-control input-sm input-overall-total-round" value="{{ number_format($total['overall'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-overall-total" name="total[overall]" value="{{ $total['overall'] }}" readonly>
									</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="row top-bottom-margin">
					<div class="col-md-12">
						<span class="signature">{{ Lang::get('invoice.WITH COMPLIMENTS') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p>{{ Lang::get('invoice.Cheques should be made payable to') }}: <strong>{{ $template['cheque_payable_to'] }}</strong></p>
						<p>
							{{ Lang::get('invoice.Telegraphic Transfer') }}:<br/>
							{{ $template['name_of_banker']['line_1'] }}<br/>
							{{ $template['name_of_banker']['line_2'] }}<br/>
							{{ $template['name_of_banker']['line_3'] }}<br/>
							{{ $template['name_of_banker']['line_4'] }}<br/>
						</p>
						<p>
							{{ Lang::get('invoice.Account Name') }}: {{ $template['account_name'] }}<br/>
							{{ Lang::get('invoice.Account No.') }}: {{ $template['account_no'] }}<br/>
							{{ Lang::get('invoice.Swift Code') }}: {{ $template['swift_code'] }}<br/>
						</p>
						<p>
							{{ Lang::get('invoice.Please note that interest will accrue at the rate of :interest_rate per month in the event that this invoice is not settled within :no_of_days days from the date hereof.', array('interest_rate' => '2%', 'no_of_days' => 30)) }}
						</p>
						<p>
							{{ Lang::get('invoice.No receipt will be issued unless requested.') }}
						</p>
						<p>
							{{ Lang::get('invoice.Bills may be paid by VISA or MASTERCARD in person at our office.') }}
						</p>
						<p>
							E. & O.E.
						</p>
					</div>
				</div>											
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container hidden-print">
			</div>
		</div>
		--}}

		<!-- BEGIN ACTION BUTTONS -->
		<!--
		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<button type="submit" class="btn btn-success pull-right">Save</button>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				&nbsp;
			</div>
		</div>
		-->
		<!-- END ACTION BUTTONS -->
	</form>
</div>

<!-- MODALS -->
<div class="modal fade" id="modal-move-timesheet" tabindex="-1" role="dialog" aria-labelledby="Move Timesheet" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="form-move-timesheet">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="Move Timesheet">Move Timesheet</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<p>Move this timesheet to another matter?</p>
							<table class="table table-bordered table-condensed">
								<col width="20%">
								<col width="15%">
								<col width="50%">
								<col width="15%">
								<thead>
									<tr>
										<td>Date</td>
										<td>Solicitor</td>
										<td>Description</td>
										<td>Unit</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="text" readonly class="form-control input-sm" name="timesheet[date]"></td>
										<td><input type="text" readonly class="form-control input-sm" name="timesheet[handler]"></td>
										<td><input type="text" readonly class="form-control input-sm" name="timesheet[description]"></td>
										<td><input type="text" readonly class="form-control input-sm" name="timesheet[units]"></td>
										<input type="hidden" name="timesheet[index]">
										<input type="hidden" name="timesheet[id]">
										<input type="hidden" name="timesheet[hourly_rate]">
										<input type="hidden" name="timesheet[work_hours]">
										<input type="hidden" name="timesheet[amount]">
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row form-horizontal">
						<div class="col-md-6 col-xs-6">
								<label class="control-label">Matter</label>
								<div class="">
									<select class="form-control input-sm drpdwn-matter" data-type="list" name="timesheet[matter_id]" placeholder="Search or Choose Matter" required>
										<option></option>
										@foreach($matter['list'] as $ndx => $item)
											<option value="{{ $item->JobAssignmentID }}">{{ $item->JobAssignmentID }} - {{ $item->Description_A }}</option>
										@endforeach
									</select>
								</div>
						</div>
					</div>
				</div>
				<div class="modal-footer" style="margin-top: 45px;">
					<button type="submit" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>				
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /MODALS -->

<!--
<script>
	var global = {};
	global.timesheet = {
		list : {{ json_encode($timesheet['list']) }},	
		//cost_summary : {{-- json_encode($timesheet['cost_summary']) --}}
	};
</script>
-->
<!-- <script type="text/javascript" src="/assets/thisapp/js/invoice_create_v2.js"></script> -->
<script type="text/javascript" src="/assets/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/timesheet_new_v2.js"></script>
@stop
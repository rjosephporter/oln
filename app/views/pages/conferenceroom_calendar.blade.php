@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link rel='stylesheet' href='/assets/fullcalendar/fullcalendar-2.1.1/fullcalendar.css' />
<script src='/assets/fullcalendar/fullcalendar-2.1.1/lib/jquery.min.js'></script>
<script src='/assets/fullcalendar/fullcalendar-2.1.1/lib/moment.min.js'></script>
<script src='/assets/fullcalendar/fullcalendar-2.1.1/fullcalendar.js'></script>
<div class="container-fluid">
<div><a href="{{ URL::to('/conference-room') }}" class="btn btn-default"> Back to Conference Room</a><br><br></div> 
<div id='calendar'></div>
<div class="modal fade"  id="complete_confirmation" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content" id="new_conference_room_reservation_log2">
        <form role="form" method="post">
          <div class="modal-body">
            <div class="row" id="new_conference_room_reservation_main_form">
            <div id="item-container-source" style="display:block">
            <div class="col-md-12 item-container">
              <div class="panel panel-default">
                <div class="panel-heading clearfix">
                  <button type="button" class="close pull-right delete-item_reservation" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 row">
                      <div class="col-md-12 form-group">
                          <input type="hidden" class="form-control" name="new_conference_id" value="{{ $conference_id }}">
                          <input type="hidden" class="form-control" name="new_reservation_date[]">
                          <input type="hidden" class="form-control" name="new_start_time[]">
                          <input type="hidden" class="form-control" name="new_end_time[]">
                          <label for="reservedby">Reserved By</label>
                          <select class="form-control" name="reservedby[]" placeholder="Select Handler" required>
                            <option></option>
                            @foreach($handler_list as $handler)
                              <option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="col-md-12 form-group">
                        <label for="new_purpose">Purpose</label>
                        <textarea class="form-control" name="new_purpose[]" rows="3" required></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>
                      <div class="col-md-12 form-group">
                        <label for="new_notes">Notes</label>
                        <textarea class="form-control" name="new_notes[]" rows="3"></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>
                     <!-- <div class="col-md-12 form-group">
                        <label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
                        <input type="text" class="form-control" name="new_reservation_date[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_start_time[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_end_time[]" readonly required>
                      </div>
                    </div>-->
                    
                  </div>
                </div>
              </div>  
            </div>
          </div>
            </div>
            <div class="col-md-12">
              <!--<button class="btn btn-info btn-sm" id="btn_add_item2_reservation">Add Item</button>-->
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary_reservation">Save</button>
          </div>
        </form>
      </div>
  </div>
</div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>

<script>

$('#reservedby').selectize({
      create: true,
      sortField: 'text'
  });

var item_container = $('#item-container-source').clone().html();
var modalopen=false;
var roomid1= getUrlVars()["roomid"];
 //$('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_conference_id[]"]').value;
//console.log(roomid1);
$(document).ready(function() {

    // page is now ready, initialize the calendar...
     tempArray=$(this).serializeArray();
      var newEvent;
      var eventArray=[];

    $('#new_conference_room_reservation_main_form').find('.item-container').find('select[name="reservedby[]"]').selectize();
    
     $.get('/conference-room-reservation/allreservation', { roomid : roomid1}, function(data){
          var index;
          var directorylist=data.val;

          var formattedEventData = [],
              k;

          for (var k = 0; k < directorylist.length; k += 1) {
              formattedEventData.push({
                  title: "Reserved By: " + directorylist[k].reserved_by + " \nPurpose: " + directorylist[k].purpose,

                  start: (directorylist[k].start_time !="00:00:00" ? directorylist[k].reservation_date+"T"+directorylist[k].start_time : directorylist[k].reservation_date),
                  end: (directorylist[k].end_time !="00:00:00" ? directorylist[k].reservation_date+"T"+directorylist[k].end_time : directorylist[k].reservation_date)

              });
          }
          console.log(formattedEventData);

           $('#calendar').fullCalendar({
                  // put your options and callbacks here
                  header: {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay'
                },
                //editable: true,
                unselectAuto:false,
                selectable:true,
                eventLimit: true, // allow "more" link when too many events
                selectHelper: true,
                select: function(start, end) {
                    agenda: 'h:mm{ - h:mm}'
                  
                   if(modalopen==false){
                    var d = $('#calendar').fullCalendar('getDate');
            
                    var start1=start.format('HH:mm:ss');
                    var end1=end.format('HH:mm:ss');
                    $('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_start_time[]"]').val(start1);
                    $('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_end_time[]"]').val(end1);
                    $('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_reservation_date[]"]').val(d);
                    $("#complete_confirmation").modal().show();
                    
                   }
                       $('#calendar').fullCalendar( 'renderEvent', eventArray[0] , true); 
                },
                events: formattedEventData
                      

              })   
     });

    

   

});

$('#new_conference_room_reservation_log2 form').submit(function(e){
        e.preventDefault();
        tempArray=$(this).serializeArray();

            //console.log($('#new_conference_room_reservation_main_form').find('.item-container').find('input[name="new_start_time[]"]').val);

              $.post('/conference-room-reservation/savenewreservation2', tempArray, function(data){
                      modalopen=true;
                      if(data.status == 'success') {
                          toastr.success(data.msg);
                        var delay = 3000; //Your delay in milliseconds
                        
                        setTimeout(function(){ window.location = '{{ URL::to('/conference-room') }}' }, delay);  
                         
                         
                      } else {
                          toastr.error(data.msg);
                      }
              });
              
            
});  

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


</script>
@stop
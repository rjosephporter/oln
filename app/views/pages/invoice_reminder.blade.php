@extends('template/default')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">

      <h1><i class="fa fa-envelope"></i> {{ Lang::get('lang.invoice.Invoice Reminder') }}</h1>
      <h5><i class="fa fa-info-circle text-info"></i> {{ Lang::get('lang.invoice.Send reminder to') }} <strong>{{ $recipient_name }}</strong> {{ Lang::get('lang.invoice.for') }} <strong>{{ Lang::get('lang.invoice.Invoice No') }}. {{ $invoice_number }}</strong>?</h5>

      <form id="invoice-reminder-form" role="form" method="post" action="/invoice/send-reminder">
        @if($type == 'accounting')
          <input type="hidden" id="reminder_batch_number" name="reminder_batch_number" value="{{ $batch_number }}"> 
        @else
          <input type="hidden" id="reminder_batch_number" name="reminder_batch_number" value="{{ $invoice_number }}"> 
        @endif
        <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email" value="{{ $recipient_email }}">
        @if($type == 'invoice')
          <input type="hidden" id="no_log" name="no_log" value="1">
        @endif

        <div class="form-group">
          <label for="reminder_recipient">{{ Lang::get('lang.invoice.To') }}</label>
          <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Client's Email" required value="{{ $recipient_email }}">
        </div>

        <div class="form-group">
          <label for="reminder_message">{{ Lang::get('lang.invoice.Message') }}</label>
          <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>{{ $message }}</textarea>
        </div>

        <div class="pull-right">
          <button type="submit" class="btn btn-success">{{ Lang::get('lang.invoice.Yes') }}</button>
          <a id="cancel" class="btn btn-danger" href="/{{$type}}">{{ Lang::get('lang.invoice.No') }}</a>
        </div>

      </form>

    </div>
  </div>
</div>

<script>
$('#invoice-reminder-form').submit(function(e) {
  e.preventDefault();
  var post_data = $(this).serializeObject();

  $.post('/{{$type}}/send-reminder', post_data, function(data) {
    console.log(data);
    toastr.success('Reminder sent. Redirecting to {{$type}} list...');
    var delay = 3000; //Your delay in milliseconds
    setTimeout(function(){ window.location = $('#cancel').attr('href'); }, delay);
  });
});
</script>
@stop
@extends('template/default')
@section('content')
<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-xs-12">
                <div id="help" class="pull-right"></div>
            </div>    
        </div>
        <div class="row top-buffer">
        @if(User::find(Session::get('principal_user'))->can('dashboard_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-dashboard very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/dashboard" class="nostyle">{{ Lang::get('lang.home.Dashboard') }}</a></h3>
        </div> 
        @endif 
        @if(User::find(Session::get('principal_user'))->can('office_access'))      
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span><img src="/assets/thisapp/images/office/fp3.png"/></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/office" class="nostyle">{{ Lang::get('lang.home.Office') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('matters_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-folder-open very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/matters" class="nostyle">{{ Lang::get('lang.home.Matters') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('ip_matters_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon very-large-glyph">IP</span>
                </div>
            </div>
            <h3 class="text-center"><a href="/matters?type=ip" class="nostyle">{{ Lang::get('lang.home.IP Matters') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('handlers_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
<!--                    <span class="glyphicon glyphicon-user very-large-glyph"></span>-->
                    <span><img src="/assets/thisapp/images/app/lawyer.png"/></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/handlers" class="nostyle">{{ Lang::get('lang.home.Handlers') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('clients_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-certificate very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/clients" class="nostyle">{{ Lang::get('lang.home.Clients') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('comm_center_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-retweet very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/commcenter" class="nostyle">{{ Lang::get('lang.home.Comm Center') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('organization_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
<!--                    <span class="glyphicon glyphicon-retweet very-large-glyph"></span>-->
                    <i class="fa fa-users" style="font-size:50px"></i>
                </div>
            </div>
            <h3 class="text-center"><a href="/orgchart" class="nostyle">{{ Lang::get('lang.home.Organization') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('timesheet_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-time very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/timesheet" class="nostyle">{{ Lang::get('lang.home.Timesheet') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('invoice_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-th-list very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/invoice" class="nostyle">{{ Lang::get('lang.home.Invoice') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('reports_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="glyphicon glyphicon-list-alt very-large-glyph"></span>
                </div>
            </div>
            <h3 class="text-center"><a href="/reports" class="nostyle">{{ Lang::get('lang.home.Reports') }}</a></h3>
        </div>
        @endif
        @if(User::find(Session::get('principal_user'))->can('accounting_access'))
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="home-button">
                <div class="glyph-container">
                    <span class="fa-stack fa-lg" style="font-size: 50px;">
                      <i class="fa fa-file-o fa-stack-2x"></i>
                      <i class="fa fa-usd fa-stack-1x"></i>
                    </span>
                </div>
            </div>
            <h3 class="text-center"><a href="/accounting" class="nostyle">{{ Lang::get('lang.home.Accounting') }}</a></h3>
        </div> 
        @endif
    </div>
        @if(User::find(Session::get('principal_user'))->can('sysadmin_access'))       
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="home-button">
                    <div class="glyph-container">
                        <span class="glyphicon glyphicon-wrench very-large-glyph"></span>
                    </div>
                </div>
                <h3 class="text-center"><a href="/sysadmin" class="nostyle">{{ Lang::get('lang.home.System Admin') }}</a></h3>
            </div>
            @endif
            @if(User::find(Session::get('principal_user'))->can('conference_room_access'))
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="home-button">
                    <div class="glyph-container">
                        <!-- <span class="glyphicon glyphicon-home very-large-glyph"></span> -->
    <!--                    <i class="fa fa-cubes" style="font-size:50px"></i>-->
                        <span><img src="/assets/thisapp/images/app/meeting.png"/></span>
                    </div>
                </div>
                <h3 class="text-center"><a href="/conference-room" class="nostyle">{{ Lang::get('lang.home.Conference Room') }}</a></h3>
            </div>
            @endif
            @if(User::find(Session::get('principal_user'))->can('hr_access'))
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <div class="home-button">
                    <div class="glyph-container">
                        <!-- <span class="glyphicon glyphicon-home very-large-glyph"></span> -->
    <!--                    <i class="fa fa-cubes" style="font-size:50px"></i>-->
                        <span><img src="/assets/thisapp/images/app/hr.png"/></span>
                    </div>
                </div>
                <h3 class="text-center"><a href="/employee" class="nostyle">{{ Lang::get('lang.home.HR') }}</a></h3>
            </div>
            @endif
        </div>
    </div>
</div>
<script type="text/x-template" id="help_items">
<div class="dropdown">
    <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
        {{ Lang::get('lang.invoice.Help') }}
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/thisapp/help_pages/how-to-do-a-global-search.html">How to do a Global Search.</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/thisapp/help_pages/how-to-print-an-invoice.html">How to print an invoice.</a></li>
    </ul>
</div>
</script>
<script type="text/javascript" src="/assets/thisapp/js/home.js"></script>
<script>
    $(document).ready(function () {
        $('#help').append($($('#help_items').html()));
        $('#help').on('click','a[role=menuitem]',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url);
        })
    })
</script>
@stop
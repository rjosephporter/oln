@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<div class="container">

    <div class="modal-content _modal_content__">
       <div style="vertical-align: middle; text-align: center; height:500px; width: 100%; padding-top: 200px;">
            <div><h3>. . . Processing Data . . .</h3></div>
            <div><img src="/assets/thisapp/images/ajax-loader-bar.gif"/></div>
       </div>
    </div>
	
    <input class="form-control" type="hidden" name="matid" id="matterid" placeholder="Matter ID" value="{{{$matter[0]->JobAssignmentID}}}">
    <div style="height: 0px; width: 0px; overflow: hidden; visibility: hidden;" id="_body_billing"></div>
    <div style="height: 0px; width: 0px; overflow: hidden; visibility: hidden;" id="_matter_timeline_ul"></div>

</div>


<!--modals-->
<div class="modal fade" id="complete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">Updating Matter...Please wait <span class="matter_for_completion"></span></h4>
      </div>
    </div>
  </div>
</div>
	

<div class="modal fade" id="_modal_cost_on_account___">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title txt-info">Cost on Accounts (Deposit)</h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label for="_cost_amount">Enter Amount Deposited</label>
            <input type="text" class="form-control" id="_cost_amount">
          </div>
          <div class="form-group">
            <label for="_cost_date">Date Deposited</label>
            <input type="text" class="form-control" id="_cost_date">
          </div>
            <input type="hidden" value="{{ $matter[0]->JobAssignmentID }}" id="_cost_matter_id"/>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary" onclick="_save_cost()">Save Deposit</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="_confirmation__create">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title txt-info">New Matter: {{ $matter[0]->JobAssignmentID }}</h4>
      </div>
      <div class="modal-body">
        <h3 style="color: green;">You have successfully created a new Matter.</h3>
          <p>&nbsp;</p>
        <p><h4>Matter No: {{ $matter[0]->JobAssignmentID }}</h4></p>
        <p><h4>{{ $matter[0]->Description_A }}</h4></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<!--billing and timeline -->
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>

<script id="_main_body_tmpl" type="text/x-jquery-tmpl"
    <div id="item-container-source">
    <div class="col-md-12 item-container">
        <form role="form" method="post" action="/matters/save">
            <div class="panel panel-default">
                <div class="panel-heading" style="color: {{ $matter[0]->matter_type == 'blue' ? 'white' : 'black' }}; background-color: {{ $matter[0]->matter_type == 'blue' ? "#0075b0"  : ($matter[0]->matter_type == 'green' ? "rgba(0, 128, 0, 0.5)" : ($matter[0]->matter_type == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($matter[0]->matter_type == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Matter No: {{$matter[0]->JobAssignmentID}} - {{$matter[0]->Description_A}}</h4>
                            Last Transaction Date: <label >@if($matter[0]->matter_type != 'gray') {{date_format(date_create($matter[0]->last_trans_date),"F d, Y")}} @else No Transaction Record @endif</label>
                        </div>
                    </div>
                    <div class='row' style="text-align: center;">
                        <div class="col-md-4" style="text-align: left;">
                            <div class="form-group">
                                Billable Amount: <label >${{number_format($matter[0]->billable_amount, 2, '.', ',')}}</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                Billed Amount: <label class="control-label">${{number_format($matter[0]->billed_amount, 2, '.', ',')}}</label>
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align: right;">
                            <div class="form-group">
                                Unbilled Amount: <label class="control-label">${{number_format($matter[0]->unbilled, 2, '.', ',')}}</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">			
                            <div class="row">
                                <div class="col-md-6">
                                    <div class='row'>
                                        <div class="col-md-12 form-group" id='div_for_existing_client' style='display: inherit;'>
                                            <label style="float: left;"><input name="ip_matter_box" type="checkbox" id="_ip_matter_option"{{$matter[0]->Completed == 9 ? ' checked' : ''}}> IP Matter</label>
                                            <label for="__current_client">Client</label>
                                            <select class="form-control" name="current_client" id='__current_client'>
                                                <option value="">Select Client</option>
                                                @foreach($client_list as $client)
                                                    <option value="{{ $client->CustomerID }}"{{$matter[0]->CustomerID == $client->CustomerID ? ' selected' : ''}}>{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="new_designated_date">Date  <small>(click to select date)</small></label>
                                                <input type="text" class="form-control" id="_date_control_id" name="new_designated_date" value="{{date_format(date_create($matter[0]->DesignatedDate),"M d, Y")}}" >
                                        </div>

                                        <div class="col-md-12 form-group" style='display: inherit;'>
                                            <label for="beneficial_owner">Beneficial Owner</label>
                                            <select class="form-control" name="beneficial_owner">
                                                <option value="">Select Beneficial Owner</option>
                                                @foreach($client_list as $client)
                                                    <option value="{{ $client->CustomerID }}"{{$details[0]->beneficial_owner == $client->CustomerID ? ' selected' : ''}}>{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="fixed_amount___">Fixed Amount</label>
                                                <label style="float: right;"><input name="fix_fee" type="checkbox" id="_fix_fee_check" onchange="_toggle_fix()"{{$details[0]->fixedpay == 1 ? 'checked' : ''}}> Fix Fee</label>
                                                <input type="text" class="form-control" name="fixed_amount___"  id="fixed_amount___" value="{{ $details[0]->fixedamount }}" required="false">
                                        </div>
                                        
                                        <div class="col-md-12 form-group">
                                            <label for="deposit___">Initial Cost on Account (Deposit)</label>
                                            <input type="text" class="form-control" name="deposit___"  id="deposit___" value="{{ count($credit) > 0 ? $credit[0]->Amount : '' }}"{{ count($credit) > 1 ? ' readonly' : '' }}>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="introducer__">Introducer</label>
                                                <select class="form-control" name="introducer__" id="introducer__" placeholder="Select Introducer">
                                                    <option value="">Select Introducer</option>
                                                    @foreach($handler_list as $handler)
                                                        <option value="{{ $handler->EmployeeID }}"{{$details[0]->introducer == $handler->EmployeeID ? ' selected' : ''}}>{{ $handler->EmployeeID }} - {{ $handler->NickName}}</option>
                                                    @endforeach
                                                </select>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="new_handler_id">Handler</label>
                                                <select class="form-control" name="new_handler_id" id="handler" required>
                                                    <option value="">Select Handler</option>
                                                    @foreach($handler_list as $handler)
                                                        <option value="{{ $handler->EmployeeID }}"{{$matter[0]->Incharge == $handler->EmployeeID ? ' selected' : ''}}>{{ $handler->EmployeeID }} - {{ $handler->NickName}}</option>
                                                    @endforeach
                                                </select>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="new_matter_description">Description</label>
                                                <textarea class="form-control" name="new_matter_description" rows="3" required>{{ strlen($matter[0]->Remark) > 0 ? $matter[0]->Remark : $matter[0]->Description_A }}</textarea>
                                        </div>

                                        <div class="col-md-12 form-group">
                                                <label for="new_matter_description">Special Client Instructions</label>
                                                <textarea class="form-control" name="client_instruction" rows="3">{{ $details[0]->special_instructions }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
          

                                        <div class="col-md-12 form-group">
                                            <div class="panel panel-default">
                                              <div class="panel-heading">
                                                <h3 class="panel-title" style="color: blue;">Matter Specific Client Information</h3>
                                              </div>
                                              <div class="panel-body">
                                                    <div class="col-md-12 form-group">
                                                        <label for="__client_">Client</label>
                                                        <input type="text" class="form-control" name="__client_" value="{{ $details[0]->client }}"/>
                                                    </div>
                                                   <div class="col-md-12 form-group">
                                                        <label for="__address_">Address</label>
                                                        <input type="text" class="form-control" name="__address_" value="{{ $details[0]->address }}"/>
                                                    </div>
                                                     <div class="col-md-12 form-group">
                                                        <label for="__tel_">Telephone No.</label>
                                                        <input type="text" class="form-control" name="__tel_" value="{{ $details[0]->tel }}"/>
                                                    </div>
                                                    <div class="col-md-12 form-group">
                                                        <label for="__email_">Email</label>
                                                        <input type="text" class="form-control" name="__email_" value="{{ $details[0]->email }}"/>
                                                    </div>
                                                    <div class="col-md-12 form-group">
                                                        <label for="__fax_">Fax</label>
                                                        <input type="text" class="form-control" name="__fax_" value="{{ $details[0]->fax }}"/>
                                                    </div>
                                                    <div class="col-md-12 form-group">
                                                        <label for="__attn_">Attn:</label>
                                                        <input type="text" class="form-control" name="__attn_" value="{{ $details[0]->attn }}"/>
                                                    </div>
                                                    <div class="col-md-12 form-group">
                                                        <label for="__re_">Re</label>
                                                        <input type="text" class="form-control" name="__re_" value="{{ $details[0]->re }}"/>
                                                    </div>
                                              </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </div>
                </div>
                <div class='panel-footer'>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class='btn btn-primary' style="float: right;">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="opt_update" value="{{$matter[0]->JobAssignmentID}}"/>
        </form>
        </div>
    </div>
</script>

<script id="_cost_body_tmpl" type="text/x-jquery-tmpl">
    <?php
        $balance = 0;
        foreach($credit as $crd):
            $balance += $crd->type == 1 ? $crd->Amount : 0;
            $balance -= $crd->type == 2 ? $crd->Amount : 0;
    ?>
    <div class="row" style="text-align: center;">
        <div class="col-md-2">{{date_format(date_create($crd->date_c),"M d, Y")}}</div>
        <div class="col-md-2">{{ $crd->type == 1 ? 'Deposit' : 'Payments' }}</div>
        <div class="col-md-3" style="text-align: right;">${{ number_format($crd->Amount, 2, '.', ',') }}</div>
        <div class="col-md-2">{{ $crd->user_c }}</div>
        <div class="col-md-3" style="text-align: right;">${{ number_format($balance, 2, '.', ',') }}</div>
    </div>
    <?php
        endforeach;
        if(count($credit) == 0):
    ?>
        <div class="row" style="text-align: center;">
            <div class="col-md-12" style="margin-top: 15px;"><h4>. . . No Record Found . . .</h4><div/>
        </div>
    <?php
        endif;
    ?>
</script>

<script id="_cost_ajax_tmpl" type="text/x-jquery-tmpl">
    <div class="row" style="text-align: center;">
        <div class="col-md-2">${$.datepicker.formatDate("M dd, yy", new Date(date_c))}</div>
        <div class="col-md-2">${ type == 1 ? "Deposit" : "Payments" }</div>
        <div class="col-md-3" style="text-align: right;">$${parseFloat(Amount).formatMoney(2)}</div>
        <div class="col-md-2">${ user_c }</div>
        <div class="col-md-3" style="text-align: right;">$${parseFloat(balance).formatMoney(2)}</div>
    </div>
</script>

<script type='text/javascript'>
    
var all_matters = {};
var fceval = {CustomerID: '', Incharge: ''};
var timeline_tmpl = "";
var billing_tmpl = "";
var matter_id = 0;
var open_tmpl = "";
var open_tmpl_body = "";

function _save_cost(){
    if(isNaN($("#_cost_amount").val()) || $("#_cost_amount").val().length == 0 || parseFloat($("#_cost_amount").val()) == 0){
        $("#_cost_amount").val("Invalid amount, try again.");
    }else{
        $.post('/matters/update',{id : $("#_cost_matter_id").val(), amount : $("#_cost_amount").val(), date : $("#_cost_date").val()},function(data){
            var balance = 0;
            $.each(data.credit, function(i, item){
                balance += item.type == 1 ? parseFloat(item.Amount) : 0;
                balance -= item.type == 2 ? parseFloat(item.Amount) : 0;
                item.balance = balance;
            });
            var credit = data.credit
            $("#_cost_amount").val('');
            $("#_modal_cost_on_account___").modal('hide');
            console.log(credit);
            $("#body_for_cost_summary").html($("#_cost_ajax_tmpl").tmpl(credit));
        },'json');
    }
}

function process_cost_account(){
    $("#_modal_cost_on_account___").modal().show();
    $("#_cost_date").datepicker({
        maxDate     : 0,
        dateFormat  : "M dd, yy"
    });
    $("#_cost_date").datepicker("setDate","{{ date('m/d/Y') }}");
}

function _toggle_fix(){
    if($("#_fix_fee_check").is(":checked")){
        document.getElementById('fixed_amount___').readonly = false;
        document.getElementById('fixed_amount___').required = true;
        if(parseInt(document.getElementById('fixed_amount___').value) == 0)
            document.getElementById('fixed_amount___').value = '';
    }
    else{
        document.getElementById('fixed_amount___').readonly = true;
        document.getElementById('fixed_amount___').required = false;
    }
}

function get_info(id_){
    var new_data = {id: id_, info: {},tl:{}, bh:{}, Incharge:''};
    $.get('/matters/detailsmatterall/' + id_, function(data){
            new_data.info = data;
            all_matters = data;
            $.get('/matters/bh/'+ id_, function(info){
                    new_data.bh = info;
                    $.get('/matters/tl/'+ id_, function(info){
                            new_data.tl = info;
                            display_info(new_data);
                    },'json');
            },'json');
    },'json');	
}
var get_params = function(search_string) {

  var parse = function(params, pairs) {
    var pair = pairs[0];
    var parts = pair.split('=');
    var key = decodeURIComponent(parts[0]);
    var value = decodeURIComponent(parts.slice(1).join('='));

    if (typeof params[key] === "undefined") {
      params[key] = value;
    } else {
      params[key] = [].concat(params[key], value);
    }

    return pairs.length == 1 ? params : parse(params, pairs.slice(1))
  }

  return search_string.length == 0 ? {} : parse({}, search_string.substr(1).split('&'));
}


function display_info(obj){
	fceval.CustomerID = ("00000" + obj.info[0].CustomerID).substr(-5,5);
	fceval.Incharge = obj.info[0].Incharge;
	$('#_body_billing').html($.tmpl(billing_tmpl,obj.bh));
        $('#_matter_timeline_ul').html('');
	var cl_i ={class:'',badge:' success '};
	for(var i = 0; i < obj.tl.length; i++){
		var cl = i == 0 ? cl_i : (obj.tl[i].WorkDate == obj.tl[i-1].WorkDate ? cl : (cl.class.length == 0 ? {class: ' class=timeline-inverted ', badge:' info '} : cl_i));
		var subj = obj.tl[i].Comment ? obj.tl[i].Comment.substring(0,12) : '';
		var comm = obj.tl[i].Comment ? obj.tl[i].Comment : '';
		$.tmpl(timeline_tmpl,{cls: cl.class, badge: cl.badge, Incharge: obj.tl[i].EmployeeID, datetime: obj.tl[i].WorkDate, info: comm}).appendTo('#_matter_timeline_ul');
	}
	$.tmpl(timeline_tmpl,{cls: '', badge: ' warning ', Incharge: obj.tl[0].Incharge, datetime: obj.tl[0].DesignatedDate, info: obj.tl[0].Description_A}).appendTo('#_matter_timeline_ul');

        var target_matter = all_matters;
        $("._modal_content__").html(open_tmpl_body);
        $("._body_billing").html($("#_body_billing").html());
        $("._matter_timeline_ul").html($("#_matter_timeline_ul").html());
        $('.r_billable_total').html('$' + parseFloat(target_matter[0].billable_amount).formatMoney(2));
	   $('.r_billed_total').html('$' + parseFloat(target_matter[0].billed_amount).formatMoney(2));
       $('.r_unbilled_total').html('$' + parseFloat(target_matter[0].unbilled).formatMoney(2));
        $("#main_body_tab").html($("#_main_body_tmpl").tmpl());
        $('select').selectize();
        $( "#_date_control_id" ).datepicker({
            maxDate     : 0,
            dateFormat  : "M dd, yy"
        });
        $("#_cost_date").datepicker({
            maxDate     : 0,
            dateFormat  : "M dd, yy"
        });
        $("#_cost_date").datepicker("setDate","{{ date('m/d/Y') }}");
        $(".m_current_modal_title").html(obj.info[0].JobAssignmentID + ": " + obj.info[0].Description_A);
        $("#body_for_cost_summary").html($("#_cost_body_tmpl").tmpl());
            $('#billing_table').dataTable({
            order : [ 5, 'desc' ], pagingType: "full", pageLength : 5, orderClasses : true, bLengthChange : false, bFilter : false
        });
        _toggle_fix();
        var params = get_params(location.search);
        if(params['source'] == 'create'){
            $("#_confirmation__create").modal().show();
        }
}

$(function(){
    $.get("/assets/thisapp/tmpl/_dis_billing.tmpl",function(tmpl){
        billing_tmpl=tmpl;
        $.get("/assets/thisapp/tmpl/_dis_timeline.tmpl",function(tmpl){
            timeline_tmpl=tmpl;
            $.get('/assets/thisapp/tmpl/open_matter.tmpl',function(tmpl){
                open_tmpl = tmpl;
                $.get('/assets/thisapp/tmpl/open_matter_body_.tmpl',function(tmpl){
                    open_tmpl_body = tmpl;
                    matter_id = $('#matterid').val();
                    get_info(matter_id);
                    $('div[data-toggle="tooltip"]').tooltip();
                });
            });   
        });
    }); 
});
</script>
@stop
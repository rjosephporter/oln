@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<div class="container">
    <!-- Dashboard -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                  <div class="panel-body"> <!-- start -->
                    <div class="row">
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="green__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: #18bc9c;">Active Last 4 Weeks</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/green.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="orange__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: orange;">Last Activity</h2>
                                <h2 class="client-name" style="color: orange;">Between 4 and 8 Weeks</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/orange.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="red__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: red;">Last Activity</h2>
                                <h2 class="client-name" style="color: red;">Beyond 8 Weeks</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/red.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="gray__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: gray;">Untouched/No Activity</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/grey.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="blue__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: blue;">Completed Matters</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/blue.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                    </div>   
                </div><!-- End of Body -->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/matters_landing_.js"></script>
@stop 
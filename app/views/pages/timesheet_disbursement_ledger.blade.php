@extends('template/default')

@section('header-custom-css')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Disbursement Ledger <span class="label label-info">{{ Input::has('matter_id') ? 'Matter ID: ' . Input::get('matter_id') : 'All Matters' }}</span></h1>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					{{ $disbursement_ledger_form }}
				</div>
			</div>
			<div class="row" style="margin-top: 30px">
				<div class="col-md-12 col-sm-12">			
					{{ $disbursement_ledger_table }}
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('footer-custom-js')
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script>
	$('select[name="ledger[matter_id]"]').selectize();
	$('select[name="ledger[matter_id]"]').on('change', function() {
		var append = $(this).val() == '' ? '' : '?matter_id=' + $(this).val();
		window.location.replace(base_url + '/timesheet/disbursement-ledger' + append);
	});
</script>
@stop
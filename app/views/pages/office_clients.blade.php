<div class="row" total-page="{{ $total_page }}">
    @foreach($clients as $client)
    <div class="col-md-3 col-xs-6">
        <div class="well well-sm">
            <div class="row">
                <!--
                <div class="col-xs-3 col-md-3 text-center">
                    <i class="glyphicon glyphicon-user" style="font-size: 3em;"></i>
                    <p class="text-center">{{ $client->handler_incharge }}</p>
                </div>
                -->
                <div class="col-xs-12 col-md-12 section-box">
                    <h2 class="client-name">{{ str_limit($client->CompanyName_A1, 25, '...') }}</h2>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="client-info">
                                <li data-toggle="tooltip" data-placement="top" title="Client ID"><i class="fa fa-info"></i>  {{ $client->customer_id }}</li>
                                <li data-toggle="tooltip" data-placement="top" title="Total Matters"><i class="fa fa-folder-open"></i>  {{ $client->matter_count }}</li>
                                <li data-toggle="tooltip" data-placement="top" title="Unbilled Amount"><i class="fa fa-bookmark-o"></i>  ${{ number_format($client->total_unbilled_amount, 2, '.', ',') }}</li>
                                <li data-toggle="tooltip" data-placement="top" title="Handler"><i class="fa fa-user"></i>  {{ $client->handler_incharge }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i>&nbsp; &nbsp;{{ Lang::get('lang.client.New Client') }} 
			</h2>
			<div class="modal-content" id="new_client_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_client_main_form">
							<div class="col-md-12 form-group">
							<label for="new_clienttype_id1">{{ Lang::get('lang.client.Client Type') }}</label>
							<select class="form-control" name="new_clienttype_id" placeholder="Client Type" required>
									
							<option value="1">Individual</option>
							<option value="2">Corporate</option>
									
						     </select>
						 	</div>
						     <!--start-->
						     <div id="item-container-source" style="display:none">
							<div class="col-md-12 item-container">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<br>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
														<label for="new_clientname">{{ Lang::get('lang.client.Client Name') }} </label>
														<input type="text" class="form-control check" id="new_clientname[]" name="new_clientname[]">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_hkid">{{ Lang::get('lang.client.Hong Kong ID Card No/Passport No') }} </label>
														<input type="text" class="form-control check" id="new_hkid[]" name="new_hkid[]">
												</div>	

												<div class="col-md-12 form-group">
													<label for="new_dob">{{ Lang::get('lang.client.Date of Birth') }}  <small>{{ Lang::get('lang.client.(click to select date)') }}</small></label>
													<input type="text" class="form-control check" name="new_dob[]" readonly>
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_pob">{{ Lang::get('lang.client.Place of Birth') }} </label>
														<input type="text" class="form-control check" id="new_pob[]" name="new_pob[]">
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nationality_id">{{ Lang::get('lang.client.Nationality') }}</label>
													<select class="form-control check" name="new_nationality_id[]" placeholder="Nationality">
															<option></option>
															@foreach($nationality_list as $nat)
																<option value="{{ $nat->CountryCode }}">{{ $nat->CountryName_A }}</option>
															@endforeach
												     </select>
												</div>				
													
												
												<div class="col-md-12 form-group">
													<label for="new_acting_id1">{{ Lang::get('lang.client.Acting for another person') }}?</label>
													<input type="checkbox" class="form-control" id="new_acting_id" name="new_acting_id" >
												</div>
												<div id="addinfo" style="display:none">
												<div class="col-md-12 form-group">
														<label for="new_personname">{{ Lang::get('lang.client.Name') }} </label>
														<input type="text" class="form-control" id="new_personname[]" name="new_personname[]" >
												</div>
												<div class="col-md-12 form-group">
														<label for="new_contactnum">{{ Lang::get('lang.client.Contact Number') }} </label>
														<input type="text" class="form-control" id="new_contactnum[]" name="new_contactnum[]" >
												</div>
												</div>

											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
														<label for="new_home_address">{{ Lang::get('lang.client.Home Address') }} </label>
														<input type="text" class="form-control check" id="new_home_address[]" name="new_home_address[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_cor_address">{{ Lang::get('lang.client.Correspondence Address (if different)') }} </label>
														<input type="text" class="form-control" id="new_cor_address[]" name="new_cor_address[]">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_occupation">{{ Lang::get('lang.client.Occupation') }} </label>
														<input type="text" class="form-control check" id="new_occupation[]" name="new_occupation[]" novalidate>
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_employer">{{ Lang::get('lang.client.Employer') }} </label>
														<input type="text" class="form-control check" id="new_employer[]" name="new_employer[]" novalidate>
												</div>	
												
												<div class="col-md-6 form-group">
														<label for="new_hometel">{{ Lang::get('lang.client.Home Telephone Number') }} </label>
														<input type="text" class="form-control" id="new_hometel[]" name="new_hometel[]" >
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_homefax">{{ Lang::get('lang.client.Home Fax Number') }} </label>
														<input type="text" class="form-control" id="new_homefax[]" name="new_homefax[]" >
												</div>
												<div class="col-md-6 form-group">
														<label for="new_offtel">{{ Lang::get('lang.client.Office Telephone Number') }} </label>
														<input type="text" class="form-control" id="new_offtel[]" name="new_offtel[]" >
												</div>
												<div class="col-md-6 form-group">
														<label for="new_mobile">{{ Lang::get('lang.client.Mobile Number')}} </label>
														<input type="text" class="form-control check" id="new_mobile[]" name="new_mobile[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_email">{{ Lang::get('lang.client.Email Address') }} </label>
														<input type="text" class="form-control check" id="new_email[]" name="new_email[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
													<label for="new_handler_id">{{ Lang::get('lang.client.Incharge') }}</label>
													<select class="form-control check" name="new_handler_id[]" placeholder="Select Incharge" novalidate>
															<option></option>
															@foreach($handler_list as $handler)
																<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
															@endforeach
												     </select>
												</div>
											</div>
										
										</div>
									</div>
								</div>	
							</div>
						</div>

						<!--corporate-->
						<div id="item-container-source2" style="display:none">
							<div class="col-md-12 item-container2">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<br>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
														<label for="new_clientcomname">{{ Lang::get('lang.client.Client Company Name') }} </label>
														<input type="text" class="form-control check2" id="new_clientcomname[]" name="new_clientcomname[]" novalidate>
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_comnum">{{ Lang::get('lang.client.Company No') }}. </label>
														<input type="text" class="form-control check2" id="new_comnum[]" name="new_comnum[]" novalidate>
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_poi">{{ Lang::get('lang.client.Place of Incorporation') }}</label>
														<input type="text" class="form-control check2" id="new_poi[]" name="new_poi[]" novalidate>
												</div>

												<div class="col-md-12 form-group">
														<label for="new_bus_address">{{ Lang::get('lang.client.Business Address') }} </label>
														<input type="text" class="form-control check2" id="new_bus_address[]" name="new_bus_address[]" novalidate>
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nature">{{ Lang::get('lang.client.Nature of Business') }}</label>
													<input type="text" class="form-control check2" id="new_nature[]" name="new_nature[]" novalidate>
												</div>	

												<div class="col-md-6 form-group">
														<label for="new_telnum">{{ Lang::get('lang.client.Telephone Number') }} </label>
														<input type="text" class="form-control check2" id="new_telnum[]" name="new_telnum[]" novalidate>
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_faxnum">{{ Lang::get('lang.client.Fax Number') }} </label>
														<input type="text" class="form-control check2" id="new_faxnum[]" name="new_faxnum[]" novalidate>
												</div>			

											</div>
											<div class="col-md-6 row">
												
												<!--<div class="col-md-12 form-group input_fields_wrapdir">
												    <label for="new_dir">Name(s) of Director(s) </label>
												    <div><input type="text" class="form-control check2" name="new_dir[]">
											
												    </div>
												    
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>-->
												<div class="col-md-12 form-group">
												    <label for="new_dir">{{ Lang::get('lang.client.Name(s) of Director(s)') }} </label>
												    <div>
												    	
												    	<input type="text" class="form-control check2" name="new_dir[]">
														
												    </div>
												</div>
												<div class="col-md-12 input_fields_wrapdir">

								                </div>
								                <div class="col-md-12 form-group">
												</div>
								                <div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
												
												<div class="col-md-12 form-group ">
												    <label for="new_share">{{ Lang::get('lang.client.Name(s) of Shareholder(s)') }}</label>
												    <div><input type="text" class="form-control check2" name="new_share[]">
											
												    </div>
												    
												</div>
												<div class="col-md-12 input_fields_wrapshare">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonshare">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_ben">{{ Lang::get('lang.client.Name(s) of Ultimate Beneficial Owner(s) (if different from Shareholder)') }}</label>
												    <div><input type="text" class="form-control check2" name="new_ben[]">
											
												    </div>
												    
												</div>
												<div class="col-md-12 input_fields_wrapben">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonben">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_person">{{ Lang::get('lang.client.Name of Person(s) representing the Company') }} </label>
												    <div><input type="text" class="form-control check2" name="new_person[]">
													<label for="new_personhkid">{{ Lang::get('lang.client.HKID Card No/Passport No') }} </label>
													<input type="text" class="form-control check2" name="new_personhkid[]">
													<label for="new_personpos">{{ Lang::get('lang.client.Capacity Position') }} </label>
													<input type="text" class="form-control check2" name="new_personpos[]">
													<label for="new_personconnum">{{ Lang::get('lang.client.Contact Number') }} </label>
													<input type="text" class="form-control check2" name="new_personconnum[]">
													<label for="new_personemail">{{ Lang::get('lang.client.Email Address') }} </label>
													<input type="text" class="form-control check2" name="new_personemail[]">
												    </div>
												    
												</div>

												<div class="col-md-12 input_fields_wrapperson">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonperson">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
											</div>
										
										</div>
									</div>
								</div>	
							</div>
						</div>
						<!--end-->
						
						</div>
						<div class="col-md-12">
							<!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/clients') }}" class="btn btn-default">{{ Lang::get('lang.client.Back') }}</a>
						<button type="submit" class="btn btn-primary">{{ Lang::get('lang.client.Save') }}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script>


	$('#new_handler_id').selectize({
	    create: true,
	    sortField: 'text'
	});

	report_types = [];
	new_client_log_count = 0;

	  /* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };

	var item_container = $('#item-container-source').clone().html();
	var item_container2 = $('#item-container-source2').clone().html();
	$(document).on('change', 'select[name="new_clienttype_id"]', function() {
    
    	var selected_client = $(this).val();
    	if(selected_client == 1){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#item-container-source').show();
    		$('#item-container-source2').hide();
    		$('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		$('#item-container-source2').show();
    		$('#item-container-source').hide();
    	}

    	
    });

    $(document).on('change', 'input[name="new_acting_id"]', function() {
    
    	var ischecked = $("#new_acting_id").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#addinfo').show();
    		
    		//$('#item-container-source2').hide();
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		
    		$('#addinfo').hide();
    		//$('#item-container-source').hide();
    	}

    	
    });


    $(document).ready(function() {

	    var max_fields      = 5; //maximum input boxes allowed
	    var wrapper         = $(".input_fields_wrapdir"); //Fields wrapper
	    var add_button      = $(".add_field_buttondir"); //Add button ID

	    var wrapper2         = $(".input_fields_wrapshare"); //Fields wrapper
	    var add_button2      = $(".add_field_buttonshare"); //Add button ID

	    var wrapper3         = $(".input_fields_wrapben"); //Fields wrapper
	    var add_button3      = $(".add_field_buttonben"); //Add button ID

	    var wrapper4         = $(".input_fields_wrapperson"); //Fields wrapper
	    var add_button4      = $(".add_field_buttonperson"); //Add button ID

	    $('#item-container-source').show();
    	$('#item-container-source2').hide();
    	$('#new_client_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
	    
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	            $(wrapper).append('<div><br><a href="#" class="close pull-right remove_field"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_dir[]"/></div>'); //add input box
	        }
	    });
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    })

	    var x1 = 1; //initlal text box count
	    $(add_button2).click(function(e1){ //on add input button click
	        e1.preventDefault();
	        if(x1 < max_fields){ //max input box allowed
	            x1++; //text box increment
	            $(wrapper2).append('<div><br><a href="#" class="close pull-right remove_field"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_share[]"/></div>'); //add input box
	        }
	    });
	    
	    $(wrapper2).on("click",".remove_field", function(e1){ //user click on remove text
	        e1.preventDefault(); $(this).parent('div').remove(); x1--;
	    })

	    var x2 = 1; //initlal text box count
	    $(add_button3).click(function(e2){ //on add input button click
	        e2.preventDefault();
	        if(x2 < max_fields){ //max input box allowed
	            x2++; //text box increment
	            $(wrapper3).append('<div><br><a href="#" class="close pull-right remove_field"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_ben[]"/></div>'); //add input box
	        }
	    });
	    
	    $(wrapper3).on("click",".remove_field", function(e2){ //user click on remove text
	        e2.preventDefault(); $(this).parent('div').remove(); x2--;
	    })

	    var x3 = 1; //initlal text box count
	    $(add_button4).click(function(e3){ //on add input button click
	        e3.preventDefault();
	        if(x3 < max_fields){ //max input box allowed
	            x3++; //text box increment
	            $(wrapper4).append('<div><br><label for="new_person">Name of Person(s) representing the Company </label><br><a href="#" class="close pull-right remove_field"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></a><input type="text" class="form-control" name="new_person[]"/><label for="new_personhkid">HKID Card No/Passport No </label><input type="text" class="form-control" name="new_personhkid[]"><label for="new_personpos">Capacity Position </label><input type="text" class="form-control" name="new_personpos[]"><label for="new_personconnum">Contact Number </label><input type="text" class="form-control" name="new_personconnum[]"><label for="new_personemail">Email Address </label><input type="text" class="form-control" name="new_personemail[]"></div>'); //add input box
	        }
	    });
	    
	    $(wrapper4).on("click",".remove_field", function(e3){ //user click on remove text
	        e3.preventDefault(); $(this).parent('div').remove(); x3--;
	    })

	    $('#new_email').focusout(function(){

                $('#new_email').filter(function(){
                   var emil=$('#new_email').val();
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if( !emailReg.test( emil ) ) {
                alert('Please enter valid email');
                } else {
                alert('Thank you for your valid email');
                }
                })
            });
	});


    /* Save New conference */
    $('#new_client_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

        var type=$('select[name="new_clienttype_id"]').val();
        console.log('type='+type);
        var empty_fields = [];
        if(type==1){
			 $(".check").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}else{
			 $(".check2").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}
		if(empty_fields.length>0){
				console.log(empty_fields);
				//toastr.error(empty_fields);
		        toastr.error('All fields are required.');
		        empty_fields.length=0;
		 }else{
		 		var ischecked = $("#new_acting_id").is(":checked"); 
	       		if(ischecked==true)
	       			$('#new_client_main_form').find('.item-container').find('input[name="new_acting_id"]').value='1';
	       		else
	       			$('#new_client_main_form').find('.item-container').find('input[name="new_acting_id"]').value='0';
        
	   

	       $.post('/clients/submitnewclient', $(this).serializeArray(), function(data){

	            console.log('data'+data);
	            if(data.status == 'success') {
	                toastr.success(data.msg);
		            var delay = 3000; //Your delay in milliseconds
		            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
	            } else {
	                toastr.error(data.msg);
	            }
	        });
	   }
    	

    });  

    	
		
</script>
@stop
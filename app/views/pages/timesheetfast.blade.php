@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class='containter'>
    <div class='row'>
        <div class='col-xs-12' style='text-align: center; vertical-align: middle;'>
            <div class='panel panel-default'>
                @if(Input::get('popup'))
                    <form method="post" action="/timesheet/savetimesheets" onsubmit="return verify2()">
                @endif
                <div class='panel-heading' style='text-align: left;'>
                    <a href="#" class='btn btn-info' onclick="additem({ts : {Comment : '', ChargeHour : 0, UniqueID : 0}})">Add Timesheet Entry</a>
                </div>
                    <input type="hidden" name="popup" value="{{ Input::get('popup', 'false') }}">
                    <table class='table' id='timesheet_entry'>
                        <thead>
                            <tr>
                                <th style='text-align: center;'>Date</th>
                                <th style='text-align: center;'>Matter No</th>
                                <th style='text-align: center;'>Handler</th>
                                <th style='text-align: center;'>Description</th>
                                <th style='text-align: center;'>Units</th>
                                <th style='text-align: center;'>Work Hours</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                     @if(Input::get('popup'))
                        <button class='btn btn-info'>Save Timesheets</button>
                    @else
                        <button class='btn btn-info' onclick="verify()">Save Timesheets</button>
                    @endif
                @if(Input::get('popup'))
                    </form>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="_modal_notification___">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <h4 style="color: red;">All Fields are Required. Please check your entry and try again.</h4>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="_saving__">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <h4 style="color: green;">. . . Saving, Please Wait . . .</h4>
            </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="_init__">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <h4 style="color: orange;">. . . Initializing, Please Wait. . .</h4>
            </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script id="row_tmpl" type="text/x-jquery-tmpl">
    <td style='text-align: center; font-size: 10px;'><input class='form-control _date_control' type='text' name='date[]' size="2"/></td>
    <td style='text-align: left;'>
        <select id='matter_${ctr}' name='matter_no[]' class="form-control" style="width: 100%; overflow: hidden;">
            <option value="" selected="selected">Select Matter</option>
         @foreach($obj['special_matters'] as $matter)
            <option value="{{$matter->JobAssignmentID}}">{{ str_pad($matter->JobAssignmentID, 5, '0', STR_PAD_LEFT) }} - {{$matter->Description_A}}</option>
        @endforeach
        @foreach($obj['matters'] as $matter)
            @if($matter->matter_type != 'blue')
                <option value="{{$matter->JobAssignmentID}}">{{$matter->Incharge}} - {{$matter->JobAssignmentID}} - {{$matter->Description_A}}</option>
            @endif
        @endforeach
        </select>
    </td>
    <td style='text-align: left;'>
        <select id='handler_${ctr}'name="handler[]" class="form-control" style="width: 100%; overflow: hidden;">
            <option value="">Select Handler</option>
            @foreach($obj['handler'] as $handler)
                <option data="{{$handler->EmployeeID}}" value='{{json_encode(array("user_id" => $handler->user_id, "EmployeeID" => $handler->EmployeeID))}}'>{{$handler->EmployeeID}} - {{$handler->NickName}}</option>
            @endforeach
        </select>
    </td>
    <td style='text-align: center;'><input class='form-control' type='text' name='description[]' value='${Comment}'/></td>
    <td style='text-align: center;'><input class='form-control' type='text' name='unit[]' onkeyup="compute_hrs()" size="1" value='${Math.round(parseFloat(ChargeHour) * 60 / 5)}'/></td>
    <td><input type="text" name="hours[]" class="form-control" readonly size="1"/><input type='hidden' value='${UniqueID}' name='timesheetnum[]'/></td>
</script>
    <script type="text/javascript">
        var count = 1
        var BillingCodes = {};
        var table = document.getElementById("timesheet_entry");

        $(function(){
            @if(Input::get('popup') ? 0 : 1)
                $.get('/utils/timesheets',function(data){
                    if(data.obj.timesheets.length > 0){
                        for(var i = 0; i < data.obj.timesheets.length; i++)
                            additem(data.obj.timesheets[i]);
                        compute_hrs();
                    }
                    if(data.count == 0)
                        additem({Comment : '', ChargeHour : 0, UniqueID : 0});
                },'json');
            @endif
            
        });

        function _remove(item){
            var num = count;
            count -= 1;
            console.log((num - 1) - item);
            table.getElementsByTagName('tbody')[0].deleteRow((num - 1) - item);
        }
        
        function additem(obj){
            obj.ctr = count;
            var newRow = table.getElementsByTagName('tbody')[0].insertRow(0);
            $(newRow).html($("#row_tmpl").tmpl(obj));
            $( "._date_control" ).datepicker({
                maxDate: 0
            });
            $( "._date_control" ).datepicker("option","dateFormat","M dd, yy").datepicker('setDate',  new Date());
            if({{Input::get('popup') ? 1 : 0}}){
                $("#matter_" + count).val({{Input::get('matter_id')}});
                obj.EmployeeID = "{{User::getEmployeeID(Session::has('principal_user') ? Session::get('principal_user') : Auth::user()->user_id)}}"
            }
            else
                $("#matter_" + count).val(obj.JobAssignmentID);
            $("#handler_" + count + " option" ).each(function( index ) {
                if($( this ).text().search(obj.EmployeeID) >=0){
                    $("#handler_" + count ).val($( this ).val());
                    return false;
                }
            });
            $('select').selectize();
            count += 1; 
        }
        
        function compute_hrs(){ 
            for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                document.getElementsByName("hours[]")[i].value = parseFloat((parseInt(document.getElementsByName("unit[]")[i].value) * 5) / 60).formatMoney(2);
            }
        }
        
        function verify2(){
           var success = true; var unit = ''; var description = ''; var matter_no = '';
           var handler = ''; var date = '';
           for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                unit = document.getElementsByName("unit[]")[i].value;
                description =  document.getElementsByName("description[]")[i].value;
                matter_no = document.getElementsByName("matter_no[]")[i].value;
                handler = document.getElementsByName("handler[]")[i].value;
                date = document.getElementsByName("date[]")[i].value;
                if(!unit || parseInt(unit) <= 0 || !description || !matter_no || parseInt(matter_no) <= 0 || !handler || !date){
                    success = false;
                    $("#_modal_notification___").modal().show();
                    break;
                }
            }
            if(success)
                $("#_saving__").modal().show();
            return success;
        }
        
        function verify(){
           var success = true; var unit = ''; var description = ''; var matter_no = '';
           var handler = ''; var date = '';
           for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                unit = document.getElementsByName("unit[]")[i].value;
                description =  document.getElementsByName("description[]")[i].value;
                matter_no = document.getElementsByName("matter_no[]")[i].value;
                handler = document.getElementsByName("handler[]")[i].value;
                date = document.getElementsByName("date[]")[i].value;
                if(!unit || parseInt(unit) <= 0 || !description || !matter_no || parseInt(matter_no) <= 0 || !handler || !date){
                    success = false;
                    $("#_modal_notification___").modal().show();
                    break;
                }
            }
            
            if(success){
                $("#_saving__").modal().show();
                var timesheetnum = [];
                var matter_no = [];
                var description = [];
                var unit =[];
                var handler = [];
                var date = [];

                for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                    timesheetnum.push(document.getElementsByName("timesheetnum[]")[i].value);
                    matter_no.push(document.getElementsByName("matter_no[]")[i].value);
                    description.push(document.getElementsByName("description[]")[i].value);
                    unit.push(document.getElementsByName("unit[]")[i].value);
                    handler.push(document.getElementsByName("handler[]")[i].value);
                    date.push(document.getElementsByName("date[]")[i].value);
                }

                var data = {
                    timesheetnum    : timesheetnum,
                    matter_no       : matter_no,
                    description     : description,
                    unit            : unit,
                    handler         : handler,
                    date            : date,
                    popup           : 0
                };
                
                $.post("/timesheet/savetimesheets",data,function(info){
                    console.log(info);
                    $("#_saving__").modal("hide");
                });
            }
        }

        @if(Input::has('popup'))
        window.onunload = function() {
            var win = window.opener;
            if (win.closed) {
                win.closeTimesheetCreate();
            }
        };            
        @endif

    </script>
@stop
@extends('template/default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3><span class="glyphicon glyphicon-wrench"></span> Sysadmin - Who is Online?</h3>
        </div>
        @include('pages/common/sysadmin_menu')
    </div>    
    <div class="row top-buffer">    
        <div class="col-xs-12">
            <table class="table table-bordered table-striped" id="user_table">
                <thead>
                    <tr>
                        <th style="text-align:center">User</th>
                        <th style="text-align:center">Name</th>
                        <th style="text-align:center">Last Activity</th>
                        <th style="text-align:center">IP</th>
                        <th style="text-align:center">Location</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($usersonline as $user): ?>
                    <tr data-employee-id="<?php echo $user->user_id?>">
                        <td style="text-align:center"><?php echo $user->Photo != null ? '<img class="img-thumbnail" style="width:75px" src="/assets/thisapp/images/staff/'.$user->Photo.'">':'NA' ?></td>
                        <td style="text-align:center"><?php echo $user->EmployeeID.' - '.$user->first_name ?></td>
                        <td style="text-align:center"><?php echo date('F d h:i A',$user->last_activity) ?></td>
                        <td style="text-align:center"><?php echo $user->ip_address ?></td>
                        <td style="text-align:center">
                            <?php
                                if($user->ip_address != ''){
                                    if(Helper::reserved_ip($user->ip_address) == false){
                                        $location = Helper::geoCheckIP($user->ip_address);
                                        echo $location['country'];
                                        echo $location['state'] != 'not found' ? ', '.$location['state'] : '';
                                        echo $location['town'] != 'not found' ? ', '.$location['town'] : '';
                                    } else {
                                        echo 'Office LAN';
                                    }
                                } else {
                                    echo 'NA';
                                }
                            ?>
                        </td>
                        <td style="text-align:center"><button class="btn btn-info btn-sm view-login-history">View User Login History</button></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>        
    </div>
</div>
@stop

@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;New Matter 
			</h2>
			<div class="modal-content" id="new_matter_log2">
				<form role="form" method="post" action="/matters/save">
					<div class="modal-body">
			
						<div class="row">
					<div class="col-md-6">
                    <label style="float: left;"><input name="ip_matter_box" type="checkbox" id="_ip_matter_option"> IP Matter</label>
                    <label style="float: right;"><input name="new_client_box" type="checkbox" id="_new_client_option" onchange="__toggle_client()"> New Client</label>
                    <div class='row'>
                        <div class="col-md-12 form-group">
                            <label for="new_designated_date">Date  <small>(click to select date)</small></label>
                            <input type="text" class="form-control _date_control" name="new_designated_date" required>
                        </div>
                        <div class="col-md-12 form-group" id='div_for_existing_client' style='display: inherit;'>
                            <label for="__current_client">Select Client</label>
                            <select class="form-control" name="current_client" id='__current_client'>
                                @foreach($client_list as $client)
                                    <option value="{{ $client->CustomerID }}">{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
                                @endforeach
                            </select>
						</div>
                        <div class="col-md-12 form-group" id='div_for_new_client' style='display: none;'>
                            <div class="row">
                                <div class="col-md-9">
                                    <label for="_new_client_name">Client Name</label>
                                <input type="text" class="form-control" name="_new_client_name"  id="_new_client_name" required="false" disabled="true">
                                
                                </div>
                                <div class="col-md-3" style="text-align: left;">
                                    <label for="_check_client" style="color: white;">Verify Client</label>
                                    <a  href="#" class="btn btn-info" id="_check_client" onclick="verify_client()">Check Client</a>
                                </div>
                            </div>
						</div>
                                            
                        <div class="col-md-12 form-group" style='display: inherit;'>
                            <label for="beneficial_owner">Select Beneficial Owner</label>
                            <select class="form-control" name="beneficial_owner" required>
                                <option value="same">Current Client</option>
                                @foreach($client_list as $client)
                                    <option value="{{ $client->CustomerID }}">{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
                                @endforeach
                            </select>
						</div>
                                            
                        <div class="col-md-12 form-group">
							<label for="fixed_amount___">Fixed Amount</label>
                                <label style="float: right;"><input name="fix_fee" type="checkbox" id="_fix_fee_check" onchange="_toggle_fix()"> Fix Fee</label>
                                <input type="text" class="form-control" name="fixed_amount___"  id="fixed_amount___" disabled="true" required="false">
						</div>
                                                
                                                <div class="col-md-12 form-group">
							<label for="deposit___">Cost on Account (Deposit)</label>
                                          
                                <input type="text" class="form-control" name="deposit___"  id="deposit___">
						</div>
                                                
                                                <div class="col-md-12 form-group">
							<label for="introducer__">Introducer (Handling Partner)</label>
						
							<select class="form-control" name="introducer__" id="introducer__" placeholder="Select Introducer" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName}}</option>
									@endforeach
						     </select>
						</div>
							
						<div class="col-md-12 form-group">
							<label for="new_handler_id">Handler</label>
							<select class="form-control" name="new_handler_id" id="handler" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName}}</option>
									@endforeach
						     </select>
						</div>

                        <div class="col-md-12 form-group">
                            <label for="new_matter_description">Description</label>
                            <textarea class="form-control" name="new_matter_description" rows="3" required></textarea>
                        </div>

                        <div class="col-md-12 form-group">
                            <label for="new_matter_description">Special Client Instructions</label>
                            <textarea class="form-control" name="client_instruction" rows="3" required></textarea>
                        </div>
                                            

                    </div>

						
					</div>
					<div class="col-md-6">
                        <div class="row">

                        <div class="col-md-12 form-group">
                            <div class="panel panel-default">
                              <div class="panel-heading">
                                <h3 class="panel-title" style="color: blue;">Matter Specific Client Information</h3>
                              </div>
                              <div class="panel-body">
                                    <div class="col-md-12 form-group">
                                        <label for="__client_">Client</label>
                                        <input type="text" class="form-control" name="__client_"/>
                                    </div>
                                   <div class="col-md-12 form-group">
                                        <label for="__address_">Address</label>
                                        <input type="text" class="form-control" name="__address_"/>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="__tel_">Telephone No.</label>
                                        <input type="text" class="form-control" name="__tel_"/>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="__email_">Email</label>
                                        <input type="text" class="form-control" name="__email_"/>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="__fax_">Fax</label>
                                        <input type="text" class="form-control" name="__fax_"/>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="__attn_">Attn:</label>
                                        <input type="text" class="form-control" name="__attn_"/>
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label for="__re_">Re</label>
                                        <input type="text" class="form-control" name="__re_"/>
                                    </div>
                              </div>
                            </div>
                        </div>
       
                                                
                        </div>
					</div>
				</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade"  aria-hidden="true" id="client_search_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Verify New Client</h4>
      </div>
      <div class="modal-body">
          <div class='row'  id='client_result_body' style='max-height: 500px; overflow-x: hidden; overflow-y: scroll; text-align: center;'><h4>Keyword Not Found</h4></div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary">View Results on Clients' Page</button> -->
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script id='search_result_tmpl' type='text/x-jquery-tmpl'>
<div class='col-md-2'>${CustomerID}</div>
<div class='col-md-10' style="text-align: left;">@{{html _mark(CompanyName_A1)}}</div>
</script>

<script>
    function __toggle_client(){
        if($("#_new_client_option").is(":checked")){
            $("#div_for_new_client").show();
            $("#div_for_existing_client").hide();
            document.getElementById('_new_client_name').required = true;
            document.getElementById('_new_client_name').disabled = false;
            document.getElementById('__current_client').required=false;
            //document.getElementById('__current_client').disabled=true;
        }
        else{
            $("#div_for_new_client").hide();
            $("#div_for_existing_client").show();
            document.getElementById('_new_client_name').required = false;
            document.getElementById('_new_client_name').disabled = true;
            document.getElementById('__current_client').required=true;
            //document.getElementById('__current_client').disabled=false;
        }
    }
    
    function _mark(name){
        name = name.trim();
        var target = $("#_new_client_name").val().trim();
        var loc = name.toLowerCase().search(target.toLowerCase());
        return name.substring(0,loc) + '<mark>' + name.substring(loc,loc + target.length) + '</mark>' + name.substring(loc + target.length,name.length);
    }
    
    function _toggle_fix(){
        if($("#_fix_fee_check").is(":checked")){
            document.getElementById('fixed_amount___').disabled = false;
            document.getElementById('fixed_amount___').required = true;
        }
        else{
            document.getElementById('fixed_amount___').disabled = true;
            document.getElementById('fixed_amount___').required = false;
        }
    }
    
    function verify_client(){
        if($("#_new_client_name").val().length > 0){
            $.get('/clients/search/' + $("#_new_client_name").val(),function(data){
                if(data.length > 0)
                    $("#client_result_body").html( $("#search_result_tmpl").tmpl(data));
                $("#client_search_modal").modal().show();
            },'json');
        }
    }

 
   $(document).ready(function() {
        $('select').selectize();
        $( "._date_control" ).datepicker({
            maxDate: 0
        });
        $( "._date_control" ).datepicker("option","dateFormat","M dd, yy").datepicker('setDate',  new Date());
    });
    
</script>
@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/assets/jquery-ui-1.10.4/css/jquery-ui-timepicker-addon.css">
<link rel="stylesheet" href="assets/chosen/chosen.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
  <script src="assets/chosen/chosen.jquery.js"></script>
  <script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>

<style>
.conference1 {
    padding: 0;
}

#calendar2{
   padding-top:30px;
    width: 230px;
    height:900px;
    margin: 0 auto;
    font-size: 10px;

}
#calendar {
    padding-top:30px;
    width: 430px;
    height:500px;
    margin: 0 auto;
    font-size: 10px;
}
.fc-header-title h2 {
    font-size: .9em;
    white-space: normal !important;
}

.fc-event-time,
.fc-event-title {
    padding: 0 1px;
    float: left;
    clear: none;
    margin-right: 10px;
}

.fc-view-month .fc-event {
    width: 100%; 
    height: 50px;
    position: absolute;
    bottom:0;
     overflow: hidden;
     font-size: 10;
    }

    .offset-0 {
    padding-left: 0;
    padding-right: 0;
}


/*.fc-view-month .fc-event, .fc-view-agendaWeek .fc-event {
    font-size: 10;
    overflow: hidden;
    height: 2px;
}
.fc-view-agendaWeek .fc-event-vert {
    font-size: 10;
    overflow: hidden;
    width: 2px !important;
}
.fc-agenda-axis {
    width: 20px !important;
    font-size: 1em;
}

.fc-button-content {
    padding: 0;
}*/
</style>
<div class="container-fluid">
  
  @if(Session::has('message.success.send_reminder')) 
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        {{ Session::get('message.success.send_reminder') }}
      </div>      
    </div>
  </div>
  @endif  

    <!-- Floorplan V2 -->
    <div class="row top-buffer">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="height:52px">
                    <h3 class="panel-title">{{ Lang::get('lang.conferenceroom.The office') }}</h3>
                    <form method="get"><div id="new_conference_room_reservation_main_form" style="margin-top:-25px;margin-left:45px;"><center><table><tr><td>
                      <div class="col-sm-12"><label for="new_reservation_date">{{ Lang::get('lang.conferenceroom.Date (click to select date)') }}</label></div></td><td></td><td> 
                      <div class="col-sm-10"><input type="text" class="form-control" id="datetimepicker2" name="new_reservation_date[]"  readonly required></div></td>
                      <td colspan='2'><div class="col-sm-2"><button type="button" class="btn btn-info btn-sm pull-right btn-primary_reservation" id="today">{{ Lang::get('lang.conferenceroom.Today') }}</button></div></td></tr></table></center></div>
                      </form>
                </div>
                <div class="col-md-12 form-group"></div>
                <!--<form role="form" method="get">
                <div class="modal-body">
                      <div class="row" id="new_conference_room_reservation_main_form">
                          <div class="col-md-3 form-group">
                          <label for="new_reservation_date">Date  <small>(click to select date)</small></label>
                            <input type="text" class="form-control" id="datetimepicker2" name="new_reservation_date[]"  readonly required>
                            
                         </div>
                          <div class="col-md-3 form-group">
                            
                             <button type="button" class="btn btn-primary_reservation" id="today" style="margin-top:25px;">Today</button>
                             
                          </div>

                      </div>
                </div>

                </form>-->
               
                <div class="col-md-12 form-group">
                <h3 id="room-name">&nbsp;</h3>
               
                </div>
               <div class="panel-body">
                    <!--<div id="floorplan"></div>-->
                    <div class="container-fluid">
                    <div class="row">
                      <div class="col-md-5 col-sm-5 col-xs-5 offset-0">
                        <h3 style="padding:10px;">{{ Lang::get('lang.conferenceroom.Conference Room 1') }}</h3>
                        <div style="position:absolute;margin:2% 5% 2% 5%; width:95%;" id="c1"></div>
                        <div class="c1Click"><img src="/assets/thisapp/images/office/cr1-bg.png" alt="Conference Room 1" title="Conference Room 1" class="img-responsive" style="position:relative;"></div>
                      </div>

                      <div class="col-md-3 col-sm-3 col-xs-3 offset-0">
                        <h3 style="padding:10px;">{{ Lang::get('lang.conferenceroom.Conference Room 2') }}</h3>
                        <div style="position:absolute;margin:3% 6% 3% 6%; width:95%;" id="c2"></div>
                        <div class="c2Click"><img src="/assets/thisapp/images/office/cr2-bg.png" alt="Conference Room 2" title="Conference Room 2" class="img-responsive" style="position:relative;"></div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-4 offset-0">
                        <h3 style="padding:10px;">{{ Lang::get('lang.conferenceroom.Conference Room 3') }}</h3>
                        <div style="position:absolute;margin:2% 5% 2% 5%; width:95%;" id="c3"></div>
                        <div class="c3Click"><img src="/assets/thisapp/images/office/cr3-bg.png" alt="Conference Room 3" title="Conference Room 3" class="img-responsive" style="position:relative;"></div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4 offset-0">
                        <h3 style="padding:10px;">{{ Lang::get('lang.conferenceroom.China Room') }}</h3>
                        <div style="position:absolute;margin:2% 5% 2% 5%; width:95%;" id="c4"></div>
                        <div class="c4Click"><img src="/assets/thisapp/images/office/cr4-bg.png" alt="Conference Room 4" title="Conference Room 4" class="img-responsive" style="position:relative;"></div>
                      </div>
                      <!--<div class="col-md-4 col-sm-4 col-xs-4 offset-0">
                        <h3 style="padding:10px;">Conference Room 5</h3>
                        <div style="position:absolute;margin:2% 5% 2% 5%; width:95%;" id="c5"></div>
                        
                        <div class="c5Click"><img src="/assets/thisapp/images/office/cr5-bg.png" alt="Conference Room 5" title="Conference Room 5" class="img-responsive" style="position:relative;"></div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-4 offset-0"></div>-->
                    </div>


                  </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Floorplan V2 -->
</div>

<!--calendar modal-->
<div class="modal fade"  id="complete_confirmation1"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-lg" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log1">
       <div class="row">
              <div class="col-md-6 row">
                    <div id='calendar2'></div>
              </div>
              <div class="col-md-6 row">
                    <div id='calendar'></div>
              </div>
          </div>
    </div>
</div>
</div>


<div class="modal fade"  id="complete_confirmation2"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-lg" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <div class="row">
              <div class="col-md-12 row">
                    <div id='calendar3'></div>
              </div>
             
          </div>
    </div>
</div>
</div>

<!--modals-->
<div class="modal fade" data-backdrop="static" id="complete_confirmationu" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">Updating Calendar...Please wait <span class="matter_for_completion"></span></h4>
      </div>
    </div>
  </div>
</div>

<!--alert-->
<div class="modal fade"  id="complete_confirmation3"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-sm" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <div class="row">
               
                      <div class="col-md-12 row">
                        <center><h5><i class="fa fa-info-circle text-info"></i> What do you want to do?</h5>
                        <button type="button" id="del" class="btn btn-info">Delete</button>
                        <button type="button" id="upd" class="btn btn-info">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>
                        <div><br></div>
                      </div>   
              
              </div>
          </div>
    </div>
</div>
</div>

<div class="modal fade"  id="complete_confirmation4"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-sm" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <center><div class="row">
           
                    <div class="col-md-12 row">
                      <h5><i class="fa fa-info-circle text-info"></i> Are you sure?</h5>
                      <form role="form" method="post" id="formmodal">
                        <input id="eventid1" name="eventidname" type="hidden" >
                        <button type="button" id="dely" class="btn btn-info">Yes</button>
                       
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>   
                      </form>  
                        <div><br></div>
                    </div>
          </div></center> 
    </div>
</div>
</div>
 

<!--other modal-->
<div class="modal fade"  id="complete_confirmation" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content" id="new_conference_room_reservation_log2">
        <form role="form" method="post">
          <div class="modal-body">
            <div class="row" id="new_conference_room_reservation_main_form">
            <div id="item-container-source" style="display:block">
            <div class="col-md-12 item-container">
              <div class="panel panel-default">
                <div class="panel-heading clearfix">
                <button type="button" class="close pull-right delete-item_reservation" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 row">
                        <div class="col-md-12 form-group">
                         <input id="conferenceid" name="conferenceid" type="hidden" >
                         <input id="eventid" name="eventid" type="hidden" >
                              <!--<label class="control-label" for="reservationtime">Choose your start-time and end times:</label>
                              <div class="controls">
                               <div class="input-prepend input-group">
                                 <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                 <input type="text" style="width: 400px" name="new_time[]" id="reservationtime" class="form-control"   class="span4"/>
                               </div>
                              </div>-->
                        
                        </div>
                   
                         <div class="col-md-12 form-group">
                         <label for="new_start_time">{{ Lang::get('lang.conferenceroom.Start Time') }}  <small>{{ Lang::get('lang.conferenceroom.(click to select time)') }}</small></label>
                          <input type="text" class="form-control" id="new_start_time" name="new_start_time[]" readonly required>
                        </div>
                        <!--<div id="datetimepicker" class="input-append date">
                          <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_start_time" name="new_start_time[]" readonly required>
                          <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                          </span>
                        </div>-->
                        <div class="col-md-12 form-group">
                          <label for="new_end_time">{{ Lang::get('lang.conferenceroom.End Time') }}  <small>{{ Lang::get('lang.conferenceroom.(click to select time)') }}</small></label>
                          <input type="text" class="form-control" id="new_end_time" name="new_end_time[]" readonly required>
                        </div>
                      <div class="col-md-12 form-group">
                         
                          <label for="reservedby">{{ Lang::get('lang.conferenceroom.Reserved By') }}</label>
                          <input type="hidden" class="form-control" id="cond" value="{{$condition}}">
                         <select data-placeholder="Select Handler" class="chosen" style="width:350px;" tabindex="2" id="reservedby2" name="reservedby[]">
                           <option value=""></option>
                            @foreach($handler_list as $handler)
                            <option value="{{ $handler->EmployeeID }}" {{ $condition  == $handler->EmployeeID ? 'selected' : '' }} >{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
                            @endforeach
                          </select>
                          <!--<select class="form-control"  id="reservedby2" name="reservedby[]" placeholder="Select Handler" required>
                            <option></option>

                            @foreach($handler_list as $handler)
                            <option value="{{ $handler->EmployeeID }}" {{ $condition  == $handler->EmployeeID ? 'selected' : '' }} >{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
                            @endforeach
                          </select>-->
                      </div>
                      <div class="col-md-12 form-group">
                        <label for="new_purpose">{{ Lang::get('lang.conferenceroom.Purpose') }}</label>
                        <textarea class="form-control" id="purpose" name="new_purpose[]" rows="3" required></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>

                      <div class="col-md-12 form-group">
                        <label for="new_notes">{{ Lang::get('lang.conferenceroom.Notes') }}</label>
                        <textarea class="form-control" id="notes" name="new_notes[]" rows="3"></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>
                     <!-- <div class="col-md-12 form-group">
                        <label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
                        <input type="text" class="form-control" name="new_reservation_date[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_start_time[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_end_time[]" readonly required>
                      </div>
                    </div>-->
                    
         
                </div>
              </div>  
            </div>
          </div>
            </div>
            <div class="col-md-12">
              <!--<button class="btn btn-info btn-sm" id="btn_add_item2_reservation">Add Item</button>-->
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('lang.conferenceroom.Cancel') }}</button>
            <button type="submit" class="btn btn-primary_reservation">{{ Lang::get('lang.conferenceroom.Save') }}</button>
          </div>
           </div>
           </div>
        </form>
  </div>
</div>
</div>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<link rel='stylesheet' type='text/css' href='/assets/fullcalendar-1.5.4/fullcalendar/fullcalendar.css' />

<!--<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
<script type='text/javascript' src='/assets/fullcalendar-2/dist/fullcalendar.js'></script>
<link rel="stylesheet" type="text/css" href="/assets/fullcalendar-2/dist/fullcalendar.css" />
<!--<script type='text/javascript' src='/assets/fullcalendar-1.5.4/fullcalendar/fullcalendar.min.js'></script>-->
<!--<script type="text/javascript" src="/assets/thisapp/js/raphael-min.js"></script>-->
<!--<script type="text/javascript" src="/assets/thisapp/js/floorplan.js"></script>-->
<script type="text/javascript" src="/assets/thisapp/js/confroom.js"></script>
<script type="text/javascript" src="/assets/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js"></script>
<!--<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="/assets/datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="/assets/datetimepicker-master/jquery.js"></script>
<script src="/assets/datetimepicker-master/jquery.datetimepicker.js"></script>-->
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>

<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/bootstrap-daterangepicker-master/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="/assets/qtip/jquery.qtip.css" />
<script type="text/javascript" src="/assets/qtip/jquery.qtip.js"></script>
<!--<link type="text/css" rel="stylesheet" href="/assets/jquery-dropdown-master/jquery.dropdown.css" />
<script type="text/javascript" src="/assets/jquery-dropdown-master/jquery.dropdown.js"></script>-->

<script>

 $('#reservedby2').selectize();

 $('#new_conference_room_reservation_log2 form').submit(function(e){
        e.preventDefault();
        tempArray=$(this).serializeArray();

    $.post('/conference-room-reservation/submitnewreservation', tempArray, function(data){
                    
      $.post('/conference-room-reservation/savenewreservation2', tempArray, function(data){
                                    // console.log(data);
        if(data.status == 'success') {
              toastr.success(data.msg);
              var delay = 3000; //Your delay in milliseconds
              setTimeout(function(){ window.location = '{{ URL::to('/conference-room') }}'; }, delay);                
        } else {
                toastr.error(data.msg);
        }
      });


});
              
            
}); 



 $(document).on('click touchstart', '#dely', function(e){
        e.preventDefault();
      tempArray=$('#formmodal').serializeArray();
      $('#eventidname').val($('#eventid').val());

        //console.log(tempArray);
      $.post('/conference-room-reservation/deletereservation', $('#formmodal').serializeArray(), function(data){
                                // console.log(data);
                                if(data.status == 'success') {
                                    toastr.success(data.message);
                                  var delay = 3000; //Your delay in milliseconds
                                  setTimeout(function(){ window.location = '{{ URL::to('/conference-room') }}'; }, delay);                
                                } else {
                                    toastr.error(data.msg);
                                }
                        });
  
});



</script>
@stop
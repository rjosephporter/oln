@if(count(Session::get('employee_switch_options')) > 1)
<!--
<div class="row">
    <div class="col-md-offset-9 col-md-3">
-->
        <form class="form-horizontal" method="post" action="{{ URL::to('/') }}/auth/change-principal-user" role="form" style="padding: 13px 0px">
            <div class="form-group no-bottom-margin" style="margin-right:-35px;margin-top:0px;">
                <label for="principal_user" class="col-md-4 control-label" style="color: white;margin-right:-5px;">View&nbsp;as</label>
                <div class="col-md-8">
                    <div style="width:87px;">{{
                        Form::select('principal_user', Session::get('employee_switch_options'), Session::get('principal_user'), array('class' => 'form-control', 'onchange="this.form.submit()"'))
                    }}</div>
                </div>
            </div>
        </form>            
<!--
    </div>
</div>
-->
@endif
<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog {{ $modal_dialog_class or '' }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ $modal_title }}</h4>
      </div>
      <form id="{{ $modal_form_id }}" role="form" method="{{ $modal_form_method or 'post' }}" action="{{ $modal_form_action or Request::url() }}">
      <div class="modal-body">
        @include($modal_content_view, $modal_content_data)         
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>
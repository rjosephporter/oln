  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="/"><span class="hidden-sm hidden-xs">{{ Lang::get('lang.common.header.Office Efficiency Management System') }}</span><span class="hidden-md hidden-lg">{{ Config::get('oln.app_name_abbr') }}</span></a>
    @if ( ! is_null(Request::segment(1)) && Request::segment(1) !== 'auth' )
    <div class="navbar-text popover-markup" >
        <a href="#" class="text-success navbar-link trigger" style="position:absolute;">{{ ucwords(Request::segment(1)) }}&nbsp;&nbsp;<span class="caret"></span></a>
        <div class="content hide">
           
        <!--Customized POPOVER (LEO) -->

        <div class="row">
            @if(User::find(Session::get('principal_user'))->can('dashboard_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-dashboard large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/dashboard" class="nostyle">{{ Lang::get('lang.home.Dashboard') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('office_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                        <div class="nav-button">
                            <div class="nav-glyph-container">
                                <span><img src="/assets/thisapp/images/office/fp3.png" height="40"/></span>
                            </div>
                        </div>
                        <h5 class="text-center"><a href="/office" class="nostyle">{{ Lang::get('lang.home.Office') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('matters_access'))
                 <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-folder-open large-glyph"></span>
                        </div>
                    </div>
                        <h5 class="text-center"><a href="/matters" class="nostyle">{{ Lang::get('lang.home.Matters') }}</a></h5>
                 </div>
            @endif
            @if(User::find(Session::get('principal_user'))->can('ip_matters_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon large-glyph">IP</span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/matters?type=ip" class="nostyle">{{ Lang::get('lang.home.IP Matters') }}</a></h5>
                </div>
            @endif
        </div>

        <div class="row padded-row-format">
            @if(User::find(Session::get('principal_user'))->can('handlers_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <!--<span class="glyphicon glyphicon-user large-glyph"></span>-->
                            <span><img src="/assets/thisapp/images/app/lawyer.png" height="40"/></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/handlers" class="nostyle">{{ Lang::get('lang.home.Handlers') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('clients_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-certificate large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/clients" class="nostyle">{{ Lang::get('lang.home.Clients') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('comm_center_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-retweet large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/commcenter" class="nostyle">{{ Lang::get('lang.home.Comm Center') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('organization_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <i class="fa fa-users" style="font-size: 35px;"></i>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/orgchart" class="nostyle">{{ Lang::get('lang.home.Organization') }}</a></h5>
                </div>
            @endif 
        </div>

        <div class="row padded-row-format">
            @if(User::find(Session::get('principal_user'))->can('timesheet_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-time large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/timesheet" class="nostyle">{{ Lang::get('lang.home.Timesheet') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('invoice_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-th-list large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/invoice" class="nostyle">{{ Lang::get('lang.home.Invoice') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('reports_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-list-alt large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/reports" class="nostyle">{{ Lang::get('lang.home.Reports') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('accounting_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="fa-stack fa-lg" style="font-size: 22px;">
                                <i class="fa fa-file-o fa-stack-2x"></i>
                                <i class="fa fa-usd fa-stack-1x"></i>
                            </span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/accounting" class="nostyle">{{ Lang::get('lang.home.Accounting') }}</a></h5>
                </div>
            @endif

        </div>

        <div class="row padded-row-format">
            @if(User::find(Session::get('principal_user'))->can('sysadmin_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <span class="glyphicon glyphicon-wrench large-glyph"></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/sysadmin" class="nostyle">{{ Lang::get('lang.home.System Admin') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('conference_room_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                            <div class="nav-glyph-container">
                                <!-- <span class="glyphicon glyphicon-home large-glyph"></span> -->
                                <span><img src="/assets/thisapp/images/app/meeting.png" height="40"/></span>
                            </div>
                        </div>
                        <h5 class="text-center"><a href="/conference-room" class="nostyle">{{ Lang::get('lang.home.Conference Room') }}</a></h5>
                </div>
            @endif

            @if(User::find(Session::get('principal_user'))->can('hr_access'))
                <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format">
                    <div class="nav-button">
                        <div class="nav-glyph-container">
                            <!-- <span class="glyphicon glyphicon-home large-glyph"></span> -->
                            <span><img src="/assets/thisapp/images/app/hr.png" height="40"/></span>
                        </div>
                    </div>
                    <h5 class="text-center"><a href="/employee" class="nostyle">{{ Lang::get('lang.home.HR') }}</a></h5>
                </div>
            @endif
            <div class="col-md-3 col-sm-3 col-xs-3 auto-text-format"></div>
        </div>


      </div>
    </div>
    @endif
    
  </div>
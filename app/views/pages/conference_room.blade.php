@extends('template/default')

@section('header-custom-css')

@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="page-header" style="margin-top:20px">
			<h1>Conference Room <small>Reservation List</small></h1>
		</div>

		<div class="col-md-12" style="margin-bottom:30px">
			<form class="form-inline" role="form" method="get">
				<div class="form-group">
					<label for="date">Date <small>(click to change)</small></label>
					<input type="text" class="form-control" id="date" name="date" placeholder="Select Date" value="{{ Input::get('date', date('F j, Y')) }}" readonly>
				</div>
				<button type="submit" class="btn btn-default">Apply</button>
			</form>	
			<div class="form-group"></div>				
			<a href="{{ URL::to('/conference-room/new') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add new</a>		
		</div>

		@foreach($conference_rooms as $room)
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">{{$room->name}}</h3>
				</div>				
				<div class="panel-body">
				@if(count($room->reservation) == 0)
					<span class="text-info"><i class="fa fa-info"></i> No reservations for today</span>
				@endif					
				<table class="table">
					@foreach($room->reservation as $reservation)
					<tr>
						<td><small>{{$reservation->start_time}} - {{$reservation->end_time}}</small></td>
						<td>{{$reservation->purpose}}</td>
					</tr>
					@endforeach
				</table>
				</div>
				<div class="panel-footer text-right">
					<a href="{{ URL::to('/conference-room/reservation?roomid='.$room->id) }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>Add Reservation</a>
					<!--<button class="btn btn-info btn-sm"><i class="fa fa-plus"></i>  Add Reservation</button>-->
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>

<!-- MODALS -->
{{-- @include('common/modal_wrapper', $modal_data) --}}
<!-- /MODALS -->

@stop

@section('footer-custom-js')
<script>
$('#date').datepicker({
	dateFormat: "MM d, yy"
});
</script>
@stop
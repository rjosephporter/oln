@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>
      <span class="fa-stack fa-lg" style="font-size: 22px;">
        <i class="fa fa-file-o fa-stack-2x"></i>
        <i class="fa fa-usd fa-stack-1x"></i>
      </span>
       Accounting [NOT YET COMPLETE] <span id="batch_text"></span> <button class="btn btn-info btn-sm pull-right" style="display:none" data-toggle="modal" data-target="#invoice-legend-modal"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Show Legend</button></h1>
			<div class="row" id="daterange-wrapper" style="margin-bottom: 15px">
				<div class="col-md-12" style="display:none">
					<form id="daterange-form" class="form-inline" role="form" method="get">
						<div class="form-group">
							<label for="from_date">From</label>
							<input type="text" class="form-control" id="from_date" placeholder="Choose Date">
						</div>
						<div class="form-group">
							<label for="to_date">To</label>
							<input type="text" class="form-control" id="to_date" placeholder="Choose Date">
						</div>
            <div class="form-group">
              <label for="invoice_payment_status">Status</label>
              <select class="form-control" id="invoice_payment_status" placeholder="Select Invoice Status">
                <option value="-1" selected>All</option>
                <option value="white">Paid</option>
                <option value="green">Unpaid for less than 30 days</option>
                <option value="yellow">Unpaid for 30 days and above</option>
                <option value="orange">Sent 1st Reminder</option>
                <option value="red">Sent 2nd Reminder</option>
              </select>              
            </div>
						<button type="submit" class="btn btn-default">Apply</button>
					</form>
				</div>
			</div>
			<div id="invoice-wrapper">
            {{ 
                Datatable::table()
                    ->addColumn('Matter ID', 'Description', 'Client ID', 'Client Name', 'Total Invoice', 'Total Balance')
                    ->setClass('table table-bordered table-striped cell-border')
                    ->noScript()
                    ->render() 
            }}
        	</div>
		</div>
	</div>
</div>

<!-- Modals -->
<div class="modal fade" id="invoice-pay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Invoice: <span id="invoice-pay-batch-number"></span></h4>
      </div>
      <form id="invoice-pay-form" role="form" method="post">
      <div class="modal-body">
        
        <h5>Mark this invoice as paid?</h5>

        <input type="hidden" id="payment_batch_number" name="payment_batch_number">
		
        <div class="form-group">
        	<label for="payment_date">Payment Date</label>
        	<input type="text" class="form-control" id="payment_date" name="payment_date" placeholder="Choose date">
        </div>
        <div class="form-group">
        	<label for="payment_amount">Payment Amount</label>
        	<div class="input-group">
        		<span class="input-group-addon">$</span>
        		<input type="number" step="any" class="form-control" id="payment_amount" name="payment_amount" placeholder="0.00">
        	</div>		    		   
        </div>
        
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-success">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="invoice-delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Invoice: <span id="invoice-delete-batch-number"></span></h4>
      </div>
      <form id="invoice-delete-form" role="form" method="post">
      <div class="modal-body">
        
        <h5><i class="fa fa-warning text-danger"></i> Are you sure you want to delete this invoice?</h5>

        <input type="hidden" id="delete_batch_number" name="delete_batch_number">
          
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Invoice: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
        
        <h5><i class="fa fa-info-circle text-info"></i> Send reminder to handler?</h5>

        <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
        <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">

        <div class="form-group">
          <label for="reminder_recipient">To</label>
          <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Handler's Email" required readonly>
        </div>

        <div class="form-group">
          <label for="reminder_message">Message</label>
          <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
          </textarea>
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="invoice-legend-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Legend</h4>
      </div>
      <!-- <form id="invoice-delete-form" role="form" method="post"> -->
      <div class="modal-body">
        
        <table class="table table-bordered">
          <thead>
            <th>Icon</th>
            <th>Description</th>
          </thead>
          <tbody>
            <tr>
              <td class="text-center white-background">&nbsp;</td>
              <td>Paid</td>
            </tr>
            <tr>
              <td class="text-center green-background">&nbsp;</td>
              <td>Unpaid for less than 30 days</td>
            </tr>
            <tr>
              <td class="text-center yellow-background">&nbsp;</td>
              <td>Unpaid for 30 days and above</td>
            </tr>  
            <tr>
              <td class="text-center orange-background">&nbsp;</td>
              <td>Unpaid for 30 days and above and sent 1st reminder</td>
            </tr>  
            <tr>
              <td class="text-center red-background">&nbsp;</td>
              <td>Unpaid for 30 days and above and sent 2nd reminder</td>
            </tr>

            <tr>
              <td class="text-center"><button class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></td>
              <td>Input payment</td>
            </tr>
            <tr>
              <td class="text-center"><button class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></button></td>
              <td>Delete Invoice</td>
            </tr>
            <tr>
              <td class="text-center"><button class="btn btn-warning btn-sm"><i class="fa fa-flag"></i></button></td>
              <td>Send Reminder</td>
            </tr>
            <tr>
              <td class="text-center"><i class="glyphicon glyphicon-lock"></i></td>
              <td>Invoice Locked</td>
            </tr>            
          </tbody>
        </table>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      <!-- </form> -->
    </div>
  </div>
</div>
<!-- /Modals -->

<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/accounting_v2.js"></script>
@stop
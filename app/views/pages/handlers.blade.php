@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/handlers.css" rel="stylesheet">
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<div class="container _handlers_con_"></div>
<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/handlers.js"></script>
<script id="_new_handler_tmpl" type="text/x-jquery-tmpl">
<div class="col-sm-3 col-xs-4 col-md-2">
    <div class="well" style="height: 300px; overflow-y: hidden; vertical-align: middle;">
        <a href="/employee"target="_blank"><span class="glyphicon glyphicon-plus very-large-glyph text-success"></span><h5>{{ Lang::get('lang.handlers.Add New Handler') }}</h5></a>
    </div>
</div>
</script>
@stop
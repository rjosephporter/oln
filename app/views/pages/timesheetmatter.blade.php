@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
    <!-- Dashboard -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <form class="form-inline" onsubmit="return false;" name="search_timesheet" method="post">  
                        <span style="margin-right:5px; font-weight: bold;">{{ Lang::get('lang.timesheet.Select/Search Matter') }}</span>   
                        <div class="form-group">
                            <select id="target_matter" class="form-control" style="width: 400px;">
                            <option value="" selected="selected">{{ Lang::get('lang.timesheet.Select Matter') }}</option>
                             @foreach($obj['special_matters'] as $matter)
                                <option value="{{$matter->JobAssignmentID}}"> {{ str_pad($matter->JobAssignmentID, 5, '0', STR_PAD_LEFT) }} - {{$matter->Description_A}}</option>
                            @endforeach
                            @foreach($obj['matters'] as $matter)
                                <option value="{{$matter->JobAssignmentID}}">{{$matter->JobAssignmentID}} - {{$matter->Incharge}} - {{$matter->Description_A}}</option>
                            @endforeach
                            </select>
                        </div>
                        <button class="btn btn-success btn-sm" style="margin-left: 5px; display: none;" onclick="process_timesheet()" id="single_create_timesheet">{{ Lang::get('lang.timesheet.Create Timesheet') }}</button>            
                        <button class="btn btn-success btn-sm" style="margin-left: 5px;" onclick='process_fast_timesheet()'>{{ Lang::get('lang.timesheet.Quick Timesheet') }}</button>
                        <!--
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Timesheet Report <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ URL::to('timesheet/report/daily') }}">Individual Handler - Daily</a></li>
                                <li><a href="{{ URL::to('timesheet/report/weekly') }}">All Handlers - Weekly</a></li>
                            </ul>
                        </div>
                        -->
                        <a href="{{ URL::to('timesheet/report/daily') }}" target="_blank" class="btn btn-success btn-sm" style="margin-left: 5px;">Individual Timesheet Report</a>
                        <a href="{{ URL::to('timesheet/disbursement-ledger') }}" class="btn btn-success btn-sm">{{ Lang::get('lang.timesheet.Disbursement Ledger') }}</a>
                    </form>
                </div>
                  <div class="panel-body"> <!-- start -->
                    <div class="row">
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="green__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.timesheet.Active Last 4 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/green.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="orange__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.timesheet.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.timesheet.Between 4 and 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/orange.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="red__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.timesheet.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.timesheet.Beyond 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/red.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="gray__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.timesheet.Untouched/No Activity') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/grey.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="blue__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.timesheet.Completed Matters') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/blue.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                    </div>   
                </div><!-- End of Body -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/matters_landing_.js"></script>

<script id="_dis_matter_landing_tmpl" type="text/x-jquery-tmpl">
    @{{if type == "green"}}
       @if(Session::get('language','en') == 'en')
            </br>
        @endif
    <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.matters.Active Last 4 Weeks') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/green.png" width="75"/>
    </div>
    @{{else type == "orange"}}
    <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.matters.Last Activity') }}</h2>
    <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.matters.Between 4 and 8 Weeks') }}</h2>
    <hr>
    <div class="icon">
      <img src="/assets/thisapp/images/matters/orange.png" width="75"/>
    </div>
    @{{else type == "red"}}
    <h2 class="client-name" style="color: red;">{{ Lang::get('lang.matters.Last Activity') }}</h2>
    <h2 class="client-name" style="color: red;">{{ Lang::get('lang.matters.Beyond 8 Weeks') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/red.png" width="75"/>
    </div>
    @{{else type == "gray"}}
     </br>
     <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.matters.Untouched/No Activity') }}</h2>
     <hr>
     <div class="icon">
         <img src="/assets/thisapp/images/matters/grey.png" width="75"/>
     </div>
     @{{else type == "blue" }}
    </br>
    <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.matters.Completed Matters') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/blue.png" width="75"/>
    </div>
    @{{/if}}
    <div class="text">
        <var>${m_count}</var>
        <label>
            <ul style="list-style: none; margin-left: 0px; padding-left: 0px; margin-top: 15px; cursor: pointer">
                <li data-toggle="tooltip" data-placement="top" title="Billable Amount"><span class="fa fa-bookmark"></span> $<span>${billable}</span></li>
                <li data-toggle="tooltip" data-placement="top" title="Billed Amount"><span class="fa fa-bookmark" style="color:green;"></span> $<span>${billed}</span></li>
                <li data-toggle="tooltip" data-placement="top" title="Unbilled Amount"><span class="fa fa-bookmark" style="color:red;"></span> $<span>${unbilled}</span></li>
            </ul>
        </label>
    </div>
    <div class="options">
        <a href="/matters/view/${lvl}/all/${Incharge}" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i>{{ Lang::get('lang.matters.View') }}</a>
    </div>
</script>

<script type='text/javascript'>
    var flag = false;
    function process_timesheet(){
        window.open("/timesheet/new?matter_id=" + $("#target_matter").val());
    }
    function process_fast_timesheet(){
        window.open("/utils/fastcreate");
    }
    $("#target_matter").change(function(data){
        var val = parseInt($("#target_matter").val());
        if(val > 0){
            if(!flag){
                flag = true;
                $("#single_create_timesheet").show()
            }
        }
    });
    $(function(){
       $('select').selectize(); 
    });
</script>
@stop 
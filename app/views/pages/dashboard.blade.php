@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<div class="container">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#my_desk" role="tab" data-toggle="tab">{{ Lang::get('dashboard.Main Dashboard') }}</a></li>
        <li><a target="_blank" href='/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}'>{{ Lang::get('dashboard.My Desk') }}</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="my_desk">
            <!-- Dashboard -->
            <div class="row top-buffer">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="panel-title" style="padding: 9px 0px">Dashboard</div>
                                </div>
                                <div class="col-md-10">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <label for="dashboard_year">Fiscal Year</label>
                                            <select class="form-control" name="dashboard_year" id="dash_board_year_office">
                                                <option value="0">All</option>
                                                @foreach($globalVar->year_range as $year)
                                                <option value="{{ $year }}" {{{ ($year == date('Y')) ? 'selected' : '' }}}>{{ $year }}</option>
                                                @endforeach
                                            </select>
                                        </div>                              
                                    </form>
                                </div>
                                <div class="col-md-1">
                                    <button id="dashboard-apply-filter"class="btn btn-primary btn-sm apply-filter" filter-for="dashboard" style="margin: 5px 0px;"><i class="fa fa-filter"></i> Apply</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4 col-xs-6">
                                    <div class="hero-widget well well-sm hero-special-height">
                                        <div class="icon">
                                            <i class="glyphicon glyphicon-folder-open"></i>
                                        </div>
                                        <div class="text">
                                            <var>{{ $matters_count }}</var>
                                            <label class="text-muted dashboard-label">matters</label>
                                        </div>
                                        <div class="options">
                                            <a href="/matters" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> View matters</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="hero-widget well well-sm">
                                        <div class="icon">
                                            <i class="fa fa-users"></i>
                                        </div>
                                        <div class="text">
                                            <var>{{ $clients_count }}</var>
                                            <label class="text-muted dashboard-label">clients</label>
                                        </div>
                                        <div class="options">
                                            <a href="/clients" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> View clients</a>
                                        </div>
                                    </div>
                                </div>                        
                                <div class="col-sm-4 col-xs-6">
                                    <div class="hero-widget well well-sm">
                                        <div class="icon">
                                            <i class="fa fa-money"></i>
                                        </div>
                                        <div class="text">
                                            <var class="dashboard-loader">{{ HTML::image('/assets/thisapp/images/ajax-loader.gif','', array('class' => 'center-block')) }}</var>
                                            <var id="dashboard-billed-amount" style="display:none">XX</var>
                                            <label class="text-muted dashboard-label">billed (<span class="dashboard-date-range"></span>)</label>
                                        </div>
                                        <div class="options">
                                            <a href="/reports" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> View report</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="hero-widget well well-sm">
                                        <div class="icon">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                        <div class="text">
                                            <var class="dashboard-loader">{{ HTML::image('/assets/thisapp/images/ajax-loader.gif','', array('class' => 'center-block')) }}</var>
                                            <var id="dashboard-unbilled-amount" style="display:none">XX</var>
                                            <label class="text-muted dashboard-label">unbilled (<span class="dashboard-date-range"></span>)</label>
                                        </div>
                                        <div class="options">
                                            <a href="/reports" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> View report</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <div class="hero-widget well well-sm">
                                        <div class="icon">
                                            <i class="fa fa-file"></i>
                                        </div>
                                        <div class="text">
                                            <var class="dashboard-loader">{{ HTML::image('/assets/thisapp/images/ajax-loader.gif','', array('class' => 'center-block')) }}</var>
                                            <var id="dashboard-total-ar" style="display:none">XX</var>
                                            <label class="text-muted dashboard-label">accounts receivable (<span class="dashboard-date-range"></span>)</label>
                                        </div>
                                        <div class="options">
                                            <a href="/accounting" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i> View accounting</a>
                                        </div>
                                    </div>
                                </div>                        
                            </div>                    
                        </div>
                    </div>
                </div>
            </div> 
            <!-- /Dashboard -->
            
            <!-- Floorplan -->
            <div class="row top-buffer" style="display:none">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">The Office</h3>
                        </div>
                        <div class="panel-body">
                            <div class="floorplan">
                                <div class="room" id="eo1" style="left:0%; top:0%; width:19.5%; height:40%">
                                    <div class="room-contents" data-toggle="modal" data-target="#openoffice">
                                        <span class="glyphicon glyphicon-user text-success medium-glyph"></span>
                                        TKL
                                        <ul class="list-inline">
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33888</small>
                                            </li>
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33451</small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="room" id="eo2" style="left:0%; top:55%; width:20.5%; height:45%">
                                    <div class="room-contents" data-toggle="modal" data-target="#openoffice">
                                        <span class="glyphicon glyphicon-user text-success medium-glyph"></span>
                                        GDO
                                        <ul class="list-inline">
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33133</small>
                                            </li>
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33717</small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="room" id="o1" style="left:20.5%; top:62%; width:10.5%; height:38%">
                                    
                                </div>
                                <div class="room" id="o2" style="left:31%; top:62%; width:8.5%; height:38%">
                                    
                                </div>
                                <div class="room" id="o3" style="left:39.5%; top:75%; width:16.5%; height:25%">
                                    <div class="room-contents" data-toggle="modal" data-target="#openoffice">
                                        <span class="glyphicon glyphicon-user text-success medium-glyph"></span>
                                        TKL
                                        <ul class="list-inline">
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33211</small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="room" id="b1" style="left:19.5%; top:0%; width:10%; height:27%">
                                    
                                </div>
                                <div class="room" id="b2" style="left:29.5%; top:0%; width:9%; height:27%">
                                    <div class="room-contents" data-toggle="modal" data-target="#openoffice">
                                        <span class="glyphicon glyphicon-user text-success medium-glyph"></span>
                                        SL
                                        <ul class="list-inline">
                                            <li>
                                                <span class="glyphicon glyphicon-folder-open medium-glyph text-info"></span>&nbsp;&nbsp;<small>33422</small>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="room" id="b3" style="left:38.5%; top:0%; width:9%; height:27%">

                                </div>
                                <div class="room" id="o4" style="left:47.5%; top:0%; width:10%; height:40%">
                                    
                                </div>
                                <div class="room" id="cf" style="left:56%; top:75%; width:15%; height:25%">

                                </div>
                                <img class="img-responsive" src="/assets/thisapp/images/office/floorplan.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Floorplan -->

            <!-- Matters Overview -->
            <div class="row top-buffer">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="panel-title" style="padding: 9px 0px">Matters Summary</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body"> <!-- start -->
                            <div class="row">
                                <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1')">
                                    <div id="green__" class="hero-widget well well-sm section-box">
                                        </br>
                                        <h2 class="client-name" style="color: #18bc9c;">Active Last 4 Weeks</h2>
                                        <hr>
                                        <div class="icon">
                                            <img src="/assets/thisapp/images/matters/green.png" width="75"/>
                                        </div>
                                        <img src="/assets/thisapp/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2')">
                                    <div id="orange__" class="hero-widget well well-sm section-box">
                                        <h2 class="client-name" style="color: orange;">Last Activity</h2>
                                        <h2 class="client-name" style="color: orange;">Between 4 and 8 Weeks</h2>
                                        <hr>
                                        <div class="icon">
                                            <img src="/assets/thisapp/images/matters/orange.png" width="75"/>
                                        </div>
                                        <img src="/assets/thisapp/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3')">
                                    <div id="red__" class="hero-widget well well-sm section-box">
                                        <h2 class="client-name" style="color: red;">Last Activity</h2>
                                        <h2 class="client-name" style="color: red;">Beyond 8 Weeks</h2>
                                        <hr>
                                        <div class="icon">
                                            <img src="/assets/thisapp/images/matters/red.png" width="75"/>
                                        </div>
                                        <img src="/assets/thisapp/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5')">
                                    <div id="gray__" class="hero-widget well well-sm section-box">
                                        </br>
                                        <h2 class="client-name" style="color: gray;">Untouched/No Activity</h2>
                                        <hr>
                                        <div class="icon">
                                            <img src="/assets/thisapp/images/matters/grey.png" width="75"/>
                                        </div>
                                        <img src="/assets/thisapp/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                                <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4')">
                                    <div id="blue__" class="hero-widget well well-sm section-box">
                                        </br>
                                        <h2 class="client-name" style="color: blue;">Completed Matters</h2>
                                        <hr>
                                        <div class="icon">
                                            <img src="/assets/thisapp/images/matters/blue.png" width="75"/>
                                        </div>
                                        <img src="/assets/thisapp/images/ajax-loader.gif"/>
                                    </div>
                                </div>
                            </div>       
                        </div><!-- End of Body -->
                    </div>
                </div>
            </div>
            <!-- /Matters Overview -->

            <!-- Matters Overview - OLD -->
            <!--
            <div class="row top-buffer">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="panel-title" style="padding: 9px 0px">Matters</div>
                                </div>
                                <div class="col-md-10">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <label for="matter_year">Year</label>
                                            <select class="form-control" name="matter_year">
                                                <option value="0">All</option>
                                                @foreach($globalVar->year_range as $year)
                                                <option value="{{ $year }}" {{{ ($year == date('Y')) ? 'selected' : '' }}}>{{ $year }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="matter_sortby">&nbsp;&nbsp;Sort by</label>
                                            <select class="form-control" name="matter_sortby">
                                                <option value="total_billable_amount">Billable Amount</option>
                                                <option value="total_unbilled_amount" selected>Unbilled Amount</option>
                                                <option value="latest_activity_date">Recent Activity</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="matter_order">&nbsp;&nbsp;Order</label>
                                            <select class="form-control" name="matter_order">
                                                <option value="asc">Ascending</option>
                                                <option value="desc" selected>Descending</option>
                                            </select>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="matter_folder_types" folder-type="green" type="checkbox" checked> 
                                                <img src="/assets/thisapp/images/matters/green.png" class="green-folder" style="width:14px" data-toggle="tooltip" data-placement="top" title="Active for the past 6 weeks" />
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="matter_folder_types" folder-type="orange" type="checkbox" checked> 
                                                <img src="/assets/thisapp/images/matters/orange.png" class="orange-folder" style="width:14px" data-toggle="tooltip" data-placement="top" title="Untouched last 6 to 8 weeks" />
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="matter_folder_types" folder-type="red" type="checkbox" checked> 
                                                <img src="/assets/thisapp/images/matters/red.png" class="red-folder" style="width:14px" data-toggle="tooltip" data-placement="top" title="Untouched more than 8 weeks" />
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="matter_folder_types" folder-type="untouched" type="checkbox" checked> 
                                                <img src="/assets/thisapp/images/matters/untouched.png" class="untouched-folder" style="width:14px" data-toggle="tooltip" data-placement="top" title="Not touched" />
                                            </label>
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="matter_folder_types" folder-type="blue" type="checkbox" checked> 
                                                <img src="/assets/thisapp/images/matters/blue.png" class="blue-folder" style="width:14px" data-toggle="tooltip" data-placement="top" title="Completed" />
                                                <small>  (hover on the icons to see description)</small>
                                            </label>
                                        </div>                                
                                    </form>
                                </div>
                                <div class="col-md-1">
                                    <button id="matter-apply-filter"class="btn btn-primary btn-sm apply-filter" filter-for="matter" style="margin: 5px 0px;"><i class="fa fa-filter"></i> Apply</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="ajax-loader-matters" class="row">
                                <div class="col-xs-12 col-sm-12">
                                    {{ HTML::image('/assets/thisapp/images/ajax-loader.gif','', array('class' => 'center-block')) }}
                                </div>
                            </div>                    
                            <div id="matters-list-container" class="row">
                                
                            </div>
                        </div>
                        <div class="panel-footer">
                            <ul class="pager pager-nomargin">
                                <li btn-type="pager" pager-for="matter" class="previous"><a href="#">&larr; Previous</a></li>
                                <li class="pages" style="font-size: 23px"><input type="number" name="matter_page" value="1" style="width: 85px"> / <i id="matter_total_page">XX</i></li>
                                <li btn-type="pager" pager-for="matter" class="next"><a href="#">Next &rarr;</a></li>
                            </ul>                  
                        </div>
                    </div>
                </div>
            </div>
            -->
            <!-- /Matters Overview - OLD -->    

            <!-- Clients -->
            <div class="row top-buffer">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="panel-title" style="padding: 9px 0px">Clients</div>
                                </div>
                                <div class="col-md-10">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">
                                            <label for="client_year">Year</label>
                                            <select class="form-control" name="client_year">
                                                <option value="0">All</option>
                                                @foreach($globalVar->year_range as $year)
                                                <option value="{{ $year }}" {{{ ($year == date('Y')) ? 'selected' : '' }}}>{{ $year }}</option>
                                                @endforeach
                                            </select>
                                        </div>                                
                                        <div class="form-group">
                                            <label for="client_sortby">Sort by</label>
                                            <select class="form-control" name="client_sortby">
                                                <option value="customer_id">Client ID</option>
                                                <option value="CompanyName_A1">Name</option>
                                                <option value="matter_count">Total Matters</option>
                                                <option value="total_unbilled_amount" selected>Unbilled Amount</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="client_order">Order</label>
                                            <select class="form-control" name="client_order">
                                                <option value="asc">Ascending</option>
                                                <option value="desc" selected>Descending</option>
                                            </select>
                                        </div>                               
                                    </form>
                                </div>
                                <div class="col-md-1">
                                    <button id="client-apply-filter" class="btn btn-primary btn-sm apply-filter" filter-for="client" style="margin: 5px 0px;"><i class="fa fa-filter"></i> Apply</button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="ajax-loader-clients" class="row">
                                <div class="col-xs-12 col-sm-12">
                                    {{ HTML::image('/assets/thisapp/images/ajax-loader.gif','', array('class' => 'center-block')) }}
                                </div>
                            </div> 
                            <div id="clients-list-container" class="row">
                                <!-- views/pages/office_clients -->
                            </div>
                        </div>
                        <div class="panel-footer">
                            <ul class="pager pager-nomargin">
                                <li btn-type="pager" pager-for="client" class="previous"><a href="#">&larr; Previous</a></li>
                                <li class="pages" style="font-size: 23px"><input type="number" name="client_page" value="1" style="width: 85px"> / <i id="client_total_page">XX</i></li>
                                <li btn-type="pager" pager-for="client" class="next"><a href="#">Next &rarr;</a></li>
                            </ul>                  
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Clients -->
        </div>
        <div class="tab-pane" id="other">
            <h4>{{ Lang::get('dashboard.Matters untouched for 8 weeks or more.') }}</h4>
            <div class="row top-buffer">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped" id="my_desk_matters">
                        <thead>
                            <tr>
                                <th>{{ Lang::get('dashboard.Matter ID') }}</th>
                                <th>{{ Lang::get('dashboard.Handler') }}</th>
                                <th>{{ Lang::get('dashboard.Client Name') }}</th>
                                <th>{{ Lang::get('dashboard.Matter Name') }}</th>
                                <th>{{ Lang::get('dashboard.Unbilled Amount') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($my_desk_matters as $md_matter)
                            <tr>
                                <td>{{ $md_matter->JobAssignmentID }}</td>
                                <td>{{ $md_matter->Incharge }}</td>
                                <td>{{ $md_matter->client_name }}</td>
                                <td>{{ $md_matter->Description_A }}</td>
                                <td>${{ number_format($md_matter->unbilled, 2) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    

</div>

<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Matter #33296</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">Reference no.</label>
                <div class="col-sm-8">
                  <p class="form-control-static">33296</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Open</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Introducer</label>
                <div class="col-sm-8">
                  <p class="form-control-static">GDO</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Client's name</label>
                <div class="col-sm-8">
                  <p class="form-control-static">FFF</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Matter</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Advice on proposed investment in China</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Subject Matter</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Employment</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Handler</label>
                <div class="col-sm-8">
                  <p class="form-control-static">VKS</p>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openhandler" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SVK</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Susan van Kross</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Out</p>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openstaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Staff status</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Tim J. Cone</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Out</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Position</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Research Assistant</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Current tasks</label>
                <div class="col-sm-8">
                  <p class="form-control-static"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Absences this month</label>
                <div class="col-sm-8">
                  <p class="form-control-static">3</p>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openoffice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">TKL's Office</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Thomas K. Lueng</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">In</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Current matters in office</label>
                <div class="col-sm-8">
                    <ul class="list-unstyled">
                        <li>
                            <span class="glyphicon glyphicon-folder-open large-glyph text-info"></span>
                            &nbsp;&nbsp;<span class="h4">33296</span>
                            <p>
                                Client: AAA<br>
                                Matter: Legal Advice<br>
                                Subject Matter: I.P.
                            </p>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-folder-open large-glyph text-info"></span>
                            &nbsp;&nbsp;<span class="h4">33296</span>
                            <p>
                                Client: FFF<br>
                                Matter: Debt Recovery Action<br>
                                Subject Matter: Dispute Resolution.
                            </p>
                        </li>
                    </ul>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/dashboard.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/matters_landing_.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/raphael-min.js"></script>
@stop
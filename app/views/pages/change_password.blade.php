@extends('template/default')

@section('header-custom-css')

@stop

@section('content')

<div class="container">
	<ul class="nav nav-tabs" role="tablist" id="myTab">
	  <li class="active"><a href="#changepass" role="tab" data-toggle="tab">Change Password </a></li>
	  <li><a href="#uploadphoto" role="tab" data-toggle="tab">Upload Photo</a></li>
	</ul>

	<div id="myTabContent" class="tab-content">
      <div class="tab-pane fade active in" id="changepass">
		   <div class="row">
				<div class="col-md-12">
					<h2>
						
					</h2>
					<div class="modal-content" id="new_change_password_log2">
						<form role="form" method="post">
							<div class="modal-body">
								<div class="row" id="new_change_password_main_form">
								<div class="row">
									<div class="col-md-6 row">
										<div class="col-md-12 form-group">
											<label for="new_old_password">Old Password </label>
											<input type="password" class="form-control" id="new_old_password" name="new_old_password" required>
										</div>
										<div class="col-md-12 form-group">
											<label for="new_password">New Password </label>
											<input type="password" class="form-control" id="new_password" name="new_password" required>
										</div>
										<div class="col-md-12 form-group">
											<label for="new_retype_password">Retype New Password </label>
											<input type="password" class="form-control" id="new_retype_password" name="new_retype_password" required>
										</div>
									</div>
								</div>
								</div>
							</div>
							<div class="modal-footer">
								<a href="{{ URL::to('/dashboard') }}" class="btn btn-default">Back</a>
								<button type="submit" class="btn btn-primary_changepassword">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
      </div>
      <div class="tab-pane fade" id="uploadphoto">
      	 <div class="row">
				<div class="col-md-12">
					
					<div class="modal-content" id="upload_photo_log2">
						<!--<form action="/account-settings/save" method="post" enctype="multipart/form-data">-->
							<div class="modal-body">
								<div class="row" id="new_upload_photo_main_form">
								<div class="row">
									<div class="col-md-6 row">
										<div class="col-md-12 form-group">
											
											{{Form::open(array('enctype' => 'multipart/form-data','url' => '/account-settings/save', 'files' => true))}}
											{{Form::label('image', 'Upload')}}
											{{Form::file('image', array('multiple'))}}
											 <br><br>
											{{Form::submit('Upload')}}
											{{Form::close()}}
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
							
						<!--</form>-->
					</div>
				</div>
		  </div>
		
		
      </div>
    </div>
</div>


<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script>

	$('#myTab a').click(function (e) {
  		e.preventDefault()
  		$(this).tab('show')
	})


	report_types = [];

	var item_container = $('#item-container-source').clone().html();


    /* Save password */
    $('#new_change_password_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

        if($('#new_password').val() != $('#new_retype_password').val()){

        	toastr.error('Passwords do not match.');
		}else{
        $.post('/account-settings/updatepass', $(this).serializeArray(), function(data){
            console.log(data);
            console.log(data.val);
            if(data.status == 'success') {
                toastr.success(data.message);
	            var delay = 3000; //Your delay in milliseconds
	            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
            } else {
                toastr.error(data.message);
            }
        });
        }
    	

    });  

     $(document).ready(function() {
    	
    	@if($errors->any())

			<?php if($errors->first() == 'success') {?>
			     toastr.success('Photo updated successfully');
				var delay = 3000; //Your delay in milliseconds
	            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);   
	                    
		     <?php } else {?>
			     toastr.error('Photo upload failed');
		     <?php }?>
					        
		@endif
    });

    
</script>
@stop
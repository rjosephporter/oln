@extends('template/default')
@section('content')
<link type="text/css" href="/assets/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet">
<style>
.top-bottom-margin {
	margin-top: 15px;
	margin-bottom: 15px;
}

.double-underline {
    text-decoration:underline;
    border-bottom: 1px solid #000;
}

.signature {
	float: left; 
	margin: 20px 10px;
	border-top: 1px solid #000;
	width: 200px; 
	text-align: center;
}

.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;
}

.left-container {
	background-color: rgba(255, 231, 0, 0.5);
}

.table-left-content-bg {
	background-color: rgba(255, 231, 0, 0.5);
}

.fixed {
	position: fixed;
	top: 0;
	width: 100%;
}

.menu {
	width: 100%;
	z-index: 1000;
}

.navbar {
	margin-bottom: 0px;
}
</style>
@if(count($timesheet['list']) > 0)
<div class="container-fluid">
	<form method="post" id="invoice-form" data-redirect-url="{{ URL::to('invoice') }}">
		<input type="hidden" name="form_type" value="{{ $form_type }}">
		<input type="hidden" name="client[type]" value="{{ $client['type'] }}">
		<div class="menu">
			<div class="row hidden-print navbar navbar-inverse" style="margin-top:0px; color:#fff;">
				<div class="client-header">
					<div class="row">
						<div class="col-md-8 col-sm-8 col-xs-8">
							<label>Client: {{ $client['name'] or '' }}</label>&nbsp;&nbsp;
							<button type="button" class="btn btn-warning" id="legally-close"><i class="fa fa-legal"></i> {{ Lang::get('lang.invoice.End Matter') }}</button>
	                                                <div id="help" class="pull-right"></div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<label>{{ Lang::get('lang.invoice.Total Invoice') }}: $<span class="text-overall-total">{{ number_format($total['overall'],2) }}</span></label>
						</div>
					</div>
					<div class="row" style="margin-top:10px">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="btn-group pull-right">						
								@if($form_type == 'new')
								<a href="{{ URL::to('invoice/print/' . $id . '?direct_print=true') }}" target="_blank" class="btn btn-default" id="btn-print" style="display:none"> <i class="fa fa-print"></i> {{ Lang::get('lang.invoice.Print') }}</a>
								<button type="submit" class="btn btn-info" id="btn-save"><i class="fa fa-save"></i> {{ Lang::get('lang.invoice.Save Invoice') }}</button>
								<button class="btn btn-warning" id="btn-add-timesheet"><i class="fa fa-list"></i> {{ Lang::get('lang.invoice.Add Timesheet') }}</button>
								@else
								<button type="submit" class="btn btn-info" id="btn-update"><i class="fa fa-save"></i> {{ Lang::get('lang.invoice.Update Invoice') }}</button>
								<a href="{{ URL::to('invoice/print/' . $id . '?direct_print=true') }}" target="_blank" class="btn btn-default"> <i class="fa fa-print"></i> {{ Lang::get('lang.invoice.Print') }}</a>

								<div class="dropdown" style="float:left">
									<button class="btn btn-info dropdown-toggle" type="button" id="drpdwn-add-timesheet" data-toggle="dropdown" aria-expanded="true">
										Add Timesheet
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" aria-labelledby="drpdwn-add-timesheet">
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#new_timesheet_log">New</a></li>
										<!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Existing</a></li> -->
									</ul>
								</div>

								@endif
								<button type="button" class="btn btn-waring" id="btn-preview-draft"><i class="fa fa-print"></i>{{ Lang::get('lang.invoice.Preview Draft') }}</button>
								<a href="{{ URL::to('invoice') }}" class="btn btn-danger"><i class="fa fa-times"></i> {{ Lang::get('lang.invoice.Cancel') }}</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				
				<div class="row">
					<div class="col-md-12 text-center">
						<img src="{{ URL::to('/') }}/assets/thisapp/images/oln_invoice_logo.png" style="width:400px" />
					</div>
				</div>
			
				<div class="row top-buffer">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<address>
							{{ $template['company_address_1'] }}<br>
							{{ $template['company_address_2'] }}<br>
							{{ $template['company_address_3'] }}<br>
						</address>			
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<address class='text-right'>
							{{ Lang::get('lang.invoice.Tel') }}: {{ $template['company_tel_no'] }}<br>
							{{ Lang::get('lang.invoice.Fax') }}: {{ $template['company_fax_no'] }}<br>
							{{ Lang::get('lang.invoice.Email') }}:&nbsp;
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="handler_email" value="{{ $handler_email or '' }}" readonly>
						</address>			
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-horizontal" role="form">				
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Client') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[name]" value="{{ $client['name'] or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Address') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[address]" value="{{ $client['address'] or '' }}">
								</div>
							</div>
							@if($client['type'] == '2')
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Attn') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[attn]" value="{{ $client['attn'] or '' }}">
								</div>
							</div>
							@endif
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Email') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[email]" value="{{ $client['email'] or '' }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Tel') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[telephone]" value="{{ $client['telephone'] or '' }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Fax') }}</label>
								<div class="col-sm-10 col-xs-10">
									<input type="text" class="form-control input-sm" name="client[fax]" value="{{ $client['fax'] or '' }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('lang.invoice.Re') }}</label>
								<div class="col-sm-10 col-xs-10">
									<textarea class="form-control input-sm" name="matter[description]">{{ $matter['description'] or '' }}</textarea>
								</div>
							</div>				
						</div>			
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-horizontal" role="form">
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('lang.invoice.Created By') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="created_by" value="{{ $created_by }}" readonly>
								</div>
							</div>				
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('lang.invoice.Date') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="date" value="{{ $date }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('lang.invoice.Ref') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="reference_number" value="{{ $reference_number or '' }}">
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-8 col-xs-8 control-label text-right">{{ Lang::get('lang.invoice.Invoice No') }}</label>
								<div class="col-sm-4 col-xs-4">
									<input type="text" class="form-control input-sm" name="new_id" value="{{ $new_id or '' }}">
								</div>
							</div>
							@if(date('Y') < 2015 && $form_type == 'new')
							<div class="form-group margin-bottom-5">
								<div class="col-sm-12 col-xs-12">
									<div class="alert alert-warning">
										<small><i class="fa fa-exclamation-circle"></i>&nbsp;&nbsp;Please ask Terence for a unique Invoice No.</small>
									</div>
								</div>
							</div>				
							@endif		
						</div>	
					</div>
				</div>

				<div class="row top-bottom-margin">
					<div class="col-md-12">
						<p>
							{{ Lang::get('lang.invoice.To our professional charges for action on your behalf in connection with that above matter from') }}
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[min_date]" value="{{ date('j F Y', strtotime($timesheet['min_date'])) }}" readonly> to 
							<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[max_date]" value="{{ date('j F Y', strtotime($timesheet['max_date'])) }}" readonly>, 
							{{ Lang::get('lang.invoice.particulars of which are as follows') }}:
						</p>
					</div>
				</div>				

			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				<div class="row" style="visibility:hidden">
					<div class="col-md-12 text-center">
						<img src="http://192.168.1.199:8083/assets/thisapp/images/oln_invoice_logo.png" style="width:400px">
					</div>
				</div>
				<div class="row>">
					<div class="col-md-12">
						<div class="form-horizontal" role="form">
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('lang.invoice.Status') }}</label>
								<div class="col-sm-9 col-xs-9">
									{{ Form::select('status', array(-2 => 'Draft', 0 => 'Approved'), $status, array('class' => 'form-control input-sm')) }}
								</div>
								<!--
								<div class="col-sm-4 col-xs-4">
									<a href="#" id="btn-request-approval" style="display:none" class="btn btn-sm btn-info"><i class="fa fa-envelope"></i> Request Approval</a>
								</div>
								-->
							</div>							
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('lang.invoice.Unique ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="id" value="{{ $id or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('lang.invoice.Batch Number') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="batch_number" value="{{ $batch_number or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label">{{ Lang::get('lang.invoice.Matter ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="matter[id]" value="{{ $matter['id'] or '' }}" readonly>
								</div>
							</div>				
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label">{{ Lang::get('lang.invoice.Client ID') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="client[id]" value="{{ $client['id'] or '' }}" readonly>
								</div>
							</div>
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('lang.invoice.Type') }}</label>
								<!--
								<div class="col-sm-9 col-xs-9">
									{{ Form::select('type', array(1 => 'Intermediary Invoice', 2 => 'Final Invoice'), 2, array('class' => 'form-control input-sm')) }}
								</div>
								-->
								<div class="col-sm-9 col-xs-9">
									@if($form_type == 'new')
									<div class="radio">
									  <label>
									    <input type="radio" name="invoice_type" value="IN" checked>
									    {{ Lang::get('lang.invoice.Intermediary Invoice') }}
									  </label>
									</div>
									<div class="radio">
									  <label>
									    <input type="radio" name="invoice_type" value="FI">
									    {{ Lang::get('lang.invoice.Final Invoice') }}
									    <input type="text" class="form-control input-sm" name="box_number" placeholder="Enter Box Number" disabled>
									  </label>
									</div>
									@else
									<div class="radio">
									  <label>
									    <input type="radio" name="invoice_type" value="IN" {{ $type == 'IN' ? 'checked' : '' }} disabled>
									    {{ Lang::get('lang.invoice.Intermediary Invoice') }}
									  </label>
									</div>
									<div class="radio">
									  <label>
									    <input type="radio" name="invoice_type" value="FI" {{ $type == 'FI' ? 'checked' : '' }} disabled>
									    {{ Lang::get('lang.invoice.Final Invoice') }} 
									    <input type="text" class="form-control input-sm" name="box_number" value="{{ $box_number or '' }}" placeholder="Enter Box Number" disabled>
									  </label>
									</div>	
									@endif								
								</div>								
							</div>							
							<div class="form-group margin-bottom-5">
								<label class="col-sm-3 col-xs-3 control-label text-right">{{ Lang::get('lang.invoice.Approver') }}</label>
								<div class="col-sm-9 col-xs-9">
									<input type="text" class="form-control input-sm" name="approver" value="{{ $approver or '' }}">
								</div>
							</div>						
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<table class="table table-bordered table-striped table-condensed" id="left-table">
					<col width="10%">
					<col width="10%">
					<col width="60%">
					<col width="10%">
					<col width="10%">
					<thead>
						<tr>
							<th>{{ Lang::get('lang.invoice.Date') }}</th>
							<th>{{ Lang::get('lang.invoice.Solicitor') }}</th>
							<th>{{ Lang::get('lang.invoice.Description') }}</th>
							<th><abbr title="Complimented Units">{{ Lang::get('lang.invoice.Comp Units') }}</abbr></th>
							<th>{{ Lang::get('lang.invoice.Charged Units') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($timesheet['list'] as $ndx => $item)
						<tr>
							<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][UniqueID]" value="{{ $item->UniqueID }}">
							<td><input type="text" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][WorkDate]" value="{{ date('d-M-Y', strtotime($item->WorkDate)) }}" readonly></td>
							<td><input type="text" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][EmployeeID]" value="{{ $item->EmployeeID }}" readonly></td>
							<td><input type="text" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][Comment]" value="{{ $item->Comment }}"></td>
							<td><input type="text" class="form-control input-sm text-input-complimented-units" value="" disabled></td>
							<td>
								<small class="text-complimentary" style="{{ ($item->complimentary) ? 'display:none' : 'display:none' }}">{{ Lang::get('lang.invoice.Complimentary') }}</small>
								<input type="text" class="form-control input-sm input-unit" value="{{ ($form_type == 'new') ? round($item->units,2) : round($item->charged_units,2) }}" style="{{ ($item->complimentary) ? '' : '' }}" disabled>
								<input type="hidden" class="form-control input-sm input-unit-raw" name="timesheet[list][{{ $ndx }}][units]" value="{{ ($form_type == 'new') ? $item->units : $item->charged_units }}" readonly>
								<input type="hidden" class="form-control input-sm input-orig-units" name="timesheet[list][{{ $ndx }}][orig_units]" value="{{ $item->units }}" readonly>
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4" class="text-right"><strong>{{ Lang::get('lang.invoice.TOTAL UNITS') }}</strong></td>
							<td>
								<input type="text" class="form-control input-sm input-timesheet-total-units-round" value="{{ round($total['cost']['units'],2) }}" disabled>
								<input type="hidden" class="form-control input-sm input-timesheet-total-units" name="total[cost][units]" value="{{ $total['cost']['units'] }}" readonly>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				<table class="table table-bordered table-condensed table-left-content-bg" id="right-table">
					<col width="10%">
					<col width="10%">
					<col width="10%">
					<col width="20%">
					<col width="20%">
					<col width="30%">
					<thead>
						<tr>
							<th>{{ Lang::get('invoice.Delete') }}</th>
							<th><abbr title="Complimentary">{{ Lang::get('lang.invoice.Comp') }}.</abbr></th>
							<th><abbr title="Complimented Units">{{ Lang::get('lang.invoice.Comp Units') }}.</abbr></th>
							<th>{{ Lang::get('lang.invoice.Hourly Rate') }}</th>
							<th>{{ Lang::get('lang.invoice.Work Hours') }}</th>
							<th>{{ Lang::get('lang.invoice.Amount') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($timesheet['list'] as $ndx => $item)
						<tr>
							<td class="text-center">
								<button type="button" class="btn btn-sm btn-danger remove-timesheet-entry" title="Delete Item" data-unique-id="{{ $item->UniqueID }}"><i class="fa fa-trash"></i></button>
							</td>
							<td class="text-center">
								{{ Form::checkbox("timesheet[list][$ndx][complimentary]", null, $item->complimentary, array("class" => "chk-complimentary", "data-unique-id" => $item->UniqueID)) }}								
							</td>
							<td>
								<input type="number" step="1" min="1" max="{{ $item->units }}" class="form-control input-sm input-complimented-units" name="timesheet[list][{{ $ndx }}][complimented_units]" value="{{ $item->complimented_units or '' }}"  {{ ($form_type="update" && $item->complimentary) ? '' : 'readonly' }}>
							</td>
							<td>
								<input type="text" class="form-control input-sm" value="{{ number_format($item->HourlyRate,2) }}" disabled>
								<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][HourlyRate]" value="{{ $item->HourlyRate }}" readonly>
							</td>
							<td>
								<input type="text" class="form-control input-sm" value="{{ round($item->WorkHour,2) }}" disabled>
								<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][WorkHour]" value="{{ $item->WorkHour }}" readonly>
							</td>
							<td>
								<input type="text" class="form-control input-sm" value="{{ number_format($item->amount,2) }}" disabled>
								<input type="hidden" class="form-control input-sm" name="timesheet[list][{{ $ndx }}][amount]" value="{{ $item->amount }}" readonly>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>			
		</div>

		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('lang.invoice.Our Fees') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed">
							<col width="90%">
							<col width="10%">				
							<tbody>
								@foreach($timesheet['cost_summary'] as $ndx => $cost)
								<tr>
									<td>{{ Lang::get('lang.invoice.Work undertaken by') }} 
										<input type="text" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[cost_summary][{{ $ndx }}][EmployeeID]" value="{{ $cost->EmployeeID }}" readonly> @ 
										<input type="text" class="form-control input-sm" style="display:inline; width:165px" value="{{ number_format($cost->HourlyRate,2) }}" disabled> 
										<input type="hidden" class="form-control input-sm" style="display:inline; width:165px" name="timesheet[cost_summary][{{ $ndx }}][HourlyRate]" value="{{ $cost->HourlyRate }}" readonly>
										{{ Lang::get('lang.invoice.per hour') }}
									</td>
									<td>
										<input type="text" class="form-control input-sm input-handler-cost-round" data-handler="{{ $cost->EmployeeID }}" value="{{ number_format($cost->sum_amount,2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-handler-cost" data-handler="{{ $cost->EmployeeID }}" name="timesheet[cost_summary][{{ $ndx }}][sum_amount]" value="{{ $cost->sum_amount }}" readonly>
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('lang.invoice.TOTAL COSTS') }}</strong></td>
									<td><strong>
										<input type="text" class="form-control input-sm input-total-costs-round" value="{{ number_format($total['cost']['amount'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-total-costs" name="total[cost][amount]" value="{{ $total['cost']['amount'] }}" readonly>
									</strong></td>
								</tr>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('lang.invoice.AGREED COSTS') }}</strong></td>
									<td><strong><input type="text" class="form-control input-sm input-agreed-cost" name="timesheet[agreed_cost]" value="{{ $timesheet['agreed_cost'] }}"></strong></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('lang.invoice.Disbursements') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed table-disbursement">
							<col width="5%">
							<col width="85%">
							<col width="10%">				
							<tbody class="items">
								@if(count($disbursements) == 0)
									<tr data-index="-1">
										<input type="hidden" class="form-control input-sm" name="disbursements[-1][invoice_disbursement_id]" value="" placeholder="ID">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="disbursement"></i></td>
										<td><input type="text" class="form-control input-sm" name="disbursements[-1][description]" value="" placeholder="Description"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="disbursement" name="disbursements[-1][amount]" value="" placeholder="Amount"></td>
									</tr>						
								@else
									@foreach($disbursements as $ndx => $disb)
									<tr data-index="{{ $ndx }}">
										<input type="hidden" class="form-control input-sm" name="disbursements[{{ $ndx }}][invoice_disbursement_id]" value="{{ $disb->invoice_disbursement_id }}">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="disbursement"></i></td>
										<td><input type="text" class="form-control input-sm" name="disbursements[{{ $ndx }}][description]" value="{{ $disb->description }}"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="disbursement" name="disbursements[{{ $ndx }}][amount]" value="{{ round($disb->amount,2) }}"></td>
									</tr>
									@endforeach
								@endif
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2">
										<button class="btn btn-sm btn-info pull-left btn-add-item" data-type="disbursement"><i class="fa fa-plus"></i> {{ Lang::get('lang.invoice.Add Disbursement') }}</button>
										<strong class="pull-right">{{ Lang::get('lang.invoice.TOTAL DISBURSEMENTS') }}</strong>
									</td>
									<td><strong>
										<input type="text" class="form-control input-sm input-disbursement-total-round" value="{{ number_format($total['disbursement'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-disbursement-total" name="total[disbursement]" value="{{ $total['disbursement'] }}" readonly>
									</strong></td>
								</tr>
							</tfoot>
						</table>
					</div>		
				</div>	
				<div class="row">
					<div class="col-md-12">
						<h4>{{ Lang::get('lang.invoice.Less') }}</h4>
					</div>
					<div class="col-md-12">
						<table class="table table-bordered table-condensed table-less">
							<col width="5%">
							<col width="85%">
							<col width="10%">				
							<tbody class="items">
								@if(count($less) == 0)
									<tr data-index="-1">
										<input type="hidden" class="form-control input-sm" name="less[-1][invoice_less_id]" value="" placeholder="ID">
										<input type="hidden" class="form-control input-sm" name="less[-1][type]" value="" placeholder="Type">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="less"></i></td>
										<td><input type="text" class="form-control input-sm" name="less[-1][description]" value="" placeholder="Description"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="less" name="less[-1][amount]" value="" placeholder="Amount"></td>
									</tr>
								@else
									@foreach($less as $ndx => $l)
									<tr data-index="{{ $ndx }}">
										<input type="hidden" class="form-control input-sm" name="less[{{ $ndx }}][invoice_less_id]" value="{{ $l->invoice_less_id }}">
										<input type="hidden" class="form-control input-sm" name="less[{{ $ndx }}][type]" value="{{ $l->type or '' }}" placeholder="Type">
										<td class="text-center"><i class="fa fa-times text-danger cursor-pointer remove-item" data-item-type="less"></i></td>
										<td><input type="text" class="form-control input-sm" name="less[{{ $ndx }}][description]" value="{{ $l->description }}"></td>
										<td><input type="text" class="form-control input-sm input-otheritem-amount" data-item-type="less" name="less[{{ $ndx }}][amount]" value="{{ round($l->amount,2) }}"></td>
									</tr>
									@endforeach
								@endif
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2">
										<button class="btn btn-sm btn-info pull-left btn-add-item" data-type="less"><i class="fa fa-plus"></i> Add Less</button>
										<strong class="pull-right">{{ Lang::get('lang.invoice.TOTAL LESS') }}</strong>
									</td>
									<td><strong>
										<input type="text" class="form-control input-sm input-less-total-round" value="{{ number_format($total['less'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-less-total" name="total[less]" value="{{ $total['less'] }}" readonly>
									</strong></td>
								</tr>
							</tfoot>
						</table>
					</div>		
				</div>

				<div class="row">
					<div class="col-md-12">
						<table class="table table-condensed">
							<col width="90%">
							<col width="10%">
							<tbody>
								<tr>
									<td class="text-right"><strong>{{ Lang::get('lang.invoice.Total Invoice') }}</strong></td>
									<td><strong class="double-underline">
										<input type="text" class="form-control input-sm input-overall-total-round" value="{{ number_format($total['overall'],2) }}" disabled>
										<input type="hidden" class="form-control input-sm input-overall-total" name="total[overall]" value="{{ $total['overall'] }}" readonly>
									</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="row top-bottom-margin">
					<div class="col-md-12">
						<span class="signature">{{ Lang::get('lang.invoice.WITH COMPLIMENTS') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<p>{{ Lang::get('lang.invoice.Cheques should be made payable to') }}: <strong>{{ $template['cheque_payable_to'] }}</strong></p>
						<p>
							{{ Lang::get('lang.invoice.Telegraphic Transfer') }}:<br/>
							{{ $template['name_of_banker']['line_1'] }}<br/>
							{{ $template['name_of_banker']['line_2'] }}<br/>
							{{ $template['name_of_banker']['line_3'] }}<br/>
							{{ $template['name_of_banker']['line_4'] }}<br/>
						</p>
						<p>
							{{ Lang::get('lang.invoice.Account Name') }}: {{ $template['account_name'] }}<br/>
							{{ Lang::get('lang.invoice.Account No') }}: {{ $template['account_no'] }}<br/>
							{{ Lang::get('lang.invoice.Swift Code') }}: {{ $template['swift_code'] }}<br/>
						</p>
						<p>
							{{ Lang::get('lang.invoice.Please note that interest will accrue at the rate of interest_rate per month in the event that this invoice is not settled within no of days days from the date hereof', array('interest_rate' => '2%', 'no_of_days' => 30)) }}
						</p>
						<p>
							{{ Lang::get('lang.invoice.No receipt will be issued unless requested') }}
						</p>
						<p>
							{{ Lang::get('lang.invoice.Bills may be paid by VISA or MASTERCARD in person at our office') }}
						</p>
						<p>
							E. & O.E.
						</p>
					</div>
				</div>											
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container hidden-print">
			</div>
		</div>

		<!-- BEGIN ACTION BUTTONS -->
		<!--
		<div class="row row-eq-height">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<button type="submit" class="btn btn-success pull-right">Save</button>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 left-container right-allignment hidden-print">
				&nbsp;
			</div>
		</div>
		-->
		<!-- END ACTION BUTTONS -->
	</form>
</div>
@else
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="alert alert-info" role="alert">
	            <i class="fa fa-exclamation-circle"></i> {{ Lang::get('lang.invoice.No available timesheet to invoice for Matter') }} {{ $matter['id'] }}</strong>
	        </div>
		</div>
	</div>
</div>
@endif

<!-- Modals -->
<div class="modal fade" id="complete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">{{ Lang::get('lang.invoice.End Matter') }} <span class="matter_for_completion">{{ $matter['id'] }}</span></h4>
      </div>
      <form id="matter_completion_form" role="form" method="post" class="my_confirm_forms">
      <div class="modal-body confirm_body_modal"style="text-align: center; vertical-align: middle;">
        <h3 style="color: red;">This action will mark Matter <span class="matter_for_completion"></span> {{ Lang::get('lang.invoice.as ENDED and COMPLETED') }} {{ Lang::get('lang.invoice.Do you want to proceed') }}</h3>

        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-file"></i> {{ Lang::get('lang.invoice.Docket/Box No') }}:</div>
            <input class="form-control" type="text" name="box_number" value="" placeholder=""/>
          </div>
        </div>

      </div>
          <input type="hidden" value="{{ $matter['id'] }}" name="matter_id" class="matter_for_completion"/>
          <input type="hidden" value="1" name="CloseStatus"/>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">{{ Lang::get('lang.invoice.Yes') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('lang.invoice.No') }}</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="new_timesheet_log" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="new_timesheet_form" role="form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">New Timesheet</h4>
			</div>
			<div class="modal-body">
				<div class="row item-container">
					<div class="col-md-6 row">
						<input type="hidden" name="new_timesheet[invoice_id]" value="{{ $id }}">
						<input type="hidden" name="new_timesheet[matter_id]" value="{{ $matter['id'] }}">
						<div class="col-md-12 form-group">
							<label>Units</label>
							<input type="number" step="1" class="form-control" name="new_timesheet[units]" required>
						</div>

						<div class="col-md-12 form-group">
							<label>Work Hours</label>
							<input type="number" step="any" class="form-control" name="new_timesheet[workhour_round]" required readonly>
							<input type="hidden" step="any" class="form-control" name="new_timesheet[workhour]">
						</div>					
					</div>
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label>Date  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_timesheet[date]" readonly required>
						</div>
						<div class="col-md-6 form-group">
								<label>Handler</label>
								<select class="form-control" name="new_timesheet[handler]" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
									@endforeach
								</select>
						</div>
						<div class="col-md-6 form-group hidden">
								<label>Hourly Rate</label>
								<select class="form-control" name="new_timesheet[hourlyrate]" placeholder="Select Hourly Rate" readonly disabled>
									<option></option>
									@foreach($hourly_rate_list as $hourly_rate)
										<option value="{{ $hourly_rate->EmployeeID }}">{{ $hourly_rate->HourlyRate }}</option>
									@endforeach
								</select>								
						</div>
						<div class="col-md-12 form-group">
							<label>Description</label>
							<textarea class="form-control" name="new_timesheet[description]" rows="3" required></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-sm">Add</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- /Modals -->

<script type="text/x-template" id="help_items">
<div class="dropdown">
  <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    {{ Lang::get('lang.invoice.Help') }}
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
    <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/thisapp/help_pages/how-to-print-this-invoice.html">How to print this invoice.</a></li>
  </ul>
</div>
</script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script>
	var global = {};
	global.timesheet = {
		list : {{ json_encode($timesheet['list']) }},	
		cost_summary : {{ json_encode($timesheet['cost_summary']) }}
	};
</script>
<script id="modal_preloader_confirm" type="text/x-jquery-tmpl">
    <h4>. . . {{ Lang::get('lang.invoice.Closing Matter') }}, {{ Lang::get('lang.invoice.Please Wait') }} . . .</h4>
    <img src="/assets/thisapp/images/ajax-loader-bar.gif">
</script>
<script type="text/javascript" src="/assets/thisapp/js/invoice_create_v2.js"></script>
<script>
    $(document).ready(function () {
        $('#help').append($($('#help_items').html()));
        $('#help').on('click','a[role=menuitem]',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url);
        })
    })
</script>
@stop
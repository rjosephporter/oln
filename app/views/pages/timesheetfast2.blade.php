@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class='containter'>
    <div class='row'>
        <div class='col-xs-12' style='text-align: center; vertical-align: middle;'>
            <div class='panel panel-default'>

                <div class='panel-heading' style='text-align: left;'>
                    <a href="#" class='btn btn-info' onclick="additem({ts : {Comment : '', ChargeHour : 0, UniqueID : 0}})">Add Timesheet Entry</a>
                </div>
                    <input type="hidden" name="popup" value="{{ Input::get('popup', 'false') }}">
                    <table class='table' id='timesheet_entry'>
                        <thead>
                            <tr>
                                <th style='text-align: center;'>Date</th>
                                <th style='text-align: center;'>Matter No</th>
                                <th style='text-align: center;'>Handler</th>
                                <th style='text-align: center;'>Description</th>
                                <th style='text-align: center;'>Units</th>
                                <th style='text-align: center;'>Work Hours</th>
                                <th style='text-align: center;'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="_modal_notification___">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: left;">
                <h4 style="color: red;" id="__body_notify">All Fields are Required. Please check your entry and try again.</h4>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="_saving__">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <h4 style="color: green;">. . . Successfully Saved . . .</h4>
            </div>
        </div>
      </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="_init__">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Quick Timesheet</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <h4 style="color: orange;">. . . Processing, Please Wait. . .</h4>
            </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script id="row_tmpl" type="text/x-jquery-tmpl">
    <td style='text-align: center; font-size: 10px;'><input id='_date_control_${ctr}' class='form-control _date_control_' type='text' name='date[]' size="5"/></td>
    <td style='text-align: left;'>
        <select name='matter_no[]' id='matter_no_${ctr}' class="form-control" style="width: 100%; overflow: hidden;">
            <option value="" selected="selected">Select Matter</option>
         @foreach($obj['special_matters'] as $matter)
            <option value="{{$matter->JobAssignmentID}}">{{ str_pad($matter->JobAssignmentID, 5, '0', STR_PAD_LEFT) }} - {{$matter->Description_A}}</option>
        @endforeach
        @foreach($obj['matters'] as $matter)
            <option value="{{$matter->JobAssignmentID}}">{{$matter->Incharge}} - {{$matter->JobAssignmentID}} - {{$matter->Description_A}}</option>
        @endforeach
        </select>
    </td>
    <td style='text-align: left;'>
        <select name="handler[]" id='handler_${ctr}' class="form-control" style="width: 100%; overflow: hidden;">
            <option value="">Select Handler</option>
            @foreach($obj['handler'] as $handler)
                <option value='{{json_encode(array("user_id" => $handler->user_id, "EmployeeID" => $handler->EmployeeID))}}'>{{$handler->EmployeeID}} - {{$handler->NickName}}</option>
            @endforeach
        </select>
    </td>
    <td style='text-align: center;'><textarea class='form-control' name='description[]' id='description_${ctr}' style="height: 100px;"></textarea></td>
    <td style='text-align: center;'><input class='form-control' type='text' name='unit' onkeyup="compute_hrs2(${ctr})" size="1" value='' id='unit_${ctr}'/></td>
    <td>
        <input id="hours_${ctr}" type="text" name="hours[]" class="form-control" readonly size="1"/>
        <input type='hidden' value='0' name='timesheetnum[]' id='timesheetnum_${ctr}'/>
    </td>
    <td><a href="#" class="btn btn-info" onclick='_save(${ctr})' id="save_${ctr}">SAVE</a></td>
</script>
    <script type="text/javascript">
        var lock = true;
        var lock2 = true;
        var count = 0;
        var BillingCodes = {};
        var table = document.getElementById("timesheet_entry");

        $(function(){
            $("._date_control").datepicker({
                maxDate: 0
            });
            $('select').selectize();
            additem({ts : {Comment : '', ChargeHour : 0, UniqueID : 0}});
        });

        function _remove(item){
            var num = count;
            count -= 1;
            console.log((num - 1) - item);
            table.getElementsByTagName('tbody')[0].deleteRow((num - 1) - item);
        }
        function _save(i){
            if(lock){
                lock = false;
                var message     = '';
                var matter_no   = $("#matter_no_" + i).val();
                var handler     = $("#handler_" + i).val();
                var description = $("#description_" + i).val();
                var unit        = parseInt($("#unit_" + i).val());
                var timenum     = $("#timesheetnum_" + i).val();
                console.log(unit);
                if(matter_no.trim().length == 0)
                    message = 'Job Assignment/Matter No is required, please select a matter no.</br>';
                if(handler.trim().length == 0)
                    message += 'Handler is required, please select a handler.</br>';
                if(description.trim().length == 0)
                    message += 'Description is required, please provide.</br>';
                if(unit <= 0 || isNaN(unit))
                    message += 'There must be at least 1 unit.</br>';

                if(message.length > 0){
                    $("#__body_notify").html(message);
                    $("#_modal_notification___").modal().show();
                    lock = true;
                }
                

                if(message.length == 0){
                        var data = {
                        timesheetnum    : {0 : timenum},
                        matter_no       : {0 : matter_no},
                        description     : {0 : description},
                        unit            : {0 : unit},
                        handler         : {0 : handler},
                        date            : {0 : $("#_date_control_" + i).val()},
                        popup           : 0
                    };
                    
                    $.post("/timesheet/savetimesheets",data,function(info){
                        console.log(info);
                        if(timenum == 0){
                            $("#save_" + i).html('UPDATE');
                            $("#timesheetnum_" + i).val(info.obj.timesheets[info.obj.timesheets.length - 1].UniqueID)
                        }
                        $("#_init__").modal('hide');
                        $("#_saving__").modal().show();
                        lock = true;
                    },'json');
                }
            }//end lock
        }
        
        function additem(obj){
            if(lock2){
                lock2 = false;
                obj.ctr = count;
                var newRow = table.getElementsByTagName('tbody')[0].insertRow(0);
                $(newRow).html($("#row_tmpl").tmpl(obj));
                $( "#_date_control_" + obj.ctr).datepicker({
                    maxDate: 0
                });
                $( "#_date_control_" + obj.ctr).datepicker("option","dateFormat","M dd, yy").datepicker('setDate',  new Date());
                $('select').selectize();
                count += 1; 
                lock2 = true;
            }//end lock 2
        }
        

        function compute_hrs2(i){
            console.log(i);
            $("#hours_" + i).val(parseFloat($("#unit_" + i).val() * 5 / 60).formatMoney(2));
        }
        
        function verify2(){
           var success = true; var unit = ''; var description = ''; var matter_no = '';
           var handler = ''; var date = '';
           for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                unit = document.getElementsByName("unit[]")[i].value;
                description =  document.getElementsByName("description[]")[i].value;
                matter_no = document.getElementsByName("matter_no[]")[i].value;
                handler = document.getElementsByName("handler[]")[i].value;
                date = document.getElementsByName("date[]")[i].value;
                if(!unit || parseInt(unit) <= 0 || !description || !matter_no || parseInt(matter_no) <= 0 || !handler || !date){
                    success = false;
                    $("#_modal_notification___").modal().show();
                    break;
                }
            }
            if(success)
                $("#_saving__").modal().show();
            return success;
        }
        
        function verify(){
           var success = true; var unit = ''; var description = ''; var matter_no = '';
           var handler = ''; var date = '';
           for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                unit = document.getElementsByName("unit[]")[i].value;
                description =  document.getElementsByName("description[]")[i].value;
                matter_no = document.getElementsByName("matter_no[]")[i].value;
                handler = document.getElementsByName("handler[]")[i].value;
                date = document.getElementsByName("date[]")[i].value;
                if(!unit || parseInt(unit) <= 0 || !description || !matter_no || parseInt(matter_no) <= 0 || !handler || !date){
                    success = false;
                    $("#_modal_notification___").modal().show();
                    break;
                }
            }
            
            if(success){
                $("#_saving__").modal().show();
                var timesheetnum = [];
                var matter_no = [];
                var description = [];
                var unit =[];
                var handler = [];
                var date = [];

                for(var i = 0; i < document.getElementsByName("unit[]").length; i++){
                    timesheetnum.push(document.getElementsByName("timesheetnum[]")[i].value);
                    matter_no.push(document.getElementsByName("matter_no[]")[i].value);
                    description.push(document.getElementsByName("description[]")[i].value);
                    unit.push(document.getElementsByName("unit[]")[i].value);
                    handler.push(document.getElementsByName("handler[]")[i].value);
                    date.push(document.getElementsByName("date[]")[i].value);
                }

                var data = {
                    timesheetnum    : timesheetnum,
                    matter_no       : matter_no,
                    description     : description,
                    unit            : unit,
                    handler         : handler,
                    date            : date,
                    popup           : 0
                };
                
                $.post("/timesheet/savetimesheets",data,function(info){
                    console.log(info);
                    $("#_saving__").modal("hide");
                });
            }
        }

        @if(Input::has('popup'))
        window.onunload = function() {
            var win = window.opener;
            if (win.closed) {
                win.closeTimesheetCreate();
            }
        };            
        @endif

    </script>
@stop
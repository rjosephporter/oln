@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3>Reports</h3>
        </div>
        <div class="col-xs-12">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#report_6" data-toggle="tab">WIP</a></li>
              <li><a href="#report_1" data-toggle="tab">Matters</a></li>
              <li><a href="#report_2" data-toggle="tab">Handlers</a></li>
              <li><a href="#report_3" data-toggle="tab">Clients</a></li>
              <li><a href="#report_5" data-toggle="tab">Timesheet</a></li>
              <li><a href="#report_4" data-toggle="tab">Others</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="report_1">
                    <div class="row top-buffer">
                        <div class="col-xs-12">

                        <div class="row" style="margin-bottom: 15px;">
                          <div style="margin-left: 15px; margin-top: 15px;">
                            <span><img src="/assets/thisapp/images/matters/green.png" width="25"> Active Last 4 Weeks</span>
                            <span style="margin-left: 10px;"><img src="/assets/thisapp/images/matters/orange.png" width="25"> Last Activity Between 4 and 8 Weeks</span>
                            <span style="margin-left: 10px;"><img src="/assets/thisapp/images/matters/red.png" width="25"> Last Activity Beyond 8 Weeks</span>
                            <span style="margin-left: 10px;"><img src="/assets/thisapp/images/matters/grey.png" width="25">Untouched/No Activity</span>
                            <span style="margin-left: 10px;"><img src="/assets/thisapp/images/matters/blue.png" width="25">Completed Matters</span>
                          </div>
                        </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <form class="form-inline" role="form">
                                      <div class="form-group">
                                        <label for="handler">Handler: </label>
                                        <select id="handler" filter-for="matters" class="form-control">
                                            <option value="0" selected>All</option>
                                            @foreach($handlers as $handler)
                                            <option value="{{ $handler->EmployeeID }}" @if($logged_in_employee == $handler->EmployeeID) selected @endif>{{ $handler->EmployeeID }}</option>
                                            @endforeach
                                        </select>
                                      </div>
                                      <div class="form-group"></div>
                                      <div class="form-group">
                                        <label for="last_activity">Last Activity: </label>
                                        <select id="last_activity" filter-for="matters" class="form-control" style="display:none">
                                            <option value="0" selected>All</option>
                                            <option value="green">Active Last 4 Weeks</option>
                                            <option value="orange">Last Activity Between 4 and 8 Weeks</option>
                                            <option value="red">Last Activity Beyond 8 Weeks</option>
                                            <option value="untouched">Untouched/No Activity</option>
                                            <option value="blue">Completed Matters</option>
                                        </select>
                                        <ul class="btn-folder-list">
                                            <li class="btn-folder" color="0"><span><img src="/assets/thisapp/images/matters/white.png" width="25"></span></li>
                                            <li class="btn-folder" color="green"><span><img src="/assets/thisapp/images/matters/green.png" width="25"></span></li>
                                            <li class="btn-folder" color="orange"><span><img src="/assets/thisapp/images/matters/orange.png" width="25"></span></li>
                                            <li class="btn-folder" color="red"><span><img src="/assets/thisapp/images/matters/red.png" width="25"></span></li>
                                            <li class="btn-folder" color="untouched"><span><img src="/assets/thisapp/images/matters/grey.png" width="25"></span></li>
                                            <li class="btn-folder" color="blue"><span><img src="/assets/thisapp/images/matters/blue.png" width="25"></span></li>
                                        </ul>
                                      </div>
                                      <div class="form-group">
                                        <label for="unbilled_amount">Unbilled Amount: </label>
                                        <select id="unbilled_amount" filter-for="matters" class="form-control">
                                            <option value="0" selected>All</option>
                                            <option value="1">less than $30,000</option>
                                            <option value="2">$30,000 and above</option>
                                        </select>
                                      </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                        
                        <div id="matters-list-wrapper" class="col-xs-12">
                            {{ 
                                Datatable::table()
                                    ->addColumn('Action', 'Matter ID', 'Client ID', 'Client Name', 'Description','Handler','Total Billable Amount', 'Total Unbilled Amount', 'Matter Type', 'Date Created', 'Last Invoice Date', 'Last Invoice Amount')
                                    ->setClass('table table-bordered table-striped cell-border')
                                    ->noScript()
                                    ->render() 
                            }}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="report_2">
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <form class="form-inline" role="form">
                                      <div class="form-group">
                                        <label for="handler_unbilled_amount">Unbilled Amount: </label>
                                        <select id="handler_unbilled_amount" filter-for="handlers" class="form-control">
                                            <option value="0" selected>All</option>
                                            <option value="1">less than $30,000</option>
                                            <option value="2">$30,000 and above</option>
                                        </select>
                                      </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div id="handlers-list-wrapper" class="col-xs-12">
                            {{ 
                                Datatable::table()
                                    ->addColumn('Employee ID','Matters','Total Billed Amount','Total Billable Amount', 'Total Unbilled Amount')
                                    ->setClass('table table-bordered table-striped cell-border')
                                    ->noScript()
                                    ->render() 
                            }}                         
                        </div>
                    </div>
                </div>
                
                <div class="tab-pane" id="report_3">
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <form class="form-inline" role="form">
                                      <div class="form-group">
                                        <label for="client_unbilled_amount">Unbilled Amount: </label>
                                        <select id="client_unbilled_amount" filter-for="clients" class="form-control">
                                            <option value="0" selected>All</option>
                                            <option value="1">less than $30,000</option>
                                            <option value="2">$30,000 and above</option>
                                        </select>
                                      </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div id="clients-list-wrapper" class="col-xs-12">
                            {{ 
                                Datatable::table()
                                    ->addColumn('Client ID','Name','Handler','Matters', 'Total Unbilled Amount')
                                    ->setClass('table table-bordered table-striped cell-border')
                                    ->noScript()
                                    ->render() 
                            }}                         
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="report_5">
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <a href="{{ URL::to('timesheet/report/daily') }}" class="btn btn-block btn-info" target="_blank">Individual Handler - Daily</a>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <a href="{{ URL::to('timesheet/report/weekly') }}" class="btn btn-block btn-info" target="_blank">All Handlers - Weekly</a>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="report_4">
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <form class="form-inline" role="form">
                                      <div class="form-group" style="display:none">
                                        <label for="chart_type">Type: </label>
                                        <select id="chart_type" filter-for="chart" class="form-control">                                            
                                            <option value="matters" selected>Matters</option>
                                            <option value="handlers">Handlers</option>
                                            <option value="clients">Clients</option>                                        
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="chart_year">Year: </label>
                                        <select id="chart_year" filter-for="chart" class="form-control">                                            
                                            <option value="2014" selected>2014</option>
                                            <option value="2013">2013</option>
                                            <option value="2012">2012</option>
                                            <option value="2011">2011</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                        <label for="chart_show">Show: </label>
                                        <select id="chart_show" filter-for="chart" class="form-control">                                            
                                            <option value="total_billed_amount" selected>Total Billed Amount</option>
                                            <option value="total_billable_amount">Total Billable Amount</option>
                                            <option value="total_unbilled_amount">Total Unbilled Amount</option>
                                        </select>
                                      </div>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div id="chart-list-wrapper" class="col-xs-12">
                            <canvas id="myChart" width="1130" height="400"></canvas>         
                        </div>
                    </div>
                </div>
                <div class="tab-pane active" id="report_6">
                    <div class="row top-buffer">
                        <div class="col-xs-12">
                            <h3>UNDER CONSTRUCTION</h3>
                            <div id="wip-list-wrapper" class="col-xs-12">
                                {{ 
                                    Datatable::table()
                                        ->addColumn('Matter ID', 'Client ID', 'Client Name', 'Description','Handler', 'WIP', 'Matter Type')
                                        ->setClass('table table-bordered table-striped cell-border')
                                        ->noScript()
                                        ->render() 
                                }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modals -->
<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Matter ID: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
        
        <h5><i class="fa fa-info-circle text-info"></i> Send reminder to handler?</h5>

        <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
        <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">

        <div class="form-group">
          <label for="reminder_recipient">To</label>
          <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Handler's Email" required readonly>
        </div>

        <div class="form-group">
          <label for="reminder_message">Message</label>
          <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
          </textarea>
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- /Modals -->
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/Chart.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/modal_matter.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/reports.js"></script>
@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/login.css" rel="stylesheet">
<div class="container">
    <div class="row" style="height:100%">
        <div class="col-xs-12">
            <div class="modal show">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body">
                        <form class="form-horizontal" method="post">
                          <h3>Login as:</h3>
                          <fieldset>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button class="btn btn-primary btn-block btn-handler" for="ADMIN">ADMIN</button>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button class="btn btn-primary btn-block btn-handler" for="GDO">GDO</button>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button class="btn btn-primary btn-block btn-handler" for="VYS">VYS</button>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <button class="btn btn-primary btn-block btn-handler" for="SAW">SAW</button>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-lg-12">
                                <a class="btn btn-danger btn-block" href="/auth/logout">Cancel</a>
                              </div>
                            </div>
                          </fieldset>
                          <input type="hidden" id="chosen_handler" name="chosen_handler">
                        </form>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>
</div>
<script>

function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('document').ready(function(){
    $('.modal').each(centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });
});

$('.btn-handler').each(function(index, value) {
  console.log($(this).attr('for'));
});

$('.btn-handler').click(function(e) {
  e.preventDefault();
  var chosen_handler = $(this).attr('for');
  $('#chosen_handler').val(chosen_handler);
  $(this).closest('form').submit();
});

</script>
@stop
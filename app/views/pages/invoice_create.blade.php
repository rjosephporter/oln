@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-sm btn-info" href="{{ URL::to('invoice') }}" style="margin-bottom:10px"><i class="fa fa-arrow-left"></i> Back to Invoice List</a>
			<div class="panel panel-default">
				<div class="panel-heading">
				@if($form_type == 'new')
					<h3 class="panel-title">New Invoice</h3>
				@else
					<h3 class="panel-title">Update Invoice: {{ $invoicehead_id }}</h3>
				@endif
				</div>
				<div class="panel-body">
					<form role="form" id="invoice-form">
						<input type="hidden" id="form-type" name="form_type" value="{{ $form_type }}">
						<!-- INVOICE HEADER -->
						<div class="row">
							<!-- LEFT COLUMN -->
							<div class="col-md-6">
								<div class="form-group">
									<label for="matter_id">Matter ID 
										@if($form_type == 'new')
											<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Search for Matter ID or Description and choose from the list of results."></i>

											@if( Input::has('matter_id') )
												<button class="btn btn-xs btn-danger" id="clear-matter-id">Clear</button>
											@endif

										@endif	
									</label>
									@if($form_type == 'new')
										@if(Input::has('matter_id'))
										<input type="text" class="form-control" id="matter_id" name="matter_id" value="{{ Input::get('matter_id') }}" readonly>
										@else
										<select class="form-control" id="matter_id" name="matter_id" placeholder="Search Matter">
										@endif
									@else
										<input type="text" class="form-control" id="matter_id" name="matter_id" readonly>
									@endif
									</select>
								</div>
								<div class="form-group">
									<label for="matter_description">Matter Description</label>
									<!-- <p class="form-control-static" id="matter_description">Commercial Dispute (Dummy Text)</p> -->
									<input type="text" class="form-control" id="matter_description" name="matter_description" placeholder="" value="{{ $matter_description or '' }}" readonly>
								</div>
								<div class="form-group">
									<label for="client_id">Client ID</label>
									<!-- <p class="form-control-static" id="client_id">23456 (Dummy Text)</p> -->
									<input type="text" class="form-control" id="client_id" name="client_id" placeholder="" value="{{ $client_id or '' }}" readonly>
								</div>
								<div class="form-group">
									<label for="client_name">Client Name</label>
									<!-- <p class="form-control-static" id="client_id">ABC Company Inc.</p> -->
									<input type="text" class="form-control" id="client_name" name="client_name" placeholder="" value="{{ $client_name or '' }}" readonly>
								</div>								
							</div>
							<!-- END LEFT COLUMN -->
							
							<!-- RIGHT COLUMN -->
							<div class="col-md-6">
								<div class="form-group" style="">
									<label for="reference_number">Reference No.</label>
									<input type="text" class="form-control" id="reference_number" name="reference_number" placeholder="Enter Reference Number" value="">									
								</div>
								<div class="form-group" style="">
									<label for="new_invoice_number">Invoice No.</label>
									<input type="text" class="form-control" id="new_invoice_number" name="new_invoice_number" placeholder="Enter Invoice Number" value="">									
								</div>

								<div class="form-group" style="display:none">
									<!-- <label for="invoice_number">Invoice No.</label> -->
									<label for="invoice_number">Reference No.</label>
									<input type="text" class="form-control" id="invoice_number" name="invoice_number" placeholder="Enter Invoice Number" value="{{ $invoicehead_id }}" readonly>									
								</div>
								<div class="form-group" style="display:none">
									<!-- <label for="invoice_batch_number">Invoice Batch No.</label> -->
									<label for="invoice_batch_number">Invoice No.</label>
									<input type="text" class="form-control" id="invoice_batch_number" name="invoice_batch_number" placeholder="Enter Invoice Batch Number" value="{{ $invoicehead_batchno }}" readonly>									
								</div>										

								<div class="form-group">
									<label for="invoice_date">Invoice Date</label>
									<input type="text" class="form-control" id="invoice_date" name="invoice_date" placeholder="Enter Invoice Date">									
								</div>								
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
						<!-- END INVOICE HEADER -->
						<hr/>
						<!-- INVOICE ITEMS -->
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<table class="table table-bordered" style="display:none">
											<col width="5%">
											<col width="75%">
											<col width="20%">
											<thead>
												<tr>
													<th colspan="3">
														<strong>Timesheet</strong>
														&nbsp;&nbsp;
														<button class="btn btn-xs btn-success" id="add-timesheet" data-target="#invoice-timesheet"><i class="fa fa-plus"></i> Add</button>
													</th>														
												</tr>
											</thead>

											<tbody id="timesheet-items">
												<tr>
													<td><strong>No.</strong></td>
													<td><strong>Description</strong></td>
													<td><strong>Amount</strong></td>
												</tr>												
												<!--
												<tr>
													<td colspan="3">
														<strong>Timesheet</strong>
														&nbsp;&nbsp;
														<button class="btn btn-xs btn-success" id="add-timesheet" data-target="#invoice-timesheet"><i class="fa fa-plus"></i> Add</button>
													</td>
												</tr>
												-->

												<tr id="no_timesheet_msg">
													<td colspan="3"><i class="fa fa-info-circle text-info"></i> No timesheet added.</td>
												</tr>
											</tbody>
										</table>

										<table class="table table-bordered">
											<col width="5%">
											<col width="15%">
											<col width="30%">
											<col width="15%">
											<col width="15%">
											<col width="20%">											
											<!-- COSTS BY HANDLER -->
											<thead>
												<tr>
													<th colspan="6">
														<strong>Costs</strong>
													</th>
												</tr>												
											</thead>
											<tbody id="cost-items">
												<tr>
													<td></td>
													<td><strong>Handler</strong></td>
													<td><strong>Hourly Rate</strong></td>
													<td><strong>Total Hours</strong></td>
													<td><strong>Complimentary</strong></td>
													<td><strong>Amount</strong></td>
												</tr>																					
											</tbody>
											<tfoot>
												<tr>
													<td colspan="5" class="text-right"><strong>Total Costs</strong></td>
													<td><strong><span id="total_cost_amount">$0.00</span></strong></td>
												</tr>
												<tr>
													<td colspan="5" class="text-right"><strong>Agreed Costs</strong></td>
													<td>
														<div class="input-group">
															<span class="input-group-addon input-sm">$</span>
															<input type="number" step="any" name="agreed_cost_amount" class="form-control input-sm amount" placeholder="None">
														</div>
													</td>
												</tr>												
											</tfoot>
											<!-- END COSTS BY HANDLER -->
										</table>

										<table class="table table-bordered">
											<col width="5%">
											<col width="75%">
											<col width="20%">											
											<tbody id="disbursement-items">
												<tr>
													<td colspan="3">
														<strong>Disbursements</strong>
														&nbsp;&nbsp;
														<button class="btn btn-xs btn-success" id="add-disbursement"><i class="fa fa-plus"></i> Add</button>														
													</td>
												</tr>
												
												<tr id="no_disbursement_msg">
													<td colspan="3"><i class="fa fa-info-circle text-info"></i> No disbursement added.</td>
												</tr>									

											</tbody>
											<tfoot id="tfoot-total-disbursements">
												<tr>
													<td colspan="2" class="text-right"><strong>Total Disbursements</strong></td>
													<td><strong><span id="total_disbursements_amount">$0.00</span></strong></td>
												</tr>										
											</tfoot>											
										</table>

										<table class="table table-bordered">
											<col width="5%">
											<col width="75%">
											<col width="20%">											
											<!-- LESS -->
											<tbody id="less-items">
												<tr>
													<td colspan="3">
														<strong>Less</strong>
														&nbsp;&nbsp;
														<button class="btn btn-xs btn-success" id="add-less"><i class="fa fa-plus"></i> Add</button>
													</td>
												</tr>												

												<tr id="no_less_msg">
													<td colspan="3"><i class="fa fa-info-circle text-info"></i> No less added.</td>
												</tr>												

											</tbody>
											<tfoot id="tfoot-total-less">
												<tr>
													<td colspan="2" class="text-right"><strong>Total Less</strong></td>
													<td><strong><span id="total_less_amount">$0.00</span></strong></td>
												</tr>										
											</tfoot>											
											<!-- END LESS -->
										</table>

										<table class="table table-bordered">
											<col width="80%">
											<col width="20%">											
											<tfoot>
												<tr>
													<td class="text-right"><strong>TOTAL INVOICE</strong></td>
													<td><strong><span id="total-invoice-amount" class="double-underline">$0.00</span></strong></td>
												</tr>										
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!-- END INVOICE ITEMS -->

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="remarks">Remarks</label>
									<textarea class="form-control" id="remarks" name="remarks" placeholder="Enter Remarks"></textarea>
								</div>								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-right">
								<a href="{{ URL::to('invoice/print/' . $invoicehead_id) }}" target="_blank" class="btn btn-info" id="print-preview"><i class="fa fa-print"></i> Print Preview</a> 
								<button type="submit" redirect-url="{{ URL::to('invoice/update/' . $invoicehead_id) }}" id="save-invoice" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
								<a href="{{ Request::url() }}" class="btn btn-danger" id="reset-form"><i class="fa fa-undo"></i> Reset</a>
							</div>						
						</div>
					</form>
				</div>
			</div>			
		</div>
	</div>
</div>

<!-- REFERENCE DOMs -->
<table id="reference-table" style="display:none">

	<tr class="timesheet-item" number="">
		<td class="text-center"><i class="fa fa-times text-danger remove-timesheet" unique-id="" style="cursor:pointer"></i> <span class="item-number"></span></td>
		<td class="description"></td>
		<td class="amount"></td>
	</tr>

	<tr class="cost-item" number="">
		<td class="show-timesheet text-center">
			<button class="btn btn-sm btn-success btn-show-timesheet" employee-id="" title="Show Timesheet"><i class="fa fa-list"></i></button>
		</td>
		<td class="handler">
			<input type="text" step="any" name="handler[]" class="form-control input-sm amount" placeholder="Handler" required readonly>
		</td>
		<td class="hourly_rate">
			<div class="input-group">
				<span class="input-group-addon input-sm">$</span>
				<input type="number" step="any" name="hourly_rate[]" class="form-control input-sm amount" placeholder="Hourly Rate" required readonly>
			</div>
		</td>
		<td class="total_work_hours">
			<input type="number" step="any" name="total_work_hours[]" class="form-control input-sm amount" placeholder="Hourly Rate" required readonly>
		</td>
		<td class="complimentary text-center">
			<input type="checkbox" name="complimentary[]" class="input-sm">
		</td>
		<td class="amount">
			<div class="input-group">
				<span class="input-group-addon input-sm">$</span>
				<input type="text" name="amount[]" class="form-control input-sm amount" placeholder="Amount" required readonly>
			</div>			
		</td>
	</tr>

	<tr class="disbursement-item" number="">
		<td class="text-center"><i class="fa fa-times text-danger remove-disbursement" style="cursor:pointer"></i> <span class="item-number"></span></td>
		<td><input type="text" name="description[]" class="form-control input-sm description" placeholder="Description" required></td>
		<td>
			<div class="input-group">
				<span class="input-group-addon input-sm">$</span>
				<input type="number" step="any" name="amount[]" class="form-control input-sm amount" placeholder="Amount" required>
			</div>
		</td>
	</tr>

	<tr class="less-item" number="">
		<td class="text-center"><i class="fa fa-times text-danger remove-less" style="cursor:pointer"></i> <span class="item-number"></span></td>
		<td><input type="text" name="description[]" class="form-control input-sm description" placeholder="Description" required></td>
		<td>
			<div class="input-group">
				<span class="input-group-addon input-sm">$</span>
				<input type="number" step="any" name="amount[]" class="form-control input-sm amount" placeholder="Amount" required>
			</div>
		</td>
	</tr>

	@if(isset($timesheet_by_handler))
		@foreach($timesheet_by_handler as $handler => $timesheet)
		<tr class="handler-timesheet" handler-id="{{ $handler }}">
			<td colspan="6">
				<table class="table table-bordered">
					<col width="5%">
					<col width="5%">
					<col width="30%">
					<col width="10%">
					<col width="15%">
					<col width="10%">
					<col width="20%">
					<col width="5%">
					<thead>
						<tr>
							<td><strong>Work Date</strong></td>
							<td><strong>Employee</strong></td>
							<td><strong>Description</strong></td>
							<td><strong>Hourly Rate</strong></td>
							<td><strong>Work Hour</strong></td>
							<td><strong>Units</strong></td>
							<td><strong>Amount</strong></td>
							<td><strong>Include</strong></td>
						</tr>
					</thead>
					<tbody>
						@foreach($timesheet as $item)
						<tr>
							<td>{{ date('Y-m-d', strtotime($item->WorkDate)) }}</td>
							<td>{{ $item->EmployeeID }}</td>
							<td>{{ $item->Comment }}</td>
							<td>${{ number_format($item->HourlyRate,2) }}</td>
							<td>{{ round($item->WorkHour,4) }}</td>
							<td>{{ round(($item->WorkHour * 60) / 5, 4) }}</td>
							<td>${{ number_format($item->WorkHour * $item->HourlyRate, 2) }}</td>
							<td class="text-center"><input type="checkbox" class="chkbox-include-item" unique-id="{{ $item->UniqueID }}" {{ isset($item->billed) ? 'checked' : '' }}></td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</td>
		</tr>
		@endforeach
	@endif

</table>
<!-- END REFERENCE DOMs -->

<!-- MODALS -->
<div class="modal fade" id="invoice-timesheet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Item from Timesheet <small>Click on the row(s) you wish to add into the invoice.</small></h4>
      </div>
      <div class="modal-body">
			<div class="row" id="daterange-wrapper" style="margin-bottom: 15px">
				<div class="col-md-12">
					<form id="daterange-form" class="form-inline" role="form" method="get">
						<div class="form-group">
							<label for="from_date">From</label>
							<input type="text" class="form-control" id="from_date" name="from_date" placeholder="Choose Date">
						</div>
						<div class="form-group">
							<label for="to_date">To</label>
							<input type="text" class="form-control" id="to_date" name="to_date" placeholder="Choose Date">
						</div>
						<button type="submit" class="btn btn-default">Apply</button>
					</form>
				</div>
			</div>             
      		<div id="timesheet-wrapper">
            {{ 
                Datatable::table()
                    ->addColumn('Matter ID', 'Employee ID','Work Hour','Hourly Rate', 'Description', 'Billing Status', 'Unique ID', 'Work Date', 'Owned', 'Update/Delete', 'Add')
                    ->setClass('table table-bordered cell-border')
                    ->noScript()
                    ->render() 
            }}
        	</div>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn btn-info" id="add-to-invoice">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- END MODALS -->
@if(isset($timesheet_by_handler))
<script>
var timesheet_by_handler = {{ json_encode($timesheet_by_handler) }};
</script>
@endif

<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/invoice_create.js"></script>
@stop
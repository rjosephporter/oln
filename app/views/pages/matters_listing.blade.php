@extends('template/default')
@section('content')

@if($obj['user'] == 'admin' || $obj['user'] == $obj['handler'])
<?php
    $billable = 0;
    $billed = 0;
    $unbilled = 0;
    $ctr = 0;
?>
<style>
  .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {
    background-color: skyblue;
    color: black;
}
.my_borders{
    margin-top: 5px; margin-bottom: 5px; margin-right: 5px;
}
	.alert-box {
		color:#555;
		border-radius:10px;
		font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
		padding:10px 36px;
		margin:10px;
	}
	.alert-box span {
		font-weight:bold;
		text-transform:uppercase;
	}

	.notice {
		background:#e3f7fc url('/assets/thisapp/images/app/notice-icon.png') no-repeat 10px 50%;
		border:1px solid #8ed9f6;
	}
</style>
<link rel="stylesheet" href="/assets/DataTables-1.10.0/extensions/TableTools/css/dataTables.tableTools.css">

<input type="hidden" id="billable" value="0">
<input type="hidden" id="billed" value="0">
<input type="hidden" id="unbilled" value="0">

    <div class="row">
        <div class="col-md-12">
            <div style="margin-left: 10px; margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-8"><span class="h3" style="padding-left: 15px;">{{ strtoupper($obj['ip']) }} @if($obj['delegated']) {{ ucfirst($obj['type']) }} Matters Delegated {{ $obj['delegated'] }} {{ $obj['handler'] }} @else {{ ucfirst($obj['type']) }} Matters {{$obj['client'] != 'all' ? ' for Client: '.$obj['matters'][0]->client_name : ''}}{{$obj['handler'] != 'all' && $obj['client'] == 'all'? ' for Handler: '.$obj['handler'] : ''}} @endif</span></div>
                    <div class="col-md-3" style="text-align: right;"> 
                        @if($obj['handler'] != 'all' && $obj['user'] == 'admin' && $obj['reg_user'] != $obj['handler'])
                        <a href="#" onclick="send_all()"><button class="btn btn-md btn-info">Email {{ ucfirst($obj['type']) }} Matters to {{ $obj['handler'] }}</button></a>
                        @endif
                        <a href="/matters/new" target="_blank"><button class="btn btn-sm btn-info">Create New Matter</button></a>
                        
                    </div>
                    <div class="col-md-1" id="help"></div>
                </div>

                <div style="margin-left: 15px; margin-top: 15px;">
                    <span><a style="color: black; font-size: 13px;" href="/matters/view/7/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}"><img src="/assets/thisapp/images/matters/white.png" width="20"/>&nbsp;View All Matters</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/1/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}"><img src="/assets/thisapp/images/matters/green.png" width="20"/>&nbsp;Active Last 4 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/2/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}" style="color: orange;"><img src="/assets/thisapp/images/matters/orange.png" width="20"/>&nbsp;Last Activity Between 4 and 8 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/3/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}" style="color: red;"><img src="/assets/thisapp/images/matters/red.png" width="20"/>&nbsp;Last Activity Beyond 8 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/5/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}" style="color: gray;"><img src="/assets/thisapp/images/matters/grey.png" width="20"/>&nbsp;Untouched/No Activity</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/4/{{$obj['client']}}/{{$obj['handler']}}?deleg={{$obj['delegated']}}&type={{$obj['ip']}}" style="color: blue;"><img src="/assets/thisapp/images/matters/blue.png" width="20"/>&nbsp;Completed Matters</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="alert-box notice" role="alert" style="text-align: center; vertical-align: middle;" id="my_info_preloader">
        <h4>. . . Processing and Loading Data, Please Wait . . .</h4>
        <img src="/assets/thisapp/images/ajax-loader-bar.gif">
    </div>
    <div class="alert-box notice" role="alert" style="text-align: center; vertical-align: middle; display: none;" id="my_email_preloader">
        <h4>. . . {{ ucfirst($obj['type']) }} Matters Listing is now being sent to {{ $obj['handler'] }}, Please Wait . . .</h4>
        <img src="/assets/thisapp/images/ajax-loader-bar.gif">
    </div>
    <div class="row">
        <div class="col-md-12">    
            <table class="table table-hover table-bordered cell-border" id="handler_modal_table" style="display: none;">                        
                <thead>
                    <tr>
                      <th colspan="6" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                      <th class="h_billable" style="text-align: right;"></th>
                      <th class="h_billed" style="text-align: right;"></th>
                      <th class="h_unbilled" style="text-align: right;"></th>
                      <th>&nbsp;</th>
                  </tr>
                  <tr>
                      <th>&nbsp;</th>
                      <th>
                        @if($obj['type'] == 'gray')
                            Designate Date
                        @else
                            Last Trans Date
                        @endif
                      </th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Client Name">Client</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter Name">Reference</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter ID">Matter</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Handler">{{ $obj['delegated'] == '' ? 'Handler' : ($obj['delegated'] == 'by' ? 'Delegated To' : 'Delegated By' )}}</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billable Amount" style="text-align: right; width:100%;">Billable Amount</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billed Amount" style="text-align: right; width:100%">Billed Amount</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Unbilled Amount" style="text-align: right; width: 100%;">Unbilled Amount</div></th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <th colspan="6" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                      <th class="h_billable" style="text-align: right;"></th>
                      <th class="h_billed" style="text-align: right;"></th>
                      <th class="h_unbilled" style="text-align: right;"></th>
                      <th>&nbsp;</th>
                  </tr>
              </tfoot>
              <tbody>
                 @foreach($obj['matters'] as $matter)
                 <?php 
                    $billable += $matter->billable_amount;
                    $billed += $matter->billed_amount;
                    $unbilled += $matter->unbilled;
                 ?>
                 <tr class="mclss" style="color: {{ $matter->matter_type == 'blue' ? 'white' : 'black' }}; background-color: {{ $matter->matter_type == 'blue' ? "#0075b0"  : ($matter->matter_type == 'green' ? "rgba(0, 128, 0, 0.5)" : ($matter->matter_type == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($matter->matter_type == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
                    <td style="text-align: center;"><img src="/assets/thisapp/images/matters/{{ $matter->matter_type == "gray" ? "grey" : $matter->matter_type }}.png" width="25"></td>
                    <td>@if($matter->matter_type == 'gray')
                            {{date_format(date_create($matter->DesignatedDate),"M d, Y")}}
                        @else
                            {{date_format(date_create($matter->last_trans_date),"M d, Y")}}
                        @endif
                    </td>
                    <td style="text-align: left;">{{$matter->client_name}}</td>
                    <td style="text-align: left;">{{$matter->Description_A}}</td>
                    <td style="text-align: center;">{{$matter->JobAssignmentID}}</td>
                    <td style="text-align: center;">{{ $obj['delegated'] == '' ? $matter->Incharge : $obj['deleg_handlers'][$ctr]}}</td>
                    <td style='text-align: right;'>${{ number_format($matter->billable_amount, 2, '.', ',') }}</td>
                    <td style='text-align: right;'>${{ number_format($matter->billed_amount, 2, '.', ',') }}</td>
                    <td style='text-align: right;'>${{ number_format($matter->unbilled, 2, '.', ',') }}</td>
                    <td valign="middle" style="text-align: center; cursor: pointer; vertical-align: middle; margin: auto;">
                        @if(($matter->matter_type == 'blue' ||  $matter->CompleteStatus >= 1) && $obj['openrights'] == 1)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Reopen Matter: {{$matter->JobAssignmentID}}"><button class="btn btn-xs btn-warning my_borders" onclick="reopenMatter({{$matter->JobAssignmentID}},'{{$obj['current']}}')"><i class="glyphicon glyphicon-eye-open"></i></button></div>
                        @endif
                        @if($obj['delegated'] == 'to')
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Decline Delegated Matter: {{$matter->JobAssignmentID}}"><button class="btn btn-xs btn-danger my_borders" onclick="declineMatter({{$matter->JobAssignmentID}},'{{$obj['current']}}')">D</button></div>
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Mark Tasks Complete for Delegated Matter: {{$matter->JobAssignmentID}}"><button class="btn btn-xs btn-success my_borders" onclick="completeMatter({{$matter->JobAssignmentID}},'{{$obj['current']}}')">C</button></div>
                        @endif
                        @if($matter->matter_type == 'blue' && $obj['role'] == 'others' && $matter->CompleteStatus == 0)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Add Docket/Box No. To Complete Legally Closing Matter: {{$matter->JobAssignmentID}}"><button class="btn btn-xs btn-danger my_borders" onclick="_closeMatter({{$matter->JobAssignmentID}},1)"><img src='/assets/thisapp/images/app/justice_scale.png' style="height: 15px; width: 13px;"/></button></div>
                        @endif
                        @if($obj['ip'] == '')
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Mark Matter: {{$matter->JobAssignmentID}} as IP"><button class="btn btn-xs btn-danger my_borders" onclick="setMtoIP({{$matter->JobAssignmentID}})">IP</button></div>
                        @endif
                        @if($obj['ip'] == 'ip')
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Move IP Matter: {{$matter->JobAssignmentID}} to Regular Matters"><button class="btn btn-xs btn-danger my_borders" onclick="setIPtoM({{$matter->JobAssignmentID}})">M</button></div>
                        @endif
                        @if($matter->matter_type == 'red' && $obj['role'] == 'accounting' && $matter->CompleteStatus == 0 && $matter->Completed == 1)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Matter: {{$matter->JobAssignmentID}} needs to be Legally Closed Completely, Please contact {{$matter->Incharge}}"><button class="btn btn-xs btn-info my_borders"><img src='/assets/thisapp/images/app/justice_scale.png' style="height: 15px; width: 13px;"/></button></div>
                        @endif
                        @if($obj['user'] == "admin" || $matter->Incharge == $obj['reg_user'])
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="View Matter: {{$matter->JobAssignmentID}}"><a href="/matters/update/{{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info my_borders"><i class="glyphicon glyphicon-edit"></i></button></a></div>
                        @endif
                        @if($obj['user'] == "admin" && $matter->Incharge != $obj['reg_user'] && floor(floatval($matter->unbilled)) > 0)
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Send Reminder to {{$matter->Incharge}}"><button class="btn btn-xs btn-info my_borders" onclick="dis({{$matter->JobAssignmentID}})"><i class="fa fa-envelope"></i></button></div>
                        @endif
                        @if($matter->matter_type != 'blue')
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Create Timesheet for {{$matter->JobAssignmentID}}"><a href="/timesheet/new?matter_id={{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info my_borders"><i class="glyphicon glyphicon-time"></i></button></a></div>
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Create Invoice for {{$matter->JobAssignmentID}}"><a href="/invoice/create-new?matter_id={{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info btn-send-reminder my_borders"><i class="glyphicon glyphicon-th-list"></i></button></a></div>
                            @if($matter->Incharge && $obj['current'] != 'ADMIN')
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Delegate Matter {{$matter->JobAssignmentID}} to a Handler" onclick="_delegateMatter({{$matter->JobAssignmentID}})"><button class="btn btn-xs btn-warning btn-send-reminder my_borders"><i class="fa fa-user"></i></button></div>
                            @else
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Delegate Matter {{$matter->JobAssignmentID}} to a Handler" onclick="__notifyAdminDelegate()"><button class="btn btn-xs btn-warning btn-send-reminder my_borders"><i class="fa fa-user"></i></button></div>
                            @endif
                            @if($matter->Incharge == '')
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Assign Matter {{$matter->JobAssignmentID}} to a Handler"><a href="/invoice/create-new?matter_id={{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info btn-send-reminder my_borders"><i class="fa fa-user"></i></button></div>
                            @endif
                        @endif
                        @if($matter->CompleteStatus == 0 && $matter->matter_type != 'blue')
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Legally Close Matter: {{$matter->JobAssignmentID}}" onclick="_closeMatter({{$matter->JobAssignmentID}},1)"><button class="btn btn-xs btn-danger my_borders"><img src='/assets/thisapp/images/app/justice_scale.png' style="height: 15px; width: 13px;"/></button></div>
                        @endif
                        @if($matter->matter_type == 'red' && $obj['role'] == 'accounting' && $matter->CompleteStatus == 1)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Accounting/Completely Close Matter: {{$matter->JobAssignmentID}}" onclick="_closeMatter({{$matter->JobAssignmentID}},2)"><button class="btn btn-xs btn-danger my_borders"><i class="fa fa-usd"></i></button></div>
                        @endif
                    </td>
                </tr>
                <?php $ctr++; ?>
                  @endforeach
              </tbody>
            </table>
        </div>
    </div>

<!-- Open Matter -->
<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>

<!-- Send Reminder to Handler  -->
<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Matter ID: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
          <div>
                <h5><i class="fa fa-info-circle text-info"></i> Send reminder to handler?</h5>
                <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
                <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">
                <div class="form-group">
                  <label for="reminder_recipient">To</label>
                  <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Handler's Email" required readonly>
                </div>
                <div style="text-align: center; vertical-align: middle;" id="rem_preloader"></div>
                <div class="form-group">
                  <label for="reminder_message">Message</label>
                  <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
                  </textarea>
                </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- End Matter Legally Close  -->
<div class="modal fade" id="complete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">End Matter <span class="matter_for_completion"></span></h4>
      </div>
      <form id="matter_completion_form" role="form" method="post" class="my_confirm_forms">
      <div class="modal-body confirm_body_modal"style="text-align: center; vertical-align: middle;">
        <h3 style="color: red;">This action will mark Matter <span class="matter_for_completion"></span> as ENDED. Do you want to proceed?</h3>

        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-file"></i> Docket/Box No.:</div>
            <input class="form-control" type="text" name="box_number_temp" value="" placeholder=""/>
          </div>
        </div>

      </div>
          <input type="hidden" value="" name="box_number" placeholder=""/>
          <input type="hidden" value="" name="matter_id" class="matter_for_completion"/>
          <input type="hidden" value="1" name="CloseStatus"/>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Accounting Close Matter -->
<div class="modal fade" id="final_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforfinalConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforfinalConfirmComplete">Accounting/Completely Close Matter <span class="matter_for_completion"></span></h4>
      </div>
      <form id="matter_completion_form" role="form" method="post" class="my_confirm_forms">
      <div class="modal-body confirm_body_modal" style="text-align: center; vertical-align: middle;">
        <h3 style="color: red;">This action will mark Matter <span class="matter_for_completion"></span> as Accounting and Completely Closed. Do you want to proceed?</h3>

        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-file"></i> Docket/Box No.:</div>
            <input class="form-control" type="text" name="box_number_temp" value="" placeholder=""/>
          </div>
        </div>

      </div>
          <input type="hidden" value="" name="box_number" placeholder=""/>
          <input type="hidden" value="" name="matter_id" class="matter_for_completion"/>
          <input type="hidden" value="2" name="CloseStatus"/>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- @begin Matter Delegation by Admin -->
<div class="modal fade" id="__delegated_by_Admin">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Delegate Matter</h4>
      </div>
      <div class="modal-body">
        <p style="color: red;">Only Handler Accounts can delegate and be delegated with matters. Please Login or Change your View as a Handler to Delegate Matters.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- @end Matter Delegation by Admin -->

<!-- Matter Delegation -->
<div class="modal fade" id="delegate_Matter" tabindex="-1" role="dialog" aria-labelledby="myModalLabelfordelegate_Matter" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelfordelegate_Matter">Delegate Matter <span class="matter_for_delegation"></span></h4>
      </div>
        <form role="form" method="post" action='/matters/delegate' id="_form_for_delegation_">
        <div style="text-align: center; display: inherit;" id="deleg_init__">
            <h4>. . . Initializing, Please Wait . . .</h4>
            <img src="/assets/thisapp/images/ajax-loader-bar.gif">
        </div>
        <div style="text-align: center; display: none;" id="confirm_for_delegation">
            <h4>. . . Delegating Matter <span class="matter_for_delegation"></span> , Please Wait . . .</h4>
            <img src="/assets/thisapp/images/ajax-loader-bar.gif">
        </div>
            
          <input type="hidden" value="" name="matter_id" class="matter_for_delegation"/>
          <input type="hidden" value="{{ $obj['current'] }}" name="delegated_by"/>
          <input type="hidden" value="" name="matter_description"/>
          <input type="hidden" value="" name="matter_client"/>
      <div class="modal-body confirm_delegate_Matter_modal" style="text-align: center; vertical-align: middle; display: none;"></div>  
      <div class="modal-footer">
          <button class="btn btn-default" id="saving_for_delegation" style="display: none;">Save</button>
        <button class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- @begin Matter Delegation Decline/Complete -->
<div class="modal fade" id="m_deleg_compl">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Matter Delegation</h4>
      </div>
      <div class="modal-body">
        <h3 style="color: green; text-align: center;" id="m_body_deleg_com_dec"></h3>
        <input type="hidden" id="deleg_type" value=""/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary __deleg_com_dec_ok" onclick="_process_dec_com()" style="display: inherit; width: 70px; float: right;">OK</a>
      </div>
    </div>
  </div>
</div>
<!-- @end Matter Delegation Decline/Complete -->

<!-- Marking IP -->
<div class="modal fade" id="_marking_ip_modal__" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforIP_Matter" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforIP_Matter">Move Matter to IP</h4>
      </div>
        <div class="modal-body" style="text-align: center; vertical-align: middle;">
            <div style="text-align: center; display: inherit;" id="_ip_init__">
                <h4>. . . Initializing, Please Wait . . .</h4>
                <img src="/assets/thisapp/images/ajax-loader-bar.gif">
            </div>
            <div id="div_for_ip_moving" style="display: nope;">
                <h4>Move <span class="ip_id_marker"></span> to IP Matters?</h4>
            </div>
            <div id="div_for_ip_confirming" style="display: none;">
                <h4>. . . Moving <span class="ip_id_marker"></span> to IP Matters, Please Wait . . .</h4>
                <img src="/assets/thisapp/images/ajax-loader-bar.gif">
            </div>
            <h4 id="_modal_body_ip"></h4>
            <input type="hidden" value="" id="ip_matter_id__"/>
        </div>  
      <div class="modal-footer">
          <a href="#" onclick="processmatterip()" class="btn btn-default ip_reg_matters_anchor" style="display: none;">Yes</a> 
        <button class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
  
    </div>
  </div>
</div>

<!-- Marking Regular -->
<div class="modal fade" id="_moving_ip_to_reg_modal__" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforIP_MattertoRegular" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforIP_MattertoRegular">Move IP Matter to Regular</h4>
      </div>
        <div class="modal-body" style="text-align: center; vertical-align: middle;">
            <div style="text-align: center; display: inherit;" id="_ip_init__to_reg">
                <h4>. . . Initializing, Please Wait . . .</h4>
                <img src="/assets/thisapp/images/ajax-loader-bar.gif">
            </div>
            <div id="div_for_ip_moving_reg" style="display: nope;">
                <h4>Move IP Matter <span class="ip_id_marker"></span> to Regular Matters?</h4>
            </div>
            <div id="div_for_ip_confirming_reg" style="display: none;">
                <h4>. . . Moving IP Matter <span class="ip_id_marker"></span> to Regular Matters, Please Wait . . .</h4>
                <img src="/assets/thisapp/images/ajax-loader-bar.gif">
            </div>
            <h4 id="_modal_body_ip_reg"></h4>
            <input type="hidden" value="" id="ip_matter_id__reg"/>
        </div>  
      <div class="modal-footer">
        <a href="#" onclick="moveiptoregular()" class="btn btn-default ip_reg_matters_anchor">Yes</a> 
        <button class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
  
    </div>
  </div>
</div>

<!-- @begin Reopening preloader -->
<div class="modal fade" id="reopen_preloader">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Reopening Matter</h4>
      </div>
      <div class="modal-body" style="text-align: center;">
        <h4>. . . Processing and Reopening Matter, Please Wait . . .</h4>
        <img src="/assets/thisapp/images/ajax-loader-bar.gif">
      </div>
    </div>
  </div>
</div>
<!-- @end Reopening preloader-->

<!-- @begin Reopening of closed matter -->
<div class="modal fade" id="m_reopen__">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Reopen Matter</h4>
      </div>
      <div class="modal-body">
        <h3 style="color: green; text-align: center;">This will reopen the closed/complete matter: <span id="m_repen_id"></span>. Do you wish to proceed?</h3>
        <input type="hidden" value="0" id="matter_to_open"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary __deleg_com_dec_ok" onclick="_final_process_open()" style="display: inherit; width: 70px; float: right;">OK</a>
      </div>
    </div>
  </div>
</div>
<!-- @end Reopening of closed matter -->

<link type="text/css" href="/assets/thisapp/css/matters.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script src="/assets/DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<!-- <script type="text/javascript" src="/assets/thisapp/js/modal_matter_.js"></script> -->

<script id="modal_preloader_confirm" type="text/x-jquery-tmpl">
    <h4>. . . Closing Matter, Please Wait . . .</h4>
    <img src="/assets/thisapp/images/ajax-loader-bar.gif">
</script>
<script id="sending_preloader" type="text/x-jquery-tmpl">
    <h4>. . . Sending . . .</h4>
    <img src="/assets/thisapp/images/ajax-loader-bar.gif">
</script>
<script id="lst_handler_tmpl" type="text/x-jquery-tmpl">
    <option value="${value}">${desc}</option>
</script>
<script id="_form_group_tmpl" type="text/x-jquery-tmpl">
    <div class="input-group" style="padding-bottom: 5px; padding-left: 5px; float: left; ">
        <select class="form-control" name="delegated_to[]">
            @foreach($obj['handlers_list'] as $handler)
            <option value='{{json_encode(array("user_id" => $handler->user_id, "EmployeeID" => $handler->EmployeeID))}}'>{{$handler->EmployeeID}} - {{$handler->NickName}}</option>
            @endforeach
        </select>
    </div>
</script>
<script id='_modal_body_tmpl' type='text/x-jquery-tmpl'>
    <h3>Select Handler/s to be Delegated with Matter <span class="matter_for_delegation"></span></h3>
    <div class="form-group _handler_delegation_form_Tmpl">
        <div class="input-group" style="text-align: center; padding-bottom: 5px;">
            <a  href="#" class="btn btn-success" onclick="_addHandlertoDelegate()">Click Here to Add Handler to be Delegated with Matter <span class="matter_for_delegation"></span></a>
        </div>
    </div>
</script>
<script type="text/x-template" id="help_items">
<div class="dropdown">
    <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
        {{ Lang::get('lang.invoice.Help') }}
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/thisapp/help_pages/how-to-delegate-a-matter.html">How to delegate a matter.</a></li>
    </ul>
</div>
</script>
<script type='text/javascript'>
var _reg_handler = '';
var _reg_type = 0;
var _reg_matter = 0;

    $(function(){
        $('#handler_modal_table').dataTable({
           "dom": 'T<"clear">lfrtip',
           pagingType: "full", 
           pageLength : 30,
           orderClasses : true, 
           bLengthChange : false,
           bFilter : true,
           order : [ {{ $obj['billing'] }}, '{{ $obj["billing"] == 1 ? "asc" : "desc" }}' ],
           "tableTools": {
                "sSwfPath": "/assets/DataTables-1.10.0/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                "aButtons": ["xls","print","pdf"]
            }
            {{ $obj['search'] ? ',"oSearch": {"sSearch": "'. $obj["search"] .'"}' : ""}}
	   });
        $(".h_billable").html("$"+parseFloat({{$billable}}).formatMoney(2));
        $(".h_billed").html("$"+parseFloat({{$billed}}).formatMoney(2));
        $(".h_unbilled").html("$"+parseFloat({{$unbilled}}).formatMoney(2));
        $("#my_info_preloader").hide();
        $("#handler_modal_table").show();
        $('div[data-toggle="tooltip"]').tooltip();
    });

    function reopenMatter(matter){
        $("#m_repen_id").html(matter);
        $("#matter_to_open").val(matter);
        $("#m_reopen__").modal().show();
    }

    function _final_process_open(){
      var matter = parseInt($("#matter_to_open").val());
      if(matter > 0 ){
        $("#m_reopen__").modal('hide');
        $("#reopen_preloader").modal().show();
        $.get('/utils/reopenmatter?matter=' + matter,function(data){
            console.log(data);
            location.reload();
        },'json');
      }
    }

    function declineMatter(id,handler){
        _reg_handler = handler;
        _reg_type = 2;
        _reg_matter = id;
        $('.__deleg_com_dec_ok').show();
        $("#m_body_deleg_com_dec").html("Decline Matter: " + id + "?");
        $("#m_deleg_compl").modal().show();
    }

    function _process_dec_com(){

        if(_reg_type == 2)
          $.get('/utils/decline/' + _reg_matter + '/' + _reg_handler,function(info){
            $("#m_body_deleg_com_dec").html(info.message);
            $('.__deleg_com_dec_ok').hide();
          },'json');
        else
        if(_reg_type == 3)
          $.get('/utils/complete/' + _reg_matter + '/' + _reg_handler,function(info){
            $("#m_body_deleg_com_dec").html(info.message);
            $('.__deleg_com_dec_ok').hide();
          });
    }

    function completeMatter(id,handler){
        _reg_handler = handler;
        _reg_type = 3;
        _reg_matter = id;
        $('.__deleg_com_dec_ok').show();
        $("#m_body_deleg_com_dec").html("Complete Delegation for Matter: " + id + "?");
        $("#m_deleg_compl").modal().show();
    }

    function _closeMatter(matter,mark){
        $(".matter_for_completion").html(matter);
        $(".matter_for_completion").val(matter);
        if(mark == 1)
            $("#complete_confirmation").modal().show();
        else{
            $.get('/matters/docketno/' + matter,function(data){
                $('input[name="box_number_temp"]').val(data.res[0].box);
                $("#final_confirmation").modal().show();
            });
        }
    }
    
    function processmatterip(){
        $("#div_for_ip_moving").hide();
        $("#div_for_ip_confirming").show();
        $("#_modal_body_ip").html('');
        $.get('/matters/setipmatter/' + $("#ip_matter_id__").val() + '/9',function(data){
            $("#div_for_ip_confirming").hide();
            $("#_modal_body_ip").html(data.message);
             $(".ip_reg_matters_anchor").hide();
        },'json');
    }
    
    function moveiptoregular(){
        $("#div_for_ip_moving_reg").hide();
        $("#div_for_ip_confirming_reg").show();
        $("#_modal_body_ip_reg").html('');
        $.get('/matters/setipmatter/' + $("#ip_matter_id__reg").val() + '/0',function(data){
            $("#div_for_ip_confirming_reg").hide();
            $("#_modal_body_ip_reg").html(data.message);
             $(".ip_reg_matters_anchor").hide();
        },'json');
    }
    
    function setIPtoM(matter){
        $("#ip_matter_id__reg").val(matter);
        $(".ip_id_marker").html(matter);
        $("#_ip_init__to_reg").show();
        $("#div_for_ip_moving_reg").hide();
        $("#div_for_ip_confirming_reg").hide();
        $("#_modal_body_ip_reg").html('');
         $(".ip_reg_matters_anchor").hide();
        $("#_moving_ip_to_reg_modal__").modal().show();
        $.get('/matters/matterstatus/' + matter, function(data){
            $("#_ip_init__to_reg").hide();
            if(data[0].Completed != 9)
                $("#_modal_body_ip_reg").html(matter + " is already moved to Regular Matters");
            else{
                $("#div_for_ip_moving_reg").show();
                $(".ip_reg_matters_anchor").show();
            }
        },'json');
    }
    
    function setMtoIP(matter){
        $("#ip_matter_id__").val(matter);
        $(".ip_id_marker").html(matter);
        $("#_ip_init__").show();
        $("#div_for_ip_moving").hide();
        $("#div_for_ip_confirming").hide();
        $("#_modal_body_ip").html('');
         $(".ip_reg_matters_anchor").hide();
        $("#_marking_ip_modal__").modal().show();
        $.get('/matters/matterstatus/' + matter, function(data){
            $("#_ip_init__").hide();
            if(data[0].Completed == 9)
                $("#_modal_body_ip").html(matter + " is already moved to IP Matters");
            else{
                $("#div_for_ip_moving").show();
                 $(".ip_reg_matters_anchor").show();
            }
        },'json');
    }
    
    function _delegateMatter(matter){
        $("#deleg_init__").show();
        $(".matter_for_delegation").val(matter);
        $(".confirm_delegate_Matter_modal").html($("#_modal_body_tmpl").tmpl());
        $(".matter_for_delegation").html(matter);
        $(".confirm_delegate_Matter_modal").hide();
        $("#delegate_Matter").modal().show();
        $.get('/matters/detailsmatterall/' + matter,function(data){
            $('input[name=matter_description]').val(data[0].Description_A);
            $('input[name=matter_client]').val(data[0].CustomerID + "-" + data[0].client_name);
            $.get('/matters/delegatedmatters/' + matter,function(data){
                for(var i = 0; i < data.length; i++){
                    _addHandlertoDelegate();
                    var h_select = document.getElementsByName("delegated_to[]")[i];
                    for(var j = 0; j < h_select.options.length; j++){
                        if(JSON.parse(h_select.options[j].value).EmployeeID == data[i].delegatedTo){
                            h_select.selectedIndex = j;
                            h_select.disabled = true;
                            break;
                        }
                    }
                }
                $(".confirm_delegate_Matter_modal").show();
                 $("#deleg_init__").hide();
            },'json');
        },'json');
    }

    function __notifyAdminDelegate(){
      $("#__delegated_by_Admin").modal().show();
      $("#deleg_init__").hide();
      $(".confirm_delegate_Matter_modal").show();
    }
    
    function _addHandlertoDelegate(){
        $("#_form_group_tmpl").tmpl().appendTo("._handler_delegation_form_Tmpl");
        $("#saving_for_delegation").show();

    }
    
    function dis(_id){
        $("#rem_preloader").html("");
        var matter = $.grep(all_matters,function(elem){
            return elem.JobAssignmentID == _id;
        });
        if(matter.length > 0){
            $.get('/accounting/reminder-info?reminder_type=matter&matter_id=' + matter[0].JobAssignmentID + '&total_unbilled_amount=' + matter[0].unbilled,function(data){
                $("#invoice-reminder-batch-number").html(matter[0].JobAssignmentID);
                $("#reminder_recipient").val(matter[0].handler_email);
                $("#reminder_message").html(data.message);
                $("#invoice-reminder-modal").modal().show(); 
            },'json');
        }
    }
    
    function send_all(){
        $("#my_email_preloader").show();
        $.get('/matters/sendmattersinfo?handler={{$obj["handler"]}}&type={{$obj["type"]}}',function(info){
           $("#my_email_preloader").hide();
        },'json');
    }
        
    $("#_form_for_delegation_").submit(function(info){
        var handlers_list = [];
        for(var i = 0; i < document.getElementsByName("delegated_to[]").length; i++){
            handlers_list.push(JSON.parse(document.getElementsByName("delegated_to[]")[i].value));
        }
        document.getElementsByName("_delegated_to_").value=JSON.stringify(handlers_list);
        console.log(JSON.parse(document.getElementsByName("_delegated_to_").value));
        $(".confirm_delegate_Matter_modal").hide();
        $("#confirm_for_delegation").show();
    });
    
    $('.my_confirm_forms').submit(function(data){
        $('input[name="box_number"]').val($('input[name="box_number_temp"]').val());
        $(".confirm_body_modal").html($("#modal_preloader_confirm").tmpl()); 
    });
    $('#invoice-reminder-form').submit(function(e) {
        $("#rem_preloader").html($('#sending_preloader').tmpl());
        e.preventDefault();
        var post_data = $(this).serializeObject();
        post_data.no_log = 1;
        $.post('/accounting/send-reminder', post_data, function(data) {
            $('#invoice-reminder-modal').modal('hide');
        })
    });
</script>
<script>
    $(document).ready(function () {
        $('#help').append($($('#help_items').html()));
        $('#help').on('click','a[role=menuitem]',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url);
        })
    })
</script>
@else

<div class="container" style="text-align: center; color: red; width: 100%; padding-bottom: 30px;"><h1>You Don't Have Enough Privilege to View this Page. </h1></div>

@endif

@stop
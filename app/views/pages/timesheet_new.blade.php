@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-time"></i> New Timesheet 
			</h2>
			<div class="modal-content" id="new_timesheet_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_timesheet_main_form">
							<!--
							<div class="col-md-4 form-group">
								<label for="new_matter_id">Matter ID</label>
								<input type="text" class="form-control" name="new_matter_id" readonly>
							</div>
							<div class="col-md-4 form-group">
								<label for="new_timesheet_date">Date</label>
								<input type="text" class="form-control" name="new_timesheet_date" readonly>
							</div>
					
							<div class="col-md-4 form-group pull-right">
								<label for="new_hourlyrate">Handler</label>
								<input type="text" class="form-control" name="new_handler" value="" readonly>
							</div>

							<div class="col-md-4 form-group pull-right">
								<label for="new_hourlyrate">Hourly Rate</label>
								<input type="text" class="form-control" name="new_hourlyrate" value="{{ $hourly_rate }}" readonly>
							</div>
							-->			
						</div>
						<div class="col-md-12">
							<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/timesheet') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div id="item-container-source" style="display:none">
	<div class="col-md-12 item-container">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_matter_id">Matter ID</label>
							<select class="form-control" name="new_matter_id[]" placeholder="Select Matter" required>
								<option></option>
								@foreach($matter_list as $matter)
									<option value="{{ $matter->JobAssignmentID }}" {{ Input::has('matter_id') && Input::get('matter_id') == $matter->JobAssignmentID ? 'selected' : '' }} >{{ $matter->JobAssignmentID }} - {{ $matter->Description_A }}</option>
								@endforeach
							</select>
						</div>												
						<div class="col-md-12 form-group">
							<label for="new_workhour">Work Hours</label>
							<input type="number" step="any" class="form-control" name="new_workhour[]" required>
						</div>
						<div class="col-md-12 form-group">
							<label for="new_timecode">Time Code</label>
							<select class="form-control" name="new_timecode[]" placeholder="Select Time Code" required>
								<option></option>
								@foreach($timecode_list as $timecode)
									<option value="{{ $timecode->TimeCode }}">{{ $timecode->Description_A }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12 form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="new_billable_checkbox[]"> Billable
								</label>
								<input type="hidden" name="new_billable[]" value="0">
							</div>
						</div>						
					</div>
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_timesheet_date">Date  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_timesheet_date[]" readonly required>
						</div>
						<div class="col-md-6 form-group">
								<label for="new_hourlyrate">Handler</label>
								<select class="form-control" name="new_handler[]" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}" {{ count($handler_list) == 1 ? 'selected' : '' }}>{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
									@endforeach
								</select>
						</div>
						<div class="col-md-6 form-group">
								<label for="new_hourlyrate">Hourly Rate</label>
								<select class="form-control" name="new_hourlyrate[]" placeholder="Select Hourly Rate" readonly disabled>
									<option></option>
									@foreach($hourly_rate_list as $hourly_rate)
										<option value="{{ $hourly_rate->EmployeeID }}">{{ $hourly_rate->HourlyRate }}</option>
									@endforeach
								</select>								
						</div>
						<div class="col-md-12 form-group">
							<label for="new_description">Description</label>
							<textarea class="form-control" name="new_description[]" rows="3" required></textarea>
							<!-- <p class="help-block">Example block-level help text here.</p> -->
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/timesheet.js"></script>
<script>
	/* Overwritten variables from timesheet.js */
	report_types = [];
	new_timesheet_log_count = 0;

    /* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };
    var item_container = $('#item-container-source').clone().html();

    /* Add item */
    $('#btn_add_item2').bind('click touchstart', function(e){
        e.preventDefault();
        $('#new_timesheet_main_form').append(item_container).focus();
        
        $('#new_timesheet_main_form').find('.item-container').find('select[name="new_matter_id[]"]').selectize();
        $('#new_timesheet_main_form').find('.item-container').find('select[name="new_timecode[]"]').selectize();
        $('#new_timesheet_main_form').find('.item-container').find('select[name="new_handler[]"]').selectize();
        $('#new_timesheet_main_form').find('.item-container').find('input[name="new_timesheet_date[]"]').datepicker(datepicker_options);
        
        new_timesheet_log_count++;
    });

    $(document).on('change', 'select[name="new_handler[]"]', function() {
    	var selected_handler = $(this).val();
    	$(this).parent().parent().find('select[name="new_hourlyrate[]"]').val(selected_handler);
    });

    $(document).on('change', 'input[name="new_billable_checkbox[]"]', function() {
    	if($(this).is(":checked")) {
    		$(this).closest('.form-group').find('input[name="new_billable[]"]').val('1');
    	} else {
    		$(this).closest('.form-group').find('input[name="new_billable[]"]').val('0');
    	}
    });

    $(document).ready(function() {
    	$('#btn_add_item2').click();
    });

    /* Save New Timesheet */
    $('#new_timesheet_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

        
        $.post('/timesheet/submit-new2', $(this).serializeArray(), function(data){
            console.log(data);
            if(data.status == 'success') {
                toastr.success(data.msg);
	            var delay = 3000; //Your delay in milliseconds
	            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
            } else {
                toastr.error(data.msg);
            }
        });
        

    });    
</script>
@stop
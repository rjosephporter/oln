@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<div class="container-fluid">
  
  @if(Session::has('message.success.send_reminder')) 
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        {{ Session::get('message.success.send_reminder') }}
      </div>      
    </div>
  </div>
  @endif  

    <!-- Floorplan V2 -->
    <div class="row top-buffer">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="height:52px">
                    <div class="panel-title pull-left">The Office (3rd & 5th floors)</div>

                    @if(in_array(Auth::id(), Config::get('oln.gdo_id')))
                      <button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#send_reminder_modal"><i class="fa fa-envelope"></i> Gordon's General Reminder</button>
                    @else
                      <button class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#send_reminder_modal" disabled><i class="fa fa-envelope"></i> Gordon's General Reminder</button>
                    @endif
                     <form method="get"><div id="new_conference_room_reservation_main_form" style="margin-top:-7px;margin-left:45px;"><center><table><tr><td>
                      <div class="col-sm-12"><label for="new_reservation_date">Date  <small>(click to select date)</small></label></div></td><td></td><td> 
                      <div class="col-sm-10"><input type="text" class="form-control" id="datetimepicker3" name="new_reservation_date[]"  readonly required></div></td>
                      <td colspan='2'><div class="col-sm-2"><button type="button" class="btn btn-info btn-sm pull-right btn-primary_reservation" id="today">Today</button></div></td></tr></table></center></div>
                      </form>

                </div>

                

                <div class="col-md-12 form-group">
                <h3 id="room-name">&nbsp;</h3>
                </div>
                
                <div class="panel-body">
                    <div id="floorplan"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Floorplan V2 -->
</div>

<!--
<div class="modal fade" id="openhandler" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">SVK</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Susan van Kross</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Out</p>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openstaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Staff status</h4>
      </div>
      <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Tim J. Cone</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Out</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Position</label>
                <div class="col-sm-8">
                  <p class="form-control-static">Research Assistant</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Current tasks</label>
                <div class="col-sm-8">
                  <p class="form-control-static"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Absences this month</label>
                <div class="col-sm-8">
                  <p class="form-control-static">3</p>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="openoffice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">TKL's Office</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-sm-4 control-label">
                    <span class="glyphicon glyphicon-user text-info very-large-glyph"></span>
                </label>
                <div class="col-sm-8">
                  <p class="form-control-static">Thomas K. Lueng</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Status</label>
                <div class="col-sm-8">
                  <p class="form-control-static">In</p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-4 control-label">Current matters in office</label>
                <div class="col-sm-8">
                    <ul class="list-unstyled">
                        <li>
                            <span class="glyphicon glyphicon-folder-open large-glyph text-info"></span>
                            &nbsp;&nbsp;<span class="h4">33296</span>
                            <p>
                                Client: AAA<br>
                                Matter: Legal Advice<br>
                                Subject Matter: I.P.
                            </p>
                        </li>
                        <li>
                            <span class="glyphicon glyphicon-folder-open large-glyph text-info"></span>
                            &nbsp;&nbsp;<span class="h4">33296</span>
                            <p>
                                Client: FFF<br>
                                Matter: Debt Recovery Action<br>
                                Subject Matter: Dispute Resolution.
                            </p>
                        </li>
                    </ul>
                </div>
              </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
-->

<div class="modal fade" id="send_reminder_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Send Reminder to Handlers</h4>
      </div>
      <form role="form" method="post" action="/office/send-reminder">
        <div class="modal-body">
          <h4>Send reminder to the following handlers listing their matters that have no activities for 8 weeks and above.</h4>
          <div class="text-right">
            <button class="btn btn-success btn-sm" id="select_all_handlers">Select All</button> 
            <button class="btn btn-warning btn-sm" id="clear_all_handlers">Clear All</button>
          </div>
          <div class="form-group">
            <label for="handlers"></label>
            <input type="text" class="form-control" id="handlers" name="handlers" placeholder="Select Handlers" initial-value="{{ implode(',', $handlers) }}" value="{{ implode(',', $handlers) }}" required>
          </div>
          @if( $show_reminder_log )
            <small>Last reminder sent at <strong>{{ $last_reminder_date }}</strong> to [<i>{{ $last_reminder_recipients }}</i>]</small>
          @else
            <small>Last reminder sent: None</small>
          @endif
          <div class="alert alert-info" role="alert">
            <strong>Note:</strong> Sending reminders to multiple recipients will take a few seconds to complete.
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Send</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>


<!--calendar modal-->
<div class="modal fade"  id="complete_confirmation1" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content" id="new_conference_room_reservation_log1">
           <div class="col-md-12 row"><input id="datetimepicker" type="hidden" >
            
      </div>
  </div>
</div>
<!--other modal-->
<div class="modal fade"  id="complete_confirmation" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content" id="new_conference_room_reservation_log2">
        <form role="form" method="post">
          <div class="modal-body">
            <div class="row" id="new_conference_room_reservation_main_form">
            <div id="item-container-source" style="display:block">
            <div class="col-md-12 item-container">
              <div class="panel panel-default">
                <div class="panel-heading clearfix">
                <button type="button" class="close pull-right delete-item_reservation" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 row">
                        <div class="col-md-12 form-group">
                         <input id="conferenceid" name="conferenceid" type="hidden" >
                              <!--<label class="control-label" for="reservationtime">Choose your start-time and end times:</label>
                              <div class="controls">
                               <div class="input-prepend input-group">
                                 <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                 <input type="text" style="width: 400px" name="new_time[]" id="reservationtime" class="form-control"   class="span4"/>
                               </div>
                              </div>-->
                        
                        </div>
                   
                         <div class="col-md-12 form-group">
                         <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_start_time" name="new_start_time[]" readonly required>
                        </div>
                        <div class="col-md-12 form-group">
                          <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_end_time" name="new_end_time[]" readonly required>
                        </div>
                      <div class="col-md-12 form-group">
                         
                          <label for="reservedby">Reserved By</label>
                          <input type="hidden" class="form-control" id="cond" value="{{$condition}}">
                          <select class="form-control"  id="reservedby2" name="reservedby[]" placeholder="Select Handler" required>
                            <option></option>

                            @foreach($handler_list as $handler)
                            <option value="{{ $handler->EmployeeID }}" {{ $condition  == $handler->EmployeeID ? 'selected' : '' }} >{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="col-md-12 form-group">
                        <label for="new_purpose">Purpose</label>
                        <textarea class="form-control" name="new_purpose[]" rows="3" required></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>

                      <div class="col-md-12 form-group">
                        <label for="new_notes">Notes</label>
                        <textarea class="form-control" name="new_notes[]" rows="3"></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>
                     <!-- <div class="col-md-12 form-group">
                        <label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
                        <input type="text" class="form-control" name="new_reservation_date[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_start_time[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_end_time[]" readonly required>
                      </div>
                    </div>-->
                    
         
                </div>
              </div>  
            </div>
          </div>
            </div>
            <div class="col-md-12">
              <!--<button class="btn btn-info btn-sm" id="btn_add_item2_reservation">Add Item</button>-->
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary_reservation">Save</button>
          </div>
           </div>
           </div>
        </form>
  </div>
</div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="/assets/datetimepicker-master/jquery.js"></script>
<script src="/assets/datetimepicker-master/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/raphael-min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/floorplan.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/floorplancfrm.js"></script>
<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/bootstrap-daterangepicker-master/daterangepicker-bs3.css" />
<script>
/* Gordon's General Reminder */
var handler_selectize = $('#handlers').selectize();
handler_selectize = handler_selectize[0].selectize;
handler_selectize.clear();

$('#select_all_handlers').click(function(e) {
  e.preventDefault();
  handler_selectize.clear();
  handler_selectize.setValue($('#handlers').attr('initial-value').split(','));
});
$('#clear_all_handlers').click(function(e) {
  e.preventDefault();
  handler_selectize.clear();
});

$(document).ready(function() {
    $('#btn_add_item2').click();
    $('#reservedby2').selectize();
      var $select = $("#reservedby2").selectize();
      var selectize = $select[0].selectize;
      
        selectize.setValue($('#cond').val()); 

    });


 $('#new_conference_room_reservation_log2 form').submit(function(e){
        e.preventDefault();
        tempArray=$(this).serializeArray();

       

          $.post('/conference-room-reservation/submitnewreservation', tempArray, function(data){
                    
                var myStringArray = data.val;
                //myStringArray.push()
                var arrayLength = myStringArray.length;
                console.log(myStringArray);
                var events = [];
                for (var i = 0; i < arrayLength; i++) {

                        var str1 = data.date;
                        var space=' ';
                       var str2 = space.concat(data.val[i]['start_time'].substring(0,5));
                       var str3 = space.concat(data.val[i]['end_time'].substring(0,5));
                       console.log('startdate'+str1.concat(str2));
                        console.log('enddate'+str1.concat(str3));
                  events.push({id: i, start: getDateObj(str1.concat(str2)), end: getDateObj(str1.concat(str3))});
                      }
                      var toaddst=$('#new_start_time').val();
                      var toaddet=$('#new_end_time').val();
                      console.log("toaddtime"+toaddst);
                      console.log("toaddet"+toaddet);

                      events.push({id: arrayLength, start: getDateObj(toaddst), end: getDateObj(toaddet)});//add the one to be added
                      var overlap=getOverlappingEvents(events).join('\n');
                      console.log(overlap);
                      if(overlap.length>0){
                         toastr.error('Scheduled time is overlapping with another reservation');
                      }else{

                        $.post('/conference-room-reservation/savenewreservation2', tempArray, function(data){
                            
                                if(data.status == 'success') {
                                    toastr.success(data.msg);
                                  var delay = 3000; //Your delay in milliseconds
                                  setTimeout(function(){ window.location = '{{ URL::to('/office') }}'; }, delay);                
                                } else {
                                    toastr.error(data.msg);
                                }
                        });
                        
                      }

                  });
              
            
}); 

$(document).on('click touchstart', '#today', function(e){
        e.preventDefault();
        var d = new Date();
  console.log(d);
  var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  console.log(dstring);
   // $('#datetimepicker2').val(dstring);
      window.open('/office/?seldate='+ dstring,"_self");
});

function setDate(){

  var d = new Date();
  console.log(d);
  var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  console.log(dstring);
    $('#datetimepicker3').val(dstring);
}

function getDateObj(s) {
      var bits = s.split(/[/ :]/);
      var date = new Date(bits[0], bits[1] - 1, bits[2]);
      date.setHours(bits[3], bits[4], 0);
      return date;
    }

    
    function getOverlappingEvents(eventArray) {
      var result = [];
      var a, b;
      console.log('eventarray'+eventArray);

      // Sort the event array on start time
      eventArray.sort(function(a, b) {
          return a.start - b.start;
        });

      // Get overlapping events
      for (var i=0, iLen=eventArray.length - 1; i<iLen; i++) {
        a = eventArray[i];
        b = eventArray[i + 1];

        if((a.start <= b.start && b.end <=a.end) || (a.start <=b.start && b.start <a.end) 
          || (a.start <=b.end && b.end<=a.end)){
        //if ((a.start <= b.start && a.end > b.start) ||
            //(a.start < b.end && a.end >= b.end) ) {
           result.push([a.id, b.id]);
        }
      }
      return result;
    }

</script>
@stop
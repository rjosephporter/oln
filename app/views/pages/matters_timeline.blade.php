<ul class="timeline _matter_timeline_ul">
    @foreach($timeline as $index => $tl)
    <li {{ (($index+1)%2 == 1) ? 'class="timeline-inverted"' : '' }}>
        <div class="timeline-badge info"><i class="glyphicon glyphicon-check"></i></div>
        <div class="timeline-panel">
            <div class="timeline-heading">
                <h4 class="timeline-title">{{ $tl->Description_A }}</h4>
                <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>{{ $tl->WorkDate }}</small></p>
            </div>
            <div class="timeline-body">
                <p>{{ $tl->Comment }}</p>
            </div>
        </div>
    </li>
    @endforeach
</ul>
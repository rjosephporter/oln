@extends('template/default')
@section('content')
@if($obj['view'] == "granted")
<?php
    $billable = 0;
    $billed = 0;
    $unbilled = 0;
?>
<script type="text/javascript">
    var params_obj = {type : "{{ $obj['type'] }}", handler : "{{ $obj['handler'] }}"};
</script>
<input type="hidden" id="billable" value="0">
<input type="hidden" id="billed" value="0">
<input type="hidden" id="unbilled" value="0">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="well well-sm">
                <div class="row">
                    <div style="margin-left: 10px; margin-bottom: 10px;">
                        <div class="row">
                            <div class="col-md-10"><span class="h3" style="margin-left: 15px;">{{ $obj['handler']}} {{ Lang::get('lang.handlers.Matters') }}</span></div>
                            <div class="col-md-2" style="padding-right: 30px;">
                                {{ $obj['user'] == "all" ? '<button class="btn btn-block btn-sm btn-info btn-send-reminder" onclick="send_all(params_obj)">{{ Lang::get('lang.handlers.Email Matters to') }} '.$obj['handler'].'</button>' : "" }}
                            </div>
                        </div>
       
                        <div style="margin-left: 15px; margin-top: 15px;">
                            <span><a style="color: black; font-size: 13px;" href="/handlers/matters?type=all&handler={{$obj['handler']}}"><img src="/assets/thisapp/images/matters/white.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.View All Matters') }}</a></span>
                            <span style="margin-left: 15px; font-size: 13px;"><a href="/handlers/matters?type=green&handler={{$obj['handler']}}"><img src="/assets/thisapp/images/matters/green.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.Active Last 4 Weeks') }}</a></span>
                            <span style="margin-left: 15px; font-size: 13px;"><a href="/handlers/matters?type=orange&handler={{$obj['handler']}}" style="color: orange;"><img src="/assets/thisapp/images/matters/orange.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.Last Activity Between 4 and 8 Weeks') }}</a></span>
                            <span style="margin-left: 15px; font-size: 13px;"><a href="/handlers/matters?type=red&handler={{$obj['handler']}}" style="color: red;"><img src="/assets/thisapp/images/matters/red.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.Last Activity Beyond 8 Weeks') }}</a></span>
                            <span style="margin-left: 15px; font-size: 13px;"><a href="/handlers/matters?type=gray&handler={{$obj['handler']}}" style="color: gray;"><img src="/assets/thisapp/images/matters/grey.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.Untouched/No Activity') }}</a></span>
                            <span style="margin-left: 15px; font-size: 13px;"><a href="/handlers/matters?type=blue&handler={{$obj['handler']}}" style="color: blue;"><img src="/assets/thisapp/images/matters/blue.png" width="20"/>&nbsp;{{ Lang::get('lang.handlers.Completed Matters') }}</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <div class="alert alert-success" style='text-align: center; display: none;' id='myalertbox'>
            <a href="#" class="close" onclick='$("#myalertbox").hide();'>&times;</a>
            <h3>{{ ucfirst($obj['type']) }} {{ Lang::get('lang.handlers.Matters Listing is now being sent to') }} {{ $obj['handler'] }}</h3> 
        </div>
    
    <div class="row">
        <div class="col-xs-12">
            <div class="well well-sm">
                <div class="row">
                    <table class="table table-hover table-bordered cell-border" id="handler_modal_table">                        
                        <thead>
                            <tr>
                              <th colspan="4" style="text-align: right; margin-right: 15px;">{{ Lang::get('lang.handlers.Totals Summary') }}</th>
                              <th class="h_billable" style="text-align: right;"></th>
                              <th class="h_billed" style="text-align: right;"></th>
                              <th class="h_unbilled" style="text-align: right;"></th>
                              <th>&nbsp;</th>
                          </tr>
                          <tr>
                              <th>&nbsp;</th>
                              <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Client Name">{{ Lang::get('lang.handlers.Client') }}</div></th>
                              <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter Name">Reference</div></th>
                              <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter ID">{{ Lang::get('lang.handlers.Matter') }}</div></th>
                              <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billable Amount" style="text-align: right; width:100%;">{{ Lang::get('lang.handlers.Billable Amount') }}</div></th>
                              <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billed Amount" style="text-align: right; width:100%">{{ Lang::get('lang.handlers.Billed Amount') }}</div></th>
                              <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Unbilled Amount" style="text-align: right; width: 100%;">{{ Lang::get('lang.handlers.Unbilled Amount') }}</div></th>
                              <th>{{ Lang::get('lang.handlers.Action') }}</th>
                          </tr>
                      </thead>
                      <tbody>
                         @foreach($obj['matters'] as $matter)
                         <?php 
                            $billable += $matter->billable_amount;
                            $billed += $matter->billed_amount;
                            $unbilled += $matter->unbilled;
                         ?>
                      <tr style="background-color: {{ $matter->matter_type == 'blue' ? "rgba(0, 0, 255, 0.5)"  : ($matter->matter_type == 'green' ? "rgba(0, 128, 0, 0.5)" : ($matter->matter_type == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($matter->matter_type == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
                            <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: center; cursor: pointer;"><img src="/assets/thisapp/images/matters/{{ $matter->matter_type == "gray" ? "grey" : $matter->matter_type }}.png" width="25"></td>
                            <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: left; cursor: pointer;">{{$matter->client_name}}</td>
                            <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: left; cursor: pointer;">{{ $matter->Description_A }}</td>
                            <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: center; cursor: pointer;">{{ $matter->JobAssignmentID }}</td>
                            <td style='text-align: right;'>${{ number_format($matter->billable_amount, 2, '.', ',') }}</td>
                            <td style='text-align: right;'>${{ number_format($matter->billed_amount, 2, '.', ',') }}</td>
                            <td style='text-align: right;'>${{ number_format($matter->unbilled, 2, '.', ',') }}</td>
                            <td title="Send Reminder to {{ $matter->Incharge }}" style="text-align: center; cursor: pointer;">{{ $obj['user'] == "all" ? '<button class="btn btn-sm btn-block btn-info btn-send-reminder" onclick="dis('.$matter->JobAssignmentID .')"><i class="fa fa-envelope"></i></button>' : '&nbsp;' }}</td>
                      </tr>
                          @endforeach
                      </tbody>
                      <tfoot>
                          <tr>
                              <th colspan="4" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                              <th style="text-align: right;">${{ number_format($billable, 2, '.', ',') }}</th>
                              <th style="text-align: right;">${{ number_format($billed, 2, '.', ',') }}</th>
                              <th style="text-align: right;">${{ number_format($unbilled, 2, '.', ',') }}</th>
                              <th>&nbsp;</th>
                          </tr>
                      </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modals -->
<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{{ Lang::get('lang.handlers.Close') }}</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ Lang::get('lang.handlers.Matter ID') }}: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
          <div>
                <h5><i class="fa fa-info-circle text-info"></i> {{ Lang::get('lang.handlers.Send reminder to handler') }}?</h5>

                <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
                <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">

                <div class="form-group">
                  <label for="reminder_recipient">{{ Lang::get('lang.handlers.To') }}</label>
                  <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Handler's Email" required readonly>
                </div>
                <div style="text-align: center; vertical-align: middle;" id="rem_preloader"></div>
                <div class="form-group">
                  <label for="reminder_message">{{ Lang::get('lang.handlers.Message') }}</label>
                  <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
                  </textarea>
                </div>
          </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">{{ Lang::get('lang.handlers.Yes') }}</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('lang.handlers.No') }}</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- /Modals -->
<link type="text/css" href="/assets/thisapp/css/matters.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript">
    $(function(){
        $('#handler_modal_table').dataTable({
           pagingType: "full", pageLength : 15, orderClasses : true, bLengthChange : false, bFilter : false, order : [ {{ $obj['billing'] }}, 'desc' ]
	});
        
        $(".h_billable").html("$"+parseFloat({{$billable}}).formatMoney(2));
        $(".h_billed").html("$"+parseFloat({{$billed}}).formatMoney(2));
        $(".h_unbilled").html("$"+parseFloat({{$unbilled}}).formatMoney(2));
    });
    function show(id){
        pM(id);
    }
    function dis(_id){
        $("#rem_preloader").html("");
        var matter = $.grep(all_matters,function(elem){
            return elem.JobAssignmentID == _id;
        });
        if(matter.length > 0){
            $.get('/accounting/reminder-info?reminder_type=matter&matter_id=' + matter[0].JobAssignmentID + '&total_unbilled_amount=' + matter[0].unbilled,function(data){
                $("#invoice-reminder-batch-number").html(matter[0].JobAssignmentID);
                $("#reminder_recipient").val(matter[0].handler_email);
                $("#reminder_message").html(data.message);
                $("#invoice-reminder-modal").modal().show(); 
            },'json');
        }
    }
    
    function send_all(obj){
        $("#myalertbox").show();
        $.get('/matters/sendmattersinfo?handler={{$obj["handler"]}}&type={{$obj["type"]}}',function(info){
           console.log(info);
           $("#myalertbox").hide();
        },'json');
    }

    $('#invoice-reminder-form').submit(function(e) {
        $("#rem_preloader").html('<h4>. . . Sending . . .</h4><img src="/assets/thisapp/images/ajax-loader-bar.gif">');
    e.preventDefault();

    var post_data = $(this).serializeObject();

    post_data.no_log = 1;

    $.post('/accounting/send-reminder', post_data, function(data) {
        $('#invoice-reminder-modal').modal('hide');
    });
});
</script>
<script type="text/javascript" src="/assets/thisapp/js/modal_matter_.js"></script>

@else

<div class="container" style="text-align: center; color: red; width: 100%; padding-bottom: 30px;"><h2>You Don't Have Enough Privilege to View this Page. </h2></div>

@endif

@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<script type="text/javascript" src="/assets/multiselect/bootstrap-multiselect-master/dist/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="/assets/multiselect/bootstrap-multiselect-master/dist/css/bootstrap-multiselect.css" type="text/css"/>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i> &nbsp;&nbsp;Update Employee 
			</h2>
			<div class="modal-content" id="new_employee_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_employee_main_form">
		
						</div>
						<div class="col-md-12">
							<!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/dashboard') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="item-container-source" style="display:none">
	<div class="col-md-12 item-container">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 row">
						<input type="hidden" id="empid" value="{{$emp_id}}">
							<div class="col-md-12 form-group">
									<label for="new_employee_id">Employee</label>
									<select class="form-control" name="new_employee_id[]" placeholder="Select Employee">
									<option></option>
										@foreach($employee_list as $employee)
										<option value="{{ $employee->EmployeeID }}"{{  $emp_id  == $employee->EmployeeID ? 'selected' : '' }}>{{ $employee->EmployeeID }}</option>
										@endforeach
									</select>
							</div>	
							<div class="col-md-12 form-group">
									<label for="new_nickname">Nickname </label>
									<input type="text" class="form-control check" id="new_nickname[]" name="new_nickname[]" required>
									<input type="hidden" class="form-control" name="userid[]" id="userid[]" >
									
							</div>
							<div class="col-md-12 form-group">
									<label for="new_fname">First name </label>
									<input type="text" class="form-control check" id="new_fname[]" name="new_fname[]" required>
							</div>
							<div class="col-md-12 form-group">
									<label for="new_lname">Last name </label>
									<input type="text" class="form-control check" id="new_lname[]" name="new_lname[]" required>
							</div>
							<div class="col-md-12 form-group">
									<label for="new_email">Email address </label>
									<input type="text" class="form-control check" id="new_email[]" name="new_email[]" required>
							</div>
							<!--<div class="col-md-12 form-group">
									<label for="new_password">Password </label>
									<input type="password" class="form-control check" id="new_password[]" name="new_password[]" required>
							</div>-->
							<div class="col-md-12 form-group">
									<label for="new_supervisor">Supervisor </label>
									<input type="text" class="form-control check" id="new_supervisor[]" name="new_supervisor[]" required>
							</div>
							<div class="col-md-6 form-group">
									<label for="new_gender">Gender </label>
									<select class="form-control gender" name="gender[]" placeholder="gender" required>
											<option value="1">Male</option>
											<option value="2">Female</option>
			
									</select>
							</div>
							<div class="col-md-6 form-group">
									<label for="new_bloodtype">Bloodtype </label>
									<select class="form-control check" name="bloodtype[]" placeholder="Bloodtype" required>
											<option value="1">A+</option>
											<option value="2">A-</option>
											<option value="3">B+</option>
											<option value="4">B-</option>
											<option value="5">O+</option>
											<option value="6">O-</option>
											<option value="7">AB+</option>
											<option value="8">AB-</option>
									</select>
							</div>
							<div class="col-md-6 form-group">
									<label for="marital_status">Marital Status </label>
									<select class="form-control check" name="marital_status[]" placeholder="Marital Status" required>
											<option value="1">Single</option>
											<option value="2">Married</option>
											<option value="3">Divorced</option>
									</select>
							</div>
							<div class="col-md-12 form-group">
									<label for="new_dob">Date of Birth  <small>(click to select date)</small></label>
									<input type="text" class="form-control check" name="new_dob[]" readonly required>
							</div>	

							<div class="col-md-12 form-group">
									<label for="new_pob">Place of Birth </label>
									<input type="text" class="form-control check" id="new_pob[]" name="new_pob[]" required>
							</div>

							<div class="col-md-12 form-group">
									<label for="new_nationality_id">Nationality</label>
									<select class="form-control check" name="new_nationality_id[]" placeholder="Nationality" required>
											<option></option>
											@foreach($nationality_list as $nat)
											<option value="{{ $nat->CountryCode }}">{{ $nat->CountryName_A }}</option>
											@endforeach
									</select>
							</div>	
					</div>
					<div class="col-md-6 row">
						<div class="col-md-8 form-group">
									<label for="new_role_id">Roles</label>
									<select multiple="multiple" class="form-control role" name="new_role_id[]" placeholder="Role" required>
											
											@foreach($role_list as $nat)
											<option value="{{ $nat->id }}">{{ $nat->name }}</option>
											@endforeach
									</select>
						</div>	
						<div class="col-md-4 form-group" style="display:none" id="handlers">
									<label for="new_handler_id">Select Handler</label>
									<select multiple="multiple" class="form-control handle" name="new_handler_id[]" placeholder="Handler">
											
											@foreach($handler_list as $nat2)
											<option value="{{ $nat2->EmployeeID }}">{{ $nat2->EmployeeID }}</option>
											@endforeach
									</select>
						</div>
						<div class="col-md-4 form-group" style="display:none" id="pa">
									<label for="new_pa_id">Select Assistant</label>
									<select multiple="multiple" class="form-control pa" name="new_pa_id[]" placeholder="Personal Assistant">
											
											@foreach($assistant_list as $nat1)
											<option value="{{ $nat1->EmployeeID }}">{{ $nat1->EmployeeID }}</option>
											@endforeach
									</select>
						</div>
						<div class="col-md-12 form-group">
							<label for="new_datehired">Date Hired  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_datehired[]" readonly required>
						</div>

						<div class="col-md-12 form-group">
							<label for="new_weekstarts">Week starts on</label>
							<input type="text" class="form-control" name="new_weekstarts[]" required>
						</div>

						<div class="col-md-4 form-group">
							<label for="weekdayam">Weekday AM Apply?</label>
							<input type="checkbox" class="form-control" id="weekdayam" name="weekdayam" >
						</div>
						<div class="weekdayaminfo" style="display:none">
							<div class="col-md-4 form-group">
								<label for="weekdayamfrom">Weekday AM From <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="weekdayamfrom[]" name="weekdayamfrom[]" readonly>
							</div>
							<div class="col-md-4 form-group">
								<label for="weekdayamto">Weekday AM To <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="weekdayamto[]" name="weekdayamto[]"  readonly>
							</div>
						</div>	

						<div class="col-md-4 form-group">
							<label for="weekdaypm">Weekday PM Apply?</label>
							<input type="checkbox" class="form-control" id="weekdaypm" name="weekdaypm" >
						</div>
						<div class="weekdaypminfo" style="display:none">
							<div class="col-md-4 form-group">
								<label for="weekdaypmfrom">Weekday PM From <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="weekdaypmfrom[]" name="weekdaypmfrom[]" readonly>
							</div>
							<div class="col-md-4 form-group">
								<label for="weekdaypmto">Weekday PM To <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="weekdaypmto[]" name="weekdaypmto[]" readonly >
							</div>
						</div>	
						<div class="col-md-4 form-group">
							<label for="saturdayam">Saturday AM Apply?</label>
							<input type="checkbox" class="form-control" id="saturdayam" name="saturdayam" >
						</div>
						<div class="saturdayaminfo" style="display:none">
							<div class="col-md-4 form-group">
								<label for="saturdayamfrom">Saturday AM From <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="saturdayamfrom[]" name="saturdayamfrom[]" readonly>
							</div>
							<div class="col-md-4 form-group">
								<label for="saturdayamto">Saturday AM To <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="saturdayamto[]" name="saturdayamto[]" readonly >
							</div>
						</div>	

						<div class="col-md-4 form-group">
							<label for="saturdaypm">Saturday PM Apply?</label>
							<input type="checkbox" class="form-control" id="saturdaypm" name="saturdaypm" >
						</div>
						<div class="saturdaypminfo" style="display:none">
							<div class="col-md-4 form-group">
								<label for="saturdaypmfrom">Saturday PM From <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="saturdaypmfrom[]" name="saturdaypmfrom[]" readonly>
							</div>
							<div class="col-md-4 form-group">
								<label for="saturdaypmto">Saturday PM To <small>(click to select time)</small></label>
								<input type="text" class="form-control" id="saturdaypmto[]" name="saturdaypmto[]" readonly >
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js"></script>


<script>

	report_types = [];
	new_employee_log_count = 0;

	  /* Init new variables */
    var datepicker_options = {
    	maxDate : '0',
    	dateFormat: "MM d, yy",
    };


	var item_container = $('#item-container-source').clone().html();

	/* Add item */
    //$('#btn_add_item2').bind('click touchstart', function(e){
        //e.preventDefault();
        $('#new_employee_main_form').append(item_container).focus();
        $('#new_employee_main_form').find('.item-container').find('input[name="new_dob[]"]').datepicker(datepicker_options);
         $('#new_employee_main_form').find('.item-container').find('input[name="new_datehired[]"]').datepicker(datepicker_options);
 
        
    var startDateTextBox=$('.weekdayaminfo').find('input[name="weekdayamfrom[]"]');
    var endDateTextBox=$('.weekdayaminfo').find('input[name="weekdayamto[]"]');
    var startDateTextBox1=$('.weekdaypminfo').find('input[name="weekdaypmfrom[]"]');
    var endDateTextBox1=$('.weekdaypminfo').find('input[name="weekdaypmto[]"]');
    var startDateTextBox2=$('.saturdayaminfo').find('input[name="saturdayamfrom[]"]');
    var endDateTextBox2=$('.saturdayaminfo').find('input[name="saturdayamto[]"]');
    var startDateTextBox3=$('.saturdaypminfo').find('input[name="saturdaypmfrom[]"]');
    var endDateTextBox3=$('.saturdaypminfo').find('input[name="saturdaypmto[]"]');

	startDateTextBox.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox.datetimepicker('setDate', testStartDate);
			}
			else {
				console.log(dateText);
				endDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );

		}
	});
	endDateTextBox.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (startDateTextBox.val() != '') {
				var testStartDate = startDateTextBox.datetimepicker('getDate');
				var testEndDate = endDateTextBox.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
		}
	}); 

	//
	startDateTextBox1.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (endDateTextBox1.val() != '') {
				var testStartDate = startDateTextBox1.datetimepicker('getDate');
				var testEndDate = endDateTextBox1.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox1.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox1.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox1.datetimepicker('option', 'minDate', startDateTextBox1.datetimepicker('getDate') );

		}
	});
	endDateTextBox1.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (startDateTextBox1.val() != '') {
				var testStartDate = startDateTextBox1.datetimepicker('getDate');
				var testEndDate = endDateTextBox1.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox1.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox1.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox1.datetimepicker('option', 'maxDate', endDateTextBox1.datetimepicker('getDate') );
		}
	}); 

	//

	startDateTextBox2.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (endDateTextBox2.val() != '') {
				var testStartDate = startDateTextBox2.datetimepicker('getDate');
				var testEndDate = endDateTextBox2.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox2.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox2.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox2.datetimepicker('option', 'minDate', startDateTextBox2.datetimepicker('getDate') );

		}
	});
	endDateTextBox2.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (startDateTextBox2.val() != '') {
				var testStartDate = startDateTextBox2.datetimepicker('getDate');
				var testEndDate = endDateTextBox2.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox2.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox2.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox2.datetimepicker('option', 'maxDate', endDateTextBox2.datetimepicker('getDate') );
		}
	}); 

	//
	startDateTextBox3.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (endDateTextBox.val() != '') {
				var testStartDate = startDateTextBox3.datetimepicker('getDate');
				var testEndDate = endDateTextBox3.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					endDateTextBox3.datetimepicker('setDate', testStartDate);
			}
			else {
				endDateTextBox3.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			endDateTextBox3.datetimepicker('option', 'minDate', startDateTextBox3.datetimepicker('getDate') );

		}
	});
	endDateTextBox3.timepicker({ 
		controlType: 'select',
		hourMin: 1,
		hourMax: 12,
		timeFormat: "hh:mm tt",
		onClose: function(dateText, inst) {
			if (startDateTextBox3.val() != '') {
				var testStartDate = startDateTextBox3.datetimepicker('getDate');
				var testEndDate = endDateTextBox3.datetimepicker('getDate');
				if (testStartDate > testEndDate)
					startDateTextBox3.datetimepicker('setDate', testEndDate);
			}
			else {
				startDateTextBox3.val(dateText);
			}
		},
		onSelect: function (selectedDateTime){
			startDateTextBox3.datetimepicker('option', 'maxDate', endDateTextBox3.datetimepicker('getDate') );
		}
	}); 
        new_employee_log_count++;
    //});

    $(document).ready(function() {
    	var a=new Array();
        $('.role').multiselect({
            buttonClass: 'form-control',
            onChange: function(element, checked) {
                if(checked === true) {
                    //action taken here if true
                    console.log(element.val());
                    a.push(element.val());
                    if(element.val()==2){
                    	$("#handlers").css("display", "block");
                    	$("#pa").css("display", "none");
                    }
                    else if(element.val()==4){
                    	$("#pa").css("display", "block");
                    	$("#handlers").css("display", "none");
                    }
                    
                    
                }
                else if(checked === false) {
                   a.remove(element.val());
                   if(!a.contains(4)){
                   		$("#pa").css("display", "none");
                   }
                   if(!a.contains(2)){
                   		$("#handlers").css("display", "none");
                   }
                }

                if (a.contains(2) && a.contains(4)) {
                	//alert("You cannot select both roles of Handler and Personal Assistant.");
                	toastr.error("You cannot select both roles of Handler and Personal Assistant.");
            	}
            }
        });

		$('.pa').multiselect({ buttonClass: 'form-control'});
		$('.handle').multiselect({ buttonClass: 'form-control'});

        Array.prototype.contains = function(elem)
		{
		   for (var i in this)
		   {
		       if (this[i] == elem) return true;
		   }
		   return false;
		}

		Array.prototype.remove = function(value) {
		var idx = this.indexOf(value);
		if (idx != -1) {
		    return this.splice(idx, 1); // The second parameter is the number of elements to remove.
		}
		return false;
		}
    

     /* Delete item */
    $(document).on('click touchstart', '.delete-item', function(e){
        e.preventDefault();
        if(new_employee_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_employee_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });

    $(document).on('change', 'input[name="weekdayam"]', function() {
    
    	var ischecked = $("#weekdayam").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		$('.weekdayaminfo').show();

    	}else{

    		$('.weekdayaminfo').hide();
       	}

    });

     $(document).on('change', 'input[name="weekdaypm"]', function() {
    
    	var ischecked = $("#weekdaypm").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		$('.weekdaypminfo').show();

    	}else{

    		$('.weekdaypminfo').hide();
       	}

    });

    $(document).on('change', 'input[name="saturdayam"]', function() {
    
    	var ischecked = $("#saturdayam").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		$('.saturdayaminfo').show();

    	}else{

    		$('.saturdayaminfo').hide();
       	}

    });


    $(document).on('change', 'input[name="saturdaypm"]', function() {
    
    	var ischecked = $("#saturdaypm").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		$('.saturdaypminfo').show();

    	}else{

    		$('.saturdaypminfo').hide();
       	}

    });

   // $(document).on('change', 'select[name="new_employee_id[]"]', function() {
    
    	var selected_employee = $('#empid').val();

    	$.get('/employee/employeeinfo', { employee_id : selected_employee}, function(data){
            console.log(data);
            //console.log(data.HKID);
            //console.log( $('#new_client_main_form').find('.item-container').find('input[name="new_hkid[]"]'));
            
            $('#new_employee_main_form').find('.item-container').find('select[name="gender[]"]').val(data.Gender);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_nickname[]"]').val(data.NickName);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_fname[]"]').val(data.first_name);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_lname[]"]').val(data.last_name);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_email[]"]').val(data.email_address);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_supervisor[]"]').val(data.SupervisorID);
            $('select[name="new_employee_id[]"]').parent().parent().find('select[name="new_nationality_id[]"]').val(data.Nationality);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_dob[]"]').val(data.DateOfBirth);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_pob[]"]').val(data.PlaceOfBirth);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_datehired[]"]').val(data.DateHire);
            $('#new_employee_main_form').find('.item-container').find('input[name="new_weekstarts[]"]').val(data.WeekStartsOn);
            
             $('select[name="new_employee_id[]"]').parent().parent().find('select[name="bloodtype[]"]').val(data.BloodType);
             $('select[name="new_employee_id[]"]').parent().parent().find('select[name="marital_status[]"]').val(data.MartialStatus);
             //$('select[name="new_employee_id[]"]').parent().parent().find('select[name="new_role_id[]"]').val(data.role_id);


            if(data.WeekdayAM_Apply=='1'){
               	$("#weekdayam").prop('checked',true);
               	  $('.weekdayaminfo').show();
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdayamfrom[]"]').val(data.WeekdayAM_Fr);
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdayamto[]"]').val(data.WeekdayAM_To);
               }
            else{
               $("#weekdayam").prop('checked',false);
               	$('.weekdayaminfo').hide();
               	$('#new_employee_main_form').find('.item-container').find('input[name="weekdayamfrom[]"]').val('');
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdayamto[]"]').val('');
           	}

           	if(data.WeekdayPM_Apply=='1'){
               	$("#weekdaypm").prop('checked',true);
               	  $('.weekdaypminfo').show();
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdaypmfrom[]"]').val(data.WeekdayPM_Fr);
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdaypmto[]"]').val(data.WeekdayPM_To);
               }
            else{
               $("#weekdaypm").prop('checked',false);
               	$('.weekdaypminfo').hide();
               	$('#new_employee_main_form').find('.item-container').find('input[name="weekdaypmfrom[]"]').val('');
               	  $('#new_employee_main_form').find('.item-container').find('input[name="weekdaypmto[]"]').val('');
           	}

           	if(data.SaturdayAM_Apply=='1'){
               	$("#saturdayam").prop('checked',true);
               	  $('.saturdayaminfo').show();
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdayamfrom[]"]').val(data.SaturdayAM_Fr);
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdayamto[]"]').val(data.SaturdayAM_To);
               }
            else{
               $("#saturdayam").prop('checked',false);
               	$('.saturdayaminfo').hide();
               	$('#new_employee_main_form').find('.item-container').find('input[name="saturdayamfrom[]"]').val('');
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdayamto[]"]').val('');
           	}

           	if(data.SaturdayPM_Apply=='1'){
               	$("#saturdaypm").prop('checked',true);
               	  $('.saturdaypminfo').show();
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdaypmfrom[]"]').val(data.SaturdayPM_Fr);
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdaypmto[]"]').val(data.SaturdayPM_To);
               }
            else{
               $("#saturdaypm").prop('checked',false);
               	$('.saturdaypminfo').hide();
               	$('#new_employee_main_form').find('.item-container').find('input[name="saturdaypmfrom[]"]').val('');
               	  $('#new_employee_main_form').find('.item-container').find('input[name="saturdaypmto[]"]').val('');
           	}
            
            var userid = data.user_id;
              $('#new_employee_main_form').find('.item-container').find('input[name="userid[]"]').val(userid);
             console.log(userid);
             var values=[];
	         $.get('/employee/roleinfo', { user_id : userid}, function(data1){
	         	
	          	for(var index=0;index<data1.length;index++){
	          		values.push(data1[index].role_id);
	          		console.log(values);
				   $(".role").val(values);

				    if(values.contains(2)){
				    	//$.get('/employee/assistinfo', { user_id : userid}, function(data1){
             				$("#handlers").css("display", "none");
                    		$("#pa").css("display", "block");
                    	//}
                    }else  if(values.contains(4)){
                    	$("#handlers").css("display", "block");
                    	$("#pa").css("display", "none");
                    }
				   $(".role").multiselect("refresh");
				 
				}
			
	        });

	         $.get('/employee/assistinfo', { user_id : selected_employee}, function(data3){

	         		console.log(data3);
	         		for(var index=0;index<data3.length;index++){
	          		values.push(data3[index].assistant_id);
	          		console.log(values);
				   $(".pa").val(values);
				   $(".pa").multiselect("refresh");
				 
				}
	         });

	         $.get('/employee/handlerinfo', { user_id : selected_employee}, function(data4){

	         		console.log(data4);
	         		for(var index=0;index<data4.length;index++){
	          		values.push(data4[index].employee_id);
	          		console.log(values);
				   $(".handle").val(values);
				   $(".handle").multiselect("refresh");
				 
				}
	         });

            
       // });

		
	});
});

    /* Save New conference */
    $('#new_employee_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

    	$.post('/employee/updateemployee', $(this).serializeArray(), function(data){
	            console.log(data);
	            if(data.status == 'success') {
	                toastr.success(data.message);
		            var delay = 3000; //Your delay in milliseconds
		            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
	            } else {
	                toastr.error(data.message);
	            }
	        }); 

    });  

    	
		
</script>
@stop
@extends('template/default')
@section('content')
<!--<link rel='stylesheet' href='/assets/fullcalendar/fullcalendar.css' />-->
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3>Communication Center</h3>
        </div>
        <div class="col-xs-12">
            <ul class="nav nav-tabs" role="tablist">
              <li class="active"><a href="#mytodolist" data-toggle="tab">My To-Do Lists</a></li>
              <li><a href="#mycalendar" data-toggle="tab">My Calendar</a></li>
              <li><a href="#mydropbox" data-toggle="tab">My Dropbox</a></li>
            </ul>
        </div>        
    </div>
</div>
<!--<script src='/assets/fullcalendar/lib/moment.min.js'></script>
<script src='/assets/fullcalendar/fullcalendar.min.js'></script>-->
@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="/assets/DataTables-1.10.0/extensions/TableTools/css/dataTables.tableTools.css">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3><span class="glyphicon glyphicon-wrench"></span> Sysadmin - User Admin</h3>
        </div>
        @include('pages/common/sysadmin_menu')
    </div>    
    <div class="row top-buffer">    
        <div class="col-xs-12">
            <table class="table table-bordered table-striped table-condensed" id="user_table">
                <thead>
                    <tr>
                        <th style="text-align:center">User</th>
                        <th style="text-align:center">Name</th>
                        <th style="text-align:center">User Roles</th>
                        <th style="text-align:center">Password Status</th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                        <th style="text-align:center"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user): ?>
                    <tr data-user-id="<?php echo $user->user_id?>" data-employee-id="<?php echo $user->EmployeeID ?>">
                        <td style="text-align:center"><?php echo $user->Photo != null ? '<img class="img-thumbnail" style="width:75px" src="/assets/thisapp/images/staff/'.$user->Photo.'">':'NA' ?></td>
                        <td style="text-align:center"><?php echo $user->EmployeeID.' - '.$user->first_name ?></td>
                        <td style="text-align:center">
                            <?php
                            $roles = json_decode($user->user_roles);
                            if($roles){
                                foreach($roles as $role){
                                    echo '<span class="user-role" data-role-id="'.$role->role_id.'">'.$role->role.'</span><br>';
                                }
                            }
                            ?>
                        </td>
                        <td style="text-align:center">
                            <?php
                            if($user->changed_password == "0"){
                                echo '<span class="glyphicon glyphicon-exclamation-sign text-warning"></span> STILL USING DEFAULT PASSWORD';
                            } else {
                                echo '<span class="glyphicon glyphicon-ok text-success"></span> PASSWORD CHANGED';
                            }
                            ?>
                        </td>
                        <td style="text-align:center"><button class="btn btn-info btn-sm view-login-history">View User Login History</button></td>
                        <td style="text-align:center"><button class="btn btn-info btn-sm" disabled>Change User Roles</button></td>
                        <td style="text-align:center"><button class="btn btn-info btn-sm edit-employee-info">Edit Employee Info</button></td>
                        <td style="text-align:center"><button class="btn btn-danger btn-sm reset-password"><span class="glyphicon glyphicon-refresh"></span> Reset Password</button></td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>        
    </div>
</div>
<script type="text/javascript" src="/assets/thisapp/js/sysadmin.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script src="/assets/DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
@stop  
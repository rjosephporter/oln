<form class="" role="form" method="POST" action="{{ URL::to('timesheet/save-disbursement-ledger') }}">
  <div class="row">
    <div class="col-md-6 col-xs-6">
      <div class="form-group">
        <label for="ledger[matter_id]">Matter</label>
        {{ Form::select('ledger[matter_id]', $ledger['matter_list'], $ledger['selected_matter'], array('class' => 'form-control input-sm', 'required' => 'true')) }}
      </div>
    </div>
    <div class="col-md-6 col-xs-6">
      <div class="form-group">
          <label for="ledger[amount]">Amount</label>
          <input type="number" step="any" class="form-control input-sm" name="ledger[amount]" placeholder="Amount" required>
      </div>
    </div>    
  </div>
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <div class="form-group">
        <label for="ledger[description]">Disbursement</label>
        <input type="text" class="form-control input-sm" name="ledger[description]" placeholder="Disbursment Description" required>
      </div>
    </div>
  </div>  
  <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i> Save</button>
</form>
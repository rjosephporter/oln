<table class="table table-bordered table-condensed table-striped">
	<thead>
		<tr>
			<th>Matter ID</th>
			<th>Disbursement</th>
			<th>Date</th>
			<th>Amount</th>
		</tr>
	</thead>
	<tbody>
		@foreach($ledger['list'] as $list)
			<tr>
				<td>{{ $list->matter_id }}</td>
				<td>{{ $list->description }}</td>
				<td>{{ date('F j, Y', strtotime($list->date_created)) }}</td>
				<td>${{ number_format($list->amount,2) }}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" class="text-right"><strong>TOTAL</strong></td>
			<td><strong>${{ number_format($ledger['list_total'],2) }}</strong></td>
		</tr>
	</tfoot>
</table>
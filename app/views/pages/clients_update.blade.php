@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i>&nbsp;&nbsp;{{ Lang::get('lang.client.Update Client') }} 
			</h2>
			<div class="modal-content" id="new_client_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_client_main_form">
							<div class="col-md-12 form-group">
							<label for="new_clienttype_id1">{{ Lang::get('lang.client.Client Type') }}</label>
							<select class="form-control" name="new_clienttype_id" id="new_clienttype_id"placeholder="Client Type" required disabled>
									
							<option value="1" {{  $clientinfo->CUSTTYPE  == 1 ? 'selected' : '' }} >Individual</option>
							<option value="2" {{  $clientinfo->CUSTTYPE  == 2 ? 'selected' : '' }} >Corporate</option>
									
						     </select>
						     <input type="hidden" name="ctype" id="ctype">
						     <input type="hidden" name="cid" id="cid" value="{{$client_id}}">
						 	</div>
						 	<!--individual-->
						 	<div id="item-container-source" style="display:none">
							<div class="col-md-12 item-container">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">
												
												<div class="col-md-12 form-group">
													<label for="new_customer_id1">{{ Lang::get('lang.client.Customer') }}</label>
													<select class="form-control" name="new_customer_id[]" id="new_customer_id[]" placeholder="Select Client" readonly disabled>
															<option></option>
															@foreach($client_list as $client)
																<option value="{{ $client->CustomerID }}"{{  $client_id  == $client->CustomerID ? 'selected' : '' }}>{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
															@endforeach
												     </select>
												</div>
																						
												<div class="col-md-12 form-group">
														<label for="new_clientname">{{ Lang::get('lang.client.Client Name') }} </label>
														<input type="text" class="form-control check" id="new_clientname[]" name="new_clientname[]" value="{{$clientinfo->CompanyName_A1}}">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_hkid">{{ Lang::get('lang.client.Hong Kong ID Card No/Passport No') }} </label>
														<input type="text" class="form-control check" id="new_hkid[]" name="new_hkid[]" value="{{$clientinfo->HKID}}">
												</div>	

												<div class="col-md-12 form-group">
													<label for="new_dob">{{ Lang::get('lang.client.Date of Birth') }}  <small>{{ Lang::get('lang.client.(click to select date)') }}</small></label>
													<input type="text" class="form-control check" name="new_dob[]" id="new_dob[]" readonly value="{{$clientinfo->DATEOFBIRTH}}">
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_pob">{{ Lang::get('lang.client.Place of Birth') }} </label>
														<input type="text" class="form-control check" id="new_pob[]" id="new_pob[]" name="new_pob[]" value="{{$clientinfo->PLACEOFBIRTH}}">
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nationality_id1">{{ Lang::get('lang.client.Nationality') }}</label>
													<select class="form-control check" name="new_nationality_id[]" id="new_nationality_id[]" placeholder="Nationality">
															<option></option>
															@foreach($nationality_list as $nat)
																<option value="{{ $nat->CountryCode }}"{{ $clientinfo->NATIONALITY  == $nat->CountryCode ? 'selected' : '' }}>{{ $nat->CountryName_A }}</option>
															@endforeach
												     </select>
												</div>				
													
												
												<div class="col-md-12 form-group">
													<label for="new_acting_id1">{{ Lang::get('lang.client.Acting for another person') }}?</label>
													<input type="checkbox" class="form-control" id="new_acting_id" name="new_acting_id">
												</div>
												<div id="addinfo" style="display:none">
												<div class="col-md-12 form-group">
														<label for="new_personname">{{ Lang::get('lang.client.Name') }} </label>
														<input type="text" class="form-control" id="new_personname[]" name="new_personname[]" value="{{$clientinfo->REPNAME}}">
												</div>
												<div class="col-md-12 form-group">
														<label for="new_contactnum">{{ Lang::get('lang.client.Contact Number') }} </label>
														<input type="text" class="form-control" id="new_contactnum[]" name="new_contactnum[]" value="{{$clientinfo->REPNUM}}">
												</div>
												</div>
											
											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
														<label for="new_home_address">{{ Lang::get('lang.client.Home Address') }} </label>
														<input type="text" class="form-control check" id="new_home_address[]" name="new_home_address[]" novalidate value="{{$clientinfo->HOMEADDRESS}}">
												</div>
												<div class="col-md-12 form-group">
														<label for="new_cor_address">{{ Lang::get('lang.client.Correspondence Address (if different)') }}</label>
														<input type="text" class="form-control" id="new_cor_address[]" name="new_cor_address[]" value="{{$clientinfo->CORADDRESS}}">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_occupation">{{ Lang::get('lang.client.Occupation') }}</label>
														<input type="text" class="form-control check" id="new_occupation[]" name="new_occupation[]" novalidate value="{{$clientinfo->OCCUPATION}}">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_employer">{{ Lang::get('lang.client.Employer') }} </label>
														<input type="text" class="form-control check" id="new_employer[]" name="new_employer[]" novalidate value="{{$clientinfo->EMPLOYER}}">
												</div>	
												
												<div class="col-md-6 form-group">
														<label for="new_hometel">{{ Lang::get('lang.client.Home Telephone Number') }} </label>
														<input type="text" class="form-control" id="new_hometel[]" name="new_hometel[]" value="{{$clientinfo->TELNUM}}">
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_homefax">{{ Lang::get('lang.client.Home Fax Number') }} </label>
														<input type="text" class="form-control" id="new_homefax[]" name="new_homefax[]" value="{{$clientinfo->FAXNUM}}">
												</div>
												<div class="col-md-6 form-group">
														<label for="new_offtel">{{ Lang::get('lang.client.Office Telephone Number') }} </label>
														<input type="text" class="form-control" id="new_offtel[]" name="new_offtel[]" value="{{$clientinfo->OFFICETEL}}">
												</div>
												<div class="col-md-6 form-group">
														<label for="new_mobile">{{ Lang::get('lang.client.Mobile Number') }} </label>
														<input type="text" class="form-control check" id="new_mobile[]" name="new_mobile[]" novalidate value="{{$clientinfo->MOBILE}}">
												</div>
												<div class="col-md-12 form-group">
														<label for="new_email">{{ Lang::get('lang.client.Email Address') }} </label>
														<input type="text" class="form-control check" id="new_email[]" name="new_email[]" novalidate value="{{$clientinfo->EMAIL}}">
												</div>
												<div class="col-md-12 form-group">
													<label for="new_handler_id">{{ Lang::get('lang.client.Incharge') }}</label>
													<select class="form-control check" name="new_handler_id[]" placeholder="Select Incharge" novalidate>
															<option></option>
															@foreach($handler_list as $handler)
																<option value="{{ $handler->EmployeeID }}"{{ $clientinfo->Incharge  == $handler->EmployeeID ? 'selected' : '' }}>{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
															@endforeach
												     </select>
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						 	<!--corporate-->
						 	<div id="item-container-source2" style="display:none">
							<div class="col-md-12 item-container2">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<br>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">

												<div class="col-md-12 form-group">
													<label for="new_customer_id2">{{ Lang::get('lang.client.Customer') }}</label>
													<select class="form-control" name="new_customer_id2[]" id="new_customer_id2[]" placeholder="Select Client" disabled>
															<option></option>
															@foreach($client_list2 as $client)
																<option value="{{ $client->CustomerID }}"{{ $client_id  == $client->CustomerID ? 'selected' : '' }}>{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
															@endforeach
												     </select>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_clientcomname">{{ Lang::get('lang.client.Client Company Name') }} </label>
														<input type="text" class="form-control check2" id="new_clientcomname[]" name="new_clientcomname[]" novalidate value="{{$clientinfo->CompanyName_A1}}">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_comnum">{{ Lang::get('lang.client.Company No') }}. </label>
														<input type="text" class="form-control check2" id="new_comnum[]" name="new_comnum[]" novalidate value="{{$clientinfo->ComNum}}">
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_poi">{{ Lang::get('lang.client.Place of Incorporation') }}</label>
														<input type="text" class="form-control check2" id="new_poi[]" name="new_poi[]" novalidate value="{{$clientinfo->PLACEOFINC}}">
												</div>

												<div class="col-md-12 form-group">
														<label for="new_bus_address">{{ Lang::get('lang.client.Business Address') }} </label>
														<input type="text" class="form-control check2" id="new_bus_address[]" name="new_bus_address[]" novalidate value="{{$clientinfo->BUSADD}}"> 
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nature">{{ Lang::get('lang.client.Nature of Business') }}</label>
													<input type="text" class="form-control check2" id="new_nature[]" name="new_nature[]" novalidate value="{{$clientinfo->Nature_A}}">
												</div>	

												<div class="col-md-6 form-group">
														<label for="new_telnum">{{ Lang::get('lang.client.Telephone Number') }} </label>
														<input type="text" class="form-control check2" id="new_telnum[]" name="new_telnum[]" novalidate value="{{$clientinfo->TELNUM}}">
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_faxnum">{{ Lang::get('lang.client.Fax Number') }}</label>
														<input type="text" class="form-control check2" id="new_faxnum[]" name="new_faxnum[]" novalidate value="{{$clientinfo->FAXNUM}}">
												</div>			

											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
												    <label for="new_dir">{{ Lang::get('lang.client.Name(s) of Director(s)') }} </label>
												    <div>
												    	
												    	<input type="text" class="form-control check2" id="new_dir[]" name="new_dir[]">
												    	<input type="hidden" class="form-control check2" id="new_dirid[]" name="new_dirid[]">
														
												    </div>
												</div>
												<div class="col-md-12 input_fields_wrapdir">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
								                <div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
												<!--<div class="col-md-12 form-group input_fields_wrapdir">
												    <label for="new_dir">Name(s) of Director(s) </label>
												    <div>
												    	
												    	<input type="text" class="form-control check2" name="new_dir[]">
														
												    </div>
												    
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>-->
												
												<div class="col-md-12 form-group">
												    <label for="new_share">{{ Lang::get('lang.client.Name(s) of Shareholder(s)') }} </label>
												    <div><input type="text" class="form-control check2" id="new_share[]" name="new_share[]">
													<input type="hidden" class="form-control check2" id="new_shareid[]" name="new_shareid[]">
												    </div>
												    
												</div>
												<div class="col-md-12 input_fields_wrapshare">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonshare">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_ben">{{ Lang::get('lang.client.Name(s) of Ultimate Beneficial Owner(s) (if different from Shareholder)') }} </label>
												    <div><input type="text" class="form-control check2" id="new_ben[]" name="new_ben[]">
													<input type="hidden" class="form-control check2" id="new_ownid[]" name="new_ownid[]">
												    </div>
												    
												</div>
												<div class="col-md-12  input_fields_wrapben">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonben">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_person">{{ Lang::get('lang.client.Name of Person(s) representing the Company') }} </label>
												    <div><input type="text" class="form-control check2" id="new_person[]" name="new_person[]">
													<label for="new_personhkid">{{ Lang::get('lang.client.HKID Card No/Passport No') }} </label>
													<input type="text" class="form-control check2" id="new_personhkid[]" name="new_personhkid[]">
													<label for="new_personpos">{{ Lang::get('lang.client.Capacity Position') }} </label>
													<input type="text" class="form-control check2" id="new_personpos[]" name="new_personpos[]">
													<label for="new_personconnum">{{ Lang::get('lang.client.Contact Number') }} </label>
													<input type="text" class="form-control check2" id="new_personconnum[]" name="new_personconnum[]">
													<label for="new_personemail">{{ Lang::get('lang.client.Email Address') }} </label>
													<input type="text" class="form-control check2" id="new_personemail[]" name="new_personemail[]">
													<input type="hidden" class="form-control check2" id="new_repid[]" name="new_repid[]">
												    </div>
												    
												</div>
												<div class="col-md-12  input_fields_wrapperson">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonperson">{{ Lang::get('lang.client.Add Item') }}</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
											</div>
										
										</div>
									</div>
								</div>	
							</div>
						</div>
						</div>
						<div class="col-md-12">
							<!--here-->

							<!--end here--><!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/clients') }}" class="btn btn-default">{{ Lang::get('lang.client.Back') }}</a>
						<button type="submit" class="btn btn-primary">{{ Lang::get('lang.client.Save') }}</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>






<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/client_update.js"></script>
<script>
$(document).ready(function(){


	$('#ctype').val($('select[name="new_clienttype_id"]').val()); 
	//console.log('cid'+$('#cid').val());
});


$(document).on('change', 'input[name="new_acting_id"]', function() {
    
    	var ischecked = $("#new_acting_id").is(":checked"); 
    	console.log(ischecked);
    	if(ischecked == true){
    		//$('#new_client_main_form').find(item_container2).remove();
    		$('#addinfo').show();
    		
    		//$('#item-container-source2').hide();
    		
    	}else{
    		//$('#new_client_main_form').find(item_container).remove();
    		
    		$('#addinfo').hide();
    		//$('#item-container-source').hide();
    	}

    	
    });
    $('#new_client_log2 form').submit(function(e){
        e.preventDefault();
        //console.log($(this).serializeArray());

        var type=$('#ctype').val();
        //console.log('type='+ $('#ctype').val());
        
        var empty_fields = [];
        if(type==1){
			 $(".check").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}else{
			 $(".check2").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}
		if(empty_fields.length>0){
				console.log(empty_fields);
				//toastr.error(empty_fields);
		        toastr.error('All fields are required.');
		        empty_fields.length=0;
		 }else{

	       $.post('/clients/updateclient', $(this).serializeArray(), function(data){
	            console.log(data);
	            if(data.status == 'success') {
	                toastr.success(data.message);
		            var delay = 3000; //Your delay in milliseconds
		            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
	            } else {
	                toastr.error(data.message);
	            }
	        }); 
  		 }
    	
    });  
</script>

@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-folder-open"></i>  Delete Client 
			</h2>
			<div class="modal-content" id="new_client_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_client_main_form">
							<div class="col-md-12 form-group">
							<label for="new_clienttype_id1">Client Type</label>
							<select class="form-control" name="new_clienttype_id" placeholder="Client Type" required>
									
							<option value="1">Individual</option>
							<option value="2">Corporate</option>
									
						     </select>
						 	</div>
						 	<!--individual-->
						 	<div id="item-container-source" style="display:none">
							<div class="col-md-12 item-container">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">
							
												<div class="col-md-12 form-group">
													<label for="new_customer_id">Customer</label>
													<select class="form-control" name="new_customer_id[]" placeholder="Select Client">
															<option></option>
															@foreach($client_list as $client)
																<option value="{{ $client->CustomerID }}">{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
															@endforeach
												     </select>
												</div>
																						
												<div class="col-md-12 form-group">
														<label for="new_clientname">Client Name </label>
														<input type="text" class="form-control check" id="new_clientname[]" name="new_clientname[]">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_hkid">Hong Kong ID Card No/Passport No </label>
														<input type="text" class="form-control check" id="new_hkid[]" name="new_hkid[]">
												</div>	

												<div class="col-md-12 form-group">
													<label for="new_dob">Date of Birth  <small>(click to select date)</small></label>
													<input type="text" class="form-control check" name="new_dob[]" readonly>
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_pob">Place of Birth </label>
														<input type="text" class="form-control check" id="new_pob[]" name="new_pob[]">
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nationality_id">Nationality</label>
													<select class="form-control check" name="new_nationality_id[]" placeholder="Nationality">
															<option></option>
															@foreach($nationality_list as $nat)
																<option value="{{ $nat->CountryCode }}">{{ $nat->CountryName_A }}</option>
															@endforeach
												     </select>
												</div>				
													
												
												<div class="col-md-12 form-group">
													<label for="new_acting_id1">Acting for another person?</label>
													<input type="checkbox" class="form-control" id="new_acting_id" name="new_acting_id" >
												</div>
												<div id="addinfo" style="display:none">
												<div class="col-md-12 form-group">
														<label for="new_personname">Name </label>
														<input type="text" class="form-control" id="new_personname[]" name="new_personname[]" >
												</div>
												<div class="col-md-12 form-group">
														<label for="new_contactnum">Contact Number </label>
														<input type="text" class="form-control" id="new_contactnum[]" name="new_contactnum[]" >
												</div>
												</div>
											
											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
														<label for="new_home_address">Home Address </label>
														<input type="text" class="form-control check" id="new_home_address[]" name="new_home_address[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_cor_address">Correspondence Address (if different) </label>
														<input type="text" class="form-control" id="new_cor_address[]" name="new_cor_address[]">
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_occupation">Occupation </label>
														<input type="text" class="form-control check" id="new_occupation[]" name="new_occupation[]" novalidate>
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_employer">Employer </label>
														<input type="text" class="form-control check" id="new_employer[]" name="new_employer[]" novalidate>
												</div>	
												
												<div class="col-md-6 form-group">
														<label for="new_hometel">Home Telephone Number </label>
														<input type="text" class="form-control" id="new_hometel[]" name="new_hometel[]" >
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_homefax">Home Fax Number </label>
														<input type="text" class="form-control" id="new_homefax[]" name="new_homefax[]" >
												</div>
												<div class="col-md-6 form-group">
														<label for="new_offtel">Office Telephone Number </label>
														<input type="text" class="form-control" id="new_offtel[]" name="new_offtel[]" >
												</div>
												<div class="col-md-6 form-group">
														<label for="new_mobile">Mobile Number </label>
														<input type="text" class="form-control check" id="new_mobile[]" name="new_mobile[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_email">Email Address </label>
														<input type="text" class="form-control check" id="new_email[]" name="new_email[]" novalidate>
												</div>
												<div class="col-md-12 form-group">
													<label for="new_handler_id">Incharge</label>
													<select class="form-control check" name="new_handler_id[]" placeholder="Select Incharge" novalidate>
															<option></option>
															@foreach($handler_list as $handler)
																<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
															@endforeach
												     </select>
												</div>
											</div>
										</div>
									</div>
								</div>	
							</div>
						</div>
						 	<!--corporate-->
						 	<div id="item-container-source2" style="display:none">
							<div class="col-md-12 item-container2">
								<div class="panel panel-default">
									<div class="panel-heading clearfix">
										<br>
									</div>
									<div class="panel-body">
										<div class="row">
											<div class="col-md-6 row">

												<div class="col-md-12 form-group">
													<label for="new_customer_id2">Customer</label>
													<select class="form-control" name="new_customer_id2[]" placeholder="Select Client">
															<option></option>
															@foreach($client_list2 as $client)
																<option value="{{ $client->CustomerID }}">{{ $client->CustomerID }} - {{ $client->CompanyName_A1 }}</option>
															@endforeach
												     </select>
												</div>
												<div class="col-md-12 form-group">
														<label for="new_clientcomname">Client Company Name </label>
														<input type="text" class="form-control check2" id="new_clientcomname[]" name="new_clientcomname[]" novalidate>
												</div>	
												<div class="col-md-12 form-group">
														<label for="new_comnum">Company No. </label>
														<input type="text" class="form-control check2" id="new_comnum[]" name="new_comnum[]" novalidate>
												</div>	

												<div class="col-md-12 form-group">
														<label for="new_poi">Place of Incorporation </label>
														<input type="text" class="form-control check2" id="new_poi[]" name="new_poi[]" novalidate>
												</div>

												<div class="col-md-12 form-group">
														<label for="new_bus_address">Business Address </label>
														<input type="text" class="form-control check2" id="new_bus_address[]" name="new_bus_address[]" novalidate>
												</div>

												<div class="col-md-12 form-group">
													<label for="new_nature">Nature of Business</label>
													<input type="text" class="form-control check2" id="new_nature[]" name="new_nature[]" novalidate>
												</div>	

												<div class="col-md-6 form-group">
														<label for="new_telnum">Telephone Number </label>
														<input type="text" class="form-control check2" id="new_telnum[]" name="new_telnum[]" novalidate>
												</div>	
												<div class="col-md-6 form-group">
														<label for="new_faxnum">Fax Number </label>
														<input type="text" class="form-control check2" id="new_faxnum[]" name="new_faxnum[]" novalidate>
												</div>			

											</div>
											<div class="col-md-6 row">
												<div class="col-md-12 form-group">
												    <label for="new_dir">Name(s) of Director(s) </label>
												    <div>
												    	
												    	<input type="text" class="form-control check2" id="new_dir[]" name="new_dir[]">
												    	<input type="hidden" class="form-control check2" id="new_dirid[]" name="new_dirid[]">
														
												    </div>
												</div>
												<div class="col-md-12 input_fields_wrapdir">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
								                <div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
												<!--<div class="col-md-12 form-group input_fields_wrapdir">
												    <label for="new_dir">Name(s) of Director(s) </label>
												    <div>
												    	
												    	<input type="text" class="form-control check2" name="new_dir[]">
														
												    </div>
												    
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttondir">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>-->
												
												<div class="col-md-12 form-group">
												    <label for="new_share">Name(s) of Shareholder(s) </label>
												    <div><input type="text" class="form-control check2" id="new_share[]" name="new_share[]">
													<input type="hidden" class="form-control check2" id="new_shareid[]" name="new_shareid[]">
												    </div>
												    
												</div>
												<div class="col-md-12 input_fields_wrapshare">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonshare">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_ben">Name(s) of Ultimate Beneficial Owner(s) (if different from Shareholder) </label>
												    <div><input type="text" class="form-control check2" id="new_ben[]" name="new_ben[]">
													<input type="hidden" class="form-control check2" id="new_ownid[]" name="new_ownid[]">
												    </div>
												    
												</div>
												<div class="col-md-12  input_fields_wrapben">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonben">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>

												<div class="col-md-12 form-group">
												    <label for="new_person">Name of Person(s) representing the Company </label>
												    <div><input type="text" class="form-control check2" id="new_person[]" name="new_person[]">
													<label for="new_personhkid">HKID Card No/Passport No </label>
													<input type="text" class="form-control check2" id="new_personhkid[]" name="new_personhkid[]">
													<label for="new_personpos">Capacity Position </label>
													<input type="text" class="form-control check2" id="new_personpos[]" name="new_personpos[]">
													<label for="new_personconnum">Contact Number </label>
													<input type="text" class="form-control check2" id="new_personconnum[]" name="new_personconnum[]">
													<label for="new_personemail">Email Address </label>
													<input type="text" class="form-control check2" id="new_personemail[]" name="new_personemail[]">
													<input type="hidden" class="form-control check2" id="new_repid[]" name="new_repid[]">
												    </div>
												    
												</div>
												<div class="col-md-12  input_fields_wrapperson">
												
								                </div>
								                <div class="col-md-12 form-group">
												</div>
												<div class="col-md-12">
													<button class="btn btn-info btn-sm add_field_buttonperson">Add Item</button>
												</div>
												<div class="col-md-12 form-group">
												</div>
											</div>
										
										</div>
									</div>
								</div>	
							</div>
						</div>
						</div>
						<div class="col-md-12">
							<!--here-->

							<!--end here--><!--<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>-->
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/clients') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary">Delete</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>






<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/clients.js"></script>
<script>
/* Save New conference */
    $('#new_client_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

        var type=$('select[name="new_clienttype_id"]').val();
        console.log('type='+type);
        var empty_fields = [];
        if(type==1){
			 $(".check").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}else{
			 $(".check2").each(function(){
			    if($(this).val().length !=0 ) {
			        return;
			  }else{
			    empty_fields.push($(this).attr('name'))
			    event.preventDefault();}
			})
		}
		if(empty_fields.length>0){
				console.log(empty_fields);
				//toastr.error(empty_fields);
		        toastr.error('All fields are required.');
		        empty_fields.length=0;
		 }else{

	       $.post('/clients/deleteclient', $(this).serializeArray(), function(data){
	            console.log(data);
	            if(data.status == 'success') {
	                toastr.success(data.message);
		            var delay = 3000; //Your delay in milliseconds
		            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
	            } else {
	                toastr.error(data.message);
	            }
	        }); 
  		 }
    	
    });  
</script>

@stop
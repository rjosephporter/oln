@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<style>
.pagination>.active>a, .pagination>.active>span, 
.pagination>.active>a:hover, .pagination>.active>span:hover, 
.pagination>.active>a:focus, .pagination>.active>span:focus {
	z-index: 1;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="hidden-print">
				<i class="glyphicon glyphicon-time"></i> Timesheet Report
				@if(!Input::has('non_billable')) 
				<span class="label label-primary">Billable Entries</span>
				<a href="{{ URL::to('timesheet/report/daily?non_billable=1') }}" class="btn btn-success">Switch to Non-Billable Entries</a>
				@else
				<span class="label label-primary">Non-Billable Entries</span>
				<a href="{{ URL::to('timesheet/report/daily') }}" class="btn btn-success">Switch to Billable Entries</a>
				@endif

				
			</h2>
			
			<div class="form-inline hidden-print" role="form" style="margin: 25px 0px 15px 0px;">
				<div class="form-group hidden">
					<label for="matter_id">Matter ID  <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Search for Matter ID or Description and choose from the list of results."></i></label>
					<select class="form-control" id="matter_id" filter-for="timesheet" placeholder="Search Matter" style="width:135px">

					</select>
				</div>
				<div class="form-group"></div>
				<div class="form-group">
					<label for="handler_id">Employee ID  <i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="Search for Employee ID or Name and choose from the list of results."></i></label>
					<select class="form-control" id="handler_id" filter-for="timesheet" placeholder="Search Employee" style="width:155px">
						<option></option>
						@foreach($handler_list as $ndx => $handler)
						<option value="{{ $handler->EmployeeID }}" {{ $principal_employee_id == $handler->EmployeeID ? 'selected' : '' }} >{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="from_date">From <small>(click to change)</small></label>
					<input type="text" class="form-control" id="from_date" filter-for="timesheet" placeholder="Date" value="{{ date('F j, Y', strtotime('1 week ago')) }}" readonly>
				</div>
				<div class="form-group"></div>
				<div class="form-group">
					<label for="to_date">To <small>(click to change)</small></label>
					<input type="text" class="form-control" id="to_date" filter-for="timesheet" placeholder="Date" value="{{ date('F j, Y') }}" readonly>
				</div>
				<div class="form-group"></div>

				<input type="hidden" id="view_type" value="{{ Input::has('non_billable') ? 'non_billable' : 'billable' }}">

				{{-- <a href="{{ URL::to('/timesheet/new') }}" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add new</a> --}}
				<button class="btn btn-info pull-right" id="print-report"><i class="fa fa-print"></i> Print</button>
			</div>

			<div class="container visible-print">
				<div class="row">
					<div class="col-md-12 text-center">
						<h4>OLDHAM, LI & NIE LAWYERS</h4>
						<h3>Staff's Daily Report - {{ Input::has('non_billable') ? 'Non-Billable Entries' : 'Billable Entries' }}</h3>
						<h4 id="text-handler"></h4>
						<h4>For The Period From <span id="text-from-date"></span> to <span id="text-to-date"></span></h4>
					</div>
				</div>
			</div>			

			<div id="timesheet-list-wrapper">
            {{ 
                Datatable::table()                    
                    ->addColumn('Work&nbsp;Date', 'Employee', 'Matter ID', 'Description', 'WorkHour', 'Units', 'Created&nbsp;On', 'Owned', 'Unique ID', 'Update/Delete', 'Action')
                    ->setClass('table table-bordered table-striped cell-border')
                    ->noScript()
                    ->render() 
            }}
        	</div>
		</div>
	</div>
</div>

<!-- Modals -->
<!--
<div class="modal fade" id="new_timesheet_log" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form role="form" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">New Timesheet</h4>
			</div>
			<div class="modal-body">
				<div class="row" id="new_timesheet_main_form">
					<div class="col-md-4 form-group">
						<label for="new_matter_id">Matter ID</label>
						<input type="text" class="form-control" name="new_matter_id" readonly>
					</div>
					<div class="col-md-4 form-group">
						<label for="new_timesheet_date">Date</label>
						<input type="text" class="form-control" name="new_timesheet_date" readonly>
					</div>
					<div class="col-md-4 form-group">
						<label for="new_hourlyrate">Hourly Rate</label>
						<input type="text" class="form-control" name="new_hourlyrate" value="{{ $hourly_rate }}" readonly>
					</div>
					
					<div class="col-md-12 item-container">
						<div class="panel panel-default">
							<div class="panel-heading clearfix">
								<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-6 row">
										<div class="col-md-12 form-group">
											<label for="new_workhour">Work Hours</label>
											<input type="number" step="any" class="form-control" name="new_workhour[]">
										</div>
										<div class="col-md-12 form-group">
											<label for="new_timecode">Time Code</label>
											<select class="form-control" name="new_timecode[]" placeholder="Select Time Code">
												<option></option>
												@foreach($timecode_list as $timecode)
													<option value="{{ $timecode->TimeCode }}">{{ $timecode->Description_A }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="col-md-6 form-group">
										<label for="new_description">Description</label>
										<textarea class="form-control" name="new_description[]" rows="3"></textarea>
										<p class="help-block">Example block-level help text here.</p>
									</div>
								</div>
							</div>
						</div>	
					</div>								
				</div>
				<div class="col-md-12">
					<button class="btn btn-info btn-sm" id="btn_add_item">Add Item</button>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-sm">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>
-->

<div class="modal fade" id="update_timesheet_log" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="update_timesheet_form" role="form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Update Timesheet</h4>
			</div>
			<div class="modal-body">
				<div class="row item-container">
					<input type="hidden" name="timesheet_id">
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_matter_id">Matter ID</label>
							<select class="form-control" name="new_matter_id" placeholder="Select Matter" required>
								<option></option>
								@foreach($matter_list as $matter)
									<option value="{{ $matter->JobAssignmentID }}">{{ $matter->JobAssignmentID }} - {{ $matter->Description_A }}</option>
								@endforeach
							</select>
						</div>	

						<div class="col-md-12 form-group">
							<label for="new_units">Units</label>
							<input type="number" step="1" class="form-control" name="new_units" required>
						</div>

						<div class="col-md-12 form-group">
							<label for="new_workhour">Work Hours</label>
							<input type="number" step="any" class="form-control" name="new_workhour_round" required readonly>
							<input type="hidden" step="any" class="form-control" name="new_workhour">
						</div>
						<!--
						<div class="col-md-12 form-group">
							<label for="new_timecode">Time Code</label>
							<select class="form-control" name="new_timecode" placeholder="Select Time Code" required>
								<option></option>
								@foreach($timecode_list as $timecode)
									<option value="{{ $timecode->TimeCode }}">{{ $timecode->Description_A }}</option>
								@endforeach
							</select>
						</div>
						-->
						<!--
						<div class="col-md-12 form-group">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="new_billable_checkbox"> Billable
								</label>
								<input type="hidden" name="new_billable" value="0">
							</div>
						</div>
						-->						
					</div>
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_timesheet_date">Date  <small>(click to select date)</small></label>
							<input type="text" class="form-control" name="new_timesheet_date" readonly required>
						</div>
						<div class="col-md-6 form-group">
								<label for="new_hourlyrate">Handler</label>
								<select class="form-control" name="new_handler" placeholder="Select Handler" required>
									<option></option>
									@foreach($handler_list as $handler)
										<option value="{{ $handler->EmployeeID }}">{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
									@endforeach
								</select>
						</div>
						<div class="col-md-6 form-group hidden">
								<label for="new_hourlyrate">Hourly Rate</label>
								<select class="form-control" name="new_hourlyrate" placeholder="Select Hourly Rate" readonly disabled>
									<option></option>
									@foreach($hourly_rate_list as $hourly_rate)
										<option value="{{ $hourly_rate->EmployeeID }}">{{ $hourly_rate->HourlyRate }}</option>
									@endforeach
								</select>								
						</div>
						<div class="col-md-12 form-group">
							<label for="new_description">Description</label>
							<textarea class="form-control" name="new_description" rows="3" required></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-sm">Update</button>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- /Modals -->

<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/timesheet.js"></script>
@stop
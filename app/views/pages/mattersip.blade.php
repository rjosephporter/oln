@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<style type="text/css">
    .custom_button{
        background-color: yellow;
    }
    .custom_button:hover{
        color: black; 
        background-color:  gold;
    }
</style>
<div class="container">
    <!-- Dashboard -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <form class="form-inline" onsubmit="return false;" name="search_timesheet" method="post">  
                        <span style="margin-right: 5px; font-weight: bold;">{{ Lang::get('lang.IP Matters.Select/Search IP Matter') }}</span>   
                        <div class="form-group">
                            <select id="target_matter" class="form-control" style="width: 400px;">
                                <option value="" selected="selected">{{ Lang::get('lang.IP Matters.Select IP Matter') }}</option>
                            @foreach($obj['matters'] as $matter)
                                <option value="{{$matter->JobAssignmentID}}">{{$matter->Incharge}} - {{$matter->JobAssignmentID}} - {{$matter->Description_A}}</option>
                            @endforeach
                            </select>
                        </div>
                        <span id="my_input_fields" style="display: none;">
                            <input type="submit" value="View on List" class="btn btn-default" onclick="process_view()"/>
                            <input type="submit" value="Create Timesheet" class="btn btn-success" onclick="process_timesheet()"/>
                            <input type="submit" value="Create Invoice" class="btn custom_button" onclick="process_invoice()"/>
                        </span>
                    </form>
                </div>
                  <div class="panel-body"> <!-- start -->
                    <div class="row">
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="green__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.IP Matters.Active Last 4 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/green-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="orange__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Between 4 and 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/orange-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="red__" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Beyond 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/red-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="gray__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.IP Matters.Untouched/No Activity') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/grey-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}')">
                            <div id="blue__" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.IP Matters.Completed Matters') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/blue-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                    </div>   
                </div><!-- End of Body -->
            </div>
        </div>
    </div>
    <!-- Dash board End -->
    <div id="delegated_to_div"></div>
    <div id="delegated_by_div"></div>
</div>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/matters_landing_ip.js"></script>
<script id="delegated_by_dashboard_tmpl" type="text/x-jquery-tmpl">
    <!-- Delegated Dashboard Start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span style="margin-right: 5px; font-weight: bold;" class="text-info">{{ Lang::get('lang.IP Matters.IP MATTERS YOU DELEGATED') }}</span>   
                </div>
                  <div class="panel-body"> <!-- start -->
                    <div class="row">
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=by&type=ip')">
                            <div id="green__b" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.IP Matters.Active Last 4 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/green-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=by&type=ip')">
                            <div id="orange__b" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Between 4 and 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/orange-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=by&type=ip')">
                            <div id="red__b" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Beyond 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/red-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=by&type=ip')">
                            <div id="gray__b" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.IP Matters.Untouched/No Activity') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/grey-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=by&type=ip')">
                            <div id="blue__b" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.IP Matters.Completed Matters') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/blue-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                    </div>   
                </div><!-- End of Body -->
            </div>
        </div>
    </div>
    <!-- Delegated Dashboard Stop -->
</script>

<script id='matter_landing_template_delegated' type="text/x-jquery-tmpl">
@{{if type == "green"}}
    </br>
    <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.IP Matters.Active Last 4 Weeks') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/green-ip.png" width="75"/>
    </div>
@{{else type == "orange"}}
    <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
    <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Between 4 and 8 Weeks') }}</h2>
    <hr>
    <div class="icon">
      <img src="/assets/thisapp/images/matters/orange-ip.png" width="75"/>
    </div>
@{{else type == "red"}}
    <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
    <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Beyond 8 Weeks') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/red-ip.png" width="75"/>
    </div>
@{{else type == "gray"}}
     </br>
     <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.IP Matters.Untouched/No Activity') }}</h2>
     <hr>
     <div class="icon">
         <img src="/assets/thisapp/images/matters/grey-ip.png" width="75"/>
     </div>
@{{else type == "blue" }}
    </br>
    <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.IP Matters.Completed Matters') }}</h2>
    <hr>
    <div class="icon">
        <img src="/assets/thisapp/images/matters/blue-ip.png" width="75"/>
    </div>
@{{/if}}
    <div class="text">
        <var>${m_count}</var>
        <label>
            <ul style="list-style: none; margin-left: 0px; padding-left: 0px; margin-top: 15px; cursor: pointer">
                <li data-toggle="tooltip" data-placement="top" title="Billable Amount"><span class="fa fa-bookmark"></span> $<span>${billable}</span></li>
                <li data-toggle="tooltip" data-placement="top" title="Billed Amount"><span class="fa fa-bookmark" style="color:green;"></span> $<span>${billed}</span></li>
                <li data-toggle="tooltip" data-placement="top" title="Unbilled Amount"><span class="fa fa-bookmark" style="color:red;"></span> $<span>${unbilled}</span></li>
            </ul>
        </label>
    </div>
    <div class="options">
        <a href="/matters/view/${lvl}/all/${Incharge}?deleg=${delegated}&type=ip" class="btn btn-primary"><i class="glyphicon glyphicon-search"></i>View</a>
    </div>
</script>

<script id="delegated_to_dashboard_tmpl" type="text/x-jquery-tmpl">
    <!-- Delegated Dashboard Start -->
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span style="margin-right: 5px; font-weight: bold;" class="text-danger">{{ Lang::get('lang.IP Matters.IP MATTERS DELEGATED TO YOU') }}</span>   
                </div>
                  <div class="panel-body"> <!-- start -->
                    <div class="row">
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/1/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=to&type=ip')">
                            <div id="green__t" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: #18bc9c;">{{ Lang::get('lang.IP Matters.Active Last 4 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/green-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/2/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=to&type=ip')">
                            <div id="orange__t" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: orange;">{{ Lang::get('lang.IP Matters.Between 4 and 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/orange-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/3/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=to&type=ip')">
                            <div id="red__t" class="hero-widget well well-sm section-box">
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Last Activity') }}</h2>
                                <h2 class="client-name" style="color: red;">{{ Lang::get('lang.IP Matters.Beyond 8 Weeks') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/red-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;"onclick="window.location.assign('/matters/view/5/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=to&type=ip')">
                            <div id="gray__t" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: gray;">{{ Lang::get('lang.IP Matters.Untouched/No Activity') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/grey-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                        <div style="width: 20%; float: left; padding: 10px; cursor: pointer;" onclick="window.location.assign('/matters/view/4/all/{{User::getEmployeeID((Session::has("principal_user")) ? Session::get("principal_user") : Auth::user()->user_id)}}?deleg=to&type=ip')">
                            <div id="blue__t" class="hero-widget well well-sm section-box">
                                </br>
                                <h2 class="client-name" style="color: blue;">{{ Lang::get('lang.IP Matters.Completed Matters') }}</h2>
                                <hr>
                                <div class="icon">
                                    <img src="/assets/thisapp/images/matters/blue-ip.png" width="75"/>
                                </div>
                                <img src="/assets/thisapp/images/ajax-loader.gif"/>
                            </div>
                        </div>
                    </div>   
                </div><!-- End of Body -->
            </div>
        </div>
    </div>
    <!-- Delegated Dashboard Stop -->
</script>

<script type='text/javascript'>
    var flag = false;
    var delegated_by = [];
    var delegated_to = [];
    
    extern_flag = true;
    
    function process_view(){
        window.open("/matters/view/7/all/all/0/1/?search=" + $("#target_matter").val());
    }
    function process_timesheet(){
        window.open("/timesheet/new?matter_id=" + $("#target_matter").val());
    }
        function process_invoice(){
        window.open("/invoice/create-new?matter_id=" + $("#target_matter").val());
    }
    $("#target_matter").change(function(data){
        var val = parseInt($("#target_matter").val());
        if(val > 0){
            if(!flag){
                flag = true;
                $("#my_input_fields").show()
            }
        }
    });
    
    function _matters_landing_js_intern(){
        $('select').selectize({sortField : 'text'}); 
        if({{count($obj['delegated_by'])}} > 0 || {{count($obj['delegated_to'])}} > 0){
            /*
            var delegated_matters = {};
            $.each(delegated_by, function(i, item){
                delegated_matters[item.JobAssignmentID] = item.JobAssignmentID;
            });
            var delegated_matters = [];
            $.each(delegated_by, function(index, elem){
                if ($.inArray(elem.JobAssignmentID,delegated_matters) === -1)
                    delegated_matters.push(elem.JobAssignmentID);
            });
            */
            var d_by = {{json_encode($obj['delegated_by'])}};
            var d_to = {{json_encode($obj['delegated_to'])}};
 
            var __all_m = all_matters;
            
            var to_flag = false;
            var _t_m = all_matters;
            for(var i = 0; i < d_to.length; i++){
                var _true = $.grep(_t_m, function (elem){
                  if(elem === undefined)
                        return false;
                   else
                   return elem.JobAssignmentID == d_to[i];
                });
                if(_true.length){
                    to_flag = true;
                    break;
                }
            }

            if(to_flag){
                $("#delegated_to_div").html($("#delegated_to_dashboard_tmpl").tmpl());
                $.each(d_to, function(index, elem){
                    delegated_to[index] = $.extend({},$.grep(__all_m,function(row){return row.JobAssignmentID == elem}));
                    delegated_to[index] = delegated_to[index][0];
                });
                all_matters = delegated_to;
                procDes_deleg({lvl : 1, matter : green_matters, type : "green",id:'#green__t',delegated : 'to'});
                procDes_deleg({lvl : 2, matter : orange_matters, type : "orange",id:'#orange__t',delegated : 'to'});
                procDes_deleg({lvl : 3, matter : red_matters,type :"red",id:'#red__t',delegated : 'to'});
                procDes_deleg({lvl : 4, matter : finished_matters, type : "blue",id:'#blue__t',delegated : 'to'});
                procDes_deleg({lvl : 5, matter : untouched_matters, type: "gray",id:'#gray__t',delegated : 'to'});
            }
            
            var by_flag = false;
            var _b_m = all_matters;
            for(var i = 0; i < d_by.length; i++){
                var _true = $.grep(_b_m, function (elem){
                   if(elem === undefined)
                        return false;
                   else
                    return elem.JobAssignmentID == d_by[i];
                });
                if(_true.length){
                    by_flag = true;
                    break;
                }
            }
            
            if(by_flag){
                $("#delegated_by_div").html($("#delegated_by_dashboard_tmpl").tmpl());
                $.each(d_by, function(index, elem){
                    delegated_by[index] = $.extend({},$.grep(__all_m,function(row){return row.JobAssignmentID == elem}));
                    delegated_by[index] = delegated_by[index][0];
                });
            
                all_matters = delegated_by;
                procDes_deleg({lvl : 1, matter : green_matters, type : "green",id:'#green__b',delegated : 'by'});
                procDes_deleg({lvl : 2, matter : orange_matters, type : "orange",id:'#orange__b',delegated : 'by'});
                procDes_deleg({lvl : 3, matter : red_matters,type :"red",id:'#red__b',delegated : 'by'});
                procDes_deleg({lvl : 4, matter : finished_matters, type : "blue",id:'#blue__b',delegated : 'by'});
                procDes_deleg({lvl : 5, matter : untouched_matters, type: "gray",id:'#gray__b',delegated : 'by'});
            }
        }
    }
</script>
@stop 
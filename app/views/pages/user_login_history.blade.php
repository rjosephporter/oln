@extends('template/default')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3><span class="glyphicon glyphicon-wrench"></span><?php echo 'System login history for' ?></h3>
        </div>
    </div>    
    <div class="row top-buffer">    
        <div class="col-xs-12">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="text-align:center">Logged In</th>
                        <th style="text-align:center">Logged Out</th>
                        <th style="text-align:center">IP</th>
                        <th style="text-align:center">Location</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($login_history as $login): ?>
                        <tr>
                            <td style="text-align:center"><?php echo date('F d h:i A',$login->login) ?></td>
                            <td style="text-align:center"><?php echo $login->logout != "0000-00-00 00:00:00" && $login->logged_in == "0" ? date('F d h:i A',$login->logout) : "STILL LOGGED IN" ?></td>
                            <td style="text-align:center"><?php echo $login->from_ip ?></td>
                            <td style="text-align:center">
                                <?php
                                    if($user->ip_address != ''){
                                        if(Helper::reserved_ip($login->from_ip) == false){
                                            $location = Helper::geoCheckIP($login->from_ip);
                                            echo $location['country'];
                                            echo $location['state'] != 'not found' ? ', '.$location['state'] : '';
                                            echo $location['town'] != 'not found' ? ', '.$location['town'] : '';
                                        } else {
                                            echo 'Office LAN';
                                        }
                                    } else {
                                        echo 'NA';
                                    }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>        
    </div>
</div>
@stop

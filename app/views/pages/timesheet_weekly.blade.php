@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">

<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h2 class="hidden-print"><i class="glyphicon glyphicon-time"></i><!-- <span class="label label-warning">TBD</span> --> Timesheet Weekly Report (All Handlers)</h2>

			<!-- BEGIN FILTER CONTROLS -->
			<div class="form-inline hidden-print" role="form" style="margin: 25px 0px 15px 0px;">
				<form>
					<div class="form-group">
						<label for="from_date">From</label>
						<input type="text" class="form-control" placeholder="Date" value="{{ date('F j, Y', strtotime($date['from'])) }}" disabled>
					</div>
					<div class="form-group"></div>
					<div class="form-group">
						<label for="to_date">To</label>
						<input type="text" class="form-control" placeholder="Date" value="{{ date('F j, Y', strtotime($date['to'])) }}" disabled>
					</div>
					<div class="form-group"></div>				
					<!-- <button type="submit" class="btn btn-success"><i class="fa fa-line-chart"></i> Generate Report</button> -->
					<button class="btn btn-success" id="btn-report-filter" data-toggle="modal" data-target="#report_filter"><i class="fa fa-filter"></i> Report Filter</button>
					
					<button class="btn btn-info pull-right" id="print-report"><i class="fa fa-print"></i> Print</button>&nbsp;&nbsp;					
				</form>
			</div>
			<!-- END FILTER CONTROLS -->

			<h2 class="text-center visible-print">Timesheet Report from <span id="text-from-date">{{ date('F j, Y', strtotime($date['from'])) }}</span> to <span id="text-to-date">{{ date('F j, Y', strtotime($date['to'])) }}</span></h2>

			<div role="tabpanel">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs hidden-print" role="tablist">
					<li role="presentation" class="active"><a href="#chart-view" aria-controls="chart-view" role="tab" data-toggle="tab">Chart View</a></li>
					<li role="presentation"><a href="#table-view" aria-controls="table-view" role="tab" data-toggle="tab">Table View</a></li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content" style="margin-top: 20px">
					<div role="tabpanel" class="tab-pane active" id="chart-view">
						<!-- BEGIN CHART -->
						<canvas id="timesheet-weekly-chart">

						</canvas>
						<!-- END CHART -->

						<!-- BEGIN OVERVIEW TABLE -->
						<table class="table table-bordered table-condensed">
							<thead>
								<tr>
									<td>&nbsp;</td>
									@foreach($handlers as $handler)
									<td><strong>{{ $handler }}</strong></td>
									@endforeach
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><strong>Billable&nbsp;Hours</strong></td>
									@foreach($graph1 as $g1)
									<td>{{ $g1 }}</td>
									@endforeach
								</tr>
								<tr>
									<td><strong>Recorded&nbsp;Hours</strong></td>
									@foreach($graph2 as $g2)
									<td>{{ $g2 }}</td>
									@endforeach
								</tr>
								<tr>
									<td><strong>Variance&nbsp;Hours</strong></td>
									@foreach($graph3 as $g3)
									<td>{{ $g3 }}</td>
									@endforeach
								</tr>					
							</tbody>
						</table>
						<!-- END OVERVIEW TABLE -->
					</div>
					<div role="tabpanel" class="tab-pane" id="table-view">
						<table class="table table-bordered table-condensed table-striped">
							<thead>
								<tr>
									<td><strong>Handlers</strong></td>
									<td><strong>Budget Billable Hours</strong></td>
									<td><strong>Actual Recorded Hours</strong></td>
									<td><strong>Actual Non-billable Hours</strong></td>
									<td><strong>Actual Billable Hours</strong></td>
									<td><strong>Variance Hours</strong></td>
								</tr>
							</thead>
							<tbody>
								@foreach($handlers as $ndx => $h)
								<tr>
									<td>{{ $h }} - {{ User::getFullName($h) }}</td>
									<td>{{ $handlers_data[$h]['budget_billable_hours'] }}</td>
									<td>{{ $graph2[$ndx] }}</td>
									<td>{{ $graph2[$ndx] - $graph1[$ndx] }}</td>
									<td>{{ $graph1[$ndx] }}</td>
									<td>{{ $graph3[$ndx] }}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td class="text-center"><strong>Total</strong></td>
									<td><strong>{{ 30 * count($handlers) }}</strong></td>
									<td><strong>{{ $graph2_total }}</strong></td>
									<td><strong>{{ $graph2_total - $graph1_total }}</strong></td>
									<td><strong>{{ $graph1_total }}</strong></td>
									<td><strong>{{ $graph3_total }}</strong></td>									
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<!-- BEGIN Modals -->
<div class="modal fade" id="report_filter" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form id="report_filter_form" role="form">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="myModalLabel">Report Filter</h4>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<!-- BEGIN FILTER CONTROLS -->
						<div class="form-inline hidden-print" role="form" style="margin: 25px 0px 15px 0px;">
								<div class="form-group">
									<label for="from_date">From <small>(click to change)</small></label>
									<input type="text" class="form-control" id="from_date" name="filter[from_date]" filter-for="timesheet" toggle-text="#text-from-date" placeholder="Date" value="{{ date('F j, Y', strtotime($date['from'])) }}" readonly>
								</div>
								<div class="form-group"></div>
								<div class="form-group">
									<label for="to_date">To</label>
									<input type="text" class="form-control" id="to_date" name="filter[to_date]" filter-for="timesheet" toggle-text="#text-to-date" placeholder="Date" value="{{ date('F j, Y', strtotime($date['to'])) }}" readonly>
								</div>
						</div>
						<!-- END FILTER CONTROLS -->
					</div>
					<div class="row">
						<table class="table table-bordered table-condensed table-striped">
							<thead>
								<tr>
									<th>Show</th>
									<th>Handler</th>
									<th>Budget Billable Hours</th>
								</tr>
							</thead>
							<tbody>
								@foreach($handlers_data as $handler => $data)
								<tr>
									<td class="text-center">
										<input type="checkbox" class="form-control" name="handlers[{{ $handler }}][show]" {{ isset($data['show']) ? 'checked' : '' }}>
									</td>
									<td>
										{{ $handler }}
									</td>
									<td>
										<input type="number" step="any" class="form-control input-sm" name="handlers[{{ $handler }}][budget_billable_hours]" value="{{ $data['budget_billable_hours'] }}">
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary btn-sm">Update</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- END Modals -->

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/jquery.weekpicker.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/ChartNew.js"></script>

<script type="text/javascript">
$('#from_date').weekpicker({
	firstDay: 1,
	maxDate: 0,
	dateFormat: "MM d, yy",
	showOtherMonths: true,
	startField: $('#from_date'),
	endField: $('#to_date'),
	onClose: function() {
		$('input[name*="filter"]').trigger('change');
	}
});

$('input[name*="filter"]').change(function(){
	console.log('changed');
	$($(this).attr('toggle-text')).text($(this).val());
});

$('#print-report').click(function(e) {
	e.preventDefault();
	window.print();
});

</script>

<script>
$('#btn-report-filter').click(function(e) {
	e.preventDefault();
});

var data = {
	labels : {{ json_encode($handlers) }},
	datasets : [
		{
            title: "Billable Hours",
            fillColor: "rgba(44, 62, 80, 0.5)",
            strokeColor: "rgba(44, 62, 80, 0.7)",
            data: {{ json_encode($graph1) }}
		},
		{
			title: "Recorded Hours",
			fillColor : "rgba(0, 128, 0, 0.5)",
			strokeColor : "rgba(0, 128, 0, 0.7)",
			data : {{ json_encode($graph2) }}
		},
		{
			title: "Variance Hours",
			fillColor : "rgba(255, 0, 0, 0.5)",
			strokeColor : "rgba(255, 0, 0, 0.7)",
			data : {{ json_encode($graph3) }}
		}		
	]
}

var options = { 
	responsive : true,
    animation : true,
    showTooltips: true,
    logarithmic : 'fuzzy',
    
    annotateDisplay: true,
    annotateLabel : "<%='<strong>' + v2 + '</strong>' + ' <br/> ' + v1 + ': ' + v3 + ''%>",

    legend: true,
    xAxisBottom: true,
    xAxisLabel: 'Handlers',
    yAxisLeft: true,
    yAxisLabel: 'Hours'
};

//Get the context of the canvas element we want to select
var ctx = document.getElementById("timesheet-weekly-chart").getContext("2d");
var myNewChart = new Chart(ctx).Bar(data,options);
</script>
@stop
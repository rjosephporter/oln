@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2>
				<i class="glyphicon glyphicon-time"></i> New Conference Room 
			</h2>
			<div class="modal-content" id="new_conference_room_log2">
				<form role="form" method="post">
					<div class="modal-body">
						<div class="row" id="new_conference_room_main_form">
						<!--
							<div class="col-md-4 form-group">
								<label for="new_conference_room_name">Conference Room Name</label>
								<input type="text" class="form-control" name="new_conference_room_name">
							</div>
							-->		
							
						</div>
						<div class="col-md-12">
							<button class="btn btn-info btn-sm" id="btn_add_item2">Add Item</button>
						</div>
					</div>
					<div class="modal-footer">
						<a href="{{ URL::to('/conference-room') }}" class="btn btn-default">Back</a>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div id="item-container-source" style="display:none">
	<div class="col-md-12 item-container">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<button type="button" class="close pull-right delete-item"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 row">
						<div class="col-md-12 form-group">
							<label for="new_conference_room_name">Conference Room Name</label>
							<input type="text" class="form-control" id="new_conference_room_name" name="new_conference_room_name[]" placeholder="" value="">
						</div>												
						<div class="col-md-12 form-group">
							<label for="new_conference_room_description">Description</label>
							<input type="text" class="form-control" id="new_conference_room_description" name="new_conference_room_description[]" placeholder="" value="">
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/conference_room.js"></script>
<script>
/* Overwritten variables from conference_room.js */
	report_types = [];
	new_conference_room_log_count = 0;

	var item_container = $('#item-container-source').clone().html();

	/* Add item */
    $('#btn_add_item2').bind('click touchstart', function(e){
        e.preventDefault();
        $('#new_conference_room_main_form').append(item_container).focus();
        
        new_conference_room_log_count++;
    });

    $(document).ready(function() {
    	$('#btn_add_item2').click();

    });

     /* Delete item */
    $(document).on('click touchstart', '.delete-item', function(e){
        e.preventDefault();
        if(new_conference_room_log_count > 1) {
            $(this).parent().parent().parent().remove();
            new_conference_room_log_count--;
        } else {
            toastr.error('You must have at least one item.');
        }
    });

 


    /* Save New conference */
    $('#new_conference_room_log2 form').submit(function(e){
        e.preventDefault();
        console.log($(this).serializeArray());

		if($('#new_conference_room_name').val() == '' || $('#new_conference_room_description').val() == ''){
            toastr.error('You must specify a conference room name/description.');
        } else{


        $.post('/conference-room/submitnewroom', $(this).serializeArray(), function(data){
            console.log(data);
            if(data.status == 'success') {
                toastr.success(data.msg);
	            var delay = 3000; //Your delay in milliseconds
	            setTimeout(function(){ window.location = '{{ Request::url() }}'; }, delay);                
            } else {
                toastr.error(data.msg);
            }
        });
    	}

    });  

    	
		
</script>
@stop
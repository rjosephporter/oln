@extends('template/default')
@section('content')
<?php
    $billable = 0;
    $billed = 0;
    $unbilled = 0;
    $date=date_create($matter_obj[0]->WorkDate);
?>
<h3>
    <div class="row">
        <div class="col-md-3">Matter No: {{$id}} - {{$matter_obj[0]->Description_A}}</div>
        <div class="col-md-3">Handler: {{$matter_obj[0]->Incharge}}</div>
        <div class="col-md-3">Designated Date: {{date_format($date,"F m, Y")}}</div>
        <div class="col-md-3">Client: {{$matter_obj[0]->CustomerID}}</div>
    </div>
</h3>
<div class="row">
    <div class="col-md-12">    
        <table class="table table-hover table-bordered cell-border" id="handler_modal_table">                        
            <thead>
                <tr>
                  <th colspan="3" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                  <th class="h_billable" style="text-align: right;"></th>
                  <th class="h_billed" style="text-align: right;"></th>
                  <th class="h_unbilled" style="text-align: right;"></th>
              </tr>
              <tr>
                  <th>Work Date</th>
                  <th>Worked By</th>
                  <th>Work Description</th>
                  <th>Billable Amount</th>
                  <th>Billed Amount</th>
                  <th>Unbilled Amount</th>
              </tr>
          </thead>
          <tbody>
             @foreach($matter_obj as $matter)
             <?php 
                $billable += $matter->PlanAmount;
                $billed += $matter->Amount;
                $unbilled += $matter->unbilled_amount;
                $date=date_create($matter->WorkDate);
             ?>
             <tr>
                 <td>{{date_format($date,"F m, Y")}}</td>
                 <td>{{$matter->EmployeeID}}</td>
                 <td>{{$matter->Description}}</td>
                 <td>${{ number_format($matter->PlanAmount, 2, '.', ',') }}</td>
                 <td>${{ number_format($matter->Amount, 2, '.', ',') }}</td>
                 <td>${{ number_format($matter->unbilled_amount, 2, '.', ',') }}</td>
             </tr>
              @endforeach
          </tbody>
          <tfoot>
              <tr>
                  <th colspan="3" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                  <th style="text-align: right;">${{ number_format($billable, 2, '.', ',') }}</th>
                  <th style="text-align: right;">${{ number_format($billed, 2, '.', ',') }}</th>
                  <th style="text-align: right;">${{ number_format($unbilled, 2, '.', ',') }}</th>
                  <th>&nbsp;</th>
              </tr>
          </tfoot>
        </table>
    </div>
</div>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript">
    $(".h_billable").html("$"+parseFloat({{$billable}}).formatMoney(2));
    $(".h_billed").html("$"+parseFloat({{$billed}}).formatMoney(2));
    $(".h_unbilled").html("$"+parseFloat({{$unbilled}}).formatMoney(2));
</script>

@stop 
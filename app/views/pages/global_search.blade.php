@extends('template/default')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Global Search <small>Results for "{{ Input::get('search-keyword') }}"</small></h1>
			</div>

			<div class="row">				
				<div class="col-md-12">
					<h3>Matters</h3>
					@if(count($results['matter']) > 0)
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Matter ID</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							@foreach($results['matter'] as $matter)
							<tr>
								<td>{{ HelperLibrary::highlightKeyword($matter->JobAssignmentID, Input::get('search-keyword')) }}</td>
								<td>{{ HelperLibrary::highlightKeyword($matter->Description_A, Input::get('search-keyword')) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@else
					<pre>No matters found.</pre>
					@endif
				</div>
			</div>

			<div class="row">				
				<div class="col-md-12">
					<h3>Clients</h3>
					@if(count($results['client']) > 0)
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Client ID</th>
								<th>Name</th>
							</tr>
						</thead>
						<tbody>
							@foreach($results['client'] as $client)
							<tr>
								<td>{{ HelperLibrary::highlightKeyword($client->CustomerID, Input::get('search-keyword')) }}</td>
								<td>{{ HelperLibrary::highlightKeyword($client->CompanyName_A1, Input::get('search-keyword')) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@else
					<pre>No clients found.</pre>
					@endif
				</div>
			</div>

			<div class="row">				
				<div class="col-md-12">
					<h3>Employees</h3>
					@if(count($results['employee']) > 0)
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Employee ID</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Nickname</th>
							</tr>
						</thead>
						<tbody>
							@foreach($results['employee'] as $emp)
							<tr>
								<td>{{ HelperLibrary::highlightKeyword($emp->EmployeeID, Input::get('search-keyword')) }}</td>
								<td>{{ HelperLibrary::highlightKeyword($emp->first_name, Input::get('search-keyword')) }}</td>
								<td>{{ HelperLibrary::highlightKeyword($emp->last_name, Input::get('search-keyword')) }}</td>
								<td>{{ HelperLibrary::highlightKeyword($emp->NickName, Input::get('search-keyword')) }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@else
					<pre>No employees found.</pre>
					@endif
				</div>
			</div>

		</div>
	</div>
</div>
@stop
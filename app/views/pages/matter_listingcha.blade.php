@extends('template/default')
@section('content')

@if($obj['user'] == 'admin' || $obj['user'] == $obj['handler'])

<?php
    $billable = 0;
    $billed = 0;
    $unbilled = 0;
?>

<style>
  .table-hover>tbody>tr:hover>td, .table-hover>tbody>tr:hover>th {
    background-color: skyblue;
    color: black;
}
.my_borders{
    margin-top: 5px; margin-bottom: 5px; margin-right: 5px;
}
	.alert-box {
		color:#555;
		border-radius:10px;
		font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
		padding:10px 36px;
		margin:10px;
	}
	.alert-box span {
		font-weight:bold;
		text-transform:uppercase;
	}
	.error {
		background:#ffecec url('../../images/error.png') no-repeat 10px 50%;
		border:1px solid #f5aca6;
	}
	.success {
		background:#e9ffd9 url('../../images/success.png') no-repeat 10px 50%;
		border:1px solid #a6ca8a;
	}
	.warning {
		background:#fff8c4 url('../../images/warning.png') no-repeat 10px 50%;
		border:1px solid #f2c779;
	}
	.notice {
		background:#e3f7fc url('/assets/thisapp/images/app/notice-icon.png') no-repeat 10px 50%;
		border:1px solid #8ed9f6;
	}
</style>
<link rel="stylesheet" href="/assets/DataTables-1.10.0/extensions/TableTools/css/dataTables.tableTools.css">

<input type="hidden" id="billable" value="0">
<input type="hidden" id="billed" value="0">
<input type="hidden" id="unbilled" value="0">

    <div class="row">
        <div class="col-md-12">
            <div style="margin-left: 10px; margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-8"><span class="h3" style="padding-left: 15px;">{{ ucfirst($obj['type']) }} Matters {{$obj['client'] != 'all' ? ' for Client: '.$obj['matters'][0]->client_name : ''}}{{$obj['handler'] != 'all' && $obj['client'] == 'all'? ' for Handler: '.$obj['handler'] : ''}}</span></div>
                    <div class="col-md-4" style="text-align: right;"> 
                        @if($obj['handler'] != 'all' && $obj['user'] == 'admin' && $obj['reg_user'] != $obj['handler'])
                        <a href="#" onclick="send_all()"><button class="btn btn-md btn-info">Email {{ ucfirst($obj['type']) }} Matters to {{ $obj['handler'] }}</button></a>
                        @endif
                        <a href="/matters/new" target="_blank"><button class="btn btn-md btn-info">Create New Matter</button></a>
                    </div>
                </div>

                <div style="margin-left: 15px; margin-top: 15px;">
                    <span><a style="color: black; font-size: 13px;" href="/matters/view/7/{{$obj['client']}}/{{$obj['handler']}}"><img src="/assets/thisapp/images/matters/white.png" width="20"/>&nbsp;View All Matters</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/1/{{$obj['client']}}/{{$obj['handler']}}"><img src="/assets/thisapp/images/matters/green.png" width="20"/>&nbsp;Active Last 4 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/2/{{$obj['client']}}/{{$obj['handler']}}" style="color: orange;"><img src="/assets/thisapp/images/matters/orange.png" width="20"/>&nbsp;Last Activity Between 4 and 8 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/3/{{$obj['client']}}/{{$obj['handler']}}" style="color: red;"><img src="/assets/thisapp/images/matters/red.png" width="20"/>&nbsp;Last Activity Beyond 8 Weeks</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/5/{{$obj['client']}}/{{$obj['handler']}}" style="color: gray;"><img src="/assets/thisapp/images/matters/grey.png" width="20"/>&nbsp;Untouched/No Activity</a></span>
                    <span style="margin-left: 15px; font-size: 13px;"><a href="/matters/view/4/{{$obj['client']}}/{{$obj['handler']}}" style="color: blue;"><img src="/assets/thisapp/images/matters/blue.png" width="20"/>&nbsp;Completed Matters</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="alert-box notice" role="alert" style="text-align: center; vertical-align: middle;" id="my_info_preloader">
        <h4>. . . Processing and Loading Data, Please Wait . . .</h4>
        <img src="/assets/thisapp/images/ajax-loader-bar.gif">
    </div>
    <div class="alert-box notice" role="alert" style="text-align: center; vertical-align: middle; display: none;" id="my_email_preloader">
        <h4>. . . {{ ucfirst($obj['type']) }} Matters Listing is now being sent to {{ $obj['handler'] }}, Please Wait . . .</h4>
        <img src="/assets/thisapp/images/ajax-loader-bar.gif">
    </div>
    <div class="row">
        <div class="col-md-12">    
            <table class="table table-hover table-bordered cell-border" id="handler_modal_table" style="display: none;">                        
                <thead>
                    <tr>
                      <th colspan="5" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                      <th class="h_billable" style="text-align: right;"></th>
                      <th class="h_billed" style="text-align: right;"></th>
                      <th class="h_unbilled" style="text-align: right;"></th>
                      <th>&nbsp;</th>
                  </tr>
                  <tr>
                      <th>&nbsp;</th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Client Name">Client</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter Name">Reference</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Matter ID">Matter</div></th>
                      <th><div style="width:100%;" data-toggle="tooltip" data-placement="top" title="Click to Sort by Handler">Handler</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billable Amount" style="text-align: right; width:100%;">Billable Amount</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Billed Amount" style="text-align: right; width:100%">Billed Amount</div></th>
                      <th><div data-toggle="tooltip" data-placement="top" title="Click to Sort by Unbilled Amount" style="text-align: right; width: 100%;">Unbilled Amount</div></th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tfoot>
                  <tr>
                      <th colspan="5" style="text-align: right; margin-right: 15px;">Totals Summary</th>
                      <th style="text-align: right;">${{ number_format($billable, 2, '.', ',') }}</th>
                      <th style="text-align: right;">${{ number_format($billed, 2, '.', ',') }}</th>
                      <th style="text-align: right;">${{ number_format($unbilled, 2, '.', ',') }}</th>
                      <th>&nbsp;</th>
                  </tr>
              </tfoot>
              <tbody>
                 @foreach($obj['matters'] as $matter)
                 <?php 
                    $billable += $matter->billable_amount;
                    $billed += $matter->billed_amount;
                    $unbilled += $matter->unbilled;
                 ?>
                 <tr class="mclss" style="color: {{ $matter->matter_type == 'blue' ? 'white' : 'black' }}; background-color: {{ $matter->matter_type == 'blue' ? "#0075b0"  : ($matter->matter_type == 'green' ? "rgba(0, 128, 0, 0.5)" : ($matter->matter_type == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($matter->matter_type == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
                    <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: center; cursor: pointer;"><img src="/assets/thisapp/images/matters/{{ $matter->matter_type == "gray" ? "grey" : $matter->matter_type }}.png" width="25"></td>
                    <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: left; cursor: pointer;">{{$matter->client_name}}</td>
                    <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: left; cursor: pointer;">{{$matter->Description_A}}</td>
                    <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: center; cursor: pointer;">{{$matter->JobAssignmentID}}</td>
                    <td onclick="show({{$matter->JobAssignmentID}})" style="text-align: center; cursor: pointer;">{{$matter->Incharge}}</td>
                    <td style='text-align: right;'>${{ number_format($matter->billable_amount, 2, '.', ',') }}</td>
                    <td style='text-align: right;'>${{ number_format($matter->billed_amount, 2, '.', ',') }}</td>
                    <td style='text-align: right;'>${{ number_format($matter->unbilled, 2, '.', ',') }}</td>
                    <td valign="middle" style="text-align: center; cursor: pointer; vertical-align: middle; margin: auto;">
                        @if($matter->matter_type == 'blue' && $obj['role'] == 'others' && $matter->CompleteStatus == 0)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Add Docket/Box No. To Complete Legally Closing Matter: {{$matter->JobAssignmentID}}"><button class="btn btn-xs btn-danger my_borders" onclick="_closeMatter({{$matter->JobAssignmentID}},1)"><i class="glyphicon glyphicon-plus"></i></button></div>
                        @endif
                        @if($matter->matter_type == 'red' && $obj['role'] == 'accounting' && $matter->CompleteStatus == 0 && $matter->Completed == 1)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Matter: {{$matter->JobAssignmentID}} needs to be Legally Closed Completely, Please contact {{$matter->Incharge}}"><button class="btn btn-xs btn-info my_borders"><i class="glyphicon glyphicon-plus"></i></button></div>
                        @endif
                        @if($obj['user'] == "admin" || $matter->Incharge == $obj['reg_user'])
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="View/Edit Matter: {{$matter->JobAssignmentID}}"><a href="/matters/update/{{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info my_borders"><i class="glyphicon glyphicon-edit"></i></button></a></div>
                        @endif
                        @if($obj['user'] == "admin" && $matter->Incharge != $obj['reg_user'] && floor(floatval($matter->unbilled)) > 0)
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Send Reminder to {{$matter->Incharge}}"><button class="btn btn-xs btn-info my_borders" onclick="dis({{$matter->JobAssignmentID}})"><i class="fa fa-envelope"></i></button></div>
                        @endif
                        @if($matter->matter_type != 'blue')
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Create Timesheet for {{$matter->JobAssignmentID}}"><a href="/timesheet/new?matter_id={{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info my_borders"><i class="glyphicon glyphicon-time"></i></button></a></div>
                            <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Create Invoice for {{$matter->JobAssignmentID}}"><a href="/invoice/create-new?matter_id={{$matter->JobAssignmentID}}" target="_blank"><button class="btn btn-xs btn-info btn-send-reminder my_borders"><i class="glyphicon glyphicon-th-list"></i></button></a></div>
                        @endif
                        @if($matter->matter_type == 'red' && $obj['role'] == 'others' && $matter->CompleteStatus == 0)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Legally Close Matter: {{$matter->JobAssignmentID}}" onclick="_closeMatter({{$matter->JobAssignmentID}},1)"><button class="btn btn-xs btn-danger my_borders"><i class="glyphicon glyphicon-remove"></i></button></div>
                        @endif
                        @if($matter->matter_type == 'red' && $obj['role'] == 'accounting' && $matter->CompleteStatus == 1)
                        <div style="float: left;" data-toggle="tooltip" data-placement="top" title="Accounting/Completely Close Matter: {{$matter->JobAssignmentID}}" onclick="_closeMatter({{$matter->JobAssignmentID}},2)"><button class="btn btn-xs btn-danger my_borders"><i class="glyphicon glyphicon-remove"></i></button></div>
                        @endif
                    </td>
                </tr>
                  @endforeach
              </tbody>
            </table>
        </div>
    </div>

<!-- Modals -->
<div class="modal fade" id="openmatter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Matter ID: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
          <div>
                <h5><i class="fa fa-info-circle text-info"></i> Send reminder to handler?</h5>
                <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
                <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">
                <div class="form-group">
                  <label for="reminder_recipient">To</label>
                  <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Handler's Email" required readonly>
                </div>
                <div style="text-align: center; vertical-align: middle;" id="rem_preloader"></div>
                <div class="form-group">
                  <label for="reminder_message">Message</label>
                  <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
                  </textarea>
                </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="complete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">Legally Close Matter <span class="matter_for_completion"></span></h4>
      </div>
      <form id="matter_completion_form" role="form" method="post" class="my_confirm_forms">
      <div class="modal-body confirm_body_modal"style="text-align: center; vertical-align: middle;">
        <h3 style="color: red;">This action will mark Matter <span class="matter_for_completion"></span> as LEGALLY CLOSED and COMPLETED. Do you want to proceed?</h3>

        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-file"></i> Docket/Box No.:</div>
            <input class="form-control" type="text" name="box_number_temp" value="" placeholder=""/>
          </div>
        </div>

      </div>
          <input type="hidden" value="" name="box_number" placeholder=""/>
          <input type="hidden" value="" name="matter_id" class="matter_for_completion"/>
          <input type="hidden" value="1" name="CloseStatus"/>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="final_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforfinalConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforfinalConfirmComplete">Accounting/Completely Close Matter <span class="matter_for_completion"></span></h4>
      </div>
      <form id="matter_completion_form" role="form" method="post" class="my_confirm_forms">
      <div class="modal-body confirm_body_modal" style="text-align: center; vertical-align: middle;">
        <h3 style="color: red;">This action will mark Matter <span class="matter_for_completion"></span> as Accounting and Completely Closed. Do you want to proceed?</h3>

        <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-file"></i> Docket/Box No.:</div>
            <input class="form-control" type="text" name="box_number_temp" value="" placeholder=""/>
          </div>
        </div>

      </div>
          <input type="hidden" value="" name="box_number" placeholder=""/>
          <input type="hidden" value="" name="matter_id" class="matter_for_completion"/>
          <input type="hidden" value="2" name="CloseStatus"/>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- /Modals -->
<link type="text/css" href="/assets/thisapp/css/matters.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/timeline.css" rel="stylesheet">
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" language="JavaScript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script src="/assets/DataTables-1.10.0/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript">
    $(function(){
        $('#handler_modal_table').dataTable({
           pagingType: "full", 
           pageLength : 30,
           orderClasses : true, 
           bLengthChange : false,
           bFilter : true,
           order : [ {{ $obj['billing'] }}, 'desc' ],
           tableTools: {
                "aButtons": "print",
                "sSwfPath": "/assets/DataTables-1.10.0/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
            }
	});
        $(".h_billable").html("$"+parseFloat({{$billable}}).formatMoney(2));
        $(".h_billed").html("$"+parseFloat({{$billed}}).formatMoney(2));
        $(".h_unbilled").html("$"+parseFloat({{$unbilled}}).formatMoney(2));
        $("#my_info_preloader").hide();
        $("#handler_modal_table").show();
    });
    function show(id){
        pM(id);
    }
    function _closeMatter(matter,mark){
        $(".matter_for_completion").html(matter);
        $(".matter_for_completion").val(matter);
        if(mark == 1)
            $("#complete_confirmation").modal().show();
        else{
            $.get('/matters/docketno/' + matter,function(data){
                console.log(data);
                $('input[name="box_number_temp"]').val(data.res[0].box);
                $("#final_confirmation").modal().show();
            });
        }
    }
    function dis(_id){
        $("#rem_preloader").html("");
        var matter = $.grep(all_matters,function(elem){
            return elem.JobAssignmentID == _id;
        });
        if(matter.length > 0){
            $.get('/accounting/reminder-info?reminder_type=matter&matter_id=' + matter[0].JobAssignmentID + '&total_unbilled_amount=' + matter[0].unbilled,function(data){
                $("#invoice-reminder-batch-number").html(matter[0].JobAssignmentID);
                $("#reminder_recipient").val(matter[0].handler_email);
                $("#reminder_message").html(data.message);
                $("#invoice-reminder-modal").modal().show(); 
            },'json');
        }
    }
    
    function send_all(){
        $("#my_email_preloader").show();
        $.get('/matters/sendmattersinfo?handler={{$obj["handler"]}}&type={{$obj["type"]}}',function(info){
           console.log(info);
           $("#my_email_preloader").hide();
        },'json');
    }
    $('.my_confirm_forms').submit(function(data){
        $('input[name="box_number"]').val($('input[name="box_number_temp"]').val());
        $(".confirm_body_modal").html($("#modal_preloader_confirm").tmpl()); 
    });
    $('#invoice-reminder-form').submit(function(e) {
        $("#rem_preloader").html($('#sending_preloader').tmpl());
        e.preventDefault();
        var post_data = $(this).serializeObject();
        post_data.no_log = 1;
        $.post('/accounting/send-reminder', post_data, function(data) {
            $('#invoice-reminder-modal').modal('hide');
        });
    });
</script>
<script type="text/javascript" src="/assets/thisapp/js/modal_matter_.js"></script>
<script id="modal_preloader_confirm" type="text/x-jquery-tmpl">
    <h4>. . . Closing Matter, Please Wait . . .</h4>
    <img src="/assets/thisapp/images/ajax-loader-bar.gif">
</script>
<script id="sending_preloader" type="text/x-jquery-tmpl">
    <h4>. . . Sending . . .</h4>
    <img src="/assets/thisapp/images/ajax-loader-bar.gif">
</script>

@else

<div class="container" style="text-align: center; color: red; width: 100%; padding-bottom: 30px;"><h1>You Don't Have Enough Privilege to View this Page. </h1></div>

@endif

@stop
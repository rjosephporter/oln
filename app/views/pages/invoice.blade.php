@extends('template/default')
@section('content')
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h1 id="page-title">
        <!-- <a href="/invoice/create-new" class="btn btn-info"><i class="glyphicon glyphicon-th-list"></i> Create</a>  -->
        Invoice List <span id="matter_id_text"></span>
                        <div id="help" class="pull-right"></div>    
                        </h1>
			<div id="breadcrumb" style="display:none">
				<button id="btn-back" class="btn btn-info btn-xs hidden-print"><i class="glyphicon glyphicon-arrow-left"></i> {{ Lang::get('lang.invoice.Back to Invoice List') }}</button>
			</div>
			<div class="row" id="daterange-wrapper" style="margin-bottom: 15px">
				<div class="col-md-12">
					<form id="daterange-form" class="form-inline" role="form" method="get">
						<div class="form-group">
							<label for="from_date">{{ Lang::get('lang.invoice.From') }}</label>
							<input type="text" class="form-control" id="from_date" placeholder="Choose Date">
						</div>
						<div class="form-group">
            {{ Lang::get('lang.invoice.To') }}
							<input type="text" class="form-control" id="to_date" placeholder="Choose Date">
						</div>
						<div class="form-group">
							<label for="status">{{ Lang::get('lang.invoice.Status') }}</label>
							<select class="form-control" id="status" placeholder="Choose Status">
								<option value="0">All</option>
								<option value="1" selected>{{ Lang::get('lang.invoice.Open Invoice') }}</option>
								<!-- <option value="2">Open Matter</option> -->
								<option value="3">{{ Lang::get('lang.invoice.Closed Invoice') }}</option>
								<!-- <option value="4">Closed Matter</option> -->
                <option value="5">{{ Lang::get('lang.invoice.Draft Invoice') }}</option>
							</select>
						</div>
						<div class="form-group" style="display:none">
							<label for="status">{{ Lang::get('lang.invoice.Matter ID') }}</label>
							<input type="text" class="form-control" id="matter_id" placeholder="Choose Matter ID">
						</div>
						<button type="submit" class="btn btn-default">{{ Lang::get('lang.invoice.Apply') }}</button>
					</form>
				</div>

				<div class="col-md-12" id="matter-info-wrapper" style="margin-top: 15px; display:none">
          <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 pull-left"><button class="btn btn-info btn-sm" id="btn-back-related"><i class="glyphicon glyphicon-arrow-left"></i> {{ Lang::get('lang.invoice.Back to Invoice List') }}</button></div>
            <div class="col-md-6 col-sm-12 col-xs-12 pull-right text-right">
              <strong>Total Amount:</strong> <span class="label label-info" id="matter-total-invoice">$9,999,999.99</span>&nbsp;&nbsp;
              <strong>Total Balance:</strong> <span class="label label-warning" id="matter-total-balance">$9,999,999.99</span>  
            </div>
          </div>
				</div>
			</div>

			<div id="invoice-wrapper">
            {{ 
                Datatable::table()
                    ->addColumn('Source', 'Invoice ID', 'Ref. No.', 'Inv. No.', 'Batch No.','Client ID', 'Client Name', 'Invoice Date', 'Created By', 'Date Created', 'Total Amount', 'Matter ID', 'Color Code', 'Total Matter Amount', 'Invoice Balance', 'Matter Balance', 'Matter ID', 'Action')
                    //->setOptions('sPaginationType','simple')
                    ->setClass('table table-bordered cell-border')
                    //->setUrl('/invoice/dt-invoice')
                    ->noScript()
                    ->render() 
            }}
        	</div>
        	<div id="invoicedetail-wrapper" style="display:none">
    				<div class="col-xs-12">
    		    		<div class="invoice-title">
    		    			<h2>Invoice</h2><h3 class="pull-right"># <span id="invoice_detail_number"></span></h3>
    		    		</div>
    		    		<hr>					
    		    		<div class="row">
    		    			<div class="col-xs-6">
    		    				<address>
    		    				<strong>Client:</strong><br>
    		    					<span id="invoice_detail_client"></span><br/><br/>
                    <strong>Matter:</strong><br>
                      <span id="invoice_detail_matter"></span><br/><br/>
                    <strong>Handler:</strong><br>
                      <span id="invoice_detail_handler"></span><br/><br/>
    		    				</address>
    		    			</div>
    		    			<div class="col-xs-6 text-right">
    		    				<address>
    		    					<strong>Invoice Date:</strong><br>
    		    					<span id="invoice_detail_date"></span><br><br>
    		    				</address>
    		    			</div>
    		    		</div>
    		    	</div>					
	            {{ 
	                Datatable::table()
	                    ->addColumn('Reference ID','Item No.','Description','Amount')
	                    ->setClass('table table-bordered table-striped cell-border')
	                    ->noScript()
	                    ->render() 
	            }}

              <div class="col-xs-12">
                <div class="row">
                  <h4>Disbursements</h4>
                  <table id="disbursement-table" class="table table-bordered table-striped cell-border">
                    <col width="14%">
                    <col width="67%">
                    <col width="19%">
                    <thead>
                      <tr>
                        <th>Item No.</th>
                        <th>Description</th>
                        <th>Amount</th>
                      </tr>
                    </thead>
                    <tbody id="disbursement-items">
                      @foreach(range(1,5) as $i)
                      <tr>
                        <td>{{ $i }}</td>
                        <td>Description {{ $i }}</td>
                        <td>${{ $i * 2 }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="2" class="text-right"><strong>Total:</strong></td>
                        <td><strong><span id="disbursement-total">$9,999.99</span></strong></td>
                      </tr>
                      <tr>
                        <td colspan="2" class="text-right"><strong>Total Costs and Disbursements:</strong></td>
                        <td><strong class="double-underline"><span id="cost-disb-total">$9,999.99</span></strong></td>
                      </tr>                      
                    </tfroot>
                  </table>                 
                </div>
              </div>
              
        	</div>
		</div>
	</div>
</div>

<!-- Modals -->
<div class="modal fade" id="related-invoice-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Invoices for Matter <span id="related-invoice-matter-id">?????</span></h4>
      </div>
      <div class="modal-body">
        
        <table class="table table-bordered">
          <thead>
            <th>Invoice ID</th>
            <th>Client</th>
            <th>Invoice Date</th>
            <th>Total Amount</th>
            <th>Invoice Balance</th>
            <th>Action</th>
          </thead>
          <tbody>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>
            <tr>
              <td>20140416143547503</td>
              <td>James Lee Ramsey</td>
              <td>September 08, 2014</td>
              <td>$9,999,999.99</td>
              <td>$9,999,999.99</td>
              <td>
              	<button class="btn btn-warning btn-xs"><i class="fa fa-envelope"></i></button>
              </td>
            </tr>                                                
          </tbody>
        </table>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="invoice-reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Invoice: <span id="invoice-reminder-batch-number"></span></h4>
      </div>
      <form id="invoice-reminder-form" role="form" method="post">
      <div class="modal-body">
        
        <h5><i class="fa fa-info-circle text-info"></i> Send reminder to client?</h5>

        <!-- <input type="hidden" id="reminder_number" name="reminder_number"> -->
        <input type="hidden" id="reminder_batch_number" name="reminder_batch_number">
        <input type="hidden" id="reminder_recipient_email" name="reminder_recipient_email">
        <input type="hidden" id="no_log" name="no_log" value="1">

        <div class="form-group">
          <label for="reminder_recipient">To</label>
          <input type="email" class="form-control" id="reminder_recipient" name="reminder_recipient" placeholder="Client's Email" required>
        </div>

        <div class="form-group">
          <label for="reminder_message">Message</label>
          <textarea class="form-control" rows="10" id="reminder_message" name="reminder_message" placeholder="Message" required>
          </textarea>
        </div>
          
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-info">Yes</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- /Modals -->
<script type="text/x-template" id="help_items">
<div class="dropdown">
    <button class="btn btn-info btn-sm dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
        {{ Lang::get('lang.invoice.Help') }}
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
        <li role="presentation"><a role="menuitem" tabindex="-1" href="/assets/thisapp/help_pages/how-to-print-an-invoice.html">How to print an invoice.</a></li>
    </ul>
</div>
</script>
<script>
    $(document).ready(function () {
        $('#help').append($($('#help_items').html()));
        $('#help').on('click','a[role=menuitem]',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            window.open(url);
        })
    })
</script>    

<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/invoice.js"></script>
@stop
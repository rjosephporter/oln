@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/login.css" rel="stylesheet">
<div class="container">
    <div class="row" style="height:100%">
        <div class="col-xs-12">
            <div class="modal show">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-body">
                        @if(Session::has('message.error.invalid_login'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          {{ Session::get('message.error.invalid_login') }}
                        </div>
                        @endif
                        <form class="form-horizontal" method="post">
                          <h3>{{ Lang::get('lang.login.Please sign in to OLN Asset Manager') }}</h3>
                          <fieldset>
                            <div class="form-group">
                              <label for="inputUsername" class="col-lg-4 control-label">{{ Lang::get('lang.login.Username') }}</label>
                              <div class="col-lg-6">
                                <!-- <input type="text" class="form-control" name="EmployeeID" value=""> -->
                                <select class="form-control" name="EmployeeID">
                                    <option></option>
                                  @foreach($users as $employee_id => $text)
                                    <option value="{{ $employee_id }}">{{ $text }}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputEmail" class="col-lg-4 control-label">{{ Lang::get('lang.login.Password') }}</label>
                              <div class="col-lg-6">
                                <input type="password" class="form-control" name="password" value="">
                              </div>
                            </div>
                            <div class="col-lg-8 col-sm-offset-4">
                                <button class="btn btn-info"><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;&nbsp;{{ Lang::get('lang.login.Sign-in') }}</button>
                            </div>
                          </fieldset>
                        </form>
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>
</div>

<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script>

function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('document').ready(function(){
    $('.modal').each(centerModal);
    $(window).on("resize", function () {
        $('.modal:visible').each(centerModal);
    });
    $('select').selectize();
});
</script>
@stop
@extends('template/default')
@section('content')
<link type="text/css" href="/assets/thisapp/css/clients.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<div class="container">
    <div class="panel panel-default">
      <div class="panel-heading">
          <div class="row">
              <div class="col-md-12">
                    <div class="form-inline form-group" style="float: left;">
                        <h3 class="panel-title"><img src="/assets/thisapp/images/clients/active.png" height="30px" style="cursor: pointer;" onclick="active(1)"/> <span style="color: green; cursor: pointer;" onclick="active(1)">{{ Lang::get('lang.client.Active clients') }}</span> &nbsp;&nbsp;&nbsp;<img src="/assets/thisapp/images/clients/inactive.png" height="30px" style="cursor: pointer;" onclick="active(0)"/> <span style="color: gray; cursor: pointer;" onclick="active(0)"/> {{ Lang::get('lang.client.Inactive Clients') }}</span></h3>
  </div>
          
<div class="form-inline form-group" style="float: right;">


                   <input type="text" class="form-control" name="search_key" id="search_key" style="width: 450px;" placeholder="Search Clients"/>
                  <button class="btn btn-success" onclick="process_search()">{{ Lang::get('lang.client.Find') }}</button>
  </div>
              </div>
          </div>

      </div>
      <div class="panel-body">
          <div class="row clients">
     
        <div class="col-sm-3 col-xs-4 col-md-2">
            <div class="well" style="height: 254px; overflow-y: hidden;">
                <a href="/clients/new"target="_blank"><span class="glyphicon glyphicon-plus very-large-glyph text-success"></span><h5>{{ Lang::get('lang.client.Create new client') }}</h5></a>
            </div>
        </div>
        <div id="active_client_area">
            <div style="width: 1005px; height: 1600px; text-align: center; vertical-align: middle;"><img src="/assets/thisapp/images/ajax-loader.gif"/></div>
        </div>
        <ul class="pager">
            <li><a href="#active_client_area" class="client_prev" id="active_client_prev" name="0" onclick="client_prev(this)">{{ Lang::get('lang.client.Previous') }}</a></li>
            <li><a href="#active_client_area" class="client_next" id="active_client_next" name="35" onclick="client_next(this)">{{ Lang::get('lang.client.Next') }}</a></li>
        </ul>
    </div>
      </div>
    </div>

</div>
<div class="modal_con"></div>
<link type="text/css" href="/assets/DataTables-1.10.0/media/css/dataTables.bootstrap.css" rel="stylesheet">
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/DataTables-1.10.0/media/js/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/jquery.tmpl.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/utils.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/clients.js"></script>

<script id="row_tmpl" type="text/x-jquery-tmpl">
    <option value="${CustomerID}">${CustomerID} - ${CompanyName_A1} - ${Incharge}</option>
</script>

@stop
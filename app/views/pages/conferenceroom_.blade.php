@extends('template/default')
@section('content')
<link type="text/css" href="/assets/selectize/css/selectize.css" rel="stylesheet">
<link type="text/css" href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/dashboard.css" rel="stylesheet">
<link type="text/css" href="/assets/thisapp/css/floorplan.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/assets/jquery-ui-1.10.4/css/jquery-ui-timepicker-addon.css">


<style>
.conference1 {
    padding: 0;
}

#calendar2{
   padding-top:30px;
    width: 230px;
    height:900px;
    margin: 0 auto;
    font-size: 10px;

}
#calendar {
    padding-top:30px;
    width: 430px;
    height:500px;
    margin: 0 auto;
    font-size: 10px;
}
.fc-header-title h2 {
    font-size: .9em;
    white-space: normal !important;
}

.fc-event-time,
.fc-event-title {
    padding: 0 1px;
    float: left;
    clear: none;
    margin-right: 10px;
}

.fc-view-month .fc-event {
    width: 100%; 
    height: 50px;
    position: absolute;
    bottom:0;
     overflow: hidden;
     font-size: 10;
    }
/*.fc-view-month .fc-event, .fc-view-agendaWeek .fc-event {
    font-size: 10;
    overflow: hidden;
    height: 2px;
}
.fc-view-agendaWeek .fc-event-vert {
    font-size: 10;
    overflow: hidden;
    width: 2px !important;
}
.fc-agenda-axis {
    width: 20px !important;
    font-size: 1em;
}

.fc-button-content {
    padding: 0;
}*/
</style>
<div class="container-fluid">
  
  @if(Session::has('message.success.send_reminder')) 
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        {{ Session::get('message.success.send_reminder') }}
      </div>      
    </div>
  </div>
  @endif  

    <!-- Floorplan V2 -->
    <div class="row top-buffer">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="height:52px">
                    <h3 class="panel-title">The Office (3rd & 5th floors)</h3>
                    <form method="get"><div id="new_conference_room_reservation_main_form" style="margin-top:-25px;margin-left:45px;"><center><table><tr><td>
                      <div class="col-sm-12"><label for="new_reservation_date">Date  <small>(click to select date)</small></label></div></td><td></td><td> 
                      <div class="col-sm-10"><input type="text" class="form-control" id="datetimepicker2" name="new_reservation_date[]"  readonly required></div></td>
                      <td colspan='2'><div class="col-sm-2"><button type="button" class="btn btn-info btn-sm pull-right btn-primary_reservation" id="today">Today</button></div></td></tr></table></center></div>
                      </form>
                </div>
                <div class="col-md-12 form-group"></div>
                <!--<form role="form" method="get">
                <div class="modal-body">
                      <div class="row" id="new_conference_room_reservation_main_form">
                          <div class="col-md-3 form-group">
                          <label for="new_reservation_date">Date  <small>(click to select date)</small></label>
                            <input type="text" class="form-control" id="datetimepicker2" name="new_reservation_date[]"  readonly required>
                            
                         </div>
                          <div class="col-md-3 form-group">
                            
                             <button type="button" class="btn btn-primary_reservation" id="today" style="margin-top:25px;">Today</button>
                             
                          </div>

                      </div>
                </div>

                </form>-->
               
                <div class="col-md-12 form-group">
                <h3 id="room-name">&nbsp;</h3>
               
                </div>
               <div class="panel-body">
                    <div id="floorplan"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Floorplan V2 -->
</div>

<!--calendar modal-->
<div class="modal fade"  id="complete_confirmation1"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-lg" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log1">
       <div class="row">
              <div class="col-md-6 row">
                    <div id='calendar2'></div>
              </div>
              <div class="col-md-6 row">
                    <div id='calendar'></div>
              </div>
          </div>
    </div>
</div>
</div>


<div class="modal fade"  id="complete_confirmation2"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-lg" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <div class="row">
              <div class="col-md-12 row">
                    <div id='calendar3'></div>
              </div>
             
          </div>
    </div>
</div>
</div>

<!--modals-->
<div class="modal fade" data-backdrop="static" id="complete_confirmationu" tabindex="-1" role="dialog" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabelforConfirmComplete">Updating Calendar...Please wait <span class="matter_for_completion"></span></h4>
      </div>
    </div>
  </div>
</div>

<!--alert-->
<div class="modal fade"  id="complete_confirmation3"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-sm" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <div class="row">
               
                      <div class="col-md-12 row">
                        <center><h5><i class="fa fa-info-circle text-info"></i> What do you want to do?</h5>
                        <button type="button" id="del" class="btn btn-info">Delete</button>
                        <button type="button" id="upd" class="btn btn-info">Update</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>
                        <div><br></div>
                      </div>   
              
              </div>
          </div>
    </div>
</div>
</div>

<div class="modal fade"  id="complete_confirmation4"  aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
<div class="modal-dialog modal-sm" role="dialog" >
    <div class="modal-content _modal_content__" id="new_conference_room_reservation_log"2>
       <center><div class="row">
           
                    <div class="col-md-12 row">
                      <h5><i class="fa fa-info-circle text-info"></i> Are you sure?</h5>
                      <form role="form" method="post" id="formmodal">
                        <input id="eventid1" name="eventidname" type="hidden" >
                        <button type="button" id="dely" class="btn btn-info">Yes</button>
                       
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>   
                      </form>  
                        <div><br></div>
                    </div>
          </div></center> 
    </div>
</div>
</div>
 

<!--other modal-->
<div class="modal fade"  id="complete_confirmation" aria-labelledby="myModalLabelforConfirmComplete" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content" id="new_conference_room_reservation_log2">
        <form role="form" method="post">
          <div class="modal-body">
            <div class="row" id="new_conference_room_reservation_main_form">
            <div id="item-container-source" style="display:block">
            <div class="col-md-12 item-container">
              <div class="panel panel-default">
                <div class="panel-heading clearfix">
                <button type="button" class="close pull-right delete-item_reservation" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12 row">
                        <div class="col-md-12 form-group">
                         <input id="conferenceid" name="conferenceid" type="hidden" >
                         <input id="eventid" name="eventid" type="hidden" >
                              <!--<label class="control-label" for="reservationtime">Choose your start-time and end times:</label>
                              <div class="controls">
                               <div class="input-prepend input-group">
                                 <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                 <input type="text" style="width: 400px" name="new_time[]" id="reservationtime" class="form-control"   class="span4"/>
                               </div>
                              </div>-->
                        
                        </div>
                   
                         <div class="col-md-12 form-group">
                         <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_start_time" name="new_start_time[]" readonly required>
                        </div>
                        <!--<div id="datetimepicker" class="input-append date">
                          <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_start_time" name="new_start_time[]" readonly required>
                          <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                          </span>
                        </div>-->
                        <div class="col-md-12 form-group">
                          <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                          <input type="text" class="form-control" id="new_end_time" name="new_end_time[]" readonly required>
                        </div>
                      <div class="col-md-12 form-group">
                         
                          <label for="reservedby">Reserved By</label>
                          <input type="hidden" class="form-control" id="cond" value="{{$condition}}">
                          <select class="form-control"  id="reservedby2" name="reservedby[]" placeholder="Select Handler" required>
                            <option></option>

                            @foreach($handler_list as $handler)
                            <option value="{{ $handler->EmployeeID }}" {{ $condition  == $handler->EmployeeID ? 'selected' : '' }} >{{ $handler->EmployeeID }} - {{ $handler->NickName }}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="col-md-12 form-group">
                        <label for="new_purpose">Purpose</label>
                        <textarea class="form-control" id="purpose" name="new_purpose[]" rows="3" required></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>

                      <div class="col-md-12 form-group">
                        <label for="new_notes">Notes</label>
                        <textarea class="form-control" id="notes" name="new_notes[]" rows="3"></textarea>
                        <!-- <p class="help-block">Example block-level help text here.</p> -->
                      </div>
                     <!-- <div class="col-md-12 form-group">
                        <label for="new_reservation_date">Reservation Date  <small>(click to select date)</small></label>
                        <input type="text" class="form-control" name="new_reservation_date[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_start_time">Start Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_start_time[]" readonly required>
                      </div>
                      <div class="col-md-6 form-group">
                        <label for="new_end_time">End Time  <small>(click to select time)</small></label>
                        <input type="text" class="form-control" name="new_end_time[]" readonly required>
                      </div>
                    </div>-->
                    
         
                </div>
              </div>  
            </div>
          </div>
            </div>
            <div class="col-md-12">
              <!--<button class="btn btn-info btn-sm" id="btn_add_item2_reservation">Add Item</button>-->
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary_reservation">Save</button>
          </div>
           </div>
           </div>
        </form>
  </div>
</div>
</div>

<link rel='stylesheet' type='text/css' href='/assets/fullcalendar-1.5.4/fullcalendar/fullcalendar.css' />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<!--<script type="text/javascript" src="/assets/thisapp/js/moment.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
<script type='text/javascript' src='/assets/fullcalendar-2/dist/fullcalendar.js'></script>
<link rel="stylesheet" type="text/css" href="/assets/fullcalendar-2/dist/fullcalendar.css" />
<!--<script type='text/javascript' src='/assets/fullcalendar-1.5.4/fullcalendar/fullcalendar.min.js'></script>-->
<script type="text/javascript" src="/assets/thisapp/js/raphael-min.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/floorplan.js"></script>
<script type="text/javascript" src="/assets/thisapp/js/floorplancfrm.js"></script>
<script type="text/javascript" src="/assets/jquery-ui-1.10.4/js/jquery-ui-timepicker-addon.js"></script>
<!--<script type="text/javascript" src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>-->
<!--<script type="text/javascript" src="/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>-->
<!--<link rel="stylesheet" type="text/css" href="/assets/datetimepicker-master/jquery.datetimepicker.css"/ >
<script src="/assets/datetimepicker-master/jquery.js"></script>
<script src="/assets/datetimepicker-master/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/assets/selectize/js/standalone/selectize.min.js"></script>-->

<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="/assets/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/bootstrap-daterangepicker-master/daterangepicker-bs3.css" />
<link rel="stylesheet" type="text/css" href="/assets/qtip/jquery.qtip.css" />
<script type="text/javascript" src="/assets/qtip/jquery.qtip.js"></script>

<script>


var arrne=[];
$(document).ready(function() {

    // page is now ready, initialize the calendar...


    $.get('/conference-room-reservation/allreservation', {}, function(data) {
    //console.log("here2");
   // console.log(data);
   
    var directorylist=data;
    var formattedEventData=[];
    //var arrne=[];
     var k;
        for (k = 0; k < data.length; k += 1) {
            var jsonobj= {
              description:"Reserved By: "+directorylist[k].reserved_by,
              calid: directorylist[k].id,
              purpose: directorylist[k].purpose,
              //title: "Purpose: "+directorylist[k].purpose, 
              title: "Booked", 
              start : (directorylist[k].start_time !="00:00:00" ? directorylist[k].reservation_date+" "+directorylist[k].start_time : directorylist[k].reservation_date), 
              end : (directorylist[k].end_time !="00:00:00" ? directorylist[k].reservation_date+" "+directorylist[k].end_time : directorylist[k].reservation_date),
              resources: directorylist[k].conference_room_id+"",
              allDay:false
              };
    
             arrne.push(jsonobj);
       
        }
        //console.log(arrne);
        $('#calendar').fullCalendar( 'refetchEvents' );
        $('#calendar').fullCalendar( 'removeEventSource', arrne );
        $('#calendar').fullCalendar( 'addEventSource', arrne );
    });
    var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
    var dstring2= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
    //console.log("dstring "+dstring);
    //console.log(getUrlVars()["seldate"]==dstring);
    //$('#new_conference_room_reservation_main_form').find('input[name="new_reservation_date[]"]').datepicker(datepicker_options);
    selectedD=  (getUrlVars()["seldate"] == undefined ? dstring : getUrlVars()["seldate"]);
     $('#datetimepicker2').daterangepicker({ singleDatePicker:true, format: 'YYYY/MM/DD', startDate: new Date()}, function(start, end, label) {
               window.open('/conference-room/?seldate='+ start.format('YYYY/MM/DD'),"_self");});

     

      $('#complete_confirmation1').on('shown.bs.modal', function () {
        $("#complete_confirmationu").modal('show');
       //$("#complete_confirmationu").modal().hide();
      lastfocus = $(this);
      $(this).find('input:text:visible:first').focus();
      $.fn.modal.Constructor.prototype.enforceFocus = function () {};

      tempArray=$(this).serializeArray();
      var newEvent;
      var eventArray=[];
      var roomid1=3;
      var formattedEventData = [];
      var startdate= selectedD;
      //console.log(startdate);
      //var arr = startdate.split('/');
      //var year1 = arr[0];
      //var month1= arr[1] - 1;
      //var day1 = arr[2];
      
      $("#calendar2").fullCalendar('gotoDate',startdate);
      $("#calendar2").fullCalendar('destroy');
      $("#calendar2").fullCalendar('render');
                      
      $("#calendar").fullCalendar('render');
      var calendar2=$("#calendar2").fullCalendar({

          //year: year1,
          //month: month1,
          //date: day1,
          selectable:true,
          defaultDate: startdate,
          select: function(start, end, allDay, jsEvent, view, resource,date) {
          selectedD=start;
                                         
          $("#calendar").fullCalendar('gotoDate',start);
                                     
      }

      });
       $("#calendar").fullCalendar('gotoDate',startdate);
                    var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                      $('#calendar').fullCalendar({
                         header: {
                              left: 'prev,next today',
                              center: 'title',
                              right: 'month,agendaWeek,resourceDay'
                          },
                           defaultDate: startdate,
                          //year: year1,
                          //month: month1,
                          //date: day1,
                           height:500,
                           weekends:false,
                          allDaySlot:false,
                           minTime:"9:00",
                           //titleFormat: "ddd, MMM dd, yyyy",
                           defaultView: "resourceDay",
                           loading: function(bool) {
                            if (bool) 
                               $("#complete_confirmationu").modal('show');
                            else 
                               $("#complete_confirmationu").modal('hide');
                          },
                          events: arrne,
                          selectable: true,
                          selectHelper: true,
                      select: function(start, end, ev) {
                          agenda: 'h:mm{ - h:mm}'
                          $('#new_start_time').val(moment(start).format('YYYY/MM/DD hh:mm a'));//val(startDt+" "+startTime);
                          $('#new_end_time').val(moment(start).format('YYYY/MM/DD hh:mm a'));//val(startDt+" "+startTime);
                           $('#purpose').val('');
                           $('#conferenceid').val(ev.data.id);
                            //console.log("selected resource");
                          //console.log(ev.data.id);
                          // $('#new_start_time').datetimepicker('setDate', start);
                          /*$('#new_start_time').datetimepicker({
                            controlType: 'select',
                            timeFormat: 'hh:mm tt',
                            dateFormat: 'yy/mm/dd',
                            hourMin:9,
                            defaultValue:start,
                            alwaysSetTime:true
                          });

                           $('#new_end_time').datetimepicker({
                            controlType: 'select',
                            timeFormat: 'hh:mm tt',
                             dateFormat: 'yy/mm/dd',
                             hourMin:9,
                            defaultValue:start,
                            alwaysSetTime:true
                          });*/

                          var startDateTextBox = $('#new_start_time');
                          var endDateTextBox = $('#new_end_time');

                            startDateTextBox.datetimepicker({ 
                               controlType: 'select',
                              //timeFormat: 'HH:mm z',
                              timeFormat: 'hh:mm tt',
                              dateFormat: 'yy/mm/dd',
                              hourMin:9,
                              defaultValue:start,
                              alwaysSetTime:true,
                              onClose: function(dateText, inst) {
                                if (endDateTextBox.val() != '') {
                                  var testStartDate = startDateTextBox.datetimepicker('getDate');
                                  var testEndDate = endDateTextBox.datetimepicker('getDate');
                                  if (testStartDate > testEndDate)
                                    endDateTextBox.datetimepicker('setDate', testStartDate);
                                }
                                else {
                                  endDateTextBox.val(dateText);
                                }
                              },
                              onSelect: function (selectedDateTime){
                                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
                              }
                            });
                            endDateTextBox.datetimepicker({ 
                               controlType: 'select',
                              //timeFormat: 'HH:mm z',
                              timeFormat: 'hh:mm tt',
                              dateFormat: 'yy/mm/dd',
                              hourMin:9,
                              defaultValue:start,
                              alwaysSetTime:true,
                              onClose: function(dateText, inst) {
                                if (startDateTextBox.val() != '') {
                                  var testStartDate = startDateTextBox.datetimepicker('getDate');
                                  var testEndDate = endDateTextBox.datetimepicker('getDate');
                                  if (testStartDate > testEndDate)
                                    startDateTextBox.datetimepicker('setDate', testEndDate);
                                }
                                else {
                                  startDateTextBox.val(dateText);
                                }
                              },
                              onSelect: function (selectedDateTime){
                                startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
                              }
                            });
                            
                            $("#complete_confirmation").modal().show();
                        //$('#calendar').fullCalendar( 'renderEvent', eventArray[0] , true); 
                          },
                           /*eventSources: [

                          // your event source
                          {
                              url: '/conference-room-reservation/allreservation',
                              type: 'GET',
                              data: {
                                  roomid : roomid1
                              },
                              error: function() {
                                  alert('there was an error while fetching events!');
                              },
                              color: 'yellow',   // a non-ajax option
                              textColor: 'black', // a non-ajax option
                               cache: true
                          }],*/

                           

                             resources: [
                              {
                                  'name': "Conference Room 1",
                                  'id': "1"
                              },
                              {
                                  'name': "Conference Room 2",
                                  'id': "2"
                              },
                              {
                                  'name': "Conference Room 3",
                                  'id': "3"
                              }
                          ],
                         
                          
                          eventRender: function(event, element) {
                             $("#complete_confirmationu").modal('hide');

                              element.attr("description",event.description);
                              element.attr("calid",event.calid);  
                              element.attr("purpose",event.purpose);                             
                               element.qtip({
                                  content: event.title + '<br />' + event.description  + '<br />'+ "start: "+moment(event.start).format('YYYY/MM/DD hh:mm') + '<br />' + "end: "+moment(event.end).format('YYYY/MM/DD hh:mm'),
                                  style: {
                                      background: 'black',
                                      color: '#FFFFFF'
                                  },
                                  position: {
                                      corner: {
                                          target: 'center',
                                          tooltip: 'bottomMiddle'
                                      }
                                  }
                              });
                          },
                            eventAfterRender: function(event, element, view) {
                             $("#complete_confirmationu").modal('hide');

                              
                          },
                           eventAfterAllRender: function(event, element, view) {
                             $("#complete_confirmationu").modal('hide');

                              
                          },
                          eventClick: function(event) {
                             //console.log("selected event");
                          //console.log(event);
                          $('#new_start_time').val(moment(event.start).format('YYYY/MM/DD hh:mm a'));//val(startDt+" "+startTime);
                          $('#new_end_time').val(moment(event.end).format('YYYY/MM/DD hh:mm a'));
                         // $('#purpose').val(event.description.substring(9,event.description.length));
                          $('#purpose').val(event.purpose);
                          $('#eventid').val(event.calid);
                           $('#conferenceid').val(event.resources[0]);
                          //$('#conferenceid').val(calEvent.calid));
                          
                          var startDateTextBox = $('#new_start_time');
                          var endDateTextBox = $('#new_end_time');

                            startDateTextBox.datetimepicker({ 
                               controlType: 'select',
                              //timeFormat: 'HH:mm z',
                              timeFormat: 'hh:mm tt',
                              dateFormat: 'yy/mm/dd',
                              hourMin:9,
                              defaultValue:event.start,
                              alwaysSetTime:true,
                              onClose: function(dateText, inst) {
                                if (endDateTextBox.val() != '') {
                                  var testStartDate = startDateTextBox.datetimepicker('getDate');
                                  var testEndDate = endDateTextBox.datetimepicker('getDate');
                                  if (testStartDate > testEndDate)
                                    endDateTextBox.datetimepicker('setDate', testStartDate);
                                }
                                else {
                                  endDateTextBox.val(dateText);
                                }
                              },
                              onSelect: function (selectedDateTime){
                                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
                              }
                            });
                            endDateTextBox.datetimepicker({ 
                               controlType: 'select',
                              //timeFormat: 'HH:mm z',
                              timeFormat: 'hh:mm tt',
                              dateFormat: 'yy/mm/dd',
                              hourMin:9,
                              defaultValue:event.end,
                              alwaysSetTime:true,
                              onClose: function(dateText, inst) {
                                if (startDateTextBox.val() != '') {
                                  var testStartDate = startDateTextBox.datetimepicker('getDate');
                                  var testEndDate = endDateTextBox.datetimepicker('getDate');
                                  if (testStartDate > testEndDate)
                                    startDateTextBox.datetimepicker('setDate', testEndDate);
                                }
                                else {
                                  startDateTextBox.val(dateText);
                                }
                              },
                              onSelect: function (selectedDateTime){
                                startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
                              }
                            });

                            
                        
                             $("#complete_confirmation3").modal().show();
                           //$("#complete_confirmation").modal().show();

                        }
                      });

                     
           });
   

});



 $('#new_conference_room_reservation_log2 form').submit(function(e){
        e.preventDefault();
        tempArray=$(this).serializeArray();

       

          $.post('/conference-room-reservation/submitnewreservation', tempArray, function(data){
                    
                /*var myStringArray = data.val;
                //myStringArray.push()
                var arrayLength = myStringArray.length;
                console.log(myStringArray);
                var events = [];
                for (var i = 0; i < arrayLength; i++) {

                        var str1 = data.date;
                        var space=' ';
                       var str2 = space.concat(data.val[i]['start_time'].substring(0,5));
                       var str3 = space.concat(data.val[i]['end_time'].substring(0,5));
                       console.log('startdate'+str1.concat(str2));
                        console.log('enddate'+str1.concat(str3));
                  events.push({id: i, start: getDateObj(str1.concat(str2)), end: getDateObj(str1.concat(str3))});
                      }
                      var toaddst=$('#new_start_time').val();
                      var toaddet=$('#new_end_time').val();
                      console.log("toaddtime"+toaddst);
                      console.log("toaddet"+toaddet);

                      events.push({id: arrayLength, start: getDateObj(toaddst), end: getDateObj(toaddet)});//add the one to be added
                      var overlap=getOverlappingEvents(events).join('\n');
                      //console.log(overlap);
                      if(overlap.length>0){
                         toastr.error('Scheduled time is overlapping with another reservation');
                      }else{
                        //data['new_start_time']=moment($('#new_start_time').val()).format('YYYY-MM-DD');
                        //console.log(data['new_start_time']);
                  */
                        $.post('/conference-room-reservation/savenewreservation2', tempArray, function(data){
                                // console.log(data);
                                if(data.status == 'success') {
                                    toastr.success(data.msg);
                                  var delay = 3000; //Your delay in milliseconds
                                  setTimeout(function(){ window.location = '{{ URL::to('/conference-room') }}'; }, delay);                
                                } else {
                                    toastr.error(data.msg);
                                }
                        });
                        
                      //}

                });
              
            
}); 

$(document).on('click touchstart', '#today', function(e){
        e.preventDefault();
        var d = new Date();
  //console.log(d);
  var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  //console.log(dstring);
   // $('#datetimepicker2').val(dstring);
      window.open('/conference-room/?seldate='+ dstring,"_self");
});

$(document).on('click touchstart', '#del', function(e){
        e.preventDefault();
        // tempArray=$(this).serializeArray();
       $("#complete_confirmation3").modal('hide');
        $('#eventid1').val($('#eventid').val());
         //console.log(data['eventid1'][0]);
       $("#complete_confirmation4").modal('show');
    
});

$(document).on('click touchstart', '#dely', function(e){
        e.preventDefault();
      tempArray=$('#formmodal').serializeArray();
      $('#eventidname').val($('#eventid').val());

        //console.log(tempArray);
      $.post('/conference-room-reservation/deletereservation', $('#formmodal').serializeArray(), function(data){
                                // console.log(data);
                                if(data.status == 'success') {
                                    toastr.success(data.message);
                                  var delay = 3000; //Your delay in milliseconds
                                  setTimeout(function(){ window.location = '{{ URL::to('/conference-room') }}'; }, delay);                
                                } else {
                                    toastr.error(data.msg);
                                }
                        });
  
});


 

$(document).on('click touchstart', '#upd', function(e){
   e.preventDefault();
   $("#complete_confirmation3").modal('hide');
       $("#complete_confirmation").modal('show');
  
});

function setDate(){

  var d = new Date();
  //console.log(d);
  var dstring= d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
  //console.log(dstring);
    $('#datetimepicker2').val(dstring);
}

function getDateObj(s) {
      var bits = s.split(/[/ :]/);
      var date = new Date(bits[0], bits[1] - 1, bits[2]);
      date.setHours(bits[3], bits[4], 0);
      return date;
    }

    
    function getOverlappingEvents(eventArray) {
      var result = [];
      var a, b;
      //console.log('eventarray'+eventArray);

      // Sort the event array on start time
      eventArray.sort(function(a, b) {
          return a.start - b.start;
        });

      // Get overlapping events
      for (var i=0, iLen=eventArray.length - 1; i<iLen; i++) {
        a = eventArray[i];
        b = eventArray[i + 1];

        if((a.start <= b.start && b.end <=a.end) || (a.start <=b.start && b.start <a.end) 
          || (a.start <=b.end && b.end<=a.end)){
        //if ((a.start <= b.start && a.end > b.start) ||
            //(a.start < b.end && a.end >= b.end) ) {
           result.push([a.id, b.id]);
        }
      }
      return result;
    }
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


</script>
@stop
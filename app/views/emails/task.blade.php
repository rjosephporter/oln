<html>
    <body>
        <?php
        $msg = json_decode($task_data['message'],TRUE);
        $response = json_decode($task_data['response'],TRUE);
        $conversations = json_decode($task_data['conversations'],TRUE);
        ?>
        <h3>OLDHAM, LI & NIE</h3>
        <h4>Click this <a href="<?php echo $base_url ?>"><b><u>link</u></b></a> to login to the OEMS and create your reply.</h4>
        <?php if($task_data['type'] == '6'){ ?>
            <h4>Click <a href="<?php echo $base_url ?>/commcenter/forcerequestapproval?tkid=<?php echo $task_data['hash'] ?>"><b><u>here</u></b></a> to approve this request immediately.</h4>
        <?php } ?>
        <h4>Click <a href="<?php echo $base_url ?>/commcenter/forcefinishtask?tkid=<?php echo $task_data['hash'] ?>&mtd=mail"><b><u>here</u></b></a> to acknowledge & finish this task with no comments.</h4>
        <hr style="margin:0">
        <p style="font-size:1.2em">STATUS: <?php echo $task_data['status'] == 1 ? 'COMPLETED':'PENDING'?></p>
        <b style="font-size:1.0em">TASK FOR <?php echo $doer; ?></b><br>
        <b style="font-size:1.0em">REQUESTED BY <?php echo $asker ?></b><br>
        <b style="font-size:1.0em">REQUIRED ON
            <?php echo $task_data['requiredbydate'] == '0000-00-00 00:00:00' ? 'TBA' : date('M j, Y',strtotime($task_data['requiredbydate'])); ?>
        </b>
        <br>
        <p style="font-size:1.5em"><?php echo $msg['message'] ?></p>
        <?php if(isset($files['taskfiles'])){ ?>
        <?php foreach($files['taskfiles'] as $taskfile): ?>
            <img style="margin-top:10px; height:auto; max-width:100%" alt="" src="<?php echo $message->embed(public_path().$taskfile); ?>">
        <?php endforeach ?>
        <?php } ?>
        <hr style="margin:0">
        <b style="font-size:1.0em">CONVERSATIONS</b>
        <p style="font-size:1.5em">
            <?php
            if(!empty($conversations)){
                foreach($conversations as $conversation):
                    echo '[On '.date('h:ia, D, M d',strtotime($conversation['datecreated'])).' '.$doer.' said'.']: '.$conversation['conversation']['message'].'<br>';
                endforeach;
            }
            ?>
        </p>
        <br>
        <b style="font-size:1.0em">FINAL RESPONSE</b>
        <p style="font-size:1.5em"><?php echo $response['message'] != '' ? $response['message'] : ''?></p>
        <br>
        <?php if(isset($files['responsefiles'])){ ?>
        <?php foreach($files['responsefiles'] as $responsefile): ?>
                <img style="margin-top:10px; height:auto; max-width:100%" alt="" src="<?php echo $message->embed(public_path().$responsefile); ?>">
        <?php endforeach ?>
        <?php } ?>
    </body>
</html>

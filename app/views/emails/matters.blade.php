<?php
    $billable = 0;
    $billed = 0;
    $unbilled = 0;
?>
<div style="padding-bottom: 30px;">
    <h3> {{ ucfirst($type) }} Matters for {{ $matters[0]['Incharge'] }}</h3>
</div>
<table style="border-width: 1px; border-style: solid; border-color: #FFFFFF; padding-bottom: 30px;" cellpadding='5' >
    <tr>
        <th style="color: black; background-color:  bisque; text-align: center" colspan="2">LEGEND</th>
    </tr>
    <tr style='color: black;'>
        <td style='background-color: rgba(0, 128, 0, 0.5);'>Green</td><td style='background-color: rgba(0, 128, 0, 0.5);'>Active Last 4 Weeks</td>
    </tr>
    <tr style='color: black;'>
        <td style='background-color: rgba(255, 165, 0, 0.5);'>Orange</td><td style='background-color: rgba(255, 165, 0, 0.5);'>Last Activity Between 4 and 8 Weeks</td>
    </tr>
    <tr style='color: black;'>
        <td style='background-color: rgba(255, 0, 0, 0.5);'>Red</td><td style="background-color: rgba(255, 0, 0, 0.5);">Last Activity Beyond 8 Weeks</td>
    </tr>    
    <tr style='color: white;'>
        <td style='background-color: #0075b0;'>Blue</td><td style='background-color: #0075b0'>Completed Matters</td>
    </tr>    
        
    <tr style='color: black;'>
        <td style='background-color: rgba(128, 128, 128, 0.5);'>Gray</td><td style='background-color: rgba(128, 128, 128, 0.5);'>Untouched Matters/No Activity</td>
    </tr>    
    
</table>
<table style="border-width: 0.5px; border-style: solid; border-color: #FFFFFF;" >
    <thead>
        <tr style="color: black; padding: 20px; border-color: black; border-style: solid; border-width: 0.5px; background-color: bisque;">
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>No.</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Client</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Reference</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Matter</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Billable Amount</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Billed Amount</th>
            <th style='border-color: black; border-style: solid; border-width: 0.5px;'>Unbilled Amount</th>
        </tr>
    </thead>
    <tbody>
    @for($i = 0; $i < count($matters); $i++)
    <tr style="color: {{ $matters[$i]['matter_type'] == 'blue' ? 'white' : 'black' }}; background-color: {{ $matters[$i]['matter_type'] == 'blue' ? "#0075b0"  : ($matters[$i]['matter_type']== 'green' ? "rgba(0, 128, 0, 0.5)" : ($matters[$i]['matter_type'] == 'orange' ? 'rgba(255, 165, 0, 0.5)' : ($matters[$i]['matter_type'] == 'red' ? "rgba(255, 0, 0, 0.5)" : 'rgba(128, 128, 128, 0.5)')))}};">
            <td style="padding: 5px;">{{ $i + 1 }}</td>
            <td style="padding: 5px;">{{ $matters[$i]['client_name'] }}</td>
            <td style="padding: 5px;">{{ $matters[$i]['Description_A'] }}</td>
            <td style="padding: 5px;">{{ $matters[$i]['JobAssignmentID'] }}</td>
            <td style="text-align: right; padding: 5px;">${{ number_format($matters[$i]['billable_amount'],2,'.',',') }}</td>
            <td style="text-align: right; padding: 5px;">${{ number_format($matters[$i]['billed_amount'],2,'.',',') }}</td>
            <td style="text-align: right; padding: 5px;">${{ number_format($matters[$i]['unbilled'],2,'.',',') }}</td>
        </tr>
        <?php
            $billable += $matters[$i]['billable_amount'];
            $billed += $matters[$i]['billed_amount'];
            $unbilled += $matters[$i]['unbilled'];
        ?>
    @endfor
    </tbody>
    <tfoot>
        <tr style="color: black; padding: 10px; border-color: black; border-style: solid; border-width: 0.5px; background-color: bisque;">
            <th style="text-align: right; padding-right: 5px; border-color: black; border-style: solid; border-width: 0.5px;" colspan="4">TOTALS</th>
            <th style="text-align: right; padding: 5px; border-color: black; border-style: solid; border-width: 0.5px;">$<?php echo number_format($billable,2,'.',','); ?></th>
            <th style="text-align: right; padding: 5px; border-color: black; border-style: solid; border-width: 0.5px;">$<?php echo number_format($billed,2,'.',','); ?></th>
            <th style="text-align: right; padding: 5px; border-color: black; border-style: solid; border-width: 0.5px;">$<?php echo number_format($unbilled,2,'.',','); ?></th>
        </tr>
    </tfoot>
</table>
@extends('template/email')
@section('content')
<?php if(isset($final_message)){ ?>
    <br>
    <table cellpadding="0" cellspacing="0" border="0" align="center">
        <tr>
            <td width="600" valign="top">
                <?php // if(isset($final_message)){ ?>
                        <?php echo $final_message ?>
                <?php // } ?>
            </td>
        </tr>
    </table>
<?php } ?>
<?php if(isset($replies)){ ?>
    <br>
    <table cellpadding="0" cellspacing="0" border="0" align="center">
        <?php // if(isset($replies)){ ?>
            <?php $i = 0 ?>
            <?php foreach($replies as $reply): ?>
                <?php $reply_message = json_decode($reply->message,true) ?>
                <tr>
                    <td width="600" valign="top">
                        <?php echo $i != 0 ? '<hr>Reply from '.$reply->NickName.' on '.$reply->datecreated.'<br>' : '' ?>
                        <?php echo $reply_message['message'] ?>
                    </td>
                </tr>
                <?php $i++ ?>
            <?php endforeach ?>
        <?php // } ?>
    </table>
<?php } ?>
<?php if(isset($message_header)){ ?>
    <br>
    <table cellpadding="0" cellspacing="0" border="0" align="center">
        <tr>
            <td width="600" valign="top">
                <?php // if(isset($message_header)){ ?>
                        <?php echo $message_header ?>
                <?php // } ?>
            </td>
        </tr>
    </table>
<?php } ?>
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <?php if(isset($message_header)){ ?>
        <td bgcolor="#000080" width="2"></td>
        <td width="8"></td>
        <?php } ?>
        <td width="<?php echo isset($message_header) ? '590' : '600' ?>" valign="top" id="task">
            <?php echo $message['message'] ?>
        </td>
    </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" border="0" align="center">
    <tr>
        <td width="600" valign="top">
            <?php echo $footer_message ?>
        </td>
    </tr>
</table>
@stop


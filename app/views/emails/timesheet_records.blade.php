<html>
<body>

<p>Hello <?php echo $user_name ?>,</p>

<p>This is a list of time sheet records you made and/or made in your behalf yesterday <?php echo $report_date ?>.</p>

<table border="1">
	<thead>
            <tr>
		<th>Work Date</th>
                <th>Input Date</th>
		<th>Solicitor</th>
                <th>Matter</th>
		<th>Descriptions</th>
		<th>Unit</th>
            </tr>    
	</thead>
	<tbody>
            <?php $total_units = 0 ?>
            <?php if(!is_null($records)){foreach($records as $record): ?>
            <tr>
                <td><?php echo date('m/d/Y',strtotime($record->WorkDate)) ?></td>
                <td><?php echo date('m/d/Y',strtotime($record->Date_C)) ?></td>
                <td><?php echo $record->EmployeeID ?></td>
                <td><?php echo $record->JobAssignmentID ?></td>
                <td><?php echo $record->Comment ?></td>
                <td style="text-align:right"><?php echo $record->units ?></td>
            </tr>
            <?php $total_units += $record->units ?>
            <?php endforeach; } else {?>
            <tr>
                <td colspan="6">No records made!</td>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="5" style="text-align:right">Total:&nbsp;&nbsp;</td>
                <td style="text-align:right"><?php echo $total_units ?></td>
            </tr>
	</tbody>
</table>

<p>To view more details, click <a href="http://192.168.99.8:9999">here</a> to login into the OEMS</p>
<p>Thank you,</p>
<p>OEMS</p>
</body>
</html>
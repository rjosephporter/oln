<html>
<body>

<p>Hello {{ $handler_name }},</p>

<p>This is to remind you that the following matters below have been inactive for 8 weeks or more.</p>

<table border="1">
	<thead>
		<th>Matter ID</th>
		<th>Client Name</th>
		<th>Matter Name</th>
		<th>Unbilled Amount</th>
	</thead>
	<tbody>
		@foreach($matters as $matter)
		<tr>
			<td>{{ $matter->JobAssignmentID }}</td>
			<td>{{ $matter->client_name }}</td>
			<td>{{ $matter->Description_A }}</td>
			<td>${{ number_format(abs($matter->unbilled), 2) }}</td>
		</tr>
		@endforeach
	</tbody>
</table>

<p>To view more details, login to <a href="{{ $base_url }}/auth/login">{{ $base_url }}/auth/login</a> using the credentials below:</p>
<p>
Username: {{ $handler_id }} <br/>
Default Password: 123456
</p>
<p>Then visit this link <a href="{{ $base_url }}/handlers/matters?type=red&handler={{ $handler_id }}">{{ $base_url }}/handlers/matters?type=red&handler={{ $handler_id }}</a></p>

<p>Thanks!</p>

</body>
</html>
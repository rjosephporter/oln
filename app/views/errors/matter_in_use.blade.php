@extends('template/default')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger" role="alert">Unable to create {{ $type }} for Matter {{ $matter_id }} right now. Someone is either creating/updating an invoice and/or timesheet  for this matter.</div>
		</div>
	</div>
</div>
@stop
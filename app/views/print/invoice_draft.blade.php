@extends($parent_template)
@section('content')
<style>
.no-margin-bottom {
	margin-bottom: 0px;
}

.top-bottom-margin {
	margin-top: 15px;
	margin-bottom: 15px;
}

.double-underline {
    text-decoration:underline;
    border-bottom: 1px solid #000;
}

.signature {
	float: left; 
	margin: 20px 10px;
	border-top: 1px solid #000;
	width: 200px; 
	text-align: center;
}

#watermark {
    position: absolute;
    top: 5%;
    left: 50%;
    margin-left: -100px;
}

.print {
    font-size:65% !important
}

.print .form-control-static {
    padding-top: 0px !important
}

.print .control-label {
    padding-top: 0px !important
}

.print table{
    margin-bottom: 0px !important
}

.print table td{
    padding: 3px !important
}

.print table th{
    padding: 3px !important
}


</style>
<div id="watermark">
    <img src="/assets/thisapp/images/TLTCF.png" alt="..." width="200px"/>
</div>
<div class="container print">
	<div class="row hidden-print">
		<div class="col-md-12">
			<div class="btn-group pull-right">
				<button id="print-page" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 text-center">
			<!-- <h2 class="text-center">{{ $template['company_name'] }}</h2> -->
			<img src="{{ URL::to('/') }}/assets/thisapp/images/oln_invoice_logo.png" style="width:250px" />
		</div>
	</div>

	<div class="row top-buffer">
		<div class="col-md-6 col-sm-6 col-xs-6">
			<address>
				{{ $template['company_address_1'] }}<br>
				{{ $template['company_address_2'] }}<br>
				{{ $template['company_address_3'] }}<br>
			</address>			
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<address class='text-right'>
				{{ Lang::get('invoice.Tel') }}: {{ $template['company_tel_no'] }}<br>
				{{ Lang::get('invoice.Fax') }}: {{ $template['company_fax_no'] }}<br>
				{{ Lang::get('invoice.Email') }}:
				<u>{{ $handler->email_address or '' }}</u>
			</address>			
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-6">
			<form class="form-horizontal" role="form">
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Client') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['name'] or '' }}</p>
					</div>
				</div>
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Address') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['address'] or '' }}</p>
					</div>
				</div>
				@if(!empty($client['attn']))
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Attn') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['attn'] or '' }}</p>
					</div>
				</div>
				@endif
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Email') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['email'] or '' }}</p>
					</div>
				</div>
<!--				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Tel') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['telephone'] or '' }}</p>
					</div>
				</div>
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Fax') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $client['fax'] or '' }}</p>
					</div>
				</div>-->
				<div class="form-group no-margin-bottom">
					<label class="col-sm-2 col-xs-2 control-label">{{ Lang::get('invoice.Re') }}</label>
					<div class="col-sm-10 col-xs-10">
						<p class="form-control-static">{{ $matter->Description_A or '' }}</p>
					</div>
				</div>				
			</form>			
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<form class="form-horizontal" role="form">
				<div class="form-group no-margin-bottom">
					<label class="col-sm-6 col-xs-6 control-label text-right">{{ Lang::get('invoice.Date') }}</label>
					<div class="col-sm-6 col-xs-6">
						<p class="form-control-static">{{ date('j F Y', strtotime($date)) }}</p>
					</div>
				</div>
				<div class="form-group no-margin-bottom">
					<label class="col-sm-6 col-xs-6 control-label text-right">{{ Lang::get('invoice.Ref') }}</label>
					<div class="col-sm-6 col-xs-6">
						<p class="form-control-static">{{ $reference_number or '' }}</p>
					</div>
				</div>
				<div class="form-group no-margin-bottom">
					<label class="col-sm-6 col-xs-6 control-label text-right">{{ Lang::get('invoice.Invoice No.') }}</label>
					<div class="col-sm-6 col-xs-6">
						<p class="form-control-static">{{ $new_id or '' }}</p>
					</div>
				</div>
			</form>	
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<p>{{ Lang::get('invoice.To our professional charges for action on your behalf in connection with that above matter from :from_date to :to_date, particulars of which are as follows:', array('from_date' => date('j F Y', strtotime($min_workdate)), 'to_date' => date('j F Y', strtotime($max_workdate)))) }}</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table table-condensed">
<!--				<col width="10%">
				<col width="10%">
				<col width="{{ ($total_complimented_units > 0) ? '60%' : '70%' }}">
				@if($total_complimented_units > 0)
				<col width="10%">
				@endif
				<col width="10%">-->
				<thead>
					<tr>
						<th width="75px">{{ Lang::get('invoice.Date') }}</th>
						<th>{{ Lang::get('invoice.Solicitor') }}</th>
						<th>{{ Lang::get('invoice.Description') }}</th>
						@if($total_complimented_units > 0)
						<th>{{ Lang::get('invoice.Complimented Units') }}</th>
						@endif
						<th width="75px">{{ Lang::get('invoice.Charged Units') }}</th>						
					</tr>
				</thead>
				<tbody>
					@foreach($invoice_items as $item)
					<tr>
						<td>{{ date('d-M-Y', strtotime($item['WorkDate'])) }}</td>
						<td>{{ $item['EmployeeID'] }}</td>
						<td>{{ $item['Comment'] }}</td>
						@if($total_complimented_units > 0)
						<td>{{ $item['complimented_units'] > 0 ? round($item['complimented_units']) : null }}</td>
						@endif
						<td>{{ round($item['units']) }}</td>						
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="{{ ($total_complimented_units > 0) ? '4' : '3' }}" class="text-right"><strong>{{ Lang::get('invoice.TOTAL UNITS') }}</strong></td>
						<td><strong>{{ round($total_units) }}</strong></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<h4>{{ Lang::get('lang.invoice.Our Fees') }}</h4>
		</div>
		<div class="col-md-12">
			<table class="table table-condensed">
<!--				<col width="90%">
				<col width="10%">				-->
				<tbody>
					@foreach($costs as $cost)
					<tr>
						<td>
							{{ Lang::get('invoice.Work undertaken by', array('handler_name' => User::getFullName($cost['EmployeeID']), 'handler_id' => $cost['EmployeeID'], 'employee_job' => $employee_job[$cost['EmployeeID']], 'hourly_rate' => number_format($cost['HourlyRate'],2))) }}
							<br/>
							{{  HelperLibrary::convertToHoursMins( $work_hours_per_handler[$cost['EmployeeID']] * 60, '%2d hours %2d minutes' ) }}
						</td>
						<td width="75px">${{ number_format($cost['sum_amount'], 2) }}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL COSTS') }}</strong></td>
						<td><strong>${{ number_format($total_costs, 2) }}</strong></td>
					</tr>
					@if( !empty($invoice['agreed_cost']) )
					<tr>
						<td class="text-right"><strong>{{ Lang::get('lang.invoice.BUT SAY') }}</strong></td>
						<td><strong>${{ number_format($agreed_cost, 2) }}</strong></td>
					</tr>
					@endif
				</tfoot>
			</table>
		</div>
	</div>

	{{-- @if(count($disbursements) > 0) --}}
        @if($total_disbursements > 0)
	<div class="row">
		<div class="col-md-12">
			<h4>{{ Lang::get('invoice.Disbursements') }}</h4>
		</div>
		<div class="col-md-12">
			@if(count($disbursements) == 0)
				<p>{{ Lang::get('invoice.No disbursements') }}</p>
			@else
			<table class="table table-condensed">
<!--				<col width="90%">
				<col width="10%">				-->
				<tbody>
					@foreach($disbursements as $disb)
					<tr>
						<td>{{ $disb['description'] }}</td>
						<td>${{ !empty($disb['amount']) ? number_format($disb['amount'], 2) : number_format(0, 2) }}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL DISBURSEMENTS') }}</strong></td>
						<td width="75px"><strong>${{ number_format($total_disbursements, 2) }}</strong></td>
					</tr>
				</tfoot>
			</table>
			@endif
		</div>		
	</div>
	@endif

	{{-- @if(count($less) > 0) --}}
        @if($total_less > 0)
	<div class="row">
		<div class="col-md-12">
			<h4>{{ Lang::get('invoice.Less') }}</h4>
		</div>
		<div class="col-md-12">
			@if(count($less) == 0)
				<p>{{ Lang::get('invoice.No Less') }}</p>
			@else
			<table class="table table-condensed">
<!--				<col width="90%">
				<col width="10%">				-->
				<tbody>
					@foreach($less as $l)
					<tr>
						<td>{{ $l['description'] }}</td>
						<td>${{ !empty($l['amount']) ? number_format($l['amount'], 2) : number_format(0, 2) }}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL LESS') }}</strong></td>
						<td width="75px"><strong>${{ number_format($total_less, 2) }}</strong></td>
					</tr>
				</tfoot>
			</table>
			@endif
		</div>		
	</div>
	@endif	

	<div class="row">
		<div class="col-md-12">
			<table class="table table-condensed">
<!--				<col width="90%">
				<col width="10%">-->
				<tbody>
					<tr>
						<td class="text-right"><strong>{{ Lang::get('invoice.TOTAL INVOICE') }}</strong></td>
						<td width="75px"><strong class="double-underline">${{ number_format($total_cost_disb, 2) }}</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row top-bottom-margin">
		<div class="col-md-12">
			<span class="signature">{{ Lang::get('invoice.WITH COMPLIMENTS') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<p>{{ Lang::get('invoice.Cheques should be made payable to') }}: <strong>{{ $template['cheque_payable_to'] }}</strong></p>
			<p>
				{{ Lang::get('invoice.Telegraphic Transfer') }}:<br/>
				{{ $template['name_of_banker']['line_1'] }}<br/>
				{{ $template['name_of_banker']['line_2'] }}<br/>
				{{ $template['name_of_banker']['line_3'] }}<br/>
				{{ $template['name_of_banker']['line_4'] }}<br/>
			</p>
			<p>
				{{ Lang::get('invoice.Account Name') }}: {{ $template['account_name'] }}<br/>
				{{ Lang::get('invoice.Account No.') }}: {{ $template['account_no'] }}<br/>
				{{ Lang::get('invoice.Swift Code') }}: {{ $template['swift_code'] }}<br/>
			</p>
			<p>
				{{ Lang::get('invoice.Please note that interest will accrue at the rate of :interest_rate per month in the event that this invoice is not settled within :no_of_days days from the date hereof.', array('interest_rate' => '2%', 'no_of_days' => 30)) }}
			</p>
			<p>
				{{ Lang::get('invoice.No receipt will be issued unless requested.') }}
			</p>
			<p>
				{{ Lang::get('invoice.Bills may be paid by VISA or MASTERCARD in person at our office.') }}
			</p>
			<p>
				E. & O.E.
			</p>
		</div>
	</div>	

</div>

<script>
$('#print-page').click(function(e) {
	e.preventDefault();
	window.print();
});
@if(Input::has('direct_print'))
	$(document).ready(function() {
		window.print();	
	});
@endif
</script>
@stop
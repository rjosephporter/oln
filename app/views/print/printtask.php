@extends('template/print')
@section('content')
<?php
foreach($task as $temp):
    $task_data = $temp;
endforeach;
$message = json_decode($task_data->message,TRUE);
$response = json_decode($task_data->response,TRUE);
?>
<div class="row">
    <div class="row">
        <div class="col-xs-3" style="text-align:center">
            <h3>OLDHAM, LI & NIE</h3>
        </div>
    </div>
</div>
<hr style="margin:0">
<div class="row">
    <div class="col-xs-12">
        <h5 style="text-align:center"></h5>
    </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <b style="font-size:1.2em">STATUS: <?php echo $task_data->status == 1 ? 'COMPLETED':'PENDING'?></b>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <b style="font-size:1.0em">TASK FOR
        <?php
            $doer = Helper::search_array_r((array)$users, 'user_id', $task_data->owner);
            echo $doer[0]['NickName'];
        ?>
    </b><br>
    <b style="font-size:1.0em">REQUESTED BY
        <?php
            if($task_data->requestedby != 'mtg' && $task_data->requestedby != ''){
                $requestedby = Helper::search_array_r((array)$users, 'uid', $task_data->requestedby);
                if($requestedby){
                    echo $requestedby[0]['NickName'];
                } else {
                    echo 'System';
                }

            } elseif($task_data->requestedby == 'mtg'){
                echo 'From Meeting';
            }
        ?>
    </b><br>
    <b style="font-size:1.0em">REQUIRED ON
        <?php
                if($task_data->requiredbydate == '0000-00-00 00:00:00'){
                    echo 'TBA';
                } else {
                    echo date('M j, Y',strtotime($task_data->requiredbydate));
                }
                ?>
        </b>
    <br>
    <p style="font-size:1.5em"><?php echo $message['message'] ?></p>
    <?php foreach($files['taskfiles'] as $taskfile): ?>
        <img style="margin-top:10px; height:auto; max-width:100%" class="img-rounded" src="http://192.168.99.8:9999/<?php echo $taskfile->filename ?>"/>
    <?php endforeach ?>
  </div>
</div>
<div style="margin-top:15px">
    <div class="panel panel-default">
      <div class="panel-body">
        <b style="font-size:1.0em">RESPONSE</b>
        <br>
        <p style="font-size:1.5em"><?php echo $response['message'] ?></p>
        <br>
        <?php foreach($files['responsefiles'] as $responsefile): ?>
            <img style="margin-top:10px; height:auto; max-width:100%" class="img-rounded" src="http://192.168.99.8:9999/<?php echo $responsefile->filename ?>"/>
        <?php endforeach ?>
      </div>
    </div>
</div>
@stop
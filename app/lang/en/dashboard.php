<?php

return array(
	'Main Dashboard'	=> 'Main Dashboard',
	'My Desk'			=> 'My Desk',

	'Matters untouched for 8 weeks or more.' => 'Matters untouched for 8 weeks or more.',

	'Matter ID'			=> 'Matter ID',
	'Handler'			=> 'Handler',
	'Client Name'		=> 'Client Name',
	'Matter Name'		=> 'Matter Name',
	'Unbilled Amount'	=> 'Unbilled Amount',
);